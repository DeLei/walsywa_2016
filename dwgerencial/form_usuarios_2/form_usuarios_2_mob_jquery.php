
function scJQGeneralAdd() {
  $('input:text.sc-js-input').listen();
  $('input:password.sc-js-input').listen();
  $('input:checkbox.sc-js-input').listen();
  $('select.sc-js-input').listen();
  $('textarea.sc-js-input').listen();

} // scJQGeneralAdd

function scFocusField(sField) {
  var $oField = $('#id_sc_field_' + sField);

  if (0 == $oField.length) {
    $oField = $('input[name=' + sField + ']');
  }

  if (0 == $oField.length && document.F1.elements[sField]) {
    $oField = $(document.F1.elements[sField]);
  }

  if (false == scSetFocusOnField($oField) && 0 < $("#id_ac_" + sField).length) {
    scSetFocusOnField($("#id_ac_" + sField));
  }
} // scFocusField

function scSetFocusOnField($oField) {
  if (0 < $oField.length && 0 < $oField[0].offsetHeight && 0 < $oField[0].offsetWidth && !$oField[0].disabled) {
    $oField[0].focus();
    return true;
  }
  return false;
} // scSetFocusOnField

function scEventControl_init(iSeqRow) {
  scEventControl_data["status" + iSeqRow] = {"blur": false, "change": false, "autocomp": false, "original": ""};
  scEventControl_data["nome" + iSeqRow] = {"blur": false, "change": false, "autocomp": false, "original": ""};
  scEventControl_data["senha" + iSeqRow] = {"blur": false, "change": false, "autocomp": false, "original": ""};
  scEventControl_data["nome_real" + iSeqRow] = {"blur": false, "change": false, "autocomp": false, "original": ""};
  scEventControl_data["grupo" + iSeqRow] = {"blur": false, "change": false, "autocomp": false, "original": ""};
  scEventControl_data["cpf" + iSeqRow] = {"blur": false, "change": false, "autocomp": false, "original": ""};
  scEventControl_data["telefone_1" + iSeqRow] = {"blur": false, "change": false, "autocomp": false, "original": ""};
  scEventControl_data["telefone_2" + iSeqRow] = {"blur": false, "change": false, "autocomp": false, "original": ""};
}

function scEventControl_active(iSeqRow) {
  if (scEventControl_data["status" + iSeqRow]["blur"]) {
    return true;
  }
  if (scEventControl_data["status" + iSeqRow]["change"]) {
    return true;
  }
  if (scEventControl_data["nome" + iSeqRow]["blur"]) {
    return true;
  }
  if (scEventControl_data["nome" + iSeqRow]["change"]) {
    return true;
  }
  if (scEventControl_data["senha" + iSeqRow]["blur"]) {
    return true;
  }
  if (scEventControl_data["senha" + iSeqRow]["change"]) {
    return true;
  }
  if (scEventControl_data["nome_real" + iSeqRow]["blur"]) {
    return true;
  }
  if (scEventControl_data["nome_real" + iSeqRow]["change"]) {
    return true;
  }
  if (scEventControl_data["grupo" + iSeqRow]["blur"]) {
    return true;
  }
  if (scEventControl_data["grupo" + iSeqRow]["change"]) {
    return true;
  }
  if (scEventControl_data["cpf" + iSeqRow]["blur"]) {
    return true;
  }
  if (scEventControl_data["cpf" + iSeqRow]["change"]) {
    return true;
  }
  if (scEventControl_data["telefone_1" + iSeqRow]["blur"]) {
    return true;
  }
  if (scEventControl_data["telefone_1" + iSeqRow]["change"]) {
    return true;
  }
  if (scEventControl_data["telefone_2" + iSeqRow]["blur"]) {
    return true;
  }
  if (scEventControl_data["telefone_2" + iSeqRow]["change"]) {
    return true;
  }
  return false;
} // scEventControl_active

function scEventControl_onFocus(oField, iSeq) {
  var fieldId, fieldName;
  fieldId = $(oField).attr("id");
  fieldName = fieldId.substr(12);
  scEventControl_data[fieldName]["blur"] = true;
  scEventControl_data[fieldName]["change"] = false;
} // scEventControl_onFocus

function scEventControl_onBlur(sFieldName) {
  scEventControl_data[sFieldName]["blur"] = false;
  if (scEventControl_data[sFieldName]["change"]) {
        if (scEventControl_data[sFieldName]["original"] == $("#id_sc_field_" + sFieldName).val()) {
          scEventControl_data[sFieldName]["change"] = false;
        }
  }
} // scEventControl_onBlur

function scEventControl_onChange(sFieldName) {
  scEventControl_data[sFieldName]["change"] = false;
} // scEventControl_onChange

function scEventControl_onAutocomp(sFieldName) {
  scEventControl_data[sFieldName]["autocomp"] = false;
} // scEventControl_onChange

var scEventControl_data = {};

function scJQEventsAdd(iSeqRow) {
  $('#id_sc_field_status' + iSeqRow).bind('blur', function() { sc_form_usuarios_2_status_onblur(this, iSeqRow) })
                                    .bind('focus', function() { sc_form_usuarios_2_status_onfocus(this, iSeqRow) });
  $('#id_sc_field_grupo' + iSeqRow).bind('blur', function() { sc_form_usuarios_2_grupo_onblur(this, iSeqRow) })
                                   .bind('focus', function() { sc_form_usuarios_2_grupo_onfocus(this, iSeqRow) });
  $('#id_sc_field_nome' + iSeqRow).bind('blur', function() { sc_form_usuarios_2_nome_onblur(this, iSeqRow) })
                                  .bind('focus', function() { sc_form_usuarios_2_nome_onfocus(this, iSeqRow) });
  $('#id_sc_field_senha' + iSeqRow).bind('blur', function() { sc_form_usuarios_2_senha_onblur(this, iSeqRow) })
                                   .bind('focus', function() { sc_form_usuarios_2_senha_onfocus(this, iSeqRow) });
  $('#id_sc_field_nome_real' + iSeqRow).bind('blur', function() { sc_form_usuarios_2_nome_real_onblur(this, iSeqRow) })
                                       .bind('focus', function() { sc_form_usuarios_2_nome_real_onfocus(this, iSeqRow) });
  $('#id_sc_field_cpf' + iSeqRow).bind('blur', function() { sc_form_usuarios_2_cpf_onblur(this, iSeqRow) })
                                 .bind('focus', function() { sc_form_usuarios_2_cpf_onfocus(this, iSeqRow) });
  $('#id_sc_field_telefone_1' + iSeqRow).bind('blur', function() { sc_form_usuarios_2_telefone_1_onblur(this, iSeqRow) })
                                        .bind('focus', function() { sc_form_usuarios_2_telefone_1_onfocus(this, iSeqRow) });
  $('#id_sc_field_telefone_2' + iSeqRow).bind('blur', function() { sc_form_usuarios_2_telefone_2_onblur(this, iSeqRow) })
                                        .bind('focus', function() { sc_form_usuarios_2_telefone_2_onfocus(this, iSeqRow) });
} // scJQEventsAdd

function sc_form_usuarios_2_status_onblur(oThis, iSeqRow) {
  do_ajax_form_usuarios_2_mob_validate_status();
  scCssBlur(oThis);
}

function sc_form_usuarios_2_status_onfocus(oThis, iSeqRow) {
  scEventControl_onFocus(oThis, iSeqRow);
  scCssFocus(oThis);
}

function sc_form_usuarios_2_grupo_onblur(oThis, iSeqRow) {
  do_ajax_form_usuarios_2_mob_validate_grupo();
  scCssBlur(oThis);
}

function sc_form_usuarios_2_grupo_onfocus(oThis, iSeqRow) {
  scEventControl_onFocus(oThis, iSeqRow);
  scCssFocus(oThis);
}

function sc_form_usuarios_2_nome_onblur(oThis, iSeqRow) {
  do_ajax_form_usuarios_2_mob_validate_nome();
  scCssBlur(oThis);
}

function sc_form_usuarios_2_nome_onfocus(oThis, iSeqRow) {
  scEventControl_onFocus(oThis, iSeqRow);
  scCssFocus(oThis);
}

function sc_form_usuarios_2_senha_onblur(oThis, iSeqRow) {
  do_ajax_form_usuarios_2_mob_validate_senha();
  scCssBlur(oThis);
}

function sc_form_usuarios_2_senha_onfocus(oThis, iSeqRow) {
  scEventControl_onFocus(oThis, iSeqRow);
  scCssFocus(oThis);
}

function sc_form_usuarios_2_nome_real_onblur(oThis, iSeqRow) {
  do_ajax_form_usuarios_2_mob_validate_nome_real();
  scCssBlur(oThis);
}

function sc_form_usuarios_2_nome_real_onfocus(oThis, iSeqRow) {
  scEventControl_onFocus(oThis, iSeqRow);
  scCssFocus(oThis);
}

function sc_form_usuarios_2_cpf_onblur(oThis, iSeqRow) {
  do_ajax_form_usuarios_2_mob_validate_cpf();
  scCssBlur(oThis);
}

function sc_form_usuarios_2_cpf_onfocus(oThis, iSeqRow) {
  scEventControl_onFocus(oThis, iSeqRow);
  scCssFocus(oThis);
}

function sc_form_usuarios_2_telefone_1_onblur(oThis, iSeqRow) {
  do_ajax_form_usuarios_2_mob_validate_telefone_1();
  scCssBlur(oThis);
}

function sc_form_usuarios_2_telefone_1_onfocus(oThis, iSeqRow) {
  scEventControl_onFocus(oThis, iSeqRow);
  scCssFocus(oThis);
}

function sc_form_usuarios_2_telefone_2_onblur(oThis, iSeqRow) {
  do_ajax_form_usuarios_2_mob_validate_telefone_2();
  scCssBlur(oThis);
}

function sc_form_usuarios_2_telefone_2_onfocus(oThis, iSeqRow) {
  scEventControl_onFocus(oThis, iSeqRow);
  scCssFocus(oThis);
}

function scJQUploadAdd(iSeqRow) {
} // scJQUploadAdd


function scJQElementsAdd(iLine) {
  scJQEventsAdd(iLine);
  scEventControl_init(iLine);
  scJQUploadAdd(iLine);
} // scJQElementsAdd

var scBtnGrpStatus = {};
function scBtnGrpShow(sGroup) {
  var btnPos = $('#sc_btgp_btn_' + sGroup).offset();
  scBtnGrpStatus[sGroup] = 'open';
  $('#sc_btgp_btn_' + sGroup).mouseout(function() {
    setTimeout(function() {
      scBtnGrpHide(sGroup);
    }, 1000);
  });
  $('#sc_btgp_div_' + sGroup + ' span a').click(function() {
    scBtnGrpStatus[sGroup] = 'out';
    scBtnGrpHide(sGroup);
  });
  $('#sc_btgp_div_' + sGroup).css({
    'left': btnPos.left
  })
  .mouseover(function() {
    scBtnGrpStatus[sGroup] = 'over';
  })
  .mouseleave(function() {
    scBtnGrpStatus[sGroup] = 'out';
    setTimeout(function() {
      scBtnGrpHide(sGroup);
    }, 1000);
  })
  .show('fast');
}
function scBtnGrpHide(sGroup) {
  if ('over' != scBtnGrpStatus[sGroup]) {
    $('#sc_btgp_div_' + sGroup).hide('fast');
  }
}
