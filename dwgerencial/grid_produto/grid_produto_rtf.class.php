<?php

class grid_produto_rtf
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $nm_data;
   var $texto_tag;
   var $arquivo;
   var $tit_doc;
   var $sc_proc_grid; 
   var $NM_cmp_hidden = array();

   //---- 
   function grid_produto_rtf()
   {
      $this->nm_data   = new nm_data("pt_br");
      $this->texto_tag = "";
   }

   //---- 
   function monta_rtf()
   {
      $this->inicializa_vars();
      $this->gera_texto_tag();
      $this->grava_arquivo_rtf();
      $this->monta_html();
   }

   //----- 
   function inicializa_vars()
   {
      global $nm_lang;
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      $this->arquivo    = "sc_rtf";
      $this->arquivo   .= "_" . date("YmdHis") . "_" . rand(0, 1000);
      $this->arquivo   .= "_grid_produto";
      $this->arquivo   .= ".rtf";
      $this->tit_doc    = "grid_produto.rtf";
   }

   //----- 
   function gera_texto_tag()
   {
     global $nm_lang;
      global
             $nm_nada, $nm_lang;

      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->sc_proc_grid = false; 
      $nm_raiz_img  = ""; 
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['grid_produto']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['grid_produto']['field_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['grid_produto']['field_display'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['usr_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['usr_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['usr_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['php_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['campos_busca']))
      { 
          $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['campos_busca'];
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->produto_produtodescricao = $Busca_temp['produto_produtodescricao']; 
          $tmp_pos = strpos($this->produto_produtodescricao, "##@@");
          if ($tmp_pos !== false)
          {
              $this->produto_produtodescricao = substr($this->produto_produtodescricao, 0, $tmp_pos);
          }
          $this->produto_produtoncm = $Busca_temp['produto_produtoncm']; 
          $tmp_pos = strpos($this->produto_produtoncm, "##@@");
          if ($tmp_pos !== false)
          {
              $this->produto_produtoncm = substr($this->produto_produtoncm, 0, $tmp_pos);
          }
          $this->produto_produtocodigo = $Busca_temp['produto_produtocodigo']; 
          $tmp_pos = strpos($this->produto_produtocodigo, "##@@");
          if ($tmp_pos !== false)
          {
              $this->produto_produtocodigo = substr($this->produto_produtocodigo, 0, $tmp_pos);
          }
          $this->produto_produtogrupocodigo = $Busca_temp['produto_produtogrupocodigo']; 
          $tmp_pos = strpos($this->produto_produtogrupocodigo, "##@@");
          if ($tmp_pos !== false)
          {
              $this->produto_produtogrupocodigo = substr($this->produto_produtogrupocodigo, 0, $tmp_pos);
          }
      } 
      $this->nm_field_dinamico = array();
      $this->nm_order_dinamico = array();
      $this->Sub_Consultas[] = "preco";
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['where_pesq_filtro'];
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['rtf_name']))
      {
          $this->arquivo = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['rtf_name'];
          $this->tit_doc = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['rtf_name'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['rtf_name']);
      }
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nmgp_select = "SELECT produto.produtocodigo as produto_produtocodigo, produto.produtodescricao as produto_produtodescricao, produtogrupo.produtogrupodescricao as cmp_maior_30_1, produto.produtoempresacodigo as produto_produtoempresacodigo, produto.produtofilialcodigo as produto_produtofilialcodigo, produto.produtounidadecodigo as produto_produtounidadecodigo, produto.produtocodigoreduzido as produto_produtocodigoreduzido from " . $this->Ini->nm_tabela; 
      } 
      else 
      { 
          $nmgp_select = "SELECT produto.produtocodigo as produto_produtocodigo, produto.produtodescricao as produto_produtodescricao, produtogrupo.produtogrupodescricao as cmp_maior_30_1, produto.produtoempresacodigo as produto_produtoempresacodigo, produto.produtofilialcodigo as produto_produtofilialcodigo, produto.produtounidadecodigo as produto_produtounidadecodigo, produto.produtocodigoreduzido as produto_produtocodigoreduzido from " . $this->Ini->nm_tabela; 
      } 
      $nmgp_select .= " " . $_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['where_pesq'];
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['where_resumo']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['where_resumo'])) 
      { 
          if (empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['where_pesq'])) 
          { 
              $nmgp_select .= " where " . $_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['where_resumo']; 
          } 
          else
          { 
              $nmgp_select .= " and (" . $_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['where_resumo'] . ")"; 
          } 
      } 
      $nmgp_order_by = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['order_grid'];
      $nmgp_select .= $nmgp_order_by; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select;
      $rs = $this->Db->Execute($nmgp_select);
      if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1)
      {
         $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
         exit;
      }
      $this->New_label['produto_produtocodigo'] = "" . $this->Ini->Nm_lang['lang_public_produto_fld_produtocodigo'] . "";
      $this->New_label['produto_produtodescricao'] = "" . $this->Ini->Nm_lang['lang_public_produto_fld_produtodescricao'] . "";
      $this->New_label['produtogrupo_produtogrupodescricao'] = "" . $this->Ini->Nm_lang['lang_public_produtogrupo_fld_produtogrupodescricao'] . "";
      $this->New_label['produto_produtoncm'] = "" . $this->Ini->Nm_lang['lang_public_produto_fld_produtoncm'] . "";
      $this->New_label['produto_produtogrupocodigo'] = "" . $this->Ini->Nm_lang['lang_public_produtogrupo_fld_produtogrupocodigo'] . "";

      $this->texto_tag .= "<table>\r\n";
      $this->texto_tag .= "<tr>\r\n";
      foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['field_order'] as $Cada_col)
      { 
          $SC_Label = (isset($this->New_label['produto_produtocodigo'])) ? $this->New_label['produto_produtocodigo'] : ""; 
          if ($Cada_col == "produto_produtocodigo" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['produto_produtodescricao'])) ? $this->New_label['produto_produtodescricao'] : ""; 
          if ($Cada_col == "produto_produtodescricao" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['produtogrupo_produtogrupodescricao'])) ? $this->New_label['produtogrupo_produtogrupodescricao'] : ""; 
          if ($Cada_col == "produtogrupo_produtogrupodescricao" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['produto_produtoempresacodigo'])) ? $this->New_label['produto_produtoempresacodigo'] : "Produtoempresacodigo"; 
          if ($Cada_col == "produto_produtoempresacodigo" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['produto_produtofilialcodigo'])) ? $this->New_label['produto_produtofilialcodigo'] : "Produtofilialcodigo"; 
          if ($Cada_col == "produto_produtofilialcodigo" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['produto_produtounidadecodigo'])) ? $this->New_label['produto_produtounidadecodigo'] : "Produtounidadecodigo"; 
          if ($Cada_col == "produto_produtounidadecodigo" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['produto_produtocodigoreduzido'])) ? $this->New_label['produto_produtocodigoreduzido'] : "Produtocodigoreduzido"; 
          if ($Cada_col == "produto_produtocodigoreduzido" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
      } 
      $this->texto_tag .= "</tr>\r\n";
      while (!$rs->EOF)
      {
         $this->texto_tag .= "<tr>\r\n";
         $this->produto_produtocodigo = $rs->fields[0] ;  
         $this->produto_produtodescricao = $rs->fields[1] ;  
         $this->produtogrupo_produtogrupodescricao = $rs->fields[2] ;  
         $this->produto_produtoempresacodigo = $rs->fields[3] ;  
         $this->produto_produtofilialcodigo = $rs->fields[4] ;  
         $this->produto_produtounidadecodigo = $rs->fields[5] ;  
         $this->produto_produtocodigoreduzido = $rs->fields[6] ;  
         $this->sc_proc_grid = true; 
         foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['field_order'] as $Cada_col)
         { 
            if ((!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off") && !in_array($Cada_col, $this->Sub_Consultas))
            { 
                $NM_func_exp = "NM_export_" . $Cada_col;
                $this->$NM_func_exp();
            } 
         } 
         $this->texto_tag .= "</tr>\r\n";
         $rs->MoveNext();
      }
      $this->texto_tag .= "</table>\r\n";

      $rs->Close();
   }
   //----- produto_produtocodigo
   function NM_export_produto_produtocodigo()
   {
         $this->produto_produtocodigo = html_entity_decode($this->produto_produtocodigo, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->produto_produtocodigo = strip_tags($this->produto_produtocodigo);
         if (!NM_is_utf8($this->produto_produtocodigo))
         {
             $this->produto_produtocodigo = mb_convert_encoding($this->produto_produtocodigo, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->produto_produtocodigo = str_replace('<', '&lt;', $this->produto_produtocodigo);
         $this->produto_produtocodigo = str_replace('>', '&gt;', $this->produto_produtocodigo);
         $this->texto_tag .= "<td>" . $this->produto_produtocodigo . "</td>\r\n";
   }
   //----- produto_produtodescricao
   function NM_export_produto_produtodescricao()
   {
         $this->produto_produtodescricao = html_entity_decode($this->produto_produtodescricao, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->produto_produtodescricao = strip_tags($this->produto_produtodescricao);
         if (!NM_is_utf8($this->produto_produtodescricao))
         {
             $this->produto_produtodescricao = mb_convert_encoding($this->produto_produtodescricao, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->produto_produtodescricao = str_replace('<', '&lt;', $this->produto_produtodescricao);
         $this->produto_produtodescricao = str_replace('>', '&gt;', $this->produto_produtodescricao);
         $this->texto_tag .= "<td>" . $this->produto_produtodescricao . "</td>\r\n";
   }
   //----- produtogrupo_produtogrupodescricao
   function NM_export_produtogrupo_produtogrupodescricao()
   {
         $this->produtogrupo_produtogrupodescricao = html_entity_decode($this->produtogrupo_produtogrupodescricao, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->produtogrupo_produtogrupodescricao = strip_tags($this->produtogrupo_produtogrupodescricao);
         if (!NM_is_utf8($this->produtogrupo_produtogrupodescricao))
         {
             $this->produtogrupo_produtogrupodescricao = mb_convert_encoding($this->produtogrupo_produtogrupodescricao, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->produtogrupo_produtogrupodescricao = str_replace('<', '&lt;', $this->produtogrupo_produtogrupodescricao);
         $this->produtogrupo_produtogrupodescricao = str_replace('>', '&gt;', $this->produtogrupo_produtogrupodescricao);
         $this->texto_tag .= "<td>" . $this->produtogrupo_produtogrupodescricao . "</td>\r\n";
   }
   //----- produto_produtoempresacodigo
   function NM_export_produto_produtoempresacodigo()
   {
         $this->produto_produtoempresacodigo = html_entity_decode($this->produto_produtoempresacodigo, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->produto_produtoempresacodigo = strip_tags($this->produto_produtoempresacodigo);
         if (!NM_is_utf8($this->produto_produtoempresacodigo))
         {
             $this->produto_produtoempresacodigo = mb_convert_encoding($this->produto_produtoempresacodigo, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->produto_produtoempresacodigo = str_replace('<', '&lt;', $this->produto_produtoempresacodigo);
         $this->produto_produtoempresacodigo = str_replace('>', '&gt;', $this->produto_produtoempresacodigo);
         $this->texto_tag .= "<td>" . $this->produto_produtoempresacodigo . "</td>\r\n";
   }
   //----- produto_produtofilialcodigo
   function NM_export_produto_produtofilialcodigo()
   {
         $this->produto_produtofilialcodigo = html_entity_decode($this->produto_produtofilialcodigo, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->produto_produtofilialcodigo = strip_tags($this->produto_produtofilialcodigo);
         if (!NM_is_utf8($this->produto_produtofilialcodigo))
         {
             $this->produto_produtofilialcodigo = mb_convert_encoding($this->produto_produtofilialcodigo, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->produto_produtofilialcodigo = str_replace('<', '&lt;', $this->produto_produtofilialcodigo);
         $this->produto_produtofilialcodigo = str_replace('>', '&gt;', $this->produto_produtofilialcodigo);
         $this->texto_tag .= "<td>" . $this->produto_produtofilialcodigo . "</td>\r\n";
   }
   //----- produto_produtounidadecodigo
   function NM_export_produto_produtounidadecodigo()
   {
         $this->produto_produtounidadecodigo = html_entity_decode($this->produto_produtounidadecodigo, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->produto_produtounidadecodigo = strip_tags($this->produto_produtounidadecodigo);
         if (!NM_is_utf8($this->produto_produtounidadecodigo))
         {
             $this->produto_produtounidadecodigo = mb_convert_encoding($this->produto_produtounidadecodigo, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->produto_produtounidadecodigo = str_replace('<', '&lt;', $this->produto_produtounidadecodigo);
         $this->produto_produtounidadecodigo = str_replace('>', '&gt;', $this->produto_produtounidadecodigo);
         $this->texto_tag .= "<td>" . $this->produto_produtounidadecodigo . "</td>\r\n";
   }
   //----- produto_produtocodigoreduzido
   function NM_export_produto_produtocodigoreduzido()
   {
         $this->produto_produtocodigoreduzido = html_entity_decode($this->produto_produtocodigoreduzido, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->produto_produtocodigoreduzido = strip_tags($this->produto_produtocodigoreduzido);
         if (!NM_is_utf8($this->produto_produtocodigoreduzido))
         {
             $this->produto_produtocodigoreduzido = mb_convert_encoding($this->produto_produtocodigoreduzido, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->produto_produtocodigoreduzido = str_replace('<', '&lt;', $this->produto_produtocodigoreduzido);
         $this->produto_produtocodigoreduzido = str_replace('>', '&gt;', $this->produto_produtocodigoreduzido);
         $this->texto_tag .= "<td>" . $this->produto_produtocodigoreduzido . "</td>\r\n";
   }

   //----- 
   function grava_arquivo_rtf()
   {
      global $nm_lang, $doc_wrap;
      $rtf_f = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->arquivo, "w");
      require_once($this->Ini->path_third      . "/rtf_new/document_generator/cl_xml2driver.php"); 
      $text_ok  =  "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n"; 
      $text_ok .=  "<DOC config_file=\"" . $this->Ini->path_third . "/rtf_new/doc_config.inc\" >\r\n"; 
      $text_ok .=  $this->texto_tag; 
      $text_ok .=  "</DOC>\r\n"; 
      $xml = new nDOCGEN($text_ok,"RTF"); 
      fwrite($rtf_f, $xml->get_result_file());
      fclose($rtf_f);
   }

   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
   //---- 
   function monta_html()
   {
      global $nm_url_saida, $nm_lang;
      include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['rtf_file']);
      if (is_file($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->arquivo))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['grid_produto']['rtf_file'] = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->arquivo;
      }
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE><?php echo $this->Ini->Nm_lang['lang_othr_grid_titl'] ?> - <?php echo $this->Ini->Nm_lang['lang_tbl_public_produto'] ?> :: RTF</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
  <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
  <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
  <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
  <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
  <META http-equiv="Pragma" content="no-cache"/>
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_css ?>" /> 
</HEAD>
<BODY class="scExportPage">
<?php echo $this->Ini->Ajax_result_set ?>
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: middle">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">RTF</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
     <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "");
 ?>
    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo $this->Ini->path_imag_temp . "/" . $this->arquivo ?>" target="_blank" style="display: none"> 
</form>
<form name="Fdown" method="get" action="grid_produto_download.php" target="_blank" style="display: none"> 
<input type="hidden" name="nm_tit_doc" value="<?php echo NM_encode_input($this->tit_doc); ?>"> 
<input type="hidden" name="nm_name_doc" value="<?php echo NM_encode_input($this->Ini->path_imag_temp . "/" . $this->arquivo) ?>"> 
</form>
<FORM name="F0" method=post action="./"> 
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="volta_grid"> 
</FORM> 
</BODY>
</HTML>
<?php
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
