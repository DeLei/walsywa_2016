<?php

// caso o e-mail for enviado a função retorna TRUE
// para checar o sucesso verificar o retorno como === TRUE
function enviar_email($para, $assunto, $conteudo)
{
	require_once('swift/swift_required.php');
	
	$ci =& get_instance();
	
	$mailer = Swift_Mailer::newInstance(Swift_SmtpTransport::newInstance($ci->config->item('servidor_smtp'), $ci->config->item('porta_smtp'), $ci->config->item('ssl_smtp') ? 'ssl' : NULL)->setUsername($ci->config->item('usuario_smtp'))->setPassword($ci->config->item('senha_smtp')));
	
	// se $para nao for um array, erros inesperados podem acontecer
	if (!is_array($para))
	{
		$para = array($para);
	}
	
	$mensagem = Swift_Message::newInstance($assunto)->setFrom($ci->config->item('remetente_smtp'))->setTo($para)->setBody($conteudo);
	
	$mensagem->setContentType('text/html');
	
	if (!$mailer->send($mensagem, $erros))
	{
		return $erros;
	}
	
	return TRUE;
}
