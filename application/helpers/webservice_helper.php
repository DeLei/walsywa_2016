<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

ini_set('display_errors', '1');
include "lib/nusoap.php";


/**
* Função:		obter_tes()
* 
* Descrição:	Retorna a TES de um produto.
* 
* 
* @param String $cliente 		= codigo do cliente.
* @param String $cliente_loja 	= numero da loja do cliente.
* @param String $produto 		= codigo do produto.
*/
function obter_tes($cliente, $cliente_loja, $produto)
{
	$ci =& get_instance();
	$ci->config->load('config');
	$webservice = $ci->config->item('webservice');
	
	if ($webservice['TES'] != '') 
	{
		$client 		=	'';
		$result 		=	'';	 
		$filial  		=	'';
		$anat 			=	array(' ');
		$avalnat 		=	array(' ');
	 
		$client = new soapclient($webservice['TES']);
					
		$apar=array('parameters' =>array( 
											'CCLIENTE'  => (string)$cliente, //codigo cliente
											'CLOJA' =>(string)$cliente_loja, // loja do cliente
											'CPRODUTO' =>(string)$produto // codigo do produto
		));
								  
		$result = $client->__soapCall('RETORNATES',$apar);
	   
		return $result->RETORNATESRESULT;
   } 
   else 
   {
		return '0';
   }
               
}

/**
 * Função:		obter_st()
 * 
 * Descrição:	Retorna a ST de um produto.
 * 
 * @param 	String 	- $itens 		- Itens do pedido que devem ser calculados.
 * @return 	Array 	- $arrImpostos 	- Array com os calculos do webservices
 */
function obter_st($itens)
{
	//Setar instancia do codeigniter
	$ci 		=& get_instance();
	
	//Carregar o config do codeigniter
	$ci->config->load('config');
	
	//Buscar a configuração com a URL do webservice
	$webservice = $ci->config->item('webservice');
	
	//Setar as variaveis da função
	$soapCliente 	='';
	$resultado 		='';
	$parametros 	= array();
	$arrImpostos 	= array();
 		
	//Verifixa se existe uma URL cadastrada
	if ($webservice['IMPOSTOS'] != '') 
	{	

		// Instanciar o cliente soap
		$soapCliente = new soapclient($webservice['IMPOSTOS']);
					
		// Montar array com os parametros para o soap cliente
		$parametros=array('parameters' => array('APRODUTO' => array('ITENS' => $itens)));
		
		//Realizar a chamada da função RETORNAST
		$resultado = $soapCliente->__soapCall('RETORNAST',$parametros);
	   
	
		//Se for array entrar no foreach
		if(is_array($resultado->RETORNASTRESULT->STRRETORNOR))
		{
			//Percorrer o array de impostos retornados
			foreach($resultado->RETORNASTRESULT->STRRETORNOR as $imposto)
			{
				$arrImpostos[] = array('ICMS' 		=> $imposto->VALORICM, 
										'IPI' 		=> $imposto->VALORIPI, 
										'ST' 		=> $imposto->VALORST, 
										'TES' 		=> $imposto->TES,
										'CF' 		=> $imposto->CF,
										'CFOP' 		=> $imposto->CFOP, 
										'PRODUTO' 	=> $imposto->PRODUTO
				);
			}
				
		}
		else
		{
			$arrImpostos[] = array('ICMS' 		=> $resultado->RETORNASTRESULT->STRRETORNOR->VALORICM, 
									'IPI' 		=> $resultado->RETORNASTRESULT->STRRETORNOR->VALORIPI, 
									'ST' 		=> $resultado->RETORNASTRESULT->STRRETORNOR->VALORST, 
									'TES' 		=> $resultado->RETORNASTRESULT->STRRETORNOR->TES,
									'CF' 		=> $resultado->RETORNASTRESULT->STRRETORNOR->CF,
									'CFOP' 		=> $resultado->RETORNASTRESULT->STRRETORNOR->CFOP, 
									'PRODUTO' 	=> $resultado->RETORNASTRESULT->STRRETORNOR->PRODUTO
				);
		}
		
		//Retornar array com os impostos
		return $arrImpostos;
   } 
   else //Se não existir uma URL na configuração retornar zerado
   {
		return array('ICMS' => '0', 'IPI' => '0', 'ST' => '0', 'TES' => '0', 'CF' => '0', 'CFOP' => '0', 'PRODUTO' => '0');
   }
}