﻿<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function dompdf($html)
{
	require_once("dompdf/dompdf_config.inc.php");
	
	$dompdf = new DOMPDF();
	$html = mb_convert_encoding($html, "HTML-ENTITIES", "UTF-8");
	$dompdf->load_html($html);
	$dompdf->render();
	$pdf = $dompdf->output();
	$nome_pdf = "pdf/pedido_" . date('d_m_Y') . "_" . time() . ".pdf";
	write_file($nome_pdf , $pdf);

	return $nome_pdf;
}