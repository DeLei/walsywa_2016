<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function currency($number, $symbol = TRUE)
{
   if($symbol)
   {
      return money_format('%.2n', $number);
   }
   else
   {
      return money_format('%!.2n', $number);
   }
}

// ** VALIDAR ** //

function hnordt_validar_data($data)
{
	//if (!preg_replace('^[0-9]{2}/[0-9]{2}/[0-9]{4}$', $data) && !ereg('^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$', $data))
	if (!preg_match('/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/', $data) && 
		!preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $data) )
	{
		return FALSE;
	}
	
	return TRUE;
}

function hnordt_validar_uma_data($descricao_campo, $nome_campo = NULL, $preenchimento_obrigatorio = FALSE)
{
	$ci =& get_instance();
	
	$data_inicial = $ci->input->post('data_' . ($nome_campo ? $nome_campo . '_' : NULL) . 'inicial');
	
	$erro = FALSE;
	
	// validar preenchimento obrigatório
	if ($preenchimento_obrigatorio)
	{
		if (!$data_inicial)
		{
			$erro = TRUE;
		}
	}
	
	// validar datas
	if (!$erro)
	{
		if ($data_inicial && !hnordt_validar_data($data_inicial))
		{
			$erro = TRUE;
		}
	}
	
	return $erro ? 'Digite um intervalo de datas válido para o campo "' . $descricao_campo . '".' : NULL;
}

function hnordt_validar_intervalo_datas($descricao_campo, $nome_campo = NULL, $preenchimento_obrigatorio = FALSE)
{
	$ci =& get_instance();
	
	$data_inicial = $ci->input->post('data_' . ($nome_campo ? $nome_campo . '_' : NULL) . 'inicial');
	$data_final = $ci->input->post('data_' . ($nome_campo ? $nome_campo . '_' : NULL) . 'final');
	
	$erro = FALSE;
	
	// validar preenchimento obrigatório
	if ($preenchimento_obrigatorio)
	{
		if (!$data_inicial || !$data_final)
		{
			$erro = TRUE;
		}
	}
	
	// validar datas
	if (!$erro)
	{
		if ($data_inicial && !hnordt_validar_data($data_inicial))
		{
			$erro = TRUE;
		}
		else if ($data_final && !hnordt_validar_data($data_final))
		{
			$erro = TRUE;
		}
	}
	
	// validar intervalo
	if (!$erro)
	{
		if ($data_inicial && !$data_final)
		{
			$erro = TRUE;
		}
		else if ($data_final && !$data_inicial)
		{
			$erro = TRUE;
		}
		else if (hnordt_converter_data_timestamp($data_inicial) > hnordt_converter_data_timestamp($data_final, 'final'))
		{
			$erro = TRUE;
		}
	}
	
	return $erro ? 'Digite um intervalo de datas válido para o campo "' . $descricao_campo . '".' : NULL;
}

// ** GERAR ** //

function hnordt_gerar_campos_upload()
{
	return '
		<div style="clear: both;"></div>
		
		<div style="background-color: #f9f9f9; border: 1px solid #ddd; margin-top: 10px; padding: 0 10px 10px 10px;">
			<p><strong>Adicionar Arquivos</strong></p>
			
			<div style="clear: both;"></div>
			
			<p style="float: left; margin-right: 10px;">Arquivo <span class="hnordt_numero_campo_upload"></span>: <a href="#" class="hnordt_botao_remover_campo_upload" style="display: none;">(remover)</a><br /><input name="hnordt_arquivo_1" type="file" class="hnordt_campo_upload" /></p>
			
			<div style="clear: both;"></div>
			
			<p><a href="#" class="hnordt_novo_campo_upload">Novo Arquivo</a></p>
			
			<p><em>* Tamanho máximo por arquivo: 2 MB</em></p>
			
			<input type="hidden" name="hnordt_quantidade_campos_upload" value="1" />
		</div>
		
		<script>
			$(document).ready(function() {
				gerar_numeros_campos_upload();
				
				$(".hnordt_novo_campo_upload").click(function() {
					var campo = $(this).parent().prev().prev().clone(true);
					
					$(this).parent().prev().before(campo);
					
					gerar_numeros_campos_upload();
					gerar_botoes_remover_campos_upload();
					
					return false;
				});
				
				$(".hnordt_botao_remover_campo_upload").live("click", function() {
					$(this).parents("p").remove();
					
					gerar_numeros_campos_upload();
					gerar_botoes_remover_campos_upload();
					
					return false;
				});
			});
			
			function gerar_numeros_campos_upload()
			{
				var i = 1;
				
				$(".hnordt_numero_campo_upload").each(function() {
					$(this).text(i);
					i++;
				});
				
				var i = 1;
				
				$(".hnordt_campo_upload").each(function() {
					$(this).attr("name", "hnordt_arquivo_" + i);
					i++;
				});
				
				// usado para gerar o upload dinâmico
				$("input[name=hnordt_quantidade_campos_upload]").val(i - 1);
			}
			
			function gerar_botoes_remover_campos_upload()
			{
				var i = 1;
				
				$(".hnordt_botao_remover_campo_upload").each(function() {
					if (i > 1)
					{
						$(this).show();
					}
					else
					{
						$(this).hide();
					}
					
					i++;
				});
			}
		</script>
	';
}

function hnordt_gerar_upload()
{
	$ci =& get_instance();
	
	$ci->load->library('upload');
	
	for ($i = $ci->input->post('hnordt_quantidade_campos_upload'); $i > 0; $i--)
	{
		// se o usuário tentou dar upload no arquivo...
		if (!$_FILES['hnordt_arquivo_' . $i]['name'])
		{
			continue;
		}
		
		$fn = preg_replace('[^A-Za-z0-9_-]', '', $_FILES['hnordt_arquivo_' . $i]['name']);
		
		$ci->upload->initialize(array('upload_path' => './uploads', 'allowed_types' => '*', 'max_size' => 2048, 'file_name' => $fn));
		
		if (!$ci->upload->do_upload('hnordt_arquivo_' . $i))
		{
			$erro = $ci->upload->display_errors();
		}	
		else
		{
			$dados_upload[] = $ci->upload->data();
		}
		
		if ($erro)
		{
			break;
		}
	}
	
	return $erro ? strip_tags($erro) : $dados_upload;
}

function hnordt_gerar_1_data($descricao_campo, $nome_campo = NULL, $float = TRUE)
{
	$ci =& get_instance();
	
	return ('<p style="' . ($float ? 'float: left; margin-right: 10px;' : NULL) . '">'
				. $descricao_campo
				. ':<br />
				<input name="data_'
					. ($nome_campo ? $nome_campo . '_' : NULL)
					. 'inicial" type="text" value="'
					. $ci->input->post('data_' . ($nome_campo ? $nome_campo . '_' : NULL) . 'inicial')
					. '" size="6" class="datepicker data_inicial"
				/>
			</p>');
}

function hnordt_gerar_intervalo_datas($descricao_campo, $nome_campo = NULL, $float = TRUE)
{
	$ci =& get_instance();
	
	return '<p style="' . ($float ? 'float: left; margin-right: 10px;' : NULL) . '">' . $descricao_campo . ':<br /><input name="data_' . ($nome_campo ? $nome_campo . '_' : NULL) . 'inicial" type="text" value="' . $ci->input->post('data_' . ($nome_campo ? $nome_campo . '_' : NULL) . 'inicial') . '" size="6" class="datepicker data_inicial" /> a <input name="data_' . ($nome_campo ? $nome_campo . '_' : NULL) . 'final" type="text" value="' . $ci->input->post('data_' . ($nome_campo ? $nome_campo . '_' : NULL) . 'final') . '" size="6" class="datepicker data_final" /></p>';
}

function hnordt_gerar_feiras_ativas()
{
	$ci =& get_instance();
	
	$feiras = $ci->feira->obter_ativas(TRUE, TRUE);
	
	if ($feiras)
	{
		return '<p style="float: left; margin-right: 10px;">Evento:<br />' . form_dropdown('id_feira', $feiras, $ci->input->post('id_feira')) . '</p>';
	}
	else
	{
		return NULL;
	}
}

// ** CONVERTER ** //

function hnordt_converter_data_timestamp($data, $tipo_data = 'inicial')
{
	$data = explode('/', $data);
	
	return ($tipo_data == 'inicial') ? mktime(0, 0, 0, $data[1], $data[0], $data[2]) : mktime(23, 59, 59, $data[1], $data[0], $data[2]);
}


function protheus2data($data){
	$dia = substr($data, -2);
	$mes = substr($data, 4, 2);
	$ano = substr($data, 0, 4);
	
	return $dia . '/' . $mes . '/' . $ano;
}

function data2protheus($data)
{
 
 $data_array = explode('/', $data);
 
 $ano = $data_array[2];
 $mes = $data_array[1];
 $dia = $data_array[0];

 return $ano . $mes . $dia;
}

/**
 * Função: 		eAjax()
 * 
 * Descrição:	Função verifica se é uma requisição AJAX se for então retornar TRUE
 * 
 * @return		string
 */
function eAjax()
{
	return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
}
