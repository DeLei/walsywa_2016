<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function corrigir_acentos($array, $chave = 'descricao'){
		foreach($array as $key => $interesse) {
			$array[$key][$chave] = utf8_encode($interesse[$chave]);
		}
		return $array;
	}
	
	function ipUsuario()
	{
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
		{
			$ipaddress = getenv('HTTP_CLIENT_IP');
		}
		else if(getenv('HTTP_X_FORWARDED_FOR'))
		{
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		}
		else if(getenv('HTTP_X_FORWARDED'))
		{
			$ipaddress = getenv('HTTP_X_FORWARDED');
		}
		else if(getenv('HTTP_FORWARDED_FOR'))
		{
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		}
		else if(getenv('HTTP_FORWARDED'))
		{
		   $ipaddress = getenv('HTTP_FORWARDED');
	    }
		else if(getenv('REMOTE_ADDR'))
		{
			$ipaddress = getenv('REMOTE_ADDR');
		}
		else
		{
			$ipaddress = 'UNKNOWN';
		}
		return $ipaddress;
	}
	
	function objectToArray($d) {
		if (is_object($d)) {
			// Gets the properties of the given object
			// with get_object_vars function
			$d = get_object_vars($d);
		}
		
		if (is_array($d)) {
			/*
			* Return array converted to object
			* Using __FUNCTION__ (Magic constant)
			* for recursive call
			*/
			return array_map(__FUNCTION__, $d);
		}else {
			// Return array
			return $d;
		}
	}

/**
 * Metódo:			debug_pre
 * 
 * Descrição:		Função Utilizada para debugar
 * 
 * Data:			31/01/2013
 * Modificação:		31/01/2013
 * 
 * @access			public
 * @param			object 		$array						- Array que deve ser exibido
 * @param			boolean		$parar						- $parar é para executar ou não o DIE();
 * @version			1.0
 * @author 			DevelopWeb Soluções Web
 */
function debug_pre($array = NULL, $parar = TRUE)
{
	//Exibir pre
	echo '<pre>';
	
	//Exibir array
	print_r($array);
	
	//Exibir pre
	echo '</pre>';
	
	//Verificar se é para parar a execução.
	if($parar){
		die();
	}
}

function debug_pre2()
{
    $arrays = func_get_args();
    $parar = array_slice(func_get_args(), 1);
	
	//Exibir pre
	echo '<pre>';
	
	//Exibir array
	foreach( $arrays as $array){
		print_r($array);
	}
	//Exibir pre
	echo '</pre>';
	
	//Verificar se é para parar a execução.
	//if($parar){
		die();
	//}
}

/**
 * MAGIA NEGRA! O.o
 */
function recursive_array_search($needle,$haystack) 
{
    foreach($haystack as $key=>$value) 
    {
        $current_key=$key;
        if($needle===$value OR (is_array($value) && recursive_array_search($needle,$value) !== false)) 
        {
            return $current_key;
        }
    }
    return false;
}



/**
 * Metódo:			remover_links
 *
 * Descrição:		Função Utilizada para remover Link HTML
 *
 * Data:		22/05/2013
 * Modificação:		22/05/2013
 *
 * @access			public
 * @param			string 		$text						- Conteúdo para remoção dos links
 * @param			boolean		$manterConteudo					- $manter Manter HTML da TAG ou REMOVER TAG e Seu conteúdo;
 * @version			1.0
 * @author 			DevelopWeb Soluções Web

 */
function remover_links($text, $manterConteudo = TRUE){
	if($manterConteudo){
		/* Exemplo: <a href="teste.html">Teste</a>
		 *  Output: Teste
		*/
		return  preg_replace('#<a.*?>(.*?)</a>#i', '\1', $text);
	}else{
		/* Exemplo: <a href="teste.html">Teste</a>
		 *  Output:
		*/
		return preg_replace('#<a.*?>.*?</a>#i', '', $text);
	}

}



/*Validação de Data*/



function validaData($date){
	$data = explode("/",$date); 
	$d = $data[0];
	$m = $data[1];
	$y = $data[2];

	return checkdate($m,$d,$y);	
}



function obter_status($status, $tipo, $retorna_imagem = FALSE){
	
	$descricao = '';
	$classeCSS = '';
	$imagem = '';	
	if($tipo == 'O' && $status == 'A')
	{
		$imagem 	=  img(base_url() . 'misc/imagens/pedidos_aguardando/aguardando_analise.png'); 
		$classeCSS 	= 'aguardando';	
		$descricao 	= 'Aguardando';
	
	}elseif($tipo == 'O' && $status == 'L'){
		$imagem 	=  img(base_url() . 'misc/imagens/pedidos_aguardando/aguardando_analise.png'); 
		$classeCSS 	= 'aguardando';	
		$descricao 	= 'L - Aguardando';
	}elseif($tipo == 'O' && $status == 'C'){
		$imagem 	=  img(base_url() . 'misc/imagens/pedidos_aguardando/transformado_pedido.png'); 
		$classeCSS = 'transformado_pedido';
		$descricao = 'Transformado em Pedido';	
	}elseif($tipo == 'P' && $status == 'A'){
		$imagem 	=  img(base_url() . 'misc/imagens/pedidos_aguardando/aguardando_analise.png');
		$classeCSS = 'aguardando';
		$descricao = 'Aguardando Análise Comercial';
		
	}elseif($tipo == 'P' && $status == 'R'){
		$imagem 	=  img(base_url() . 'misc/imagens/pedidos_aguardando/reprovado.png');
		$classeCSS = 'reprovado';
		$descricao = 'Reprovado';
	}
	//CUSTOM E0102/2014
	elseif($tipo == 'P' && $status == 'L'){
		$imagem 	=  img(base_url() . 'misc/imagens/pedidos_aguardando/aguardando_importacao.png');
		$classeCSS = 'aguardando_importacao';
		$descricao = 'Aguardando Importação';
	}
	elseif($tipo == 'P' && $status == 'I'){
		$imagem 	=  img(base_url() . 'misc/imagens/pedidos_aguardando/importado.png');
		$classeCSS = 'importado';
		$descricao = 'Importado';
	}
	//FIM CUSTOM E0102/2014
	if($retorna_imagem){
		return $imagem;
	
	}else{
		return '<span class="status_label '.$classeCSS.'">'.$descricao.'</span>';
	}
	
	

}

function convert_chars_to_entities( $str ){ 
	$str = str_replace( 'u00d8','Ø', $str ); 
	return $str; 
}


/**
 * Metódo:			exibir_texto
 *
 * Descrição:		Função Utilizada para corrigir e converter caracteres especiais para exibição correta.
 *
 * Data:		22/05/2014
 * Modificação:		22/05/2014
 *
 * @access			public
 * @param			string 		$text						- Conteúdo para conversão dos caracteres especiais.
 * @version			1.0
 * @author 			DevelopWeb Soluções Web
 */
function exibir_texto($text){
	$_descricao_produto=iconv('ISO-8859-1', 'UTF-8', $text);
	$descricao_produto = convert_chars_to_entities( $_descricao_produto); 
	return $descricao_produto;
}

/**
 * Metódo:			aplicar_desconto
 *
 * Descrição:		Função Utilizada para aplicar desconto em percentual ao preco.
 *
 * Data:			05/06/2014
 * Modificação:		05/06/2014
 *
 * @access			public
 * @param			float 		$preco_unitario						- Preço que será aplicado desconto.
 * @param			float 		$percentual_desconto				- Percentual de desconto aplicado ao preco.
 * @version			1.0
 * @author 			DevelopWeb Soluções Web
 */
function aplicar_desconto($preco_unitario , $percentual_desconto = 0){
	 $valor_unitario = 0;
	if ($percentual_desconto > 0) {
		$valor_unitario = ($preco_unitario - (($preco_unitario * $percentual_desconto) / 100));
    } else {
		$valor_unitario = $preco_unitario;
	}
	
	return $valor_unitario;
}