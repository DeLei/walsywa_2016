<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['db_cliente'] = array(
	'tabelas' => array(
		'pedidos' => 'SC5010',
		'itens_pedidos' => 'SC6010',
		'representantes_pedidos' => '', // em branco para protheus
		'premiacoes' => 'SE3010',
		'metas' => 'SCT010',
		'formas_pagamento' => 'SE4010',
		'transportadoras' => 'SA4010',
		'tabelas_precos' => 'DA0010',
		'produtos_tabelas_precos' => 'DA1010',
		'grupos_produtos' => 'SBM010',
		'produtos' => 'SB1010',
		'produtos_estoque' => 'SB2010',
		'opcionais_produtos' => '',
		'clientes' => 'SA1010',
		'titulos' => 'SE1010',
		'representantes' => 'SA3010',
		'notas_fiscais' => 'SF2010',
		'itens_notas_fiscais' => 'SD2010',
		'pedidos_dw' => 'SZW010',
		'nome_desconhecido' => 'SF4010'
	),
	
	'campos' => array(
		'pedidos_dw' => array(
			'id_pedido' => 'ZW_IDPED',
			'data_emissao' => 'ZW_EMISSAO',
			'id_usuario' => 'ZW_IDUSER',
			'codigo_representante' => 'ZW_VEND1',
			'tipo' => 'ZW_TIPOPED',
			'status' => 'ZW_STATUS',
			'codigo_cliente' => 'ZW_CLIENTE',
			'loja_cliente' => 'ZW_LOJACLI',
			'nome_cliente' => 'ZW_NOMECLI',
			'id_prospects' => 'ZW_IDPROS',
			'nome_prospects' => 'ZW_NOMEPRO',
			'condicao_pagamento' => 'ZW_CONDPAG',
			'tipo_frete' => 'ZW_TPFRETE',
			'valor_frete' => 'ZW_DESPESA',
			'codigo_transportadora' => 'ZW_TRANSP',
			'ordem_compra' => 'ZW_PEDCLI',
			'data_entrega' => 'ZW_ENTREG',
			'obs_pedido' => 'ZW_OBSPED',
			'motivo_reprovacao' => 'ZW_MOTREP',
			'id_feira' => 'ZW_EVENTO',
			'codigo_produto' => 'ZW_PRODUTO',
			'descricao_produto' => 'ZW_DESCPRO',
			'unidade_medida' => 'ZW_UM',
			'preco_unitario' => 'ZW_PRUNIT',
			'valor_total' => 'ZW_VALOR',
			'ipi' => 'ZW_IPI',
			'desconto' => 'ZW_DESC',
			'tabela_precos' => 'ZW_TABELA',
			'peso_unitario' => 'ZW_PESOUNI',
			'substituicao_tributaria' => 'ZW_ST',
			'quantidade' => 'ZW_QTDVEN',
			'delecao' => 'D_E_L_E_T_',
			'chave' => 'R_E_C_N_O_'
		),
		'pedidos' => array( //SC5010
			'codigo_representante' => 'C5_VEND1',
			'data_emissao' => 'C5_EMISSAO',
			'codigo' => 'C5_NUM',
			'tipo' => 'C5_TIPO',
			'codigo_cliente' => 'C5_CLIENTE',
			'codigo_ordem_compra' => 'C5_PEDCLI',
			'loja_cliente' => 'C5_LOJACLI',
			'codigo_nota_fiscal' => 'C5_NOTA',
			'codigo_forma_pagamento' => 'C5_CONDPAG',
			'codigo_transportadora' => 'C5_TRANSP',
			'tipo_frete' => 'C5_TPFRETE',
			'tabela_precos' => 'C5_TABELA',
			'delecao' => 'D_E_L_E_T_'
		),
		'itens_pedidos' => array( //SC6010
			'codigo_pedido' => 'C6_NUM',
			'valor_total_item' => 'C6_VALOR',
			'valor_total_desconto_item' => 'C6_VALDESC',
			'quantidade_vendida_produto' => 'C6_QTDVEN',
			'quantidade_faturada_produto' => 'C6_QTDENT',
			'preco_produto' => 'C6_PRCVEN',
			'codigo_item' => 'C6_ITEM',
			'codigo_produto' => 'C6_PRODUTO',
			'descricao_produto' => 'C6_DESCRI',
			'unidade_medida_produto' => 'C6_UM',
			'delecao' => 'D_E_L_E_T_'
		),
		'representantes_pedidos' => array( // em branco para protheus
			'codigo_representante' => '',
			'codigo_pedido' => '',
		),
		'premiacoes' => array(
			'codigo_representante' => 'E3_VEND',
			'data_emissao' => 'E3_EMISSAO',
			'valor' => 'E3_COMIS',
			'delecao' => 'D_E_L_E_T_'
		),
		'metas' => array(
			'codigo_representante' => 'CT_VEND',
			'data' => 'CT_DATA',
			'valor' => 'CT_VALOR',
			'delecao' => 'D_E_L_E_T_'
		),
		'formas_pagamento' => array( //SE4010
			'codigo' => 'E4_CODIGO',
			'descricao' => 'E4_DESCRI',
			'delecao' => 'D_E_L_E_T_'
		),
		'transportadoras' => array(
			'codigo' => 'A4_COD',
			'nome' => 'A4_NOME',
			'telefone' => 'A4_TEL',
			'delecao' => 'D_E_L_E_T_'
		),
		'tabelas_precos' => array(
			'codigo' => 'DA0_CODTAB',
			'descricao' => 'DA0_DESCRI',
			'delecao' => 'D_E_L_E_T_'
		),
		'produtos_tabelas_precos' => array(
			'codigo_tabela_precos' => 'DA1_CODTAB',
			'codigo_produto' => 'DA1_CODPRO',
			'preco' => 'DA1_PRCVEN',
			'st' => 'DA1_PERST',
			'delecao' => 'D_E_L_E_T_'
		),
		'produtos' => array(
			'codigo' => 'B1_COD',
			'codigo_real' => 'B1_COD',
			'codigo_grupo' => 'B1_GRUPO',
			'codigo_classe' => '',
			'descricao' => 'B1_DESC',
			'unidade_medida' => 'B1_UM',
			'ipi' => 'B1_IPI',
			'delecao' => 'D_E_L_E_T_',
			'inativo' => 'B1_MSBLQL',
			'peso' => 'B1_PESO',
			'disponivel_venda' => '' // B1_DISPVEN
		),
		'produtos_estoque' => array(
			'codigo' => 'B2_COD',
			'quantidade_disponivel' => '', // protheus nao precisa
			'quantidade_atual' => 'B2_QATU',
			'quantidade_pedidos_venda' => 'B2_QPEDVEN',
			'quantidade_empenhada' => 'B2_QEMP',
			'quantidade_reservada' => 'B2_RESERVA',
			'ano' => '', // protheus nao precisa
			'mes' => '', // protheus nao precisa
			'preco' => '', // protheus nao precisa
			'delecao' => 'D_E_L_E_T_'
		),
		'opcionais_produtos' => array(
			'codigo_produto' => '',
			'codigo' => '',
			'descricao' => ''
		),
		'grupos_produtos' => array(
			'codigo' => 'BM_GRUPO',
			'descricao' => 'BM_DESC',
			'delecao' => 'D_E_L_E_T_'
		),
		'clientes' => array( //SA1010
			'codigo' => 'A1_COD',
			'loja' => 'A1_LOJA',
			'nome' => 'A1_NOME',
			'cpf' => 'A1_CGC',
			'pessoa_contato' => 'A1_CONTATO',
			'endereco' => 'A1_END',
			'bairro' => 'A1_BAIRRO',
			'cep' => 'A1_CEP',
			'cidade' => 'A1_MUN',
			'estado' => 'A1_EST',
			'telefone' => 'A1_TEL',
			'email' => 'A1_EMAIL',
			'limite_credito' => 'A1_LC',
			'total_titulos_em_aberto' => 'A1_SALDUP',
			'codigo_representante' => 'A1_VEND',
			'data_cadastro' => 'A1_PRICOM',
			'data_primeira_compra' => 'A1_PRICOM',
			'chave_primaria' => 'R_E_C_N_O_',
			'delecao' => 'D_E_L_E_T_',
			'tipo_pessoa' => 'A1_PESSOA',
			'nome_fantasia' => 'A1_NREDUZ',
			'rg' => 'A1_INSCR',
			'numero' => '',
			'site' => '',
			'observacao' => 'A1_OBS',
			'tabela_frete' => 'A1_XTABFRE'
		),
		'titulos' => array(
			'valor' => 'E1_VALOR',
			'codigo_cliente' => 'E1_CLIENTE',
			'loja_cliente' => 'E1_LOJA',
			'data_vencimento' => 'E1_VENCTO',
			'data_baixa' => 'E1_BAIXA',
			'codigo' => 'E1_NUM',
			'parcela' => 'E1_PARCELA',
			'delecao' => 'D_E_L_E_T_'
		),
		'representantes' => array(
			'codigo' => 'A3_COD',
			'nome' => 'A3_NOME',
			'cpf' => 'A3_CGC',
			'endereco' => 'A3_END',
			'bairro' => 'A3_BAIRRO',
			'cep' => 'A3_CEP',
			'cidade' => 'A3_MUN',
			'estado' => 'A3_EST',
			'telefone' => 'A3_TEL',
			'email' => 'A3_EMAIL',
			'grupo' => 'A3_GRPREP',
			'tabela' => 'A3_TABELA',
			'delecao' => 'D_E_L_E_T_'
		),
		'notas_fiscais' => array( //SF2010
			'codigo' => 'F2_DOC',
			'data_emissao' => 'F2_EMISSAO',
			'valor_total' => 'F2_VALFAT',
			'valor_icms' => 'F2_VALICM',
			'valor_ipi' => 'F2_VALIPI',
			'codigo_cliente' => 'F2_CLIENTE',
			'loja_cliente' => 'F2_LOJA',
			'codigo_transportadora' => 'F2_TRANSP',
			'delecao' => 'D_E_L_E_T_',
			'codigo_representante' => 'F2_VEND1'
		),
		'itens_notas_fiscais' => array( //SD2010
			'codigo_nota_fiscal' => 'D2_DOC',
			'codigo_pedido' => 'D2_PEDIDO',
			'codigo_produto' => 'D2_COD',
			'delecao' => 'D_E_L_E_T_'
		)
	)
);