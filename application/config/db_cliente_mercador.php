<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['db_cliente'] = array(
	'tabelas' => array(
		'pedidos_compra' => 'PedCompras',
		'itens_pedidos_compra' => 'ItensPedCompras',
		
		'pedidos' => 'OrcPedVendas',
		'itens_pedidos' => 'ItensOrcPed',
		'representantes_pedidos' => 'VdrOrcPed', // somente para o mercador
		'premiacoes' => '',
		'metas' => '',
		'formas_pagamento' => 'Prazos',
		'transportadoras' => 'Fornecedores',
		'tabelas_precos' => '',
		'produtos_tabelas_precos' => '',
		'grupos_produtos' => 'GruposMerc',
		'produtos' => 'mercadorias',
		'produtos_estoque' => 'MovMercadorias',
		'opcionais_produtos' => '',
		'clientes' => 'Clientes',
		'titulos' => 'CReceber',
		'representantes' => 'Pessoal',
		'notas_fiscais' => 'NFs',
		'itens_notas_fiscais' => 'ItensNFs'
	),
	
	'campos' => array(
		'pedidos_compra' => array(
			'codigo' => 'PedCmp_SeqPed',
			'data_entrega' => 'PedCmp_DtaEtg',
		),
		'itens_pedidos_compra' => array(
			'codigo_pedido' => 'PedCmp_SeqPed',
			'codigo_produto' => 'Mcd_SeqMcd',
			'quantidade' => 'ItmPedCmp_QtdItm',
		),
		
		'pedidos' => array(
			'codigo_representante' => '', // para mercador usar ''
			'data_emissao' => 'OrcPed_DtaEms',
			'codigo' => 'OrcPed_CodSeq',
			'tipo' => '', // para mercador usar ''
			'codigo_cliente' => 'Cli_CodCli',
			'loja_cliente' => 'Cli_CodCli',
			'codigo_nota_fiscal' => '', // TODO: buscar NF na tabela de NFs
			'codigo_forma_pagamento' => 'OrcPed_PrzPgt',
			'codigo_transportadora' => 'OrcPed_CodTpt',
			'tipo_frete' => 'OrcPed_FreCifFob',
			'delecao' => ''
		),
		'itens_pedidos' => array(
			'codigo_pedido' => 'OrcPed_CodSeq',
			'valor_total_item' => 'ItmOrcPed_PrcUntFin * ItmOrcPed_QtdItm',
			'valor_total_desconto_item' => 'ItmOrcPed_PrcVda - ItmOrcPed_PrcUntFin',
			'quantidade_vendida_produto' => 'ItmOrcPed_QtdItm',
			'quantidade_faturada_produto' => 'ItmOrcPed_QtdLbr',
			'preco_produto' => 'ItmOrcPed_PrcUntFin',
			'codigo_item' => 'ItmOrcPed_SeqItm',
			'codigo_produto' => 'Mcd_SeqMcd',
			'descricao_produto' => 'ItmOrcPed_DcrMcd',
			'unidade_medida_produto' => '', // no mercador a unidade de medida est� somente no cadastro de produtos
			'delecao' => ''
		),
		'representantes_pedidos' => array(
			'codigo_representante' => 'Vdr_CodVdr',
			'codigo_pedido' => 'OrcPed_CodSeq',
		),
		'premiacoes' => array(
			'codigo_representante' => '',
			'data_emissao' => '',
			'valor' => '',
			'delecao' => ''
		),
		'metas' => array(
			'codigo_representante' => '',
			'data' => '',
			'valor' => '',
			'delecao' => ''
		),
		'formas_pagamento' => array(
			'codigo' => 'Prz_CodPrz',
			'descricao' => 'Prz_NmePrz',
			'delecao' => ''
		),
		'transportadoras' => array(
			'codigo' => 'Fnc_CodFnc',
			'nome' => 'Fnc_NmeFnc',
			'telefone' => 'Fnc_FneFnc',
			'tipo' => 'Fnc_TipFnc',
			'delecao' => ''
		),
		'tabelas_precos' => array( // TODO
			'codigo' => '',
			'descricao' => '',
			'delecao' => ''
		),
		'produtos_tabelas_precos' => array( // TODO
			'codigo_tabela_precos' => '',
			'codigo_produto' => '',
			'preco' => '',
			'delecao' => ''
		),
		'produtos' => array(
			'codigo' => 'Mcd_SeqMcd',
			'codigo_real' => 'Mcd_CodMcd',
			'codigo_grupo' => 'GruMcd_CodGru',
			'codigo_classe' => 'ClaMcd_CodCla',
			'descricao' => 'Mcd_NmeMcd',
			'unidade_medida' => 'UndMed_SigUnd',
			'ipi' => 'Mcd_AlqIpi',
			'delecao' => '',
			'inativo' => '', // somente para protheus,
			'disponivel_venda' => '' // somente para protheus,
		),
		'produtos_estoque' => array(
			'codigo' => 'Mcd_SeqMcd',
			'quantidade_disponivel' => 'SUM(((MvtMcd_EtqAnt + MvtMcd_CmpMcd + MvtMcd_DevCli + MvtMcd_EntTfr + MvtMcd_OutEnt + MvtMcd_OutEntGrd) - (MvtMcd_VdaMcd + MvtMcd_DevFnc + MvtMcd_SaiTfr + MvtMcd_OutSai + MvtMcd_OutSaiGrd) - MvtMcd_PdtVda))',
			'quantidade_atual' => '',
			'quantidade_pedidos_venda' => '',
			'quantidade_empenhada' => '',
			'quantidade_reservada' => '',
			'ano' => 'MvtMcd_AnoMvt',
			'mes' => 'MvtMcd_MesMvt',
			'preco' => 'MvtMcd_PrcVdaAtu',
			'delecao' => ''
		),
		'opcionais_produtos' => array(
			'codigo_produto' => '',
			'codigo' => '',
			'descricao' => ''
		),
		'grupos_produtos' => array(
			'codigo' => 'GruMcd_CodGru',
			'codigo_classe' => 'ClaMcd_CodCla',
			'descricao' => 'GruMcd_NmeGru',
			'delecao' => ''
		),
		'clientes' => array(
			'codigo' => 'Cli_CodCli',
			'loja' => 'Cli_CodCli',
			'nome' => 'Cli_NmeCli',
			'cpf' => 'Cli_CgcCli',
			'pessoa_contato' => '', // TODO: encontrar no mercador
			'endereco' => 'Cli_EndCli',
			'bairro' => 'Cli_BaiCli',
			'cep' => 'Cep_CodCep',
			'cidade' => 'Cli_CidCli',
			'estado' => 'Cli_EstCli',
			'telefone' => 'Cli_FneCli',
			'email' => 'Cli_EmaCli',
			'limite_credito' => 'Cli_LmtCre',
			'total_titulos_em_aberto' => '',
			'codigo_representante' => 'Cli_VdrCli',
			'data_cadastro' => 'Cli_DtaCad',
			'data_primeira_compra' => 'Cli_DtaCad', // TODO: encontrar no mercador
			'chave_primaria' => '', // TODO: 29/09/2010
			'delecao' => '',
			'tipo_pessoa' => '',
			'nome_fantasia' => '',
			'rg' => '',
			'numero' => '',
			'site' => '',
			'observacao' => ''
		),
		'titulos' => array( // TODO: mercador
			'valor' => 'Crb_VlrDoc',
			'codigo_cliente' => 'Cli_CodCli',
			'loja_cliente' => 'Cli_CodCli',
			'data_vencimento' => 'Crb_DtaEms', // TODO: encontrar no mercador
			'data_baixa' => 'Crb_DtaEft', // TODO: encontrar no mercador
			'codigo' => 'Crb_NumDoc',
			'parcela' => 'Crb_NumPcl',
			'delecao' => ''
		),
		'representantes' => array(
			'codigo' => 'Pes_CodPes',
			'nome' => 'Pes_NmePes',
			'delecao' => ''
		),
		'notas_fiscais' => array(
			'codigo' => 'NfiMcd_NumNfi',
			'data_emissao' => 'NfiMcd_DtaEms',
			'valor_total' => 'NfiMcd_TotNfi',
			'valor_icms' => 'NfiMcd_VlrICM',
			'valor_ipi' => 'NfiMcd_VlrIPI',
			'codigo_cliente' => 'NfiMcd_RmtDtn',
			'loja_cliente' => 'NfiMcd_RmtDtn',
			'codigo_transportadora' => 'NfiMcd_CodTpt',
			'delecao' => '',
			'codigo_representante' => '' // TODO: no mercador n�o h� o c�digo do repres na NF
		),
		'itens_notas_fiscais' => array(
			'codigo_nota_fiscal' => 'ItmNfi_SeqItm',
			'codigo_pedido' => 'ItmNfi_SeqPed',
			'delecao' => ''
		)
	)
);