<?php

class Autocomplete extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
		
		$this->teste_db_cliente = $this->load->database('db_cliente', TRUE);
		
		$this->config->load('db_cliente_' . $this->teste_db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');
	}
	
	function obter_clientes($palavras_chave, $codigo_do_representante = NULL)
	{
		if ($codigo_do_representante)
		{
			$this->teste_db_cliente->where($this->_db_cliente['campos']['clientes']['codigo_representante'], $codigo_do_representante);
		}
		
		$campos = array('codigo', 'loja', 'nome', 'cpf');
		
		foreach ($campos as $campo)
		{
			$select[] = $this->_db_cliente['campos']['clientes'][$campo] . ' AS ' . $campo;
		}
		
		$this->teste_db_cliente->where('A1_MSBLQL <>', '1')->where($this->_db_cliente['campos']['clientes']['delecao'] . ' != "*" AND (UPPER(' . $this->_db_cliente['campos']['clientes']['nome'] . ') LIKE UPPER("%' . $palavras_chave . '%") OR UPPER(' . $this->_db_cliente['campos']['clientes']['codigo'] . ') LIKE UPPER("%' . $palavras_chave . '%") OR UPPER(' . $this->_db_cliente['campos']['clientes']['cpf'] . ') LIKE UPPER("%' . $palavras_chave . '%"))');
		
		return $this->teste_db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['clientes'])->get()->result_array();
	}
	
	
	function obter_produtos_tabela_preco($palavras_chave, $codigo_grupo_produtos = NULL, $codigo_tabela_precos = NULL, $unidade = NULL)
	{
		if($codigo_tabela_precos == NULL)
		{
			$_codigo_tabela_precos = $this->db_cliente->obter_primeiro_item_tabela_precos();
		
			$codigo_tabela_precos = $_codigo_tabela_precos['codigo'];
		}
		
		$produtos = $this->db_cliente->obter_produtos_tabela_precos($palavras_chave, $codigo_grupo_produtos, $codigo_tabela_precos, $unidade);
		//var_dump($produtos);
		return $produtos;

	}
	
	function obter_prospects($palavras_chave, $codigo_do_representante = NULL)
	{
		if ($codigo_do_representante)
		{
			$this->db->where('codigo_usuario', $codigo_do_representante);
		}
	
		
		$this->db->where('status = "ativo" AND (UPPER(nome) LIKE UPPER("%' . $palavras_chave . '%") OR UPPER(cpf) LIKE UPPER("%' . $palavras_chave . '%"))');
		
		return $this->db->from('prospects')->get()->result_array();
	}
	
	function obter_produtos($chave)
	{
		if($chave != NULL)
		{
			$produtos = $this->db_cliente->obter_produtos_auto($chave);
		}
		
		return $produtos;
		
	}
}