<?php

class Db_formulario_visita extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		
		$this->config->load('db_cliente_' . $this->db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');
	}
	
	function exportar_formulario_visita($formulario_visita) {
		$tabela = $this->_db_cliente['tabelas']['formulario_visitas'];
		$campos = $this->_db_cliente['campos']['formulario_visitas'];
		
		$cli_ddd = substr($formulario_visita['telefone_cliente'], 1,2);
		$cli_fone = substr($formulario_visita['telefone_cliente'], 5);
	
		$dados = array(
			$campos['filial']						=>	"",//$formulario_visita['filial'],
			$campos['codigo_representante']			=>	$formulario_visita['codigo_representante'],
			$campos['data_visitacao']				=>	data2protheus($formulario_visita['data_visitacao']),
			$campos['data_preenchimento']			=>	data2protheus($formulario_visita['data_preenchimento']),
			$campos['setor']						=>	$formulario_visita['setor'],
			$campos['cliente_novo']					=>	$formulario_visita['cliente_novo'],
			$campos['cliente_ddd']					=>	$cli_ddd,
			$campos['cliente_fone']					=>	str_replace('-','',$cli_fone),
			$campos['tipo_cliente']					=>	$formulario_visita['tipo_cliente'],
			$campos['cidade_cliente']				=>	$formulario_visita['cidade_regiao_cliente'],
			$campos['estado_cliente']				=>	$formulario_visita['estado_cliente'],
			$campos['cep_cliente']					=>	str_replace('-','',$formulario_visita['cep_cliente']),
			$campos['gerou_pedido_orcamento']		=>	$formulario_visita['gerou_pedido_orcamento'],
			$campos['nome_cliente']					=>	$formulario_visita['cliente'],
			$campos['cliente']						=>	$formulario_visita['codigo_cliente'] ? $formulario_visita['codigo_cliente'] : '',
			$campos['loja_cliente']                 =>  $formulario_visita['loja_cliente'] ? $formulario_visita['loja_cliente'] : '',
			$campos['classificacao_cliente']		=>	$formulario_visita['classificacao_cliente'],
			$campos['obra']							=>	$formulario_visita['obra'],
			$campos['nome_obra']					=>	$formulario_visita['nome_obra'],
			$campos['escritorio']					=>	$formulario_visita['escritorio'],
			$campos['segmento']						=>	$formulario_visita['segmento'],
			$campos['fase_obra']					=>	$formulario_visita['fase_obra'],
			$campos['aplicacao']					=>	$formulario_visita['aplicacao'],
			$campos['viavel_proximo_passo']			=>	$formulario_visita['viavel_proximo_passo'],
			$campos['proximo_passo']				=>	$formulario_visita['proximo_passo'],
			$campos['data_proximo_passo']			=>	data2protheus($formulario_visita['data_proximo_passo']),
			$campos['hora_proximo_passo']			=>	$formulario_visita['horario_proximo_passo'],
			$campos['resumo_reuniao']				=>	$formulario_visita['resumo_reuniao'],
			$campos['material_pdv']					=>	$formulario_visita['material_pdv'],
			$campos['tipo_visita']					=>	$formulario_visita['tipo_visita']
		);
		if($formulario_visita['codigo_formulario']){
			$visita = $this->db_cliente->update($tabela, $dados, array($campos['codigo_formulario'] => $formulario_visita['codigo_formulario']));
		}else{
			$id = $this->db_cliente->select('count(DISTINCT '.$campos['chave'] . ') as quantidade')->from($tabela)->get()->row_array();
			$id = $id['quantidade'] + 1;
			$dados[$campos['codigo_formulario']] = str_pad($id, 6, '0', STR_PAD_LEFT );
			$dados[$campos['chave']] = $id; 
			$formulario_visita['codigo_formulario'] = str_pad($id, 6, '0', STR_PAD_LEFT );
			$visita = $this->db_cliente->insert($tabela, $dados);
		}
		
		if($visita){
			return $this->exportar_grid_contatos($formulario_visita['codigo_formulario'], $formulario_visita['grids']);
		}else{
			return false;
		}
	
	}
	
	function exportar_grid_contatos($codigo_formulario, $grids){
		$dados = array();
		$tabela = $this->_db_cliente['tabelas']['contatos_formulario_visita'];
		$campos = $this->_db_cliente['campos']['contatos_formulario_visita'];
		
		if(is_array($grids['grid_contatos'])){
			
			$this->db_cliente->delete($tabela, array($campos['codigo_formulario'] => $codigo_formulario));
			
			foreach($this->grid_iterator($grids['grid_contatos']) as $contato){
				$tipo = explode(' - ', $contato['tipo_contato']);
				$dados = array(
					$campos['filial'] 				=> '',
					$campos['codigo_formulario'] 	=> $codigo_formulario,
					$campos['codigo_contato']		=> '',
					$campos['nome_contato']			=> $contato['nome_contato'],
					$campos['tipo_contato']			=> $tipo[0],
					$campos['telefone']				=> $contato['telefone_contato'],
					$campos['email']				=> ($contato['email_contato'])?$contato['email_contato']:''				
				);
				if(count($dados) > 0){
					$id = $this->db_cliente->select('max('.$campos['chave'] . ') as quantidade')->from($tabela)->get()->row_array();
					$id = $id['quantidade'] + 1;
					$dados[$campos['chave']] = $id;
					
					$this->db_cliente->insert($tabela, $dados);
				}
			}
		}
		return $this->exportar_grid_concorrentes($codigo_formulario, $grids);
	}
	
	function exportar_grid_concorrentes($codigo_formulario, $grids){
		$dados = array();
		$tabela = $this->_db_cliente['tabelas']['concorrentes_formulario_visita'];
		$campos = $this->_db_cliente['campos']['concorrentes_formulario_visita'];
		
		
		if(is_array($grids['grid_concorrentes'])){
			
			$this->db_cliente->delete($tabela, array($campos['codigo_formulario'] => $codigo_formulario));
			
			foreach($this->grid_iterator($grids['grid_concorrentes']) as $grid){
				$concorrente = explode(' - ', $grid['nome_concorrente_revenda']);
				$prod = explode(' - ', $grid['descricao_concorrente_revenda']);
				
				if(count($prod) == 1){
					$prod[1] = $prod[0];
					$prod[0] = '';
				}
				
				$dados = array(
					$campos['filial'] 				=> '',
					$campos['codigo_formulario'] 	=> $codigo_formulario,
					$campos['produto']				=> $prod[0],
					$campos['nome_produto']			=> $prod[1],
					$campos['concorrente']			=> $concorrente[1],
					$campos['preco']				=> (double)str_replace('R$ ', '', str_replace(',','.', str_replace('.', '', $grid['preco_concorrente_revenda']))),
					$campos['atualizacao']			=> data2protheus(date('d/m/Y')),
					$campos['codigo_concorrente']	=> $concorrente[0]					
				);
				
				if(count($dados) > 0){
					$id = $this->db_cliente->select('max('.$campos['chave'] . ') as quantidade')->from($tabela)->get()->row_array();
					$id = $id['quantidade'] + 1;
					$dados[$campos['chave']] = $id;
					
					$this->db_cliente->insert($tabela, $dados);
				}
			}
		}
		return $this->exportar_grid_produtos($codigo_formulario, $grids);
	}
	
	function exportar_grid_produtos($codigo_formulario, $grids){
		$dados = array();
		$tabela = $this->_db_cliente['tabelas']['produtos_apresentados'];
		$campos = $this->_db_cliente['campos']['produtos_apresentados'];
		
		
		if(is_array($grids['grid_produtos'])){
			
			$this->db_cliente->delete($tabela, array($campos['codigo_formulario'] => $codigo_formulario));
			
			foreach($this->grid_iterator($grids['grid_produtos']) as $grid){
				$prod = explode(' - ', $grid['descricao_produto_apresentado']);
				$dados = array(
					$campos['filial'] 				=> '',
					$campos['codigo_formulario'] 	=> $codigo_formulario,
					$campos['produto_apresentado']	=> $prod[0],
					$campos['preco']				=> (double)str_replace('R$ ', '', str_replace(',','.', str_replace('.', '', $grid['preco_objetivo_produto_apresentado'])))			
				);
				
				if(count($dados) > 0){
					$id = $this->db_cliente->select('max('.$campos['chave'] . ') as quantidade')->from($tabela)->get()->row_array();
					$id = $id['quantidade'] + 1;
					$dados[$campos['chave']] = $id;
					
					$this->db_cliente->insert($tabela, $dados);
				}
			}
		}
		return $this->exportar_grid_interesse($codigo_formulario, $grids);
	}
	
	function exportar_grid_interesse($codigo_formulario, $grids){
		$dados = array();
		$tabela = $this->_db_cliente['tabelas']['produto_treinamento'];
		$campos = $this->_db_cliente['campos']['produto_treinamento'];
		
		if(is_array($grids['grid_interesse'])){
			
			$this->db_cliente->delete($tabela, array($campos['codigo_formulario'] => $codigo_formulario));
			
			foreach($this->grid_iterator($grids['grid_interesse']) as $grid){
				$trei = explode(' - ', $grid['interesse_treinamento']);
				$dados = array(
					$campos['filial'] 				=> '',
					$campos['codigo_formulario'] 	=> $codigo_formulario,
					$campos['produto']				=> $trei[0],		
				);
				
				if(count($dados) > 0){
					$id = $this->db_cliente->select('max('.$campos['chave'] . ') as quantidade')->from($tabela)->get()->row_array();
					$id = $id['quantidade'] + 1;
					$dados[$campos['chave']] = $id;
					
					$this->db_cliente->insert($tabela, $dados);
				}
			}
		}
		return $codigo_formulario;
	}
	
	function grid_iterator($grids){
		$ret = array();
		if(is_array($grids)){
			foreach($grids as $k => $grid){
				foreach($grid as $items){
					foreach($items as $key => $value){
						$ret[$k][$key] = $value;
					}
				}
			}
		}
		return $ret;
	}
	
	function obter_contatos_visita($codigo_formulario){
		$tabela = $this->_db_cliente['tabelas']['contatos_formulario_visita'];
		$campos = $this->_db_cliente['campos']['contatos_formulario_visita'];
		$select = '';
	
		foreach ($campos as $campo_portal => $campo_protheus)
		{
			if($campo_portal !== 'D_E_L_E_T_'){
				$select .= "$tabela.$campo_protheus as $campo_portal, ";
			}
		}
		
		return $this->db_cliente
					->select($select)
					->select("{$this->_db_cliente['campos']['segmentos']['descricao']} AS descricao")
					->where($this->_db_cliente['campos']['segmentos']['filial'], '05')
					->where($this->_db_cliente['campos']['segmentos']['tabela'], 'ZZ')
					->where("{$this->_db_cliente['tabelas']['segmentos']}.{$this->_db_cliente['campos']['segmentos']['delecao']}", '')
					->join($this->_db_cliente['tabelas']['segmentos'], "{$tabela}.{$campos['tipo_contato']} = {$this->_db_cliente['tabelas']['segmentos']}.{$this->_db_cliente['campos']['segmentos']['segmento']}", 'left')
					->from($tabela)
					->where($campos['codigo_formulario'], $codigo_formulario)
					->get()
					->result_array();
	}
	
	function obter_concorrentes_formulario_visita($codigo_formulario){
		$tabela = $this->_db_cliente['tabelas']['concorrentes_formulario_visita'];
		$campos = $this->_db_cliente['campos']['concorrentes_formulario_visita'];
		$select = '';
	
		foreach ($campos as $campo_portal => $campo_protheus)
		{
			$select .= "$tabela.$campo_protheus as $campo_portal, ";
		}
		
		return $this->db_cliente
					->select($select)
					->from($tabela)
					->where($campos['codigo_formulario'], $codigo_formulario)
					->get()
					->result_array();
	}
	
	function obter_produtos_apresentados($codigo_formulario){
		$tabela = $this->_db_cliente['tabelas']['produtos_apresentados'];
		$campos = $this->_db_cliente['campos']['produtos_apresentados'];
		$tbl_prod = $this->_db_cliente['tabelas']['produtos'];
		$camp_prod = $this->_db_cliente['campos']['produtos'];
		$select = '';
	
		foreach ($campos as $campo_portal => $campo_protheus)
		{
			$select .= "$tabela.$campo_protheus as $campo_portal, ";
		}
		
		return $this->db_cliente
					->select($select)
					->select("{$tbl_prod}.{$camp_prod['descricao']} AS descricao")
					->join($tbl_prod, "{$tbl_prod}.{$camp_prod['codigo']} = {$tabela}.{$campos['produto_apresentado']}", 'left')
					->from($tabela)
					->where($campos['codigo_formulario'], $codigo_formulario)
					->get()
					->result_array();
	}
	
	function obter_interesse_treinamento($codigo_formulario){
		$tabela = $this->_db_cliente['tabelas']['produto_treinamento'];
		$campos = $this->_db_cliente['campos']['produto_treinamento'];
		$tbl_prod = $this->_db_cliente['tabelas']['produtos'];
		$camp_prod = $this->_db_cliente['campos']['produtos'];
		$select = '';
	
		foreach ($campos as $campo_portal => $campo_protheus)
		{
			$select .= "$tabela.$campo_protheus as $campo_portal, ";
		}
		
		return 	$this->db_cliente
					->select($select)
					->select(" {$tbl_prod}.{$camp_prod['descricao']} as descricao ")
					->join($tbl_prod, "{$tbl_prod}.{$camp_prod['codigo']} = {$tabela}.{$campos['produto']}")
					->from($tabela)
					->where($campos['codigo_formulario'], $codigo_formulario)
					->get()
					->result_array();
	}
		
	function obter_segmentos_obra() {
	
		return 	$this->db_cliente
					->select("{$this->_db_cliente['campos']['segmentos']['segmento']} AS segmento")
					->select("{$this->_db_cliente['campos']['segmentos']['descricao']} AS descricao")
					->from("{$this->_db_cliente['tabelas']['segmentos']} (NOLOCK)")
					->where($this->_db_cliente['campos']['segmentos']['filial'], '05')
					->where($this->_db_cliente['campos']['segmentos']['tabela'], 'ZV')
					->where($this->_db_cliente['campos']['segmentos']['delecao'], '')
					->get()
					->result_array();
	}
	
	function obter_tipos_contato() {
	
		return 	$this->db_cliente
					->select("{$this->_db_cliente['campos']['segmentos']['segmento']} AS tipo")
					->select("{$this->_db_cliente['campos']['segmentos']['descricao']} AS descricao")
					->from("{$this->_db_cliente['tabelas']['segmentos']} (NOLOCK)")
					->where($this->_db_cliente['campos']['segmentos']['filial'], '05')
					->where($this->_db_cliente['campos']['segmentos']['tabela'], 'ZZ')
					->where($this->_db_cliente['campos']['segmentos']['delecao'], '')
					->get()
					->result_array();
	}
		
	function obter_tipos_cliente() {
	
		return 	$this->db_cliente
					->select("{$this->_db_cliente['campos']['segmentos']['descricao']} AS descricao")
					->select("{$this->_db_cliente['campos']['segmentos']['segmento']} AS tipo")
					->from("{$this->_db_cliente['tabelas']['segmentos']} (NOLOCK)")
					->where($this->_db_cliente['campos']['segmentos']['tabela'], 'T3')
					->where($this->_db_cliente['campos']['segmentos']['filial'], '05')
					->where($this->_db_cliente['campos']['segmentos']['delecao'], '')
					->order_by("CAST({$this->_db_cliente['campos']['segmentos']['segmento']} AS INT)")
					->get()
					->result_array();
	}
		
	function obter_produtos_formulario_visita($palavra_chave) {
	
		return 	$this->db_cliente
					->select("{$this->_db_cliente['campos']['produtos']['codigo']} AS codigo")
					->select("{$this->_db_cliente['campos']['produtos']['descricao']} AS descricao")
					->from("{$this->_db_cliente['tabelas']['produtos']} (NOLOCK)")
					->where("UPPER({$this->_db_cliente['campos']['produtos']['codigo']}) LIKE UPPER('%{$palavra_chave}%') OR UPPER({$this->_db_cliente['campos']['produtos']['descricao']}) LIKE UPPER('%{$palavra_chave}%')")
					->where($this->_db_cliente['campos']['produtos']['delecao'], '')
					->get()
					->result_array();		 
	}
	
	function obter_contatos_cliente($codigo_cliente, $loja_cliente) {
		
		$resultado =
            $this->db_cliente
			->select("{$this->_db_cliente['campos']['contatos']['nome']} AS contato_nome")
			->select("{$this->_db_cliente['campos']['contatos']['fone']} AS contato_fone")
			->select("{$this->_db_cliente['campos']['contatos']['email']} AS contato_email")
			->from($this->_db_cliente['tabelas']['contatos_entidades'])
			->join($this->_db_cliente['tabelas']['contatos'], "{$this->_db_cliente['campos']['contatos']['codigo_contato']} = {$this->_db_cliente['campos']['contatos_entidades']['codigo_contato']}", "inner")
			->where($this->_db_cliente['campos']['contatos_entidades']['codigo_entidade'], "{$codigo_cliente}{$loja_cliente}")
			->where($this->_db_cliente['campos']['contatos_entidades']['entidade'], "SA1")
			->where("{$this->_db_cliente['tabelas']['contatos']}.{$this->_db_cliente['campos']['contatos']['delecao']}", '')
			->where("{$this->_db_cliente['tabelas']['contatos_entidades']}.{$this->_db_cliente['campos']['contatos_entidades']['delecao']}", '')
			->limit(10)
			->get()
			->result_array();
		        
        foreach($resultado as $codigo => $contato) {
            $resultado[$codigo]["contato_nome"] = utf8_encode($contato["contato_nome"]);
        }
        
        return $resultado;
	}
	
	function obter_clientes_formulario_visita($palavra_chave, $representante, $nivel, $coordenacao = FALSE, $dados_representante = null) 
	{
		$limitar_representante = true;
		if(isset($dados_representante['categoria']) && $dados_representante['categoria'] == 6){
			$limitar_representante = false;
		}
		/*
		003051 - [CUSTOM] Ajuste qry busca cliente form visitas direto
		
		$codigo_usuario = '';
		if(!in_array(trim($this->session->userdata('grupo_usuario')), array('gestores_comerciais'))){
			$codigo_usuario = trim($this->session->userdata('codigo_usuario'));
		}
		
		$this->db_cliente
			->select("{$this->_db_cliente['campos']['clientes']['codigo']} 						AS codigo")
			->select("{$this->_db_cliente['campos']['clientes']['loja']}   						AS loja")
			->select("{$this->_db_cliente['campos']['clientes']['nome']}   						AS nome")
			->select("{$this->_db_cliente['campos']['clientes']['nome_fantasia']}  				AS nome_fantasia")
			->select("{$this->_db_cliente['campos']['clientes']['cpf']}   						AS cpf")
			->select("{$this->_db_cliente['campos']['clientes']['telefone']}   					AS telefone")
			->select("{$this->_db_cliente['campos']['clientes']['estado']}   					AS estado")
			->select("{$this->_db_cliente['campos']['clientes']['codigo_representante']}   		AS codigo_representante")
			->select("{$this->_db_cliente['campos']['representantes']['nome']}   				AS nome_representante,
				CASE
					WHEN A3_NOME IS NULL THEN 'A'
					WHEN A3_NOME LIKE '%DIRETO%' THEN 'A'
					ELSE 'V'
				END cor_cliente
			")
			->from($this->_db_cliente['tabelas']['clientes'])
			->join($this->_db_cliente['tabelas']['representantes'], $this->_db_cliente['campos']['representantes']['codigo'].' = ' . $this->_db_cliente['campos']['clientes']['codigo_representante'] , 'LEFT')
			
			
			->where("(UPPER({$this->_db_cliente['campos']['clientes']['nome']}) LIKE UPPER('%{$palavra_chave}%') OR UPPER({$this->_db_cliente['campos']['clientes']['codigo']}) LIKE UPPER('%{$palavra_chave}%') OR UPPER({$this->_db_cliente['campos']['clientes']['cpf']}) LIKE UPPER('%{$palavra_chave}%') )")
			
			->where($this->_db_cliente['campos']['clientes']['filial'].' = '.$this->_db_cliente['campos']['representantes']['filial'] )
			->where($this->_db_cliente['tabelas']['clientes'].'.'.$this->_db_cliente['campos']['clientes']['delecao'], '')
			->where($this->_db_cliente['tabelas']['representantes'].'.'.$this->_db_cliente['campos']['representantes']['delecao'], '')
			
			
			
			;
			
			if($limitar_representante){
				if(!in_array($nivel, array('gestores_comerciais'))){
					$this->db_cliente->where(" (( '{$representante}' = '48' AND A1_EST IN ('PR','SC','RS')) OR  ( '{$representante}' <> '48' AND A1_EST IN ( SELECT A3_EST  FROM SA3010 SA3 WHERE  SA3.A3_COD = '{$representante}' AND SA3.D_E_L_E_T_ = ''))  OR A1_VEND = '{$representante}')");
				}
			}
			
			$dados = $this->db_cliente
				->get()
				->result_array();	
			*/
			
		if($coordenacao && $representante)
		{
		//echo 'if';
		
			$this->db_cliente->select($this->_db_cliente['campos']['coordenadores']['codigo_representante']);
			$this->db_cliente->from($this->_db_cliente['tabelas']['coordenadores']);
			$this->db_cliente->where($this->_db_cliente['campos']['coordenadores']['codigo_coordenador'], $representante);
			$this->db_cliente->where($this->_db_cliente['campos']['coordenadores']['delecao'], '');
			$teste = $this->db_cliente->get()->result_array();
			$subQuery = $this->db_cliente->last_query();
			
			$this->db_cliente->where($this->_db_cliente['tabelas']['clientes'].'.'.$this->_db_cliente['campos']['clientes']['codigo_representante'] .' IN ('.$subQuery.')', null, false);
		}
		else if($representante)
		{
		//echo 'else';
		
			$this->db_cliente->where($this->_db_cliente['campos']['clientes']['codigo_representante'], $representante);
		}
		
		$dados = $this->db_cliente->select("{$this->_db_cliente['campos']['clientes']['codigo']} 				AS codigo")
						->select("{$this->_db_cliente['campos']['clientes']['loja']}   					AS loja")
						->select("{$this->_db_cliente['campos']['clientes']['nome']}   					AS nome")
						->select("{$this->_db_cliente['campos']['clientes']['nome_fantasia']}  			AS nome_fantasia")
						->select("{$this->_db_cliente['campos']['clientes']['cpf']}   					AS cpf")
						->select("{$this->_db_cliente['campos']['clientes']['telefone']}   				AS telefone")
						->select("{$this->_db_cliente['campos']['clientes']['estado']}   				AS estado")
						->select("{$this->_db_cliente['campos']['clientes']['codigo_representante']}	AS codigo_representante")
						->from($this->_db_cliente['tabelas']['clientes'])
						->where("(UPPER({$this->_db_cliente['campos']['clientes']['nome']}) LIKE UPPER('%{$palavra_chave}%') OR UPPER({$this->_db_cliente['campos']['clientes']['codigo']}) LIKE UPPER('%{$palavra_chave}%') OR UPPER({$this->_db_cliente['campos']['clientes']['cpf']}) LIKE UPPER('%{$palavra_chave}%') )")
						->where($this->_db_cliente['tabelas']['clientes'].'.'.$this->_db_cliente['campos']['clientes']['delecao'], '')
						->where($this->_db_cliente['tabelas']['clientes'].'.'.$this->_db_cliente['campos']['clientes']['bloqueado'] . '<>', '1')
						->get()->result_array();
		//echo $this->db_cliente->last_query();
		//die();
		return $dados;
		
	}
	
	function obter_formulario_visita($codigo){
		$tabela = $this->_db_cliente['tabelas']['formulario_visitas'];
		$campos = $this->_db_cliente['campos']['formulario_visitas'];
		$select = '';
	
		foreach ($campos as $campo_portal => $campo_protheus)
		{
			$select .= "$campo_protheus as $campo_portal, ";
		}
		
		return $this->db_cliente->from($tabela)->select($select)->where($campos['codigo_formulario'], $codigo)->get()->row_array();
		
		//debug_pre($this->db_cliente->last_query());
		
	}
	
	function get_representantes() {
		$representantes = $this->db_cliente->select('A3_COD as codigo, A3_NOME as nome')->from('SA3010')->get()->result_array();
		$dados = array('todos' => 'TODOS');
		foreach($representantes as $representante){
			if($representante['codigo'] !== ''){
				$representante['nome'] = htmlentities($representante['nome']);
				$dados[$representante['codigo']] = "{$representante['codigo']} - {$representante['nome']}"; 
			}
		}
		return $dados;
	}
	
	function get_representantesNotas() 
	{
		$usuario = $this->db->from('usuarios')->where('codigo', $this->session->userdata('codigo_usuario'))->where('usuarios.status !=', 'inativo')->get()->row();
		
		if ($this->db->from('usuarios')->where(array('grupo' => 'supervisores', 'codigo' => $this->session->userdata('codigo_usuario')))->get()->row())
		{
			$representantes = $this->db->from('representantes_supervisores')
										->join('usuarios', 'codigo' . ' = ' . 'codigo_representante')
										->where('id_supervisor', $usuario->id)
										->where('grupo !=', 'gestores_comerciais')
										->where('usuarios.status !=', 'inativo')
										->get()->result();
								//echo $this->db->last_query();
		}
		else if ($this->db->from('usuarios')->where(array('grupo' => 'representantes', 'id' => $usuario->id))->get()->row())
		{
			$representantes = $this->db->from('usuarios')
										->where('codigo', $this->session->userdata('codigo_usuario'))
										->where('grupo !=', 'gestores_comerciais')
										->where('usuarios.status !=', 'inativo')
										->get()->row();
		}	
		else
		{
			$representantes = $this->db->from('usuarios')
										->where('grupo !=', 'gestores_comerciais')
										->where('usuarios.status !=', 'inativo')
										->get()->result();
		}
		
		//$teste = $this->db->from('usuarios')->where(array('grupo' => 'representantes', 'codigo' => $this->session->userdata('codigo_usuario')))->get()->row();
		//echo var_dump($teste);
		//echo $this->session->userdata('codigo_usuario');
		//echo 'codigo<br>';
		//echo $this->db->last_query();
		//echo "<br><pre>";
		//echo var_dump($representantes);
		//echo "</pre>";
		
		$dados = array('' => 'SELECIONE');
		if(count($representantes) > 1)
		{	
			foreach($representantes as $representante)
			{
				if($representante->codigo !== '')
				{
					$representante->nome = htmlentities($representante->nome);
					$dados[$representante->codigo] = "{$representante->codigo} - {$representante->nome}";
				}
			}
		}
		else
		{
			$representantes->nome = htmlentities($representantes->nome);
			$dados[$representantes->codigo] = "{$representantes->codigo} - {$representantes->nome}";
		}
		return $dados;
	}
	
	function obter_formularios_visita($db_cliente, $per_page = null){
	
		$campos = array('codigo_formulario', 'codigo_representante', 'loja_cliente', 'nome_cliente', 'cidade_cliente', 'estado_cliente', 'data_visitacao');
		
		foreach ($campos as $campo)
		{
			$select[] = $this->_db_cliente['tabelas']['formulario_visitas'] . '.' .$this->_db_cliente['campos']['formulario_visitas'][$campo] . ' as ' . $campo;
		}
		
		$db_cliente->from($this->_db_cliente['tabelas']['formulario_visitas']);
		
		if(is_null($per_page)){
		
			$dados = $db_cliente
						->select('count(DISTINCT '.$this->_db_cliente['campos']['formulario_visitas']['codigo_formulario'] . ') as quantidade')
						->join('SA3010', "SA3010.A3_COD = {$this->_db_cliente['tabelas']['formulario_visitas']}.{$this->_db_cliente['campos']['formulario_visitas']['codigo_representante']} AND SA3010.D_E_L_E_T_ != '*'")
						->get()
						->row_array();
						
		} else {
		
			$dados = $db_cliente
						->select(implode(', ', $select))
						->select('A3_NOME as nome_representante')
						->join('SA3010', "SA3010.A3_COD = {$this->_db_cliente['tabelas']['formulario_visitas']}.{$this->_db_cliente['campos']['formulario_visitas']['codigo_representante']} AND SA3010.D_E_L_E_T_ != '*'")
						->limit(20, $per_page)
						->get()
						->result_array();
		}
		return $dados;
	}
	
	function obter_concorrentes($palavra_chave) {
		
		return $this->db_cliente
					->select("{$this->_db_cliente['campos']['concorrentes']['codigo']} AS codigo")
					->select("{$this->_db_cliente['campos']['concorrentes']['nome']} AS nome")
					->from($this->_db_cliente['tabelas']['concorrentes'])
					->where("UPPER({$this->_db_cliente['campos']['concorrentes']['codigo']}) LIKE UPPER('%{$palavra_chave}%') OR UPPER({$this->_db_cliente['campos']['concorrentes']['nome']}) LIKE UPPER('%{$palavra_chave}%')")
					->where($this->_db_cliente['campos']['concorrentes']['delecao'],'')
					->get()
					->result_array();
	}
}