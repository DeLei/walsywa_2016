<?php

class Db_rnc extends CI_Model {
		
	function __construct()
	{
		parent::__construct();
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->config->load('db_cliente_' . $this->db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');
	}
	
	function obter_rnc($codigo)
	{
		$select = array();
		foreach($this->_db_cliente['campos']['rnc'] as $alias => $campo)
		{
			$select[] = $campo . ' AS ' . $alias;
		}
		
		return $this->db_cliente->select($select)
								->from($this->_db_cliente['tabelas']['rnc'])
								->where($this->_db_cliente['tabelas']['rnc'] . '.' . $this->_db_cliente['campos']['rnc']['delecao'] . ' !=', '*')
								->where($this->_db_cliente['campos']['rnc']['codigo'], $codigo)
								
								->get()->row();
	}
	
	function gravar_rnc($dados_formulario)
	{
		$tabela = $this->_db_cliente['tabelas']['rnc'];
		$campos = $this->_db_cliente['campos']['rnc'];

		$dados = array(
			$campos['filial']				=>	$dados_formulario['filial'],
			$campos['codigo']				=>	$dados_formulario['codigo'],			
			$campos['data_incidente']		=>	$dados_formulario['data_incidente'],
			$campos['data_emissao']			=>	date('Ymd'),
			$campos['codigo_cliente']		=>	$dados_formulario['codigo_cliente'],			
			$campos['nome_cliente']			=>	$dados_formulario['nome_cliente'],
			$campos['pedido']				=>  $dados_formulario['pedido'],
			$campos['codigo_nota_fiscal']	=>  $dados_formulario['codigo_nota_fiscal'],
			$campos['serie_nota_fiscal']	=>  $dados_formulario['serie_nota_fiscal'],
			$campos['descricao']			=>	$dados_formulario['descricao'],
			$campos['status']				=>	$dados_formulario['status'],
			$campos['codigo_representante']	=>	$dados_formulario['codigo_representante'],
			$campos['responsavel']			=>	$dados_formulario['responsavel'],
			$campos['stinter']				=>	1,
			$campos['origem']				=>	'P',
			
		);

		if($dados_formulario['codigo']){
		
			$visita = $this->db_cliente->update($tabela, $dados, array($campos['codigo']	=>	$dados_formulario['codigo']));
			
		} else {
		
			$id = $this->db_cliente
			->select('MAX('. $campos['chave'] . ') as quantidade')
			->from($tabela)
			->get()
			->row_array();
			
			$id = $id['quantidade'] + 1;
			
			
			$dados[$campos['codigo']] = str_pad($id, 6, '0', STR_PAD_LEFT );
			$dados[$campos['chave']] = $id; 
			$dados_formulario['codigo'] = str_pad($id, 6, '0', STR_PAD_LEFT );
			
			return $this->db_cliente->insert($tabela, $dados);
		}		
	}
	
	function obter_relatorios($db_cliente, $per_page = null)
	{
		
		$campos = array('status', 'codigo',  'nome_cliente',  'data_incidente', 'pedido', 'codigo_nota_fiscal', 'codigo_representante');
		
		foreach ($campos as $campo)
		{
			$select[] = $this->_db_cliente['tabelas']['rnc'] . '.' .$this->_db_cliente['campos']['rnc'][$campo] . ' as ' . $campo;
		}
		
		$db_cliente->from($this->_db_cliente['tabelas']['rnc']);
		
		if(is_null($per_page)){
		
			$dados = $db_cliente
						->select('count(DISTINCT '.$this->_db_cliente['campos']['rnc']['codigo'] . ') as quantidade')
						->join('SA3010', "SA3010.A3_COD = {$this->_db_cliente['tabelas']['rnc']}.{$this->_db_cliente['campos']['rnc']['codigo_representante']} AND SA3010.D_E_L_E_T_ != '*'",'LEFT')
						->get()
						->row_array();

		} else {
		
			$dados = $db_cliente
						->select(implode(', ', $select))
						->select('A3_NOME as nome_representante')
						->join('SA3010', "SA3010.A3_COD = {$this->_db_cliente['tabelas']['rnc']}.{$this->_db_cliente['campos']['rnc']['codigo_representante']} AND SA3010.D_E_L_E_T_ != '*'",'LEFT')
						->limit(20, $per_page)
						->get()
						->result_array();
		}
		//debug_pre($db_cliente->last_query());
		
		return $dados;
	}
	
	function excluir_relatorio($codigo)
	{
		$tabela = $this->_db_cliente['tabelas']['rnc'];
		$campo = $this->_db_cliente['campos']['rnc']['codigo'];
		
		return $this->db_cliente->delete($tabela, array($campo => $codigo)); 
	}
	
	
	function get_status(){
		
		return array(
			'A' => 'ABERTO',
			'T' => 'ATRASADO',
			'F' => 'FINALIZADO',
		);
		
	
	}

	function get_status_img(){
		
		return array(
			'A' => 'ABERTO',
			'T' => 'ATRASADO',
			'F' => 'FINALIZADO',
		);
		
	
	}
	

	function get_tipos(){
		
		return array(
			'L' => 'Logística',
			'V' => 'Vendas',
			
		);		
	
	}
	
	
	function get_responsavel_tipos(){
		
		return array(
			'P' => 'PA',
			'E' => 'Expedição',
			'Q' => 'Qualidade',
			'C' => 'Cliente',
			'S' => 'Sistema',
			'V' => 'Vendedor Externo',
			'R' => 'Representante',
			'T' => 'Transportadora'			
		);		
	
	}
	
	function obter_pedidos_cliente($filial, $codigo_cliente, $term){
			

		$sql = "SELECT C5_NUM,C5_EMISSAO,C5_NOTA,C5_SERIE
					FROM SC5010 SC5
					WHERE   
							SC5.C5_FILIAL  = ?
						AND SC5.C5_CLIENTE = ?
						AND C5_NUM like '%".$term."%'
						AND SC5.C5_EMISSAO >= CONVERT(Char(10),DATEADD(MONTH, -12, GETDATE()),112)
						AND SC5.D_E_L_E_T_ = ''";
		
		
		$query = $this->db_cliente->query($sql, array($filial, $codigo_cliente));
		
		$dados = array();
		foreach( $query->result() as $pedido ){
			
			$dados[] = array(
				'codigo' => $pedido->C5_NUM,
				'data_emissao' => protheus2data($pedido->C5_EMISSAO),
				'nota_fiscal' => $pedido->C5_NOTA,
				'serie' => 		$pedido->C5_SERIE		
							);

		}		
		return $dados;
	}
	
	function obter_notas_fiscais_cliente($filial, $codigo_cliente, $term){
			

		$sql = "SELECT DISTINCT F2_DOC,F2_SERIE,F2_EMISSAO,D2_PEDIDO
					FROM SF2010 SF2
					INNER JOIN SD2010 SD2 ON
								SD2.D2_FILIAL = SF2.F2_FILIAL
						AND SD2.D2_SERIE = SF2.F2_SERIE
						AND SD2.D2_DOC   = SF2.F2_DOC
						AND SD2.D2_CLIENTE = SF2.F2_CLIENTE
						AND SD2.D2_LOJA = SF2.F2_LOJA
						AND SD2.D_E_L_E_T_ = ''
					WHERE
						SF2.F2_FILIAL    = ?
						AND SF2.F2_CLIENTE  = ?
						AND F2_DOC like '%".$term."%'
						AND SF2.F2_EMISSAO  >= CONVERT(Char(10),DATEADD(MONTH, -3, GETDATE()),112)
						AND SF2.D_E_L_E_T_  = ''
					";
							
		
		$query = $this->db_cliente->query($sql, array($filial, $codigo_cliente));
		
		$dados = array();
		foreach( $query->result() as $pedido ){
			
			$dados[] = array(
				'codigo' => $pedido->D2_PEDIDO,
				'data_emissao' => protheus2data($pedido->F2_EMISSAO),
				'nota_fiscal' => $pedido->F2_DOC,
				'serie' => 		$pedido->F2_SERIE		
							);

		}		
		return $dados;
	}
	
	
}