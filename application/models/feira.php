<?php

class Feira extends CI_Model {
	
    function __construct()
    {
        parent::__construct();
    }
	
	function obter_ativas($retorno_dropdown = FALSE, $opcao_nenhuma = FALSE)
	{
		$feiras = $this->db->from('feiras')->where(array('status' => 'ativa'))->order_by('data_inicial_timestamp', 'desc')->get()->result();
		
		if (!$retorno_dropdown)
		{
			return $feiras;
		}
		
		$_feiras = $opcao_nenhuma ? array('Nenhuma') : array();
		
		foreach ($feiras as $feira)
		{
			$_feiras[$feira->id] = $feira->nome . ' - ' . $feira->data_inicial . ' a ' . $feira->data_final;
		}
		
		return $_feiras;
	}
	
}