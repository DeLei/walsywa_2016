<?php

class Cliente extends CI_Model {
	
    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		
		$this->config->load('db_cliente_' . $this->db_cliente->erp);
		$this->config_db_cliente = $this->config->item('db_cliente');
		
		$this->tabela = $this->config_db_cliente['tabelas']['clientes'];
		$this->campos = $this->config_db_cliente['campos']['clientes'];
    }
	
	function obter_cidades()
	{
		$cidades = $this->db_cliente->distinct()->select($this->campos['cidade'])->from($this->tabela)->order_by($this->campos['cidade'], 'asc')->get()->result_array();
		$_cidades = array();
		
		foreach ($cidades as $cidade)
		{
			$cidade = $cidade[$this->campos['cidade']];
			
			if (!trim($cidade))
			{
				continue;
			}
			
			$_cidades[$cidade] = $cidade;
		}
		
		return $_cidades;
	}
	
	function obter_estados()
	{
		$estados = $this->db_cliente->distinct()->select($this->campos['estado'])->from($this->tabela)->order_by($this->campos['estado'], 'asc')->get()->result_array();
		
		$_estados = array();
		
		foreach ($estados as $estado)
		{
			$estado = $estado[$this->campos['estado']];
			
			if (!trim($estado))
			{
				continue;
			}
			
			$_estados[$estado] = $estado;
		}
		
		return $_estados;
	}
	
}