<?php

class Shazam_model extends CI_Model {
	
    function __construct() {
        parent::__construct();
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->config->load('db_cliente_' . $this->db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');
    }
	
	function obter_shazam_vendedor($codigo_vendedor, $restricao = 'N') {
		return $this->db_cliente->query("SELECT * FROM GET_QTD_SHAZAM('$codigo_vendedor', 'E', '$restricao')")->row();
	}
	
	function retirar_um_shazam_vendedor($codigo_vendedor, $restricao = 'N') {
		return $this->db_cliente->query("EXEC RETIRA_SHAZAM $codigo_vendedor, '$restricao' ")->row();
	}
	
	function buscar_descontos_shazam($codigo_cliente, $codigo_vendedor, $codigo_produtos, $valor_total_pedido) 
	{
		//return $this->db_cliente->query('EXEC PRD_SHAZAM \'' . '1' . '\',\'' . $codigo_cliente . '\',\'' . $codigo_vendedor . '\',\'' . $codigo_produtos . '\',1,' . $valor_total_pedido . '')->result();
		return $this->db_cliente->query("EXEC PRD_SHAZAM '$codigo_cliente', '$codigo_vendedor', '$codigo_produtos', 1, '$valor_total_pedido'")->result();
	}
	
	// custom 475
	function obter_shazam_restricao($codigo_vendedor, $produtos) {
		return $this->db_cliente->query("SELECT * FROM PROD_RESTRI('$codigo_vendedor', '$produtos')")->row_array();
	}
	// fim custom 475
	
	function estado_filial($codigo_filial) {
		$dados = array(
			'03' => 'MG',
			'04' => 'SC',
			'05' => 'SP',
			'06' => 'PE'
		);
		return $dados[$codigo_filial];
	}
	
	function aplicar_descontos_shazam($sessao_pedido) 
	{
		/* Busca os Descontos */
		$produtos = array();
		$total = 0;

		// custom 475
			$contagem = 0;
			$produtos_lista = '';
		// fim custom 475
		$sessao_pedido['sem_shazam'] = '';
		
		//montagem da lista dos códigos dos produtoas para a consulta de restrições
		if($sessao_pedido['produtos'] != null)
		{
			foreach($sessao_pedido['produtos'] as $key => $produto) 
			{
				// custom 475
					if($contagem == 0){
						$produtos_lista = $produto['codigo'];
					}else{
						$produtos_lista = $produtos_lista.','.$produto['codigo'];
					}
					$contagem++;
					$produto['restricao'] = 'S';
					$sessao_pedido['produtos'][$key]['shazam_'] = '0';
				// fim custom 475
			}
		}
		
		if($sessao_pedido['produtos_programados'] != null)
		{
			foreach($sessao_pedido['produtos_programados'] as $key => $produto) 
			{
				// custom 475
					if($contagem == 0){
						$produtos_lista = $produto['codigo'];
					}else{
						$produtos_lista = $produtos_lista.','.$produto['codigo'];
					}
					$contagem++;
					$produto['restricao'] = 'S';
					$sessao_pedido['produtos_programados'][$key]['shazam_'] = '0';
				// fim custom 475
			}
		}
		
		// custom 475
		$restricoes = $this->obter_shazam_restricao($sessao_pedido['cliente']['codigo'], $produtos_lista);
		
		
		//busca a quantidade de cartas na manga diponíveis com e sem restrição
		$cartaRestriSim = $this->obter_shazam_vendedor($sessao_pedido['cliente']['codigo_representante'], 'S');
		$cartaRestriNao = $this->obter_shazam_vendedor($sessao_pedido['cliente']['codigo_representante'], 'N');
		$qtdSim = 0;
		$qtdNao = 0;
		
		// se a quantiadde de shazam for zero para ambos os tipos ignora o cálculo, pois não pode ser aplicado sem a liberação
		if($cartaRestriSim->QTDE > 0 || $cartaRestriNao->QTDE > 0)
		{	
			if($restricoes != null)
			{
				foreach($restricoes as $restricao) 
				{
					if(count($sessao_pedido['produtos']) > 0)
					{
						foreach($sessao_pedido['produtos'] as $key => $produto) 
						{
							if($restricao == $produto['codigo'])
							{
								$sessao_pedido['produtos'][$key]['restricao'] = 'S';
								$sessao_pedido['produtos'][$key]['shazam_'] = '1';
								$qtdSim++;
							}
						}
					}
					
					if(count($sessao_pedido['produtos_programados']) > 0)
					{
						foreach($sessao_pedido['produtos_programados'] as $key => $produto) 
						{
							if($restricao == $produto['codigo'])
							{
								$sessao_pedido['produtos_programados'][$key]['restricao'] = 'S';
								//$sessao_pedido['produtos_programados'][$key]['shazam_'] = '1';
								$qtdSim++;
							}
						}
					}
					
				}
			}				
			else
			{
				$qtdNao++;
			}	

			
			
			// verifica a quantidade de shazam de cada tipo e a quantidade de produtos de cada tipo para poder liberar ou não
			if(($cartaRestriSim->QTDE > 0 && $qtdSim > 0) || ($cartaRestriNao->QTDE > 0 && $qtdNao > 0))
			{
				if(count($sessao_pedido['produtos']) > 0)
				{
					foreach($sessao_pedido['produtos'] as $key => $produto) 
					{
						$sessao_pedido['produtos'][$key]['shazam_'] = '1';
						$sessao_pedido['produtos'][$key]['restricao'] = 'S';
						$sessao_pedido['produtos'][$key]['desconto'] = '0';
						$produtos[] = $produto['codigo'];
						$total += $produto['quantidade_pedido'] * $produto['preco'];
					}
				}
				
				if(count($sessao_pedido['produtos_programados']) > 0)
				{
					foreach($sessao_pedido['produtos_programados'] as $key => $produto) 
					{
						$sessao_pedido['produtos_programados'][$key]['shazam_'] = '1';
						$sessao_pedido['produtos_programados'][$key]['restricao'] = 'S';
						$sessao_pedido['produtos_programados'][$key]['desconto'] = '0';
						$produtos[] = $produto['codigo'];
						$total += $produto['quantidade_pedido'] * $produto['preco'];
					}
				}
					
				foreach(array('produtos', 'produtos_programados') as $grupoProdutos) 
				{
					if(count($sessao_pedido[$grupoProdutos]) > 0)
					{
						foreach($sessao_pedido[$grupoProdutos] as $key => $produto) 
						{
							if($restricoes != null)
							{
								foreach($restricoes as $restricao) 
								{
									if($restricao == $produto['codigo'])
									{
										$sessao_pedido[$grupoProdutos][$key]['restricao'] = 'S';
									}
									else
									{
										$sessao_pedido[$grupoProdutos][$key]['restricao'] = 'N';
									}
								}
							}
							else
							{
								$sessao_pedido[$grupoProdutos][$key]['restricao'] = 'N';
							}
						}
					}
				}
			
				// fim custom 475
				
				$descontos = $this->buscar_descontos_shazam($sessao_pedido['cliente']['codigo'], $sessao_pedido['cliente']['codigo_representante'], implode(',', $produtos), $total);
			
			//echo "<pre>";
			//echo var_dump($descontos);
			//echo "</pre>";
			
			
				/* Processa os Descontos */
				foreach($descontos as $desconto) 
				{
					foreach(array('produtos', 'produtos_programados') as $grupoProdutos) 
					{
						if($sessao_pedido[$grupoProdutos] != null)
						{
							foreach($sessao_pedido[$grupoProdutos] as $key => $produto) 
							{
								if ($desconto->PRODS == $produto['codigo']) 
								{
									if (($sessao_pedido['cliente']['tipo_cliente'] == 'XXX') or ($sessao_pedido['cliente']['tipo_cliente'] == 'CON') or ($sessao_pedido['cliente']['estado'] == $this->estado_filial($produto['filial']))) 
									{
										$tipoUsado = 'E';
									} else {
										$tipoUsado = 'I';
									}
									if ($desconto->Z40_TIPO == $tipoUsado) 
									{
										$sessao_pedido[$grupoProdutos][$key]['preco'] = $desconto->PRECO;
										$sessao_pedido['vencimento_shazam'] = $desconto->VCTO; //coloca vencimento no cabecalho do pedido
										$sessao_pedido['sem_shazam'] = '';
									}
								}
							}
						}
					}
				}
			}
			else
			{
				$sessao_pedido['sem_shazam'] = 'Desconto de carta na manga não foi aplicado a este pedido, regras comerciais não permitiram.';
			}
			//$sessao_pedido['desconto_shazam'] = 'S';
			
			//remover um shazam após o cálculo de acordo com o qual foi utilizado, restrito ou não
			if($restricoes != null)
			{
				$this->retirar_um_shazam_vendedor($sessao_pedido['cliente']['codigo_representante'], 'S');
			}
			else
			{
				$this->retirar_um_shazam_vendedor($sessao_pedido['cliente']['codigo_representante'], 'N');
			}
		}
		
		return $sessao_pedido;
	}
}