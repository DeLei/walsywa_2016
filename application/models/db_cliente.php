<?php

class Db_cliente extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		
		$this->config->load('db_cliente_' . $this->db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');
		
	}
	
	
	/**
	 * Função:			obter_impostos_item()
	 * 
	 * Descrição:		A função é responsável por obter as TES/CF/CFOP e os 
	 * 					calculos de ST/IPI/ICMS.
	 * 
	 * @post
	 * @return 			Valor da ST, TES e CF
	 */
	function obter_impostos_item($cliente, $cliente_loja, $cliente_tipo, $quantidade_produto, $preco_venda_produto, $codigo_produto, $filial)
	{	
		//Setar o Array de retorno
		$retorno 		= array();
		
		//Setar as variaveis usadas no laço
		$total_pedido 		= 0;
		$total_st_pedido 	= 0;
		$itens 				= array();
		
		//Montar array para o webservices
		$itens[] = array(	'CCLIENTE' 	=> (string)str_pad($cliente, 6, ' '), 		// Código do cliente 
							'CLOJA' 	=> (string)$cliente_loja, 	// Loja do Cliente
							'NFILIAL'	=> (string)$filial,			// Filial
							'CTIPO' 	=> (string)$cliente_tipo, 	// Tipo do cliente / para calcular normalmente é S
							'NQTD' 		=> (string)$quantidade_produto, // Quantidade comprada do item 
							'NPRC' 		=> (string)number_format($preco_venda_produto,3, '.', ''), // preço de venda (com .) sempre com 3decimais
							'NVALOR' 	=> (string)number_format($quantidade_produto * $preco_venda_produto,3, '.', ''), // quantidade de compra do item * preço de venda (com .) sempre com 3 decimais
							'CPRODUTO' 	=> (string)$codigo_produto);	

		
		
		// Realizar a chamada ao webservices passando o array auxiliar 
		// com todos os parametros que o webservices precisa.
		$impostos = obter_st($itens);			

		/* Removido pois os calculo é por item e não por pedido
		
		//percorrer os impostos calculados pelo webservices
		foreach($impostos as $imposto)
		{
			
			// Montar array com os impostos calculados e o produto
			$retorno[] = array( 
					'produto' 	=> $imposto['PRODUTO'],
					'TES'	  	=> $imposto['TES'],
					'CF'	  	=> $imposto['CF'],
					'CFOP'	  	=> $imposto['CFOP'],
					'ST'	  	=> number_format($imposto['ST'], 3, ',', '.'),											
					'IPI'		=> number_format($imposto['IPI'], 3, ',', '.'),
					'ICMS'		=> number_format($imposto['ICMS'], 3, ',', '.'),						
					'total'	 	=> number_format(($quantidade_produto * $preco_venda_produto + $imposto['ST']), 3, ',', '.')
			);		
			
			// Calcular os totais do pedido com impostos
			$total_pedido 		= $total_pedido 		+ (($produto['preco']*$produto['quantidade']) + $imposto['IPI'] + $imposto['ST']);
			$total_st_pedido 	= $total_st_pedido 		+ $imposto['ST'];
			$total_ipi_pedido 	= $total_ipi_pedido 	+ $imposto['IPI'];
			$total_icms_pedido 	= $total_icms_pedido 	+ $imposto['ICMS'];
		}
		
		// Montar array com os totais
		$retorno[] = array(
				'total_geral' 		=> number_format($total_pedido, 		3, ',', '.'),
				'valor_st_total' 	=> number_format($total_st_pedido, 	3, ',', '.'),
				'valor_ipi_total' 	=> number_format($total_ipi_pedido, 	3, ',', '.'),
				'valor_icms_total' 	=> number_format($total_icms_pedido, 	3, ',', '.'),					
		);
		*/
		
		return array(	'st' 	=> $impostos[0]['ST'], 
						'ipi' 	=> $impostos[0]['IPI'], 
						'icms' 	=> $impostos[0]['ICMS'], 
						'tes' 	=> $impostos[0]['TES'], 
						'cf' 	=> $impostos[0]['CF']
		);
		
	}
	
	
	
	
	public function calcular_st($codigo_da_empresa, $codigo_do_produto, $codigo_do_cliente, $loja_do_cliente, $total_dos_produtos_sem_ipi, $total_dos_produtos_com_ipi, $filial = 5)
	{
		// $total_dos_produtos_sem_ipi e $total_dos_produtos_com_ipi devem considerar o desconto dado, ou seja, total = total - desconto
		//SE A1_TIPO = ‘S’ AND ( A1_GRPTRIB = ‘VAZIO’ OR A1_GRPTRIB = ‘SN’) = EFETUE O CALCULO
		$cliente = $this->db_cliente->query('SELECT A1_FILIAL, A1_GRPTRIB, A1_EST, A1_TIPO FROM SA1' . $codigo_da_empresa . ' WHERE A1_COD = \'' . $codigo_do_cliente . '\' AND A1_LOJA = \'' . $loja_do_cliente . '\' AND D_E_L_E_T_ != \'*\'')->row_array();
		$produto = $this->db_cliente->query('SELECT B1_PICMRET, B1_ORIGEM, B1_GRTRIB, B1_COD FROM SB1' . $codigo_da_empresa . ' WHERE B1_COD = \'' . $codigo_do_produto . '\' AND D_E_L_E_T_ != \'*\'')->row_array();
		$tesInteligente = $this->db_cliente->query('SELECT F4_CODIGO, F4_CF, F4_BASEICM, F4_BSICMST FROM SFM' . $codigo_da_empresa . ' JOIN SF4010 ON (F4_CODIGO = FM_TS) WHERE FM_GRPROD = \'' . $produto['B1_GRTRIB'] . '\' AND FM_FILIAL = ' . $filial . ' AND FM_EST = \'' . $cliente['A1_EST'] . '\' AND FM_GRTRIB = \'' . $cliente['A1_GRPTRIB'] . '\' AND FM_TIPO = \'01\'  AND SFM010.D_E_L_E_T_ != \'*\' AND SF4010.D_E_L_E_T_ != \'*\'')->row_array();
		$array['tes'] = ($tesInteligente['F4_CODIGO']?$tesInteligente['F4_CODIGO']:0);
		$array['cf'] = ($tesInteligente['F4_CF']?$tesInteligente['F4_CF']:0);
		
		if($cliente['A1_TIPO'] == 'S' AND ($cliente['A1_GRPTRIB'] == '' OR $cliente['A1_GRPTRIB'] == 'SN')){
			//Erick pediu para retirar
			/*if ($produto['B1_PICMRET'] > 0)
			{
				return $total_dos_produtos_com_ipi * ($produto['B1_PICMRET'] / 100);
			}
			else */
			if ($produto['B1_GRTRIB']){
				$desc_icms_normal = true;
				$excessao_fiscal = $this->db_cliente->query('SELECT * FROM SF7' . $codigo_da_empresa . ' WHERE F7_GRTRIB = \'' . $produto['B1_GRTRIB'] . '\' AND F7_GRPCLI = \'' . $cliente['A1_GRPTRIB'] . '\' AND F7_EST = \'' . $cliente['A1_EST'] . '\' AND F7_TIPOCLI = \'' . $cliente['A1_TIPO'] . '\' AND F7_FILIAL = ' . $filial . ' AND D_E_L_E_T_ != \'*\'')->result_array();
				$estadosFiliais = array('01'=>'SP','02'=>'MG','03'=>'MG','04'=>'SC','05'=>'SP');
				$filialEstado = $estadosFiliais[str_pad($filial, 2, "0", STR_PAD_LEFT)];
				$margem = 0;
				
				foreach($excessao_fiscal as $ex_fiscal){
					if($ex_fiscal['F7_MARGEM'] > $margem){
						$margem = $ex_fiscal['F7_MARGEM'];
						$alicota_destino = $ex_fiscal['F7_ALIQDST'];
						$alicota_interna = $ex_fiscal['F7_ALIQINT'];
					}
				}
				
				if($margem <= 0){
					$excessao_fiscal = $this->db_cliente->query('SELECT * FROM SF7' . $codigo_da_empresa . ' WHERE F7_GRTRIB = \'' . $produto['B1_GRTRIB'] . '\' AND F7_GRPCLI = \'' . $cliente['A1_GRPTRIB'] . '\' AND F7_EST = \'**\' AND F7_TIPOCLI = \'S\' AND F7_FILIAL = ' . $filial . ' AND D_E_L_E_T_ != \'*\'')->result_array();
					foreach($excessao_fiscal as $ex_fiscal){
						if($ex_fiscal['F7_MARGEM'] > $margem){
							$margem = $ex_fiscal['F7_MARGEM'];
							$alicota_destino = $ex_fiscal['F7_ALIQDST'];
							$alicota_interna = $ex_fiscal['F7_ALIQINT'];
						}
					}
				}
				
				//Se o produto for importado busca o ICMS da tabela mysql, senão executa a regra antiga
				if ($produto['B1_ORIGEM'] == 1){
					$aliquota = $this->db->from('alicota')->where('origem', $filialEstado)->where('destino', $cliente['A1_EST'])->where('procedencia', 'importado')->get()->row()->alicota;
					$alicota_destino = $aliquota;
					$alicota_interna = $aliquota;
				}else{
					$icms = unserialize($this->db->select('icms')->from('empresas')->where('id', 1)->get()->row()->icms);
					if(!$alicota_destino > 0){ // alicota calculo antigo
						$alicota_destino = $icms[trim($cliente['A1_EST'])];
					}else{
						$desc_icms_normal = FALSE;		
						if(trim($cliente['A1_EST']) != trim($filialEstado)){
							// calculo da tabela de alicota aqui
							$recupera_alicota = $this->db->select('alicota')->from('alicota')->where('origem', $filialEstado)->where('destino', $cliente['A1_EST'])->get()->row();
							if ($recupera_alicota){
								$alicota_destino = $recupera_alicota->alicota;
								
							}else{
								$alicota_destino = 0;
							}
						}else{
							if(!$alicota_destino > 0){ // alicota calculo antigo
								$alicota_destino = $icms[trim($cliente['A1_EST'])];
							}
						}
					}	
					
					if(!$alicota_interna > 0){ // alicota calculo antigo
						if(trim($cliente['A1_EST']) != trim($filialEstado)){
							// calculo da tabela de alicota aqui
							$recupera_alicota = $this->db->select('alicota')->from('alicota')->where('origem', $filialEstado)->where('destino', $cliente['A1_EST'])->get()->row();
							if ($recupera_alicota){
								$alicota_interna = $recupera_alicota->alicota;
							}
						}else{
							$alicota_interna = $icms[trim($filialEstado)];	
						}
					}
				}
				
				/*
				if(!$alicota_interna > 0)
				{
					$alicota_interna = $icms[trim($filialEstado)];
				}	
				*/
	
				$valor_da_st = $total_dos_produtos_com_ipi * ($margem / 100 + 1); // obter IVA
				
				//$tesInteligente = $this->db_cliente->query('SELECT F4_CODIGO, F4_CF, F4_BASEICM, F4_BSICMST FROM SFM' . $codigo_da_empresa . ' JOIN SF4010 ON (F4_CODIGO = FM_TS) WHERE FM_GRPROD = \'' . $produto['B1_GRTRIB'] . '\' AND FM_FILIAL = ' . $filial . ' AND FM_EST = \'' . $cliente['A1_EST'] . '\' AND FM_GRTRIB = \'' . $cliente['A1_GRPTRIB'] . '\' AND FM_TIPO = \'01\'  AND SFM010.D_E_L_E_T_ != \'*\' AND SF4010.D_E_L_E_T_ != \'*\'')->row_array();
				
				if($tesInteligente['F4_BSICMST'] OR $tesInteligente['F4_BSICMST'] > 0){
					$total_dos_produtos_sem_ipi = $total_dos_produtos_sem_ipi * ($tesInteligente['F4_BASEICM']/100);
					$valor_da_st = $valor_da_st * ($tesInteligente['F4_BSICMST']/100);
				}
				
				$valor_do_icms_normal = $total_dos_produtos_sem_ipi * (($alicota_interna / 100)+1);
				$valor_da_st = $valor_da_st * ($alicota_destino/100); //percentual da alicota de destino
				
				if($desc_icms_normal){
					$valor_da_st = $valor_da_st - ($valor_do_icms_normal - $total_dos_produtos_sem_ipi);
				}else{
					$valor_da_st = $valor_da_st - $total_dos_produtos_sem_ipi*($alicota_interna/100);//subtrai o ICMS normal(valor normal pela alicota do estado)
				}
				
				if($margem){
					$array['st'] = $valor_da_st > 0 ? $valor_da_st : 0;
					return $array;
				}else{
					$array['st'] = 0;
					return $array;
				}
			}
			$array['st'] = 0;
			return $array;
			
		}else{
			$array['st'] = 0;
			return $array;
		}
	}
	
	
	function exportar_pedido($dados_pedido, $programado = FALSE)
	{
		//echo '<pre>';
		//echo var_dump($dados_pedido);
		//echo '</pre>';
		//die();
		
		if($dados_pedido)
		{
			$modent = 'P'; // FORMA DE PEDIDO 'I' IMEDIATO OU 'P' PROGRAMADO
			if(!$programado)
			{
				$modent = 'I';
				$produtos = $dados_pedido['produtos'];
				$obs_pedido = ($dados_pedido['observacao_pedido_imediato'] ? $dados_pedido['observacao_pedido_imediato'] : 0);
				
				$data_entrega = date('Ymd',time());
			}
			else
			{
				$produtos = $dados_pedido['produtos_programados'];
				$obs_pedido = ($dados_pedido['observacao_pedido_programado'] ? $dados_pedido['observacao_pedido_programado'] : 0);
				
				$_data_entrega = explode('/', $dados_pedido['data_entrega']);
				$data_entrega = $_data_entrega[2] . $_data_entrega[1] . $_data_entrega[0];
			}
			
			$_codido_cliente = explode('|', $dados_pedido['codigo_loja_cliente']);
			$codido_cliente = $_codido_cliente[0];
			$loja_cliente = $_codido_cliente[1];
			
			
			//E175/2014 - Edição de Orçamento
			//Descrição: Alterado o processo para obter o código do novo IDPED para facilitar manutenção.
			$id_pedido =  $this->obter_idped($dados_pedido['codigo_usuario']);
			//FIM E175/2014 - Edição de Orçamento
			foreach($produtos as $key => $produto)
			{
				
				// Chave Primária
				$chave = $this->db_cliente->select($this->_db_cliente['campos']['pedidos_dw']['chave'])->from($this->_db_cliente['tabelas']['pedidos_dw'])->order_by($this->_db_cliente['campos']['pedidos_dw']['chave'], 'DESC')->get()->row_array();
				$representante = $this->db_cliente->select($this->_db_cliente['campos']['representantes']['comissao'])->from($this->_db_cliente['tabelas']['representantes'])->where($this->_db_cliente['campos']['representantes']['codigo'],$dados_pedido['codigo_usuario'])->get()->row_array();
				$clientes = $this->db_cliente->select($this->_db_cliente['campos']['clientes']['estado'].' AS estado')->select($this->_db_cliente['campos']['clientes']['tipo_cliente'].' AS grupo_tributario')->select($this->_db_cliente['campos']['clientes']['tipo'].' AS tipo')->from($this->_db_cliente['tabelas']['clientes'])->where($this->_db_cliente['campos']['clientes']['nome'], (string)$dados_pedido['nome_cliente'])->get()->row_array();
				//não sabes os campos a serem filtrados $nome_desconhecido =  $this->db_cliente->select($this->_db_cliente['campos']['nome_desconhecido']['codigo'].', '.$this->_db_cliente['campos']['nome_desconhecido']['codigo'])->from($this->_db_cliente['tabelas']['nome_desconhecido'])->where($dados_pedido['codigo'])->get()->row_array();
				$produto_list =  $this->db_cliente->select($this->_db_cliente['campos']['produtos']['locpad'] .' AS locpad')->from($this->_db_cliente['tabelas']['produtos'])->where($this->_db_cliente['campos']['produtos']['codigo'],$produto['codigo'])->get()->row_array();
				//$representante = obter_representante();
				$chaves = $key + 1;
				$data = date('Ymd', time());
				$dados = array(
				
				   $this->_db_cliente['campos']['pedidos_dw']['numero_item']				=> ''.$chaves.'',
				   $this->_db_cliente['campos']['pedidos_dw']['data_edicao']						=> '',
				   $this->_db_cliente['campos']['pedidos_dw']['preco_venda']				=> number_format($produto['preco'] - (($produto['preco'] * $produto['desconto']) / 100), 5, '.', ''),
				   $this->_db_cliente['campos']['pedidos_dw']['preco_tabela']				=> number_format((float)$produto['preco'], 5, '.', ''),				   
				   
				   $this->_db_cliente['campos']['pedidos_dw']['id_pedido'] 					=> $id_pedido, // id_pedido (Código do Usuário / (Quantidade de pedidos do Usuário + 1))
				   $this->_db_cliente['campos']['pedidos_dw']['data_emissao'] 				=> ($data ? $data : ''), // data_emissao
				   $this->_db_cliente['campos']['pedidos_dw']['id_usuario']  				=> $dados_pedido['codigo_usuario'], // id_usuario
				   $this->_db_cliente['campos']['pedidos_dw']['codigo_representante']  		=> $dados_pedido['codigo_usuario'], // codigo_representante
				   $this->_db_cliente['campos']['pedidos_dw']['tipo']						=> ($dados_pedido['tipo'] == 'orcamento' ? 'O' : 'P'), // tipo
                                    
                            //CUSTOM: MPD-220 - 07/08/2013
				   $this->_db_cliente['campos']['pedidos_dw']['status'] 					=> ($dados_pedido['liberado'] == "L" ? "L" : 'A'), // status (A = AGUARDANDO ANALISE, L = IMPORTADO)
                            //FIM CUSTOM: MPD-220 - 07/08/2013
                                    
				   $this->_db_cliente['campos']['pedidos_dw']['codigo_cliente']				=> $codido_cliente, // codigo_cliente
				   $this->_db_cliente['campos']['pedidos_dw']['loja_cliente']				=> ($loja_cliente ? $loja_cliente : ''), // loja_cliente
				   $this->_db_cliente['campos']['pedidos_dw']['nome_cliente']				=> ($dados_pedido['nome_cliente'] ? $dados_pedido['nome_cliente'] : ''),
				   $this->_db_cliente['campos']['pedidos_dw']['id_prospects'] 				=> $dados_pedido['id_prospect'], // id_prospect
				   $this->_db_cliente['campos']['pedidos_dw']['nome_prospects']				=> ($dados_pedido['nome_prospect'] ? $dados_pedido['nome_prospect'] : ''),
				   $this->_db_cliente['campos']['pedidos_dw']['condicao_pagamento']	 		=> $dados_pedido['codigo_forma_pagamento'], // condicao_pagamento
				   $this->_db_cliente['campos']['pedidos_dw']['tipo_frete']					=> substr($dados_pedido['tipo_frete'], 0, 1), // tipo_frete
				   $this->_db_cliente['campos']['pedidos_dw']['valor_frete']	 			=> (float)number_format($dados_pedido['valor_frete'], 5, '.', ''), // valor_frete
				   $this->_db_cliente['campos']['pedidos_dw']['codigo_transportadora'] 		=> $dados_pedido['codigo_transportadora'], // codigo_transportdora
				   $this->_db_cliente['campos']['pedidos_dw']['ordem_compra'] 	 			=> $dados_pedido['codigo_ordem_compra'], // ordem_compra
				   $this->_db_cliente['campos']['pedidos_dw']['data_entrega'] 	 			=> $data_entrega, // data_entrega
				   $this->_db_cliente['campos']['pedidos_dw']['obs_pedido']  				=> $dados_pedido['observacao_pedido_imediato'], // obs_pedido
				   $this->_db_cliente['campos']['pedidos_dw']['motivo_reprovacao']   		=> '', // motivo_reprovacao = NULL (prospects)
				   $this->_db_cliente['campos']['pedidos_dw']['id_feira']  					=> $dados_pedido['id_feira'], // id_feira (id_evento)
				   $this->_db_cliente['campos']['pedidos_dw']['codigo_produto']  			=> $produto['codigo'], // cod_produto
				   $this->_db_cliente['campos']['pedidos_dw']['descricao_produto'] 			=> $produto['descricao'], // descricao_produto
				   $this->_db_cliente['campos']['pedidos_dw']['unidade_medida']  			=> ($produto['unidade_medida']?$produto['unidade_medida']:' '), // unidade_medida
				   $this->_db_cliente['campos']['pedidos_dw']['preco_unitario'] 			=> (float)number_format($produto['preco'], 5, '.', ''), // preco_unitario
				   $this->_db_cliente['campos']['pedidos_dw']['valor_total']  				=> (float)number_format(($produto['preco'] - (($produto['preco'] * $produto['desconto']) / 100)) * $produto['quantidade_pedido'], 5, '.', ''), // valor_total
				   $this->_db_cliente['campos']['pedidos_dw']['ipi'] 						=> (float)number_format(($produto['ipi']?$produto['ipi']:0), 5, '.', ''), // ipi
				   $this->_db_cliente['campos']['pedidos_dw']['desconto'] 					=> (float)number_format(($produto['desconto']?$produto['desconto']:0), 5, '.', ''), // desconto = NULL
				   $this->_db_cliente['campos']['pedidos_dw']['tabela_precos']  			=> ( is_array($produto['tabela_precos']) ? $produto['tabela_precos']['codigo'] : $produto['tabela_precos']), //$dados_pedido['cliente']['tabela_precos'],//['codigo_tabela_precos'], // codigo_tabela_precos
				   $this->_db_cliente['campos']['pedidos_dw']['peso_unitario']  			=> (float)number_format($produto['peso'], 5, '.', ''), // peso_unitario
				   //$this->_db_cliente['campos']['pedidos_dw']['substituicao_tributaria']  	=> number_format($produto['st'], 5, '.', ''), // subs_tributaria
				   $this->_db_cliente['campos']['pedidos_dw']['quantidade']					=> (float)number_format($produto['quantidade_pedido'], 5, '.', ''), // quantidade
				   $this->_db_cliente['campos']['pedidos_dw']['filial']						=> $produto['filial'],
				   //$this->_db_cliente['campos']['pedidos_dw']['tipo_pedido']				=> $dados_pedido['tipo_pedido'],
				   //$this->_db_cliente['campos']['pedidos_dw']['autorizado']					=> $dados_pedido['autorizado'],
				   //$this->_db_cliente['campos']['pedidos_dw']['cultura']					=> $dados_pedido['cultura'],
				   $this->_db_cliente['campos']['pedidos_dw']['modent']						=> $modent,
				   $this->_db_cliente['campos']['pedidos_dw']['delecao']					=> '',
				   $this->_db_cliente['campos']['pedidos_dw']['grupo_tributario_cliente']	=> $clientes['grupo_tributario'],
				   $this->_db_cliente['campos']['pedidos_dw']['estado_cliente']				=> $clientes['estado'],
				   $this->_db_cliente['campos']['pedidos_dw']['chave']						=> ($chave[$this->_db_cliente['campos']['pedidos_dw']['chave']] ? $chave[$this->_db_cliente['campos']['pedidos_dw']['chave']] + 1 : 1),
				   //CUSTOM E175/2014 - Edição de Orçamento - 03/04/2014
				   //Descrição normalização da forma de obter as colunas da tabela
				   $this->_db_cliente['campos']['pedidos_dw']['tipo_venda']	=> 'N',
				   $this->_db_cliente['campos']['pedidos_dw']['comissao']	=> (float)($representante['comissao'] ? $representante['comissao'] : 0), 
				   $this->_db_cliente['campos']['pedidos_dw']['codigo_representante_02']	=> $dados_pedido['codigo_usuario'],
				  // 'ZW_COMIS2' 	=> $representante['comissao'], coluna não existe
				   $this->_db_cliente['campos']['pedidos_dw']['tipo_cliente'] =>($clientes['tipo'] ? $clientes['tipo'] : ''),
				   $this->_db_cliente['campos']['pedidos_dw']['transportadora_redespacho'] => $dados_pedido['codigo_transportadora'],
				   $this->_db_cliente['campos']['pedidos_dw']['tes'] => $produto['tes'],//$nome_desconhecido['codigo'] não sabemos como retornar,
				   $this->_db_cliente['campos']['pedidos_dw']['cf'] => $produto['cf'],//$nome_desconhecido['cf'] não sabemos como retornar,
				   $this->_db_cliente['campos']['pedidos_dw']['substituicao_tributaria'] => $produto['st'],
				   $this->_db_cliente['campos']['pedidos_dw']['local'] => $produto_list['locpad'],
				   $this->_db_cliente['campos']['pedidos_dw']['vencimento_shazam'] => $dados_pedido['vencimento_shazam'] ? $dados_pedido['vencimento_shazam'] : '',
				   //FIM CUSTOM E175/2014 - Edição de Orçamento
				   
				   // custom 475
				   $this->_db_cliente['campos']['pedidos_dw']['restricao_shazam'] => $produto['restricao'],
				   // fim custom 475
				);
				
				// chamado 135
				$this->db->trans_begin();
				try{
					$this->db_cliente->insert($this->_db_cliente['tabelas']['pedidos_dw'], $dados); 
					if ($this->db->trans_status() === FALSE)
					{
						$this->db->trans_rollback();
						redirect('criar_pedidos/confirmacao/' . ($dados_pedido['tipo'] == 'orcamento' ? 'O' : 'P') . '/A tabela está trancada. Tente novamente em alguns instantes.');
					}
					else
					{
						$this->db->trans_commit();
					}
				}catch(Exception $e){
					redirect('criar_pedidos/confirmacao/' . ($dados_pedido['tipo'] == 'orcamento' ? 'O' : 'P'). '/A tabela está trancada.. Tente novamente em alguns instantes.');
				}
				// fim chamado 135
				
			}
			
		}

		return $id_pedido;
	}
	
	function editar_pedido($dados_pedido, $programado = FALSE)
	{
		//var_dump($dados_pedido); 
		if($dados_pedido)
		{			
			// Recebendo data de emissao
			$data_emissao = $this->db_cliente->select($this->_db_cliente['campos']['pedidos_dw']['data_emissao'])
											->from($this->_db_cliente['tabelas']['pedidos_dw'])
											->where($this->_db_cliente['campos']['pedidos_dw']['id_pedido'], $dados_pedido['id_pedido'])
											->where($this->_db_cliente['campos']['pedidos_dw']['tipo'],($dados_pedido['tipo'] == 'orcamento' ? 'O' : 'P'))		
											->limit(1)
											->get()->row_array();
			
			// chamado 135
				if($dados_pedido["copiando"] == '1')
				{
					$data_emissao[$this->_db_cliente['campos']['pedidos_dw']['data_emissao']] =  date('Ymd', time());
					$data_edicao = '';
				}
				else if($dados_pedido["editando"] == '1')
				{
					$data_emissao[$this->_db_cliente['campos']['pedidos_dw']['data_emissao']] = $dados_pedido['data_emissao'];
					$data_edicao = date('Ymd', time());
				}
				else{
					$data_emissao[$this->_db_cliente['campos']['pedidos_dw']['data_emissao']] =  date('Ymd', time());
					$data_edicao = '';
				}
			// fim chamado 135
			
			// Deletar Pedidos
			 $this->db_cliente->delete($this->_db_cliente['tabelas']['pedidos_dw'], array(
																		$this->_db_cliente['campos']['pedidos_dw']['id_pedido'] => $dados_pedido['id_pedido'],
																		$this->_db_cliente['campos']['pedidos_dw']['modent'] 	=> ($programado)?'P':'I',
																		$this->_db_cliente['campos']['pedidos_dw']['tipo'] 		=> ($dados_pedido['tipo'] == 'orcamento' ? 'O' : 'P')		
																		)); 
			//Caso seja uma edição e seja adicionado um novo item Imediato ou Programado irá adicionar como sendo um novo pedido / orçamento
			if($this->db_cliente->affected_rows()<= 0 ){				
				$dados_pedido['id_pedido'] =   $this->obter_idped($dados_pedido['codigo_usuario']);
			}
			//Caso seja uma edição 
			
			// Inserir Pedidos
			$produtos = $dados_pedido['produtos'];
			$_codido_cliente = explode('|', $dados_pedido['codigo_loja_cliente']);
			$codido_cliente = $_codido_cliente[0];
			$loja_cliente = $_codido_cliente[1];
			
			$modent = 'P'; // FORMA DE PEDIDO 'I' IMEDIATO OU 'P' PROGRAMADO
			if(!$programado)
			{
				$modent = 'I';
				$produtos = $dados_pedido['produtos'];
				$obs_pedido = ($dados_pedido['observacao_pedido_imediato'] ? $dados_pedido['observacao_pedido_imediato'] : 0);
				
				$data_entrega = date('Ymd',time());
			}
			else
			{
				$produtos = $dados_pedido['produtos_programados'];
				$obs_pedido = ($dados_pedido['observacao_pedido_programado'] ? $dados_pedido['observacao_pedido_programado'] : 0);
				
				$_data_entrega = explode('/', $dados_pedido['data_entrega']);
				$data_entrega = $_data_entrega[2] . $_data_entrega[1] . $_data_entrega[0];
			}

			$_data_entrega = explode('/', $dados_pedido['data_entrega']);
			$data_entrega = $_data_entrega[2] . $_data_entrega[1] . $_data_entrega[0];
		
			foreach($produtos as $produto)
			{
				
				$chave = $this->db_cliente->select($this->_db_cliente['campos']['pedidos_dw']['chave'])->from($this->_db_cliente['tabelas']['pedidos_dw'])->order_by($this->_db_cliente['campos']['pedidos_dw']['chave'], 'DESC')->get()->row_array();
				$representante = $this->db_cliente->select($this->_db_cliente['campos']['representantes']['comissao'])->from($this->_db_cliente['tabelas']['representantes'])->where($this->_db_cliente['campos']['representantes']['codigo'],$dados_pedido['codigo_usuario'])->get()->row_array();
				$clientes = $this->db_cliente->select($this->_db_cliente['campos']['clientes']['estado'].' AS estado')->select($this->_db_cliente['campos']['clientes']['tipo_cliente'].' AS grupo_tributario')->select($this->_db_cliente['campos']['clientes']['tipo'].' AS tipo')->from($this->_db_cliente['tabelas']['clientes'])->where($this->_db_cliente['campos']['clientes']['nome'], (string)$dados_pedido['nome_cliente'])->get()->row_array();
				//nao sabemos os campos a serem filtrados: $nome_desconhecido =  $this->db_cliente->select($this->_db_cliente['campos']['nome_desconhecido']['codigo'].', '.$this->_db_cliente['campos']['nome_desconhecido']['codigo'])->from($this->_db_cliente['tabelas']['nome_desconhecido'])->where($dados_pedido['codigo'])->get()->row_array();
				$produto_list =  $this->db_cliente->select($this->_db_cliente['campos']['produtos']['locpad'] .' AS locpad')->from($this->_db_cliente['tabelas']['produtos'])->where($this->_db_cliente['campos']['produtos']['codigo'],$produto['codigo'])->get()->row_array();
			
				$chaves = $key + 1;
			
				//chamado 238
					if($data_edicao == null){
						$data_edicao = '';
					}
				//fim chamado 238
			
				$dados = array(
				   $this->_db_cliente['campos']['pedidos_dw']['id_pedido']  						=> $dados_pedido['id_pedido'], // id_pedido (Código do Usuário / (Quantidade de pedidos do Usuário + 1))
				   $this->_db_cliente['campos']['pedidos_dw']['numero_item']						=> ''.$chaves.'',
				   $this->_db_cliente['campos']['pedidos_dw']['data_edicao']						=> $data_edicao,
				   $this->_db_cliente['campos']['pedidos_dw']['preco_venda']						=> number_format($produto['preco'] - (($produto['preco'] * $produto['desconto']) / 100), 5, '.', ''),
				   $this->_db_cliente['campos']['pedidos_dw']['preco_tabela']						=> number_format((float)$produto['preco'], 5, '.', ''),
				   $this->_db_cliente['campos']['pedidos_dw']['data_emissao'] 						=> ($data_emissao[$this->_db_cliente['campos']['pedidos_dw']['data_emissao']] ? $data_emissao[$this->_db_cliente['campos']['pedidos_dw']['data_emissao']] : date('Ymd',time())), // data_emissao
				   $this->_db_cliente['campos']['pedidos_dw']['id_usuario']  				 		=> $dados_pedido['codigo_usuario'], // id_usuario
				   $this->_db_cliente['campos']['pedidos_dw']['codigo_representante'] 				=> $dados_pedido['codigo_usuario'], // codigo_representante
				   $this->_db_cliente['campos']['pedidos_dw']['tipo'] 								=> ($dados_pedido['tipo'] == 'orcamento' ? 'O' : 'P'), // tipo
				   $this->_db_cliente['campos']['pedidos_dw']['status']								=> 'A', // status (<branco> = AGUARDANDO ANALISE)
				   $this->_db_cliente['campos']['pedidos_dw']['codigo_cliente']			 			=> $codido_cliente, // codigo_cliente
				   $this->_db_cliente['campos']['pedidos_dw']['loja_cliente']						=> ($loja_cliente ? $loja_cliente : ''), // loja_cliente
				   $this->_db_cliente['campos']['pedidos_dw']['nome_cliente']						=> ($dados_pedido['nome_cliente'] ? $dados_pedido['nome_cliente'] : ''),
				   $this->_db_cliente['campos']['pedidos_dw']['id_prospects'] 						=> $dados_pedido['id_prospect'], // id_prospect
				   $this->_db_cliente['campos']['pedidos_dw']['nome_prospects']						=> ($dados_pedido['nome_prospect'] ? $dados_pedido['nome_prospect'] : ''),
				   $this->_db_cliente['campos']['pedidos_dw']['condicao_pagamento']			 		=> $dados_pedido['codigo_forma_pagamento'], // condicao_pagamento
				   $this->_db_cliente['campos']['pedidos_dw']['tipo_frete']					=> substr($dados_pedido['tipo_frete'], 0, 1), // tipo_frete
				   $this->_db_cliente['campos']['pedidos_dw']['valor_frete']	 			=> (float)number_format($dados_pedido['valor_frete'], 5, '.', ''), // valor_frete
				   $this->_db_cliente['campos']['pedidos_dw']['codigo_transportadora'] 		=> $dados_pedido['codigo_transportadora'], // codigo_transportdora
				   $this->_db_cliente['campos']['pedidos_dw']['ordem_compra'] 	 			=> $dados_pedido['codigo_ordem_compra'], // ordem_compra
				   $this->_db_cliente['campos']['pedidos_dw']['data_entrega'] 	 			=> $data_entrega, // data_entrega
				   $this->_db_cliente['campos']['pedidos_dw']['obs_pedido']  				=> $dados_pedido['observacao_pedido_imediato'], // obs_pedido
				   $this->_db_cliente['campos']['pedidos_dw']['motivo_reprovacao']   		=> '', // motivo_reprovacao = NULL (prospects)
				   $this->_db_cliente['campos']['pedidos_dw']['id_feira']  					=> $dados_pedido['id_feira'], // id_feira (id_evento)
				   $this->_db_cliente['campos']['pedidos_dw']['codigo_produto']  			=> $produto['codigo'], // cod_produto
				   $this->_db_cliente['campos']['pedidos_dw']['descricao_produto'] 			=> $produto['descricao'], // descricao_produto
				   $this->_db_cliente['campos']['pedidos_dw']['unidade_medida']  			=> ($produto['unidade_medida']?$produto['unidade_medida']:' '), // unidade_medida
				   $this->_db_cliente['campos']['pedidos_dw']['preco_unitario'] 			=> (float)number_format($produto['preco'], 5, '.', ''), // preco_unitario
				   $this->_db_cliente['campos']['pedidos_dw']['valor_total']  				=> (float)number_format(($produto['preco'] - (($produto['preco'] * $produto['desconto']) / 100)) * $produto['quantidade_pedido'], 5, '.', ''), // valor_total
				   $this->_db_cliente['campos']['pedidos_dw']['ipi'] 						=> (float)number_format(($produto['ipi']?$produto['ipi']:0), 5, '.', ''), // ipi
				   $this->_db_cliente['campos']['pedidos_dw']['desconto'] 					=> (float)number_format(($produto['desconto']?$produto['desconto']:0), 5, '.', ''), // desconto = NULL
				    $this->_db_cliente['campos']['pedidos_dw']['tabela_precos']  			=> (is_array($produto['tabela_precos'])?$produto['tabela_precos']['codigo']:$produto['tabela_precos']), //$dados_pedido['cliente']['tabela_precos'],//['codigo_tabela_precos'], // codigo_tabela_precos
				   $this->_db_cliente['campos']['pedidos_dw']['peso_unitario']  			=> (float)number_format($produto['peso'], 5, '.', ''), // peso_unitario
				   //$this->_db_cliente['campos']['pedidos_dw']['substituicao_tributaria']  	=> number_format($produto['st'], 5, '.', ''), // subs_tributaria
				   $this->_db_cliente['campos']['pedidos_dw']['quantidade']					=> (float)number_format($produto['quantidade_pedido'], 5, '.', ''), // quantidade
				   $this->_db_cliente['campos']['pedidos_dw']['filial']						=> $produto['filial'],
				   $this->_db_cliente['campos']['pedidos_dw']['modent']						=> $modent,
				   $this->_db_cliente['campos']['pedidos_dw']['grupo_tributario_cliente']	=> $clientes['grupo_tributario'],
				   $this->_db_cliente['campos']['pedidos_dw']['estado_cliente']				=> $clientes['estado'],
				   //$this->_db_cliente['campos']['pedidos_dw']['tipo_pedido']				=> $dados_pedido['tipo_pedido'],
				   //$this->_db_cliente['campos']['pedidos_dw']['autorizado']					=> $dados_pedido['autorizado'],
				   //$this->_db_cliente['campos']['pedidos_dw']['cultura']					=> $dados_pedido['cultura'],
				   //$this->_db_cliente['campos']['pedidos_dw']['delecao']					=> '',
				   $this->_db_cliente['campos']['pedidos_dw']['chave']						=> ($chave[$this->_db_cliente['campos']['pedidos_dw']['chave']] ? $chave[$this->_db_cliente['campos']['pedidos_dw']['chave']] + 1 : 1),
				   //CUSTOM E175/2014 - Edição de Orçamento - 03/04/2014
				   //Descrição normalização da forma de obter as colunas da tabela
				   $this->_db_cliente['campos']['pedidos_dw']['tipo_venda']	=> 'N',
				   $this->_db_cliente['campos']['pedidos_dw']['comissao']	=> (float)($representante['comissao'] ? $representante['comissao'] : 0), 
				   $this->_db_cliente['campos']['pedidos_dw']['codigo_representante_02']	=> $dados_pedido['codigo_usuario'],
				  // 'ZW_COMIS2' 	=> $representante['comissao'], coluna não existe
				   $this->_db_cliente['campos']['pedidos_dw']['tipo_cliente'] => ($clientes['tipo'] ? $clientes['tipo'] : ''),
				   $this->_db_cliente['campos']['pedidos_dw']['transportadora_redespacho'] 	=> $dados_pedido['codigo_transportadora'],
				   $this->_db_cliente['campos']['pedidos_dw']['tes'] 	=> $produto['tes'],//$nome_desconhecido['codigo'] não sabemos como retornar,
				   $this->_db_cliente['campos']['pedidos_dw']['cf']		=> $produto['cf'],//$nome_desconhecido['cf'] não sabemos como retornar,
				   $this->_db_cliente['campos']['pedidos_dw']['substituicao_tributaria']	=> $produto['st'],
				   $this->_db_cliente['campos']['pedidos_dw']['local']	=> $produto_list['locpad'],
				   $this->_db_cliente['campos']['pedidos_dw']['vencimento_shazam'] => $dados_pedido['vencimento_shazam'] ? $dados_pedido['vencimento_shazam'] : '',
				   //FIM CUSTOM E175/2014 - Edição de Orçamento - 03/04/2014
				);
				
				$this->db_cliente->insert($this->_db_cliente['tabelas']['pedidos_dw'], $dados); 
				
			}
			
		}

		return $dados_pedido['id_pedido'];
	}
	
	// chamado 50
	function obterUltimoPedido($cod){
	
		$retorno =  $this->db_cliente
						->select_max($this->_db_cliente['campos']['pedidos_dw']['id_pedido'])
						->from($this->_db_cliente['tabelas']['pedidos_dw'])
						->like($this->_db_cliente['campos']['pedidos_dw']['id_pedido'], $cod.'-')
						
						->get()->row();
		
		return $retorno;
	}
	// fim chamado 50
	
	function obter_orcamento_conversao($id_pedido){
		$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['id_pedido'] , $id_pedido);
		$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['tipo'], 'O');
		$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['status'], STATUS_AGUARDANDO);
		
		$dados = $this->db_cliente
					->from($this->_db_cliente['tabelas']['pedidos_dw'])
					->get()->result_array();
					
				
					
		return $dados;
	}
	
	
	function transformar_pedido($id_pedido)
	{
	
		//Obtem os dados do Orçamento que será convertido em pedido.
	
		if($id_pedido)
		{
			//Obter orçamento 
			$orcamento = $this->obter_orcamento_conversao($id_pedido);
			
			//Converte o orçamento em pedido
			
			if(COUNT($orcamento) > 0){
				//Conversão do orçamento em pedido
				$dados = array(
				$this->_db_cliente['campos']['pedidos_dw']['tipo_pedido'] => 'P', 
				$this->_db_cliente['campos']['pedidos_dw']['tipo'] => 'P',
				$this->_db_cliente['campos']['pedidos_dw']['data_conversao'] => date('Ymd', time()),
				);

				$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['id_pedido'] , $id_pedido);
				$this->db_cliente->update($this->_db_cliente['tabelas']['pedidos_dw'] , $dados);
				
				//echo 'Itens do Orçamento: '.$this->db_cliente->affected_rows();
				
				
				foreach($orcamento as $item_orcamento){
					$chave = $this->db_cliente->select($this->_db_cliente['campos']['pedidos_dw']['chave'])->from($this->_db_cliente['tabelas']['pedidos_dw'])->order_by($this->_db_cliente['campos']['pedidos_dw']['chave'], 'DESC')->get()->row_array();
					$dados = $item_orcamento;
					
					$dados[$this->_db_cliente['campos']['pedidos_dw']['data_conversao']] = date('Ymd', time());
					$dados[$this->_db_cliente['campos']['pedidos_dw']['status']] = STATUS_CONVERTIDO_EM_PEDIDO;
					$dados[$this->_db_cliente['campos']['pedidos_dw']['chave']] = ($chave[$this->_db_cliente['campos']['pedidos_dw']['chave']] ? $chave[$this->_db_cliente['campos']['pedidos_dw']['chave']] + 1 : 1);
					
					$this->db_cliente->insert($this->_db_cliente['tabelas']['pedidos_dw'], $dados); 	
				
				}
				
			}
			
			
			//Converte orçamento em pedido.
			
			//Altera status do orçamento já convertido.
			
		
			
			
			return TRUE;
		}
	}
	
	function aprovar_pedido($id_pedido)
	{
		if($id_pedido)
		{
		
			$dados = array(
				$this->_db_cliente['campos']['pedidos_dw']['status']  => 'L',
				$this->_db_cliente['campos']['pedidos_dw']['data_aprovacao']  => date('Ymd', time())
			);

			$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['id_pedido'], $id_pedido);
			$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['tipo'], 'P');
			$this->db_cliente->update($this->_db_cliente['tabelas']['pedidos_dw'], $dados);
		
			return TRUE;
		}
	}
	
	
	function reprovar_pedido($id_pedido, $motivo)
	{
		if($id_pedido)
		{
		
			$dados = array($this->_db_cliente['campos']['pedidos_dw']['status'] => 'R', $this->_db_cliente['campos']['pedidos_dw']['motivo_reprovacao'] => $motivo);

			$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['id_pedido'], $id_pedido);
			$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['tipo'], 'P');
			$this->db_cliente->update($this->_db_cliente['tabelas']['pedidos_dw'], $dados);
		
			return TRUE;
		}
	}
	
	
	// ** TOP 10
	
	/*function obter_top_10_clientes($codigo_representante = NULL)
	{
		if ($codigo_representante)
		{
			$this->db_cliente->where('D2_VEND1', $codigo_representante);
		}
		
		$top_10_clientes = $this->db_cliente->select('D2_CLIENTE, D2_LOJA, SUM(D2_TOTAL) AS D2_TOTAL')->from('SD2' . $this->db_cliente->dbsuffix)->where('D_E_L_E_T_', '')->order_by('D2_TOTAL', 'desc')->group_by(array('D2_CLIENTE', 'D2_LOJA'))->limit(10)->get()->result();
		$_top_10_clientes = array();
		
		foreach ($top_10_clientes as $cliente)
		{
			$_top_10_clientes[] = array(
				'nome' => $this->obter_nome_cliente($cliente->D2_CLIENTE, $cliente->D2_LOJA),
				'valor_comprado' => trim($cliente->D2_TOTAL)
			);
		}
		
		return $_top_10_clientes;
	}
	
	function obter_top_10_produtos($codigo_representante = NULL)
	{
		if ($codigo_representante)
		{
			$this->db_cliente->where('D2_VEND1', $codigo_representante);
		}
		
		$top_10_produtos = $this->db_cliente->select('D2_COD, SUM(D2_TOTAL) AS D2_TOTAL')->from('SD2' . $this->db_cliente->dbsuffix)->where('D_E_L_E_T_', '')->order_by('D2_TOTAL', 'desc')->group_by('D2_COD')->limit(10)->get()->result();
		$_top_10_produtos = array();
		
		foreach ($top_10_produtos as $produto)
		{
			$_top_10_produtos[] = array(
				'descricao' => $this->obter_descricao_produto($produto->D2_COD),
				'valor_comprado' => trim($produto->D2_TOTAL)
			);
		}
		
		return $_top_10_produtos;
	}
	
	function obter_top_10_representantes()
	{
		$top_10_representantes = $this->db_cliente->select('D2_VEND1, SUM(D2_TOTAL) AS D2_TOTAL')->from('SD2' . $this->db_cliente->dbsuffix)->where('D_E_L_E_T_', '')->order_by('D2_TOTAL', 'desc')->group_by('D2_VEND1')->limit(10)->get()->result();
		$_top_10_representantes = array();
		
		foreach ($top_10_representantes as $representante)
		{
			$_top_10_representantes[] = array(
				'nome' => $this->obter_nome_representante($representante->D2_VEND1),
				'valor_vendido' => trim($representante->D2_TOTAL)
			);
		}
		
		return $_top_10_representantes;
	}
	
	function obter_top_10_clientes_inadimplentes($codigo_representante = NULL)
	{
		if ($codigo_representante)
		{
			$this->db_cliente->where('E1_VEND1', $codigo_representante);
		}
		
		$top_10_clientes_inadimplentes = $this->db_cliente->select('E1_CLIENTE, E1_LOJA, SUM(E1_VALOR) AS E1_VALOR')->from($this->_db_cliente['tabelas']['titulos'])->where(array('E1_VENCTO <' => date('Ymd'), 'E1_BAIXA' => '', 'D_E_L_E_T_' => ''))->order_by('E1_VALOR', 'desc')->group_by(array('E1_CLIENTE', 'E1_LOJA'))->limit(10)->get()->result();
		$_top_10_clientes_inadimplentes = array();
		
		foreach ($top_10_clientes_inadimplentes as $cliente)
		{
			$_top_10_clientes_inadimplentes[] = array(
				'nome' => $this->obter_nome_cliente($cliente->E1_CLIENTE, $cliente->E1_LOJA),
				'valor_inadimplente' => trim($cliente->E1_VALOR)
			);
		}
		
		return $_top_10_clientes_inadimplentes;
	}*/
	
	// TOP 10 **
	
	// ** VENDAS
	
	function obter_codigos_representantes()
	{
		$representantes = $this->db->select('codigo')->from('usuarios')->where('grupo', 'representantes')->get()->result();
		$_representantes = array();
		
		foreach ($representantes as $representante)
		{
			$_representantes[] = $representante->codigo;
		}
		
		return $_representantes;
	}
	
	function obter_total_vendas($codigo_representante = NULL, $data_inicial_timestamp = NULL, $data_final_timestamp = NULL)
	{
		$this->db_cliente->where_in($this->_db_cliente['campos']['pedidos']['codigo_representante'], $this->obter_codigos_representantes());
		
		if ($codigo_representante)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['pedidos']['codigo_representante'], $codigo_representante);
		}
		
		if ($data_inicial_timestamp)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['pedidos']['data_emissao'] . ' >=', date('Ymd', $data_inicial_timestamp));
		}
		
		if ($data_final_timestamp)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['pedidos']['data_emissao'] . ' <=', date('Ymd', $data_final_timestamp));
		}
		
		if ($this->_db_cliente['campos']['itens_pedidos']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['itens_pedidos'] . '.' . $this->_db_cliente['campos']['itens_pedidos']['delecao'], '');
		}
		
		if ($this->_db_cliente['campos']['pedidos']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['pedidos'] . '.' . $this->_db_cliente['campos']['pedidos']['delecao'], '');
		}
		
		if ($this->db_cliente->erp == 'protheus')
		{
			$this->db_cliente->where($this->_db_cliente['campos']['pedidos']['tipo'], 'N');
		}
		
		$total_vendas = $this->db_cliente->select('SUM(' . $this->_db_cliente['campos']['itens_pedidos']['valor_total_item'] . ')')->from($this->_db_cliente['tabelas']['itens_pedidos'])->join($this->_db_cliente['tabelas']['pedidos'], $this->_db_cliente['campos']['pedidos']['codigo'] . ' = ' . $this->_db_cliente['campos']['itens_pedidos']['codigo_pedido'])->get()->row();
		
		return $total_vendas->computed;
	}
	
	// VENDAS **
	
	// ** PREMIAÇÕES
	
	function obter_total_premiacao_vendas($codigo_representante = NULL, $data_inicial_timestamp = NULL, $data_final_timestamp = NULL)
	{
		if (!$this->_db_cliente['tabelas']['premiacoes'])
		{
			return 0;
		}
		
		$this->db_cliente->where_in($this->_db_cliente['campos']['premiacoes']['codigo_representante'], $this->obter_codigos_representantes());
		
		if ($codigo_representante)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['premiacoes']['codigo_representante'], $codigo_representante);
		}
		
		if ($data_inicial_timestamp)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['premiacoes']['data_emissao'] . ' >=', date('Ymd', $data_inicial_timestamp));
		}
		
		if ($data_final_timestamp)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['premiacoes']['data_emissao'] . ' <=', date('Ymd', $data_final_timestamp));
		}
		
		if ($this->_db_cliente['campos']['premiacoes']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['premiacoes']['delecao'], '');
		}
		
		$total_premiacao_vendas = $this->db_cliente->select('SUM(' . $this->_db_cliente['campos']['premiacoes']['valor'] . ')')->from($this->_db_cliente['tabelas']['premiacoes'])->get()->row();
		
		return $total_premiacao_vendas->computed;
	}
	
	// PREMIAÇÕES **
	
	// ** METAS
	
	function obter_total_meta_vendas($codigo_representante = NULL, $data_inicial_timestamp = NULL, $data_final_timestamp = NULL)
	{
		if (!$this->_db_cliente['tabelas']['metas'])
		{
			return 0;
		}
		
		if ($codigo_representante)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['metas']['codigo_representante'], $codigo_representante);
		}
		
		if ($data_inicial_timestamp)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['metas']['data'] . '', date('Ym', $data_inicial_timestamp));
		}
		
		if ($this->_db_cliente['campos']['metas']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['metas']['delecao'], '');
		}
		
		$total_meta_vendas = $this->db_cliente->select('SUM(' . $this->_db_cliente['campos']['metas']['valor'] . ')')->from($this->_db_cliente['tabelas']['metas'])->get()->row();
		
		return $total_meta_vendas->computed;
	}
	
	// METAS **
	
	// ** FORMAS DE PAGAMENTO
	
	function obter_forma_pagamento($codigo)
	{
		$campos = array('codigo', 'descricao');
		
		foreach ($campos as $campo)
		{
			$select[] = $this->_db_cliente['campos']['formas_pagamento'][$campo] . ' AS ' . $campo;
		}
		
		if ($this->_db_cliente['campos']['formas_pagamento']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['formas_pagamento']['delecao'], '');
		}
		
		return $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['formas_pagamento'])->where(array($this->_db_cliente['campos']['formas_pagamento']['codigo'] => $codigo))->get()->row_array();
	}
	
	function obter_formas_pagamento($codigo = NULL)
	{
		$campos = array('codigo', 'descricao');
		
		foreach ($campos as $campo)
		{
			$select[] = $this->_db_cliente['campos']['formas_pagamento'][$campo] . ' AS ' . $campo;
		}
		
		if ($this->_db_cliente['campos']['formas_pagamento']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['formas_pagamento']['delecao'], '');
		}
		
		if($this->_db_cliente['campos']['formas_pagamento']['descricao'])
		{
			$this->db_cliente->order_by($this->_db_cliente['campos']['formas_pagamento']['descricao']);
		}
		
		if($codigo)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['formas_pagamento']['codigo'], $codigo);
		}
		
		return $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['formas_pagamento'])->get()->result_array();
	}
	
	// FORMAS DE PAGAMENTO **
	
	// ** TRANSPORTADORAS
	
	function obter_transportadora($codigo)
	{
		$campos = array('codigo', 'nome', 'telefone');
		
		foreach ($campos as $campo)
		{
			$select[] = $this->_db_cliente['campos']['transportadoras'][$campo] . ' AS ' . $campo;
		}
		
		if ($this->_db_cliente['campos']['transportadoras']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['transportadoras']['delecao'], '');
		}
		
		return $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['transportadoras'])->where($this->_db_cliente['campos']['transportadoras']['codigo'], $codigo)->get()->row_array();

	}
	
	function obter_transportadoras($codigo = NULL)
	{
		$campos = array('codigo', 'nome', 'telefone', 'portal');
		
		foreach ($campos as $campo)
		{
			$select[] = $this->_db_cliente['campos']['transportadoras'][$campo] . ' AS ' . $campo;
		}
		
		if ($this->_db_cliente['campos']['transportadoras']['tipo'] && $this->db_cliente->erp == 'mercador')
		{
			$this->db_cliente->where($this->_db_cliente['campos']['transportadoras']['tipo'], 2);
		}
		
		if ($this->_db_cliente['campos']['transportadoras']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['transportadoras']['delecao'], '');
		}
		
		if($codigo)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['transportadoras']['codigo'], $codigo);
		}

		$this->db_cliente->where($this->_db_cliente['campos']['transportadoras']['portal'], 'S');
		$this->db_cliente->order_by($this->_db_cliente['campos']['transportadoras']['nome'], 'ASC');
		
		$transportadoras =  $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['transportadoras'])->get()->result_array();
		
		return $transportadoras;
	}
	
	// TRANSPORTADORAS **
	
	// ** TABELAS DE PREÇOS
	
	function obter_tabela_precos($codigo)
	{
		if (!$this->_db_cliente['tabelas']['tabelas_precos'])
		{
			return array('codigo' => 1, 'descricao' => 'Geral');
		}
		
		$campos = array('codigo', 'descricao');
		
		foreach ($campos as $campo)
		{
			$select[] = $this->_db_cliente['campos']['tabelas_precos'][$campo] . ' AS ' . $campo;
		}
		
		if ($this->_db_cliente['campos']['tabelas_precos']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['tabelas_precos']['delecao'], '');
		}
		
		return $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['tabelas_precos'])->where($this->_db_cliente['campos']['tabelas_precos']['codigo'], $codigo)->get()->row_array();
	}
	
	function obter_tabelas_precos()
	{
		if (!$this->_db_cliente['tabelas']['tabelas_precos'])
		{
			return array(0 => array('codigo' => 1, 'descricao' => 'Geral'));
		}
		
		$campos = array('codigo', 'descricao');
		
		foreach ($campos as $campo)
		{
			$select[] = $this->_db_cliente['campos']['tabelas_precos'][$campo] . ' AS ' . $campo;
		}
		
		if ($this->_db_cliente['campos']['tabelas_precos']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['tabelas_precos']['delecao'], '');
		}
		
		$this->db_cliente->order_by($this->_db_cliente['campos']['tabelas_precos']['descricao'], 'ASC');
		
		return $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['tabelas_precos'])->get()->result_array();
	}
	
	function obter_primeiro_item_tabela_precos()
	{
		if (!$this->_db_cliente['tabelas']['tabelas_precos'])
		{
			return array('codigo' => 1, 'descricao' => 'Geral');
		}
		
		$campos = array('codigo', 'descricao');
		
		foreach ($campos as $campo)
		{
			$select[] = $this->_db_cliente['campos']['tabelas_precos'][$campo] . ' AS ' . $campo;
		}
		
		if ($this->_db_cliente['campos']['tabelas_precos']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['tabelas_precos']['delecao'], '');
		}
		
		$this->db_cliente->limit(1);
		
		return $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['tabelas_precos'])->get()->row_array();
	}
	
	// TABELAS DE PREÇOS **
	
	// ** GRUPOS DE PRODUTOS
	
	function obter_grupos_produtos($somente_ativos = TRUE)
	{
		$campos = array('codigo', 'descricao');
		
		foreach ($campos as $campo)
		{
			$select[] = $this->_db_cliente['campos']['grupos_produtos'][$campo] . ' AS ' . $campo;
		}
		
		if ($this->_db_cliente['campos']['grupos_produtos']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['grupos_produtos']['delecao'], '');
		}
		
		if ($this->_db_cliente['campos']['grupos_produtos']['codigo_classe'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['grupos_produtos']['codigo_classe'], 11); // MENHDI
		}
		
		$grupos = $this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['grupos_produtos'])->order_by($this->_db_cliente['campos']['grupos_produtos']['codigo'], 'asc')->get()->result_array();
		
		if (!$somente_ativos)
		{
			return $grupos;
		}
		
		foreach ($grupos as $indice => $grupo) {
			if (!$this->db->from('grupos_produtos_ativos')->where('codigo', $grupo['codigo'])->get()->row())
			{
				unset($grupos[$indice]);
			}
		}
		
		return $grupos;
	}
	
	
	function obter_nome_grupo_produtos($codigo)
	{
		
		$representante = $this->db_cliente->select($this->_db_cliente['campos']['grupos_produtos']['descricao'])->from($this->_db_cliente['tabelas']['grupos_produtos'])->where(array($this->_db_cliente['campos']['grupos_produtos']['codigo'] => $codigo))->get()->row_array();
		
		return trim($representante[$this->_db_cliente['campos']['grupos_produtos']['descricao']]);
		
	}
	
	
	// GRUPOS DE PRODUTOS **
	
	// PRODUTOS **
	
	function obter_codigo_tabela_precos($codigo_filial = NULL, $tipo_sessao = NULL)
	{	
		
		if($this->session->userdata('sessao_pedido'))
		{
			$sessao = $this->session->userdata('sessao_pedido');
		}
		else if($this->session->userdata('sessao_orcamento'))
		{
			$sessao = $this->session->userdata('sessao_orcamento');
		}
		
		$codigo_tabela_precos = NULL;
		$uf_filial = $this->obter_uf_filial($codigo_filial);
		
		if($sessao['cliente']['tabela_precos'] == '203')
		{
			$codigo_tabela_precos = '203';
		}
		else
		{
			if($sessao['cliente']['tipo_cliente'] == 'XXX') 
			{
				$codigo_tabela_precos = '001';
			}
			if($sessao['cliente']['estado'] == $uf_filial['ZT_ESTENT'])
			{
				$codigo_tabela_precos = '001';
			}
			if( ($sessao['cliente']['tipo_cliente'] != 'XXX') && ($sessao['cliente']['estado'] != $uf_filial['ZT_ESTENT']) ) 
			{
				$codigo_tabela_precos = '301';
			}
		}
		/*
		else
		{
			$filial = $this->db_cliente
								->select($this->_db_cliente['campos']['filiais']['estado_filial'] . ' AS estado')
								->from($this->_db_cliente['tabelas']['filiais'])
								->where(
									array(
										$this->_db_cliente['campos']['filiais']['codigo_filial'] => $codigo_filial,
										$this->_db_cliente['campos']['filiais']['estado_filial'] => $sessao['cliente']['estado'],
										$this->_db_cliente['campos']['filiais']['status'] => 'A'
									)
								)
								->get()
								->row();
			debug_pre($filial, TRUE);
			if($filial)
			{
				$codigo_tabela_precos = '001';
			}
			else
			{
				$codigo_tabela_precos = '301';
			}
		}
		*/
		
		return $codigo_tabela_precos;
	}
	
	function obter_produto($codigo, $codigo_tabela_precos = NULL, $codigo_filial = NULL, $tipo_sessao = NULL)
	{
		$codigo_tabela_precos = $this->obter_codigo_tabela_precos($codigo_filial, $tipo_sessao);
		
		if ($this->_db_cliente['campos']['produtos']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['produtos']['delecao'], '');
		}
		
		$campos = array('codigo', 'codigo_real', 'descricao', 'unidade_medida', 'ipi', 'peso');
		
		foreach ($campos as $campo)
		{
			$select[] = $this->_db_cliente['campos']['produtos'][$campo] . ' AS ' . $campo;
		}
		
		$produto = $this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['produtos'])->where($this->_db_cliente['campos']['produtos']['codigo'], $codigo)->get()->row_array();
		
		if ($this->_db_cliente['tabelas']['produtos_tabelas_precos'])
		{
			if ($codigo_tabela_precos)
			{
				$this->db_cliente->where($this->_db_cliente['campos']['produtos_tabelas_precos']['codigo_tabela_precos'], $codigo_tabela_precos);
			}
			
			if ($this->_db_cliente['campos']['produtos_tabelas_precos']['delecao'])
			{
				$this->db_cliente->where($this->_db_cliente['campos']['produtos_tabelas_precos']['delecao'], '');
			}
			
			//$_produto = $this->db_cliente->select($this->_db_cliente['campos']['produtos_tabelas_precos']['preco'] . ', ' . $this->_db_cliente['campos']['produtos_tabelas_precos']['st'] . ', ' . $this->_db_cliente['campos']['produtos_tabelas_precos']['validacao_desconto'])->from($this->_db_cliente['tabelas']['produtos_tabelas_precos'])->where($this->_db_cliente['campos']['produtos_tabelas_precos']['codigo_produto'], $codigo)->get()->row_array();
			$_produto = $this->db_cliente->select($this->_db_cliente['campos']['produtos_tabelas_precos']['preco'] . ', ' . $this->_db_cliente['campos']['produtos_tabelas_precos']['st'] )->from($this->_db_cliente['tabelas']['produtos_tabelas_precos'])->where($this->_db_cliente['campos']['produtos_tabelas_precos']['codigo_produto'], $codigo)->get()->row_array();
			
			$preco = $_produto[$this->_db_cliente['campos']['produtos_tabelas_precos']['preco']];
			$st =  $_produto[$this->_db_cliente['campos']['produtos_tabelas_precos']['st']];
			//$desconto =  $_produto[$this->_db_cliente['campos']['produtos_tabelas_precos']['validacao_desconto']];
		}
		else
		{
			/*
			// se estivermos tentando obter o preço...
			if ($codigo_tabela_precos)
			{
				$preco = $this->obter_preco_produto($produto['codigo']);
			}
			*/
			
			$preco = 0;
		}
		
		//echo $this->db_cliente->last_query();
		
		$dados = array_map('trim', array(
			'codigo' => $produto['codigo'],
			'codigo_real' => $produto['codigo_real'],
			'descricao' => str_replace(',', ".", $produto['descricao']),
			'unidade_medida' => $produto['unidade_medida'],
			'ipi' => $produto['ipi'],
			'peso' => $produto['peso'],
			'preco' => $preco,
			'st' => $st,
			//'validacao_desconto' => $desconto,
			'quantidade_disponivel_estoque' => $this->obter_quantidade_disponivel_estoque_produto($produto['codigo'])
		));
		
		$dados = array_merge($dados, array(
			'tabela_precos' => $codigo_tabela_precos,
			'codigo_filial' => $codigo_filial
		));

		return $dados;
	}
	
	function obter_data_entrega_prevista_produto($codigo_produto, $quantidade)
	{
		$this->db_cliente->select($this->_db_cliente['campos']['itens_pedidos_compra']['quantidade'] . ' AS quantidade, ' . $this->_db_cliente['campos']['pedidos_compra']['data_entrega'] . ' AS data_entrega');
		
		$this->db_cliente->from($this->_db_cliente['tabelas']['itens_pedidos_compra']);
		$this->db_cliente->join($this->_db_cliente['tabelas']['pedidos_compra'], $this->_db_cliente['tabelas']['pedidos_compra'] . '.' . $this->_db_cliente['campos']['pedidos_compra']['codigo'] . ' = ' . $this->_db_cliente['tabelas']['itens_pedidos_compra'] . '.' . $this->_db_cliente['campos']['itens_pedidos_compra']['codigo_pedido']);
		
		$this->db_cliente->where($this->_db_cliente['campos']['itens_pedidos_compra']['codigo_produto'], $codigo_produto);
		$this->db_cliente->where($this->_db_cliente['campos']['pedidos_compra']['data_entrega'] . ' > GETDATE()');
		
		$this->db_cliente->order_by($this->_db_cliente['campos']['pedidos_compra']['data_entrega'], 'asc');
		
		$itens = $this->db_cliente->get()->result();
		
		$total = 0;
		
		foreach ($itens as $item)
		{
			$quantidade -= $item->quantidade;
			
			$total += $item->quantidade;
			
			if ($quantidade <= 0)
			{
				$data_entrega = strtotime($item->data_entrega);
				break;
			}
		}
		
		// obter a última data de entrega
		if (!$data_entrega)
		{
			$data_entrega = strtotime($itens[count($itens) - 1]->data_entrega);
		}
		
		return array($total, $data_entrega);
	}
	
	function obter_preco_produto($codigo_produto)
	{
		if ($this->_db_cliente['campos']['produtos_estoque']['ano'])
		{
			$this->db_cliente->order_by($this->_db_cliente['campos']['produtos_estoque']['ano'], 'desc');
		}
		
		if ($this->_db_cliente['campos']['produtos_estoque']['mes'])
		{
			$this->db_cliente->order_by($this->_db_cliente['campos']['produtos_estoque']['mes'], 'desc');
		}
		
		if ($this->_db_cliente['campos']['produtos_estoque']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['produtos_estoque']['delecao'], '');
		}
		
		$campos = array("codigo", "preco");
	
		foreach ($campos as $campo)
		{
			$select[] = $this->_db_cliente['campos']['produtos_estoque'][$campo] . ' AS ' . $campo;
		}
		
		$produto = $this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['produtos_estoque'])->where($this->_db_cliente['campos']['produtos_estoque']['codigo'], $codigo_produto)->get()->row_array();
		
		return $produto['preco'];
	}
	
	function obter_quantidade_disponivel_estoque_produto($codigo_produto, $unidade = NULL)
	{
		/*if ($this->_db_cliente['campos']['produtos_estoque']['ano'])
		{
			$this->db_cliente->order_by($this->_db_cliente['campos']['produtos_estoque']['ano'], 'desc');
			
			$this->db_cliente->group_by($this->_db_cliente['campos']['produtos_estoque']['ano']);
		}
		
		if ($this->_db_cliente['campos']['produtos_estoque']['mes'])
		{
			$this->db_cliente->order_by($this->_db_cliente['campos']['produtos_estoque']['mes'], 'desc');
			
			$this->db_cliente->group_by($this->_db_cliente['campos']['produtos_estoque']['mes']);
		}*/
		
		if ($this->_db_cliente['campos']['produtos_estoque']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['produtos_estoque']['delecao'], '');
		}
		
		/*if ($this->_db_cliente['campos']['produtos_estoque']['quantidade_disponivel'])
		{
			$campos = array('quantidade_disponivel');
		}
		else
		{
			$campos = array('quantidade_atual', 'quantidade_pedidos_venda', 'quantidade_empenhada', 'quantidade_reservada');
		}
		
		foreach ($campos as $campo)
		{
			$select[] = 'SUM(' . $this->_db_cliente['campos']['produtos_estoque'][$campo] . ') AS ' . $campo;
		}*/
		
		//Alteração,trocada a query de consulta mediante solicitação do cliente, realizada dia 15/04/14 - Darlan Borges
		$select[] = 'SUM(' . $this->_db_cliente['campos']['produtos_estoque_novo']['quantidade'] . '-' .$this->_db_cliente['campos']['produtos_estoque_novo']['quantidade_empenhada'].') AS  quantidade_disponivel_estoque';
		
		if($unidade)
		{
			/*$this->db_cliente->where($this->_db_cliente['campos']['produtos_estoque']['status'] . ' <>', '2');
			$this->db_cliente->where($this->_db_cliente['campos']['produtos_estoque']['local'], '01');
			
			$where_filial .= '( ';
			
			if($unidade == '01')
			{
				$where_filial .= $this->_db_cliente['campos']['produtos_estoque']['filial'] . ' = "01" OR ';
				$where_filial .= $this->_db_cliente['campos']['produtos_estoque']['filial'] . ' = "06"';
			}
			
			if($unidade == '02')
			{
				$where_filial .= $this->_db_cliente['campos']['produtos_estoque']['filial'] . ' = "01" OR ';
				$where_filial .= $this->_db_cliente['campos']['produtos_estoque']['filial'] . ' = "02" OR ';
				$where_filial .= $this->_db_cliente['campos']['produtos_estoque']['filial'] . ' = "06"';
			}
			
			if($unidade == '03')
			{
				$where_filial .= $this->_db_cliente['campos']['produtos_estoque']['filial'] . ' = "03"';
			}
			
			if($unidade == '04')
			{
				$where_filial .= $this->_db_cliente['campos']['produtos_estoque']['filial'] . ' = "04"';
			}
			
			if($unidade == '10')
			{
				$where_filial .= $this->_db_cliente['campos']['produtos_estoque']['filial'] . ' = "01" OR ';
				$where_filial .= $this->_db_cliente['campos']['produtos_estoque']['filial'] . ' = "06" OR ';
				$where_filial .= $this->_db_cliente['campos']['produtos_estoque']['filial'] . ' = "10"';
			}
			$where_filial .= ' )';
			
			$this->db_cliente->where($where_filial);*/
			
			$this->db_cliente->where($this->_db_cliente['campos']['produtos_estoque_novo']['filial'], $unidade);
		}
		$this->db_cliente->where($this->_db_cliente['campos']['produtos_estoque_novo']['local'], '01');
		$produto = $this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['produtos_estoque_novo'])->where($this->_db_cliente['campos']['produtos_estoque_novo']['codigo'], $codigo_produto)->get()->row_array();

		
		/*if ($this->_db_cliente['campos']['produtos_estoque']['quantidade_disponivel'])
		{
			$quantidade_disponivel_estoque = $produto['quantidade_disponivel'];
		}
		else
		{
			$quantidade_disponivel_estoque = $produto['quantidade_atual'] - ($produto['quantidade_pedidos_venda'] + $produto['quantidade_empenhada'] + $produto['quantidade_reservada']);
		}
		return $quantidade_disponivel_estoque;*/
		return $quantidade_disponivel_estoque = $produto['quantidade_disponivel_estoque'];
	}

	function obter_produtos_tabela_precos($palavras_chave = NULL, $codigo_grupo_produtos = NULL, $codigo_tabela_precos = NULL, $unidade = NULL)
	{
		/*
	//		SB1010.D_E_L_E_T_ = ''
	//		AND DA1010.D_E_L_E_T_ = ''
	//		AND SBF010.D_E_L_E_T_ != '*'
	//	  	AND SB1010.B1_MSBLQL != 1
	AND (UPPER(SB1010.B1_DESC) LIKE UPPER('% %')
			   OR UPPER(SB1010.B1_COD) LIKE UPPER('% %'))
	//  	AND SB1010.B1_COD = DA1010.DA1_CODPRO
	//	 	AND DA1010.DA1_CODTAB = '001'
	//		AND BF_LOCAL = '01'
		
	//	  AND (SBF010.BF_PRODUTO = SB1010.B1_COD)
		  
		  
	//	group by SB1010.B1_COD,  SB1010.B1_DESC,SB1010.B1_UM 
		*/
		
		$this->db_cliente
			//SELECT SB1010.B1_COD AS "codigo",
			->select($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['codigo'].' AS codigo')
			// SB1010.B1_COD AS "codigo_real",
			->select($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['codigo_real'].' AS codigo_real')
			// SB1010.B1_DESC AS"descricao",
			->select($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['descricao'].' AS descricao')
			//SB1010.B1_UM AS "unidade_medida",
			->select($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['unidade_medida'].' AS unidade_medida')
			// SUM(BF_QUANT-BF_EMPENHO) AS "quantidade_disponivel_estoque"
			->select('SUM('.$this->_db_cliente['campos']['produtos_estoque_novo']['quantidade'].' - '.$this->_db_cliente['campos']['produtos_estoque_novo']['quantidade_empenhada'].')'.' AS estoque_atual')
			// FROM SB1010,DA1010,SBF010			
			->from($this->_db_cliente['tabelas']['produtos'])
			->from($this->_db_cliente['tabelas']['produtos_tabelas_precos'])
			->from($this->_db_cliente['tabelas']['produtos_estoque_novo'])
			//tratando joins 
			->where($this->_db_cliente['tabelas']['produtos']. '.' .$this->_db_cliente['campos']['produtos']['codigo'] .' = '.$this->_db_cliente['tabelas']['produtos_tabelas_precos'].'.'.$this->_db_cliente['campos']['produtos_tabelas_precos']['codigo_produto'])
			//->where($this->_db_cliente['tabelas']['produtos']. '.' .$this->_db_cliente['campos']['produtos']['codigo'] .' = '.$this->_db_cliente['tabelas']['produtos_estoque_novo'].'.'.$this->_db_cliente['campos']['produtos_estoque_novo']['codigo'])			
				
			
			->where($this->_db_cliente['tabelas']['produtos']. '.' . $this->_db_cliente['campos']['produtos']['delecao'].' !=','*')
			->where($this->_db_cliente['tabelas']['produtos_tabelas_precos']. '.' . $this->_db_cliente['campos']['produtos_tabelas_precos']['delecao'].' !=','*')
			//CHAMADO 001895 - Produto sem estoque não está aparecendo no portal
			//->where($this->_db_cliente['tabelas']['produtos_estoque_novo']. '.' . $this->_db_cliente['campos']['produtos_estoque_novo']['delete'].' !=','*')
			->where($this->_db_cliente['tabelas']['produtos']. '.' . $this->_db_cliente['campos']['produtos']['inativo'].' !=','1')
			
			
			 //AND DA1010.DA1_CODTAB = '001'
			->where($this->_db_cliente['tabelas']['produtos_tabelas_precos']. '.' . $this->_db_cliente['campos']['produtos_tabelas_precos']['codigo_tabela_precos'],$codigo_tabela_precos)
			//CHAMADO 001895 - Produto sem estoque não está aparecendo no portal
			//->where($this->_db_cliente['tabelas']['produtos_estoque_novo']. '.' . $this->_db_cliente['campos']['produtos_estoque_novo']['local'],'01')
			
			
			->where("(UPPER(".$this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['descricao'].") LIKE UPPER('%".$palavras_chave."%')
			   OR UPPER(".$this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['codigo'].") LIKE UPPER('%".$palavras_chave."%'))")
			->group_by($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['codigo'])
			->group_by($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['descricao'])
			->group_by($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['unidade_medida'])
			;
		
		$produtos = $this->db_cliente->get()->result_array();
			
		return $produtos;
		
		
		/*
		
		$campos = array('codigo', 'codigo_real', 'descricao', 'unidade_medida');
		
		foreach ($campos as $campo)
		{
			$select[] = $this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos'][$campo] . ' AS ' . $campo;
		}
		
		//$select[] = $this->_db_cliente['tabelas']['produtos_tabelas_precos'] . '.' . $this->_db_cliente['campos']['produtos_tabelas_precos']['validacao_desconto'] . ' AS validacao_desconto';
		
		if ($this->_db_cliente['campos']['produtos']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['delecao'], '');
		}
		
		if ($this->_db_cliente['tabelas']['produtos_tabelas_precos']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['produtos_tabelas_precos'] . '.' . $this->_db_cliente['campos']['produtos_tabelas_precos']['delecao'], '');
		}
		
		if ($this->db_cliente->erp == 'protheus')
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['inativo'] . ' !=', 1);
		}
		
		if($palavras_chave)
		{
			$this->db_cliente->where('(UPPER(' . $this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['descricao'] . ') LIKE UPPER("%' . $palavras_chave . '%") OR UPPER(' . $this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['codigo'] . ') LIKE UPPER("%' . $palavras_chave . '%"))');
		}
		
		if ($codigo_grupo_produtos)
		{
			// (String) = fix
			$this->db_cliente->where($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['codigo_grupo'], (String)$codigo_grupo_produtos);
		}
		
		if ($this->_db_cliente['campos']['produtos']['disponivel_venda'])
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['disponivel_venda'] . ' !=', 1);
		}
		
		if ($this->_db_cliente['campos']['produtos']['codigo_classe'] && $this->db_cliente->erp == 'mercador')
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['codigo_classe'], 11);
		}
		
		//Associando tabelas
		$this->db_cliente->where($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['codigo'] . ' = ' . $this->_db_cliente['tabelas']['produtos_tabelas_precos'] . '.' . $this->_db_cliente['campos']['produtos_tabelas_precos']['codigo_produto']);
		
		if($codigo_tabela_precos)
		{
			
			$this->db_cliente->where($this->_db_cliente['tabelas']['produtos_tabelas_precos'] . '.' . $this->_db_cliente['campos']['produtos_tabelas_precos']['codigo_tabela_precos'], $codigo_tabela_precos);
		}
		
		$produtos = $this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['produtos'] . ', ' . $this->_db_cliente['tabelas']['produtos_tabelas_precos'])->get()->result_array();
		//var_dump($produtos);
		
		
		
		
		foreach ($produtos as &$produto)
		{
			$produto['estoque_atual'] = $this->obter_quantidade_disponivel_estoque_produto($produto['codigo'], $unidade);
		}
		
		//$produtos['last_query'] = $this->db_cliente->last_query();
		//echo $this->db_cliente->last_query();
		return $produtos;
		*/
	}
	
	function obter_produtos($codigo_grupo_produtos = NULL)
	{
		$campos = array('codigo', 'codigo_real', 'descricao', 'unidade_medida', 'peso');
		
		foreach ($campos as $campo)
		{
			$select[] = $this->_db_cliente['campos']['produtos'][$campo] . ' AS ' . $campo;
		}
		
		if ($this->_db_cliente['campos']['produtos']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['produtos']['delecao'], '');
		}
		
		if ($this->db_cliente->erp == 'protheus')
		{
			$this->db_cliente->where($this->_db_cliente['campos']['produtos']['inativo'] . ' !=', 1);
		}
		
		if ($codigo_grupo_produtos)
		{
			// (String) = fix
			$this->db_cliente->where($this->_db_cliente['campos']['produtos']['codigo_grupo'], (String)$codigo_grupo_produtos);
		}
		
		if ($this->_db_cliente['campos']['produtos']['disponivel_venda'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['produtos']['disponivel_venda'] . ' !=', 1);
		}
		
		if ($this->_db_cliente['campos']['produtos']['codigo_classe'] && $this->db_cliente->erp == 'mercador')
		{
			$this->db_cliente->where($this->_db_cliente['campos']['produtos']['codigo_classe'], 11);
		}
		
		$produtos = $this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['produtos'])->get()->result_array();
		
		foreach ($produtos as &$produto)
		{
			$produto['estoque_atual'] = $this->obter_quantidade_disponivel_estoque_produto($produto['codigo']);
		}
		
		return $produtos;
	}
	
	function obter_descricao_produto($codigo)
	{
		if ($this->_db_cliente['campos']['produtos']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['produtos']['delecao'], '');
		}
		
		$produto = $this->db_cliente->select($this->_db_cliente['campos']['produtos']['descricao'])->from($this->_db_cliente['tabelas']['produtos'])->where($this->_db_cliente['campos']['produtos']['codigo'], $codigo)->get()->row_array();
		
		return trim($produto[$this->_db_cliente['campos']['produtos']['descricao']]);
	}
	
	function obter_opcional_produto($codigo)
	{
		if (!$this->_db_cliente['tabelas']['opcionais_produtos'])
		{
			return array();
		}
		
		if ($this->_db_cliente['campos']['opcionais_produtos']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['opcionais_produtos']['delecao'], '');
		}
		
		$opcional_produto = $this->db_cliente->select(array($this->_db_cliente['campos']['opcionais_produtos']['codigo'], $this->_db_cliente['campos']['opcionais_produtos']['descricao']))->from($this->_db_cliente['tabelas']['opcionais_produtos'])->where($this->_db_cliente['campos']['opcionais_produtos']['codigo'], $codigo)->get()->row_array();
		
		$dados = array_map('trim', array(
			'codigo' => $opcional_produto[$this->_db_cliente['campos']['opcionais_produtos']['codigo']],
			'descricao' => $opcional_produto[$this->_db_cliente['campos']['opcionais_produtos']['descricao']]
		));
		
		return $dados;
	}
	
	function obter_opcionais_produto($codigo_produto = NULL)
	{
		if (!$this->_db_cliente['tabelas']['opcionais_produtos'])
		{
			return array();
		}
		
		$campos = array('codigo', 'descricao');
		
		foreach ($campos as $campo)
		{
			$select[] = $this->_db_cliente['campos']['opcionais_produtos'][$campo] . ' AS ' . $campo;
		}
		
		if ($this->_db_cliente['campos']['opcionais_produtos']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['opcionais_produtos']['delecao'], '');
		}
		
		if ($codigo_produto)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['opcionais_produtos']['codigo_produto'], $codigo_produto);
		}
		
		return $this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['opcionais_produtos'])->get()->result_array();
	}
	
	// ** PRODUTOS
	
	// ** CLIENTES
	
	function obter_peso_total_pedido($id_pedido = NULL, $codigo_pedido = NULL)
	{
		if ($codigo_pedido)
		{
			$select[] = $this->_db_cliente['campos']['itens_pedidos']['codigo_produto'];
			$select[] = $this->_db_cliente['campos']['itens_pedidos']['unidade_medida_produto'];
			$select[] = $this->_db_cliente['campos']['itens_pedidos']['quantidade_vendida_produto'];
			
			$itens_pedido = $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['itens_pedidos'])->where($this->_db_cliente['campos']['itens_pedidos']['codigo_pedido'], $codigo_pedido)->get()->result_array();
			
			foreach ($itens_pedido as $item_pedido)
			{
				$unidade_medida = $item_pedido[$this->_db_cliente['campos']['itens_pedidos']['unidade_medida_produto']];
				$quantidade_vendida = $item_pedido[$this->_db_cliente['campos']['itens_pedidos']['quantidade_vendida_produto']];
				
				$produto = $this->obter_produto($item_pedido[$this->_db_cliente['campos']['itens_pedidos']['codigo_produto']]);
				$conversao = $produto['conversao'];
				$conversao = str_replace(',', '.', $conversao);
				
				$peso_total += strtolower($unidade_medida) == 'kg' ? $quantidade_vendida : $quantidade_vendida * $conversao;
			}
		}
		else
		{
			$itens_pedido = $this->db->from('itens_pedidos')->where('id_pedido', $id_pedido)->order_by('id', 'asc')->get()->result();
			
			foreach ($itens_pedido as $item_pedido)
			{
				$unidade_medida = $item_pedido->unidade_medida;
				$quantidade_vendida = $item_pedido->quantidade;
				
				$produto = $this->obter_produto($item_pedido->codigo);
				$conversao = $produto['conversao'];
				$conversao = str_replace(',', '.', $conversao);
				
				$peso_total += strtolower($unidade_medida) == 'kg' ? $quantidade_vendida : $quantidade_vendida * $conversao;
			}
		}
		
		return $peso_total;
	}
	
	function obter_cliente($codigo, $loja)
	{
		$campos = array(
            'codigo', 'loja', 'tipo', 'nome', 'cpf', 'pessoa_contato', 'endereco', 
            'numero', 'bairro', 'cep', 'cidade', 'estado', 'ddd', 'telefone', 'email', 
            'limite_credito', 'total_titulos_em_aberto', 'pedidos_liberados', 
            'codigo_representante', 'data_cadastro', 'tabela_precos', 
            'categoria', 'tipo_cliente', 'cliente_sativ1'
        );

		foreach ($campos as $campo)
		{
			if (!$this->_db_cliente['campos']['clientes'][$campo]) continue;
			
			$select[] = $this->_db_cliente['campos']['clientes'][$campo] . ' AS ' . $campo;
		}
		
		if ($this->_db_cliente['campos']['clientes']['delecao'])
		{
			$this->db_cliente->where("{$this->_db_cliente['tabelas']['clientes']}.{$this->_db_cliente['campos']['clientes']['delecao']}", '');
		}
		
		$cliente = $this->db_cliente
						->select($select)
						->from($this->_db_cliente['tabelas']['clientes'])
						->where(array(
							$this->_db_cliente['campos']['clientes']['codigo'] => $codigo, 
							$this->_db_cliente['campos']['clientes']['loja'] => $loja
						))
						->get()
						->row();
		
		$ultimo_pedido = $this->obter_ultimo_pedido_cliente($codigo, $loja);
		
		$dados = array_map('trim', array(
			'status' => time() - $ultimo_pedido['emissao_timestamp'] <= 3600 * 24 * 180 ? 'Ativo' : 'Inativo',
			'tipo' => $cliente->tipo,
			'codigo' => $cliente->codigo,
			'loja' => $cliente->loja,
			'nome' => $cliente->nome,
			'cpf' => $cliente->cpf,
			'pessoa_contato' => $cliente->pessoa_contato,
			'endereco' => $cliente->endereco,
			'numero' => $cliente->numero,
			'bairro' => $cliente->bairro,
			'cep' => $cliente->cep,
			'cidade' => $cliente->cidade,
			'estado' => $cliente->estado,
			'ddd' => $cliente->ddd,
			'telefone' => $cliente->telefone,
			'email' => $cliente->email,
			'rg' => $cliente->rg,
			'limite_credito' => $cliente->limite_credito,
			'total_titulos_em_aberto' => $cliente->total_titulos_em_aberto,
			'pedidos_liberados' => $cliente->pedidos_liberados,
			'codigo_representante' => $cliente->codigo_representante,
			'data_cadastro_timestamp' => strtotime($cliente->data_cadastro),
			'tabela_precos' => $cliente->tabela_precos,
			'categoria' => $cliente->categoria,
			'tipo_cliente' => $cliente->tipo_cliente,
			'cliente_xtpcli' => $cliente->cliente_xtpcli,
			'cliente_sativ1' => $cliente->cliente_sativ1
		));
		
		$dados = array_merge($dados, array(
			'ultimo_pedido' => $ultimo_pedido,
			'total_titulos_vencidos' => $this->obter_total_titulos_vencidos_cliente($codigo, $loja),
			'descricao_forma_pagamento' => $ultimo_pedido['descricao_forma_pagamento'],
			'codigo_pedido' => $ultimo_pedido['codigo_pedido']
		));
		
		
		/* Permite inclusão de Formulário de visitas 	*/
		$codigo_usuario = '';
		if(!in_array(trim($this->session->userdata('grupo_usuario')), array('gestores_comerciais'))){
			$codigo_usuario = trim($this->session->userdata('codigo_usuario'));
		} else {
			$codigo_usuario = $cliente->codigo_representante;
		}
			
			
		$dados_cliente = $this->db_cliente			
		->from("GET_CLI_VEN('".$codigo_usuario."')")
		->where($this->_db_cliente['campos']['clientes']['delecao'].' != ', '*')
		->where(array(
					$this->_db_cliente['campos']['clientes']['codigo'] => $codigo, 
					$this->_db_cliente['campos']['clientes']['loja'] => $loja
				))
		
		->get()
		->row_array();
		
		$permite_inclusao_formulario = 'N';
		
		
		
		if(isset($dados_cliente['OKFORM']) && $dados_cliente['OKFORM'] == 'S'  ){
			$permite_inclusao_formulario = 'S';
		}
		
		$dados['status_formulario'] = $dados_cliente['STATUS'];
		
		$dados['permite_inclusao_formulario'] = $permite_inclusao_formulario;
		return $dados;
	}
	
	// criada para a consulta de títulos vencidos
	// obter todas as informações do cliente consumiria muito processamento
	function obter_nome_cliente($codigo, $loja)
	{
		if ($this->_db_cliente['campos']['clientes']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['clientes']['delecao'], '');
		}
		
		$cliente = $this->db_cliente->select($this->_db_cliente['campos']['clientes']['nome'])->from($this->_db_cliente['tabelas']['clientes'])->where(array($this->_db_cliente['campos']['clientes']['codigo'] => $codigo, $this->_db_cliente['campos']['clientes']['loja'] => $loja))->get()->row_array();
		
		return trim($cliente[$this->_db_cliente['campos']['clientes']['nome']]);
	}
	
	function obter_nome_produto($cod_produto)
	{
		if ($this->_db_cliente['campos']['produtos']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['produtos']['delecao'], '');
		}
		
		$produto = $this->db_cliente->select($this->_db_cliente['campos']['produtos']['descricao'])->from($this->_db_cliente['tabelas']['produtos'])->where(array($this->_db_cliente['campos']['produtos']['codigo'] => $cod_produto))->get()->row_array();
		
		return trim($produto[$this->_db_cliente['campos']['produtos']['descricao']]);
	}
	
	
	function criar_cliente($campos = array())
	{
		$_campos = array_keys($campos);
		
		foreach ($_campos as $campo)
		{
			// se o campo não existe na configuração, vamos pular
			if (!$this->_db_cliente['campos']['clientes'][$campo]) continue;
			
			// endereco
			if ($campo == 'endereco' && !$this->_db_cliente['campos']['clientes']['numero'])
			{
				$valor = $campos[$campo] . ', ' . $campos['numero'];
			}
			
			// específico do protheus
			if ($this->db_cliente->erp == 'protheus') {
				// tipo de pessoa
				if ($campo == 'tipo_pessoa')
				{
					$valor = ($campos[$campo] == 'pessoa_fisica') ? 'F' : 'J';
				}
			}
			
			if (!$valor) {
				$valor = $campos[$campo];
			}
			
			// assimilar valores
			$dados[$this->_db_cliente['campos']['clientes'][$campo]] = $valor;
			
			unset($valor);
		}
		
		// data
		if ($this->_db_cliente['campos']['clientes']['data_cadastro'])
		{
			$dados[$this->_db_cliente['campos']['clientes']['data_cadastro']] = date('Ymd');
		}
	
		// obter último código
		$ultimo_codigo = $this->db_cliente->select($this->_db_cliente['campos']['clientes']['codigo'])->from($this->_db_cliente['tabelas']['clientes'])->order_by($this->_db_cliente['campos']['clientes']['chave_primaria'], 'desc')->limit(1)->get()->row_array();
		$dados[$this->_db_cliente['campos']['clientes']['codigo']] = $ultimo_codigo[$this->_db_cliente['campos']['clientes']['codigo']] + 1;
		
		// obter última chave primária
		$ultima_chave_primaria = $this->db_cliente->select($this->_db_cliente['campos']['clientes']['chave_primaria'])->from($this->_db_cliente['tabelas']['clientes'])->order_by($this->_db_cliente['campos']['clientes']['chave_primaria'], 'desc')->limit(1)->get()->row_array();
		$dados[$this->_db_cliente['campos']['clientes']['chave_primaria']] = $ultima_chave_primaria[$this->_db_cliente['campos']['clientes']['chave_primaria']] + 1;
		
		// loja
		if ($this->_db_cliente['campos']['clientes']['loja'])
		{
			$dados[$this->_db_cliente['campos']['clientes']['loja']] = '01';
		}
		
		$this->db_cliente->insert($this->_db_cliente['tabelas']['clientes'], $dados);
		
		return array('codigo' => $dados[$this->_db_cliente['campos']['clientes']['codigo']], 'loja' => $dados[$this->_db_cliente['campos']['clientes']['loja']] ? $dados[$this->_db_cliente['campos']['clientes']['loja']] : $dados[$this->_db_cliente['campos']['clientes']['codigo']]);
	}
	
	function obter_ultimo_pedido_cliente($codigo, $loja)
	{
		if ($this->_db_cliente['campos']['pedidos']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['pedidos']['delecao'], '');
		}
		$ultimo_pedido = $this->db_cliente->select(
				array(
					$this->_db_cliente['campos']['pedidos']['filial'], 
					$this->_db_cliente['campos']['pedidos']['data_emissao'], 
					$this->_db_cliente['campos']['pedidos']['codigo_forma_pagamento'], 
					$this->_db_cliente['campos']['pedidos']['codigo'])
					)
			->from($this->_db_cliente['tabelas']['pedidos'])
			->where(array($this->_db_cliente['campos']['pedidos']['codigo_cliente'] => $codigo, $this->_db_cliente['campos']['pedidos']['loja_cliente'] => $loja))
			->order_by($this->_db_cliente['campos']['pedidos']['data_emissao'], 'desc')
			->limit(1)->get()->row_array();
		
		if ($this->_db_cliente['campos']['itens_pedidos']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['itens_pedidos']['delecao'], '');
		}
		$_ultimo_pedido = $this->db_cliente->select(
				'SUM(' . $this->_db_cliente['campos']['itens_pedidos']['valor_total_item'] . ') AS valor_total, 
				SUM(' . $this->_db_cliente['campos']['itens_pedidos']['valor_total_desconto_item'] . ') AS valor_total_desconto')
			->from($this->_db_cliente['tabelas']['itens_pedidos'])
			->where(
					array(
						$this->_db_cliente['campos']['itens_pedidos']['codigo_pedido'] => $ultimo_pedido[$this->_db_cliente['campos']['pedidos']['codigo']],
						$this->_db_cliente['campos']['itens_pedidos']['filial'] => $ultimo_pedido['C5_FILIAL']
					)
				)
			->get()->row();
		
		$forma_pagamento = $this->obter_forma_pagamento($ultimo_pedido[$this->_db_cliente['campos']['pedidos']['codigo_forma_pagamento']]);
		
		return array_map('trim', array(
			'emissao_timestamp' => strtotime($ultimo_pedido[$this->_db_cliente['campos']['pedidos']['data_emissao']]),
			'codigo_pedido' => $ultimo_pedido[$this->_db_cliente['campos']['pedidos']['codigo']],
			'filial' => $ultimo_pedido[$this->_db_cliente['campos']['pedidos']['filial']],
			'valor' => $_ultimo_pedido->valor_total,
			'valor_desconto' => $_ultimo_pedido->valor_total_desconto,
			'descricao_forma_pagamento' => $forma_pagamento['descricao']
		));
	}
	
	function obter_total_titulos_vencidos_cliente($codigo, $loja)
	{
		if ($this->_db_cliente['campos']['titulos']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['titulos']['delecao'], '');
		}
		$titulos_vencidos = $this->db_cliente->select('SUM(' . $this->_db_cliente['campos']['titulos']['valor'] . ')')->from($this->_db_cliente['tabelas']['titulos'])->where(array($this->_db_cliente['campos']['titulos']['codigo_cliente'] => $codigo, $this->_db_cliente['campos']['titulos']['loja_cliente'] => $loja, $this->_db_cliente['campos']['titulos']['data_vencimento'] . ' <' => date('Ymd'), $this->_db_cliente['campos']['titulos']['data_baixa'] => ''))->get()->row();
		
		return $titulos_vencidos->computed;
	}
	
	function obter_clientes($codigo_representante = NULL, $estado = NULL)
	{
		if ($codigo_representante) {
			$this->db_cliente->where($this->_db_cliente['campos']['clientes']['codigo_representante'], $codigo_representante);
		}
		
		if ($estado) {
			$this->db_cliente->where($this->_db_cliente['campos']['clientes']['estado'], $estado);
		}
		
		$campos = array('codigo', 'loja', 'nome', 'nome_fantasia', 'cpf', 'telefone', 'estado');
		
		foreach ($campos as $campo)
		{
			$select[] = $this->_db_cliente['campos']['clientes'][$campo] . ' AS ' . $campo;
		}
		
		if ($this->_db_cliente['campos']['clientes']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['clientes']['delecao'], '');
		}
		
		return $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['clientes'])->get()->result_array();
	}
	
	function obter_estados()
	{
		$campos = array('estado');
		
		foreach ($campos as $campo)
		{
			$select[] = $this->_db_cliente['campos']['clientes'][$campo] . ' AS ' . $campo;
		}
		
		if ($this->_db_cliente['campos']['clientes']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['clientes']['delecao'], '');
		}
		
		return $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['clientes'])->get()->result_array();
		
	}
	
	function obter_grupo_estados()
	{
		$campos = array('estado');
		
		foreach ($campos as $campo)
		{
			$select[] = $this->_db_cliente['campos']['clientes'][$campo] . ' AS ' . $campo;
		}
		
		if ($this->_db_cliente['campos']['clientes']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['clientes']['delecao'], '');
		}
		
		$estados = $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['clientes'])->group_by($this->_db_cliente['campos']['clientes']['estado'])->get()->result_array();
		
		
		$_estados = array();
		
		foreach($estados as $estado)
		{
			$_estados[$estado['estado']] = ' ' . $estado['estado'];
		}
		
		return $_estados;
		
	}
	
	function obter_total_clientes_criados($codigo_representante = NULL, $data_inicial_timestamp = NULL, $data_final_timestamp = NULL)
	{
		$this->db_cliente->where_in($this->_db_cliente['campos']['clientes']['codigo_representante'], $this->obter_codigos_representantes());
		
		if ($codigo_representante) {
			$this->db_cliente->where($this->_db_cliente['campos']['clientes']['codigo'], $codigo_representante);
		}
		
		if ($data_inicial_timestamp) {
			$this->db_cliente->where($this->_db_cliente['campos']['clientes']['data_primeira_compra'] . ' >=', date('Ymd', $data_inicial_timestamp));
		}
		
		if ($data_final_timestamp) {
			$this->db_cliente->where($this->_db_cliente['campos']['clientes']['data_primeira_compra'] . ' <=', date('Ymd', $data_final_timestamp));
		}
		
		return $this->db_cliente->from($this->_db_cliente['tabelas']['clientes'])->get()->num_rows();
	}
	
	function obter_tabela_preco_descricao($cod_tabela_precos)
	{
		$descricao = $this->db_cliente->select($this->_db_cliente['campos']['tabelas_precos']['descricao']." as descricao")
			->from($this->_db_cliente['tabelas']['tabelas_precos'])
			->where($this->_db_cliente['campos']['tabelas_precos']['codigo'].' = '.$cod_tabela_precos.' 
				AND '.$this->_db_cliente['campos']['tabelas_precos']['delecao']." <> '*'")
			->get()->row()->descricao;
		
		return($descricao);
	}
	
	function obter_tabela_preco($codigo, $loja)
	{
		
		/*$campos = array('tabela_frete');
		
		foreach ($campos as $campo)
		{
			$select[] = $this->_db_cliente['campos']['clientes'][$campo] . ' AS ' . $campo;
		}
		*/
		
		if ($this->_db_cliente['campos']['clientes']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['clientes']['delecao'], '');
		}
		
		//return $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['clientes'])->where(array($this->_db_cliente['campos']['clientes']['codigo'] => $codigo, $this->_db_cliente['campos']['clientes']['loja'] => $loja))->get()->row_array();
		return $this->db_cliente->from($this->_db_cliente['tabelas']['clientes'])->where(array($this->_db_cliente['campos']['clientes']['codigo'] => $codigo, $this->_db_cliente['campos']['clientes']['loja'] => $loja))->get()->row_array();
	}
	
	// CLIENTES **
	
	// ** REPRESENTANTES
	
	function obter_representante($codigo)
	{
		$campos = array('codigo', 'nome', 'cpf', 'endereco', 'bairro', 'cep', 'cidade', 'estado', 'telefone', 'email', 'delecao', 'grupo', 'categoria');//, 'tabela');
		
		foreach ($campos as $campo)
		{
			if($this->_db_cliente['campos']['representantes'][$campo])
			{
				$select[] = $this->_db_cliente['campos']['representantes'][$campo] . ' AS ' . $campo;
			}
		}
		
		if ($this->_db_cliente['campos']['representantes']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['representantes']['delecao'], '');
		}
		//echo 'CODIGO:'.$codigo;
		$dados =  $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['representantes'])->where(array($this->_db_cliente['campos']['representantes']['codigo'] => $codigo))->get()->row_array();
	
		return $dados;
	}
	
	function obter_representantes()
	{
		$campos = array('codigo', 'nome');
		
		foreach ($campos as $campo)
		{
			$select[] = $this->_db_cliente['campos']['representantes'][$campo] . ' AS ' . $campo;
		}
		
		if ($this->_db_cliente['campos']['representantes']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['representantes']['delecao'] . ' <>', '*');
		}
		
		$this->db_cliente->order_by($this->_db_cliente['campos']['representantes']['nome']);
		
		$retorno  = $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['representantes'])->get()->result_array();
		
		
		
		return $retorno;
	}
	
	function obter_nome_representante($codigo)
	{
		$representante = $this->db_cliente->select($this->_db_cliente['campos']['representantes']['nome'])->from($this->_db_cliente['tabelas']['representantes'])->where(array($this->_db_cliente['campos']['representantes']['codigo'] => $codigo))->get()->row_array();
		
		return trim($representante[$this->_db_cliente['campos']['representantes']['nome']]);
	}
	
	// REPRESENTANTES **
	
	// ** PEDIDOS
	
	function obter_pedidos_pdr($id_pedido = NULL, $result = NULL, $filial = NULL, $tipo = 'pedido')
	{	
		//Manter Orçamento
		if($tipo == 'pedido')
		{
			$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['tipo'], 'P');
		}
		else
		{
			$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['tipo'], 'O');
		}
		
		if ($id_pedido)
		{
			if($filial)
			{
				$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['filial'], $filial);
			}
		
			$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['id_pedido'], $id_pedido);
		}
		
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['id_pedido'] . ' as id_pedido';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['data_emissao'] . ' as data_emissao';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['id_usuario'] . ' as id_usuario';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['codigo_representante'] . ' as codigo_representante';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['tipo'] . ' as tipo';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['status'] . ' as status';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['codigo_cliente'] . ' as codigo_cliente';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['loja_cliente'] . ' as loja_cliente';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['nome_cliente'] . ' as nome_cliente';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['id_prospects'] . ' as id_prospects';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['nome_prospects'] . ' as nome_prospects';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['condicao_pagamento'] . ' as condicao_pagamento';
		//$select[] = $this->_db_cliente['campos']['pedidos_dw']['tipo_frete'] . ' as tipo_frete';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['valor_frete'] . ' as valor_frete';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['codigo_transportadora'] . ' as codigo_transportadora';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['ordem_compra'] . ' as ordem_compra';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['data_entrega'] . ' as data_entrega';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['obs_pedido'] . ' as obs_pedido';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['motivo_reprovacao'] . ' as motivo_reprovacao';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['id_feira'] . ' as id_feira';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['codigo_produto'] . ' as codigo_produto';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['descricao_produto'] . ' as descricao_produto';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['unidade_medida'] . ' as unidade_medida';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['preco_unitario'] . ' as preco_unitario';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['valor_total'] . ' as valor_total';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['ipi'] . ' as ipi';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['desconto'] . ' as desconto';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['tabela_precos'] . ' as tabela_precos';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['peso_unitario'] . ' as peso_unitario';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['substituicao_tributaria'] . ' as substituicao_tributaria';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['quantidade'] . ' as quantidade';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['delecao'] . ' as delecao';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['modent'] . ' as modent';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['chave'] . ' as chave';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['filial'] . ' as filial';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['data_edicao'] . ' as data_edicao';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['data_aprovacao'] . ' as data_aprovacao';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['data_conversao'] . ' as data_conversao';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['vencimento_shazam'] . ' as vencimento_shazam';
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['restricao_shazam'] . ' as restricao_shazam';
		//$select[] = $this->_db_cliente['campos']['pedidos_dw']['tipo_pedido'] . ' as tipo_pedido';
		//$select[] = $this->_db_cliente['campos']['pedidos_dw']['autorizado'] . ' as autorizado';
		//$select[] = $this->_db_cliente['campos']['pedidos_dw']['cultura'] . ' as cultura';

		if($result)
		{
			$pedido = $this->db_cliente
							->select(implode(', ', $select))
							->from($this->_db_cliente['tabelas']['pedidos_dw'])
							->get()
							->result_array();
		}
		else
		{
			$pedido = $this->db_cliente
							->select(implode(', ', $select))
							->from($this->_db_cliente['tabelas']['pedidos_dw'])
							->get()
							->row_array();
		}

		//debug_pre($pedido, TRUE);
		return $pedido;
		
	}
	
	function obter_pedido($codigo, $codigo_nota_fiscal = NULL, $id = NULL, $filial = NULL)
	{
		if ($codigo)
		{
			$this->db_cliente->where(
				array(
					$this->_db_cliente['campos']['pedidos']['codigo'] => $codigo,
					$this->_db_cliente['campos']['pedidos']['filial'] => $filial
				)
			);
		}
		else if ($codigo_nota_fiscal)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['pedidos']['nota_fiscal'], $codigo_nota_fiscal);
		}
		
		if ($codigo || $codigo_nota_fiscal)
		{
			$select[] = $this->_db_cliente['campos']['pedidos']['codigo_representante'];
			$select[] = $this->_db_cliente['campos']['pedidos']['codigo'];
			$select[] = $this->_db_cliente['campos']['pedidos']['filial'];
			$select[] = $this->_db_cliente['campos']['pedidos']['nota_fiscal'];
			$select[] = $this->_db_cliente['campos']['pedidos']['data_emissao'];
			$select[] = $this->_db_cliente['campos']['pedidos']['codigo_cliente'];
			$select[] = $this->_db_cliente['campos']['pedidos']['loja_cliente'];
			$select[] = $this->_db_cliente['campos']['pedidos']['codigo_forma_pagamento'];
			$select[] = $this->_db_cliente['campos']['pedidos']['codigo_transportadora'];
			$select[] = $this->_db_cliente['campos']['pedidos']['tipo_frete'];
			$select[] = $this->_db_cliente['campos']['pedidos']['tabela_precos'];
			$select[] = $this->_db_cliente['campos']['pedidos']['unidade'];
			$select[] = $this->_db_cliente['campos']['pedidos']['tipo_pedido'];
			
			if ($this->_db_cliente['campos']['pedidos']['delecao'])
			{
				$this->db_cliente->where($this->_db_cliente['campos']['pedidos']['delecao'], '');
			}
			$pedido = $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['pedidos'])->get()->row_array();
			
			if ($this->_db_cliente['campos']['itens_pedidos']['delecao'])
			{
				$this->db_cliente->where($this->_db_cliente['campos']['itens_pedidos']['delecao'], '');
			}
			$_pedido = $this->db_cliente->select('SUM(' . $this->_db_cliente['campos']['itens_pedidos']['quantidade_vendida_produto'] . '), SUM(' . $this->_db_cliente['campos']['itens_pedidos']['quantidade_faturada_produto'] . '), SUM(' . $this->_db_cliente['campos']['itens_pedidos']['quantidade_vendida_produto'] . ' * ' . $this->_db_cliente['campos']['itens_pedidos']['preco_produto'] . '), SUM(' . $this->_db_cliente['campos']['itens_pedidos']['quantidade_faturada_produto'] . ' * ' . $this->_db_cliente['campos']['itens_pedidos']['preco_produto'] . ')')->from($this->_db_cliente['tabelas']['itens_pedidos'])->where($this->_db_cliente['campos']['itens_pedidos']['codigo_pedido'], $pedido[$this->_db_cliente['campos']['pedidos']['codigo']])->get()->row_array();
			
			if (preg_match("/x/i", $pedido[$this->_db_cliente['campos']['pedidos']['codigo_nota_fiscal']])) {
				$status = 'faturado_residuo_eliminado';
			} else if ($_pedido['computed1'] <= 0) { // quantidade faturada = 0
				$status = 'aguardando_faturamento';
			} else if ($_pedido['computed1'] < $_pedido['computed']) { // quantidade faturada é menor que quantidade vendida
				$status = 'parcialmente_faturado';
			} else {
				$status = 'faturado';
			}
			
			$dados = array_map('trim', array(
				'tipo' => 'pedido',
				'status' => $status,
				'codigo_representante' => $pedido[$this->_db_cliente['campos']['pedidos']['codigo_representante']],
				'codigo' => $pedido[$this->_db_cliente['campos']['pedidos']['codigo']],
				'emissao_timestamp' => strtotime($pedido[$this->_db_cliente['campos']['pedidos']['data_emissao']]),
				'codigo_ordem_compra' => 'não há',
				'quantidade_vendida' => $_pedido['computed'],
				'quantidade_faturada' => $_pedido['computed1'],
				'valor_total' => $_pedido['computed2'],
				'valor_parcial' => $_pedido['computed3'],
				'codigo_forma_pagamento' => $pedido[$this->_db_cliente['campos']['pedidos']['codigo_forma_pagamento']],
				'codigo_transportadora' => $pedido[$this->_db_cliente['campos']['pedidos']['codigo_transportadora']],
				'tipo_frete' => $pedido[$this->_db_cliente['campos']['pedidos']['tipo_frete']] == 'C' ? 'CIF' : 'FOB',
				'tabela_precos' => $pedido[$this->_db_cliente['campos']['pedidos']['tabela_precos']],
				'unidade' => $pedido[$this->_db_cliente['campos']['pedidos']['unidade']],
				'tipo_pedido' => $pedido[$this->_db_cliente['campos']['pedidos']['tipo_pedido']]
			));
			
			$dados = array_merge($dados, array(
				'cliente' => $this->obter_cliente($pedido[$this->_db_cliente['campos']['pedidos']['codigo_cliente']], $pedido[$this->_db_cliente['campos']['pedidos']['loja_cliente']]),
				'itens' => $this->obter_itens_pedido($pedido[$this->_db_cliente['campos']['pedidos']['codigo']] , 0,$filial),
				'notas_fiscais' => $this->obter_notas_fiscais_pedido($pedido[$this->_db_cliente['campos']['pedidos']['codigo']], $filial)
			));
		}
		else 
		{
			$pedido = $this->db->from('pedidos')->where('id', $id)->get()->row();
			$_pedido = $this->db->select('SUM(quantidade) AS quantidade_vendida, SUM(preco * quantidade) AS valor_total')->from('itens_pedidos')->where('id_pedido', $pedido->id)->get()->row();
			
			$dados = array_map('trim', array(
				'tipo' => $pedido->tipo,
				'status' => $pedido->status,
				'codigo' => 'não há',
				'emissao_timestamp' => $pedido->timestamp,
				'codigo_ordem_compra' => $pedido->codigo_ordem_compra,
				'quantidade_vendida' => $_pedido->quantidade_vendida,
				'quantidade_faturada' => 0,
				'valor_total' => $_pedido->valor_total,
				'valor_parcial' => 0,
				'codigo_forma_pagamento' => $_pedido->codigo_forma_pagamento,
				'codigo_transportadora' => $_pedido->codigo_transportadora,
				'tipo_frete' => $pedido->tipo_frete
			));
			
			$dados = array_merge($dados, array(
				'cliente' => $this->obter_cliente($pedido->codigo_cliente, $pedido->loja_cliente),
				'prospect' => $this->db->from('prospects')->where('id', $pedido->id_prospect)->get()->row(),
				'itens' => $this->obter_itens_pedido(NULL, $pedido->id)
			));
		}
		$dados['filial'] = $filial;
		$dados['codigo_loja_cliente'] = $dados['cliente']['codigo'] . '|' . $dados['cliente']['loja'];
		$dados['_codigo_loja_cliente'] = $dados['cliente']['codigo'] . ' - ' . $dados['cliente']['nome'] . ' - ' . $dados['cliente']['cpf'];

		//echo '<pre>';print_r($dados);echo '</pre>';
		return $dados;
	}
	
	/* Obtem as informações de posicionamento do pedido processado */
	function obter_posicionamento_pedido($id_pedido, $filial){
		$select[] = $this->_db_cliente['campos']['posicionamento_pedido']['data_pedido'] . ' AS data_pedido';
		$select[] = $this->_db_cliente['campos']['posicionamento_pedido']['hora_pedido'] . ' AS hora_pedido';
		$select[] = $this->_db_cliente['campos']['posicionamento_pedido']['data_picking'] . ' AS data_picking';
		$select[] = $this->_db_cliente['campos']['posicionamento_pedido']['hora_picking'] . ' AS hora_picking';
		$select[] = $this->_db_cliente['campos']['posicionamento_pedido']['data_nf'] . ' AS data_nf';
		$select[] = $this->_db_cliente['campos']['posicionamento_pedido']['hora_nf'] . ' AS hora_nf';
		$select[] = $this->_db_cliente['campos']['posicionamento_pedido']['data_coleta'] . ' AS data_coleta';
		$select[] = $this->_db_cliente['campos']['posicionamento_pedido']['hora_coleta'] . ' AS hora_coleta';
		$select[] = $this->_db_cliente['campos']['posicionamento_pedido']['data_separacao'] . ' AS data_separacao';
		$select[] = $this->_db_cliente['campos']['posicionamento_pedido']['hora_separacao'] . ' AS hora_separacao';
		$select[] = $this->_db_cliente['campos']['posicionamento_pedido']['data_conferencia'] . ' AS data_conferencia';
		$select[] = $this->_db_cliente['campos']['posicionamento_pedido']['hora_conferencia'] . ' AS hora_conferencia';
		
		$pedido = $this->db_cliente->select(implode(', ', $select))
								   ->from($this->_db_cliente['tabelas']['posicionamento_pedido'])
								   ->where($this->_db_cliente['campos']['posicionamento_pedido']['pedido'], $id_pedido)
								   ->where($this->_db_cliente['campos']['posicionamento_pedido']['filial'], $filial)
								   ->get()->row_array();
								   
	   return $pedido;
	}
	
	// essa função é basicamente usada para notas fiscais, pois pegar informação de um pedido inteiro é perda de processamento
	function obter_frete_pedido($codigo_pedido)
	{
		if ($this->_db_cliente['campos']['pedidos']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['pedidos']['delecao'], '');
		}
		
		$pedido = $this->db_cliente->select($this->_db_cliente['campos']['pedidos']['tipo_frete'])->from($this->_db_cliente['tabelas']['pedidos'])->where($this->_db_cliente['campos']['pedidos']['codigo'], $codigo_pedido)->get()->row_array();
		
		return $pedido[$this->_db_cliente['campos']['pedidos']['tipo_frete']] == 'C' ? 'CIF' : 'FOB';
	}
	
	function obter_itens_pedido($codigo_pedido, $id_pedido = NULL, $filial = NULL){
		if ($codigo_pedido){
			$select[] = $this->_db_cliente['campos']['itens_pedidos']['codigo_item'];
			$select[] = $this->_db_cliente['campos']['itens_pedidos']['codigo_produto'];
			$select[] = $this->_db_cliente['campos']['itens_pedidos']['descricao_produto'];
			$select[] = $this->_db_cliente['campos']['itens_pedidos']['unidade_medida_produto'];
			$select[] = $this->_db_cliente['campos']['itens_pedidos']['preco_produto'];
			$select[] = $this->_db_cliente['campos']['itens_pedidos']['quantidade_vendida_produto'];
			$select[] = $this->_db_cliente['campos']['itens_pedidos']['quantidade_faturada_produto'];
			$select[] = $this->_db_cliente['campos']['itens_pedidos']['desconto'];
			$select[] = $this->_db_cliente['campos']['itens_pedidos']['filial'];
			$select[] = $this->_db_cliente['campos']['itens_pedidos']['valor_total_desconto_item'];
			
			$itens_pedido = $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['itens_pedidos'])
				->where(
					array (
						$this->_db_cliente['campos']['itens_pedidos']['codigo_pedido'] => $codigo_pedido,
						$this->_db_cliente['campos']['itens_pedidos']['filial'] => $filial
					)
				)
				->get()->result_array();
			$_itens_pedido = array();
			
			foreach ($itens_pedido as $item_pedido){
				$_itens_pedido[] = array_map('trim', array(
					'codigo_item' => $item_pedido[$this->_db_cliente['campos']['itens_pedidos']['codigo_item']],
					'codigo_produto' => $item_pedido[$this->_db_cliente['campos']['itens_pedidos']['codigo_produto']],
					'descricao_produto' => $item_pedido[$this->_db_cliente['campos']['itens_pedidos']['descricao_produto']],
					'unidade_medida_produto' => $item_pedido[$this->_db_cliente['campos']['itens_pedidos']['unidade_medida_produto']],
					'preco_produto' => $item_pedido[$this->_db_cliente['campos']['itens_pedidos']['preco_produto']],
					'filial' => $item_pedido[$this->_db_cliente['campos']['itens_pedidos']['filial']],
					'valor_total_desconto_item' => $item_pedido[$this->_db_cliente['campos']['itens_pedidos']['valor_total_desconto_item']],
					'desconto' => (float)$item_pedido[$this->_db_cliente['campos']['itens_pedidos']['desconto']],
					'quantidade_vendida_produto' => $item_pedido[$this->_db_cliente['campos']['itens_pedidos']['quantidade_vendida_produto']],
					'quantidade_faturada_produto' => $item_pedido[$this->_db_cliente['campos']['itens_pedidos']['quantidade_faturada_produto']],
					'valor_parcial_item' => $item_pedido[$this->_db_cliente['campos']['itens_pedidos']['preco_produto']] * $item_pedido[$this->_db_cliente['campos']['itens_pedidos']['quantidade_faturada_produto']],
					'valor_total_item' => $item_pedido[$this->_db_cliente['campos']['itens_pedidos']['preco_produto']] * $item_pedido[$this->_db_cliente['campos']['itens_pedidos']['quantidade_vendida_produto']]
				));
			}
		}else{
			$itens_pedido = $this->db->from('itens_pedidos')->where('id_pedido', $id_pedido)->order_by('id', 'asc')->get()->result();
			$_itens_pedido = array();
			
			$i = 1;
			
			foreach ($itens_pedido as $item_pedido){
				$_itens_pedido[] = array_map('trim', array(
					'codigo_item' => $i,
					'codigo_produto' => $item_pedido->codigo,
					'descricao_produto' => $item_pedido->descricao,
					'unidade_medida_produto' => $item_pedido->unidade_medida,
					'preco_produto' => $item_pedido->preco,
					'filial' => $item_pedido->filial,
					'desconto' => $item_pedido->desconto,
					'quantidade_vendida_produto' => $item_pedido->quantidade,
					'quantidade_faturada_produto' => 0,
					'valor_parcial_item' => 0,
					'valor_total_item' => $item_pedido->preco * $item_pedido->quantidade
				));
				
				$i++;
			}
		}
		
		return $_itens_pedido;
	}
	
	function obter_notas_fiscais_pedido($codigo_pedido, $filial = NULL)
	{
		if ($this->_db_cliente['campos']['itens_notas_fiscais']['delecao'])
		{
			$this->db_cliente->where(
				array(
					$this->_db_cliente['campos']['itens_notas_fiscais']['delecao'] => '',
					$this->_db_cliente['campos']['itens_notas_fiscais']['filial'] => $filial
					)
				);
		}
		
		$notas_fiscais_pedido = $this->db_cliente->distinct()->select($this->_db_cliente['campos']['itens_notas_fiscais']['codigo_nota_fiscal'])->from($this->_db_cliente['tabelas']['itens_notas_fiscais'])->where(array($this->_db_cliente['campos']['itens_notas_fiscais']['codigo_pedido'] => $codigo_pedido))->get()->result_array();
		$_notas_fiscais_pedido = array();
		
		foreach ($notas_fiscais_pedido as $nota_fiscal_pedido)
		{
			$_notas_fiscais_pedido[] = $this->obter_nota_fiscal($nota_fiscal_pedido[$this->_db_cliente['campos']['itens_notas_fiscais']['codigo_nota_fiscal']], $filial);
		}
		
		return $_notas_fiscais_pedido;
	}
	
	function obter_pedidos($codigo_cliente, $loja_cliente = NULL, $data_inicial_timestamp = NULL, $data_final_timestamp = NULL, $tipo = 'pedido', $id_prospect = NULL, $codigo_representante = NULL)
	{
		// ** obter pedidos no db_cliente
		
		if ($tipo == 'pedido')
		{
			if ($codigo_cliente)
			{
				$this->db_cliente->where(array($this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['codigo'] => $codigo_cliente, $this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['loja'] => $loja_cliente));
			}
			
			if($codigo_representante)
			{
				$this->db_cliente->where(array($this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['codigo_representante'] => $codigo_representante));
			}
			
			if ($data_inicial_timestamp)
			{
				$this->db_cliente->where($this->_db_cliente['campos']['pedidos']['data_emissao'] . ' >=', date('Ymd', $data_inicial_timestamp));
			}
			
			if ($data_final_timestamp)
			{
				$this->db_cliente->where($this->_db_cliente['campos']['pedidos']['data_emissao'] . ' <=', date('Ymd', $data_final_timestamp));
			}
			
			if ($this->session->userdata('codigo_usuario'))
			{
				if ($this->_db_cliente['erp'] == 'protheus')
				{
					$this->db_cliente->where($this->_db_cliente['campos']['pedidos']['codigo_representante'], $this->session->userdata('codigo_usuario'));
				}
				else if ($this->_db_cliente['erp'] == 'mercador')
				{
					$this->db_cliente->where($this->_db_cliente['campos']['representantes_pedidos']['codigo_representante'], $this->session->userdata('codigo_usuario'));
					
					$this->db_cliente->join($this->_db_cliente['tabelas']['representantes_pedidos'], $this->_db_cliente['campos']['representantes_pedidos']['codigo_pedido'] . ' = ' . $this->_db_cliente['campos']['pedidos']['codigo']);
				}
			}
			
			$this->db_cliente->join($this->_db_cliente['tabelas']['itens_pedidos'], $this->_db_cliente['tabelas']['itens_pedidos'] . '.' . $this->_db_cliente['campos']['itens_pedidos']['codigo_pedido'] . ' = ' . $this->_db_cliente['tabelas']['pedidos'] . '.' . $this->_db_cliente['campos']['pedidos']['codigo']);
			$this->db_cliente->join($this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['codigo'] . ' = ' . $this->_db_cliente['tabelas']['pedidos'] . '.' . $this->_db_cliente['campos']['pedidos']['codigo_cliente'] . ' AND ' . $this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['loja'] . ' = ' . $this->_db_cliente['tabelas']['pedidos'] . '.' . $this->_db_cliente['campos']['pedidos']['loja_cliente']);
			
			// ** deleções
			if ($this->_db_cliente['campos']['pedidos']['delecao'])
			{
				$this->db_cliente->where($this->_db_cliente['tabelas']['pedidos'] . '.' . $this->_db_cliente['campos']['pedidos']['delecao'], '');
			}
			if ($this->_db_cliente['campos']['itens_pedidos']['delecao'])
			{
				$this->db_cliente->where($this->_db_cliente['tabelas']['itens_pedidos'] . '.' . $this->_db_cliente['campos']['itens_pedidos']['delecao'], '');
			}
			if ($this->_db_cliente['campos']['clientes']['delecao'])
			{
				$this->db_cliente->where($this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['delecao'], '');
			}
			// deleções **
			
			/*$select[] = $this->_db_cliente['campos']['pedidos']['codigo'];
			$select[] = $this->_db_cliente['campos']['pedidos']['nota_fiscal'];
			$select[] = $this->_db_cliente['campos']['pedidos']['data_emissao'];
			$select[] = $this->_db_cliente['campos']['clientes']['nome'];
			
			$group_by[] = $this->_db_cliente['campos']['pedidos']['codigo'];
			$group_by[] = $this->_db_cliente['campos']['pedidos']['nota_fiscal'];
			$group_by[] = $this->_db_cliente['campos']['pedidos']['data_emissao'];
			$group_by[] = $this->_db_cliente['campos']['clientes']['nome'];*/
			$select[] = $this->_db_cliente['tabelas']['pedidos'] . '.' . $this->_db_cliente['campos']['pedidos']['codigo'];
			$select[] = $this->_db_cliente['campos']['pedidos']['nota_fiscal'];
			$select[] = $this->_db_cliente['campos']['pedidos']['data_emissao'];
			$select[] = $this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['nome'];
			
			$group_by[] = $this->_db_cliente['tabelas']['pedidos'] . '.' . $this->_db_cliente['campos']['pedidos']['codigo'];
			$group_by[] = $this->_db_cliente['campos']['pedidos']['nota_fiscal'];
			$group_by[] = $this->_db_cliente['campos']['pedidos']['data_emissao'];
			$group_by[] = $this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['nome'];
			
			$pedidos = $this->db_cliente->select(implode(', ', $select) . ', SUM(' . $this->_db_cliente['campos']['itens_pedidos']['quantidade_vendida_produto'] . '), SUM(' . $this->_db_cliente['campos']['itens_pedidos']['quantidade_faturada_produto'] . '), SUM(' . $this->_db_cliente['campos']['itens_pedidos']['quantidade_vendida_produto'] . ' * ' . $this->_db_cliente['campos']['itens_pedidos']['preco_produto'] . '), SUM(' . $this->_db_cliente['campos']['itens_pedidos']['quantidade_faturada_produto'] . ' * ' . $this->_db_cliente['campos']['itens_pedidos']['preco_produto'] . ')')->from($this->_db_cliente['tabelas']['pedidos'])->group_by(implode(', ', $group_by))->get()->result_array();
			$_pedidos = array();
			
			//$_pedidos['last_query'] = $this->db_cliente->last_query();
			
			foreach ($pedidos as $pedido)
			{
				if (eregi("x", $pedido[$this->_db_cliente['campos']['pedidos']['nota_fiscal']])) {
					$status = 'faturado_residuo_eliminado';
				} else if ($pedido['computed1'] <= 0) { // quantidade faturada = 0
					$status = 'aguardando_faturamento';
				} else if ($pedido['computed1'] < $pedido['computed']) { // quantidade faturada é menor que quantidade vendida
					$status = 'parcialmente_faturado';
				} else {
					$status = 'faturado';
				}
				
				$_pedidos[] = array_map('trim', array(
					'status' => $status,
					'codigo' => $pedido[$this->_db_cliente['campos']['pedidos']['codigo']],
					'emissao_timestamp' => strtotime($pedido[$this->_db_cliente['campos']['pedidos']['data_emissao']]),
					'nome_cliente' => $pedido[$this->_db_cliente['campos']['clientes']['nome']],
					'quantidade_vendida' => $pedido['computed'],
					'quantidade_faturada' => $pedido['computed1'],
					'valor_total' => $pedido['computed2'],
					'valor_parcial' => $pedido['computed3']
				));
			}
		}
		
		// obter pedidos no db_cliente **
		
		// ** obter pedidos no portal
		
		if ($codigo_cliente)
		{
			$this->db->where(array('codigo_cliente' => $codigo_cliente, 'loja_cliente' => $loja_cliente));
		}
		
		if ($id_prospect)
		{
			$this->db->where(array('id_prospect' => $id_prospect));
		}
		
		if ($data_inicial_timestamp)
		{
			$this->db->where('timestamp >=', $data_inicial_timestamp);
		}
		
		if ($data_final_timestamp)
		{
			$this->db->where('timestamp <=', $data_final_timestamp);
		}
		
		if ($this->session->userdata('codigo_usuario'))
		{
			$this->db->where('codigo_usuario', $this->session->userdata('codigo_usuario'));
		}
		
		$pedidos = $this->db->from('pedidos')->where(array('tipo' => $tipo, 'status IS NOT NULL' => NULL))->get()->result();
			
		foreach ($pedidos as $pedido)
		{
			$_pedido = $this->db->select('SUM(quantidade) AS quantidade_vendida, SUM(preco * quantidade) AS valor_total')->from('itens_pedidos')->where('id_pedido', $pedido->id)->get()->row();
			
			if ($pedido->id_prospect)
			{
				$prospect = $this->db->from('prospects')->where('id', $pedido->id_prospect)->get()->row();
			}
			
			$_pedidos[] = array_map('trim', array(
				'status' => $pedido->status,
				'codigo' => 'não há',
				'id' => $pedido->id,
				'emissao_timestamp' => $pedido->timestamp,
				'nome_cliente' => $pedido->id_prospect ? $prospect->nome : $this->obter_nome_cliente($pedido->codigo_cliente, $pedido->loja_cliente),
				'quantidade_vendida' => $_pedido->quantidade_vendida,
				'quantidade_faturada' => 0,
				'valor_total' => $_pedido->valor_total,
				'valor_parcial' => 0
			));
		}
		
		// obter pedidos no portal **
		
		return $_pedidos;
	}
	
	// PEDIDOS **
	
	// ** NOTAS FISCAIS
	
	function obter_nota_fiscal($codigo = NULL, $filial = NULL)
	{
		if ($this->_db_cliente['campos']['notas_fiscais']['delecao'])
		{
			$this->db_cliente->where(
				array(
						$this->_db_cliente['campos']['notas_fiscais']['delecao'] => '',
						$this->_db_cliente['campos']['notas_fiscais']['filial'] => $filial
					)
				);
		}
		
		$select[] = $this->_db_cliente['campos']['notas_fiscais']['codigo'];
		$select[] = $this->_db_cliente['campos']['notas_fiscais']['data_emissao'];
		$select[] = $this->_db_cliente['campos']['notas_fiscais']['valor_total'];
		$select[] = $this->_db_cliente['campos']['notas_fiscais']['valor_icms'];
		$select[] = $this->_db_cliente['campos']['notas_fiscais']['valor_ipi'];
		$select[] = $this->_db_cliente['campos']['notas_fiscais']['codigo_cliente'];
		$select[] = $this->_db_cliente['campos']['notas_fiscais']['loja_cliente'];
		$select[] = $this->_db_cliente['campos']['notas_fiscais']['codigo_transportadora'];
		$select[] = $this->_db_cliente['campos']['notas_fiscais']['tipo_frete'];
		
		$nota_fiscal = $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['notas_fiscais'])->where($this->_db_cliente['campos']['notas_fiscais']['codigo'], $codigo)->get()->row_array();
		
		$dados = array_map('trim', array(
			'codigo' => $nota_fiscal[$this->_db_cliente['campos']['notas_fiscais']['codigo']],
			'emissao_timestamp' => strtotime($nota_fiscal[$this->_db_cliente['campos']['notas_fiscais']['data_emissao']]),
			'valor_total' => $nota_fiscal[$this->_db_cliente['campos']['notas_fiscais']['valor_total']],
			'valor_icms' => $nota_fiscal[$this->_db_cliente['campos']['notas_fiscais']['valor_icms']],
			'valor_ipi' => $nota_fiscal[$this->_db_cliente['campos']['notas_fiscais']['valor_ipi']],
			'tipo_frete' => $nota_fiscal[$this->_db_cliente['campos']['notas_fiscais']['tipo_frete']] == 'C' ? 'CIF' : 'FOB'
		));
		
		$dados = array_merge($dados, array(
			'cliente' => $this->obter_cliente($nota_fiscal[$this->_db_cliente['campos']['notas_fiscais']['codigo_cliente']], $nota_fiscal[$this->_db_cliente['campos']['notas_fiscais']['loja_cliente']]),
			'transportadora' => $this->obter_transportadora($nota_fiscal[$this->_db_cliente['campos']['notas_fiscais']['codigo_transportadora']]),
			//'tipo_frete' => $this->obter_frete_pedido(NULL, $nota_fiscal[$this->_db_cliente['campos']['notas_fiscais']['codigo']]),
			'titulos' => $this->obter_titulos($nota_fiscal[$this->_db_cliente['campos']['notas_fiscais']['codigo']], $filial)
		));
		
		return $dados;
	}
	
	function obter_notas_fiscais($codigo_cliente, $loja_cliente, $data_inicial = NULL, $data_final = NULL)
	{
		if ($data_inicial)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['notas_fiscais']['data_emissao'] . ' >=', $data_inicial);
		}
		
		if ($data_final)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['notas_fiscais']['data_emissao'] . ' <=', $data_final);
		}
		
		if ($this->session->userdata('codigo_usuario'))
		{
			$this->db_cliente->where($this->_db_cliente['campos']['notas_fiscais']['codigo_representante'], $this->session->userdata('codigo_usuario'));
		}
		
		if ($this->_db_cliente['campos']['notas_fiscais']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['notas_fiscais']['delecao'], '');
		}
		
		$campos = array('codigo', 'data_emissao', 'valor_total', 'valor_icms', 'valor_ipi', 'codigo_cliente', 'loja_cliente', 'codigo_transportadora');
		
		foreach ($campos as $campo)
		{
			$select[] = $this->_db_cliente['campos']['notas_fiscais'][$campo] . ' AS ' . $campo;
		}
		
		$notas_fiscais = $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['notas_fiscais'])->where(array($this->_db_cliente['campos']['notas_fiscais']['codigo_cliente'] => $codigo_cliente, $this->_db_cliente['campos']['notas_fiscais']['loja_cliente'] => $loja_cliente))->get()->result_array();
		$_notas_fiscais = array();
		
		foreach ($notas_fiscais as $nota_fiscal)
		{
			$_notas_fiscais[] = $this->obter_nota_fiscal($nota_fiscal['codigo']);
		}
		
		return $_notas_fiscais;
	}

	
	// NOTAS FISCAIS **
	
	// ** TÍTULOS
	
	function obter_titulo($codigo, $parcela)
	{
		if ($this->_db_cliente['campos']['titulos']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['titulos']['delecao'], '');
		}
		
		$select[] = $this->_db_cliente['campos']['titulos']['codigo'];
		$select[] = $this->_db_cliente['campos']['titulos']['parcela'];
		$select[] = $this->_db_cliente['campos']['titulos']['data_vencimento'];
		$select[] = $this->_db_cliente['campos']['titulos']['valor'];
		$select[] = $this->_db_cliente['campos']['titulos']['data_baixa'];
		
		$titulo = $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['titulos'])->where(array($this->_db_cliente['campos']['titulos']['codigo'] => $codigo, $this->_db_cliente['campos']['titulos']['parcela'] => $parcela))->get()->row_array();
		
		$vencimento_timestamp = strtotime($titulo[$this->_db_cliente['campos']['titulos']['data_vencimento']]);
		$pagamento_timestamp = trim($titulo[$this->_db_cliente['campos']['titulos']['data_baixa']]) ? strtotime($titulo[$this->_db_cliente['campos']['titulos']['data_baixa']]) : NULL;
		
		if ($pagamento_timestamp)
		{
			$dias = round(($pagamento_timestamp - $vencimento_timestamp) / (3600 * 24));
			
			if ($pagamento_timestamp > $vencimento_timestamp) $status = 'Pago com atraso de ' . $dias . ' dia(s)';
			else $status = 'Pago em dia';
		}
		else
		{
			$dias = abs(round((time() - $vencimento_timestamp) / (3600 * 24)));
			
			if (time() > $vencimento_timestamp ) $status = $dias . ' dia(s) vencido';
			else $status = $dias . ' dia(s) para vencer';
		}
		
		return array_map('trim', array(
			'codigo' => $titulo[$this->_db_cliente['campos']['titulos']['codigo']],
			'parcela' => trim($titulo[$this->_db_cliente['campos']['titulos']['parcela']]) ? $titulo[$this->_db_cliente['campos']['titulos']['parcela']] : 1,
			'status' => $status,
			'valor' => $titulo[$this->_db_cliente['campos']['titulos']['valor']],
			'vencimento_timestamp' => $vencimento_timestamp,
			'pagamento_timestamp' => $pagamento_timestamp
		));
	}
	
	function obter_titulos($codigo_nota_fiscal = NULL, $filial = NULL)
	{
		if ($this->_db_cliente['campos']['titulos']['delecao'])
		{
			$this->db_cliente->where(
				array(
					$this->_db_cliente['campos']['titulos']['delecao'] => '',
					$this->_db_cliente['campos']['titulos']['filial'] => $filial
					)
				);
		}
		
		$select[] = $this->_db_cliente['campos']['titulos']['codigo'];
		$select[] = $this->_db_cliente['campos']['titulos']['parcela'];
		$select[] = $this->_db_cliente['campos']['titulos']['data_vencimento'];
		$select[] = $this->_db_cliente['campos']['titulos']['valor'];
		$select[] = $this->_db_cliente['campos']['titulos']['data_baixa'];
		
		$titulos = $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['titulos'])->where(array($this->_db_cliente['campos']['titulos']['codigo'] => $codigo_nota_fiscal))->get()->result_array();
		$_titulos = array();
		
		foreach ($titulos as $titulo)
		{
			$vencimento_timestamp = strtotime($titulo[$this->_db_cliente['campos']['titulos']['data_vencimento']]);
			$pagamento_timestamp = trim($titulo[$this->_db_cliente['campos']['titulos']['data_baixa']]) ? strtotime($titulo[$this->_db_cliente['campos']['titulos']['data_baixa']]) : NULL;
			
			if ($pagamento_timestamp)
			{
				$dias = round(($pagamento_timestamp - $vencimento_timestamp) / (3600 * 24));
				
				if ($pagamento_timestamp > $vencimento_timestamp) $status = 'Pago com atraso de ' . $dias . ' dia(s)';
				else $status = 'Pago em dia';
			}
			else
			{
				$dias = abs(round((time() - $vencimento_timestamp) / (3600 * 24)));
				
				if (time() > $vencimento_timestamp ) $status = $dias . ' dia(s) vencido';
				else $status = $dias . ' dia(s) para vencer';
			}
			
			$_titulos[] = array_map('trim', array(
				'codigo' => $titulo[$this->_db_cliente['campos']['titulos']['codigo']],
				'parcela' => trim($titulo[$this->_db_cliente['campos']['titulos']['parcela']]) ? $titulo[$this->_db_cliente['campos']['titulos']['parcela']] : 1,
				'status' => $status,
				'valor' => $titulo[$this->_db_cliente['campos']['titulos']['valor']],
				'vencimento_timestamp' => $vencimento_timestamp,
				'pagamento_timestamp' => $pagamento_timestamp
			));
		}
		
		return $_titulos;
	}
	
	function obter_titulos_vencidos($codigo_cliente = NULL, $loja_cliente = NULL, $data_vencimento_inicial = NULL, $data_vencimento_final = NULL)
	{
		if ($codigo_cliente)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['titulos']['codigo_cliente'], $codigo_cliente);
		}
		
		if ($loja_cliente)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['titulos']['loja_cliente'], $loja_cliente);
		}
		
		if ($data_vencimento_inicial)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['titulos']['data_vencimento'] . ' >=', $data_vencimento_inicial);
		}
		
		if ($data_vencimento_final)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['titulos']['data_vencimento'] . ' <=', $data_vencimento_final);
		}
		
		if ($this->_db_cliente['campos']['titulos']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['titulos']['delecao'], '');
		}
		
		$select[] = $this->_db_cliente['campos']['titulos']['codigo'];
		$select[] = $this->_db_cliente['campos']['titulos']['parcela'];
		$select[] = $this->_db_cliente['campos']['titulos']['data_vencimento'];
		$select[] = $this->_db_cliente['campos']['titulos']['valor'];
		$select[] = $this->_db_cliente['campos']['titulos']['data_baixa'];
		$select[] = $this->_db_cliente['campos']['titulos']['codigo_cliente'];
		$select[] = $this->_db_cliente['campos']['titulos']['loja_cliente'];
		
		$titulos = $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['titulos'])->where(array($this->_db_cliente['campos']['titulos']['data_vencimento'] . ' <' => date('Ymd'), $this->_db_cliente['campos']['titulos']['data_baixa'] => ''))->get()->result_array();
		$_titulos = array();
		
		foreach ($titulos as $titulo)
		{
			$vencimento_timestamp = strtotime($titulo[$this->_db_cliente['campos']['titulos']['data_vencimento']]);
			$pagamento_timestamp = trim($titulo[$this->_db_cliente['campos']['titulos']['data_baixa']]) ? strtotime($titulo[$this->_db_cliente['campos']['titulos']['data_baixa']]) : NULL;
			
			if ($pagamento_timestamp)
			{
				$dias = round(($pagamento_timestamp - $vencimento_timestamp) / (3600 * 24));
				
				if ($pagamento_timestamp > $vencimento_timestamp) $status = 'Pago com atraso de ' . $dias . ' dia(s)';
				else $status = 'Pago em dia';
			}
			else
			{
				$dias = abs(round((time() - $vencimento_timestamp) / (3600 * 24)));
				
				if (time() > $vencimento_timestamp ) $status = $dias . ' dia(s) vencido';
				else $status = $dias . ' dia(s) para vencer';
			}
			
			$dados = array_map('trim', array(
				'codigo' => $titulo[$this->_db_cliente['campos']['titulos']['codigo']],
				'parcela' => trim($titulo[$this->_db_cliente['campos']['titulos']['parcela']]) ? $titulo[$this->_db_cliente['campos']['titulos']['parcela']] : 1,
				'status' => $status,
				'valor' => $titulo[$this->_db_cliente['campos']['titulos']['valor']],
				'vencimento_timestamp' => $vencimento_timestamp,
				'pagamento_timestamp' => $pagamento_timestamp
			));
			
			$_titulos[] = array_merge($dados, array(
				'cliente' => array('nome' => $this->obter_nome_cliente($titulo[$this->_db_cliente['campos']['titulos']['codigo_cliente']], $titulo[$this->_db_cliente['campos']['titulos']['loja_cliente']]))
			));
		}
		
		return $_titulos;
	}
	
	
	function obter_faturamento($codigo_representante = NULL, $data_inicial_timestamp = NULL, $data_final_timestamp = NULL, $estado = NULL, $codigo_cliente = NULL, $codigo_produto = NULL)
	{
		
		if($codigo_representante)
		{
			
			if ($data_inicial_timestamp)
			{
				$this->db_cliente->where($this->_db_cliente['campos']['notas_fiscais']['data_emissao'] . ' >=', date('Ymd', $data_inicial_timestamp));
			}
			
			if ($data_final_timestamp)
			{
				$this->db_cliente->where($this->_db_cliente['campos']['notas_fiscais']['data_emissao'] . ' <=', date('Ymd', $data_final_timestamp));
			}
			
			$select[] = 'SUM(' . $this->_db_cliente['campos']['notas_fiscais']['valor_total'] . ') AS valor_total'; 
			
			if ($this->_db_cliente['campos']['notas_fiscais']['delecao'])
			{
				$this->db_cliente->where($this->_db_cliente['campos']['notas_fiscais']['delecao'] . ' !=', '*');
			}
			
			return $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['notas_fiscais'])->where($this->_db_cliente['campos']['notas_fiscais']['codigo_representante'], $codigo_representante)->get()->result_array();
			
		}
		else if($estado)
		{
			/*
			// Selecionando Todos os Clientes do estado passado por parametro /*
			$this->db_cliente->where($this->_db_cliente['campos']['clientes']['estado'], $estado);
			
			$campos = array('codigo');
			
			foreach ($campos as $campo)
			{
				$select[] = $this->_db_cliente['campos']['clientes'][$campo] . ' AS ' . $campo;
			}
			
			if ($this->_db_cliente['campos']['clientes']['delecao'])
			{
				$this->db_cliente->where($this->_db_cliente['campos']['clientes']['delecao'], '');
			}
			
			$clientes = $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['clientes'])->get()->result_array();
			//
			
			
			foreach($clientes as $cliente){
				//$this->db_cliente->where($this->_db_cliente['campos']['notas_fiscais']['codigo_cliente'], $cliente['codigo']);
				
				$where[] = $this->_db_cliente['campos']['notas_fiscais']['codigo_cliente'] . ' = ' .  $cliente['codigo'];
			}
			
			$_where = '(' . implode(' OR ', $where) . ')';
			
			if ($data_inicial_timestamp)
			{
				$this->db_cliente->where($this->_db_cliente['campos']['notas_fiscais']['data_emissao'] . ' >=', date('Ymd', $data_inicial_timestamp));
			}
			
			if ($data_final_timestamp)
			{
				$this->db_cliente->where($this->_db_cliente['campos']['notas_fiscais']['data_emissao'] . ' <=', date('Ymd', $data_final_timestamp));
			}
			
			$select_total[] = 'SUM(' . $this->_db_cliente['campos']['notas_fiscais']['valor_total'] . ') AS valor_total';
			return $this->db_cliente->select(implode(', ', $select_total))->from($this->_db_cliente['tabelas']['notas_fiscais'])->where($_where)->get()->result_array();
			
			//return $this->db_cliente->last_query();
			*/
				
				
			$this->db_cliente->where(
				$this->_db_cliente['tabelas']['notas_fiscais'] . '.' . $this->_db_cliente['campos']['notas_fiscais']['codigo_cliente'] . ' = ' .
				$this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['codigo']
			);
			
			$this->db_cliente->where(
				$this->_db_cliente['tabelas']['notas_fiscais'] . '.' . $this->_db_cliente['campos']['notas_fiscais']['loja_cliente'] . ' = ' .
				$this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['loja']
			);
			
			$this->db_cliente->where(
				$this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['estado'], $estado
			);
			
			if ($data_inicial_timestamp)
			{
				$this->db_cliente->where($this->_db_cliente['tabelas']['notas_fiscais'] . '.' . $this->_db_cliente['campos']['notas_fiscais']['data_emissao'] . ' >=', date('Ymd', $data_inicial_timestamp));
			}
			
			if ($data_final_timestamp)
			{
				$this->db_cliente->where($this->_db_cliente['tabelas']['notas_fiscais'] . '.' . $this->_db_cliente['campos']['notas_fiscais']['data_emissao'] . ' <=', date('Ymd', $data_final_timestamp));
			}
			
			if ($this->_db_cliente['campos']['notas_fiscais']['delecao'])
			{
				$this->db_cliente->where($this->_db_cliente['tabelas']['notas_fiscais'] . '.' . $this->_db_cliente['campos']['notas_fiscais']['delecao'], '');
			}
			
			if ($this->_db_cliente['campos']['clientes']['delecao'])
			{
				$this->db_cliente->where($this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['delecao'], '');
			}
				
			$select_total[] = 'SUM(' . $this->_db_cliente['campos']['notas_fiscais']['valor_total'] . ') AS valor_total';
			return $this->db_cliente->select(implode(', ', $select_total))->from($this->_db_cliente['tabelas']['notas_fiscais'] . ',' . $this->_db_cliente['tabelas']['clientes'])->get()->result_array();
				
		}
		else if($codigo_cliente)
		{
			
			if ($data_inicial_timestamp)
			{
				$this->db_cliente->where($this->_db_cliente['campos']['notas_fiscais']['data_emissao'] . ' >=', date('Ymd', $data_inicial_timestamp));
			}
			
			if ($data_final_timestamp)
			{
				$this->db_cliente->where($this->_db_cliente['campos']['notas_fiscais']['data_emissao'] . ' <=', date('Ymd', $data_final_timestamp));
			}
			
			$select[] = 'SUM(' . $this->_db_cliente['campos']['notas_fiscais']['valor_total'] . ') AS valor_total'; 
			
			if ($this->_db_cliente['campos']['notas_fiscais']['delecao'])
			{
				$this->db_cliente->where($this->_db_cliente['campos']['notas_fiscais']['delecao'], '');
			}
			
			$codigos_cliente = explode('|', $codigo_cliente);
			
			return $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['notas_fiscais'])->where(array($this->_db_cliente['campos']['notas_fiscais']['codigo_cliente'] => $codigos_cliente[0], $this->_db_cliente['campos']['notas_fiscais']['loja_cliente'] => $codigos_cliente[1]))->get()->result_array();
			
			//return $this->db_cliente->last_query();
			
		}
		else if($codigo_produto)
		{
			
			/*
			$select_itens[] = $this->_db_cliente['campos']['itens_notas_fiscais']['codigo_nota_fiscal'] . ' AS codigo_nota_fiscal'; 
			$codigos_notas = $this->db_cliente->select(implode(', ', $select_itens))->from($this->_db_cliente['tabelas']['itens_notas_fiscais'])->where($this->_db_cliente['campos']['itens_notas_fiscais']['codigo_produto'], $codigo_produto)->get()->result_array();
			
			if($codigos_notas)
			{
			
			
				//Trocar AND para OR, e colocar dentro de () - parênteses
				foreach($codigos_notas as $codigo_nota)
				{
					$where[] = $this->_db_cliente['campos']['notas_fiscais']['codigo'] . ' = ' . $codigo_nota['codigo_nota_fiscal'];
				}
				$_where = '(' . implode(' OR ', $where) . ')';
				
				
				
				if ($data_inicial_timestamp)
				{
					$this->db_cliente->where($this->_db_cliente['campos']['notas_fiscais']['data_emissao'] . ' >=', date('Ymd', $data_inicial_timestamp));
				}
				
				if ($data_final_timestamp)
				{
					$this->db_cliente->where($this->_db_cliente['campos']['notas_fiscais']['data_emissao'] . ' <=', date('Ymd', $data_final_timestamp));
				}
				
				$select[] = 'SUM(' . $this->_db_cliente['campos']['notas_fiscais']['valor_total'] . ') AS valor_total'; 
				
				if ($this->_db_cliente['campos']['notas_fiscais']['delecao'])
				{
					$this->db_cliente->where($this->_db_cliente['campos']['notas_fiscais']['delecao'], '');
				}
				
				
				return $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['notas_fiscais'])->where($_where)->get()->result_array();
				
				
				//return $this->db_cliente->last_query();
			}
			*/
			
			
			
			$tabelas = array('notas_fiscais', 'itens_notas_fiscais');
		
			foreach ($tabelas as $tabela)
			{
				$from[] = $this->_db_cliente['tabelas'][$tabela] . ' AS ' . $tabela;
			}
			
			//--
			
			$this->db_cliente->where('notas_fiscais.' .$this->_db_cliente['campos']['notas_fiscais']['codigo'] . ' = ' . 'itens_notas_fiscais.' .$this->_db_cliente['campos']['itens_notas_fiscais']['codigo_nota_fiscal']);
			
			$this->db_cliente->where('itens_notas_fiscais.' .$this->_db_cliente['campos']['itens_notas_fiscais']['codigo_produto'], $codigo_produto);
			
			if ($data_inicial_timestamp)
			{
				$this->db_cliente->where('notas_fiscais.' .$this->_db_cliente['campos']['notas_fiscais']['data_emissao'] . ' >=', date('Ymd', $data_inicial_timestamp));
			}
			
			if ($data_final_timestamp)
			{
				$this->db_cliente->where('notas_fiscais.' .$this->_db_cliente['campos']['notas_fiscais']['data_emissao'] . ' <=', date('Ymd', $data_final_timestamp));
			}
			
			if ($this->_db_cliente['campos']['notas_fiscais']['delecao'])
			{
				$this->db_cliente->where('notas_fiscais.' . $this->_db_cliente['campos']['notas_fiscais']['delecao'], '');
			}
			
			if ($this->_db_cliente['campos']['itens_notas_fiscais']['delecao'])
			{
				$this->db_cliente->where('itens_notas_fiscais.' . $this->_db_cliente['campos']['itens_notas_fiscais']['delecao'], '');
			}
			
			return $this->db_cliente->select('SUM(' . 'notas_fiscais.' . $this->_db_cliente['campos']['notas_fiscais']['valor_total'] . ') as valor_total')->from(implode(', ', $from))->get()->result_array();
			
			
			
			
			
		}
		else
		{
			return 'Erro';
		}
	
	}

	
	function obter_vendas($data_inicial_timestamp = NULL, $data_final_timestamp = NULL, $codigo_representante = NULL)
	{
			
		$tabelas = array('pedidos', 'itens_pedidos');
		
		foreach ($tabelas as $tabela)
		{
			$from[] = $this->_db_cliente['tabelas'][$tabela] . ' AS ' . $tabela;
		}
		
		$this->db_cliente->where('pedidos.' .$this->_db_cliente['campos']['pedidos']['codigo'] . ' = ' . 'itens_pedidos.' .$this->_db_cliente['campos']['itens_pedidos']['codigo_pedido']);
		
		if ($data_inicial_timestamp)
		{
			$this->db_cliente->where('pedidos.' .$this->_db_cliente['campos']['pedidos']['data_emissao'] . ' >=', date('Ymd', $data_inicial_timestamp));
		}
		
		if ($data_final_timestamp)
		{
			$this->db_cliente->where('pedidos.' .$this->_db_cliente['campos']['pedidos']['data_emissao'] . ' <=', date('Ymd', $data_final_timestamp));
		}
		
		if ($this->_db_cliente['campos']['pedidos']['delecao'])
		{
			$this->db_cliente->where('pedidos.' . $this->_db_cliente['campos']['pedidos']['delecao'], '');
		}
		
		if ($codigo_representante)
		{
			$this->db_cliente->where('pedidos.' .$this->_db_cliente['campos']['pedidos']['codigo_representante'] . ' = ' . $codigo_representante);
		}
		
		return $this->db_cliente->select('SUM(' . 'itens_pedidos.' . $this->_db_cliente['campos']['itens_pedidos']['valor_total_item'] . ') as valor_total_item')->from(implode(', ', $from))->get()->result_array();
		
		//return $this->db_cliente->last_query();
	}

	
	function obter_novos_clientes($codigo_representante = NULL, $data_inicial_timestamp = NULL, $data_final_timestamp = NULL, $estado = NULL)
	{
	
		if($codigo_representante)
		{
			if ($data_inicial_timestamp)
			{
				$this->db_cliente->where($this->_db_cliente['campos']['clientes']['data_cadastro'] . ' >=', date('Ymd', $data_inicial_timestamp));
			}
			
			if ($data_final_timestamp)
			{
				$this->db_cliente->where($this->_db_cliente['campos']['clientes']['data_cadastro'] . ' <=', date('Ymd', $data_final_timestamp));
			}
			
			if ($this->_db_cliente['campos']['clientes']['delecao'])
			{
				$this->db_cliente->where($this->_db_cliente['campos']['clientes']['delecao'], '');
			}
			
			return $this->db_cliente->select('COUNT(*) AS total')->from($this->_db_cliente['tabelas']['clientes'])->where(array(
				$this->_db_cliente['campos']['clientes']['codigo_representante'] => $codigo_representante
			))->get()->result_array();
			
			
			//return $this->db_cliente->last_query();
			
		}
		else if($estado)
		{
			
			if ($data_inicial_timestamp)
			{
				$this->db_cliente->where($this->_db_cliente['campos']['clientes']['data_cadastro'] . ' >=', date('Ymd', $data_inicial_timestamp));
			}
			
			if ($data_final_timestamp)
			{
				$this->db_cliente->where($this->_db_cliente['campos']['clientes']['data_cadastro'] . ' <=', date('Ymd', $data_final_timestamp));
			}
			
			
			return $this->db_cliente->select('COUNT(*) AS total')->from($this->_db_cliente['tabelas']['clientes'])->where(array(
				$this->_db_cliente['campos']['clientes']['estado'] => $estado
			))->get()->result_array();
			
		}
	}
	
	
	
	
	// TÍTULOS **
	
	// ** FORMATAÇÕES
	
	function formatar_cpf($cpf) {
		$cpf = ereg_replace('[^0-9]', '', $cpf);
		
		if (strlen($cpf) == 11)
		{
			$cpf = substr($cpf, 0, 3) . '.' . substr($cpf, 2, 3) . '.' . substr($cpf, 5, 3) . '-' . substr($cpf, 8, 2);
		}
		else if (strlen($cpf) == 14)
		{
			$cpf = substr($cpf, 0, 2) . '.' . substr($cpf, 2, 3) . '.' . substr($cpf, 5, 3) . '/' . substr($cpf, 8, 4) . '-' . substr($cpf, 12, 2);
		}
		
		return $cpf;
	}
	
	function formatar_cep($cep) {
		$cep = ereg_replace('[^0-9]', '', $cep);
		
		return substr($cep, 0, 5) . '-' . substr($cep, 5, 3);
	}
	
	// FORMATAÇÕES **
	
	function obter_produtos_auto($palavras_chave)
	{
		if ($this->_db_cliente['campos']['produtos']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['produtos']['delecao'], '');
		}
		
		if($palavras_chave)
		{
			$this->db_cliente->where('(UPPER(' . $this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['descricao'] . ') LIKE UPPER("%' . $palavras_chave . '%") OR UPPER(' . $this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['codigo'] . ') LIKE UPPER("%' . $palavras_chave . '%"))');
		}
		
		$produto = $this->db_cliente->select('B1_COD as codigo, B1_DESC as descricao, B1_IPI as ipi')->from($this->_db_cliente['tabelas']['produtos'])->get()->result_array();
		
		return ($produto);
	}
	
	function obterPrevisaoChegada($filial, $codigo)
	{
		if($filial && $codigo)
		{
			$this->db_cliente->select('C7_FILIAL filial, C7_PRODUTO produto, C7_DESCRI descricao, C7_QUANT quantidade, C7_CHEGADA chegada');
			$this->db_cliente->where(array('C7_IMPORT'=>'I','D_E_L_E_T_ !='=>'*','C7_FILIAL'=>$filial,'C7_PRODUTO'=> $codigo));
			$this->db_cliente->order_by('C7_CHEGADA');
			$chegada = $this->db_cliente->get('SC7010')->result();
			return $chegada;
		}
	}
	
	function obter_prospects($palavras_chave, $codigo_do_representante = NULL) {	
		$prospects = $this->db->select('id, nome, cpf')
			->from('prospects')
			->where('prospects.status !=', 'convertido_cliente')
			->get()->result_array();
		
		return $prospects;
	}
        
        
        //CUSTOM: Aristides - 20/06/2013
        //
        //
        function verificar_desconto_maximo($classe = NULL){
            
                $desconto_maximo_campo = $this->_db_cliente['campos']['descontos']['desconto_maximo'];
                $codigo_desconto = $this->_db_cliente['campos']['descontos']['codigo_desconto'];
                $tabela_descontos = $this->_db_cliente['tabelas']['descontos'];
                
                $classe = $this->db_cliente->select($desconto_maximo_campo)->from($tabela_descontos)->where($codigo_desconto, $classe)->get()->result();
                
                return $classe;
        }
        //
        //
        //FIM CUSTOM: Aristides - 20/06/2013
		
		
	function obter_filiais()
	{
		$filiais = $this->db_cliente
							->select($this->_db_cliente['campos']['filiais']['codigo_filial'] . ' AS codigo_filial')
							->select($this->_db_cliente['campos']['filiais']['descricao_filial'] . ' AS descricao_filial')
							->from($this->_db_cliente['tabelas']['filiais'])
							->where(array(
								$this->_db_cliente['campos']['filiais']['status'] => 'A'
							))
							->order_by($this->_db_cliente['campos']['filiais']['codigo_filial'])
							->get()->result();
							
		$retorno = array();
		
		foreach($filiais as  $filial)
		{
			$retorno[$filial->codigo_filial] = $filial->codigo_filial . ' - ' . $filial->descricao_filial;
		}
		
		return $retorno;
	}
        
        
	/**
	* Metódo:		obter_idped
	* 
	* Descrição:	Função utilizada para obter o novo  IDPED .
	* 
	* Data:			03/04/2012
	* Modificação:	03/04/2012
	* 
	* @access		public
	* @param		string 		$codigo_usuario	- Código do Usuário do Representante.
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function obter_idped($codigo_usuario){
	
		$this->db_cliente->order_by($this->_db_cliente['campos']['pedidos_dw']['id_pedido'], "desc"); 
	
		$ultimo_pedido = $this->db_cliente
				->select($this->_db_cliente['campos']['pedidos_dw']['id_pedido'])
				->from($this->_db_cliente['tabelas']['pedidos_dw'])
				->where($this->_db_cliente['campos']['pedidos_dw']['id_usuario'], $codigo_usuario)
				->get()->row_array();
			
                  
			if($ultimo_pedido)
			{
				$_id_pedido = explode('-', $ultimo_pedido[$this->_db_cliente['campos']['pedidos_dw']['id_pedido']]);
				$id_pedido = ltrim($codigo_usuario, 0) . '-' . str_pad($_id_pedido[1] + 1, 5, 0, STR_PAD_LEFT);
				//custom 746
				
				$this->db_cliente->order_by($this->_db_cliente['campos']['pedidos_dw']['id_pedido'], "desc"); 
	
				$verica_id = $this->db_cliente
						->select($this->_db_cliente['campos']['pedidos_dw']['id_pedido'])
						->from($this->_db_cliente['tabelas']['pedidos_dw'])
						->where($this->_db_cliente['campos']['pedidos_dw']['id_pedido'], $id_pedido)
						->get()->row_array();
				
				if($verica_id)
				{
					$id_pedido = ltrim($codigo_usuario, 0) . '-' . str_pad($_id_pedido[1] + 2, 5, 0, STR_PAD_LEFT);
				
					$this->db_cliente->order_by($this->_db_cliente['campos']['pedidos_dw']['id_pedido'], "desc"); 
		
					$verica_id = $this->db_cliente
							->select($this->_db_cliente['campos']['pedidos_dw']['id_pedido'])
							->from($this->_db_cliente['tabelas']['pedidos_dw'])
							->where($this->_db_cliente['campos']['pedidos_dw']['id_pedido'], $id_pedido)
							->get()->row_array();
					
					if($verica_id)
					{
						$id_pedido = ltrim($codigo_usuario, 0) . '-' . str_pad($_id_pedido[1] + 5, 5, 0, STR_PAD_LEFT);
					}
				}
				
				//fim custom 746
			}
			else
			{
				$id_pedido = ltrim($codigo_usuario, 0) . '-' . str_pad(1, 5, 0, STR_PAD_LEFT);
				//custom 746
				
				
				$verica_id = $this->db_cliente
						->select($this->_db_cliente['campos']['pedidos_dw']['id_pedido'])
						->from($this->_db_cliente['tabelas']['pedidos_dw'])
						->where($this->_db_cliente['campos']['pedidos_dw']['id_pedido'], $id_pedido)
						->get()->row_array();
				
				if($verica_id)
				{
					$id_pedido = ltrim($codigo_usuario, 0) . '-' . str_pad($id_pedido[1] + 2, 5, 0, STR_PAD_LEFT);
				
					$this->db_cliente->order_by($this->_db_cliente['campos']['pedidos_dw']['id_pedido'], "desc"); 
		
					$verica_id = $this->db_cliente
							->select($this->_db_cliente['campos']['pedidos_dw']['id_pedido'])
							->from($this->_db_cliente['tabelas']['pedidos_dw'])
							->where($this->_db_cliente['campos']['pedidos_dw']['id_pedido'], $id_pedido)
							->get()->row_array();
					
					if($verica_id)
					{
						$id_pedido = ltrim($codigo_usuario, 0) . '-' . str_pad($id_pedido[1] + 5, 5, 0, STR_PAD_LEFT);
					}
				}
				
				//fim custom 746
			}
		
			return $id_pedido;
	
	}		
	
	function obter_uf_filial($codigo_filial)
	{
		return	$this->db_cliente
					->select($this->_db_cliente['campos']['filiais']['estado_filial'])
					->from($this->_db_cliente['tabelas']['filiais'])
					->where($this->_db_cliente['campos']['filiais']['codigo_filial'], $codigo_filial)
					->get()
					->row_array();
	}
	
	public function verificar_pode_visitar($vendedor, $cliente) {
		$dados = $this->db_cliente			
				->from("GET_CLI_VEN('')")
				->where('A1_COD', $cliente)
				->where('A1_VEND', $vendedor)
				->get()
				->row();

		return $dados;
	}
	
	function obter_cliente_formulario($codigo, $loja)
	{
		$campos = array(
            'codigo', 'loja', 'tipo', 'nome', 'cpf', 'pessoa_contato', 'endereco', 
            'numero', 'bairro', 'cep', 'cidade', 'estado', 'ddd', 'telefone', 'email', 
            'limite_credito', 'total_titulos_em_aberto', 'pedidos_liberados', 
            'codigo_representante', 'data_cadastro', 'tabela_precos', 
            'categoria', 'tipo_cliente', 'cliente_sativ1'
        );

		foreach ($campos as $campo)
		{
			if (!$this->_db_cliente['campos']['clientes'][$campo]) continue;
			
			$select[] = $this->_db_cliente['campos']['clientes'][$campo] . ' AS ' . $campo;
		}
		
		if ($this->_db_cliente['campos']['clientes']['delecao'])
		{
			$this->db_cliente->where("{$this->_db_cliente['tabelas']['clientes']}.{$this->_db_cliente['campos']['clientes']['delecao']}", '');
		}
		
		$cliente = $this->db_cliente
						->select($select)
						->from($this->_db_cliente['tabelas']['clientes'])
						->where(array(
							$this->_db_cliente['campos']['clientes']['codigo'] => $codigo, 
							$this->_db_cliente['campos']['clientes']['loja'] => $loja
						))
						->get()
						->row();
		
		$ultimo_pedido = $this->obter_ultimo_pedido_cliente($codigo, $loja);
		
		$dados = array_map('trim', array(
			'status' => time() - $ultimo_pedido['emissao_timestamp'] <= 3600 * 24 * 180 ? 'Ativo' : 'Inativo',
			'tipo' => $cliente->tipo,
			'codigo' => $cliente->codigo,
			'loja' => $cliente->loja,
			'nome' => $cliente->nome,
			'cpf' => $cliente->cpf,
			'pessoa_contato' => $cliente->pessoa_contato,
			'endereco' => $cliente->endereco,
			'numero' => $cliente->numero,
			'bairro' => $cliente->bairro,
			'cep' => $cliente->cep,
			'cidade' => $cliente->cidade,
			'estado' => $cliente->estado,
			'ddd' => $cliente->ddd,
			'telefone' => $cliente->telefone,
			'email' => $cliente->email,
			'rg' => $cliente->rg,
			'limite_credito' => $cliente->limite_credito,
			'total_titulos_em_aberto' => $cliente->total_titulos_em_aberto,
			'pedidos_liberados' => $cliente->pedidos_liberados,
			'codigo_representante' => $cliente->codigo_representante,
			'data_cadastro_timestamp' => strtotime($cliente->data_cadastro),
			'tabela_precos' => $cliente->tabela_precos,
			'categoria' => $cliente->categoria,
			'tipo_cliente' => $cliente->tipo_cliente,
			'cliente_xtpcli' => $cliente->cliente_xtpcli,
			'cliente_sativ1' => $cliente->cliente_sativ1
		));
		
		$dados = array_merge($dados, array(
			'ultimo_pedido' => $ultimo_pedido,
			'total_titulos_vencidos' => $this->obter_total_titulos_vencidos_cliente($codigo, $loja),
			'descricao_forma_pagamento' => $ultimo_pedido['descricao_forma_pagamento'],
			'codigo_pedido' => $ultimo_pedido['codigo_pedido']
		));
		
		
		/* Permite inclusão de Formulário de visitas 	*/
		$codigo_usuario = '';
		if(!in_array(trim($this->session->userdata('grupo_usuario')), array('gestores_comerciais'))){
			$codigo_usuario = trim($this->session->userdata('codigo_usuario'));
		}
		
		$dados['permite_inclusao_formulario'] = $permite_inclusao_formulario;
		return $dados;
	}
	
	
}