<?php

class Shazam_model extends CI_Model {
	
    function __construct() {
        parent::__construct();
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->config->load('db_cliente_' . $this->db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');
    }
	
	function obter_shazam_vendedor($codigo_vendedor) {
		return $this->db_cliente->query("SELECT * FROM GET_QTD_SHAZAM(" . $codigo_vendedor . ", 'E', 'N')")->row();
	}
	
	function retirar_um_shazam_vendedor($codigo_vendedor) {
		return $this->db_cliente->query('EXEC RETIRA_SHAZAM  \'' . $codigo_vendedor . '\'')->row();
	}
	
	function buscar_descontos_shazam($codigo_cliente, $codigo_vendedor, $codigo_produtos, $valor_total_pedido) {
		return $this->db_cliente->query('EXEC PRD_SHAZAM \'' . $codigo_cliente . '\',\'' . $codigo_vendedor . '\',\'' . $codigo_produtos . '\',1,' . $valor_total_pedido . '')->result();
	}
	
	function estado_filial($codigo_filial) {
		$dados = array(
			'03' => 'MG',
			'04' => 'SC',
			'05' => 'SP',
			'06' => 'PE'
		);
		return $dados[$codigo_filial];
	}
	
	function aplicar_descontos_shazam($sessao_pedido) {
	
		/* Busca os Descontos */
		$produtos = array();
		$total = 0;
		foreach($sessao_pedido['produtos'] as $key => $produto) {
			$produtos[] = $produto['codigo'];
			$total += $produto['quantidade_pedido'] * $produto['preco'];
		}
		foreach($sessao_pedido['produtos_programados'] as $key => $produto) {
			$produtos[] = $produto['codigo'];
			$total += $produto['quantidade_pedido'] * $produto['preco'];
		}
		$descontos = $this->buscar_descontos_shazam($sessao_pedido['cliente']['codigo'], $sessao_pedido['cliente']['codigo_representante'], implode(',', $produtos), $total);
		
		/* Processa os Descontos */
		foreach($descontos as $desconto) {
			foreach(array('produtos', 'produtos_programados') as $grupoProdutos) {
				foreach($sessao_pedido[$grupoProdutos] as $key => $produto) {
					if ($desconto->PRODS == $produto['codigo']) {
						if (($sessao_pedido['cliente']['tipo_cliente'] == 'XXX') or ($sessao_pedido['cliente']['tipo_cliente'] == 'CON') or ($sessao_pedido['cliente']['estado'] == $this->estado_filial($produto['filial']))) {
							$tipoUsado = 'E';
						} else {
							$tipoUsado = 'I';
						}
						if ($desconto->Z40_TIPO == $tipoUsado) {
							$sessao_pedido[$grupoProdutos][$key]['preco'] = $desconto->PRECO;
							$sessao_pedido['vencimento_shazam'] = $desconto->VCTO; //coloca vencimento no cabecalho do pedido
						}
					}
				}
			}
		}
		$this->retirar_um_shazam_vendedor($sessao_pedido['cliente']['codigo_representante']);
		return $sessao_pedido;
	}
}