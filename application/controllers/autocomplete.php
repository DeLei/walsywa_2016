<?php

class Autocomplete extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	
	function index($funcao = NULL)
	{
		$dados = array();
		$funcao = '_' . $funcao;
		
		if (method_exists('Autocomplete', $funcao))
		{
			$itens = $this->$funcao();
			
			foreach ($itens as $indice => $valor)
			{
				$dados[] = array('label' => $valor, 'value' => $indice);
			}
		}
		
		echo json_encode($dados);
	}
	
	function _obter_usuarios()
	{
		$usuarios = $this->db->from('usuarios')->where('status', 'ativo')->order_by('grupo', 'desc')->order_by('nome', 'asc')->get()->result();
		$_usuarios = array();
		
		foreach ($usuarios as $usuario)
		{
			$_usuarios[$usuario->id] = $usuario->nome . ' (' . element($usuario->grupo, $this->_obter_grupos()) . ')';
		}
		
		return $_usuarios;
	}
	
	function _obter_representantes()
	{
		$representantes = $this->db->from('usuarios')->where(array('status' => 'ativo', 'grupo' => 'representantes'))->order_by('nome', 'asc')->get()->result();
		$_representantes = array();
		
		foreach ($representantes as $representante)
		{
			$_representantes[$representante->id] = $representante->nome;
		}
		
		return $_representantes;
	}
	
	function _obter_representantes_db_cliente()
	{
		$representantes = $this->db_cliente->obter_representantes();
		$_representantes = array();
		
		foreach ($representantes as $representante)
		{
			$_representantes[$representante['codigo']] = $representante['codigo'] . ' - ' . $representante['nome'];
		}
		
		return $_representantes;
	}
	
	function _obter_grupos()
	{
		return array('administradores' => 'Administradores', 'gestores_comerciais' => 'Gestores Comerciais', 'gestores_financeiros' => 'Gestores Financeiros', 'representantes' => 'Representantes', 'prepostos' => 'Prepostos', 'supervisores' => 'Supervisores');
	}
	
	function _obter_clientes()
	{
		$clientes = $this->db_cliente->obter_clientes($this->session->userdata('codigo_usuario'));
		$_clientes = array();
		
		foreach ($clientes as $cliente)
		{
			$_clientes[$cliente['codigo'] . '|' . $cliente['loja']] = $cliente['nome'] . ' - ' . $cliente['cpf'];
		}
		
		return $_clientes;
	}
	
	function _obter_clientes_consultar_pedidos()
	{
		$clientes = $this->db_cliente->obter_clientes($this->session->userdata('codigo_usuario_consultar_pedidos'));
		$_clientes = array();
		
		foreach ($clientes as $cliente)
		{
			$_clientes[$cliente['codigo'] . '|' . $cliente['loja']] = $cliente['nome'] . ' - ' . $cliente['cpf'];
		}
		
		return $_clientes;
	}
	
	function _obter_clientes_consultar_orcamentos()
	{
		$clientes = $this->db_cliente->obter_clientes($this->session->userdata('codigo_usuario_consultar_orcamentos'));
		$_clientes = array();
		
		foreach ($clientes as $cliente)
		{
			$_clientes[$cliente['codigo'] . '|' . $cliente['loja']] = $cliente['nome'] . ' - ' . $cliente['cpf'];
		}
		
		return $_clientes;
	}
	
	function _obter_clientes_consultar_notas_fiscais()
	{
		$clientes = $this->db_cliente->obter_clientes($this->session->userdata('codigo_usuario_consultar_notas_fiscais'));
		$_clientes = array();
		
		foreach ($clientes as $cliente)
		{
			$_clientes[$cliente['codigo'] . '|' . $cliente['loja']] = $cliente['nome'] . ' - ' . $cliente['cpf'];
		}
		
		return $_clientes;
	}
	
	function _obter_formas_pagamento()
	{
		$formas_pagamento = $this->db_cliente->obter_formas_pagamento();
		$_formas_pagamento = array();
		
		foreach ($formas_pagamento as $forma_pagamento)
		{
			$_formas_pagamento[$forma_pagamento['codigo']] = $forma_pagamento['descricao'];
		}
		
		return $_formas_pagamento;
	}
	
	function _obter_transportadoras()
	{
		$transportadoras = $this->db_cliente->obter_transportadoras();
		$_transportadoras = array();
		
		foreach ($transportadoras as $transportadora)
		{
			$_transportadoras[$transportadora['codigo']] = $transportadora['nome'];
		}
		
		return $_transportadoras;
	}
	
	function _obter_tabelas_precos()
	{
		$tabelas_precos = $this->db_cliente->obter_tabelas_precos();
		$_tabelas_precos = array();
		
		foreach ($tabelas_precos as $tabela_precos)
		{
			$_tabelas_precos[$tabela_precos['codigo']] = $tabela_precos['descricao'];
		}
		
		return $_tabelas_precos;
	}
	
	function _obter_produtos()
	{
		$produtos = $this->db_cliente->obter_produtos();
		$_produtos = array();
		
		foreach ($produtos as $produto)
		{
			$_produtos[$produto['codigo']] = $produto['descricao'];
		}
		
		return $_produtos;
	}
	
}