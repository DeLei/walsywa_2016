<?php

class Rnc extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('autocomplete');
		$this->load->model('db_rnc');
		ini_set('display_errors', 1);
	}
	
	
	function ver_detalhes($codigo = null){
		if(is_null($codigo)){
			redirect('rnc/listagem');
			
		}
		
		
		$rnc = $this->db_rnc->obter_rnc($codigo);
		
		$this->load->view('layout', array('conteudo' => $this->load->view('rnc/ver_detalhes', array(
			'rnc'					=>		$rnc,
			'statusList'			=>		$this->db_rnc->get_status(),
			'filiaisList'			=> 		$this->_obter_filiais(),
			'tipoList' 				=> 		$this->db_rnc->get_tipos(),
			'responsavelTipoList' 	=> 		$this->db_rnc->get_responsavel_tipos(),
			), TRUE)));
	}
	
	
	function index($codigo = null) 
	{
		if($codigo)
		{
			$rnc = $this->db_rnc->obter_rnc($codigo);
		}
		
		if($this->input->post() || $codigo)
		{	
			
			if($codigo)
			{
				$_codigo_loja_cliente = $rnc->codigo_cliente . ' - ' . $rnc->nome_cliente;
				$codigo_cliente = $rnc->codigo_cliente;
				$loja_cliente = $rnc->loja_cliente;
				$nome_cliente = $rnc->nome_cliente;
				$codigo_nota_fiscal = $rnc->codigo_nota_fiscal;
				$serie_nota_fiscal = $rnc->serie_nota_fiscal;
			}
			else
			{
				$_codigo_loja_cliente = $this->input->post('_codigo_loja_cliente');
				$cliente = explode('|', $this->input->post('codigo_loja_cliente'));
				$codigo_cliente = $cliente[0];
				$loja_cliente = $cliente[1];
				
				$cliente = explode('-', $this->input->post('_codigo_loja_cliente'));
				$nome_cliente = $cliente[1];
				
				
				$nota_fiscal_serie = explode('|', $this->input->post('codigo_nota_fiscal'));
				$codigo_nota_fiscal = $nota_fiscal_serie[0];
				$serie_nota_fiscal = $nota_fiscal_serie[1];
			}
			
			
			
			$dados_formulario = array(
				'filial'				=>	($rnc->filial ? $rnc->filial : $this->input->post('filial')),
				'codigo'				=>	($rnc->codigo ? $rnc->codigo : null),
				'data_inclusao'			=>	date('Ymd'),
				'data_incidente'		=>	($rnc->data_incidente ? $rnc->data_incidente : data2protheus($this->input->post('data_incidente'))),
				'_codigo_loja_cliente'	=>	$_codigo_loja_cliente,
				'codigo_loja_cliente'	=>  $codigo_cliente,
				'codigo_cliente'		=>	$codigo_cliente,
				'loja_cliente'			=>	$loja_cliente,
				'nome_cliente'			=> 	$nome_cliente,
				'pedido'				=>	($rnc->pedido ? $rnc->pedido : $this->input->post('pedido')),
				'_pedido'				=>	($rnc->pedido ? $rnc->pedido : $this->input->post('pedido')),
				'codigo_nota_fiscal'	=>	($rnc->codigo_nota_fiscal ? $rnc->codigo_nota_fiscal : $codigo_nota_fiscal),
				'serie_nota_fiscal'		=>	($rnc->serie_nota_fiscal ? $rnc->serie_nota_fiscal : $serie_nota_fiscal),				
				'descricao'				=>	($this->input->post('descricao') ? $this->input->post('descricao') : $rnc->descricao),
				'status'				=>	'A', // A Walsywa ainda não definiu quais serão os status. Então, por hora todos serão 'A' de 'Aberto'.
				'codigo_representante'	=> 	$this->session->userdata('codigo_usuario'),	
				'responsavel'			=> 	($this->session->userdata('nome_usuario'))?$this->session->userdata('nome_usuario'):$this->session->userdata('nome_real_usuario'),			
				'erros'					=>	array()
			);
			
			
			
			$dados_formulario = $this->validar_campos_obrigatorios($dados_formulario);
			$dados_formulario = $this->validar_tamanhos($dados_formulario);
			
			if(count($dados_formulario['erros']) <= 0 && $this->input->post())
			{
				$this->db_rnc->gravar_rnc($dados_formulario);
				redirect('rnc/listagem');
			}
		}
		
		$this->load->view('layout', array('conteudo' => $this->load->view('rnc/formulario', array(
			'codigo_usuario' 	=>	$this->session->userdata('codigo_usuario'),
			'dados_formulario'	=>	$dados_formulario,
			'filialList'		=> 	$this->_obter_filiais()
		), TRUE)));
	}
	
	
	function obter_pedidos_cliente(){
		$codigo_cliente = $this->input->get('codigo_loja_cliente');		
		$filial = $this->input->get('filial');
		$term = trim($this->input->get('term'));
		
		$pedidos = $this->db_rnc->obter_pedidos_cliente($filial, $codigo_cliente, $term);
		
		$dados = array();
		 foreach ($pedidos as $pedido) {
			
		 
            $dados[] = array('label' => trim($pedido['codigo'])   , 'value' => trim($pedido['codigo']),  'item' => $pedido);
        }
		
		echo json_encode($dados);
	}
	
	
	function obter_notas_fiscais_clientes(){
	
		$codigo_cliente = $this->input->get('codigo_loja_cliente');
		$filial = $this->input->get('filial');
		$term = trim($this->input->get('term'));
		
		
		$pedidos = $this->db_rnc->obter_notas_fiscais_cliente($filial, $codigo_cliente, $term);
		
		$dados = array();
		foreach ($pedidos as $pedido) {
			
		 
            $dados[] = array('label' => trim($pedido['nota_fiscal']) .'/'.$pedido['serie']  , 'value' => trim($pedido['nota_fiscal']),  'item' => $pedido);
        }
		
		echo json_encode($dados);
	
	}
	
	
	
	function validar_campos_obrigatorios($dados_formulario)
	{
		if(!$dados_formulario['data_incidente'])
		{
			array_push($dados_formulario['erros'], "Por favor, informe a data do incidente.");
		}		
		
		if(!$dados_formulario['codigo_cliente'])
		{
			array_push($dados_formulario['erros'], "Por favor, informe o cliente.");
		}			
		
				
		
		if(!$dados_formulario['pedido'])
		{
			array_push($dados_formulario['erros'], "Por favor, informe o pedido.");
		}
		
		
		if(!$dados_formulario['codigo_nota_fiscal'])
		{
			array_push($dados_formulario['erros'], "Por favor, informe a nota fiscal.");
		}
		
		
		if(!$dados_formulario['descricao'])
		{
			array_push($dados_formulario['erros'], "Por favor, informe a descrição do incidente.");
		}
		
		return $dados_formulario;
	}
	
	function validar_tamanhos($dados_formulario)
	{
		
		
		if(strlen($dados_formulario['descricao']) > 250)
		{
			array_push($dados_formulario['erros'], "A descrição do incidente não deve possuir mais de 250 caracteres.");
		}		
		
		return $dados_formulario;
	}
	
	function listagem()
	{
	
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->_db_cliente = $this->config->item('db_cliente');
		
		$filtros = $this->get_filtros();
		
		if($_POST || $this->input->get('my_submit'))
		{
		
			if (!$this->input->get('ordenar_ms'))
			{
				$_GET['ordenar_ms'] = 'ZL_CODIGO';
				$_GET['ordenar_ms_tipo'] = 'desc';
			}
			
			if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores')))
			{
				$this->db_cliente->where('ZL_VENDEDO', $this->session->userdata('codigo_usuario'));
			}
			
			$this->filtragem_mssql($filtros);			
		
			$relatorios = $this->db_rnc->obter_relatorios($this->db_cliente, $this->input->get('per_page'));
			
			if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores')))
			{
				$this->db_cliente->where('ZL_VENDEDO', $this->session->userdata('codigo_usuario'));
			}
						
			$this->filtragem_mssql($filtros, FALSE);
			
			$total = $this->db_rnc->obter_relatorios($this->db_cliente);
			
			
			
		}
		
		$this->load->view('layout', array('conteudo' => $this->load->view('rnc/listagem', array(
			'codigo_usuario' 			=>		$codigo_usuario,
			'relatorios'				=>		$relatorios,
			'statusList'				=>		$this->db_rnc->get_status(),
			'filtragem' 				=>		$this->filtragem($filtros),
			'paginacao' 				=>		$this->paginacao($total['quantidade']),
			'total'						=>		$total
		), TRUE)));
	}
	
	function get_filtros()
	{		
		$this->load->model('db_formulario_visita');
	
		$filtros = array(
			
			array(
				'nome' => 'filial',
				'descricao' => 'Filial',
				'tipo' => 'opcoes',
				'opcoes'=>  array('todos' => 'TODAS') + $this->_obter_filiais(),
				'campo_mssql' => $this->_db_cliente['campos']['rnc']['filial'],
				
			),
			array(
				'nome' => 'status',
				'descricao' => 'Status',
				'tipo' => 'opcoes',
				'campo_mssql' =>   $this->_db_cliente['campos']['rnc']['codigo'],
				'opcoes' => array('todos' => 'TODOS') + $this->db_rnc->get_status(),
				'ordenar_ms' => 0
			),
			
			array(
				'nome' => 'codigo',
				'descricao' => 'Código RNC',
				'tipo' => 'texto',
				'campo_mssql' => $this->_db_cliente['campos']['rnc']['codigo'],
				'ordenar_ms' => 1
			),
			in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) ?array(
				'nome' => 'codigo_representante',
				'descricao' => 'Representante',
				'tipo' => 'opcoes',
				'campo_mssql' => $this->_db_cliente['campos']['rnc']['codigo_representante'],
				'opcoes' => $this->db_formulario_visita->get_representantes()
			) : null,
			array(
				'nome' => 'data_incidente',
				'descricao' => 'Data do Incidente',
				'tipo' => 'texto',
				'campo_mssql' => $this->_db_cliente['campos']['rnc']['data_incidente'],
				'ordernar_ms' => in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) ? 3: 2
			),	
			array(
				'nome' => 'codigo_cliente',
				'descricao' => 'Cód. Cliente',
				'tipo' => 'texto',
				'campo_mssql' => $this->_db_cliente['campos']['rnc']['codigo_cliente'],
				
			),			
			array(
				'nome' => 'nome_cliente',
				'descricao' => 'Cliente',
				'tipo' => 'texto',
				'campo_mssql' => $this->_db_cliente['campos']['rnc']['nome_cliente'],
				'ordenar_ms' => (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')))? 5: 4
			),
			
			array(
				'nome' => 'pedido',
				'descricao' => 'Pedido',
				'tipo' => 'texto',
				'campo_mssql' => $this->_db_cliente['campos']['rnc']['pedido'],
				'ordenar_ms'	=> 2
			),
			array(
				'nome' => 'nota_fiscal',
				'descricao' => 'Nota Fiscal',
				'tipo' => 'texto',
				'campo_mssql' => $this->_db_cliente['campos']['rnc']['codigo_nota_fiscal'],
				'ordenar_ms'	=> 3
			),
			
			array(
				'nome' => 'emissao',
				'descricao' => 'Emissão',
				'tipo' => 'data',
				'campo_mssql' => $this->_db_cliente['campos']['rnc']['data_emissao'],
				
			)
		);
		
		return $filtros;
	}
	
	function excluir($codigo) 
	{
		$this->db_rnc->excluir_relatorio($codigo);
		redirect('rnc/listagem');
	}
	
	
	function obter_clientes($codigo_do_representante = NULL) {
        $palavras_chave = addslashes($_GET['term']);

        $dados = array();
		
		if(in_array($this->session->userdata('grupo_usuario'),array('gestores_comerciais'))){
			$codigo_do_representante = 0;
		}
		
		
        $clientes = $this->autocomplete->obter_clientes($palavras_chave, $codigo_do_representante);

        foreach ($clientes as $cliente) {
            $dados[] = array('label' => trim($cliente['codigo']) . ' - ' . trim(exibir_texto($cliente['nome'])) . ' - ' . trim($cliente['cpf']), 'value' => trim($cliente['codigo']) );
        }

        echo json_encode($dados);
    }
	
}