<?php

class Noticias extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	
	function ver_detalhes($id = NULL)
	{
		if (!$id)
		{
			echo 'Notícia não encontrada.';
			//redirect('noticias');	
		}
		
		$noticia = $this->db->from('noticias')->where('id', $id)->get()->row();
		
		echo '<div style="width: 600px; height: 300px;">
				<h2 style="margin: 0;">' . $noticia->titulo . '</h2>
				<p style="margin: 0;">' . date('d/m/Y H:i', $noticia->timestamp) . '</p>'.
				($noticia->link_imagem ? '<img src="' .base_url().$noticia->link_imagem . '" style="float: left; margin-top: 10px; margin-right: 10px;" />' : NULL)
				.'<div style="margin-top: 10px;">' . $noticia->conteudo . '</div>
			</div>';
	}
	
	function index()
	{	
		$filtros = array(
            array(
                'nome' => 'status',
                'descricao' => 'Status',
                'tipo' => 'opcoes',
                'opcoes' => array('todos' => 'Todas', 'ativa' => 'Ativa', 'inativa' => 'Inativa'),
                'campo_mysql' => 'status',
                'ordenar' => 1
            ),
			array(
                'nome' => 'criacao',
                'descricao' => 'Criação',
                'tipo' => 'data',
                'campo_mysql' => 'timestamp',
                'ordenar' => 0
            ),
            array(
                'nome' => 'titulo',
                'descricao' => 'Título',
                'tipo' => 'texto',
                'campo_mysql' => 'titulo',
                'ordenar' => 2
            )
        );
        
		if($_GET['my_submit']){
			if(!in_array($this->session->userdata('grupo_usuario'),array('gestores_comerciais', 'gestores_financeiros'))){
					$this->db->where('status','ativa');
			}
		
			$this->filtragem_mysql($filtros);
			$noticias = $this->db->from('noticias')->order_by('timestamp', 'desc')->limit(20, $this->input->get('per_page'))->get()->result();
		
			if(!in_array($this->session->userdata('grupo_usuario'),array('gestores_comerciais', 'gestores_financeiros'))){
					$this->db->where('status','ativa');
			}
			$this->filtragem_mysql($filtros);
			$total = $this->db->from('noticias')->get()->num_rows();
			$paginacao = $this->paginacao($total);
			
			foreach ($noticias as $noticia){
				if(in_array($this->session->userdata('grupo_usuario'),array('gestores_comerciais', 'gestores_financeiros'))){
					$noticia->link_imagem = ' | ' . anchor('noticias/editar/' . $noticia->id, 'Editar');// . ' | ') . anchor('noticias/excluir/' . $noticia->id, 'Excluir');
					$noticia->link_imagem .= ' | <a href="#" onclick="excluir('.$noticia->id.')">Excluir</a>';
				}else{
					$noticia->link_imagem = '';
				}
				//$noticia->status = element($noticia->status, $this->_obter_status());
			}
			
			$paginacao .= '
				<script type="text/javascript">
					function excluir(id) {
						var exc = confirm("Tem certeza que deseja realmente excluir?");
						if (exc)
							window.location = ("'.base_url().'index.php/noticias/excluir/"+id);
					}
				</script>
				';
			
		}
	
		$this->load->view('layout', array('conteudo' => $this->load->view('noticias/index', array('noticias' => $noticias, 'filtragem' => $this->filtragem($filtros), 'paginacao' => $paginacao,'total'=>$total), TRUE)));
	}
	
	function criar()
	{
		// ** VALIDAR FORM
		if ($_POST)
		{
			if (!$this->input->post('titulo'))
			{
				$erro = 'Digite um título.';
			}
			else if (!$this->input->post('conteudo'))
			{
				$erro = 'Digite um conteudo.';
			}
			else
			{
				$this->load->library('upload', array(
					'upload_path' => dirname($this->input->server('SCRIPT_FILENAME')) . '/uploads/',
					'allowed_types' => 'gif|jpg|png'
				));
				
				// $_FILES['imagem']['size'] serve para checar se o usuário tentou um upload
				if ($_FILES['imagem']['size'] && !$this->upload->do_upload('imagem'))
				{
					$erro = 'Selecione uma imagem válida.';
				}
				else
				{
					$dados_upload = $this->upload->data();
					
					if ($dados_upload) {
						$this->load->library('image_lib', array('source_image' => $dados_upload['full_path'], 'width' => 240, 'height' => 120)); 
						
						$this->image_lib->resize();
						
						if($dados_upload['file_name'])
						{
							$link_imagem =  'uploads/' . $dados_upload['file_name'];
						}
					}
					
					$this->db->insert('noticias', array(
						'timestamp' => time(),
						'status' => 'ativa',
						'link_imagem' => $link_imagem,
						'titulo' => $this->input->post('titulo'),
						'conteudo' => $this->input->post('conteudo')
					));
					
					redirect('noticias');
				}
			}
		}
		// VALIDAR FORM **
		$conteudo = '
			<div class="caixa">
				<ul>
					<li>' . anchor('noticias', 'Voltar', 'onclick="history.go(-1); return false;"') . '</li>
				</ul>
			</div>
		';
		// ** EXIBIR FORM
		$conteudo .= heading('Cadastrar Notícia', 2);
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		$conteudo .= form_open_multipart(current_url());
		$conteudo .= '<p>' . form_label('Imagem:' . br() . form_upload('imagem')) . '</p>';
		$conteudo .= '<p>' . form_label('Título:' . br() . form_input('titulo', $this->input->post('titulo'))) . '</p>';
		$conteudo .= '<p>' . form_label('Conteúdo:' . br() . form_textarea(array('class' => 'editor', 'cols' => 60, 'rows' => 8, 'name' => 'conteudo', 'value' => $this->input->post('conteudo')))) . '</p>';
		$conteudo .= '<p>' . form_submit('concluir', 'Concluir') . ' ou ' . anchor('noticias', 'Cancelar') . '</p>';
		$conteudo .= form_close();
		// EXIBIR FORM **
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function editar($id = NULL)
	{
		if (!$id)
		{
			redirect();	
		}
		
		$noticia = $this->db->from('noticias')->where('id', $id)->get()->row();
		
		// ** VALIDAR FORM
		if ($_POST)
		{
			if (!$this->input->post('titulo'))
			{
				$erro = 'Digite um título.';
			}
			else if (!$this->input->post('conteudo'))
			{
				$erro = 'Digite um conteudo.';
			}
			else
			{
				/*
				$this->load->library('upload', array(
					'upload_path' => dirname($this->input->server('SCRIPT_FILENAME')) . '/uploads/',
					'allowed_types' => 'gif|jpg|png'
				));
				 */
				$config['upload_path']= 'uploads/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size'] = 0;
				$config['max_width'] = 0;
				$config['max_height'] = 0;
				$config['encrypt_name'] = TRUE;
				
				$this->load->library('upload',$config);
				
				// $_FILES['imagem']['size'] serve para checar se o usuário tentou um upload
				if ($_FILES['imagem']['size'] && !$this->upload->do_upload('imagem'))
				{
					$erro = 'Selecione uma imagem válida.';
				}
				else
				{
					$dados_upload = $this->upload->data();
					
					if ($dados_upload) {
						$this->load->library('image_lib', array('source_image' => $dados_upload['full_path'], 'width' => 240, 'height' => 120)); 
						
						$this->image_lib->resize();
						
						if($dados_upload['file_name'])
						{
							$link_imagem = 'uploads/' . $dados_upload['file_name'];
						}
					}
					
					$this->db->update('noticias', array(
						'status' => $this->input->post('status'),
						'link_imagem' => $link_imagem ? $link_imagem : $noticia->link_imagem,
						'titulo' => $this->input->post('titulo'),
						'conteudo' => $this->input->post('conteudo')
					), array('id' => $id));
					
					redirect('noticias');
				}
			}
		}
		else
		{
			foreach ($noticia as $indice => $valor)
			{
				$_POST[$indice] = $valor;
			}
		}
		// VALIDAR FORM **
		$conteudo .= '
			<div class="caixa">
				<ul>
					<li>' . anchor('noticias', 'Voltar', 'onclick="history.go(-1); return false;"') . '</li>
				</ul>
			</div>
		';
		// ** EXIBIR FORM
		$conteudo .= heading('Editar Notícia', 2);
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		$conteudo .= form_open_multipart(current_url());
		$conteudo .= '<p>' . form_label('Status:' . br() . form_dropdown('status', $this->_obter_status(), $this->input->post('status'))) . '</p>';
		$conteudo .= '<p>' . form_label('Imagem:' . br(). form_upload('imagem',$config)) . '</p>';
		$conteudo .= '<p>' . form_label('Título:' . br() . form_input('titulo', $this->input->post('titulo'))) . '</p>';
		$conteudo .= '<p>' . form_label('Conteúdo:' . br() . form_textarea(array('class' => 'editor', 'cols' => 60, 'rows' => 8, 'name' => 'conteudo', 'value' => $this->input->post('conteudo')))) . '</p>';
		//$conteudo .= '<p>' . 'Conteúdo:' . br() . '<div dojoType="dijit.Editor" extraPlugins="[\'foreColor\', \'fontSize\']" onchange="$(\'input[name=conteudo]\').val(arguments[0]);" style="height: 200px;">' . $this->input->post('conteudo') . '</div>' . form_hidden('conteudo', $this->input->post('conteudo')) . '</p>';
		$conteudo .= '<p>' . form_submit('concluir', 'Concluir') . ' ou ' . anchor('noticias', 'Cancelar') . '</p>';
		$conteudo .= form_close();
		// EXIBIR FORM **
		
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function excluir($id = NULL)
	{
		$noticia = $this->db->from('noticias')->where('id', $id)->get()->row();
		
		$this->db->delete('noticias', array('id' => $noticia->id));
		
		if($noticia->link_imagem)
			@unlink($noticia->link_imagem);
		
		echo '<script>window.history.back()</script>';
	}
	
	//
	
	function _obter_status()
	{
		return array('ativa' => 'Ativa', 'inativa' => 'Inativa');
	}
	
}