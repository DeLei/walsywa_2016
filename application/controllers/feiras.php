<?php

class Feiras extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	
	function relatorio()
	{
		// TODO ROBSON
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function index()
	{
		$feiras = $this->db->from('feiras')->get()->result();
		
		$conteudo = heading('Eventos', 2);
		
		if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros'))) {
			$conteudo .= '<p>' . anchor('feiras/criar', 'Cadastrar') . '</p>';
		}
		
		if($feiras)
		{
			$conteudo .= '<div style="width: 800px;">
							<table cellspacing="0" class="novo_grid" style="margin-top: 10px;">
								<thead>
									<tr>
										<th>Criação</th>
										<th>Status</th>
										<th>Imagem</th>
										<th>Nome</th>
										<th>Início</th>
										<th>Opções</th>
										</tr>
									</thead>
								<tbody>';
			
			foreach ($feiras as $feira)
			{
				$conteudo .= '<tr>
								<td class="center">' . date('d/m/Y', $feira->timestamp) . '</td>
								<td>' . element($feira->status, $this->_obter_status()) . '</td>
								<td class="center">' . ($feira->link_imagem ? '<img src="' . $feira->link_imagem . '" style="border: 2px solid #ddd;" />' : 'não há') . '</td>
								<td>' . $feira->nome . '</td><td class="center">' . date('d/m/Y', $feira->data_inicial_timestamp) . '</td>
								<td class="center">' . anchor('feiras/ver_detalhes/' . $feira->id, 'Relatório') . ' | ' . anchor('feiras/editar/' . $feira->id, 'Editar') . '</td>
							</tr>';
			}
			
			$conteudo .= '</tbody></table></div>';
		}
		else $conteudo .= '<br>Nenhum evento foi cadastrado.';
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function criar()
	{
		// ** VALIDAR FORM
		if ($_POST)
		{
			//$erro = hnordt_validar_intervalo_datas('Data', 'inicial', TRUE);
			$erro = hnordt_validar_uma_data('Data', NULL, TRUE);
			if ($erro)
			{
				//
			}
			else if (!$this->input->post('nome'))
			{
				$erro = 'Digite um nome.';
			}
			else
			{
				$this->load->library('upload', array(
					'upload_path' => dirname($this->input->server('SCRIPT_FILENAME')) . '/uploads/',
					'allowed_types' => 'gif|jpg|png'
				));
				
				// $_FILES['imagem']['size'] serve para checar se o usuário tentou um upload
				if ($_FILES['imagem']['size'] && !$this->upload->do_upload('imagem'))
				{
					$erro = 'Selecione uma imagem válida.';
				}
				else
				{
					$dados_upload = $this->upload->data();
					
					if ($dados_upload) {
						$this->load->library('image_lib', array('source_image' => $dados_upload['full_path'], 'width' => 100, 'height' => 100)); 
						
						$this->image_lib->resize();
						
						if ($dados_upload['file_name'])
						{
							$link_imagem = base_url() . 'uploads/' . $dados_upload['file_name'];
						}
					}
					
					$this->db->insert('feiras', array(
						'timestamp' => time(),
						'status' => 'ativa',
						'link_imagem' => $link_imagem,
						'nome' => $this->input->post('nome'),
						'data_inicial' => $this->input->post('data_inicial'),
						'data_inicial_timestamp' => hnordt_converter_data_timestamp($this->input->post('data_inicial')),
						//'data_final' => $this->input->post('data_final'),
						//'data_final_timestamp' => hnordt_converter_data_timestamp($this->input->post('data_final'))
					));
					
					redirect('feiras');
				}
			}
		}
		// VALIDAR FORM **
		$conteudo .= '
			<div class="caixa">
				<ul>
					<li>' . anchor('feiras', 'Voltar', 'onclick="history.go(-1); return false;"') . '</li>
				</ul>
			</div>
		';
		// ** EXIBIR FORM
		$conteudo .= heading('Cadastrar Evento', 2);
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		$conteudo .= form_open_multipart(current_url());
		$conteudo .= '<p>' . form_label('Imagem:' . br() . form_upload('imagem')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Nome:' . br() . form_input('nome', $this->input->post('nome'), 'size="60"')) . '</p>';
		$conteudo .= hnordt_gerar_1_data('Data');
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '<p>' . form_submit('concluir', 'Concluir') . ' ou ' . anchor('feiras', 'Cancelar') . '</p>';
		$conteudo .= form_close();
		// EXIBIR FORM **
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function editar($id = NULL)
	{
		if (!$id)
		{
			redirect();	
		}
		
		$feira = $this->db->from('feiras')->where('id', $id)->get()->row();
		
		// ** VALIDAR FORM
		if ($_POST)
		{
			//$erro = hnordt_validar_intervalo_datas('Data', NULL, TRUE);
			$erro = hnordt_validar_uma_data('Data', NULL, TRUE);
			
			if ($erro)
			{
				//
			}
			else if (!$this->input->post('nome'))
			{
				$erro = 'Digite um nome.';
			}
			else
			{
				$this->load->library('upload', array(
					'upload_path' => dirname($this->input->server('SCRIPT_FILENAME')) . '/uploads/',
					'allowed_types' => 'gif|jpg|png'
				));
				
				// $_FILES['imagem']['size'] serve para checar se o usuário tentou um upload
				if ($_FILES['imagem']['size'] && !$this->upload->do_upload('imagem'))
				{
					$erro = 'Selecione uma imagem válida.';
				}
				else
				{
					$dados_upload = $this->upload->data();
					
					if ($dados_upload) {
						$this->load->library('image_lib', array('source_image' => $dados_upload['full_path'], 'width' => 100, 'height' => 100)); 
						
						$this->image_lib->resize();
						
						if ($dados_upload['file_name'])
						{
							$link_imagem = base_url() . 'uploads/' . $dados_upload['file_name'];
						}
					}
					
					$this->db->update('feiras', array(
						'status' => $this->input->post('status'),
						'link_imagem' => $link_imagem ? $link_imagem : $feira->link_imagem,
						'nome' => $this->input->post('nome'),
						'data_inicial' => $this->input->post('data_inicial'),
						'data_inicial_timestamp' => hnordt_converter_data_timestamp($this->input->post('data_inicial')),
						//'data_final' => $this->input->post('data_final'),
						//'data_final_timestamp' => hnordt_converter_data_timestamp($this->input->post('data_final'))
					), array('id' => $id));
					
					redirect('feiras');
				}
			}
		}
		else
		{
			foreach ($feira as $indice => $valor)
			{
				$_POST[$indice] = $valor;
			}
		}
		// VALIDAR FORM **
		$conteudo .= '
			<div class="caixa">
				<ul>
					<li>' . anchor('feiras', 'Voltar', 'onclick="history.go(-1); return false;"') . '</li>
				</ul>
			</div>
		';
		// ** EXIBIR FORM
		$conteudo .= heading('Editar Evento', 2);
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		$conteudo .= form_open_multipart(current_url());
		$conteudo .= '<p>' . form_label('Status:' . br() . form_dropdown('status', $this->_obter_status(), $this->input->post('status'))) . '</p>';
		$conteudo .= '<p>' . form_label('Imagem:' . br() . form_upload('imagem')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Nome:' . br() . form_input('nome', $this->input->post('nome'), 'size="60"')) . '</p>';
		$conteudo .= hnordt_gerar_1_data('Data');
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '<p>' . form_submit('concluir', 'Concluir') . ' ou ' . anchor('feiras', 'Cancelar') . '</p>';
		$conteudo .= form_close();
		// EXIBIR FORM **
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function ver_detalhes($id_feira)
	{
		$feira = $this->db->from('feiras')->where('id', $id_feira)->get()->row();
		
		$conteudo = '
			<div class="caixa">
				<ul>
					<li>' . anchor('feiras', 'Voltar', 'onclick="history.go(-1); return false;"') . '</li>
				</ul>
			</div>
		';
		
		$conteudo .= '<h2>Evento ' . $feira->nome . '</h2>';
		
		$conteudo .= '<p><strong>Data:</strong> ' . $feira->data_inicial ;//. ' a ' . $feira->data_final . '</p>';
		
		$prospects_cadastrados = $this->db->from('prospects')->where('id_feira', $feira->id)->get()->num_rows();
		$prospects_convertidos = $this->db->from('prospects')->where(array('id_feira' => $feira->id, 'conversao_cliente_timestamp IS NOT NULL' => NULL))->get()->num_rows();
		$prospects_convertidos_p = @round(($prospects_convertidos / $prospects_cadastrados) * 100, 2);
		
		$orcamentos_incluido = $this->db->select('SUM(valor_total_com_ipi_e_desconto) AS resultado')->from('pedidos')->join('itens_pedidos', 'itens_pedidos.id_pedido = pedidos.id')->where('id_feira = ' . $feira->id . ' AND (pedidos.tipo = "orcamento" OR conversao_pedido_timestamp IS NOT NULL)')->get()->row()->resultado;
		$orcamentos_aprovado = $this->db->select('SUM(valor_total_com_ipi_e_desconto) AS resultado')->from('pedidos')->join('itens_pedidos', 'itens_pedidos.id_pedido = pedidos.id')->where(array('id_feira' => $feira->id, 'conversao_pedido_timestamp IS NOT NULL' => NULL))->get()->row()->resultado;
		$orcamentos_aprovado_p = @round(($orcamentos_aprovado / $orcamentos_incluido) * 100, 2);
		
		$pedidos = $this->db->select('SUM(valor_total_com_ipi_e_desconto) AS resultado')->from('pedidos')->join('itens_pedidos', 'itens_pedidos.id_pedido = pedidos.id')->where(array('id_feira' => $feira->id, 'pedidos.tipo' => 'pedido'))->get()->row()->resultado;
		
		$conteudo .= '
			<table class="hnordt">
				<thead>
					<tr>
						<th colspan="3">Prospects</th>
						<th colspan="3">Orçamentos</th>
						<th>Pedidos</th>
					</tr>
					<tr>
						<th>Cadastrados</th>
						<th>Convertidos</th>
						<th>Convertidos (%)</th>
						<th>Incluído (R$)</th>
						<th>Aprovado (R$)</th>
						<th>Aprovado (%)</th>
						<th>(R$)</th>
					</tr>
				</thead>
				
				<tbody>
					<tr>
						<td class="right">' . number_format($prospects_cadastrados, 2, ',', '.') . '</td>
						<td class="right">' . number_format($prospects_convertidos, 2, ',', '.') . '</td>
						<td class="right">' . number_format($prospects_convertidos_p, 2, ',', '.') . '</td>
						<td class="right">' . number_format($orcamentos_incluido, 2, ',', '.') . '</td>
						<td class="right">' . number_format($orcamentos_aprovado, 2, ',', '.') . '</td>
						<td class="right">' . number_format($orcamentos_aprovado_p, 2, ',', '.') . '</td>
						<td class="right">' . number_format($pedidos, 2, ',', '.') . '</td>
					</tr>
				</tbody>
			</table>
		';
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	//
	
	function _obter_status()
	{
		return array('ativa' => 'Ativa', 'inativa' => 'Inativa');
	}
	
}