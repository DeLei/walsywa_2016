<?php
class Clientes extends MY_Controller 
{
	
	//Define o tempo em que o cliente se torna inativo não realizando compra neste periodo
	private $periodo_atividade;
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->library("mascara");
		
		$this->periodo_atividade = date('Ymd', strtotime('-6 months'));
	}
	
	function editar_codigo_usuario($novo_codigo = NULL)
	{
		$this->session->set_userdata('codigo_usuario_clientes', $novo_codigo);
		
		redirect('clientes');
	}
	
	function index(){
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->config->load('db_cliente_' . $this->db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');
		
		$filtros = array(
			in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'engenharia')) ? array(
				'nome' => 'representante',
				'descricao' => 'Representante',
				'tipo' => 'texto',
				'campo_mssql' => 'A3_NOME'
			) : NULL,
			array(
				'nome' => 'codigo',
				'descricao' => 'Código',
				'tipo' => 'faixa_codigo',
				'campo_mssql' => 'A1_COD'
			),
			array(
				'nome' => 'loja',
				'descricao' => 'Loja',
				'tipo' => 'texto',
				'campo_mssql' => 'A1_LOJA'
			),
			array(
				'nome' => 'nome',
				'descricao' => 'Nome',
				'tipo' => 'texto',
				'campo_mssql' => 'A1_NOME'
			),
			array(
				'nome' => 'fantasia',
				'descricao' => 'Nome fantasia',
				'tipo' => 'texto',
				'campo_mssql' => 'A1_NREDUZ'
			),
			array(
				'nome' => 'cpf',
				'descricao' => 'CPF/CNPJ(Números)',
				'tipo' => 'texto',
				'campo_mssql' => 'A1_CGC'
			),
			array(
				'nome' => 'telefone',
				'descricao' => 'Telefone',
				'tipo' => 'texto',
				'campo_mssql' => 'A1_TEL'
			),
			array(
				'nome' => 'cep',
				'descricao' => 'CEP',
				'tipo' => 'cep',
				'campo_mssql' => 'A1_CEP',
			),
			array(
				'nome' => 'ultimo_pedido',
				'descricao' => 'Última Compra',
				'tipo' => 'data',
				'campo_mssql' => 'A1_ULTCOM'
			),
			array(
				'nome' => 'status',
				'descricao' => 'Status',
				'tipo' => 'opcoes',
				'campo_mssql' => 'STATUS',
				'opcoes' => $this->obterStatusClientes()
			) ,
			array(
				'nome' => 'incluir_visitas',
				'descricao' => 'Incluir Visitas',
				'tipo' => 'opcoes',
				'campo_mssql' => 'OKFORM',
				'opcoes' => $this->obterStatusIncluirVisitas()
			)			
		);
		
		if($_POST['my_submit'] || $_GET['my_submit']){

			if($this->input->get('ultimo_pedido_inicial') > $this->input->get('ultimo_pedido_final')) {
				$erro = "A Data Inicial da ÚLTIMA COMPRA deve ser menor do que a Data Final";
			}
			
			if(!$this->input->get('ordenar_ms') AND ($this->input->get('ultimo_pedido_inicial') OR $this->input->get('ultimo_pedido_final'))) {
				$_GET['ordenar_ms'] = 'A1_ULTCOM';
				$_GET['ordenar_ms_tipo'] = 'desc';                                    
            } else if(!$this->input->get('ordenar_ms')) {
				$_GET['ordenar_ms'] = 'A1_COD';
				$_GET['ordenar_ms_tipo'] = 'asc';
			}
			
			//Se o usuário logado não for gestor comercial, listará apenas os clientes cadastrados a menos de 1 ano
			if ($this->session->userdata('grupo_usuario') != 'gestores_comerciais'){
				//$this->db_cliente->where($this->_db_cliente['campos']['clientes']['ultima_compra']. ' <=', date('Ymd'));
				//$this->db_cliente->where($this->_db_cliente['campos']['clientes']['ultima_compra']. ' >=', date('Ymd', strtotime("-1 year", time())));
			}
				
			// caso o usuário logado for um representante, vamos mostrar somente os pedidos que ele mesmo fez
			if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'engenharia', 'supervisores')))
			{
				//$this->db_cliente->where('A1_VEND', $this->session->userdata('codigo_usuario'));
				$vendedor_especifico = $this->session->userdata('codigo_usuario');
				
			} else if($this->session->userdata('grupo_usuario') == 'supervisores') {
				/*
				$vendedor_especifico = '';
				$representantes = $this->db->select('codigo_representante')->from('representantes_supervisores')->where('id_supervisor', $this->session->userdata('id_usuario'))->get()->result();
				foreach($representantes as $rep){
					$reps[] = $rep->codigo_representante;
				}
				$reps[] = $this->session->userdata('codigo_usuario');
				$this->db_cliente->where_in('A1_VEND', $reps);
				*/
			} else {
			
				$vendedor_especifico = '';
			}
				
			$this->filtragem_mssql($filtros);
			$codigo_usuario = '';
			if(!in_array(trim($this->session->userdata('grupo_usuario')), array('gestores_comerciais'))){
				$codigo_usuario = trim($this->session->userdata('codigo_usuario'));
			}
			
			
			$clientes = $this->db_cliente			
			->from("GET_CLI_VEN('".$codigo_usuario."')")
			->limit(15, $this->input->get('per_page'))
			->get()
			->result_array();
		
			
		
		}
		
		$this->load->view('layout', array('conteudo' => $this->load->view('clientes/index', array(
			'clientes' => $clientes,
			'erro' => $erro,
			'statusClientes' => $this->obterStatusClientes(),
			'filtragem' => $this->filtragem($filtros),
		), TRUE)));
	}
	
	private function obterStatusClientes() {
	
		$status = array();		
		$status['todos'] = 'Todos';
		$status['1'] = 'Cliente OK';
		$status['2'] = 'Visitas > 30 Dias';
		$status['3'] = 'Visitas > 60 Dias';
		$status['4'] = 'Visitas > 90 Dias';
		$status['8'] = 'Visitas > 90 Dias, lista não visitados';
		$status['9'] = 'Visitas > 120 Dias';
		$status['7'] = 'Não Processado (Hoje está não visitado)';
		$status['5'] = 'Clientes outro vendedor';
		$status['6'] = 'Disponível';				

		return $status;
	}
	
	private function obterStatusIncluirVisitas() {
		
		$status = array();
		
		$status['todos'] = 'Todos';
		
		$status['S'] = 'Sim';
		$status['N'] = 'Não';
		
		return $status;
	}

	function valida_acesso($codigo_cliente){
		$acessos = $this->db->select()
							->from('controle_acessos_clientes')
							->where('id_usuario', $this->session->userdata('id_usuario'))
							->where('dia_acesso', date('Y-m-d'))
							->get()->num_rows();
							
		$cliente = $this->db->select()
						   ->from('controle_acessos_clientes')
						   ->where('codigo_cliente', $codigo_cliente)
						   ->where('id_usuario', $this->session->userdata('id_usuario'))
						   ->where('dia_acesso', date('Y-m-d'))
						   ->get()->num_rows();
		if (!$cliente){
			if($acessos < 15){
				$this->db->insert('controle_acessos_clientes',
							array('id_usuario' => $this->session->userdata('id_usuario'),
								  'dia_acesso' => date('Y-m-d'),
								  'codigo_cliente' => $codigo_cliente));
			}else{
				redirect('clientes/index?erro=esgotado');
			}
		}
	}
	
	function ver_detalhes($codigo = NULL, $loja = NULL)
	{
		
		if (!isset($codigo) || !isset($loja))
		{
			
			redirect();	
		}
		
		if ($this->session->userdata('grupo_usuario') != 'gestores_comerciais'){
			
			$this->valida_acesso($codigo);
		}
		
		/* Regra em que apenas pode ser visto 15x o cadastro de clientes no dia */
		$visualizacao_clientes = $this->db
			->from('visualizacao_clientes')
			->where('id_usuario', $this->session->userdata('id_usuario'))
			->get()->row();
		
		if ($visualizacao_clientes) {
			if ($visualizacao_clientes->data_visualizacao == date('Ymd', time())) {
				if ($visualizacao_clientes->visualizacoes_clientes >= 1500) {
					redirect('clientes');	
				}
				$this->db
					->where('id_usuario', $this->session->userdata('id_usuario'))
					->update('visualizacao_clientes', array(
						'visualizacoes_clientes ' => $visualizacao_clientes->visualizacoes_clientes + 1,
					));
			} else {
				$this->db->delete('visualizacao_clientes', array('id_usuario' => $this->session->userdata('id_usuario')));
				$this->db->insert('visualizacao_clientes', 
					array(
					'id_usuario' => $this->session->userdata('id_usuario'), 
					'visualizacoes_clientes' => 1,
					'data_visualizacao' => date('Ymd', time()),
					));
			}
		} else {
			$this->db->insert('visualizacao_clientes', 
				array(
				'id_usuario' => $this->session->userdata('id_usuario'), 
				'visualizacoes_clientes' => 1,
				'data_visualizacao' => date('Ymd', time()),
				));
		}
		
		// TODO: representantes/prepostos só podem ver SEUS clientes
		$cliente = $this->db_cliente->obter_cliente($codigo, $loja);
		
		$this->filtragem_mysql($filtros);
		$limit_consulta = 20;
		$historicos = $this->db
			->from('historicos_clientes')
			->where(array('codigo_cliente' => $cliente['codigo'], 'loja_cliente' => $cliente['loja']))
			->limit($limit_consulta, $this->input->get('per_page'))
			->order_by('id', 'desc')->get()->result();
		
		
		
		if (!$cliente)
		{
			redirect();	
		}
		
		$filtragem = $this->filtragem($filtros);
		$paginacao = $this->paginacao_rapida();			
		
		
		$contador = $this->paginacao_contador_desc($this->get_total_registro_paginacao(), $limit_consulta);
		$total_registro = $this->get_total_registro_paginacao();
		
			
		
		$conteudo = $this->load->view('clientes/ver_detalhes', array(
			'cliente' 		=> $cliente,
			'filtragem' 	=> $filtragem,
			'historicos'	=> $historicos,
			'paginacao'		=> $paginacao,
			'contador'		=> $contador,
			'total_registro'=> $total_registro	,
			'view_historicos' => $this->retorna_historico($historicos, $total_registro)
			),true);
				
		
		
		
		$this->load->view('layout', array('conteudo' => $conteudo));
		
		//para-aprovacao.. $this->load->view('clientes/tabela_detalhes', array('historicos' => $historicos, 'total_registro' => $total_registro));
		
	}
	
	private function retorna_historico($historicos, $total_registro){
		$conteudo = '';
		$tabela_cabecalho = '
			<table cellspacing="0" class="novo_grid">
			<thead>
				<tr>
					<th>Data</th>
					<th>Contato</th>
					<th>Cargo</th>
					<th>E-mail</th>
					<th>Protocolo</th>
					<th>Opções</th>
					</tr>
				</thead>
			<tbody>';
		
		$entrou = false;
		$tabela_corpo = '';
		foreach ($historicos as $historico)
		{
			$entrou = true;
			$tabela_corpo .=
				'<tr>
					<td>' . date('d/m/Y H:i', $historico->timestamp) . '</td>
					<td>' . $historico->pessoa_contato . '</td>
					<td>' . $historico->cargo . '</td>
					<td>' . mailto($historico->email) . '</td>
					<td>' . $historico->protocolo . '</td>
					<td>
						<a class="colorbox_inline" href="#historico_' . $historico->id . '">Ver Detalhes</a><div style="display: none;">
						<div id="historico_' . $historico->id . '" style="max-width: 740px;">
							<table cellspacing="5">
								<tr>
									<td><strong>Cliente:</strong></td>
									<td>' . $cliente['nome'] . '</td></tr><tr><td><strong>Contato:</strong></td>
									<td>' . $historico->pessoa_contato . '</td></tr><tr><td><strong>Cargo:</strong></td>
									<td>' . $historico->cargo . '</td></tr><tr><td><strong>E-mail:</strong></td>
									<td>' . mailto($historico->email) . '</td>
								</tr>
							</table>
							<p>' . nl2br($historico->descricao) . '</p>
						</div>
					</div>
					</td>
				</tr>';
		}
		
		// HISTÓRICOS **
		if($entrou){
			$tabela_corpo.= '</tbody></table>';
			//$conteudo .= $tabela_cabecalho . $tabela_corpo . '<center><br>'.$total_registro.' registros encontrados.<br>' . $this->paginacao($total_registro).' </center>';
			$conteudo .= $tabela_cabecalho . $tabela_corpo;// . $this->paginacao_rapida();
		}
		//else $conteudo .= '<center><br>Nenhum registro encontrado.</center>';
		
		return($conteudo);
	}
	
	function criar_historico_ajax($codigo_cliente = NULL, $loja_cliente = NULL)
	{
		if (!$codigo_cliente || !$loja_cliente)
		{
			redirect();	
		}
		
		// TODO: representantes/prepostos só podem criar históricos para SEUS clientes
		
		$cliente = $this->db_cliente->obter_cliente($codigo_cliente, $loja_cliente);
		
		if (!$cliente)
		{
			redirect();	
		}
		
		$dados = array(
			'erro' => NULL
		);
		
		if (!$this->input->post('pessoa_contato'))
		{
			$dados['erro'] = 'Digite uma pessoa de contato.';
		}
		else if ($this->input->post('email') && !valid_email($this->input->post('email')))
		{
			$dados['erro'] = 'Digite um e-mail válido.';
		}
		else if (!$this->input->post('descricao'))
		{
			$dados['erro'] = 'Digite uma descrição para o histórico.';
		}
		else
		{
			if ($this->input->post('criar_compromisso'))
			{
				/*
				if (!$this->input->post('nome_compromisso'))
				{
					$dados['erro'] = 'Digite um nome para o compromisso.';
				}
				*/
				if (!$this->input->post('descricao_compromisso'))
				{
					$dados['erro'] = 'Digite uma descrição para o compromisso.';
				}
				else if (!$this->_validar_data($this->input->post('data_inicial_compromisso')))
				{
					$dados['erro'] = 'Digite uma data inicial válida.';
				}
				else if (!$this->_validar_horario($this->input->post('horario_inicial_compromisso')))
				{
					$dados['erro'] = 'Digite um horário inicial válido.';
				}
				else
				{
					$data_inicial = explode('/', $this->input->post('data_inicial_compromisso'));
					$horario_inicial = explode(':', $this->input->post('horario_inicial_compromisso'));
					
					$inicio_timestamp = mktime($horario_inicial[0], $horario_inicial[1], 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
					
					if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')))
					{
						$this->db->insert('compromissos', array(
							'timestamp' => time(),
							'id_usuario' => $this->input->post('id_usuario_compromisso') ? $this->input->post('id_usuario_compromisso') : $this->session->userdata('id_usuario'),
							'status' => 'agendado',
							'tipo' => 'Cliente',
							'codigo_tipo' => $cliente['codigo'] . '|' . $cliente['loja'],
							'detalhes' => TRUE,
							'id_representante' => $cliente['codigo_representante'],
							'id_usuario_cad' => $this->db->from('usuarios')->where(array('codigo' => $cliente['codigo_representante'], 'grupo' => 'representantes'))->get()->row()->id,
							'nome' => $cliente['nome'],
							'descricao' => $this->input->post('descricao_compromisso'),
							'inicio_timestamp' => $inicio_timestamp
						));
					}
					else
					{
						$this->db->insert('compromissos', array(
							'timestamp' => time(),
							'id_usuario' => $this->input->post('id_usuario_compromisso') ? $this->input->post('id_usuario_compromisso') : $this->session->userdata('id_usuario'),
							'status' => 'agendado',
							'tipo' => 'Cliente',
							'codigo_tipo' => $cliente['codigo'] . '|' . $cliente['loja'],
							'id_representante' => $cliente['codigo_representante'],
							'nome' => $cliente['nome'],
							'descricao' => $this->input->post('descricao_compromisso'),
							'inicio_timestamp' => $inicio_timestamp
						));
					}
				}
			}
			
			if (!$dados['erro'])
			{
				$i = $this->db->from('historicos_clientes')->get()->num_rows();
				$i += $this->db->from('historicos_prospects')->get()->num_rows();
				
				$protocolo = date('Y') . '/' . ($this->session->userdata('codigo_usuario') ? $this->session->userdata('codigo_usuario') : 0) . '.' . $i;
				
				$this->db->insert('historicos_clientes', array(
					'timestamp' => time(),
					'codigo_cliente' => $cliente['codigo'],
					'loja_cliente' => $cliente['loja'],
					'pessoa_contato' => $this->input->post('pessoa_contato'),
					'cargo' => $this->input->post('cargo'),
					'email' => $this->input->post('email'),
					'descricao' => $this->input->post('descricao'),
					'protocolo' => $protocolo
				));
			}
		}
		
		echo json_encode($dados);
	}
	
	//
	
	function _obter_representantes()
	{
		$representantes = $this->db->from('usuarios')->where('status', 'ativo')->where('grupo', 'representantes')->order_by('nome', 'asc')->get()->result();
		$_representantes = array(0 => 'Todos');
		
		foreach ($representantes as $representante)
		{
			$_representantes[$representante->codigo] = $representante->codigo . ' - ' . $representante->nome;
		}
		
		return $_representantes;
	}
	
	function _obter_usuarios()
	{
		$usuarios = $this->db->from('usuarios')->where(array('status' => 'ativo', 'grupo' => 'representantes'))->order_by('grupo', 'desc')->order_by('nome', 'asc')->get()->result();
		$_usuarios = array();
		
		foreach ($usuarios as $usuario)
		{
			$_usuarios[$usuario->id] = $usuario->nome . ' (grupo ' . element($usuario->grupo, $this->_obter_grupos()) . ')';
		}
		
		return $_usuarios;
	}
	
	public function atendimentos($cliente)
	{
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->config->load('db_cliente_' . $this->db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');
		
		$filtros = array(
			
			array(
				'nome' => 'status',
				'descricao' => 'Status',
				'tipo' => 'opcoes',
				'campo_mssql' => '',//$this->_db_cliente['campos']['atendimentos']['operacao'],
				'opcoes' => array(
					'todos' => 'TODAS',
					'CANCELADO' => 'CANCELADO',
					'NF. EMITIDA' => 'NF. EMITIDA',
					'ATENDIMENTO' => 'ATENDIMENTO',
					'ORCAMENTO' => 'ORÇAMENTO',
					'FATURAMENTO' => 'FATURAMENTO'
					),
				//'ordenar_ms' => 3
			),
			array(
				'nome' => 'numero',
				'descricao' => 'Número',
				'tipo' => 'texto',
				'campo_mssql' => $this->_db_cliente['campos']['atendimentos']['numero'],
				'ordenar_ms' => 1
			),
			array(
				'nome' => 'data_atendimento',
				'descricao' => 'Emissão',
				'tipo' => 'data',
				'campo_mssql' => $this->_db_cliente['campos']['atendimentos']['data_atendimento'],
				'ordenar_ms' => 2
			),
			
			array(
				'nome' => 'operacao',
				'descricao' => 'Operação',
				'tipo' => 'opcoes',
				'campo_mssql' => $this->_db_cliente['campos']['atendimentos']['operacao'],
				'opcoes' => array(
					'todos' => 'TODAS',
					1 => 'FATURAMENTO', 
					2 => 'ORÇAMENTO', 
					3 => 'ATENDIMENTO'
					),
				'ordenar_ms' => 3
			),
			array(
				'nome' => 'representante',
				'descricao' => 'Representante',
				'tipo' => 'texto',
				'campo_mssql' => $this->_db_cliente['campos']['representantes']['nome'],
				'ordenar_ms' => 4
			)
		);
		
			// para que a paginação no mssql funcione, devemos ordenar de alguma forma, caso o usuário não escolher uma ordenação, vamos fazer manualmente
			if (!$this->input->get('ordenar_ms'))
			{
				// vamos setar o $_GET ao invés de usar o order_by()
				// usando o order_by a flexinha no layout indicando o que está sendo ordenado não apareceria para o usuário
				$_GET['ordenar_ms'] = $this->_db_cliente['campos']['atendimentos']['data_atendimento'];
				$_GET['ordenar_ms_tipo'] = 'asc';
			}
			
			// ** FILTRAGEM PARA EXIBIÇÃO DOS RESULTADOS
			
			$this->db_cliente->select('SUA010.*, CASE_STATUS = CASE
												   WHEN UA_STATUS = \'CAN\' AND UA_CANC = \'S\' AND UA_CODCANC <> \'\' THEN (
												      \'CANCELADO\'
												   )
												   WHEN UA_OPER = 1 AND UA_STATUS = \'NF.\' AND UA_DOC <> \'\' THEN (
												      \'NF. EMITIDA\'
												   )
												   WHEN UA_OPER = 3 THEN (
												      \'ATENDIMENTO\'
												   )
												   WHEN UA_OPER = 2 THEN (
												      \'ORCAMENTO\'
												   )
												   WHEN UA_OPER = 1 THEN (
												      \'FATURAMENTO\'
												   )
												END');
			if ($this->input->get('status'))
			{
			
				$this->db_cliente->where('1 = CASE \''.$this->input->get('status').'\'
											WHEN \'CANCELADO\'
											THEN CASE WHEN UA_STATUS = \'CAN\' AND UA_CANC = \'S\' AND UA_CODCANC <> \'\' THEN 1 ELSE 0 END
											
											WHEN \'NF. EMITIDA\'
											THEN CASE WHEN UA_OPER = 1 AND UA_STATUS = \'NF.\' AND UA_DOC <> \'\' THEN 1 ELSE 0 END 
											
											WHEN \'ATENDIMENTO\' 
											THEN CASE WHEN UA_OPER = 3 THEN 1 ELSE 0 END  
											
											WHEN \'ORCAMENTO\'
											THEN CASE WHEN UA_OPER = 2 THEN 1 ELSE 0 END  
											
											WHEN \'FATURAMENTO\'
											THEN CASE WHEN UA_OPER = 1 THEN 1 ELSE 0 END 
											
											ELSE
											CASE 1 WHEN 1 THEN 1 ELSE 0 END 
										  END');
			}
			$this->filtragem_mssql($filtros);
			
			$atendimentos = $this->db_cliente->from($this->_db_cliente['tabelas']['atendimentos'])
			->join($this->_db_cliente['tabelas']['representantes'], $this->_db_cliente['campos']['representantes']['codigo'].' = '.$this->_db_cliente['campos']['atendimentos']['representante'])			
			->where(array(	$this->_db_cliente['tabelas']['atendimentos'].'.'.$this->_db_cliente['campos']['atendimentos']['delete'].' <> ' => '*',
							$this->_db_cliente['campos']['atendimentos']['cliente'] => $cliente,
					))
			->limit(20, $this->input->get('per_page'))
			->get()->result_array();
			//echo $this->db_cliente->last_query();
			// FILTRAGEM PARA EXIBIÇÃO DOS RESULTADOS **
			if ($this->input->get('status'))
			{
			
				$this->db_cliente->where('1 = CASE \''.$this->input->get('status').'\'
											WHEN \'CANCELADO\'
											THEN CASE WHEN UA_STATUS = \'CAN\' AND UA_CANC = \'S\' AND UA_CODCANC <> \'\' THEN 1 ELSE 0 END
											
											WHEN \'NF. EMITIDA\'
											THEN CASE WHEN UA_OPER = 1 AND UA_STATUS = \'NF.\' AND UA_DOC <> \'\' THEN 1 ELSE 0 END 
											
											WHEN \'ATENDIMENTO\' 
											THEN CASE WHEN UA_OPER = 3 THEN 1 ELSE 0 END  
											
											WHEN \'ORCAMENTO\'
											THEN CASE WHEN UA_OPER = 2 THEN 1 ELSE 0 END  
											
											WHEN \'FATURAMENTO\'
											THEN CASE WHEN UA_OPER = 1 THEN 1 ELSE 0 END 
											
											ELSE
											CASE 1 WHEN 1 THEN 1 ELSE 0 END 
										  END');
			}
			// ** FILTRAGEM PARA PAGINAÇÃO
			$this->filtragem_mssql($filtros, FALSE);
			
			$total = $this->db_cliente->from($this->_db_cliente['tabelas']['atendimentos'])
			->join($this->_db_cliente['tabelas']['representantes'], $this->_db_cliente['campos']['representantes']['codigo'].' = '.$this->_db_cliente['campos']['atendimentos']['representante'])			
			->where(array(	$this->_db_cliente['tabelas']['atendimentos'].'.'.$this->_db_cliente['campos']['atendimentos']['delete'].' <> ' => '*',
							$this->_db_cliente['campos']['atendimentos']['cliente'] => $cliente,
					))
			->get()->num_rows();
			//echo $this->db_cliente->last_query();
		
		/*
		echo '<pre>';
		print_r($atendimentos);
		echo '<br />';
		print_r($total);
		echo '</pre>';
		//die();
		*/
		$this->load->view('layout', array('conteudo' => $this->load->view('clientes/atendimentos', array(
			'atendimentos' => $atendimentos, 
			'total' => $total, 
			'filtragem' => $this->filtragem($filtros), 
			'paginacao' => $this->paginacao($total)
		), TRUE)));
	}
	
	public function detalhes_atendimento($cli, $num)
	{
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->config->load('db_cliente_' . $this->db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');
		
		$atendimentos = $this->db_cliente->from($this->_db_cliente['tabelas']['atendimentos'])
		->join($this->_db_cliente['tabelas']['representantes'], $this->_db_cliente['campos']['representantes']['codigo'].' = '.$this->_db_cliente['campos']['atendimentos']['representante'])			
		->join('SUH010','UH_MIDIA = UA_MIDIA', 'left')
		->join('DA0010','DA0_CODTAB = UA_TABELA', 'left')
		->join('SE4010','E4_CODIGO = UA_CONDPG', 'left')
		->join('SU7010','U7_COD = UA_OPERADO', 'left')
		->where(array(	$this->_db_cliente['tabelas']['atendimentos'].'.'.$this->_db_cliente['campos']['atendimentos']['delete'].' <> ' => '*',
						//'SUH010.'.$this->_db_cliente['campos']['atendimentos']['delete'].' <> ' => '*',
						$this->_db_cliente['tabelas']['atendimentos'].'.'.$this->_db_cliente['campos']['atendimentos']['cliente'] => $cli,
						$this->_db_cliente['campos']['atendimentos']['numero'] => $num
				))
		->get()->row_array();
		
	
		/*
		echo $this->db_cliente->last_query();
		echo '<pre>';
		print_r($atendimentos);
		echo '</pre>';
		die();
		*/
	
		
		$this->load->view('layout', array('conteudo' => $this->load->view('clientes/detalhes_atendimento', array(
			'atendimento' => $atendimentos), TRUE)));
	}
}