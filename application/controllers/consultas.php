<?php

class Consultas extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('db_formulario_visita');
	}
	//Chamado 1374 Adicionado filtro por representante quando o usuário logado for um supervisor:	
	function notas_fiscais(){		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->config->load('db_cliente_' . $this->db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');
				
		$filtros = array(
			array(
				'nome' => 'codigo',
				'descricao' => 'Código N.F.',
				'tipo' => 'texto',
				'campo_mssql' => 'F2_DOC',
				'ordenar_ms' => 0
			),
			 
			//Chamado 1374 - Permitir ordenação por cliente:
			in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores')) ? array(
                'nome' => 'representante',
                'descricao' => 'Representante',
                'tipo' => 'opcoes',
        		'opcoes' => $this->db_formulario_visita->get_representantesNotas(),
                'campo_mysql' => 'A3_COD',
                'ordenar' => 1
			) : NULL,
			array(
				'nome' => 'cliente',
				'descricao' => 'Cliente',
				'tipo' => 'texto',
				'campo_mssql' => 'A1_NOME',
				'ordenar_ms' => 2
			),
			array(
				'nome' => 'emissao',
				'descricao' => 'Emissão',
				'tipo' => 'data',
				'campo_mssql' => 'F2_EMISSAO',
				'ordenar_ms' => 3
			),
			array(
				'nome' => 'transportadora',
				'descricao' => 'Transportadora',
				'tipo' => 'texto',
				'campo_mssql' => 'A4_NOME',
				'ordenar_ms' => 8
			)
		);
				
		if($_POST || $this->input->get('my_submit')){
			if (($this->session->userdata('grupo_usuario') != 'gestores_comerciais') and ((!$_GET['codigo']) and (!$_GET['emissao_inicial']) and (!$_GET['transportadora']) and (!$_GET['representante']) and (!$_GET['cliente'])  )){
				$erro = "Realize um filtro para efetuar a pesquisa.";
			}else{
				// para que a paginação no mssql funcione, devemos ordenar de alguma forma, caso o usuário não escolher uma ordenação, vamos fazer manualmente
				//if (!$this->input->get('ordenar_ms'))
				//{
				//	// vamos setar o $_GET ao invés de usar o order_by()
				//	// usando o order_by a flexinha no layout indicando o que está sendo ordenado não apareceria para o usuário
				//	$_GET['ordenar_ms'] = 'F2_EMISSAO';
				//	$_GET['ordenar_ms_tipo'] = 'desc';
				//}
				//
				//// caso o usuário logado for um representante, vamos mostrar somente as nfs dele
				//if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores')))
				//{
				//	$this->db_cliente->where('F2_VEND1', $this->session->userdata('codigo_usuario'));
				//}else if($this->session->userdata('grupo_usuario') == 'supervisores'){
				//	$representantes = $this->db->select('codigo_representante')->from('representantes_supervisores')->where('id_supervisor', $this->session->userdata('id_usuario'))->get()->result();
				//	foreach($representantes as $rep){
				//		$reps[] = $rep->codigo_representante;
				//	}
				//	$reps[] = $this->session->userdata('codigo_usuario');
				//	$this->db_cliente->where_in('F2_VEND1', $reps);
				//}
				//
				//$this->filtragem_mssql($filtros);
				
				//$nfs = $this->db_cliente->select('F2_DOC, F2_EMISSAO, F2_VALMERC, F2_VALICM, F2_VALIPI, F2_TPFRETE, F2_TRANSP, F2_FILIAL, F2_SERIE, F2_CLIENTE, F2_LOJA, A3_NOME, A1_NOME, A4_NOME, A4_TEL')
				//				->from($this->_db_cliente['tabelas']['notas_fiscais'] . ' (NOLOCK) SF2')
				//				->join($this->_db_cliente['tabelas']['representantes'] . ' (NOLOCK) SA3', 'SA3.A3_FILIAL = \'\' AND SA3.A3_COD = SF2.F2_VEND1 AND SA3.D_E_L_E_T_ = \'\'', 'INNER')
				//				->join($this->_db_cliente['tabelas']['clientes'] . ' (NOLOCK) SA1', 'SA1.A1_FILIAL = \'\' AND SA1.A1_COD = SF2.F2_CLIENTE AND SA1.A1_LOJA = SF2.F2_LOJA AND SA1.D_E_L_E_T_ = \'\'', 'INNER')
				//				->join($this->_db_cliente['tabelas']['transportadoras'] . ' (NOLOCK) SA4', 'SA4.A4_FILIAL = \'\' AND SA4.A4_COD = SF2.F2_TRANSP AND SA4.D_E_L_E_T_ = \'\'', 'left outer')
				//				->where(array(
				//					'SF2.D_E_L_E_T_ !=' => '*',
				//					'SF2.F2_EMISSAO >=' => date('Ymd',mktime(0, 0, 0, date('m'), date('d'), date('Y')-1))
				//				))
				//				->limit(20, $this->input->get('per_page'))
				//				->group_by('F2_DOC, F2_EMISSAO, F2_VALMERC, F2_VALICM, F2_VALIPI, F2_TPFRETE, F2_TRANSP, F2_FILIAL, F2_SERIE, F2_CLIENTE, F2_LOJA, A3_NOME, A1_NOME, A4_NOME, A4_TEL')
				//				->get()->result_array();
								
				//echo $this->db_cliente->last_query();
				//die();
				// caso o usuário logado for um representante, vamos mostrar somente as nfs dele
				//if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores')))
				//{
				//	$this->db_cliente->where('F2_VEND1', $this->session->userdata('codigo_usuario'));
				//}else if($this->session->userdata('grupo_usuario') == 'supervisores'){
				//	$representantes = $this->db->select('codigo_representante')->from('representantes_supervisores')->where('id_supervisor', $this->session->userdata('id_usuario'))->get()->result();
				//	foreach($representantes as $rep){
				//		$reps[] = $rep->codigo_representante;
				//	}
				//	$reps[] = $this->session->userdata('codigo_usuario');
				//	$this->db_cliente->where_in('F2_VEND1', $reps);
				//}
				
				// FALSE = não usar order_by
				//$this->filtragem_mssql($filtros, FALSE);
				
				//$total = $this->db_cliente->select('COUNT(DISTINCT F2_DOC) AS quantidade, SUM(F2_VALMERC) AS valor')->from($this->_db_cliente['tabelas']['notas_fiscais'])
				//				->join($this->_db_cliente['tabelas']['representantes'], 'A3_COD = F2_VEND1')
				//				->join($this->_db_cliente['tabelas']['clientes'], 'A1_COD = F2_CLIENTE AND A1_LOJA = F2_LOJA')
				//				->join($this->_db_cliente['tabelas']['transportadoras'], 'A4_COD = F2_TRANSP', 'left outer')
				//				->where(array(
				//					$this->_db_cliente['tabelas']['notas_fiscais'] . '.D_E_L_E_T_ !=' => '*',
				//					$this->_db_cliente['tabelas']['representantes'] . '.D_E_L_E_T_ !=' => '*',
				//					$this->_db_cliente['tabelas']['notas_fiscais'] . '.F2_EMISSAO >=' => date('Ymd',mktime(0, 0, 0, date('m'), date('d'), date('Y')-1))
				//				))
				//				->get()->row_array();
				
				if($_GET['emissao_inicial'] != "")
				{
					$dataIniTemp = explode('/', $_GET['emissao_inicial']);
					$dataInicio = $dataIniTemp[2].$dataIniTemp[1].$dataIniTemp[0];
				}else{
					$dataInicio = '';
				}
				
				if($_GET['emissao_final'] != "")
				{
					$dataFimTemp = explode('/', $_GET['emissao_final']);
					$dataFinal = $dataFimTemp[2].$dataFimTemp[1].$dataFimTemp[0];
				}else{
					$dataFinal = '';
				}
				
				$representante =  $_GET['representante'] ?  $_GET['representante'] : $this->session->userdata('codigo_usuario');
				
					$total = $this->db_cliente			
						->from("NF_REP_TOTAL('". $representante ."','". $dataInicio ."','". $dataFinal ."','".$_GET['codigo']."','".$_GET['cliente']."','".$_GET['transportadora']."')")
						->get()
						->row();
				
				
					$notas = $this->db_cliente			
						->from("NF_REP_ITENS('". $representante ."','". $dataInicio ."','". $dataFinal ."','".$_GET['codigo']."','".$_GET['cliente']."','".$_GET['transportadora']."')")
						->get()
						->result_array();
			}
		}
		
		$this->load->view('layout', array(
			'conteudo' => $this->load->view('consultas/notas_fiscais', array(
				'nfs' => $notas, 
				'erro' => $erro, 
				'total' => $total, 
				'filtragem' => $this->filtragem($filtros),
				'paginacao' => $this->paginacao($total->TOT_SIMP)), TRUE)));
	}
	
	function titulos()
	{
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->config->load('db_cliente_' . $this->db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');
		
		if($_GET['cod_filial'])
		{
			// Customizável (Agrosandri) 
			// $this->db_cliente->where('E1_FILIAL', $_GET['cod_filial']);
		}
		
		if($_GET['serie'])
		{
			 $this->db_cliente->where('E1_PREFIXO', $_GET['serie']);
		}
		
		if($_GET['cliente'])
		{
			 $this->db_cliente->where('E1_CLIENTE', $_GET['cliente']);
		}
		
		if($_GET['loja'])
		{
			 $this->db_cliente->where('E1_LOJA', $_GET['loja']);
		}
		
		$this->db_cliente->where('E1_SALDO >', 0);
		
		$titulos = $this->db_cliente->select('E1_NUM, E1_PARCELA, E1_VALOR, E1_VENCTO, E1_BAIXA')->from($this->_db_cliente['tabelas']['titulos'])->where('E1_NUM', $_GET['codigo_nf'])->get()->result_array();
		
		foreach ($titulos as &$titulo)
		{
			$titulo['status'] = $this->_obter_status_do_titulo(strtotime($titulo['E1_VENCTO']), strtotime($titulo['E1_BAIXA']));
		}
		
		$this->load->view('consultas/titulos', array('titulos' => $titulos));
	}
	
	function titulos_vencidos()
	{
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->config->load('db_cliente_' . $this->db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');
		
		$filtros = array(
			//in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais')) ?
			array(
				'nome' => 'cliente',
				'descricao' => 'Cliente',
				'tipo' => 'texto',
				'campo_mssql' => 'A1_NOME',
				'ordenar_ms' => 0
			),
			in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores')) ? array(
				'nome' => 'representante',
				'descricao' => 'Representante',
				'tipo' => 'texto',
				'campo_mssql' => 'A3_NOME',
				'ordenar_ms' => 1
			) : NULL,
			array(
				'nome' => 'codigo',
				'descricao' => 'Código N.F.',
				'tipo' => 'texto',
				'campo_mssql' => 'E1_NUM',
				'ordenar_ms' => 2
			),
			array(
				'nome' => 'parcela',
				'descricao' => 'Parcela',
				'tipo' => 'texto',
				'campo_mssql' => 'E1_PARCELA',
				'ordenar_ms' => 3
			),
			array(
				'nome' => 'vencimento',
				'descricao' => 'Vencimento',
				'tipo' => 'data',
				'campo_mssql' => 'E1_VENCTO',
				'ordenar_ms' => 6
			)
		);
		
		if($_POST || $this->input->get('my_submit')){
			if (($this->session->userdata('grupo_usuario') != 'gestores_comerciais') and ((!$_GET['codigo']) and (!$_GET['parcela']) and (!$_GET['vencimento_inicial']) and (!$_GET['representante']) and (!$_GET['cliente'])  )){
				$erro = "Realize um filtro para efetuar a pesquisa.";
				
			//if($_POST || $this->input->get('my_submit')){
			//	if (($this->session->userdata('grupo_usuario') != 'gestores_comerciais') and ((!$_GET['codigo']) and (!$_GET['parcela']) and (!$_GET['vencimento_inicial']))){
			//		$erro = "Realize um filtro para efetuar a pesquisa.";	
			}else{
				// para que a paginação no mssql funcione, devemos ordenar de alguma forma, caso o usuário não escolher uma ordenação, vamos fazer manualmente
				if (!$this->input->get('ordenar_ms'))
				{
					// vamos setar o $_GET ao invés de usar o order_by()
					// usando o order_by a flexinha no layout indicando o que está sendo ordenado não apareceria para o usuário
					$_GET['ordenar_ms'] = 'E1_VENCTO';
					$_GET['ordenar_ms_tipo'] = 'asc';
				}
				
				$vencimento_filtro = str_replace("/", "", $_GET['vencimento_inicial']);
				$vencimento_filtro = substr($vencimento_filtro, -4) . substr($vencimento_filtro, 2, 2) . substr($vencimento_filtro, 0, 2);
				
				//Se o usuário logado não for gestor comercial, listará apenas os titulos vencidos a menos de 1 ano				
				if ($this->session->userdata('grupo_usuario') != 'gestores_comerciais'){
					if ($vencimento_filtro < date('Ymd', strtotime("-1 year", time()))){
						$vencimento = substr($vencimento_filtro, -2) . '/' . substr($vencimento_filtro, 4, 2) . '/' . substr($vencimento_filtro, 0, 4);
						$_GET['vencimento_inicial'] = $vencimento;
					}else{
						$this->db_cliente->where($this->_db_cliente['campos']['titulos']['data_vencimento']. ' <=', date('Ymd'));
						$this->db_cliente->where($this->_db_cliente['campos']['titulos']['data_vencimento']. ' >=', date('Ymd', strtotime("-1 year", time())));
					}
				}
				
				$this->filtragem_mssql($filtros);
				
				// caso o usuário logado for um representante, vamos mostrar somente as nfs dele, se for supervisor lista as suas e dos representantes
				if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores'))){
					$this->db_cliente->where('E1_VEND1', $this->session->userdata('codigo_usuario'));
				}else if($this->session->userdata('grupo_usuario') == 'supervisores'){
					$representantes = $this->db->select('codigo_representante')->from('representantes_supervisores')->where('id_supervisor', $this->session->userdata('id_usuario'))->get()->result();
					foreach($representantes as $rep){
						$reps[] = $rep->codigo_representante;
					}
					$reps[] = $this->session->userdata('codigo_usuario');
					$this->db_cliente->where_in('E1_VEND1', $reps);
				}
				
				$titulos = $this->db_cliente->select('E1_NUM, E1_PARCELA, E1_VALOR, E1_VENCTO, A3_NOME, A1_NOME')->from($this->_db_cliente['tabelas']['titulos'])
								->join($this->_db_cliente['tabelas']['representantes'], 'A3_COD = E1_VEND1')
								->join($this->_db_cliente['tabelas']['clientes'], 'A1_COD = E1_CLIENTE AND A1_LOJA = E1_LOJA')
								->where(array(
									'E1_VENCTO <' => date('Ymd'),
									'E1_BAIXA' => '',
									$this->_db_cliente['tabelas']['titulos'] . '.D_E_L_E_T_ !=' => '*',
									$this->_db_cliente['tabelas']['representantes'] . '.D_E_L_E_T_ !=' => '*'
								))
								->limit(20, $this->input->get('per_page'))
								->group_by('E1_NUM, E1_PARCELA, E1_VALOR, E1_VENCTO, A3_NOME, A1_NOME')
								->get()
								->result_array();
								
				foreach ($titulos as &$titulo)
				{
					$titulo['status'] = $this->_obter_status_do_titulo(strtotime($titulo['E1_VENCTO']), strtotime($titulo['E1_BAIXA']));
				}
				
				/////////////////////////////////////
				
				// caso o usuário logado for um representante, vamos mostrar somente as nfs dele
				if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores')))
				{
					$this->db_cliente->where('E1_VEND1', $this->session->userdata('codigo_usuario'));
				}else if($this->session->userdata('grupo_usuario') == 'supervisores'){
					$representantes = $this->db->select('codigo_representante')->from('representantes_supervisores')->where('id_supervisor', $this->session->userdata('id_usuario'))->get()->result();
					foreach($representantes as $rep){
						$reps[] = $rep->codigo_representante;
					}
					$reps[] = $this->session->userdata('codigo_usuario');
					$this->db_cliente->where_in('E1_VEND1', $reps);
				}
				
				// FALSE = não usar order_by
				$this->filtragem_mssql($filtros, FALSE);
				
				$total = $this->db_cliente->select('COUNT(DISTINCT E1_NUM) AS quantidade, SUM(E1_VALOR) AS valor')->from($this->_db_cliente['tabelas']['titulos'])
								->join($this->_db_cliente['tabelas']['representantes'], 'A3_COD = E1_VEND1')
								->join($this->_db_cliente['tabelas']['clientes'], 'A1_COD = E1_CLIENTE AND A1_LOJA = E1_LOJA')
								->where(array(
									'E1_VENCTO <' => date('Ymd'),
									'E1_BAIXA' => '',
									$this->_db_cliente['tabelas']['titulos'] . '.D_E_L_E_T_ !=' => '*',
									$this->_db_cliente['tabelas']['representantes'] . '.D_E_L_E_T_ !=' => '*'
								))
								->get()
								->row_array();
								
			}
		}
		
		$this->load->view('layout', array('conteudo' => $this->load->view('consultas/titulos_vencidos', array('titulos' => $titulos, 'erro' => $erro, 'total' => $total, 'filtragem' => $this->filtragem($filtros), 'paginacao' => $this->paginacao($total['quantidade'])), TRUE)));
	}
	
	function valida_acesso($codigo_nota){
		$acessos = $this->db->select()
							->from('controle_acessos_notas')
							->where('id_usuario', $this->session->userdata('id_usuario'))
							->where('dia_acesso', date('Y-m-d'))
							->get()->num_rows();
							
		$titulo = $this->db->select()
						   ->from('controle_acessos_notas')
						   ->where('codigo_nota', $codigo_nota)
						   ->where('id_usuario', $this->session->userdata('id_usuario'))
						   ->where('dia_acesso', date('Y-m-d'))
						   ->get()->num_rows();
		if (!$titulo){
			if($acessos < 10){
				$this->db->insert('controle_acessos_notas',
							array('id_usuario' => $this->session->userdata('id_usuario'),
								  'dia_acesso' => date('Y-m-d'),
								  'codigo_nota' => $codigo_nota));
			}else{
				redirect('consultas/notas_fiscais?erro=esgotado');
			}
		}
	}
	
	function obter_itens($id = NULL, $cliente = NULL, $loja = NULL, $pdf = NULL)
	{
		$this->teste_db_cliente = $this->load->database('db_cliente', TRUE);
		
		$this->config->load('db_cliente_' . $this->teste_db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');
		
		if(!$id)
		{
			redirect('consultas/notas_fiscais');
		}
		
		if ($this->session->userdata('grupo_usuario') != 'gestores_comerciais'){
			$this->valida_acesso($id);
		}
		
		$itens = $this->teste_db_cliente
					->select(
						$this->_db_cliente['campos']['itens_notas_fiscais']['codigo_nota_fiscal'] . ' AS codigo_nota_fiscal,' .
						$this->_db_cliente['campos']['produtos']['descricao'] . ' AS descricao, ' . 
						$this->_db_cliente['campos']['itens_notas_fiscais']['preco_unitario'] . ' AS preco_unitario,' . 
						$this->_db_cliente['campos']['itens_notas_fiscais']['unidade_medida'] . ' AS unidade_medida,' . 
						//$this->_db_cliente['campos']['itens_notas_fiscais']['quantidade_entrega'] . ' AS quantidade_entrega,' . 
						$this->_db_cliente['campos']['itens_notas_fiscais']['total_faturado'] . ' AS total_faturado,' . 
						$this->_db_cliente['campos']['itens_notas_fiscais']['ipi'] . ' AS ipi,' .
						
						//----------------------------------------
						$this->_db_cliente['campos']['itens_notas_fiscais']['base_calc_icms'] . ' AS base_calc_icms,' .
						$this->_db_cliente['campos']['itens_notas_fiscais']['aliq_icms'] . ' AS aliq_icms,' .
						$this->_db_cliente['campos']['itens_notas_fiscais']['valor_icms'] . ' AS valor_icms,' .
						//----------------------------------------
						
						$this->_db_cliente['campos']['itens_notas_fiscais']['st'] . ' AS st,' . 
						$this->_db_cliente['campos']['itens_pedidos']['quantidade_vendida_produto'] . ' AS quantidade_vendida_produto'
					)
					->from($this->_db_cliente['tabelas']['itens_notas_fiscais'])
					->join($this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos']['codigo'] . ' = ' . $this->_db_cliente['campos']['itens_notas_fiscais']['codigo_produto'])
					->join($this->_db_cliente['tabelas']['itens_pedidos'], $this->_db_cliente['campos']['itens_pedidos']['codigo_pedido'] . ' = ' . $this->_db_cliente['campos']['itens_notas_fiscais']['codigo_pedido'])
					->where(
						$this->_db_cliente['campos']['itens_notas_fiscais']['codigo_nota_fiscal'] . ' = "' . $id . '" AND ' .
						$this->_db_cliente['campos']['itens_notas_fiscais']['codigo_cliente'] . ' = "' . $cliente . '" AND ' .
						$this->_db_cliente['campos']['itens_notas_fiscais']['loja_cliente'] . ' = "' . $loja . '" AND ' .
						
						//$this->_db_cliente['campos']['itens_pedidos']['filial'] . ' = ' . $this->_db_cliente['campos']['itens_notas_fiscais']['filial'] . ' AND ' .
						//$this->_db_cliente['campos']['itens_pedidos']['item'] . ' = ' . $this->_db_cliente['campos']['itens_notas_fiscais']['item'] . ' AND ' .
						
						$this->_db_cliente['campos']['itens_pedidos']['codigo_produto'] . ' = ' . $this->_db_cliente['campos']['itens_notas_fiscais']['codigo_produto']
					)
					->get()
					->result_array();


		$nota_fiscal = $this->teste_db_cliente
			->select(
				$this->_db_cliente['campos']['notas_fiscais']['codigo_cliente'] . ' AS codigo_cliente,' .
				$this->_db_cliente['campos']['notas_fiscais']['loja_cliente'] . ' AS loja_cliente,' .
				$this->_db_cliente['campos']['notas_fiscais']['codigo_transportadora'] . ' AS codigo_transportadora,' .
				$this->_db_cliente['campos']['notas_fiscais']['data_emissao'] . ' AS data_emissao,' .
				$this->_db_cliente['campos']['notas_fiscais']['hora_emissao'] . ' AS hora_emissao,' .
				
				$this->_db_cliente['campos']['notas_fiscais']['codigo'] . ' AS codigo_nota_fiscal,' .
				$this->_db_cliente['campos']['notas_fiscais']['serie'] . ' AS serie_nota_fiscal,' .
				$this->_db_cliente['campos']['notas_fiscais']['base_calculo_icms'] . ' AS base_calculo_icms,' .
				$this->_db_cliente['campos']['notas_fiscais']['valor_icms'] . ' AS valor_icms,' .
				$this->_db_cliente['campos']['notas_fiscais']['base_calculo_icms_st'] . ' AS base_calculo_icms_st,' .
				$this->_db_cliente['campos']['notas_fiscais']['valor_icms_st'] . ' AS valor_icms_st,' .
				$this->_db_cliente['campos']['notas_fiscais']['valor_total_produto'] . ' AS valor_total_produto,' .
				$this->_db_cliente['campos']['notas_fiscais']['valor_ipi'] . ' AS valor_ipi,' .
				
				$this->_db_cliente['campos']['notas_fiscais']['valor_frete'] . ' AS valor_frete,' .
				$this->_db_cliente['campos']['notas_fiscais']['valor_seguro'] . ' AS valor_seguro,' .
				$this->_db_cliente['campos']['notas_fiscais']['valor_desconto'] . ' AS valor_desconto,' .
				$this->_db_cliente['campos']['notas_fiscais']['despesas_necessarias'] . ' AS despesas_necessarias,' .
				$this->_db_cliente['campos']['notas_fiscais']['valor_total'] . ' AS valor_total,' .
				$this->_db_cliente['campos']['notas_fiscais']['condicao_pagamento'] . ' AS condicao_pagamento,' .
				$this->_db_cliente['campos']['notas_fiscais']['codigo_representante'] . ' AS codigo_representante'
			)
			->from($this->_db_cliente['tabelas']['notas_fiscais'])
			->where(array(
				$this->_db_cliente['campos']['notas_fiscais']['codigo'] => $id,
				$this->_db_cliente['campos']['notas_fiscais']['codigo_cliente'] => $cliente,
				$this->_db_cliente['campos']['notas_fiscais']['loja_cliente'] => $loja,
				$this->_db_cliente['campos']['notas_fiscais']['delecao'] . ' <> ' => '*'
			))
			->get()
			->row_array();


		$cliente = $this->db_cliente->obter_cliente($nota_fiscal['codigo_cliente'], $nota_fiscal['loja_cliente']);
		$transportadora = $this->db_cliente->obter_transportadora($nota_fiscal['codigo_transportadora']);
		$representante = $this->db_cliente->obter_representante($nota_fiscal['codigo_representante']);
		
		if($pdf)
		{
			$dados = array(
				'itens' => $itens,
				'cliente' => $cliente,
				'transportadora' => $transportadora,
				'representante' => $representante,
				'nota_fiscal' => $nota_fiscal
			);
			$this->gerar_pdf_nota_fiscal($dados);
		}
		else
		{
			$this->load->view('layout', array('conteudo' => $this->load->view('consultas/itens', 
			array(
				'itens' => $itens,
				'cliente' => $cliente,
				'transportadora' => $transportadora,
				'representante' => $representante,
				'nota_fiscal' => $nota_fiscal
			), TRUE)));
		}
	}
	
	function _obter_status_do_titulo($vencimento_timestamp, $pagamento_timestamp = NULL)
	{
		if ($pagamento_timestamp)
		{
			$dias = round(($pagamento_timestamp - $vencimento_timestamp) / (3600 * 24));
			
			if ($pagamento_timestamp > $vencimento_timestamp) $status = '<span style="color: #060;">Pago com atraso de ' . $dias . ' dia(s)</span>';
			else $status = '<span style="color: #060;">Pago em dia</span>';
		}
		else
		{
			$dias = abs(round((time() - $vencimento_timestamp) / (3600 * 24)));
			
			if (time() > $vencimento_timestamp ) $status = '<span style="color: #900;">' . $dias . ' dia(s) vencido</span>';
			else $status = '<span style="color: #060;">' . $dias . ' dia(s) para vencer</span>';
		}
		
		return $status;
	}
	
	
	function gerar_pdf_nota_fiscal($dados)
	{
		$empresa = $this->db->from('empresas')->where('id', 1)->get()->row();
		$header = '<table style="border:none;">
						<tr>
							<td rowspan=3><img src="'.base_url() . 'third_party/img/logo.jpg" width="80px" height="52px" style="margin-right: 10px; float:left;"></td>
							<td>'. $empresa->razao_social .' CNPJ: '. $empresa->cnpj .'</td>
						</tr>
						<tr>
							<td>'. $empresa->endereco .' - '. $empresa->bairro .' - '.$empresa->cep .' - '.$empresa->cidade .' - '. $empresa->estado .'</td>
						</tr>
						<tr>
							<td>Telefone: '. $empresa->telefone_1 .' - E-mail: '.$empresa->email .'</td>
						</tr>
					</table>';
		
		$itens = $dados['itens'];
		$cliente = $dados['cliente'];
		$transportadora = $dados['transportadora'];
		$representante = $dados['representante'];
		$nota_fiscal = $dados['nota_fiscal'];
		
		$this->load->view('pdf',array('header'=>$header,'html'=>$this->load->view('consultas/itens', 
			array(
				'itens' => $itens,
				'cliente' => $cliente,
				'transportadora' => $transportadora,
				'representante' => $representante,
				'nota_fiscal' => $nota_fiscal
			), TRUE)));
		
	}
	
	
	
}