<?php

class Relatorios extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	
	private function retorna_total_registro_acessos()
	{
		$r = $this->db->select("distinct( id_usuario)
				FROM (`acessos_usuarios`)
				GROUP BY  `id_usuario`")
			->get()
			->result();
		$total_registro = 0;
		foreach ($r as $acesso){
			$total_registro ++;
		}
		return($total_registro);
	}
	
	function resultados_representantes()
	{
		$conteudo = heading('Resultados do Representante', 2);
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		$conteudo .= form_open(current_url());
		$conteudo .= '<p>' . form_label('Representante: ' . br() . form_dropdown('codigo_usuario', $this->_obter_representantes(), $this->input->post('codigo_usuario'))) . '</p>';
		$conteudo .= '<p>' . form_label('Data:' . br() . form_input('data_inicial', $this->input->post('data_inicial'), 'class="datepicker data_inicial" size="6"')) . ' à ' . form_input('data_final', $this->input->post('data_final'), 'class="datepicker" size="6"') . '</p>';
		$conteudo .= '<p>' . form_submit('consultar', 'Consultar') . '</p>';
		$conteudo .= form_close();
		
		if ($_POST)
		{
			if ($this->input->post('data_inicial'))
			{
				$data_inicial = explode('/', $this->input->post('data_inicial'));
				$inicio_timestamp = mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
			}
			
			if ($this->input->post('data_final'))
			{
				$data_final = explode('/', $this->input->post('data_final'));
				$fim_timestamp = mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]);
			}
			
			$conteudo .= '
				<div class="topo_grid"></div>
				<table cellspacing="0" class="novo_grid">
					<thead>
						<tr>
							<th>Cód. Repres.</th>
							<th>Nome</th>
							<th>Metas</th>
							<th>Realizado</th>
							<th>%</th>
							<th>Premiações</th>
							<th>Positivação Clientes</th>
							<th>Orçamentos Convertidos</th>
						</tr>
					</thead>
					
					<tbody>
			';
			
			//
			
			if ($this->input->post('codigo_usuario'))
			{
				$representantes[$this->input->post('codigo_usuario')] = NULL;
			}
			else
			{
				$representantes = $this->_obter_representantes();
			}
			$_representantes = array();
			foreach ($representantes as $indice => $valor)
			{
				$_representantes[] = $this->db->from('usuarios')->where('id', $indice)->get()->row();
			}
			$representantes = $_representantes;
			
			//
			
			foreach ($representantes as $representante)
			{
				$total_meta_vendas = $this->db_cliente->obter_total_meta_vendas($representante->codigo, $inicio_timestamp, $fim_timestamp);
				$total_vendas = $this->db_cliente->obter_total_vendas($representante->codigo, $inicio_timestamp, $fim_timestamp);
				$total_premiacao_vendas = $this->db_cliente->obter_total_premiacao_vendas($representante->codigo, $inicio_timestamp, $fim_timestamp);
				$total_prospects_convertidos = $this->db->from('prospects')->where(array('status' => 'convertido_cliente', 'timestamp >=' => ($inicio_timestamp ? $inicio_timestamp : 0), 'timestamp <=' => ($fim_timestamp ? $fim_timestamp : 0)))->get()->num_rows();
				
				$conteudo .= '
					<tr>
						<td class="right">' . $representante->codigo . '</td>
						<td>' . $representante->nome . '</td>
						<td class="right">' . number_format($total_meta_vendas, 2, ',', '.') . '</td>
						<td class="right">' . number_format($total_vendas, 2, ',', '.') . '</td>
						<td class="right">' . number_format($total_meta_vendas > 0 ? (($total_vendas / $total_meta_vendas) * 100) : 0, 2, ',', '.') . '</td>
						<td class="right">' . number_format($total_premiacao_vendas, 2, ',', '.') . '</td>
						<td class="right">' . $total_prospects_convertidos . '</td>
						<td class="right">{0}</td>
					</tr>
				';
			}
			
			$conteudo .= '</tbody></table>';
		}
		
		$conteudo .= '<div class="rodape_grid"></div>';
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function relatorio_acessos()
	{
		$conteudo .= '<div class="caixa"><ul>';
		$conteudo .= '<li>' . anchor('#', 'Imprimir', 'onclick="print(); return false;"') . '</li>';
		$conteudo .= '</ul></div><br>';
		
		$conteudo .= heading('Relatório de Acessos', 2).'<br>';
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		//$conteudo .= form_open(current_url());
		
		if(isset($_POST) || isset($_GET)){
			$filtros = array(
				array(
					'nome' => 'usuario',
					'descricao' => 'Usuário',
					'tipo' => 'texto',
					'campo_mysql' => 'id_usuario',
					'ordenar' => 0
				),
				array(
					'nome' => 'criacao',
					'descricao' => 'Data',
					'tipo' => 'data',
					'campo_mysql' => 'timestamp',
					'ordenar' => 1
				)
			);
			
			// RETORNA CONSULTA COM TODOS OS USUARIOS ENCONTRADOS
			//$this->filtragem_mysql($filtros);
			$limit_consulta = 20;
			$this->db->group_by("id_usuario"); 
			$acessos = $this->db
				->select('COUNT(id_usuario) as ranking, id_usuario, timestamp')
				->from('acessos_usuarios')
				//->limit($limit_consulta, $this->input->get('per_page'))
				->order_by('ranking', 'desc')
				->get()
				->result();
			
			$consulta = $this->retorna_relatorios_acessos($acessos, $limit_consulta);
			$paginacao = $this->paginacao_total($consulta[2], $limit_consulta);
		}
		
		
		$conteudo .= '<div class="topo_grid">';
		$conteudo .= '	<p><strong>Filtros</strong></p>';
		
		$conteudo .= form_open(current_url(), array('method' => 'get'));
		
		$conteudo .= $this->filtragem($filtros);
		
		$conteudo .= '<div style="clear: both;"></div>';
	
		$conteudo .= form_close();
		
		$conteudo .= '</div>';
		
		$conteudo .= $consulta[0];
		
		$conteudo .= $paginacao;
		
		$this->load->view('layout',array('conteudo' => $conteudo));
	}
	
	private function retorna_registros_encontrados(){
		
	}
	
	private function retorna_registro_acesso($acesso, $nome, $ranking)
	{
		return('
			<tr>
				<td class="center">' . $ranking . 'º</td>
				<td>' . $nome . '</td>
				<td class="right">' . $this->db->select('COUNT(id_usuario) as total')->from('acessos_usuarios')->where(array('id_usuario' => $acesso->id_usuario))->get()->row()->total . '</td>
				<td class="center">' . date('d/m/y H:i:s', $this->db->from('acessos_usuarios')->where(array('id_usuario' => $acesso->id_usuario))->order_by("timestamp", "asc")->limit(1)->get()->row()->timestamp) . '</td>
				<td class="center">' . date('d/m/y H:i:s', $this->db->from('acessos_usuarios')->where(array('id_usuario' => $acesso->id_usuario))->order_by("timestamp", "desc")->limit(1)->get()->row()->timestamp) . '</td>
				<td class="nao_exibir_impressao center">' . anchor('relatorios/ver_acessos/' . $acesso->id_usuario, 'Ver Detalhes') .  '</td>
			</tr>');
	}
	
	private function retorna_relatorios_acessos($acessos, $limit_consulta)
	{
		$tabela_cabecalho =
			'<table class="novo_grid">
				<thead>
					<tr>
						<th>Raking</th>
						<th>Usuário</th>
						<th>Acessos Totais</th>
						<th>Primeiro Acesso</th>
						<th>Último Acesso</th>
						<th class="nao_exibir_impressao">Opções</th>
					</tr>
				</thead>
				<tbody>';
		
		$i = $this->paginacao_contador_asc();
		//echo 'I:'.$i.'<br>';
		$limite_imprimir = ($i + $limit_consulta) -1;
		//echo 'Limite:'.$limite_imprimir;
		
		$tabela_corpo = '';
		$ranking = 0;
		$total_registro_nome = 1;
		foreach ($acessos as $acesso)
		{
			$ranking ++;
			if($this->input->get('usuario'))
			{
				$j = $this->retorna_busca_por_nome($acesso, $this->input->get('usuario'));
				if($j[0]['nome'])
				{
					//echo '<br>('.$total_registro_nome.' >= '.$i.' && '.$i.' <= '.$limite_imprimir.')';
					//if($i >= $total_registro_nome && $i <= $limite_imprimir)
					if($total_registro_nome >= $i && $i <= $limite_imprimir)
					{//echo '...';
						$tabela_corpo .= $this->retorna_registro_acesso($acesso, $j[0]['nome'], $ranking);
						$i++;
					}
					$total_registro_nome ++;
				}
			}
			else
			{
				if($ranking >= $i && $i <= $limite_imprimir)
				{
					$nome = $this->db->from('usuarios')->where(array('id' => $acesso->id_usuario))->get()->row()->nome;
					$tabela_corpo .= $this->retorna_registro_acesso($acesso, $nome, $i);
					$i++;
				}
			}
		}
		
		$tabela_corpo.= '</tbody></table>';
		$conteudo[0] = $tabela_cabecalho . $tabela_corpo;
		
		if($this->input->get('usuario')){
			$conteudo[1] = $total_registro_nome;
			$conteudo[2] = $total_registro_nome -1;
		}
		else{
			$conteudo[1] = $ranking;
			$conteudo[2] = $ranking;
		}
		
		//if(!$conteudo[2])
		//	$conteudo[0] = null;
		
		//echo '<br>Total consultado:'.$conteudo[1];
		//echo 'Total de registro:'.$conteudo[2];
		
		return($conteudo);
	}

	private function retorna_busca_por_nome($acesso, $id_usuario_get)
	{
		$query = $this->db->query("
				SELECT
					distinct(usuarios.id),
					usuarios.nome
				FROM usuarios, acessos_usuarios
				where usuarios.id = '". $acesso->id_usuario ."'
					AND usuarios.nome like '%". $id_usuario_get ."%' 
				limit 1;");
		
        $j = $query->result_array();
		return($j);
	}
	
	function ver_acessos($id = NULL)
	{
		$usuario = $this->db->from('usuarios')->where(array('id' => $id))->get()->row();
		
		// setar valores padrões (último mes)
		if (!$_GET)
		{
			$_GET['tipo'] = 'este_mes';
		}
		
		$conteudo = '
			<div class="caixa">
				<ul>
					<li>' . anchor('#', 'Voltar', 'onclick="history.go(-1); return false;"') . '</li>
				</ul>
			</div>
		';
		
		$conteudo .= heading('Acessos', 2);
		
		$conteudo .= '<div class="nao_exibir_impressao">';
		
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		
		$conteudo .= '<div class="topo_grid">';
		$conteudo .= '	<p><strong>Filtros</strong></p>';
		
		//$conteudo .= form_open(current_url());
		$conteudo .= form_open(current_url(), array('method' => 'get'));
		
		$conteudo .= '<div style="float: left; margin-right: 20px;">';
		
		$conteudo .= '<p>' . form_label('Forma de pesquisa:' . br()).
						'<select name="tipo">
							<option value="este_mes">Mês atual</option>
							<option value="#">Pesquisa por data</option>
						</select>
						</p>';
		$conteudo .= '</div>';
		
		$conteudo .= '<div style="float: left; margin-right: 20px;">';
		$conteudo .= '<p>' . form_label('Data:' . br() . form_input('data_inicial', $this->input->post('data_inicial'), 'class="datepicker data_inicial" size="6"')) . ' à ' . form_input('data_final', $this->input->post('data_final'), 'class="datepicker" size="6"') . '</p>';
		$conteudo .= '</div>';
		
		$conteudo .= '<div style="clear: both;"></div>';
	
		$conteudo .= '<p>' . form_submit('filtrar', 'Filtrar') 
			.' <input type="submit" value="Limpar Filtros" onclick="resetar_form($(this).parents(\'form\'), true); return false;">'
			. '</p>';
		
		$conteudo .= form_close();
		
		$conteudo .= '</div>';
		
		if($id)
		{
			if ($_POST || $_GET)
			{
				if ($_GET['tipo'] == 'este_mes')
				{
					$_GET['data_inicial'] = date('01/m/Y');
					$_GET['data_final'] = date('t/m/Y');
				}
				
				if ($this->input->get('data_inicial'))
				{
					$data_inicial = explode('/', $this->input->get('data_inicial'));
					$inicio_timestamp = mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
				}
				
				if ($this->input->get('data_final'))
				{
					$data_final = explode('/', $this->input->get('data_final'));
					$fim_timestamp = mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]);
				}
				
				if ($data_inicial && $data_final)
					$this->db->where(array('id_usuario' => $id, 'timestamp >= ' => $inicio_timestamp, 'timestamp <= ' => $fim_timestamp));
			}
			
			$limit_consulta = 20;
			$acessos = $this->db
				->from('acessos_usuarios')
				->where('id_usuario', $id)
				->limit($limit_consulta, $this->input->get('per_page'))
				->order_by('timestamp', 'desc')->get()->result();
			
			// RETORNA PAGINACAO PRONTA
			$paginacao = $this->paginacao_rapida();
			$contador = $this->paginacao_contador_desc($this->get_total_registro_paginacao(), $limit_consulta);
			
			/*
			$conteudo .= '
				<ul>
					<li><strong>Usuário:</strong> ' . ($usuario->nome_real ? $usuario->nome : $usuario->nome) . '</li>
					<li><strong>Período:</strong> ' . date('d/m/Y', $inicio_timestamp) . ' a ' . date('d/m/Y', $fim_timestamp) . '</li>
					<li><strong>Total de acessos:</strong> ' . count($acessos) . '</li>
				</ul>
			';	*/
			
			if ($acessos)
			{
				
				$conteudo .= '<table cellspacing="0" class="novo_grid" style="float: left;">
				<thead>
					<tr>
						<th>Acesso</th>
						<th>Data</th>
						<th>Local</th>
					</tr>
				</thead>
				<tbody>	';
				
				$i = count($acessos);
				foreach ($acessos as $acesso)
				{
					$desc_acesso = ($i == count($acessos)) ? 'Último Acesso' : $i . 'º Acesso';
					
					$conteudo .= '
						<tr>
							<td class="center">' . $contador . 'º</td>
							<td class="center">' . date('d/m/y H:i:s', $acesso->timestamp) . '</td>
							<td class="center"><a href="#local_google_maps" class="ver_local" latitude="' . $acesso->lat . '" longitude="' . $acesso->lon . '" descricao="' . $desc_acesso . ' - ' . date('d/m/Y H:i:s', $acesso->timestamp) . '">Ver Local</a></td>
						</tr>';
						//<td class="center">
						//	<a href="#" class="ver_local" latitude="' . $acesso->lat . '" longitude="' . $acesso->lon . '" descricao="' . $desc_acesso . ' - ' . date('d/m/Y H:i:s', $acesso->timestamp) . '">Ver Local</a>
						//</td>
						
						//<a href="#nota_fiscal_' . $nota_fiscal['codigo'] . '" class="colorbox_inline">Ver Títulos</a>
						
					
					$i--;
					$contador--;
				}

				$conteudo .= '</tbody></table>';
				//$conteudo .= '</div><br>';
				
				$conteudo .= $paginacao;
				
				$conteudo .= '<div id="mapa" style="display: none; float: left; margin-left: 20px;">
						<div style="border: 1px solid #ddd; width: 400px; height: 280px; margin-top: 10px;"></div>
					</div>
					
					<div style="clear: both;"></div>';
				
				$conteudo .= '
					<script>
						$(document).ready(function() {
							setTimeout(function() { $(".ver_local:eq(0)").trigger("click"); }, 1000);
							
							$(".ver_local").click(function(e) {
								var lat = $(this).attr("latitude");
								var lon = $(this).attr("longitude");
								var descricao = $(this).attr("descricao");
								
								if (!lat || !lon)
								{
									$("#mapa").hide();
									
									// e.clientX checa se o evento foi disparado pelo click do USUÁRIO
									if (e.clientX)
										alert("Não foi possível rastrear o local.");
								}
								else
								{
									new google.maps.Geocoder().geocode({ "latLng": new google.maps.LatLng(lat, lon) }, function(dados) {
										$("#mapa").show().find("h3").text(descricao).end().find("div").goMap({ markers: [{ latitude: lat, longitude: lon, html: { content: dados[0].formatted_address, popup: true } }], zoom: 15, maptype: "ROADMAP" });
									});
								}
								
								return false;
							});
						});
					</script>
				';
				
				//$conteudo .= $this->load->view('relatorios/lista_acessos', array('acessos' => $acessos, 'paginacao' => $paginacao, 'contador' => $contador));
			}
			else
			{
				$conteudo .= '<p class="erro">Nenhum resultado encontrado.</p>';
			}
		}
		
		$this->load->view('layout', array('conteudo' => $conteudo));
		
	}
	//
	
	function _obter_representantes()
	{
		$representantes = $this->db->from('usuarios')->where('status', 'ativo')->where('grupo', 'representantes')->order_by('nome', 'asc')->get()->result();
		$_representantes = array();
		
		foreach ($representantes as $representante)
		{
			$_representantes[$representante->id] = $representante->nome . ' - ' . $representante->codigo;
		}
		
		return $_representantes;
	}
	
}