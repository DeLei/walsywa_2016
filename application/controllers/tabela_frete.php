<?php

class Tabela_frete extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	
	function index()
	{
		/*
		$conteudo = '
			<div class="caixa">
				<ul>
					<li>' . anchor('tabela_frete', 'Voltar', 'onclick="history.go(-1); return false;"') . '</li>
				</ul>
			</div>
		';*/
		
		$conteudo .= heading('Tabela de Frete', 2);
		$conteudo .= '<br /><br />';
		
		$this->db->order_by('id', 'ASC');
		$tabela_frete = $this->db->from('tabela_frete')->get()->result();
		
		
		//Inserir metas
		$conteudo .= '
			<script type="text/javascript">
				$(document).ready(function(){
					$(".inserir_frete").live("click", function(e){
						e.preventDefault();
						
						var valor_banco = $(this).attr("href");
						
						var valor_frete = prompt("Digite o valor da frete:");
						
						var link = $(this);
						
						if(valor_frete)
						{
							$.ajax({
								type: "POST",
								url: "' . base_url() . 'index.php/tabela_frete/inserir_frete",
								data: {valor_banco: valor_banco, valor_frete: valor_frete},
								success: function(dados){
									link.parent().html(dados);
								}
							});
						}
						
					});
				});
			</script>
		';
		
		
		
		$conteudo .= '<table cellspacing="0" class="novo_grid"><thead>';
		
		$conteudo .= '<th></th>';
		$conteudo .= '<th>Quantidade em KG</th>';
		$conteudo .= '<th>A</th>';
		$conteudo .= '<th>B</th>';
		$conteudo .= '<th>C</th>';
		$conteudo .= '<th>D</th>';
		$conteudo .= '<th>L</th>';
		$conteudo .= '<th>I</th>';
		
		$conteudo .= '</thead><tbody>';
		
		foreach($tabela_frete as $valores)
		{
			$conteudo .= '<tr>';
	
				$conteudo .= '<td class="center">' . $valores->id . '</td>';
	
				if($valores->kg == 1)
				{
					$conteudo .= '<td class="right">1 até 600</td>';
				}
				else if($valores->kg == 601)
				{
					$conteudo .= '<td class="right">601 até 999</td>';
				}
				else if($valores->kg == 1000)
				{
					$conteudo .= '<td class="right">1000 até 1999</td>';
				}
				else if($valores->kg == 2000)
				{
					$conteudo .= '<td class="right">2000 até 2999</td>';
				}
				else if($valores->kg == 3000)
				{
					$conteudo .= '<td class="right">3000 até 9999</td>';
				}
				else if($valores->kg == 10000)
				{
					$conteudo .= '<td class="right">10000 até 9999999999</td>';
				}
	
				$conteudo .= '<td class="right"><a href="' . $valores->kg . '|a" class="inserir_frete">' . $valores->a . '</a></td>';
				$conteudo .= '<td class="right"><a href="' . $valores->kg . '|b" class="inserir_frete">' . $valores->b . '</a></td>';
				$conteudo .= '<td class="right"><a href="' . $valores->kg . '|c" class="inserir_frete">' . $valores->c . '</a></td>';
				$conteudo .= '<td class="right"><a href="' . $valores->kg . '|d" class="inserir_frete">' . $valores->d . '</a></td>';
				$conteudo .= '<td class="right"><a href="' . $valores->kg . '|l" class="inserir_frete">' . $valores->l . '</a></td>';
				$conteudo .= '<td class="right"><a href="' . $valores->kg . '|i" class="inserir_frete">' . $valores->i . '</a></td>';
			
			$conteudo .= '</tr>';
		}
		
		$conteudo .= '</body>';
		$conteudo .= '</table>';
		
		
		
		$conteudo .= form_open_multipart(current_url());
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	
	
	function inserir_frete()
	{
		$valor_banco = $_POST["valor_banco"];
		$valor_frete = $_POST["valor_frete"];
		
		$valor_banco = explode('|', $valor_banco);
		$posicao_kg = $valor_banco[0];
		$posicao_frete = $valor_banco[1];
		
		if($valor_frete == NULL)
		{
			$valor_frete = 0;
		}
		
		$this->db->where(array('kg' => $posicao_kg));
		$this->db->update('tabela_frete', array(
							$posicao_frete => $valor_frete
						));
						
		$resultado = $this->db->select($posicao_frete)->from('tabela_frete')->where('kg', $posicao_kg)->get()->row();
		
		echo '<a href="' . $posicao_kg . '|' . $posicao_frete . '" class="inserir_frete">' . $resultado->$posicao_frete . '</a>';

	}
	
}