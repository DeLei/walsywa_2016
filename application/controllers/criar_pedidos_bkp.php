<?php

class Criar_pedidos extends MY_Controller {

    /**
     * Variável para armazenamento de dados exibidos na sessão.
     * @var array()
     */
    private $data;

    function __construct() {
        parent::__construct();
        $this->load->model('autocomplete');
    }

    function index() {
        redirect('criar_pedidos/informacoes_cliente');
    }

    //ANALIZADO
    //($id_pedido = NULL, $imprimir = FALSE, $filial = NULL, $id_pedido_2 = NULL, $tipo_pedido_2 = NULL)
    //AGUARDANDO ANALISE
    //http://localhost:85/pdr_matriz_ajuste_1/index.php/criar_pedidos/informacoes_cliente/pedido/000001/1-00006/1/I
    function copiar_pedido_analizado($id_pedido = NULL, $filial = NULL) {
        // Se nao vier filial, o pedido é buscado na SZW
        // Se houver filial, o pedido é buscado na SC5
        #http://localhost:85/pdr_matriz_ajuste_1/index.php/criar_pedidos/informacoes_cliente/pedido/1//1/I
        $pedido = $this->db_cliente->obter_pedidos_pdr($id_pedido, FALSE, $filial);
        echo '<pre>';
        print_r($pedido);
        echo '</pre>';

        if ($pedido) {
            echo 'if';
            $produtos = $this->db_cliente->obter_pedidos_pdr($id_pedido, TRUE, $filial);
        } else {
            echo 'else';
            $pedido = $this->db_cliente->obter_pedido($id_pedido, 0, 0, $filial);
            $itens = $pedido['itens'];
        }

        $empresa = $this->db->from('empresas')->where('id', 1)->get()->row();
        echo '<pre>';
        print_r($pedido);
        echo '</pre>';
    }

    function copiar_pedido($representante = NULL, $pedido = NULL, $filial = NULL, $editar = NULL) {
        if (!$representante || !$pedido) {
            redirect('pedidos');
        }
        $sessao_pedido = $this->db_cliente->obter_pedidos_pdr($pedido, TRUE, $filial);

        if (!$editar) {
            $sessao_pedido['id_pedido'] = null;
        }
        $this->session->set_userdata('sessao_pedido', $sessao_pedido);
        if ($sessao_pedido['tipo_pedido'] == 'O') {
            redirect('criar_pedidos/informacoes_cliente/orcamento');
        } else {
            redirect('criar_pedidos');
        }
    }

    function copiar_pedido_sc($representante = NULL, $pedido = NULL, $filial = NULL) {
        if (!$representante || !$pedido) {
            redirect('pedidos');
        }
        $sessao_pedido = $this->db_cliente->obter_pedido($pedido, 0, 0, $filial);

        $sessao_pedido['codigo_usuario'] = $sessao_pedido['codigo_representante'];
        $sessao_pedido['codigo_filial'] = $sessao_pedido['filial'];
        $sessao_pedido['tipo_pedido'] = 'V';

        $sessao_pedido['id_pedido'] = null;
        $sessao_pedido['codigo'] = null;

        $this->session->unset_userdata('sessao_pedido');
        $this->session->set_userdata('sessao_pedido', $sessao_pedido);

        // coloca tabela de preço nos produtos
        foreach ($sessao_pedido['itens'] as $key => $produto) {
            //$this->add_produtos();
            $this->add_produtos($sessao_pedido['itens'][$key]["codigo_produto"], $sessao_pedido['itens'][$key]["quantidade_vendida_produto"], $sessao_pedido['itens'][$key]["preco_produto"], $sessao_pedido["tabela_precos"], $sessao_pedido['filial']);
        }


        redirect('criar_pedidos');
    }

	// CORREÇÃO
	function editar($tipo = 'pedido',$codigo_usuario = NULL, $id_pedido = NULL  ){
		 $this->deletar_sessao($tipo);
	
		
	
		$url = 'criar_pedidos/informacoes_cliente/'.($tipo == TIPO_ORCAMENTO ? $tipo : '') . $codigo_usuario.'/'.$id_pedido;
	
		redirect('criar_pedidos/informacoes_cliente/'.$tipo.'/'. $codigo_usuario.'/'.$id_pedido);
	}
	
	// FIM CORREÇÃO
	
	function aplicar_shazam($tipo) {
		$pedido = $this->obter_sessao($tipo);
		$pedido = $this->shazam_model->aplicar_descontos_shazam($pedido);
		//$pedido['shazam'] = true;
		$this->atualizar_sessao($pedido, $tipo);
		redirect('criar_pedidos/adicionar_produtos' . ($tipo == 'orcamento' ? '/orcamento' : ''));
	}
	
    // Etapa 1
    function informacoes_cliente($tipo = 'pedido', $codigo_usuario = NULL, $id_pedido = NULL, $copiar = NULL, $onde = NULL, $id_pro_cli = NULL) {
		
        //CUSTOM E175/2014 - Edição de Orçamento
        // Adicionada regra para obter a sessão do Pedido ou orçamento separadamente, de acordo com o funcionamento da Matriz
        $pedido = $this->obter_sessao($tipo);
		

        $cliente_sessao = $pedido['cliente'];
        $prospect_sessao = $pedido['nome_prospect'];
		
        if (($cliente_sessao || $prospect_sessao)) {

            /* $erro = array(
              'tipo_erro' => 'alteracao_de_cliente',
              'mensagem' => 'Você não pode alterar o '.(($tipo == TIPO_ORCAMENTO)?'Cliente/Prospect de um Orçamento':'Cliente de um Pedido').' com produtos já incluídos.'
              );
              $this->session->set_flashdata('erro', $erro); */
            //if (!$copiar) {
				//Chamado 001892 - Dois botões de copiar
                //redirect('criar_pedidos/adicionar_produtos/' . ($tipo == TIPO_ORCAMENTO ? $tipo : '') . '/' . $copiar);
            //}
            if ($copiar == 1) {
                $this->session->set_userdata(array('copiando' => 1));
            } else {
                $this->session->set_userdata(array('copiando' => 0));
            }
        }

        //Obter Dados
        // Obter Prospect e Cliente em Orçamento

        if ($tipo == TIPO_ORCAMENTO && $id_pro_cli) {
            //echo '<br>1 tipo orcamento';
            $codigo_cliente = explode('-', $id_pro_cli);
			
            //Obter Cliente
            //Se $codigo_cliente[1] receber o codigo da LOJA, recebe os dados do cliente, se não, recebe os dados do prospect
            if ($codigo_cliente[1]) {
                $cliente = $this->db_cliente->obter_cliente($codigo_cliente[0], $codigo_cliente[1]);

                $_POST['codigo_loja_cliente'] = $codigo_cliente[0] . '|' . $codigo_cliente[1];
                $_POST['_codigo_loja_cliente'] = $cliente['codigo'] . ' - ' . $cliente['nome'] . ' - ' . $cliente['cpf'];
				

                $editar['cliente'] = $cliente;
            } else {
                //Obter Prospect
                $prospect = $this->db->from('prospects')->where('id', $id_pro_cli)->get()->row();

                $_POST['id_prospect'] = $prospect->id;
                $_POST['_id_prospect'] = $prospect->nome . ' - ' . $prospect->cpf;
            }
        }

        //Obter Cliente em Pedidos
        if ($tipo == TIPO_PEDIDO && $id_pro_cli) {
            //echo '<br>2 obter cliente pedido';
            $codigo_cliente = explode('-', $id_pro_cli);

            $cliente = $this->db_cliente->obter_cliente($codigo_cliente[0], $codigo_cliente[1]);

            $_POST['codigo_loja_cliente'] = $codigo_cliente[0] . '|' . $codigo_cliente[1];
            $_POST['_codigo_loja_cliente'] = $cliente['codigo'] . ' - ' . $cliente['nome'] . ' - ' . $cliente['cpf'];
			
        }

        //** Editar

        if ($id_pedido) {
            //echo '3 obter pedido';
            //echo 'if..';
            //Se Onde, Copiar Pedido Analizado, se não, Copiar Pedido Aguardadno analise
            //Obter Pedido para ser Editado

            $pedido = $this->db_cliente->obter_pedidos_pdr($id_pedido, FALSE, NULL, $tipo);
            $produtos = $this->db_cliente->obter_pedidos_pdr($id_pedido, TRUE, NULL, $tipo);
            //print_r($pedido);
            $editar = array();

            //Copiar Pedido

            if (!$copiar) {
                $editar['id_pedido'] = $pedido['id_pedido'];
            }

			//chamado 239
				if(!$editar['id_pedido'])
				{
					// chamado 50
						$pedidoExplode =  split("-", $id_pedido);
						$ultimoPedido = $this->db_cliente->obterUltimoPedido($pedidoExplode[0]);
						$gerarNovoCodigo = split("-", $ultimoPedido->ZW_IDPED);
						$user = $gerarNovoCodigo[0];
						$novoCod = $gerarNovoCodigo[1] + 1;
						$temp = $user . '-' . $novoCod;
						$editar['id_pedido'] = $temp;
					// fim chamdo 50
				}
			//fim chamado 239
			
            $editar['codigo_usuario'] = $pedido['id_usuario'];
            $editar['data_emissao'] = $pedido['data_emissao'];
			$editar['vencimento_shazam'] = $pedido['vencimento_shazam'];
            $editar['codigo_loja_cliente'] = $pedido['codigo_cliente'] . '|' . $pedido['loja_cliente'];
            $cliente = $this->db_cliente->obter_cliente($pedido['codigo_cliente'], $pedido['loja_cliente']);
            $editar['cliente'] = $cliente;
            $editar['_codigo_loja_cliente'] = $pedido['codigo_cliente'] . ' - ' . $cliente['nome'] . ' - ' . $cliente['cpf'];
            $editar['nome_cliente'] = $cliente['nome'];
            $editar['tipo'] = $tipo;
            $editar['id_prospect'] = $pedido['id_prospects'];
            $prospect = $this->db->from('prospects')->where('id', $pedido['id_prospects'])->get()->row();
            $editar['nome_prospect'] = $prospect->nome;
            $editar['codigo_tabela_precos'] = $pedido['tabela_precos'];
            $editar['tipo_pedido'] = $pedido['tipo_pedido'];
            $editar['nome_tipo_pedido'] = $tipo_pedido[$pedido['tipo_pedido']];
            $editar['total_st'] = $pedido['substituicao_tributaria'];
            $editar['codigo_forma_pagamento'] = $pedido['condicao_pagamento'];
            $editar['tipo_frete'] = $pedido['tipo_frete'];
            $editar['id_feira'] = $pedido['id_feira'];
            $editar['codigo_transportadora'] = $pedido['codigo_transportadora'];
            $editar['codigo_ordem_compra'] = $pedido['ordem_compra'];
            $editar['data_entrega'] = date('d/m/Y', strtotime($pedido['data_entrega']));
            $editar['observacao_pedido_imediato'] = $pedido['obs_pedido'];
            $editar['autorizado'] = $pedido['autorizado'];
            $editar['cultura'] = $pedido['cultura'];
            $editar['modent'] = $pedido['modent'];

			$produtos_alteracao_tabela_preco = array();
            foreach ($produtos as $indice => $produto) {			
				//Caso esteja sendo copiado o pedido / orcamento será alterado o preço para o preço unitário para o preço de tabela
				$editar_produtos[$indice]['preco'] = $produto['preco_unitario'];
				if($copiar){
					$dados_produto = $this->db_cliente->obter_produto($produto['codigo_produto'], $produto['tabela_precos'], $produto['filial']);
					if(floatval($dados_produto['preco']) != floatval($produto['preco_unitario'])){						
						$this->session->set_flashdata('copia_produto_alterados','Os produtos copiados com preço diferente da tabela de preços serão alterados');					
						$editar_produtos[$indice]['preco'] = number_format($dados_produto['preco'],5);
						$produtos_alteracao_tabela_preco[$produto['codigo']] = 'Produto '. $produto['codigo'].' - '.$produto['descricao']. ' preço de tabela.';	
					}
				}
				
				//
				
                $total_dos_produtos_sem_ipi = $produto['preco_unitario'] * $produto['quantidade'];
                $total_dos_produtos_com_ipi = $total_dos_produtos_sem_ipi + ($total_dos_produtos_sem_ipi * ($produto['ipi'] / 100));
                //WebService
                $array = $this->db_cliente->obter_impostos_item($pedido['codigo_cliente'], $pedido['loja_cliente'], $cliente['tipo'], $produto['quantidade'], $produto['preco_unitario'], $this->input->post('codigo_produto'), $produto['filial']);
                $array = $this->db_cliente->calcular_st('010', $this->input->post('codigo_produto'), $pedido['codigo_cliente'], $pedido['loja_cliente'], $total_dos_produtos_sem_ipi, $total_dos_produtos_com_ipi, $produto['filial']);
				
				$editar_produtos[$indice]['desconto'] = $produto['desconto'];
                $editar_produtos[$indice]['codigo'] = $produto['codigo_produto'];
                $editar_produtos[$indice]['codigo_real'] = $produto['codigo_produto'];
                $editar_produtos[$indice]['descricao'] = $produto['descricao_produto'];
                $editar_produtos[$indice]['unidade_medida'] = $produto['unidade_medida'];
                $editar_produtos[$indice]['ipi'] = $produto['ipi'];
                $editar_produtos[$indice]['peso'] = $produto['peso_unitario'];
                

                $editar_produtos[$indice]['tabela_precos'] = $this->db_cliente->obter_tabela_precos($produto['tabela_precos']);
                $editar_produtos[$indice]['quantidade_pedido'] = $produto['quantidade'];
                $editar_produtos[$indice]['filial'] = $produto['filial'];
                $editar_produtos[$indice]['st'] = $array['st'];
                $editar_produtos[$indice]['tes'] = $array['tes'];
                $editar_produtos[$indice]['cf'] = $array['cf'];
                //print_r($array);die;

                $previsaoArray = $this->db_cliente->obterPrevisaoChegada($produto['filial'], $produto['codigo_produto']);
                if ($previsaoArray[0]->chegada) {
                    $editar_produtos[$indice]['previsao_chegada'] = date('d/m/Y', strtotime($previsaoArray[0]->chegada));
                }

                //print_r($editar_produtos[$indice]['tabela_precos']);
                $peso_total += $produto['peso_unitario'];
                $valor_total += $produto['preco_unitario'];
                $valor_total_ipi += $produto['preco_unitario'] + $produto['ipi'];
            }

            $editar['valor_frete'] = $this->obter_valor_frete($peso_total, $pedido['codigo_cliente'] . '|' . $pedido['loja_cliente']);
            $editar['valor_total'] = $valor_total;
            $editar['peso_total_geral'] = $peso_total;
            $editar['valor_total_ipi'] = $valor_total_ipi;
            $editar['valor_total_frete'] = $valor_total_ipi + $editar['valor_frete'];

            $sessao_pedido = array();
            if ($editar['modent'] == 'I')
                $editar['produtos'] = $editar_produtos;
            else
                $editar['produtos_programados'] = $editar_produtos;

			//chamado 135
				$editar['editando'] = '1';
			//fim chamado 135
				
            $this->atualizar_sessao($editar, $tipo);
            //CAI NESSE AQUI >>
            if (!$copiar) {
                redirect('criar_pedidos/adicionar_produtos/' . $tipo);
            }
        }
        if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores'))) {
            //echo '<br>5 in array';
            // se nenhum código de usuário foi informado vamos pegar o primeiro código disponível no dropdown
            if (!$codigo_usuario) {
                $codigo_usuario = $this->_obter_indice_primeiro_usuario_usuarios('codigo');
            }

            $_codigo_usuario = $codigo_usuario;
        } else {
            //echo '<br>6 else in array';
            $_codigo_usuario = $this->session->userdata('codigo_usuario');
        }
        $codigo_usuario = $codigo_usuario ? $codigo_usuario : $_codigo_usuario;


        //----------------------------------------

        if ($_POST) {
            //echo '<br>7 if POST';
            if (!$this->input->post('codigo_loja_cliente')) {
                $cliente_prospect = $this->input->post('id_prospect');
                $_POST['codigo_loja_cliente'] = 000010;
            } else {
                $cliente_prospect = $this->input->post('codigo_loja_cliente');
            }

            if (!$cliente_prospect) {
                if ($this->input->post('ClienteOuProspet') == 'prospect') {
                    $erro = 'Selecione um prospect.';
                } else {
                    $erro = 'Selecione um cliente.';
                }
            } else {
                //echo '<br>8 else POST';
                $codigo_usuario = $this->input->post('codigo_usuario');
                if (!$codigo_usuario) {
                    $codigo_usuario = $_codigo_usuario;
                }

                //Obter Nomes
                $codigo_loja_cliente = explode('|', $this->input->post('codigo_loja_cliente'));
                $cliente = $this->db_cliente->obter_cliente($codigo_loja_cliente[0], $codigo_loja_cliente[1]);
                $prospect = $this->db->from('prospects')->where('id', $this->input->post('id_prospect'))->get()->row();

                if ($this->input->post('ClienteOuProspet') == 'prospect') {
                    $dados_sessao = array(
                        'codigo_usuario' => $codigo_usuario,
                        'codigo_loja_cliente' => '|',
                        '_codigo_loja_cliente' => '',
                        'cliente' => 0,
                        'nome_cliente' => 0,
                        'tipo' => $tipo,
                        'id_prospect' => $this->input->post('id_prospect'),
                        'nome_prospect' => $this->input->post('_id_prospect'),
                        'nome_prospects' => $prospect->nome
                    );
                } else {
                    $dados_sessao = array(
                        'codigo_usuario' => $codigo_usuario,
                        'codigo_loja_cliente' => $this->input->post('codigo_loja_cliente'),
                        //'_codigo_loja_cliente' => $this->input->post('_codigo_loja_cliente'),
                        '_codigo_loja_cliente' => "{$cliente['codigo']} - {$cliente['nome']} - {$cliente['cpf']}",
                        'cliente' => $cliente,
                        'nome_cliente' => $cliente['nome'],
                        'tipo' => $tipo,
                        'id_prospect' => 0,
                        'nome_prospect' => null,
                        'nome_prospects' => null
                    );
                }
                // ALTERA O CLIENTE E MANTEM PRODUTOS CARREGADOS NA SESSAO
                $aux_session = $this->obter_sessao($tipo);
				
                if ($aux_session) {
                    if (array_key_exists('id_pedido', $aux_session)) {
                        $dados_sessao['id_pedido'] = $aux_session['id_pedido'];
                    }
                }
                $dados_sessao['produtos'] = $aux_session['produtos'];
                $dados_sessao['produtos_programados'] = $aux_session['produtos_programados'];

                /*                 * ************** */
                $_produto = $dados_sessao['produtos'];
                $_produto_programados = $dados_sessao['produtos_programados'];

				//chamado 135
					$dados_sessao['copiando'] = $copiar;
				//fim chamado 135
				
                $this->atualizar_sessao($dados_sessao, $tipo);

                $sessao_pedido = $this->obter_sessao($tipo);
                if ($sessao_pedido['codigo_usuario']) {
                    $this->db->delete('pedidos', array('id_usuario' => $sessao_pedido['codigo_usuario']));
                }
                redirect('criar_pedidos/adicionar_produtos/' . ($tipo == TIPO_ORCAMENTO ? $tipo : '') . '/' . $copiar);
            }
        }
        // recebendo sessão
        $sessao_pedido = $this->obter_sessao($tipo);

        if ($codigo_usuario == $sessao_pedido['codigo_usuario']) {//echo '<br>9 ';
            if ($sessao_pedido['codigo_usuario'] && $sessao_pedido['codigo_loja_cliente']) {
                $_POST['codigo_usuario'] = $sessao_pedido['codigo_usuario'];
                $_POST['codigo_loja_cliente'] = $sessao_pedido['codigo_loja_cliente'];
                $_POST['_codigo_loja_cliente'] = $sessao_pedido['_codigo_loja_cliente'];
            }
        }


        if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores'))) { //echo '<br> 10';
            $dropdown_representantes = $this->_obter_dropdown_representantes('codigo', array('name' => 'codigo_usuario', 'value' => $this->input->post('codigo_usuario') ? $this->input->post('codigo_usuario') : $codigo_usuario, 'id' => 'usuarios'), FALSE);
        }
		//debug_pre($sessao_pedido);
        $this->load->view('layout', array('conteudo' => $this->load->view('criar_pedidos/informacoes_cliente', array
                (
                'erro' => $erro,
                'dropdown_representantes' => $dropdown_representantes,
                'codigo_usuario' => $codigo_usuario,
                'loja_cliente' => $loja_cliente,
                'tipo' => $tipo,
                'copiar' => $copiar,
                'pedido' => $sessao_tipo,
                'sessao_pedido' => $sessao_pedido
                    ), TRUE
            )
                )
        );
    }

    //VALIDA PRODUTOS ADICIONADOS
    private function valida_produtos($produto_sessao, $tipo = NULL) {
        //$codigo_produto, $quantidade, $preco_unico, $desconto
        /* Array
          (
          [codigo] => 460001
          [codigo_real] => 460001
          [descricao] => PINO C/ROSCA 1/4 20X12P
          [unidade_medida] => PC
          [ipi] => 10.00
          [peso] => 0.0000
          [preco] => 0.14
          [desconto] => 0.00
          [tabela_precos] => Array
          (
          [codigo] => 001
          [descricao] => Tabela Precos  ICMS 18%
          )

          [quantidade_pedido] => 32.00
          )
         */
        $sessao_pedido = $this->obter_sessao($tipo);

        $_POST['codigo_produto'] = $produto_sessao['codigo'];
        $_POST['quantidade'] = $produto_sessao['quantidade_pedido'];
        $_POST['preco_unico'] = $produto_sessao['preco'];
        $_POST['desconto'] = $produto_sessao['desconto'];
        $_POST['codigo_tabela_precos'] = $produto_sessao['tabela_precos']['codigo'];

        // Inserindo Quantidade e Produtos na sessao
        $dados_produto = $this->db_cliente->obter_produto($this->input->post('codigo_produto'), $this->input->post("codigo_tabela_precos"));
        $dados_produto['quantidade_pedido'] = $this->input->post('quantidade');
        //$dados_produto['preco'] = str_replace(",",".",$dados_produto['preco']);
        $dados_produto['preco'] = str_replace(",", ".", $this->input->post('preco_unico'));
        //$dados_produto['desconto'] = str_replace(",",".",$this->input->post('desconto'));
        // Buscando Estoque do Produto

        $estoque_produto = $this->obter_estoque_disponivel($this->input->post('codigo_produto'), TRUE, $this->input->post('filial'));

        if ($dados_produto['quantidade_pedido'] > $estoque_produto) {
            // Pedidos Imediato Dividido pelo programado
            if ($estoque_produto > 0) {
                $quantidade_imediata = $estoque_produto;
                $dados_produto['quantidade_pedido'] = $quantidade_imediata;
                if ($sessao_pedido['produtos']) {
                    foreach ($sessao_pedido['produtos'] as $indice => $produto) {
                        if (trim($produto['codigo']) == trim($this->input->post('codigo_produto'))) {
                            $dados_produto['quantidade_pedido'] = $dados_produto['quantidade_pedido'] + $produto['quantidade_pedido'];
                            $sessao_pedido['produtos'][$indice] = $dados_produto;
                            $this->inserir_pedido_temp(trim($this->input->post('codigo_produto')), $sessao_pedido['codigo_usuario'], $dados_produto['quantidade_pedido'], $this->input->post('filial'));
                            $somar_produto = TRUE;
                        }
                    }
                }
                if (!$somar_produto) {

                    $this->inserir_pedido_temp(trim($this->input->post('codigo_produto')), $sessao_pedido['codigo_usuario'], $dados_produto['quantidade_pedido'], $this->input->post('filial'));

                    $sessao_pedido['produtos'][] = $dados_produto;
                }
            }


            // Pedidos Programados
            $quantidade_programada = $this->input->post('quantidade') - $estoque_produto;

            if ($estoque_produto < 0) {
                $dados_produto['quantidade_pedido'] = $this->input->post('quantidade');
            } else {
                $dados_produto['quantidade_pedido'] = $quantidade_programada;
            }

            if ($sessao_pedido['produtos_programados']) {
                foreach ($sessao_pedido['produtos_programados'] as $indice => $produto) {
                    if (trim($produto['codigo']) == trim($this->input->post('codigo_produto'))) {

                        $dados_produto['quantidade_pedido'] = $dados_produto['quantidade_pedido'] + $produto['quantidade_pedido'];

                        $sessao_pedido['produtos_programados'][$indice] = $dados_produto;

                        $somar_produto_programado = TRUE;
                    }
                }
            }

            if (!$somar_produto_programado) {
                $sessao_pedido['produtos_programados'][] = $dados_produto;
            }
        } else {
            //Pedidos Imediatos

            if ($sessao_pedido['produtos']) {
                foreach ($sessao_pedido['produtos'] as $indice => $produto) {
                    if (trim($produto['codigo']) == trim($this->input->post('codigo_produto'))) {
                        $dados_produto['quantidade_pedido'] = $this->input->post('quantidade') + $produto['quantidade_pedido'];

                        $sessao_pedido['produtos'][$indice] = $dados_produto;

                        $this->inserir_pedido_temp(trim($this->input->post('codigo_produto')), $sessao_pedido['codigo_usuario'], $dados_produto['quantidade_pedido'], $this->input->post('filial'));

                        $somar_produto = TRUE;
                    }
                }
            }

            if (!$somar_produto) {
                $this->inserir_pedido_temp(trim($this->input->post('codigo_produto')), $sessao_pedido['codigo_usuario'], $dados_produto['quantidade_pedido'], $this->input->post('filial'));
                $sessao_pedido['produtos'][] = $dados_produto;
            }
        }

        //Adicionando Produtos na Sessão
        $dados_sessao = $sessao_pedido;
        $this->atualizar_sessao($dados_sessao, $tipo);
        //echo '<pre>';print_r($dados_sessao);echo '</pre>';
        //Limpando Campo do Produto Adicionado

        $_POST['codigo_produto'] = "";
        $_POST['_codigo_produto'] = "";
        $_POST['quantidade'] = "";
    }

    function calcular_imposto() {
        $ref = $this->input->server('HTTP_REFERER', TRUE);
        $this->session->set_userdata('imposto_calculado', 1);
        redirect($ref);
    }
	
	function testes() {
		$sessao_pedido = $this->obter_sessao();
		echo '<pre>'; var_dump($sessao_pedido); exit;
	}
	
    //Etapa 2
    function adicionar_produtos($tipo = TIPO_PEDIDO, $copiar = NULL) {
		$sessao_pedido = $this->obter_sessao($tipo);
		
		// Chamado 001809 - Copia com desconto
		$validacao_desconto_maximo = $this->validar_desconto_maximo_classe_cliente($sessao_pedido,$tipo);
		if($validacao_desconto_maximo['erro']){
			//Exibir nome dos produtos cujo desconto foi alterado.
			//$erro = $validacao_desconto_maximo['mensagem'];	
			$erro = 'A classe do cliente foi alterada, os descontos foram removidos.';	
		}
		//Fim chamado 001809
	
	
        $sessao_pedido = $this->obter_sessao($tipo);
        //CUSTOM: Aristides - 21/06/2013         
        if ($this->session->flashdata('erro')) {
            $erro = $this->session->flashdata('erro');
            $erro = $dado['mensagem'];
        }
        //FIM CUSTOM: Aristides - 21/06/2013 
        //DEFININDO TABELA DE PREÇOS - INICIO			
        $_POST["codigo_tabela_precos"] = $this->regra_tabela_preco($sessao);
        //DEFININDO TABELA DE PREÇOS - FIM
	
		$dados_produto = $this->db_cliente->obter_produto($_POST["codigo_produto"], $_POST["codigo_tabela_precos"], $_POST["filial"]);
		
        if ($sessao_pedido['codigo_usuario'] && $sessao_pedido['codigo_loja_cliente']) {
            if ($_POST) {
                if ($this->input->post('adicionar_produto')) {
                    if (!$this->input->post('codigo_produto')) {
                        $erro = "Selecione um produto.";
                    } else if (!$this->input->post('quantidade')) {
                        $erro = "Insira uma quantidade.";
                    }
                    //CUSTOM: MPD-220_Walsywa_01_05082013 - 09/08/2013
                    //else if (floatval($this->input->post('preco_unico')) < floatval($this->input->post('hidden_preco'))) {
					else if (floatval($this->input->post('preco_unico')) < floatval($dados_produto['preco'])) {
                        //$erro = "O preço mínimo do item deve ser " . floatval($this->input->post('hidden_preco'));
                        $erro = "O preço mínimo do item deve ser " . $dados_produto['preco'];
						$_POST['preco_unico'] = $dados_produto['preco'];
	                }
                    //FIM CUSTOM: MPD-220_Walsywa_01_05082013 - 09/08/2013
                    else {
                        // Inserindo Quantidade e Produtos na sessao						
                        $dados_produto = $this->db_cliente->obter_produto($this->input->post('codigo_produto'), $this->input->post("codigo_tabela_precos"), $this->input->post("filial"));
						
                        $dados_produto['quantidade_pedido'] = $this->input->post('quantidade');
                        $dados_produto['preco'] = str_replace(",", ".", $this->input->post('preco_unico'));
                        // Buscando Estoque do Produto
                        $estoque_produto = $this->obter_estoque_disponivel($this->input->post('codigo_produto'), TRUE, $this->input->post('filial'));
                        $dados_produto['filial'] = $this->input->post('filial');

                        $total_dos_produtos_sem_ipi = $dados_produto['preco'] * $this->input->post('quantidade');
                        $total_dos_produtos_com_ipi = $total_dos_produtos_sem_ipi + ($total_dos_produtos_sem_ipi * ($dados_produto['ipi'] / 100));


                        if ($dados_produto['quantidade_pedido'] > $estoque_produto) {
                            // Pedidos Imediato Dividido pelo programado
                            if ($estoque_produto > 0) {
                                $quantidade_imediata = $estoque_produto;
                                $dados_produto['quantidade_pedido'] = $quantidade_imediata;
                                if ($sessao_pedido['produtos']) {
                                    foreach ($sessao_pedido['produtos'] as $indice => $produto) {
                                        if ($produto['filial'] == $this->input->post('filial') && trim($produto['codigo']) == trim($this->input->post('codigo_produto'))) {
                                            $dados_produto['quantidade_pedido'] = $dados_produto['quantidade_pedido'] + $produto['quantidade_pedido'];
                                            $sessao_pedido['produtos'][$indice] = $dados_produto;
                                            $this->inserir_pedido_temp(trim($this->input->post('codigo_produto')), $sessao_pedido['codigo_usuario'], $dados_produto['quantidade_pedido'], $this->input->post('filial'));
                                            $somar_produto = TRUE;
                                        }
                                    }
                                }

                                if (!$somar_produto) {
                                    $this->inserir_pedido_temp(trim($this->input->post('codigo_produto')), $sessao_pedido['codigo_usuario'], $dados_produto['quantidade_pedido'], $this->input->post('filial'));
                                    $sessao_pedido['produtos'][] = $dados_produto;
                                }
                            }


                            // Pedidos Programados
                            $quantidade_programada = $this->input->post('quantidade') - $estoque_produto;
                            $chegada = 0;
                            $previsaoChegada1 = $this->db_cliente->obterPrevisaoChegada($produto['filial'], $produto['codigo']);
                            $previsaoChegada = $previsaoChegada1[0];
                            if ($previsaoChegada && $chegada < $previsaoChegada->chegada) {
                                $dados_produto['previsao_chegada'] = date('d/m/Y', strtotime($previsaoChegada->chegada));
                            } else {
                                $dados_produto['previsao_chegada'] = 'Sem Previsao';
                            }
                            if ($estoque_produto < 0) {
                                $dados_produto['quantidade_pedido'] = $this->input->post('quantidade');
                            } else {
                                $dados_produto['quantidade_pedido'] = $quantidade_programada;
                            }
                            if ($sessao_pedido['produtos_programados']) {
                                foreach ($sessao_pedido['produtos_programados'] as $indice => $produto) {
                                    if ($produto['filial'] == $this->input->post('filial') && trim($produto['codigo']) == trim($this->input->post('codigo_produto'))) {
                                        $chegada = 0;
                                        $previsaoChegada1 = $this->db_cliente->obterPrevisaoChegada($produto['filial'], $produto['codigo']);
                                        $previsaoChegada = $previsaoChegada1[0];
                                        if ($previsaoChegada && $chegada < $previsaoChegada->chegada) {
                                            $dados_produto['previsao_chegada'] = date('d/m/Y', strtotime($previsaoChegada->chegada));
                                        } else {
                                            $dados_produto['previsao_chegada'] = 'Sem Previsao';
                                        }
                                        $dados_produto['quantidade_pedido'] = $dados_produto['quantidade_pedido'] + $produto['quantidade_pedido'];
                                        $sessao_pedido['produtos_programados'][$indice] = $dados_produto;
                                        $somar_produto_programado = TRUE;
                                    }
                                }
                            }
                            if (!$somar_produto_programado) {
                                $sessao_pedido['produtos_programados'][] = $dados_produto;
                            }
                        } else {
                            //Pedidos Imediatos
                            if ($sessao_pedido['produtos']) {
                                foreach ($sessao_pedido['produtos'] as $indice => $produto) {
                                    if (trim($produto['codigo']) == trim($this->input->post('codigo_produto'))) {
                                        //$produto['preco'] = $this->db_cliente->obter_preco_produto($produto['codigo']); 
                                        $dados_produto['quantidade_pedido'] = $this->input->post('quantidade') + $produto['quantidade_pedido'];
                                        //$dados_produto['preco'] = 22;
                                        $sessao_pedido['produtos'][$indice] = $dados_produto;
                                        $this->inserir_pedido_temp(trim($this->input->post('codigo_produto')), $sessao_pedido['codigo_usuario'], $dados_produto['quantidade_pedido'], $this->input->post('filial'));
                                        $somar_produto = TRUE;
                                    }
                                }
                            }
                            if (!$somar_produto) {
                                $this->inserir_pedido_temp(trim($this->input->post('codigo_produto')), $sessao_pedido['codigo_usuario'], $dados_produto['quantidade_pedido'], $this->input->post('filial'));
                                $sessao_pedido['produtos'][] = $dados_produto;
                            }
                        }
                        //Adicionando Produtos na Sessão
                        $dados_sessao = $sessao_pedido;
                        $this->atualizar_sessao($dados_sessao, $tipo);
                        //Limpando Campo do Produto Adicionado
                        $_POST['codigo_produto'] = "";
                        $_POST['_codigo_produto'] = "";
                        $_POST['quantidade'] = "";
                    }
                }
                if ($this->input->post('continuar')){	
					
					if (count($sessao_pedido['produtos']) > 0 || count($sessao_pedido['produtos_programados']) > 0) {
					/**												
						Para melhorar o desempenho do DW Portal do Representante. o cálculo de impostos irá ocorrer apenas uma vez.				
					*/
					//WebService							
							$cliente = $this->db_cliente->obter_cliente($sessao_pedido['cliente']['codigo'], $sessao_pedido['cliente']['loja']);	
							if(isset($sessao_pedido['produtos'])){
								foreach($sessao_pedido['produtos'] as &$produto){
									$array = $this->db_cliente->obter_impostos_item(
										$cliente['codigo'],
										$cliente['loja'],
										$cliente['tipo'], 
										$produto['quantidade_pedido'], 
										$produto['preco'], 
										$produto['codigo'], 
										$produto['filial']
									);
									
									$produto['st'] = $array['st'];
									$produto['tes'] = $array['tes'];
									$produto['cf'] = $array['cf'];
								}
							}
							
							if(isset($sessao_pedido['produtos_programados'])){
								foreach($sessao_pedido['produtos_programados'] as &$produto){
									$array = $this->db_cliente->obter_impostos_item(
										$cliente['codigo'],
										$cliente['loja'],
										$cliente['tipo'], 
										$produto['quantidade_pedido'], 
										$produto['preco'], 
										$produto['codigo'], 
										$produto['filial']
									);
									$produto['st'] = $array['st'];
									$produto['tes'] = $array['tes'];
									$produto['cf'] = $array['cf'];
								}
							}
							//$array = $this->db_cliente->calcular_st('010', $produto['codigo'], $cod_loj_cli[0], $cod_loj_cli[1], $total_dos_produtos_sem_ipi, $total_dos_produtos_com_ipi,$produto['filial']);
							
							
							// calcular st **	
						$this->atualizar_sessao($sessao_pedido, $tipo);
					
                        redirect('criar_pedidos/outras_informacoes/' . ($tipo == 'orcamento' ? $tipo : ''));
                    } else {
                        $erro = "Adicione um ou mais produtos no pedido.";
                    }
                }
            }
           
			$codigo_tabela_precos = $this->input->post("codigo_tabela_precos");


			$total_dos_produtos_sem_ipi = 0;
            $total_dos_produtos_com_ipi = 0;
            $valor_total_ipi = 0;
            $valor_total_sd = 0;
            $valor_total = 0;
			$ipi = 0;	
			
			
			
            if ($sessao_pedido['produtos']) {
                $produtos = array_reverse($sessao_pedido['produtos'], TRUE);
                $item = count($produtos);

               
                // Obtendo dados de cada produto    
                foreach ($produtos as $indice => $produto) {
                    $cod_loj_cli = explode('|', $sessao_pedido['codigo_loja_cliente']);
                    //Verifica se é uma cópia.
                    if ($copiar) {
                        //Busca o valor atual do produto para atualizar o preço, caso algum pedido antigo seja copiado.
                        $produtoValor = $this->db_cliente->obter_produto($produto['codigo'], $codigo_tabela_precos, $produto['filial']);
						
						$produto['preco'] = $produtoValor['preco'];
                    }
                    if ($produto['desconto'] > 0) {
                        $valor_unitario_produto_desconto = ($produto['preco'] - (($produto['preco'] * $produto['desconto']) / 100));
                    } else {
                        $valor_unitario_produto_desconto = $produto['preco'];
                    }

                    $total_dos_produtos_sem_ipi = $valor_unitario_produto_desconto * $produto['quantidade_pedido'];
                    $total_dos_produtos_com_ipi = $total_dos_produtos_sem_ipi + ($total_dos_produtos_sem_ipi * ($produto['ipi'] / 100));
                    $produto['total_sem_ipi'] = $total_dos_produtos_sem_ipi;
                    $produto['total_com_ipi'] = $total_dos_produtos_com_ipi;

                    //Adicionando produto na sessão.
                   
                    $item--;

                    // $total_produtos += $produto['preco'] * $produto['quantidade_pedido']; //Valor total dos produtos (imediato)
					$desconto_total += (($produto['preco'] - $valor_unitario_produto_desconto) * $produto['quantidade_pedido']); //Valor total de desconto (imediato)
                    $ipi += (($valor_unitario_produto_desconto * $produto['quantidade_pedido']) * ($produto['ipi'] / 100)); //Valor total de IPI (imediato)
                    $total_st += $valor_st; //Valor total de ST (imediato)
                    //$valor_total_sd += $total_dos_produtos_com_ipi + $total_st; //Valor total dos itens com ipi e desconto (imediato)
                    $valor_total_sd += $total_dos_produtos_com_ipi; //Valor total dos itens com ipi e desconto (imediato)
                    $valor_total += $produto['quantidade_pedido'] * ($produto['preco'] - (($produto['desconto'] / 100) * $produto['preco']));
                    // $valor_total_ipi += $produto['quantidade_pedido'] * (($produto['preco'] + (($produto['ipi'] / 100) * $produto['preco'])) - (($produto['desconto'] / 100) * $produto['preco'])) + $valor_st;
                    $valor_total_ipi += $produto['quantidade_pedido'] * (($produto['preco'] + (($produto['ipi'] / 100) * $produto['preco'])) - (($produto['desconto'] / 100) * $produto['preco']));

                    
                    $this->data['produtos'][$indice] = $produto;
                             
                }
				
            }
			
			$this->data['valor_total_sd'] = $valor_total_sd;
            $this->data['total_produtos'] = $valor_total;
			$this->data['desconto_total'] = $desconto_total;
			$this->data['ipi'] = $ipi;
			$this->data['total_st'] = $total_st;
			
            // Produtos Programado
            $valor_total_sd_programado = 0;
            //* Lista de Produtos		
            if ($sessao_pedido['produtos_programados']) {
                $produtos = array_reverse($sessao_pedido['produtos_programados'], TRUE);
                //die(var_dump($produtos));

                $item = count($produtos);
				
				$ipi_programado = 0;
				
                foreach ($produtos as $indice => $produto) {
                    if ($produto['desconto'] > 0) {
                        $valor_unitario_produto_desconto = ($produto['preco'] - (($produto['preco'] * $produto['desconto']) / 100));
                    } else {
                        $valor_unitario_produto_desconto = $produto['preco'];
                    }
                    // ** calcular st

                    $cod_loj_cli = explode('|', $sessao_pedido['codigo_loja_cliente']);

                    $total_dos_produtos_sem_ipi = $valor_unitario_produto_desconto * $produto['quantidade_pedido'];
                    $total_dos_produtos_com_ipi = $total_dos_produtos_sem_ipi + ($total_dos_produtos_sem_ipi * ($produto['ipi'] / 100));
					$produto['total_com_ipi'] = $total_dos_produtos_com_ipi;
                    $item--;

                    //mafag
                    $_descricao_produto = iconv('ISO-8859-1', 'UTF-8', $produto['descricao']);
                    $descricao_produto = convert_chars_to_entities($_descricao_produto);

                    $total_produtos_programado += $produto['preco'] * $produto['quantidade_pedido']; //Valor total dos produtos (programado)
                    $ipi_programado += (($valor_unitario_produto_desconto * $produto['quantidade_pedido']) * ($produto['ipi'] / 100)); //Valor total de IPI (programado)
                    $desconto_total_programado += (($produto['preco'] - $valor_unitario_produto_desconto) * $produto['quantidade_pedido']); //Valor total de desconto (programado)
                    $total_st_programado += $valor_st; //Valor total de ST(programado)
                    $valor_total_sd_programado += $total_dos_produtos_com_ipi + $total_st_programado; //Valor total dos itens com ipi e desconto (programado)

                    $valor_total_programado += $produto['quantidade_pedido'] * ($produto['preco'] - (($produto['desconto'] / 100) * $produto['preco']));
                    $valor_total_programado_ipi += ($produto['quantidade_pedido'] * (($produto['preco'] + (($produto['ipi'] / 100) * $produto['preco'])) - (($produto['desconto'] / 100) * $produto['preco']))) + $valor_st;
                
					$this->data['produtos_programados'][$indice] = $produto;
                  
				}
            }
			
			
			
			$this->data['valor_total_sd_programado'] = $valor_total_sd_programado;
            $this->data['total_produtos_programado'] = $valor_total_programado;	
			$this->data['desconto_total_programado'] = $desconto_total_programado;	
			$this->data['ipi_programado'] = $ipi_programado;
			$this->data['total_st_programado'] = $total_st_programado;
            // Inserindo Totais na Sessão
            $dados_sessao = $sessao_pedido;

			
			
            //$dados_sessao['valor_frete'] = $valor_frete;
            $dados_sessao['valor_total'] = $valor_total_programado;
            //$dados_sessao['peso_total_geral'] = $peso_total_geral;
            //$dados_sessao['valor_total_ipi'] = $valor_total_ipi;
            //$dados_sessao['valor_total_frete'] = $valor_total_frete;

            $this->atualizar_sessao($dados_sessao, $tipo);
            //--
            //*		
            $codigo_usuario = $sessao_pedido['codigo_usuario'];
            $codigo_loja_cliente = $sessao_pedido['codigo_loja_cliente'];
            $usuario = $this->db->from('usuarios')->where('codigo', $sessao_pedido['codigo_usuario'])->get()->row();
            

            
           
           

            

            $lista_produtos2 = $this->load->view('criar_pedidos/lista_produtos', $this->data, true);
           
           
            $this->load->view('layout', array('conteudo' => $this->load->view('criar_pedidos/adicionar_produtos', array
                    (
                    'filiais' => $this->db_cliente->obter_filiais(),
                    'erro' => $erro,
                    'tipo' => $tipo,
                    'codigo_usuario' => $codigo_usuario,
                    'codigo_tabela_precos' => $codigo_tabela_precos,
                    'codigo_loja_cliente' => $codigo_loja_cliente,
                    'tabelas_precos' => $this->_obter_tabelas_precos($codigo_usuario),
                    'lista_produtos' => $lista_produtos2,
                    'valor_frete' => $valor_frete,
                    'valor_total' => $valor_total,
                    'peso_total_geral' => $peso_total_geral,
                    'valor_total_ipi' => $valor_total_ipi,
                    'valor_total_frete' => $valor_total_frete,
                    'pedido' => $sessao_pedido,
                    'usuario' => $usuario,
                    'copiar' => $this->session->userdata('copiando'),
                    'imposto_calculado' => $imposto_calculado
                        ), TRUE
                )
                    )
            );
        } else {
            redirect('criar_pedidos/informacoes_cliente/' . ($tipo == TIPO_ORCAMENTO ? $tipo : ''));
        }
    }

    //CUSTOM: Aristides - 19/06/2013
    //   
    function validar_classes_desconto($classe = NULL) {
	
        $classes = $this->db_cliente->verificar_desconto_maximo($classe);
		
        //$desconto_cliente = $resultado[0]->ZY_DESCON1;                
        //$classes = $this->db->select()->from('descontos_classe')->where('codigo_classe', $classe)->get()->result();

        foreach ($classes as $classe) {
            $desconto_maximo = $classe->ZY_DESCON1;
        }
        $desconto_maximo = str_replace(',', '.', $desconto_maximo);

        return $desconto_maximo;
    }

    //Etapa 3
    function outras_informacoes($tipo = NULL) {
        $sessao_pedido = $this->obter_sessao($tipo);
		
        $classe_desconto = $sessao_pedido['cliente']['categoria'];
        //CUSTOM: Aristides - 21/06/2013            
        if ($this->session->flashdata('erro')) {
            $erro = $this->session->flashdata('erro');
        }

        $desconto_geral = str_replace(',', '.', $this->input->post('desconto_geral'));
        //$desconto_maximo = str_replace(',', '.', $desconto_maximo);
        $desconto_maximo = $this->validar_classes_desconto($classe_desconto);

        //CUSTOM: Aristides - 24/06/2013
        if (!$desconto_maximo) {
            $desconto_maximo = 0;
        }
        //FIM CUSTOM: Aristides - 24/06/2013
        if (!$sessao_pedido['produtos'] && !$sessao_pedido['produtos_programados']) {
            redirect('criar_pedidos/adicionar_produtos/' . ($tipo == 'orcamento' ? $tipo : ''));
        }
        if ($sessao_pedido['produtos_programados']) {
            $chegada = 0;
            $semPrevisao = false;
            foreach ($sessao_pedido['produtos_programados'] as $produtoProgramado) {
                $previsaoChegada1 = $this->db_cliente->obterPrevisaoChegada($produtoProgramado['filial'], $produtoProgramado['codigo']);
                $previsaoChegada = $previsaoChegada1[0];
                if ($previsaoChegada && $chegada < $previsaoChegada->chegada) {
                    $chegada = $previsaoChegada;
                } else {
                    $semPrevisao = true;
                }
            }
        }
        //print_r($sessao_pedido);
        if ($_POST) {
            if ($this->input->post('continuar')) {
                if ($this->input->post('data_entrega')) {
                    $dataEntregaArray = explode('/', $this->input->post('data_entrega'));
                    $dataEntrega = $dataEntregaArray[2] . $dataEntregaArray[1] . $dataEntregaArray[0];
                }
                if (!($this->input->post("codigo_forma_pagamento"))) {
                    $erro = "Selecione uma forma de pagamento.";
                } else if ($this->input->post('tipo_frete') == 'FOB' && !$this->input->post('codigo_transportadora')) {
                    $erro = 'Selecione uma transportadora.';
                } else if ($this->input->post('tipo_frete') == 'Redespacho' && !$this->input->post('codigo_transportadora')) {
                    $erro = 'Selecione uma transportadora.';
                } else if (!$this->input->post('data_entrega') && $sessao_pedido['produtos_programados']) {
                    $erro = 'Digite uma Data de entrega para o Pedido Programado.';
                } else if (floatval($desconto_geral) > floatval($desconto_maximo)) {
                    $erro = 'Desconto acima do limite para esse cliente. O desconto máximo permitido é de ' . str_replace('.', ',', $desconto_maximo) . '%.';
                } else {
                    $alerta = '';
                    if ($semPrevisao && $sessao_pedido['produtos_programados']) {
                        $alerta .= 'Atenção!\nExiste um ou mais produtos sem data de entrega prevista no pedido Programado!\n\n';
                    }
                    if ($dataEntrega < $chegada->chegada) {
                        $dataDeChegada = date('d/m/Y', strtotime($chegada->chegada));
                        $alerta .= 'Atenção!\nA privisão de entrada em estoque do produto \'' . $chegada->descricao . '\' e para o dia ' . $dataDeChegada . '.';
                    }
                    if ($alerta != '') {
                        $this->session->set_flashdata('semPrevisao', $alerta);
                    }

                    $dados_sessao = $sessao_pedido;

                    if (!$dados_sessao['desconto_geral']) {
                        $dados_sessao['desconto_geral'] = 0;
                    }

                    $dados_sessao['codigo_forma_pagamento'] = $this->input->post('codigo_forma_pagamento');
                    $dados_sessao['tipo_frete'] = $this->input->post('tipo_frete');
                    $dados_sessao['id_feira'] = $this->input->post('id_feira');
                    $dados_sessao['total_sd'] = $this->input->post('total_sd');
                    $dados_sessao['codigo_transportadora'] = $this->input->post('codigo_transportadora');
                    $dados_sessao['codigo_ordem_compra'] = $this->input->post('codigo_ordem_compra');
                    $dados_sessao['data_entrega'] = $this->input->post('data_entrega');
                    $dados_sessao['observacao_pedido_imediato'] = $this->input->post('observacao_pedido_imediato');
                    $dados_sessao['observacao_pedido_programado'] = $this->input->post('observacao_pedido_programado');


                    $desconto_geral = str_replace(',', '.', $this->input->post('desconto_geral'));

                    $dados_sessao['ver_desconto'] = json_decode($this->verificar_desconto($this->input->post('codigo_forma_pagamento'), $this->input->post('total_sd')));

                    if ($dados_sessao['cliente']['tabela_preco']) {
                        $dados_sessao['ver_desconto'] = $dados_sessao['cliente']['desconto'];
                    }

                    if ($dados_sessao['produtos']) {
                        foreach ($dados_sessao['produtos'] as $chProduto => $produto) {
                            if ($produto['desconto'] == 0 OR $dados_sessao['desconto_geral'] == $produto['desconto']) {
                                $dados_sessao['produtos'][$chProduto]['desconto'] = $desconto_geral;
                            }
                            if ($dados_sessao['produtos'][$chProduto]['desconto'] > $dados_sessao['ver_desconto']) {
                                $dados_sessao['produtos'][$chProduto]['alerta'] = 'S';
                            } else {
                                $dados_sessao['produtos'][$chProduto]['alerta'] = 'N';
                            }
                        }
                    }
                    if ($dados_sessao['produtos_programados']) {
                        foreach ($dados_sessao['produtos_programados'] as $chProduto_programado => $produto_programado) {
                            if ($produto_programado['desconto'] == 0 OR $dados_sessao['desconto_geral'] == $produto_programado['desconto']) {
                                $dados_sessao['produtos_programados'][$chProduto_programado]['desconto'] = $desconto_geral;
                            }
                            if ($dados_sessao['produtos_programados'][$chProduto_programado]['desconto'] > $dados_sessao['ver_desconto']) {
                                $dados_sessao['produtos_programados'][$chProduto_programado]['alerta'] = 'S';
                            } else {
                                $dados_sessao['produtos_programados'][$chProduto_programado]['alerta'] = 'N';
                            }
                        }
                    }
                    $dados_sessao['desconto_geral'] = $desconto_geral;

                    $this->atualizar_sessao($dados_sessao, $tipo);

                    redirect('criar_pedidos/confirmacao/' . ($tipo == 'orcamento' ? $tipo : ''));
                }
            }
        }
        // Obtendo Posts pela Sessao
        $_POST['codigo_forma_pagamento'] = ($sessao_pedido['codigo_forma_pagamento'] ? $sessao_pedido['codigo_forma_pagamento'] : $this->input->post('codigo_forma_pagamento'));
        $_POST['tipo_frete'] = ($sessao_pedido['tipo_frete'] ? $sessao_pedido['tipo_frete'] : $this->input->post('tipo_frete'));
        $_POST['id_feira'] = ($sessao_pedido['id_feira'] ? $sessao_pedido['id_feira'] : $this->input->post('id_feira'));
        $_POST['desconto_geral'] = ($sessao_pedido['desconto_geral'] ? $sessao_pedido['desconto_geral'] : $this->input->post('desconto_geral'));
        $_POST['codigo_transportadora'] = ($sessao_pedido['codigo_transportadora'] ? $sessao_pedido['codigo_transportadora'] : $this->input->post('codigo_transportadora'));
        $_POST['codigo_ordem_compra'] = ($sessao_pedido['codigo_ordem_compra'] ? $sessao_pedido['codigo_ordem_compra'] : $this->input->post('codigo_ordem_compra'));
        $_POST['data_entrega'] = ($sessao_pedido['data_entrega'] == '31/12/1969' ? '' : ($sessao_pedido['data_entrega'] ? $sessao_pedido['data_entrega'] : $this->input->post('data_entrega')));
        $_POST['observacao_pedido_imediato'] = ($sessao_pedido['observacao_pedido_imediato'] ? $sessao_pedido['observacao_pedido_imediato'] : $this->input->post('observacao_pedido_imediato'));
        $_POST['observacao_pedido_programado'] = ($sessao_pedido['observacao_pedido_programado'] ? $sessao_pedido['observacao_pedido_programado'] : $this->input->post('observacao_pedido_programado'));

        $usuario = $this->db->from('usuarios')->where('codigo', $sessao_pedido['codigo_usuario'])->get()->row();

        $this->load->view('layout', array('conteudo' => $this->load->view('criar_pedidos/outras_informacoes', array
                (
                'erro' => $erro,
                'obter_formas_pagamento' => $this->_obter_formas_pagamento(), //$pagamento,//
                'obter_transportadoras' => $this->_obter_transportadoras(),
                'eventos' => hnordt_gerar_feiras_ativas(), // Evento,
                'pedido' => $sessao_pedido,
                'tipo' => $tipo,
                'classes' => $classes,
                'usuario' => $usuario,
                'copiar' => $this->session->userdata('copiando')
                    ), TRUE
            )
                )
        );
    }

    // Etapa 4
    function confirmacao($tipo = NULL) {

        $pedido = $this->obter_sessao($tipo);

        //CUSTOM: MPD-220 - 07/08/2013
        $categoria_cliente = $pedido['cliente']['categoria'];
	
        if (($categoria_cliente == "B" OR $categoria_cliente == "C") && $tipo != "orcamento") {
            $pedido['liberado'] = "L";
        } else {
            unset($pedido['liberado']);
        }
        $this->atualizar_sessao($pedido, $tipo);
        $pedido = $this->obter_sessao($tipo);
        if (!$pedido['codigo_forma_pagamento'] || !$pedido['tipo_frete']) {
            redirect('criar_pedidos/outras_informacoes/' . ($tipo == TIPO_ORCAMENTO ? $tipo : ''));
        }
        if ($this->input->post('confirmar')) {
            // Se existir id_pedido editar, se não inserir
            if ($pedido['id_pedido']) {
                //$id_pedido = $this->db_cliente->editar_pedido($pedido);
                if ($pedido['produtos']) {
                    $id_pedido = $this->db_cliente->editar_pedido($pedido);
                }

                if ($pedido['produtos_programados']) {
                    $id_pedido_programado = $this->db_cliente->editar_pedido($pedido, TRUE);
                }

                if ($id_pedido) {
                    if ($pedido['codigo_usuario']) {
                        $this->db->delete('pedidos', array('id_usuario' => $pedido['codigo_usuario']));
                    }
                    $sessao_pedido = $this->obter_sessao($tipo);
                    $this->deletar_sessao($tipo);
                }


                if (($pedido['produtos'] && $id_pedido) && !($pedido['produtos_programados'] && $id_pedido_programado)) {
                    redirect('pedidos/ver_detalhes/' . (($tipo == 'orcamento') ? 'orcamento' : 'pedido') . '/' . $id_pedido);
                } else if (($pedido['produtos_programados'] && $id_pedido_programado) && !($pedido['produtos'] && $id_pedido)) {
                    redirect('pedidos/ver_detalhes/' . (($tipo == 'orcamento') ? 'orcamento' : 'pedido') . '/' . $id_pedido_programado);
                } else {
                    if (($pedido['produtos'] && $id_pedido) && ($pedido['produtos_programados'] && $id_pedido_programado)) {
                        redirect('pedidos/ver_detalhes/' . (($tipo == 'orcamento') ? 'orcamento' : 'pedido') . '/' . $id_pedido . '/0/0/' . $id_pedido_programado . '/2');
                    } else {
                        redirect('pedidos/aguardando_analise/' . ($tipo == TIPO_ORCAMENTO ? $tipo : ''));
                    }
                }
                if (($pedido['produtos_programados'] && $id_pedido_programado) && !($pedido['produtos'] && $id_pedido)) {
                    redirect('pedidos/ver_detalhes/' . (($tipo == 'orcamento') ? 'orcamento' : 'pedido') . '/' . $id_pedido_programado);
                }
            } else {

                if ($pedido['produtos']) {
                    $id_pedido = $this->db_cliente->exportar_pedido($pedido);
                }

                if ($pedido['produtos_programados']) {
                    $id_pedido_programado = $this->db_cliente->exportar_pedido($pedido, TRUE);
                }

                //if($id_pedido)
                //{
                //Deleta registro em tabela temporária utilizada para obtenção do estoque real, contando inclusões simultâneas.
                if ($pedido['codigo_usuario']) {
                    $this->db->delete('pedidos', array('id_usuario' => $pedido['codigo_usuario']));
                }

                $sessao_pedido = $this->obter_sessao($tipo);
                $this->deletar_sessao($tipo);

                if (($pedido['produtos'] && $id_pedido) && !($pedido['produtos_programados'] && $id_pedido_programado)) {
                    redirect('pedidos/ver_detalhes/' . (($tipo == 'orcamento') ? 'orcamento' : 'pedido') . '/' . $id_pedido);
                } else if (($pedido['produtos_programados'] && $id_pedido_programado) && !($pedido['produtos'] && $id_pedido)) {
                    redirect('pedidos/ver_detalhes/' . (($tipo == 'orcamento') ? 'orcamento' : 'pedido') . '/' . $id_pedido_programado);
                } else {
                    if (($pedido['produtos'] && $id_pedido) && ($pedido['produtos_programados'] && $id_pedido_programado)) {
                        redirect('pedidos/ver_detalhes/' . (($tipo == 'orcamento') ? 'orcamento' : 'pedido') . '/' . $id_pedido . '/0/0/' . $id_pedido_programado . '/2');
                    } else {
                        redirect('pedidos/aguardando_analise/' . ($tipo == TIPO_ORCAMENTO ? $tipo : ''));
                    }
                }
                //}
            }
        }
        if ($this->session->flashdata('semPrevisao')) {
            echo '<script type="text/javascript">alert("' . $this->session->flashdata('semPrevisao') . '");</script>';
        }

        $produtos = $pedido['produtos'];
        $usuario = $this->db->from('usuarios')->where('codigo', $pedido['codigo_usuario'])->get()->row();

        $formas_pagamento = $this->db_cliente->obter_formas_pagamento($pedido['codigo_forma_pagamento']);

        $evento = $this->db->from('feiras')->where('id', $pedido['id_feira'])->get()->row();
        $transportadora = $this->db_cliente->obter_transportadoras($pedido['codigo_transportadora']);

        $copiar = $this->session->userdata('copiando');

        $this->session->set_userdata('copiando', 0);

        $this->load->view('layout', array('conteudo' => $this->load->view('criar_pedidos/confirmacao', array
                (
                'pedido' => $pedido,
                'produto' => $produto,
                'usuario' => $usuario,
                'formas_pagamento' => $formas_pagamento[0]['descricao'],
                'evento' => $evento,
                'tipo' => $tipo,
                'transportadora' => $transportadora[0]['nome'],
                'produtos' => $produtos,
                'copiar' => $copiar,
				'mensagem' => $mensagem
                    ), TRUE
            )
                )
        );
    }

    //----------------------------------------------------------------------------------------------------------------------------------


    function excluir_produto($tipo = NULL) {

        $indice = $_POST['indice'];
        $programado = $_POST['programado'];

        if ($indice != '') {
            $sessao_pedido = $this->obter_sessao($tipo);
            //$produtos = $sessao_pedido['produtos'];


            if ($programado) {
                $produtos = array_reverse($sessao_pedido['produtos_programados'], TRUE);

                unset($produtos[$indice]);

                $sessao_pedido['produtos_programados'] = $produtos;
            } else {
                $produtos = array_reverse($sessao_pedido['produtos'], TRUE);

                unset($produtos[$indice]);

                $this->db->delete('pedidos', array('codigo_produto' => $produtos[$indice]['codigo'], 'filial' => $produtos[$indice]['filial'], 'id_usuario' => $this->session->userdata('id_usuario')));

                $sessao_pedido['produtos'] = $produtos;
            }

            $dados_sessao = $sessao_pedido;

            $this->atualizar_sessao($dados_sessao, $tipo);

            echo '1';
        } else {
            echo '0';
        }
    }

    function alterar_quantidade($tipo = NULL) {
        $indice = $_POST['indice'];
        $quantidade = $_POST['quantidade'];
        $programado = $_POST['programado'];

        if (trim($quantidade) == '0' || trim($quantidade) == 0) {
            echo '0';
        } else {
            if ($indice != '' && $quantidade != '') {

                if ($programado) {
                    $sessao_pedido = $this->obter_sessao($tipo);
                    $produtos = $sessao_pedido['produtos_programados'];

                    $produtos[$indice]['quantidade_pedido'] = $quantidade;

                    $sessao_pedido['produtos_programados'] = $produtos;

                    $dados_sessao = $sessao_pedido;

                    $this->atualizar_sessao($dados_sessao, $tipo);

                    echo '1';
                } else {

                    $sessao_pedido = $this->obter_sessao($tipo);
                    $produtos = $sessao_pedido['produtos'];

                    $estoque = $this->obter_estoque_disponivel($produtos[$indice]['codigo'], FALSE, $produtos[$indice]['filial']);

                    if ($quantidade > $estoque) {
                        echo '2|' . $estoque;
                    } else {

                        $produtos[$indice]['quantidade_pedido'] = $quantidade;

                        $sessao_pedido['produtos'] = $produtos;

                        $this->inserir_pedido_temp($produtos[$indice]['codigo'], $sessao_pedido['codigo_usuario'], $quantidade, $produtos[$indice]['filial']);

                        $dados_sessao = $sessao_pedido;

                        $this->atualizar_sessao($dados_sessao, $tipo);

                        echo '1';
                    }
                }
            } else {
                echo '0';
            }
        }
    }

    function alterar_desconto($tipo = NULL) {
        $indice = $_POST['indice'];
        $desconto = $_POST['desconto'];
        $programado = $_POST['programado'];

        $sessao_pedido = $this->obter_sessao($tipo);
        $classe_desconto = $sessao_pedido['cliente']['categoria'];
        $desconto_maximo = $this->validar_classes_desconto($classe_desconto);

        if (floatval($desconto) > floatval($desconto_maximo)) {
            echo '2';
        } else if (trim($desconto) > '100' || trim($desconto) > 100) {
            echo '1';
        } else {
            if ($indice != '' && $desconto != '') {

                if ($programado) {

                    $produtos = $sessao_pedido['produtos_programados'];

                    $produtos[$indice]['desconto'] = $desconto;

                    $sessao_pedido['produtos_programados'] = $produtos;

                    $dados_sessao = $sessao_pedido;

                    $this->atualizar_sessao($dados_sessao, $tipo);

                    echo '0';
                } else {

                    $produtos = $sessao_pedido['produtos'];

                    $produtos[$indice]['desconto'] = $desconto;

                    $sessao_pedido['produtos'] = $produtos;

                    $this->inserir_pedido_temp($produtos[$indice]['codigo'], $sessao_pedido['codigo_usuario'], $desconto, $produtos[$indice]['filial']);

                    $dados_sessao = $sessao_pedido;

                    $this->atualizar_sessao($dados_sessao, $tipo);

                    echo '0';
                }
            } else {
                echo '1';
            }
        }
    }

    function transferir_quantidade_programada($tipo = NULL) {

        $indice = $_POST['indice'];
        $indice_produto_programado = $indice;
        $quantidade = $_POST['valor'];

        if (($indice != '') && ($quantidade != '')) {
            $sessao_pedido = $this->obter_sessao($tipo);
            $produtos = $sessao_pedido['produtos_programados'];
            $estoque = $this->obter_estoque_disponivel($produtos[$indice]['codigo'], FALSE, $produtos[$indice]['filial']);

            //Buscando quantidade Imediata

            if (is_array($sessao_pedido['produtos'])) {
                foreach ($sessao_pedido['produtos'] as $indice => $produto_sessao) {
                    if ($produtos[$indice_produto_programado]['codigo'] == $produto_sessao['codigo'] && $produtos[$indice_produto_programado]['filial'] == $produto_sessao['filial']) {
                        $quantidade_imediata = $produto_sessao['quantidade_pedido'];
                    }
                }
            }

            if ($estoque > $quantidade_imediata) {
                $estoque = $estoque - $quantidade_imediata;
            }


            if ($quantidade > $estoque) {
                if ($estoque < 0) {
                    $estoque = 0;
                }

                echo '2|' . $estoque;
            } else {
                $produtos[$indice_produto_programado]['quantidade_pedido'] = $produtos[$indice_produto_programado]['quantidade_pedido'] - $quantidade;

                if ($quantidade_imediata) {
                    $quantidade_imediata = $quantidade + $estoque;
                } else {
                    $quantidade_imediata = $quantidade;
                }

                //Produto Programado

                $sessao_pedido['produtos_programados'] = $produtos;


                //Produto Imediato
                if ($sessao_pedido['produtos']) {
                    $criar_imediato = TRUE;

                    foreach ($sessao_pedido['produtos'] as $indice => $produto_sessao) {
                        if ($produtos[$indice_produto_programado]['codigo'] == $produto_sessao['codigo'] && $produtos[$indice_produto_programado]['filial'] == $produto_sessao['filial']) {
                            $produtos[$indice_produto_programado]['quantidade_pedido'] = $quantidade + $produto_sessao['quantidade_pedido'];
                            $sessao_pedido['produtos'][$indice] = $produtos[$indice_produto_programado];

                            $criar_imediato = FALSE;
                        }
                    }

                    if ($criar_imediato) {
                        $produtos[$indice_produto_programado]['quantidade_pedido'] = $quantidade_imediata;
                        $sessao_pedido['produtos'][] = $produtos[$indice_produto_programado];
                    }
                } else {
                    $produtos[$indice_produto_programado]['quantidade_pedido'] = $quantidade_imediata;
                    $sessao_pedido['produtos'][] = $produtos[$indice_produto_programado];
                }


                $this->inserir_pedido_temp($produtos[$indice_produto_programado]['codigo'], $sessao_pedido['codigo_usuario'], $quantidade_imediata, $produtos[$indice_produto_programado]['filial']);

                if ($sessao_pedido['produtos_programados'][$indice_produto_programado]['quantidade_pedido'] == 0) {
                    unset($sessao_pedido['produtos_programados'][$indice_produto_programado]);
                }
                $dados_sessao = $sessao_pedido;

                $this->atualizar_sessao($dados_sessao, $tipo);

                echo '1';
            }
        } else {
            echo '0';
        }
    }

    function transferir_quantidade($tipo = NULL) {

        $indice = $_POST['indice'];
        $indice_produto_imediato = $indice;
        $quantidade = $_POST['valor'];

        if (($indice != '') && ($quantidade != '')) {

            $sessao_pedido = $this->obter_sessao($tipo);
            $produtos = $sessao_pedido['produtos'];


            if ($quantidade > $produtos[$indice]['quantidade_pedido']) {
                echo '2|' . $produtos[$indice]['quantidade_pedido'];
            } else {
                $produtos[$indice]['quantidade_pedido'] = $produtos[$indice]['quantidade_pedido'] - $quantidade;

                //Produto Imediato
                $sessao_pedido['produtos'] = $produtos;

                //Produto Programado
                $produto_programado = $produtos[$indice];
                if ($sessao_pedido['produtos_programados']) {
                    $criar_programado = TRUE;

                    foreach ($sessao_pedido['produtos_programados'] as $indice => $produto_sessao) {
                        if ($produto_programado['codigo'] == $produto_sessao['codigo'] && $produto_programado['filial'] == $produto_sessao['filial']) {
                            $chegada = 0;
                            $previsaoChegada1 = $this->db_cliente->obterPrevisaoChegada($produto_programado['filial'], $produto_programado['codigo']);
                            $previsaoChegada = $previsaoChegada1[0];
                            if ($previsaoChegada->chegada AND $chegada < $previsaoChegada->chegada) {
                                $produto_programado['previsao_chegada'] = date('d/m/Y', strtotime($previsaoChegada->chegada));
                            } else {
                                $produto_programado['previsao_chegada'] = 'Sem Previsao';
                            }
                            $produto_programado['quantidade_pedido'] = $quantidade + $produto_sessao['quantidade_pedido'];
                            $sessao_pedido['produtos_programados'][$indice] = $produto_programado;

                            $criar_programado = FALSE;
                        }
                    }

                    if ($criar_programado) {

                        $chegada = 0;
                        $previsaoChegada1 = $this->db_cliente->obterPrevisaoChegada($produto_programado['filial'], $produto_programado['codigo']);
                        $previsaoChegada = $previsaoChegada1[0];
                        if ($previsaoChegada->chegada AND $chegada < $previsaoChegada->chegada) {
                            $produto_programado['previsao_chegada'] = date('d/m/Y', strtotime($previsaoChegada->chegada));
                        } else {
                            $produto_programado['previsao_chegada'] = 'Sem Previsao';
                        }

                        $produto_programado['quantidade_pedido'] = $quantidade;

                        $sessao_pedido['produtos_programados'][] = $produto_programado;
                    }
                } else {
                    $chegada = 0;
                    $previsaoChegada1 = $this->db_cliente->obterPrevisaoChegada($produto_programado['filial'], $produto_programado['codigo']);
                    $previsaoChegada = $previsaoChegada1[0];
                    if ($previsaoChegada->chegada AND $chegada < $previsaoChegada->chegada) {
                        $produto_programado['previsao_chegada'] = date('d/m/Y', strtotime($previsaoChegada->chegada));
                    } else {
                        $produto_programado['previsao_chegada'] = 'Sem Previsao';
                    }

                    $produto_programado['quantidade_pedido'] = $quantidade;
                    $sessao_pedido['produtos_programados'][] = $produto_programado;
                }

                $this->inserir_pedido_temp($sessao_pedido['produtos'][$indice_produto_imediato]['codigo'], $sessao_pedido['codigo_usuario'], $produtos[$indice_produto_imediato]['quantidade_pedido'], $produtos[$indice_produto_imediato]['filial']);


                if ($sessao_pedido['produtos'][$indice_produto_imediato]['quantidade_pedido'] == 0) {
                    unset($sessao_pedido['produtos'][$indice_produto_imediato]);
                }

                $dados_sessao = $sessao_pedido;

                $this->atualizar_sessao($dados_sessao, $tipo);


                echo '1';
            }
        } else {
            echo '0';
        }
    }

    function alterar_frete() {
        $frete = $_POST['frete'];


        $sessao_pedido = $this->session->userdata('sessao_pedido');
        $valor_frete = $sessao_pedido['valor_frete'];

        $sessao_pedido['valor_frete'] = $frete;
        $sessao_pedido['valor_total_frete'] = $sessao_pedido['valor_frete'] + $sessao_pedido['valor_total_ipi'];

        $dados_sessao = $sessao_pedido;

        $this->session->set_userdata('sessao_pedido', $dados_sessao);

        echo '1';
    }

    function obter_quantidade_pedido_temp($codigo_produto, $filial) {
        $pedido = $this->db->select('quantidade')->from('pedidos')->where(array('codigo_produto' => $codigo_produto, 'filial' => $filial))->get()->row();

        return $pedido->quantidade;
    }

    function sessao() {
        echo '<pre>';
        print_r($_SESSION);
        echo '</pre>';
    }

    function inserir_pedido_temp($codigo_produto, $codigo_usuario, $quantidade, $filial) {

        $pedido = $this->db->from('pedidos')->where(array('codigo_produto' => $codigo_produto, 'id_usuario' => $codigo_usuario, 'filial' => $filial))->get()->row();


        $data = array(
            'timestamp' => time(),
            'codigo_produto' => $codigo_produto,
            'id_usuario' => $codigo_usuario,
            'quantidade' => $quantidade,
            'filial' => $filial
        );

        if ($pedido) {
            $this->db->where('codigo_produto', $codigo_produto);
            $this->db->update('pedidos', $data);
        } else {
            $this->db->insert('pedidos', $data);
        }
    }

    function obter_clientes($codigo_do_representante = NULL) {
        $palavras_chave = addslashes($_GET['term']);

        $dados = array();

        $clientes = $this->autocomplete->obter_clientes($palavras_chave, $codigo_do_representante);

        foreach ($clientes as $cliente) {
            $dados[] = array('label' => trim($cliente['codigo']) . ' - ' . trim(exibir_texto($cliente['nome'])) . ' - ' . trim($cliente['cpf']), 'value' => trim($cliente['codigo']) . '|' . $cliente['loja']);
        }

        echo json_encode($dados);
    }

    function obter_prospects($codigo_do_representante = NULL) {
        $palavras_chave = addslashes($_GET['term']);

        $dados = array();

        $prospects = $this->autocomplete->obter_prospects($palavras_chave, $codigo_do_representante);

        foreach ($prospects as $prospect) {

            if (!$prospect['nome']) {
                $prospect['nome'] = $prospect['nome_fantasia'];
            }

            $dados[] = array('label' => $prospect['nome'] . ' - ' . $prospect['cpf'], 'value' => $prospect['id']);
        }


        echo json_encode($dados);
    }

    function obter_produtos($codigo_tabela_precos = NULL) {

        $palavras_chave = addslashes($_GET['term']);

        $dados = array();

        $produtos = array();

        //$produtos = $this->autocomplete->obter_produtos($palavras_chave);
        $produtos = $this->autocomplete->obter_produtos_tabela_preco($palavras_chave, NULL, $codigo_tabela_precos);
		
        // var_dump($produtos);
        //die();
        if ($produtos) {
            foreach ($produtos as $produto) {
                //$dados[] = array('label' => trim($produto['codigo']) . ' - ' . trim($produto['descricao']) . ' - Estoque atual: ' .  number_format(($produto['estoque_atual'] > 0 ? $produto['estoque_atual'] : 0), 2, ',', '.'), 'value' => $produto['codigo']);
                $_descricao_produto = iconv('ISO-8859-1', 'UTF-8', $produto['descricao']);
                $descricao_produto = convert_chars_to_entities($_descricao_produto);
                //echo $descricao_produto;
                $dados[] = array('label' => trim($produto['codigo']) . ' - ' . trim($descricao_produto) /* utf8_decode(trim($produto['descricao'])) . ' - Estoque atual: ' .  ($produto['estoque_atual'] > 0 ? $produto['estoque_atual'] : 0) */, 'value' => $produto['codigo']);
            }
        }

        echo json_encode($dados);
    }

    function _obter_tabelas_precos($codigo_usuario) {
        $tabelas_precos = $this->db_cliente->obter_tabelas_precos();
        $_tabelas_precos = array();

        $_tabelas_precos[] = "Selecione";

        $tabela_precos_usuario = $this->db->from('usuarios')->where('codigo', $codigo_usuario)->get()->row()->tabela_precos;
        $_tabela_precos_usuario = unserialize($tabela_precos_usuario);

        if ($_tabela_precos_usuario) {
            foreach ($tabelas_precos as $tabela_precos) {

                foreach ($_tabela_precos_usuario as $codigo_tabela_precos) {
                    if ($codigo_tabela_precos == $tabela_precos['codigo']) {
                        $_tabelas_precos[$tabela_precos['codigo']] = $tabela_precos['descricao'];
                    }
                }
            }
        }

        return $_tabelas_precos;
    }

    function _obter_formas_pagamento() {
        $formas_pagamento = $this->db_cliente->obter_formas_pagamento();
        $_formas_pagamento = array();

        foreach ($formas_pagamento as $forma_pagamento) {
            $_formas_pagamento[$forma_pagamento['codigo']] = utf8_encode($forma_pagamento['descricao']);
        }

        return $_formas_pagamento;
    }

    function _obter_transportadoras() {
        $transportadoras = $this->db_cliente->obter_transportadoras();
        $_transportadoras = array();

        foreach ($transportadoras as $transportadora) {
            $_transportadoras[$transportadora['codigo']] = $transportadora['codigo'] . ' - ' . utf8_encode($transportadora['nome']);
        }

        return $_transportadoras;
    }

    function obter_valor_frete($total_peso, $cliente, $teste_tabela_preco = FALSE) {

        $cliente = explode('|', $cliente);

        if (!$teste_tabela_preco) {
            $tabela_preco = $this->db_cliente->obter_tabela_preco($cliente[0], $cliente[1]);
        } else {
            $tabela_preco = 'a';
        }

        if ($tabela_preco['tabela_frete'] == 'N') {
            $valor_frete = 0;
        } else {

            //Valor do Frete
            if ($total_peso <= 600) {
                $frete = $this->db->select(strtolower($tabela_preco['tabela_frete']))->from('tabela_frete')->where(array('kg' => 1))->get()->row_array();

                $valor_frete = $frete[strtolower($tabela_preco['tabela_frete'])];
            } else if ($total_peso >= 601 && $total_peso <= 999) {
                $frete = $this->db->select(strtolower($tabela_preco['tabela_frete']))->from('tabela_frete')->where(array('kg' => 601))->get()->row_array();

                if ($tabela_preco['tabela_frete'] == 'I') {
                    $valor_frete = $frete[strtolower($tabela_preco['tabela_frete'])];
                } else {
                    $valor_frete = $total_peso * $frete[strtolower($tabela_preco['tabela_frete'])];
                }
            } else if ($total_peso >= 1000 && $total_peso <= 1999) {
                $frete = $this->db->select(strtolower($tabela_preco['tabela_frete']))->from('tabela_frete')->where(array('kg' => 1000))->get()->row_array();

                $valor_frete = $total_peso * $frete[strtolower($tabela_preco['tabela_frete'])];
            } else if ($total_peso >= 2000 && $total_peso <= 2999) {
                $frete = $this->db->select(strtolower($tabela_preco['tabela_frete']))->from('tabela_frete')->where(array('kg' => 2000))->get()->row_array();

                $valor_frete = $total_peso * $frete[strtolower($tabela_preco['tabela_frete'])];
            } else if ($total_peso >= 3000 && $total_peso <= 9999) {
                $frete = $this->db->select(strtolower($tabela_preco['tabela_frete']))->from('tabela_frete')->where(array('kg' => 3000))->get()->row_array();

                $valor_frete = $total_peso * $frete[strtolower($tabela_preco['tabela_frete'])];
            } else if ($total_peso >= 10000) {
                $frete = $this->db->select(strtolower($tabela_preco['tabela_frete']))->from('tabela_frete')->where(array('kg' => 10000))->get()->row_array();

                $valor_frete = $total_peso * $frete[strtolower($tabela_preco['tabela_frete'])];
            }
        }

        return $valor_frete;
    }

    function cancelar_pedido($tipo = TIPO_PEDIDO) {
        $sessao_pedido = $this->obter_sessao($tipo);
        if ($sessao_pedido['codigo_usuario']) {
            $this->db->delete('pedidos', array('id_usuario' => $sessao_pedido['codigo_usuario']));
        }
        $this->deletar_sessao($tipo);
        $this->session->set_userdata('imposto_calculado', 0);
        redirect('criar_pedidos/informacoes_cliente/' . $tipo);
    }

	
	
	
	
    function obter_informacoes_cliente_ajax($codigo_loja_cliente = NULL) {
	
        $dados = array(
            'cliente_encontrado' => FALSE,
            'html' => NULL
        );

        $codigo_loja_cliente = explode('_', $codigo_loja_cliente);
        $codigo_cliente = $codigo_loja_cliente[0];
        $loja_cliente = $codigo_loja_cliente[1];

        if ($codigo_cliente && $loja_cliente) {
            $cliente = $this->db_cliente->obter_cliente($codigo_cliente, $loja_cliente);
        }
		
        $inatividade = array(
            '0' => 'Não Informado',
            '1' => 'Alt CNPJ',
            '2' => 'Cliente s/ Crédito',
            '3' => 'Fech. Oper.',
            '4' => 'Insatisfação Cliente',
            '5' => 'Mud. Atividade',
            '6' => 'Inativo Compra',
            '7' => 'Operação Inativa',
            '8' => 'Mercad',
        );

        if ($cliente) {
            $dados['cliente_encontrado'] = TRUE;

            $dados['html'] = '<p><strong>' . $cliente['nome'] . '</strong> - ' . $cliente['cpf'] . ' - <strong>' . $cliente['status'] . '</strong>  ';

            $classes = $this->db->select()->from('descontos_classe')->get()->result();
            foreach ($classes as $classe) {
                $listagem[$classe->codigo_classe] = $classe->nome_classe;
            }

            $inat = $inatividade[0];
            if ($cliente['status'] == 'Inativo') {
                foreach ($inatividade as $chaveInat => $valorInat) {
                    if ($chaveInat == $cliente['motivo_inativo']) {
                        $inat = $valorInat;
                    }
                }
            }
			$cliente['pode_visitar'] = '1';
			
			if($this->session->userdata('grupo_usuario') == 'representantes') {
				if (trim($this->session->userdata('codigo_usuario')) != '') {
					$podeVisitar = $this->db_cliente->verificar_pode_visitar($this->session->userdata('codigo_usuario'), $cliente['codigo']);
					if ($podeVisitar) {
						if ($podeVisitar->OKFORM == 'N') {
							$cliente['pode_visitar'] = '2';
						}
					}
				}
			}
			
            $dados['html'] .= $inat;

            $dados['html'] .= '</p>
				<ul style="float: left; margin-right: 10px;">
				
					<li><strong>Contato:</strong> ' . $cliente['pessoa_contato'] . '</li>
					<li><strong>Limite de crédito:</strong> R$ ' . number_format($cliente['limite_credito'], 2, ',', '.') . '</li>
					<li><strong>Títulos em aberto:</strong> R$ ' . number_format($cliente['total_titulos_em_aberto'], 2, ',', '.') . '</li>
					<li><strong>Títulos vencidos:</strong> R$ ' . number_format($cliente['total_titulos_vencidos'], 2, ',', '.') . '</li>
				</ul>
				
				<ul style="float: left; margin-right: 10px;">
					<li><strong>Telefone:</strong> ' . $cliente['telefone'] . '</li>
					<li><strong>Endereço:</strong> ' . $cliente['endereco'] . '</li>
					<li><strong>Bairro:</strong> ' . $cliente['bairro'] . '</li>
					<li><strong>CEP:</strong> ' . $cliente['cep'] . '</li>
				</ul>
				
				<ul style="float: left; margin-right: 10px;">
					<li><strong>Cidade:</strong> ' . $cliente['cidade'] . '</li>
					<li><strong>Estado:</strong> ' . $cliente['estado'] . '</li>
					<li><strong>Tabela de preços:</strong> ' . $this->db_cliente->obter_tabela_preco_descricao($cliente['tabela_precos']) . '</li>
					<li><strong>Classe:</strong> ' . ($cliente['categoria'] ? element($cliente['categoria'], $listagem) : 'Sem Classe') . '</li>
				</ul>
				
				<div style="clear: both;"></div>
			';
        }
				
		$dados['cliente'] = $cliente;		
		
        echo json_encode($dados);
    }

    function teste() {
        echo $this->obter_estoque_disponivel('460155', TRUE, '01');
    }

    function obter_estoque_disponivel($codigo_produto, $ativar_quantidade_temp = TRUE, $filial) {
        $estoque_produto = $this->db_cliente->obter_quantidade_disponivel_estoque_produto($codigo_produto, $filial);

        if ($ativar_quantidade_temp) {
            $quantidade_pedido_temp = $this->obter_quantidade_pedido_temp($codigo_produto, $filial);
        }


        //Subtraindo Estoque
        if ($sessao_pedido['produtos']) {

            foreach ($sessao_pedido['produtos'] as $indice => $produto) {

                if (trim($produto['codigo']) == trim($codigo_produto)) {

                    $estoque_produto = $estoque_produto - $produto['quantidade_pedido'] - $quantidade_pedido_temp;
                }
            }
        } else {
            $estoque_produto = $estoque_produto - $quantidade_pedido_temp;
        }

        return $estoque_produto;
    }

    function transferir_tudo($tipo = NULL) {
        $valor = $_POST['valor'];
        if ($valor) {
            $sessao_pedido = $this->obter_sessao($tipo);

            if ($sessao_pedido['produtos']) {
                $produtos_imediados = $sessao_pedido['produtos'];
                unset($sessao_pedido['produtos']);

                if ($sessao_pedido['produtos_programados']) {
                    $produtos_programados = $sessao_pedido['produtos_programados'];
                    unset($sessao_pedido['produtos_programados']);

                    foreach ($produtos_imediados as $chave_i => $imediatos) {
                        $existe = 0;
                        foreach ($produtos_programados as $chave_p => $programados) {
                            if ($imediatos['codigo'] == $programados['codigo'] && $imediatos['filial'] == $programados['filial']) {
                                $produtos_programados[$chave_p]['quantidade_pedido'] = $programados['quantidade_pedido'] + $imediatos['quantidade_pedido'];
                                $existe = 1;
                            }
                        }
                        if ($existe == 0) {
                            $produtos_programados[count($produtos_programados)] = $imediatos;
                        }
                    }
                } else {
                    $produtos_programados = $produtos_imediados;
                }


                $sessao_pedido['produtos_programados'] = $produtos_programados;

                $dados_sessao = $sessao_pedido;

                $this->atualizar_sessao($dados_sessao, $tipo);

                //$this->inserir_pedido_temp($sessao_pedido['produtos'][$indice_produto_imediato]['codigo'], $sessao_pedido['codigo_usuario'], $produtos[$indice_produto_imediato]['quantidade_pedido']);
                $this->db->delete('pedidos', array('id_usuario' => $sessao_pedido['codigo_usuario']));
                $dados['status'] = 1;
                $dados['mensagem'] = 'Produtos Transferidos com sucesso.';
            } else {
                $dados['status'] = 2;
                $dados['mensagem'] = 'Não Existem Produtos no Pedido Imediatos para Transferir.';
            }
        } else {
            $dados['status'] = 2;
            $dados['mensagem'] = 'Erro de acesso!';
        }
        echo json_encode($dados);
    }

    function verificar_desconto($cond = NULL, $total = NULL) {
        $valor = 0;
        if (!$cond) {
            $cond = $this->input->post('condicao');
        }
        $condicao = $this->db->where('cod', $cond)->get('formas_pagamento')->row_array();

        if (!$total) {
            $total = $this->input->post('total');
        }

        $arrayCondicao = $this->db->where(array('valor_minimo <=' => $total, 'valor_maximo >=' => $total))->get('precos_pagamento')->result_array();
        $array = unserialize($arrayCondicao[0]['array']);

        if (count($arrayCondicao) > 0)
            foreach ($array as $cArray => $vArray) {
                if ($cArray == $condicao['id_grupo']) {
                    $valor = $vArray;
                }
            }
        //echo json_encode($valor);
        return json_encode($valor);
    }

    //CUSTOM: Aristides - 14/06/2013
    function verificar_desconto_maximo_usuario($metodo_retorno = NULL) {

        $categoria_cliente = $_SESSION['sessao_pedido']['cliente']['categoria'];
        if ($categoria_cliente == NULL) {

            $auxiliar = array(
                'desconto' => 0.01,
                'msg' => 'vazio'
            );
        } else {
            //carrega a base de dados
            $this->db_cliente = $this->load->database('db_cliente', TRUE);
            $this->config->load('db_cliente_' . $this->db_cliente->erp);
            $this->_db_cliente = $this->config->item('db_cliente');
            //              

            $desconto_maximo = $this->_db_cliente['campos']['descontos']['desconto_maximo'];
            $codigo_desconto = $this->_db_cliente['campos']['descontos']['codigo_desconto'];
            $tabela_descontos = $this->_db_cliente['tabelas']['descontos'];
//                
            $this->db_cliente->select($desconto_maximo);
            $this->db_cliente->from($tabela_descontos);

            $categoria_cliente = $_SESSION['sessao_pedido']['cliente']['categoria'];

            $this->db_cliente->where($codigo_desconto, $categoria_cliente);

            $resultado = $this->db_cliente->get()->result();
            $desconto_cliente = $resultado[0]->ZY_DESCON1;

            $desconto_post = $this->input->post('desconto_geral');
            $desconto_replace = str_replace(',', '.', $desconto_post);
            $desconto_geral = floatval($desconto_replace);

            //$desconto_geral = number_format($desconto_number, 0, '', '');

            if ($desconto_geral > $desconto_cliente) {

                $auxiliar = array(
                    'desconto' => $desconto_cliente,
                    'msg' => 'Desconto acima do limite desse usuário'
                );
            } else {

                $auxiliar = array(
                    'desconto' => $desconto_geral,
                    'msg' => 'ok'
                );
            }
        }


        if ($metodo_retorno == 'array') {
            return $auxiliar['desconto'];
        } else {
            echo json_encode($auxiliar);
        }
    }

    //FIM CUSTOM: Aristides - 14/06/2013
	
    function obter_preco_produto($tipo_sessao = NULL) {
			
		if($this->session->userdata('sessao_pedido'))
		{
			$sessao = $this->session->userdata('sessao_pedido');
		}
		else if($this->session->userdata('sessao_orcamento'))
		{
			$sessao = $this->session->userdata('sessao_orcamento');
		}
		
        $codigo_produto = $_POST['codigo_produto'];
		$tipo_sessao = $_POST['tipo_sessao'];	
		
		$codigo_filial = $_POST['filial'];
		$uf_filial = $this->db_cliente->obter_uf_filial($codigo_filial);
		
		$codigo_tabela_precos = $_POST['codigo_tabela_precos'];
		/*
		if($sessao['cliente']['tabela_precos'] = '203')
		{
			$codigo_tabela_precos = '203';
		}
		else
		{*/
			if($sessao['cliente']['tipo_cliente'] == 'XXX') 
			{
				$codigo_tabela_precos = '001';
			}
			if($sessao['cliente']['estado'] == $uf_filial['ZT_ESTENT'])
			{
				$codigo_tabela_precos = '001';
			}
			if( ($sessao['cliente']['tipo_cliente'] != 'XXX') && ($sessao['cliente']['estado'] != $uf_filial['ZT_ESTENT']) ) 
			{
				$codigo_tabela_precos = '301';
			}
		//}
		
		if ($codigo_produto && $codigo_tabela_precos) 
		{
            $dados_produto = $this->db_cliente->obter_produto($codigo_produto, $codigo_tabela_precos, $codigo_filial, $tipo_sessao);
			
			echo number_format((double)$dados_produto['preco'], 5, '.', '');
        } 
		else 
		{
            echo 0;
        }
    }

    function listar_estoque() {
        $chegadas = $this->db_cliente->obterPrevisaoChegada($this->input->get('filial'), $this->input->get('codigo'));
        //die(print_r($chegadas));
        $json['titulo'] = 'Tabela de Previsão Chegada do produto "' . $chegadas[0]->descricao . '" na filial ' . $this->input->get('filial');
        $json['tabela'] = '<table class="novo_grid"><thead>';
        $json['tabela'] .= '<tr><th>Item</th><th>Quantidade</th><th>Data de Entrada</th></tr></thead><tbody>';
        foreach ($chegadas as $indice => $chegada) {
            $json['tabela'] .= '<tr><td class="center">' . ($indice + 1) . '</td><td class="right">' . $chegada->quantidade . '</td><td class="center">' . date('d/m/Y', strtotime($chegada->chegada)) . '</td></tr>';
        }
        $json['tabela'] .= '</tbody></table>';
        echo json_encode($json);
    }

    function add_produtos($codigo_produto, $quantidade, $preco, $tabela_precos, $filial) {

        $sessao_pedido = $this->session->userdata('sessao_pedido');

        $_POST['codigo_produto'] = $codigo_produto;
        $_POST['quantidade'] = $quantidade;
        $_POST['filial'] = $filial;
        $_POST['preco_unico'] = $preco;
        $_POST['codigo_tabela_precos'] = $tabela_precos;

        // Inserindo Quantidade e Produtos na sessao

        $dados_produto = $this->db_cliente->obter_produto($this->input->post('codigo_produto'), $this->input->post("codigo_tabela_precos"));
		
        $dados_produto['quantidade_pedido'] = $this->input->post('quantidade');
        //$dados_produto['preco'] = str_replace(",",".",$dados_produto['preco']);
        $dados_produto['preco'] = str_replace(",", ".", $this->input->post('preco_unico'));
        //$dados_produto['desconto'] = str_replace(",",".",$this->input->post('desconto'));
        // Buscando Estoque do Produto
        $estoque_produto = $this->obter_estoque_disponivel($this->input->post('codigo_produto'), TRUE, $this->input->post('filial'));

        $dados_produto['filial'] = $this->input->post('filial');

        if ($dados_produto['quantidade_pedido'] > $estoque_produto) {
            // Pedidos Imediato Dividido pelo programado
            if ($estoque_produto > 0) {
                $quantidade_imediata = $estoque_produto;

                $dados_produto['quantidade_pedido'] = $quantidade_imediata;

                if ($sessao_pedido['produtos']) {
                    foreach ($sessao_pedido['produtos'] as $indice => $produto) {
                        if ($produto['filial'] == $this->input->post('filial') && trim($produto['codigo']) == trim($this->input->post('codigo_produto'))) {
                            $dados_produto['quantidade_pedido'] = $dados_produto['quantidade_pedido'] + $produto['quantidade_pedido'];
                            $sessao_pedido['produtos'][$indice] = $dados_produto;

                            $this->inserir_pedido_temp(trim($this->input->post('codigo_produto')), $sessao_pedido['codigo_usuario'], $dados_produto['quantidade_pedido'], $this->input->post('filial'));

                            $somar_produto = TRUE;
                        }
                    }
                }

                if (!$somar_produto) {

                    $this->inserir_pedido_temp(trim($this->input->post('codigo_produto')), $sessao_pedido['codigo_usuario'], $dados_produto['quantidade_pedido'], $this->input->post('filial'));

                    $sessao_pedido['produtos'][] = $dados_produto;
                }
            }


            // Pedidos Programados
            $quantidade_programada = $this->input->post('quantidade') - $estoque_produto;
            $chegada = 0;
            $previsaoChegada1 = $this->db_cliente->obterPrevisaoChegada($produto['filial'], $produto['codigo']);
            $previsaoChegada = $previsaoChegada1[0];
            if ($previsaoChegada && $chegada < $previsaoChegada->chegada) {
                $dados_produto['previsao_chegada'] = date('d/m/Y', strtotime($previsaoChegada->chegada));
            } else {
                $dados_produto['previsao_chegada'] = 'Sem Previsao';
            }

            if ($estoque_produto < 0) {
                $dados_produto['quantidade_pedido'] = $this->input->post('quantidade');
            } else {
                $dados_produto['quantidade_pedido'] = $quantidade_programada;
            }

            if ($sessao_pedido['produtos_programados']) {
                foreach ($sessao_pedido['produtos_programados'] as $indice => $produto) {
                    if ($produto['filial'] == $this->input->post('filial') && trim($produto['codigo']) == trim($this->input->post('codigo_produto'))) {
                        $chegada = 0;
                        $previsaoChegada1 = $this->db_cliente->obterPrevisaoChegada($produto['filial'], $produto['codigo']);
                        $previsaoChegada = $previsaoChegada1[0];
                        if ($previsaoChegada && $chegada < $previsaoChegada->chegada) {
                            $dados_produto['previsao_chegada'] = date('d/m/Y', strtotime($previsaoChegada->chegada));
                        } else {
                            $dados_produto['previsao_chegada'] = 'Sem Previsao';
                        }

                        $dados_produto['quantidade_pedido'] = $dados_produto['quantidade_pedido'] + $produto['quantidade_pedido'];

                        $sessao_pedido['produtos_programados'][$indice] = $dados_produto;

                        $somar_produto_programado = TRUE;
                    }
                }
            }

            if (!$somar_produto_programado) {
                $sessao_pedido['produtos_programados'][] = $dados_produto;
            }
        } else {
            //Pedidos Imediatos

            if ($sessao_pedido['produtos']) {
                foreach ($sessao_pedido['produtos'] as $indice => $produto) {
                    if (trim($produto['codigo']) == trim($this->input->post('codigo_produto'))) {
                        $dados_produto['quantidade_pedido'] = $this->input->post('quantidade') + $produto['quantidade_pedido'];

                        $sessao_pedido['produtos'][$indice] = $dados_produto;

                        $this->inserir_pedido_temp(trim($this->input->post('codigo_produto')), $sessao_pedido['codigo_usuario'], $dados_produto['quantidade_pedido'], $this->input->post('filial'));

                        $somar_produto = TRUE;
                    }
                }
            }

            if (!$somar_produto) {

                $this->inserir_pedido_temp(trim($this->input->post('codigo_produto')), $sessao_pedido['codigo_usuario'], $dados_produto['quantidade_pedido'], $this->input->post('filial'));

                $sessao_pedido['produtos'][] = $dados_produto;
            }
        }



        //Adicionando Produtos na Sessão
        $dados_sessao = $sessao_pedido;
        $this->session->set_userdata('sessao_pedido', $dados_sessao);

        //Limpando Campo do Produto Adicionado

        $_POST['codigo_produto'] = "";
        $_POST['_codigo_produto'] = "";
        $_POST['quantidade'] = "";
    }

    public function calc_imposto($cliente, $loja, $tipo, $quantidade, $preco, $codigo_produto) {
        //$array = $this->db_cliente->obter_impostos_item($cliente, $loja, $tipo, $quantidade, $preco, $codigo_produto);
        $array = obter_st($cliente, $loja, $tipo, $codigo_produto, 'auto', $quantidade, $preco);
        var_dump($array);
    }

    function imprimir_sessao($tipo_pedido = TIPO_PEDIDO) {

        if ($tipo_pedido == "tudo") {
            echo "<pre>";
            print_r($_SESSION);
            echo "</pre>";
            die();
        } else {

            $sessao_pedido = $this->session->userdata('sessao_' . $tipo_pedido);
            echo "<pre>";
            print_r($sessao_pedido);
            echo "</pre>";
        }
    }

    /**
     * Metódo:		obter_sessao
     * 
     * Descrição:	Função Utilizada para obter a sessão de acordo com o tipo de ação realizada no DW Portal do Representante
     * 
     * Data:			02/04/2012
     * Modificação:	02/04/2012
     * 
     * @access		public
     * @param		string 		$tipo 			- Opções: pedido / orcamento 	
     * @version		1.0
     * @author 		DevelopWeb Soluções Web
     * 
     */
    function obter_sessao($tipo = NULL) {
        if ($tipo == TIPO_ORCAMENTO) {
            return $this->session->userdata('sessao_orcamento');
        } else {
            return $this->session->userdata('sessao_pedido');
        }
    }

    /**
     * Metódo:		atualizar_sessao
     * 
     * Descrição:	Função Utilizada para atualizar a sessão de acordo com o tipo de ação realizada no DW Portal do Representante
     * 
     * Data:			02/04/2012
     * Modificação:	02/04/2012
     * 
     * @access		public
     * @param		Array 		$sessao_pedido	- Dados do Pedido/Orçamento
     * @param		string 		$tipo 			- Opções: pedido | orcamento 	
     * @version		1.0
     * @author 		DevelopWeb Soluções Web
     * 
     */
    function atualizar_sessao($sessao_pedido, $tipo = NULL) {		
        if ($tipo == TIPO_ORCAMENTO) {
            $sessao_pedido['tipo_pedido'] = TIPO_ORCAMENTO;
            $this->session->set_userdata('sessao_orcamento', $sessao_pedido);
        } else {
            $sessao_pedido['tipo_pedido'] = TIPO_PEDIDO;
            $this->session->set_userdata('sessao_pedido', $sessao_pedido);
        }
    }

    /**
     * Metódo:		deletar_sessao
     * 
     * Descrição:	Função Utilizada para remover a sessão de acordo com o tipo de ação realizada no DW Portal do Representante
     * 
     * Data:			02/04/2012
     * Modificação:	02/04/2012
     * 
     * @access		public
     * @param		Array 		$sessao_pedido	- Dados do Pedido/Orçamento
     * @param		string 		$tipo 			- Opções: pedido | orcamento 	
     * @version		1.0
     * @author 		DevelopWeb Soluções Web
     * 
     */
    function deletar_sessao($tipo = NULL) {

        if ($tipo == TIPO_ORCAMENTO) {
            $this->session->unset_userdata('sessao_orcamento');
        } else {
            $this->session->unset_userdata('sessao_pedido');
        }
    }

    /**
     * Metódo:		regra_tabela_preco
     * 
     * Descrição:	Se o cliente tiver uma tabela de preço especial, recebe a tabela senão tabela de preço padrão
     * 
     * Data:			02/04/2012
     * Modificação:	02/04/2012
     * 
     * @access		public
     * @param		string 		$codigo_cliente	- Código do Cliente	
     * @version		1.0
     * @author 		DevelopWeb Soluções Web
     * 
     */
    function regra_tabela_preco($sessao_pedido) {
        if ($sessao_pedido['cliente']['tabela_precos']) { //SE O CLIENTE TIVER UMA TABELA DE PREÇO ESPECIAL, RECEBE A TABELA
            return $sessao_pedido['cliente']['tabela_precos'];
        } else { //TABELA DE PREÇO PADRÃO
            $configuracoes = $this->db->from('configuracoes')->get()->result();

            foreach ($configuracoes as $valor) {
                switch ($valor->campo) {
                    case 'tabela_precos_padrao':
                        $tabela_precos_padrao = $valor->valor;
                        break;
                }
            }

            //Tabela de Preços - Padrão
            return $tabela_precos_padrao;
        }
    }

	//CORREÇÃO - 20140620
	/** Verifica se os produtos adicionados estão com desconto dentro da política. 
	*/
	function validar_desconto_maximo_classe_cliente($sessao_pedido, $tipo){
		$classe_desconto = $sessao_pedido['cliente']['categoria'];
        $desconto_maximo = $this->validar_classes_desconto($classe_desconto);
		$erros = array();
		$status = array();
			if(isset($sessao_pedido['produtos'])){
				foreach($sessao_pedido['produtos'] as $indice => $produto){					
					$status = $this->validar_desconto_produto($produto,$desconto_maximo);
					if($status['erro']){							
						$erros[] = $status['mensagem'];						
					}				
				}
			}
							
			if(isset($sessao_pedido['produtos_programados'])){
				foreach($sessao_pedido['produtos_programados'] as $indice => $produto){					
					$status = $this->validar_desconto_produto($produto,$desconto_maximo);
					if($status['erro']){						
						$erros[] = $status['mensagem'];						
					}	
				}
			}
			
			if(count($erros) > 0){
			//Caso algum produto esteja fora do desconto maximo, todos os descontos serão zerados.
			if(isset($sessao_pedido['produtos'])){
				foreach($sessao_pedido['produtos'] as $indice => $produto){		
					$produtos = $sessao_pedido['produtos'];
					$produtos[$indice]['desconto'] = 0;
					
					$sessao_pedido['produtos'] = $produtos;
					$dados_sessao = $sessao_pedido;
					$this->atualizar_sessao($dados_sessao, $tipo);	
				}
			}
			
			if(isset($sessao_pedido['produtos_programados'])){
				foreach($sessao_pedido['produtos_programados'] as $indice => $produto){	
					$produtos = $sessao_pedido['produtos_programados'];
					$produtos[$indice]['desconto'] = 0;
					$sessao_pedido['produtos_programados'] = $produtos;
					$dados_sessao = $sessao_pedido;
					$this->atualizar_sessao($dados_sessao, $tipo);
		
				}
			}
			$mensagem = '';
			
			foreach($erros as $erro){
				$mensagem .= $erro.'<br/>';
				
			}
			
			return array('erro' =>true,
			'mensagem' => $mensagem);			
		}else{
			return array( 	'erro' => false,
							'mensagem' => 'sucesso'
			);
		}		
	}	
	
	
	function validar_desconto_produto($produto, $desconto_maximo){
		$desconto = $produto['desconto'];
		$status = array();
		$descricao_produto = $produto['codigo'].' - '.exibir_texto($produto['descricao']);
		
		if (floatval($desconto) > floatval($desconto_maximo)){
           $status = array(
							'erro' => true,
							'mensagem' => 'Produto '.$descricao_produto.' com desconto inválido para esta classe de cliente.'
						);
        } else if (trim($desconto) > 100 || trim($desconto) > '100') {
            $status = array(
							'erro' => true,
							'mensagem' => 'Produto '.$descricao_produto.'com percentual de desconto inválido.'
						);						
        } else {
			$status = array(
					'erro' => false,
					'mensagem' => 'Produto '.$descricao_produto.' com desconto Válido!'
			);

        }
		return $status;
	}
	//FIM CORREÇÃO
	
	
	function obter_informacoes_cliente_formulario_ajax($codigo_loja_cliente = NULL) {
	
        $dados = array(
            'cliente_encontrado' => FALSE,
            'html' => NULL
        );

        $codigo_loja_cliente = explode('_', $codigo_loja_cliente);
        $codigo_cliente = $codigo_loja_cliente[0];
        $loja_cliente = $codigo_loja_cliente[1];

        if ($codigo_cliente && $loja_cliente) {
            $cliente = $this->db_cliente->obter_cliente_formulario($codigo_cliente, $loja_cliente);
        }
		
        $inatividade = array(
            '0' => 'Não Informado',
            '1' => 'Alt CNPJ',
            '2' => 'Cliente s/ Crédito',
            '3' => 'Fech. Oper.',
            '4' => 'Insatisfação Cliente',
            '5' => 'Mud. Atividade',
            '6' => 'Inativo Compra',
            '7' => 'Operação Inativa',
            '8' => 'Mercad',
        );

        if ($cliente) {
            $dados['cliente_encontrado'] = TRUE;

            $dados['html'] = '<p><strong>' . $cliente['nome'] . '</strong> - ' . $cliente['cpf'] . ' - <strong>' . $cliente['status'] . '</strong>  ';

            $classes = $this->db->select()->from('descontos_classe')->get()->result();
            foreach ($classes as $classe) {
                $listagem[$classe->codigo_classe] = $classe->nome_classe;
            }

            $inat = $inatividade[0];
            if ($cliente['status'] == 'Inativo') {
                foreach ($inatividade as $chaveInat => $valorInat) {
                    if ($chaveInat == $cliente['motivo_inativo']) {
                        $inat = $valorInat;
                    }
                }
            }
			$cliente['pode_visitar'] = '1';
			
			/*
			if($this->session->userdata('grupo_usuario') == 'representantes') {
				if (trim($this->session->userdata('codigo_usuario')) != '') {
					$podeVisitar = $this->db_cliente->verificar_pode_visitar($this->session->userdata('codigo_usuario'), $cliente['codigo']);
					if ($podeVisitar) {
						if ($podeVisitar->OKFORM == 'N') {
							$cliente['pode_visitar'] = '2';
						}
					}
				}
			}
			*/
            $dados['html'] .= $inat;

            $dados['html'] .= '</p>
				<ul style="float: left; margin-right: 10px;">
				
					<li><strong>Contato:</strong> ' . $cliente['pessoa_contato'] . '</li>
					<li><strong>Limite de crédito:</strong> R$ ' . number_format($cliente['limite_credito'], 2, ',', '.') . '</li>
					<li><strong>Títulos em aberto:</strong> R$ ' . number_format($cliente['total_titulos_em_aberto'], 2, ',', '.') . '</li>
					<li><strong>Títulos vencidos:</strong> R$ ' . number_format($cliente['total_titulos_vencidos'], 2, ',', '.') . '</li>
				</ul>
				
				<ul style="float: left; margin-right: 10px;">
					<li><strong>Telefone:</strong> ' . $cliente['telefone'] . '</li>
					<li><strong>Endereço:</strong> ' . $cliente['endereco'] . '</li>
					<li><strong>Bairro:</strong> ' . $cliente['bairro'] . '</li>
					<li><strong>CEP:</strong> ' . $cliente['cep'] . '</li>
				</ul>
				
				<ul style="float: left; margin-right: 10px;">
					<li><strong>Cidade:</strong> ' . $cliente['cidade'] . '</li>
					<li><strong>Estado:</strong> ' . $cliente['estado'] . '</li>
					<li><strong>Tabela de preços:</strong> ' . $this->db_cliente->obter_tabela_preco_descricao($cliente['tabela_precos']) . '</li>
					<li><strong>Classe:</strong> ' . ($cliente['categoria'] ? element($cliente['categoria'], $listagem) : 'Sem Classe') . '</li>
				</ul>
				
				<div style="clear: both;"></div>
			';
        }
				
		$dados['cliente'] = $cliente;		
		
        echo json_encode($dados);
    }

	
}
