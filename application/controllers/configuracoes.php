<?php

class Configuracoes extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	
	function dados_empresa()
	{
		$empresa = $this->db->from('empresas')->where('id', 1)->get()->row_array();
		
		if ($_POST)
		{
			if (!$this->input->post('razao_social'))
			{
				$erro = 'Digite uma razão social.';
			}
			else if ($this->input->post('cnpj') && !$this->_validar_cnpj($this->input->post('cnpj')))
			{
				$erro = 'Digite um CNPJ válido.';
			}
			else if ($this->input->post('cep') && !$this->_validar_cep($this->input->post('cep')))
			{
				$erro = 'Digite um CEP válido.';
			}
			else if ($this->input->post('telefone_1') && !$this->_validar_telefone($this->input->post('telefone_1')))
			{
				$erro = 'Digite um telefone válido para o campo Telefone 1.';
			}
			else if ($this->input->post('telefone_2') && !$this->_validar_telefone($this->input->post('telefone_2')))
			{
				$erro = 'Digite um telefone válido para o campo Telefone 2.';
			}
			else if ($this->input->post('email') && !valid_email($this->input->post('email')))
			{
				$erro = 'Digite um e-mail válido.';
			}
			else
			{
				if (!$empresa)
				{
					$_POST['codigo_cliente'] = 'DW1000';
					
					$_POST['icms'] = serialize($_POST['icms']);
					
					$this->db->insert('empresas', $_POST);
				}
				else
				{
					$_POST['icms'] = serialize($_POST['icms']);
					
					$this->db->update('empresas', $_POST, array('id' => $empresa['id']));
				}
				
				$sucesso = 'Dados atualizados com sucesso.';
			}
		}
		else
		{
			$_POST = $empresa;
		}
		
		$_POST['icms'] = unserialize($_POST['icms']);
		
		$conteudo = heading('Dados da Empresa', 2);
		
		if ($erro)
		{
			$conteudo .= '<p class="erro">' . $erro . '</p>';
		}
		else if ($sucesso)
		{
			$conteudo .= '<p class="sucesso">' . $sucesso . '</p>';
		}
		
		$conteudo .= form_open(current_url());
		
		$conteudo .= heading('Informações Gerais', 3);
		
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Cód. cliente:' . br() . form_input('codigo_cliente', $this->input->post('codigo_cliente'), 'size="10" readonly="readonly"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Razão social:' . br() . form_input('razao_social', $this->input->post('razao_social'), 'size="40"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Nome fantasia:' . br() . form_input('nome_fantasia', $this->input->post('nome_fantasia'))) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('CNPJ:' . br() . form_input('cnpj', $this->input->post('cnpj'), 'alt="cnpj" size="14"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Inscrição estadual:' . br() . form_input('inscricao_estadual', $this->input->post('inscricao_estadual'), 'size="10"')) . '</p>';
		
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= heading('Endereço', 3);
		
		$conteudo .= '<div dojoType="dijit.Tooltip" connectId="cep" position="below">Ao digitar o CEP os campos Endereço, Bairro, Cidade e Estado serão preenchidos automaticamente.</div>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('CEP:' . br() . form_input('cep', $this->input->post('cep'), 'id="cep" alt="cep" size="5"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Endereço:' . br() . form_input('endereco', $this->input->post('endereco'), 'size="53"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Nº:' . br() . form_input('numero', $this->input->post('numero'), 'size="1"')) . '</p>';

		/***/
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Complemento:' . br() . form_input('complemento', $this->input->post('complemento'), 'size="33"')) . '</p>';
		
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Bairro:' . br() . form_input('bairro', $this->input->post('bairro'), 'size="20"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Cidade:' . br() . form_input('cidade', $this->input->post('cidade'), 'size="20"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Estado:' . br() . form_dropdown('estado', $this->_obter_estados(), $this->input->post('estado'))) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Site:' . br() . form_input('site', $this->input->post('site'), 'size="40"')) . '</p>';
		
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= heading('Contato', 3);
		
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Telefone 1:' . br() . form_input('telefone_1', $this->input->post('telefone_1'), 'alt="phone" size="10"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Telefone 2:' . br() . form_input('telefone_2', $this->input->post('telefone_2'), 'alt="phone" size="10"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('E-mail:' . br() . form_input('email', $this->input->post('email'), 'size="40"')) . '</p>';
		
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= heading('Porcentagem do ICMS Por Estado', 3);
		
		$ufs = $this->_obter_estados();
		
		$c = 0;
		
		foreach ($ufs as $i => $v)
		{
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label($i . ':' . br() . form_input('icms[' . $i . ']', isset($_POST['icms'][$i]) ? $_POST['icms'][$i] : 17, 'alt="integer" size="1"')) . '</p>';
			
			$c++;
			
			if ($c % 14 == 0)
			{
				$conteudo .= '<div style="clear: both;"></div>';
			}
		}
		
		/***/
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '<div style="border-top: 1px solid #DDD; margin-top: 25px;"></div>';
		
		$conteudo .= '<p>' . form_submit('', 'Concluir') . '</p>';
		
		$conteudo .= form_close();
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function grupos_produtos_ativos($id = NULL)
	{
		$conteudo = heading('Grupos de Produtos Ativos', 2);
		
		if ($_POST)
		{
			$this->db->delete('grupos_produtos_ativos', array('id >' => 0));
			
			if ($_POST['codigo_grupos_produtos_ativos'])
			{
				foreach ($_POST['codigo_grupos_produtos_ativos'] as $indice => $valor)
				{
					$this->db->insert('grupos_produtos_ativos', array('codigo' => $valor));
				}
			}
		}
		
		$grupos_ativos = $this->db->from('grupos_produtos_ativos')->get()->result();
		foreach ($grupos_ativos as $grupo_ativo)
		{
			$_POST['codigo_grupos_produtos_ativos'][] = $grupo_ativo->codigo;
		}
		
		$conteudo .= form_open(current_url());
		
		$grupos = $this->db_cliente->obter_grupos_produtos(FALSE);
		
		if (!$grupos)
		{
			$conteudo .= '<p>Não há grupos de produtos cadastrados.</p>';
		}
		else
		{
			foreach ($grupos as $grupo)
			{
				$conteudo .= '<div style="float: left; width: 25%;"><p style="margin-right: 10px;">' . form_label(form_checkbox('codigo_grupos_produtos_ativos[]', $grupo['codigo'], $_POST['codigo_grupos_produtos_ativos'] && in_array($grupo['codigo'], $_POST['codigo_grupos_produtos_ativos']) ? TRUE : FALSE) . ' ' . $grupo['codigo'] . ' - ' . $grupo['descricao']) . '</p></div>';
			}
		}
		
		$conteudo .= '<div style="clear: both;"></div><p>' . form_submit('concluir', 'Concluir') . '</p>';
		
		$conteudo .= form_close();
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
}