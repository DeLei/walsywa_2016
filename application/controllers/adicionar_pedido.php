<?php

class Adicionar_pedido extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	
	/*
		PASSO 1:
		- Selecionar representante
		- Selecionar cliente ou prospect
		- Selecionar evento
	*/
	function passo_1($id)
	{
		$pedido = $this->db->from('pedidos')->where('id', $id)->get()->row();
		
		// se nenhum pedido for encontrado vamos fazer com que nada aconteça
		if (!$pedido)
		{
			redirect();
		}
	}
	
	/*
		PASSO 2:
		- Adicionar itens
		- Aplicar desconto
		- Digitar data de entrega
	*/
	function passo_2($id)
	{
		
	}
	
	/*
		PASSO 3:
		- Selecionar forma de pagamento
		- Selecionar transportadora
		- Digitar ordem de compra
		- Digitar observação do pedido imediato
		- Digitar observação do pedido programado
	*/
	function passo_3($id)
	{
		
	}
	
	/*
		PASSO 4:
		- Confirmar
	*/
	function passo_4($id)
	{
		
	}
	
	/*
		VER ESPELHO
	*/
	function ver_espelho($id)
	{
	
	}
	
	function consultar($tipo_pedido = 'orcamento')
	{
		// ** deletar pedidos não concluídos
		
		$pedidos_nao_concluidos = $this->db->from('pedidos')->where(array('id_usuario' => $this->session->userdata('id_usuario'), 'tipo' => $tipo_pedido, 'status IS NULL' => NULL))->get()->result();
		if ($pedidos_nao_concluidos) {
			foreach ($pedidos_nao_concluidos as $pedido_nao_concluido)
			{
				$this->db->delete('pedidos', array('id' => $pedido_nao_concluido->id));
				$this->db->delete('itens_pedidos', array('id_pedido' => $pedido_nao_concluido->id));
			}
		}
		
		// deletar pedidos não concluídos **
		
		//
		
		if (in_array($this->session->userdata('grupo_usuario'), array('revendas')))
		{
			$this->db->where('codigo_usuario', $this->session->userdata('codigo_usuario'));
		}
		else if (in_array($this->session->userdata('grupo_usuario'), array('vendedores')))
		{
			$this->db->where('id_usuario', $this->session->userdata('id_usuario'));
		}
		
		$conteudo .= heading(($tipo_pedido == 'pedido') ? 'Pedidos' : 'Orçamentos', 2);
		
		if ($tipo_pedido == 'orcamento')
		{
			$conteudo .= '<p><button dojoType="dijit.form.Button" type="button">' . anchor('processar_pedido/index/' . (($tipo_pedido == 'pedido') ? 'pedido' : 'orcamento'), 'Cadastrar Novo ' . (($tipo_pedido == 'pedido') ? 'Pedido' : 'Orçamento')) . '</button></p>';
		}
		
		$conteudo .= '<table cellspacing="0"><thead><tr><th>Revenda</th><th>Filial</th><th>Vendedor</th><th>Prospect</th><th>Emissão</th><th>Total (R$)</th><th>Opções</th></tr></thead><tbody>';
		
		$pedidos = $this->db->from('pedidos')->where(array('tipo' => $tipo_pedido, 'status IS NOT NULL' => NULL))->get()->result();
		foreach ($pedidos as $pedido)
		{
			$revenda = $this->db->from('usuarios')->where('codigo', $pedido->codigo_usuario)->get()->row();
			$vendedor = $this->db->from('usuarios')->where('id', $pedido->id_usuario)->get()->row();
			
			$filial = $this->db->from('filiais')->where('id', $vendedor->id_filial)->get()->row();
			if (!$filial)
			{
				$filial = $this->db->from('filiais')->where('codigo_revenda', $revenda->codigo)->order_by('id', 'asc')->limit(1)->get()->row();
			}
			
			$prospect = $this->db->from('prospects')->where('id', $pedido->id_prospect)->get()->row();
			
			$valor_total_com_ipi_e_desconto = $this->db->select('SUM(valor_total_com_ipi_e_desconto) AS valor_total_com_ipi_e_desconto')->from('itens_pedidos')->where('id_pedido', $pedido->id)->get()->row();
			
			$conteudo .= '<tr><td>' . $revenda->nome_real . '</td><td>' . $filial->nome . '</td><td>' . $vendedor->nome_real . '</td><td>' . $prospect->nome . '</td><td>' . date('d/m/Y', $pedido->timestamp) . '</td><td>' . number_format($valor_total_com_ipi_e_desconto->valor_total_com_ipi_e_desconto, 2, ',', '.') . '</td><td>' . anchor('processar_pedido/gerar_proposta/' . $pedido->id, '+ Opções') . '</td></tr>';
		}
		
		$conteudo .= '</tbody></table>';
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function index($tipo_pedido = 'orcamento', $id_prospect = NULL)
	{
		$this->db->delete('pedidos', array('id_usuario' => $this->session->userdata('id_usuario'), 'tipo' => $tipo_pedido, 'status' => NULL));
		
		$prospect = $this->db->from('prospects')->where('id', $id_prospect)->get()->row();
		
		$this->db->insert('pedidos', array(
			'codigo_usuario' => $prospect ? $prospect->codigo_usuario : NULL,
			'tipo' => $tipo_pedido,
			'id_prospect' => $id_prospect
		));
		
		redirect('processar_pedido/selecionar_cliente_prospect/' . $this->db->insert_id());
	}
	
	function excluir($id_pedido = NULL)
	{
		$pedido = $this->db->from('pedidos')->where('id', $id_pedido)->get()->row();
		
		$this->db->delete('pedidos', array('id' => $id_pedido));
		$this->db->delete('itens_pedidos', array('id_pedido' => $id_pedido));
		
		redirect('processar_pedido/consultar/' . $pedido->tipo);
	}
	
	function selecionar_cliente_prospect($id_pedido = NULL)
	{
		$pedido = $this->db->from('pedidos')->where('id', $id_pedido)->get()->row();
		
		// ** validar formulário
		
		if ($_POST)
		{
			if ($this->input->post('continuar'))
			{
				if (/*!$this->input->post('codigo_loja_cliente') && */!$this->input->post('id_prospect'))
				{
					$erro = 'Nenhum prospect foi selecionado.';
				}
				else
				{
					$usuario = $this->db->from('usuarios')->where('id', $this->input->post('id_usuario') ? $this->input->post('id_usuario') : $this->session->userdata('id_usuario'))->get()->row();
					
					$dados['id_usuario'] = $usuario->id;
					$dados['codigo_usuario'] = $usuario->codigo;
					
					/*if ($this->input->post('criar_para') == 'cliente')
					{
						$codigo_loja_cliente = explode('|', $this->input->post('codigo_loja_cliente'));
						
						$dados += array(
							'codigo_cliente' => $codigo_loja_cliente[0],
							'loja_cliente' => $codigo_loja_cliente[1],
							'id_prospect' => NULL
						);
					}
					else if ($this->input->post('criar_para') == 'prospect')
					{*/
						$dados += array(
							'codigo_cliente' => NULL,
							'loja_cliente' => NULL,
							'id_prospect' => $this->input->post('id_prospect')
						);
					//}
					
					// ** obter id da filial
					
					$usuario = $this->db->from('usuarios')->where('id', $pedido->id_usuario)->get()->row();
					
					if ($usuario->id_filial)
					{
						$dados['id_filial'] = $usuario->id_filial;
					}
					else
					{
						$filial = $this->db->from('filiais')->where('codigo_revenda', $dados['codigo_usuario'])->order_by('id', 'asc')->limit(1)->get()->row();
						
						$dados['id_filial'] = $filial->id;
					}
					
					// obter id da filial **
					
					$this->db->update('pedidos', $dados, array('id' => $id_pedido));
					
					redirect('processar_pedido/adicionar_casco/' . $id_pedido);
				}
			}
		}
		else
		{
			$_POST = array(
				'id_usuario' => $pedido->id_usuario,
				'codigo_usuario' => $pedido->codigo_usuario,
				//'criar_para' => $pedido->codigo_cliente ? 'cliente' : 'prospect',
				//'codigo_loja_cliente' => $pedido->codigo_cliente ? ($pedido->codigo_cliente . '|' . $pedido->loja_cliente) : NULL,
				'id_prospect' => $pedido->id_prospect ? $pedido->id_prospect : NULL
			);
		}
		
		// validar formulário **
		
		// ** exibir campos para seleção de cliente/prospect
		
		$conteudo .= heading('Configurando o Seu ' . ($pedido->tipo == 'pedido' ? 'Pedido' : 'Orçamento'), 2);
		
		$conteudo .= $this->_gerar_passo_a_passo($id_pedido, 1);
		
		$conteudo .= heading('Passo 1 - Selecionar Prospect', 3);
		
		if ($erro)
		{
			$conteudo .= '<p class="erro">' . $erro . '</p>';
		}
		else if ($sucesso)
		{
			$conteudo .= '<p class="sucesso">' . $sucesso . '</p>';
		}
		
		$conteudo .= form_open(current_url());
		
		if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')))
		{
			$id_usuario = $this->input->post('id_usuario') ? $this->input->post('id_usuario') : $this->_obter_indice_primeiro_usuario_usuarios('id');
			
			$conteudo .= '
				<p>Usuário:</p>
				
				' . $this->_gerar_dropdown_usuarios('id', array('name' => 'id_usuario', 'value' => $id_usuario, 'onChange' => 'id_usuario_modificado')) . '
				
				<script type="text/javascript">
					function id_usuario_modificado()
					{
						$("form").submit();
					}
				</script>
			';
		}
		else
		{
			$id_usuario = $this->session->userdata('id_usuario');
		}
		
		$usuario = $this->db->from('usuarios')->where('id', $id_usuario)->get()->row();
		$codigo_usuario = $usuario->codigo;
		
		//$conteudo .= '<p>Criar para:<br />' . form_label(form_radio('criar_para', 'prospect', ($this->input->post('criar_para') == 'prospect') ? TRUE : FALSE) . ' Prospect') . ' ' . form_label(form_radio('criar_para', 'cliente', ($this->input->post('criar_para') == 'cliente') ? TRUE : FALSE) . ' Cliente') . '</p>';
		$conteudo .= '
			<script type="text/javascript">
				dojo.addOnLoad(function(){
					/*verificar_criar_para();
					
					$("input[name=criar_para]").click(function() {
						verificar_criar_para();
					});*/
					
					var id_prospect = dijit.byId("id_prospect").getValue();
					
					if (id_prospect)
					{
						obter_informacoes_prospect(id_prospect);
					}
					else
					{
						$("#informacoes_cliente_prospect").hide();
					}
				});
				
				/*function verificar_criar_para()
				{
					if ($("input[name=criar_para]:checked").val() == "cliente")
					{
						$("#codigo_loja_cliente").parents("p").show();
						$("#id_prospect").parents("p").hide();
						
						var codigo_loja_cliente = dijit.byId("codigo_loja_cliente").getValue();
						
						if (codigo_loja_cliente) 
						{
							obter_informacoes_cliente(codigo_loja_cliente);
						}
						else
						{
							$("#informacoes_cliente_prospect").hide();
						}
					}
					else if ($("input[name=criar_para]:checked").val() == "prospect")
					{
						$("#codigo_loja_cliente").parents("p").hide();
						$("#id_prospect").parents("p").show();
						
						var id_prospect = dijit.byId("id_prospect").getValue();
						
						if (id_prospect)
						{
							obter_informacoes_prospect(id_prospect);
						}
						else
						{
							$("#informacoes_cliente_prospect").hide();
						}
					}
				}
				
				function codigo_loja_cliente_modificado()
				{
					var codigo_loja_cliente = dijit.byId("codigo_loja_cliente").getValue();
					
					obter_informacoes_cliente(codigo_loja_cliente);
				}*/
				
				function id_prospect_modificado()
				{
					var id_prospect = dijit.byId("id_prospect").getValue();
					
					obter_informacoes_prospect(id_prospect);
				}
				
				/*function obter_informacoes_cliente(codigo_loja_cliente)
				{
					$.get("' . site_url('pedidos/obter_informacoes_cliente_ajax') . '/" + codigo_loja_cliente.replace("|", "_"), function(dados) {
						if (dados.cliente_encontrado)
						{
							$("#informacoes_cliente_prospect").slideDown().html(dados.html);
						}
						else
						{
							$("#informacoes_cliente_prospect").hide();
						}
					}, "json");
				}*/
				
				function obter_informacoes_prospect(id_prospect)
				{
					$.get("' . site_url('pedidos/obter_informacoes_prospect_ajax') . '/" + id_prospect, function(dados) {
						if (dados.prospect_encontrado)
						{
							$("#informacoes_cliente_prospect").slideDown().html(dados.html);
						}
						else
						{
							$("#informacoes_cliente_prospect").hide();
						}
					}, "json");
				}
			</script>
		';
		
		//$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Cliente:' . br() . form_dropdown('codigo_loja_cliente', $this->_obter_clientes($codigo_usuario), $this->input->post('codigo_loja_cliente'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" ' . ($this->input->post('codigo_loja_cliente') ? NULL : 'displayedValue="Selecione um cliente..."') . ' id="codigo_loja_cliente" onChange="codigo_loja_cliente_modificado" intermediateChanges="true" style="width: 400px;"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Prospect:' . br() . form_dropdown('id_prospect', $this->_obter_prospects($codigo_usuario), $this->input->post('id_prospect'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" ' . ($this->input->post('id_prospect') ? NULL : 'displayedValue="Selecione um prospect..."') . ' id="id_prospect" onChange="id_prospect_modificado" intermediateChanges="true" style="width: 400px;"')) . '</p>';
		
		$conteudo .= '<div style="clear: left;"></div>';
		
		$conteudo .= '<div id="informacoes_cliente_prospect" style="background-color: #EEE; border: 1px solid #DDD; display: none; float: left; margin-top: 10px; padding: 0 10px 10px 10px; -webkit-border-radius: 5px;"></div>';
		
		// exibir campos para seleção de cliente/prospect **
		
		// ** opções do formulário
		
		$conteudo .= '<div style="clear: left;"></div>';
		$conteudo .= '<p>' . form_submit('continuar', 'Continuar', 'class="continuar"') . '</p>';
		$conteudo .= form_close();
		
		// opções do formulário **
		
		// ** visão geral
		
		$conteudo .= '
			<script type="text/javascript">
				$(document).ready(function() {
					$.get("' . site_url('processar_pedido/obter_visao_geral_ajax/' . $id_pedido) . '", function(dados) {
						$("#conteudo").prepend(dados);
						
						alinhar_conteudo_tabelas();
					});
				});
			</script>
		';
		
		// visão geral **
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function adicionar_casco($id_pedido = NULL)
	{
		$pedido = $this->db->from('pedidos')->where('id', $id_pedido)->get()->row();
		
		// ** validar formulário
		
		if ($_POST)
		{
			if ($this->input->post('voltar'))
			{
				redirect('processar_pedido/selecionar_cliente_prospect/' . $id_pedido);
			}
			else if ($this->input->post('adicionar_item'))
			{
				if (!$this->input->post('codigo_produto'))
				{
					$erro = 'Selecione um casco.';
				}
				else if (!is_numeric($this->input->post('preco_item')))
				{
					$erro = 'Digite um preço válido.';
				}
				else if (!is_numeric($this->input->post('quantidade_item')))
				{
					$erro = 'Digite uma quantidade válida.';
				}
				else if (!is_numeric($this->input->post('desconto_item')))
				{
					$erro = 'Digite um desconto válido.';
				}
				else if ($this->db->from('itens_pedidos')->where(array('id_pedido' => $id_pedido, 'tipo_produto' => 'casco'))->get()->row())
				{
					$erro = 'Você não pode adicionar mais de um casco.';
				}
				else
				{
					$this->db->delete('itens_pedidos', array('id_pedido' => $id_pedido, 'tipo_produto' => 'casco', 'codigo' => $this->input->post('codigo_produto')));
					
					$produto = $this->db_cliente->obter_produto($this->input->post('codigo_produto'));
					
					$this->db->insert('itens_pedidos', array(
						'timestamp' => time(),
						'tipo' => 'imediato',
						'id_pedido' => $id_pedido,
						'tipo_produto' => 'casco',
						'codigo' => $produto['id'],
						'codigo_real' => $produto['codigo'],
						'descricao' => $produto['descricao'],
						'unidade_medida' => $produto['unidade_medida'],
						'preco' => $this->input->post('preco_item'),
						'codigo_tabela_precos' => ($produto['preco'] == $this->input->post('preco_produto')) ? 1 : NULL,
						'ipi' => 0,
						'desconto' => $this->input->post('desconto_item'),
						'quantidade' => $this->input->post('quantidade_item')
					));
					
					$this->_calcular_valores_item_pedido($this->db->insert_id());
					
					$this->db->update('pedidos', array('codigo_real_casco' => $produto['codigo']), array('id' => $id_pedido));
					
					$sucesso = 'Casco adicionado com sucesso. ' . form_submit('adicionar_novo_item', 'Adicionar Novo Motor', 'class="adicionar_novo_item" style="display: none;"');
					
					unset($_POST);
				}
			}
			else if ($this->input->post('continuar'))
			{
				if (!$this->db->from('itens_pedidos')->where(array('id_pedido' => $id_pedido, 'tipo_produto' => 'casco'))->get()->row())
				{
					$erro = 'Nenhum casco foi adicionado.';
				}
				else
				{
					redirect('processar_pedido/adicionar_motores/' . $id_pedido);
				}
			}
		}
		
		// validar formulário **
		
		// ** exibir campos para adição de item
		
		$conteudo .= heading('Configurando o Seu ' . ($pedido->tipo == 'pedido' ? 'Pedido' : 'Orçamento'), 2);
		
		$conteudo .= $this->_gerar_passo_a_passo($id_pedido, 2);
		
		$conteudo .= heading('Passo 2 - Adicionar Casco', 3);
		
		if ($erro)
		{
			$conteudo .= '<p class="erro">' . $erro . '</p>';
		}
		else if ($sucesso)
		{
			$conteudo .= '<p class="sucesso">' . $sucesso . '</p>';
		}
		
		$conteudo .= form_open(current_url() . '/#itens_adicionados');
		
		$conteudo .= '<div id="campos_item">';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Casco:' . br() . form_dropdown('codigo_produto', $this->_obter_cascos(), $this->input->post('codigo_produto'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" ' . ($this->input->post('codigo_produto') ? NULL : 'displayedValue="Selecione um casco..."') . ' id="codigo_produto" onChange="codigo_produto_modificado" intermediateChanges="true" style="width: 400px;"')) . '</p>';
		$conteudo .= '<div style="clear: left;"></div>';
		$conteudo .= '<p style="float: left; margin-right: 10px; display: none;">' . form_label('Preço:' . br() . form_input('preco_item', NULL, 'dojoType="dijit.form.CurrencyTextBox" currency="R$ " displayedValue="R$ ' . ($this->input->post('preco_item') ? number_format($this->input->post('preco_item'), 2, ',', '') : '0,00') . '" promptMessage="Para centavos use a vírgula. Exemplo: 1423,20" id="preco_item" onChange="preco_item_modificado" style="width: 120px;"')) . '<br /><span id="preco_item_sugerido" style="font-size: 11px;"></span></p>';
		$conteudo .= '<p style="float: left; margin-right: 10px; display: none;">' . form_label('Quantidade:' . br() . form_input('quantidade_item', NULL, 'dojoType="dijit.form.NumberSpinner" constraints="{min: 1}" displayedValue="' . ($this->input->post('quantidade_item') ? $this->input->post('quantidade_item') : 1) . '" id="quantidade_item" onChange="quantidade_item_modificada" style="width: 120px;"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px; display: none;">' . form_label('Desconto (%):' . br() . form_input('desconto_item', NULL, 'dojoType="dijit.form.NumberSpinner" constraints="{min: 0, max: 100, places: 2}" displayedValue="' . ($this->input->post('desconto_item') ? number_format($this->input->post('desconto_item'), 2, ',', '') : '0,00') . '" promptMessage="Para decimais use a vírgula. Exemplo: 13,20" id="desconto_item" onChange="desconto_item_modificado" style="width: 120px;"')) . '</p>';
		$conteudo .= '<div style="clear: left;"></div>';
		/*$conteudo .= '
			<table cellspacing="0" class="info">
				<tr id="preco_item_sugerido"></tr>
				<tr id="preco_item_digitado"></tr>
				<tr id="valor_total_item_sem_desconto"></tr>
				<tr id="valor_total_desconto_item"></tr>
				<tr id="valor_total_item_com_desconto"></tr>
			</table>
		';*/
		$conteudo .= '<div style="clear: left;"></div>';
		$conteudo .= '<p>' . form_submit('adicionar_item', 'Adicionar Casco', 'class="adicionar_item"') . '</p>';
		$conteudo .= '</div>';
		
		$itens_adicionados = $this->db->from('itens_pedidos')->where(array('id_pedido' => $id_pedido, 'tipo_produto' => 'casco'))->order_by('id', 'desc')->get()->result();
		$conteudo .= '
			<script type="text/javascript">
				var itens_adicionados = ' . json_encode($itens_adicionados) . ';
				
				$(document).ready(function() {
					if ($("input[name=adicionar_novo_item]").size())
					{
						$("#campos_item").hide();
					}
					
					$("input[name=adicionar_novo_item]").click(function() {
						$(this).parents("p").hide();
						
						$("#campos_item").show();
						
						return false;
					});
					
					$("input[name=adicionar_item]").click(function() {
						var codigo_produto = dijit.byId("codigo_produto").getValue();
						
						var retorno = true;
						
						$.each(itens_adicionados, function(indice, valor) {
							if (valor.codigo == codigo_produto)
							{
								retorno = confirm("O casco " + valor.descricao + " já foi adicionado e seu o preço, quantidade e desconto serão atualizados.\n\nDeseja continuar?");
							}
						});
						
						return retorno;
					});
					
					$(".excluir_item").click(function() {
						if (!confirm("Você deseja excluir este item?"))
						{
							return false;
						}
					});
				});
				
				function codigo_produto_modificado()
				{
					return true;
					
					var descricao_produto = dijit.byId("codigo_produto").getDisplayedValue();
					
					var _descricao_produto = descricao_produto.split(" - ");
					
					// preço do produto (vem do cadastro de produtos)
					// preço do item (o usuário escolhe)
					var preco_produto = _descricao_produto[_descricao_produto.length - 1];
					
					dijit.byId("preco_item").setValue(preco_produto);
					preco_item_modificado();
					
					$("#preco_item_sugerido").html("<strong>Sugerido:</strong> " + preco_produto.replace("R$ ", "")); //$("#preco_item_sugerido").html("<th>Preço sugerido:</th> <td style=\"text-align: right;\">" + preco_produto.replace("R$ ", "") + "</td><br />");
				}
				
				function preco_item_modificado()
				{
					var preco_item = dijit.byId("preco_item").getValue();
					
					$("#preco_item_digitado").html("<th>Preço digitado:</th> <td style=\"text-align: right;\">" + number_format(preco_item, 2, ",", ".") + "</td><br />");
					
					// calcular desconto
					desconto_item_modificado(dijit.byId("desconto_item").getValue());
				}
				
				function quantidade_item_modificada()
				{
					// calcular desconto
					desconto_item_modificado(dijit.byId("desconto_item").getValue());
				}
				
				function desconto_item_modificado()
				{
					var preco_item = dijit.byId("preco_item").getValue();
					var quantidade_item = dijit.byId("quantidade_item").getValue();
					var desconto_item = dijit.byId("desconto_item").getValue();
					
					var valor_total_desconto_item = (preco_item * (desconto_item / 100)) * quantidade_item;
					var valor_total_item_sem_desconto = preco_item * quantidade_item;
					var valor_total_item_com_desconto = valor_total_item_sem_desconto - valor_total_desconto_item;
					
					$("#valor_total_desconto_item").html("<th>Valor do desconto:</th> <td style=\"text-align: right;\">" + number_format(valor_total_desconto_item, 2, ",", ".") + "</td><br />");
					$("#valor_total_item_sem_desconto").html("<th>Total sem desconto:</th> <td style=\"text-align: right;\">" + number_format(valor_total_item_sem_desconto, 2, ",", ".") + "</td><br />");
					$("#valor_total_item_com_desconto").html("<th>Total com desconto:</th> <td style=\"text-align: right;\">" + number_format(valor_total_item_com_desconto, 2, ",", ".") + "</td><br />");
				}
			</script>
		';
		
		// exibir campos para adição de item **
		
		// ** exibir itens adicionados
		
		$conteudo .= heading('Casco Adicionado', 3);
		
		if ($itens_adicionados)
		{
			$item = $this->db->from('itens_pedidos')->where(array('id_pedido' => $id_pedido, 'tipo_produto' => 'casco'))->get()->row();
			$_casco = $this->db->from('cascos')->where('codigo', $item->codigo_real)->get()->row();
			
			$conteudo .= '
				<div id="itens_adicionados"  style="background-color: #EEE; margin-top: 10px; padding: 10px 10px 10px 10px; border-radius: 5px; float: left;">
					' . ($_casco->imagem ? '<img src="' . (base_url() . 'uploads/' . $_casco->imagem) . '" style="border: 4px solid #FFF; float: left; margin-right: 10px;" />' : NULL) . '
					
					<p style="float: left; margin: 0;"><strong>' . $item->codigo_real . ' - ' . $item->descricao . '</strong> ' . anchor('processar_pedido/excluir_item/' . $id_pedido . '/' . $item->id . '/adicionar_casco', '<img src="' . base_url() . 'misc/imagens/cross.png" alt="Excluir Item" />', 'class="excluir_item" title="Excluir Item"') . '</p>
				</div>
			';
			
			/*$conteudo .= '
				<table cellspacing="0" class="normal" id="itens_adicionados">
					<thead>
						<tr>
							<th>Código</th>
							<th>Descrição</th>
							<th>U.M.</th>
							<!--<th>Preço</th>
							<th>Qtd.</th>
							<th>Desc. Tot.</th>
							<th>Tot. S/ Desc.</th>
							<th>Tot. C/ Desc.</th>-->
							<th>&nbsp;</th>
						</tr>
					</thead>
					
					<tbody>
			';
			
			foreach ($itens_adicionados as $item)
			{
				$conteudo .= '
					<tr>
						<td>' . $item->codigo_real . '</td>
						<td>' . $item->descricao . '</td>
						<td>' . $item->unidade_medida . '</td>
						<!--<td>' . number_format($item->preco, 2, ',', '.') . '</td>
						<td>' . number_format($item->quantidade, 2, ',', '.') . '</td>
						<td>' . number_format($item->valor_total_desconto, 2, ',', '.') . '</td>
						<td>' . number_format($item->valor_total_com_ipi, 2, ',', '.') . '</td>
						<td>' . number_format($item->valor_total_com_ipi_e_desconto, 2, ',', '.') . '</td>-->
						<td>' . anchor('processar_pedido/excluir_item/' . $id_pedido . '/' . $item->id . '/adicionar_casco', '<img src="' . base_url() . 'misc/imagens/cross.png" alt="Excluir Item" />', 'class="excluir_item" title="Excluir Item"') . '</td>
					</tr>
				';
				
				$valor_total_desconto += $item->valor_total_desconto;
				$valor_total_com_ipi += $item->valor_total_com_ipi;
				$valor_total_com_ipi_e_desconto += $item->valor_total_com_ipi_e_desconto;
			}
			
			$conteudo .= '
				<!--<tr style="font-weight: bold;">
					<td colspan="5">&nbsp;</td>
					<td>' . number_format($valor_total_desconto, 2, ',', '.') . '</td>
					<td>' . number_format($valor_total_com_ipi, 2, ',', '.') . '</td>
					<td style="color: #060; font-size: 16px;">' . number_format($valor_total_com_ipi_e_desconto, 2, ',', '.') . '</td>
					<td>&nbsp;</td>
				</tr>-->
			';
			
			$conteudo .= '</tbody></table>';*/
		}
		else
		{
			$conteudo .= '<p>Nenhum casco foi adicionado.</p>';
		}
		
		// exibir itens adicionados **
		
		// ** opções do formulário
		
		$conteudo .= '<div style="clear: left;"></div>';
		$conteudo .= '<p>' . form_submit('voltar', 'Voltar', 'class="voltar"') . ' ' . form_submit('continuar', 'Continuar', 'class="continuar"') . '</p>';
		$conteudo .= form_close();
		
		// opções do formulário **
		
		// ** visão geral
		
		$conteudo .= '
			<script type="text/javascript">
				$(document).ready(function() {
					$.get("' . site_url('processar_pedido/obter_visao_geral_ajax/' . $id_pedido) . '", function(dados) {
						$("#conteudo").prepend(dados);
						
						alinhar_conteudo_tabelas();
					});
				});
			</script>
		';
		
		// visão geral **
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function adicionar_motores($id_pedido = NULL)
	{
		$pedido = $this->db->from('pedidos')->where('id', $id_pedido)->get()->row();
		
		$pedido = $this->db->from('pedidos')->where('id', $id_pedido)->get()->row();
		$casco = $this->db->from('itens_pedidos')->where(array('id_pedido' => $id_pedido, 'tipo_produto' => 'casco'))->get()->row();
		
		// ** validar formulário
		
		if ($_POST)
		{
			if ($this->input->post('voltar'))
			{
				redirect('processar_pedido/adicionar_casco/' . $id_pedido);
			}
			else if ($this->input->post('adicionar_item'))
			{
				if (!$this->input->post('codigo_produto'))
				{
					$erro = 'Selecione um motor.';
				}
				else if (!is_numeric($this->input->post('preco_item')))
				{
					$erro = 'Digite um preço válido.';
				}
				else if (!is_numeric($this->input->post('quantidade_item')))
				{
					$erro = 'Digite uma quantidade válida.';
				}
				else if (!is_numeric($this->input->post('desconto_item')))
				{
					$erro = 'Digite um desconto válido.';
				}
				else
				{
					$this->db->delete('itens_pedidos', array('id_pedido' => $id_pedido, 'tipo_produto' => 'motor', 'codigo' => $this->input->post('codigo_produto')));
					
					$produto = $this->db->from('produtos')->where('id', $this->input->post('codigo_produto'))->get()->row_array();
					$unidade_medida = $this->db->from('unidades_medida')->where('id', $produto->id_unidade_medida)->get()->row();
					
					$this->db->insert('itens_pedidos', array(
						'timestamp' => time(),
						'tipo' => 'imediato',
						'id_pedido' => $id_pedido,
						'tipo_produto' => 'motor',
						'codigo' => $produto['id'],
						'codigo_real' => $produto['codigo'],
						'descricao' => $produto['descricao'],
						'unidade_medida' => $unidade_medida->descricao,
						'preco' => $this->input->post('preco_item'),
						'codigo_tabela_precos' => ($produto['preco'] == $this->input->post('preco_produto')) ? 1 : NULL,
						'ipi' => 0,
						'desconto' => $this->input->post('desconto_item'),
						'quantidade' => $this->input->post('quantidade_item')
					));
					
					$this->_calcular_valores_item_pedido($this->db->insert_id());
					
					$sucesso = 'Motor adicionado com sucesso. ' . form_submit('adicionar_novo_item', 'Adicionar Novo Motor', 'class="adicionar_novo_item"');
					
					unset($_POST);
				}
			}
			else if ($this->input->post('continuar'))
			{
				/*if (!$this->db->from('itens_pedidos')->where(array('id_pedido' => $id_pedido, 'tipo_produto' => 'motor'))->get()->row())
				{
					$erro = 'Nenhum motor foi adicionado.';
				}
				else
				{*/
					redirect('processar_pedido/adicionar_acessorios_nff/' . $id_pedido);
				//}
			}
		}
		
		// validar formulário **
		
		// ** exibir campos para adição de item
		
		$conteudo .= heading('Configurando o Seu ' . ($pedido->tipo == 'pedido' ? 'Pedido' : 'Orçamento'), 2);
		
		$conteudo .= $this->_gerar_passo_a_passo($id_pedido, 3);
		
		$conteudo .= heading('Passo 3 - Adicionar Motores', 3);
		
		if ($erro)
		{
			$conteudo .= '<p class="erro">' . $erro . '</p>';
		}
		else if ($sucesso)
		{
			$conteudo .= '<p class="sucesso">' . $sucesso . '</p>';
		}
		
		$conteudo .= form_open(current_url() . '/#itens_adicionados');
		
		$conteudo .= '<div id="campos_item">';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Motor:' . br() . form_dropdown('codigo_produto', $this->_obter_motores($pedido->codigo_usuario, $casco->codigo_real), $this->input->post('codigo_produto'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" ' . ($this->input->post('codigo_produto') ? NULL : 'displayedValue="Selecione um motor..."') . ' id="codigo_produto" onChange="codigo_produto_modificado" intermediateChanges="true" style="width: 400px;"')) . '</p>';
		$conteudo .= '<div style="clear: left;"></div>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Preço:' . br() . form_input('preco_item', NULL, 'dojoType="dijit.form.CurrencyTextBox" currency="R$ " displayedValue="R$ ' . ($this->input->post('preco_item') ? number_format($this->input->post('preco_item'), 2, ',', '') : '0,00') . '" promptMessage="Para centavos use a vírgula. Exemplo: 1423,20" id="preco_item" onChange="preco_item_modificado" style="width: 120px;"')) . '<br /><span id="preco_item_sugerido" style="font-size: 11px;"></span></p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Quantidade:' . br() . form_input('quantidade_item', NULL, 'dojoType="dijit.form.NumberSpinner" constraints="{min: 1}" displayedValue="' . ($this->input->post('quantidade_item') ? $this->input->post('quantidade_item') : 1) . '" id="quantidade_item" onChange="quantidade_item_modificada" style="width: 120px;"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Desconto (%):' . br() . form_input('desconto_item', NULL, 'dojoType="dijit.form.NumberSpinner" constraints="{min: 0, max: 100, places: 2}" displayedValue="' . ($this->input->post('desconto_item') ? number_format($this->input->post('desconto_item'), 2, ',', '') : '0,00') . '" promptMessage="Para decimais use a vírgula. Exemplo: 13,20" id="desconto_item" onChange="desconto_item_modificado" style="width: 120px;"')) . '</p>';
		$conteudo .= '<div style="clear: left;"></div>';
		/*$conteudo .= '
			<table cellspacing="0" class="info">
				<tr id="preco_item_sugerido"></tr>
				<tr id="preco_item_digitado"></tr>
				<tr id="valor_total_item_sem_desconto"></tr>
				<tr id="valor_total_desconto_item"></tr>
				<tr id="valor_total_item_com_desconto"></tr>
			</table>
		';*/
		$conteudo .= '<div style="clear: left;"></div>';
		$conteudo .= '<p>' . form_submit('adicionar_item', 'Adicionar Motor', 'class="adicionar_item"') . '</p>';
		$conteudo .= '</div>';
		
		$itens_adicionados = $this->db->from('itens_pedidos')->where(array('id_pedido' => $id_pedido, 'tipo_produto' => 'motor'))->order_by('id', 'desc')->get()->result();
		$conteudo .= '
			<script type="text/javascript">
				var itens_adicionados = ' . json_encode($itens_adicionados) . ';
				
				$(document).ready(function() {
					if ($("input[name=adicionar_novo_item]").size())
					{
						$("#campos_item").hide();
					}
					
					$("input[name=adicionar_novo_item]").click(function() {
						$(this).parents("p").hide();
						
						$("#campos_item").show();
						
						return false;
					});
					
					$("input[name=adicionar_item]").click(function() {
						var codigo_produto = dijit.byId("codigo_produto").getValue();
						
						var retorno = true;
						
						$.each(itens_adicionados, function(indice, valor) {
							if (valor.codigo == codigo_produto)
							{
								retorno = confirm("O motor " + valor.descricao + " já foi adicionado e seu o preço, quantidade e desconto serão atualizados.\n\nDeseja continuar?");
							}
						});
						
						return retorno;
					});
					
					$(".excluir_item").click(function() {
						if (!confirm("Você deseja excluir este item?"))
						{
							return false;
						}
					});
				});
				
				function codigo_produto_modificado()
				{
					return true;
					
					var descricao_produto = dijit.byId("codigo_produto").getDisplayedValue();
					
					var _descricao_produto = descricao_produto.split(" - ");
					
					// preço do produto (vem do cadastro de produtos)
					// preço do item (o usuário escolhe)
					var preco_produto = _descricao_produto[_descricao_produto.length - 1];
					
					dijit.byId("preco_item").setValue(preco_produto);
					preco_item_modificado();
					
					$("#preco_item_sugerido").html("<strong>Sugerido:</strong> " + preco_produto.replace("R$ ", "")); //$("#preco_item_sugerido").html("<th>Preço sugerido:</th> <td style=\"text-align: right;\">" + preco_produto.replace("R$ ", "") + "</td><br />");
				}
				
				function preco_item_modificado()
				{
					var preco_item = dijit.byId("preco_item").getValue();
					
					$("#preco_item_digitado").html("<th>Preço digitado:</th> <td style=\"text-align: right;\">" + number_format(preco_item, 2, ",", ".") + "</td><br />");
					
					// calcular desconto
					desconto_item_modificado(dijit.byId("desconto_item").getValue());
				}
				
				function quantidade_item_modificada()
				{
					// calcular desconto
					desconto_item_modificado(dijit.byId("desconto_item").getValue());
				}
				
				function desconto_item_modificado()
				{
					var preco_item = dijit.byId("preco_item").getValue();
					var quantidade_item = dijit.byId("quantidade_item").getValue();
					var desconto_item = dijit.byId("desconto_item").getValue();
					
					var valor_total_desconto_item = (preco_item * (desconto_item / 100)) * quantidade_item;
					var valor_total_item_sem_desconto = preco_item * quantidade_item;
					var valor_total_item_com_desconto = valor_total_item_sem_desconto - valor_total_desconto_item;
					
					$("#valor_total_desconto_item").html("<th>Valor do desconto:</th> <td style=\"text-align: right;\">" + number_format(valor_total_desconto_item, 2, ",", ".") + "</td><br />");
					$("#valor_total_item_sem_desconto").html("<th>Total sem desconto:</th> <td style=\"text-align: right;\">" + number_format(valor_total_item_sem_desconto, 2, ",", ".") + "</td><br />");
					$("#valor_total_item_com_desconto").html("<th>Total com desconto:</th> <td style=\"text-align: right;\">" + number_format(valor_total_item_com_desconto, 2, ",", ".") + "</td><br />");
				}
			</script>
		';
		
		// exibir campos para adição de item **
		
		// ** exibir itens adicionados
		
		$conteudo .= heading('Motores Adicionados', 3);
		
		if ($itens_adicionados)
		{
			$conteudo .= '
				<table cellspacing="0" class="normal" id="itens_adicionados">
					<thead>
						<tr>
							<th>Código</th>
							<th>Descrição</th>
							<th>U.M.</th>
							<th>Preço</th>
							<th>Qtd.</th>
							<th>Desc. Tot.</th>
							<th>Tot. S/ Desc.</th>
							<th>Tot. C/ Desc.</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					
					<tbody>
			';
			
			foreach ($itens_adicionados as $item)
			{
				$conteudo .= '
					<tr>
						<td>' . $item->codigo_real . '</td>
						<td>' . $item->descricao . '</td>
						<td>' . $item->unidade_medida . '</td>
						<td>' . number_format($item->preco, 2, ',', '.') . '</td>
						<td>' . number_format($item->quantidade, 2, ',', '.') . '</td>
						<td>' . number_format($item->valor_total_desconto, 2, ',', '.') . '</td>
						<td>' . number_format($item->valor_total_com_ipi, 2, ',', '.') . '</td>
						<td>' . number_format($item->valor_total_com_ipi_e_desconto, 2, ',', '.') . '</td>
						<td>' . anchor('processar_pedido/excluir_item/' . $id_pedido . '/' . $item->id . '/adicionar_motores', '<img src="' . base_url() . 'misc/imagens/cross.png" alt="Excluir Item" />', 'class="excluir_item" title="Excluir Item"') . '</td>
					</tr>
				';
				
				$valor_total_desconto += $item->valor_total_desconto;
				$valor_total_com_ipi += $item->valor_total_com_ipi;
				$valor_total_com_ipi_e_desconto += $item->valor_total_com_ipi_e_desconto;
			}
			
			$conteudo .= '
				<tr style="font-weight: bold;">
					<td colspan="5">&nbsp;</td>
					<td>' . number_format($valor_total_desconto, 2, ',', '.') . '</td>
					<td>' . number_format($valor_total_com_ipi, 2, ',', '.') . '</td>
					<td style="color: #060; font-size: 16px;">' . number_format($valor_total_com_ipi_e_desconto, 2, ',', '.') . '</td>
					<td>&nbsp;</td>
				</tr>
			';
			
			$conteudo .= '</tbody></table>';
		}
		else
		{
			$conteudo .= '<p>Nenhum motor foi adicionado.</p>';
		}
		
		// exibir itens adicionados **
		
		// ** opções do formulário
		
		$conteudo .= '<div style="clear: left;"></div>';
		$conteudo .= '<p>' . form_submit('voltar', 'Voltar', 'class="voltar"') . ' ' . form_submit('continuar', 'Continuar', 'class="continuar"') . '</p>';
		$conteudo .= form_close();
		
		// opções do formulário **
		
		// ** visão geral
		
		$conteudo .= '
			<script type="text/javascript">
				$(document).ready(function() {
					$.get("' . site_url('processar_pedido/obter_visao_geral_ajax/' . $id_pedido) . '", function(dados) {
						$("#conteudo").prepend(dados);
						
						alinhar_conteudo_tabelas();
					});
				});
			</script>
		';
		
		// visão geral **
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function adicionar_acessorios_ff($id_pedido = NULL)
	{
		$pedido = $this->db->from('pedidos')->where('id', $id_pedido)->get()->row();
		
		$casco = $this->db->from('itens_pedidos')->where(array('id_pedido' => $id_pedido, 'tipo_produto' => 'casco'))->get()->row();
		
		// ** validar formulário
		
		if ($_POST)
		{
			if ($this->input->post('voltar'))
			{
				redirect('processar_pedido/adicionar_motores/' . $id_pedido);
			}
			else if ($this->input->post('adicionar_item'))
			{
				if (!$this->input->post('codigo_produto'))
				{
					$erro = 'Selecione um acessório.';
				}
				else if (!is_numeric($this->input->post('preco_item')))
				{
					$erro = 'Digite um preço válido.';
				}
				else if (!is_numeric($this->input->post('quantidade_item')))
				{
					$erro = 'Digite uma quantidade válida.';
				}
				else if (!is_numeric($this->input->post('desconto_item')))
				{
					$erro = 'Digite um desconto válido.';
				}
				else
				{
					$this->db->delete('itens_pedidos', array('id_pedido' => $id_pedido, 'tipo_produto' => 'acessorio_ff', 'codigo' => $this->input->post('codigo_produto')));
					
					$produto = $this->db_cliente->obter_opcional_produto($this->input->post('codigo_produto'));
					
					$this->db->insert('itens_pedidos', array(
						'timestamp' => time(),
						'tipo' => 'imediato',
						'id_pedido' => $id_pedido,
						'tipo_produto' => 'acessorio_ff',
						'codigo' => $produto['id'],
						'codigo_real' => $produto['codigo'],
						'descricao' => $produto['descricao'],
						'unidade_medida' => NULL,
						'preco' => $this->input->post('preco_item'),
						'codigo_tabela_precos' => ($produto['preco'] == $this->input->post('preco_produto')) ? 1 : NULL,
						'ipi' => 0,
						'desconto' => $this->input->post('desconto_item'),
						'quantidade' => $this->input->post('quantidade_item')
					));
					
					$this->_calcular_valores_item_pedido($this->db->insert_id());
					
					$sucesso = 'Acessório adicionado com sucesso. ' . form_submit('adicionar_novo_item', 'Adicionar Novo Acessório', 'class="adicionar_novo_item"');
					
					unset($_POST);
				}
			}
			else if ($this->input->post('continuar'))
			{
				redirect('processar_pedido/adicionar_acessorios_nff/' . $id_pedido);
			}
		}
		
		// validar formulário **
		
		// ** exibir campos para adição de item
		
		$conteudo .= heading('Configurando o Seu ' . ($pedido->tipo == 'pedido' ? 'Pedido' : 'Orçamento'), 2);
		
		$conteudo .= $this->_gerar_passo_a_passo($id_pedido, 4);
		
		$conteudo .= heading('Passo 4 - Adicionar Acessórios Fibrafort', 3);
		
		if ($erro)
		{
			$conteudo .= '<p class="erro">' . $erro . '</p>';
		}
		else if ($sucesso)
		{
			$conteudo .= '<p class="sucesso">' . $sucesso . '</p>';
		}
		
		$conteudo .= form_open(current_url() . '/#itens_adicionados');
		
		$conteudo .= '<div id="campos_item">';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Acessório:' . br() . form_dropdown('codigo_produto', $this->_obter_acessorios_ff($casco->codigo_real), $this->input->post('codigo_produto'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" ' . ($this->input->post('codigo_produto') ? NULL : 'displayedValue="Selecione um acessório..."') . ' id="codigo_produto" onChange="codigo_produto_modificado" intermediateChanges="true" style="width: 400px;"')) . '</p>';
		$conteudo .= '<div style="clear: left;"></div>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Preço:' . br() . form_input('preco_item', NULL, 'dojoType="dijit.form.CurrencyTextBox" currency="R$ " displayedValue="R$ ' . ($this->input->post('preco_item') ? number_format($this->input->post('preco_item'), 2, ',', '') : '0,00') . '" promptMessage="Para centavos use a vírgula. Exemplo: 1423,20" id="preco_item" onChange="preco_item_modificado" style="width: 120px;"')) . '<br /><span id="preco_item_sugerido" style="font-size: 11px;"></span></p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Quantidade:' . br() . form_input('quantidade_item', NULL, 'dojoType="dijit.form.NumberSpinner" constraints="{min: 1}" displayedValue="' . ($this->input->post('quantidade_item') ? $this->input->post('quantidade_item') : 1) . '" id="quantidade_item" onChange="quantidade_item_modificada" style="width: 120px;"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Desconto (%):' . br() . form_input('desconto_item', NULL, 'dojoType="dijit.form.NumberSpinner" constraints="{min: 0, max: 100, places: 2}" displayedValue="' . ($this->input->post('desconto_item') ? number_format($this->input->post('desconto_item'), 2, ',', '') : '0,00') . '" promptMessage="Para decimais use a vírgula. Exemplo: 13,20" id="desconto_item" onChange="desconto_item_modificado" style="width: 120px;"')) . '</p>';
		$conteudo .= '<div style="clear: left;"></div>';
		/*$conteudo .= '
			<table cellspacing="0" class="info">
				<tr id="preco_item_sugerido"></tr>
				<tr id="preco_item_digitado"></tr>
				<tr id="valor_total_item_sem_desconto"></tr>
				<tr id="valor_total_desconto_item"></tr>
				<tr id="valor_total_item_com_desconto"></tr>
			</table>
		';*/
		$conteudo .= '<div style="clear: left;"></div>';
		$conteudo .= '<p>' . form_submit('adicionar_item', 'Adicionar Acessório', 'class="adicionar_item"') . '</p>';
		$conteudo .= '</div>';
		
		$itens_adicionados = $this->db->from('itens_pedidos')->where(array('id_pedido' => $id_pedido, 'tipo_produto' => 'acessorio_ff'))->order_by('id', 'desc')->get()->result();
		$conteudo .= '
			<script type="text/javascript">
				var itens_adicionados = ' . json_encode($itens_adicionados) . ';
				
				$(document).ready(function() {
					if ($("input[name=adicionar_novo_item]").size())
					{
						$("#campos_item").hide();
					}
					
					$("input[name=adicionar_novo_item]").click(function() {
						$(this).parents("p").hide();
						
						$("#campos_item").show();
						
						return false;
					});
					
					$("input[name=adicionar_item]").click(function() {
						var codigo_produto = dijit.byId("codigo_produto").getValue();
						
						var retorno = true;
						
						$.each(itens_adicionados, function(indice, valor) {
							if (valor.codigo == codigo_produto)
							{
								retorno = confirm("O acessório " + valor.descricao + " já foi adicionado e seu o preço, quantidade e desconto serão atualizados.\n\nDeseja continuar?");
							}
						});
						
						return retorno;
					});
					
					$(".excluir_item").click(function() {
						if (!confirm("Você deseja excluir este item?"))
						{
							return false;
						}
					});
				});
				
				function codigo_produto_modificado()
				{
					var descricao_produto = dijit.byId("codigo_produto").getDisplayedValue();
					
					var _descricao_produto = descricao_produto.split(" - ");
					
					// preço do produto (vem do cadastro de produtos)
					// preço do item (o usuário escolhe)
					var preco_produto = _descricao_produto[_descricao_produto.length - 1];
					
					dijit.byId("preco_item").setValue(preco_produto);
					preco_item_modificado();
					
					$("#preco_item_sugerido").html("<strong>Sugerido:</strong> " + preco_produto.replace("R$ ", "")); //$("#preco_item_sugerido").html("<th>Preço sugerido:</th> <td style=\"text-align: right;\">" + preco_produto.replace("R$ ", "") + "</td><br />");
				}
				
				function preco_item_modificado()
				{
					var preco_item = dijit.byId("preco_item").getValue();
					
					$("#preco_item_digitado").html("<th>Preço digitado:</th> <td style=\"text-align: right;\">" + number_format(preco_item, 2, ",", ".") + "</td><br />");
					
					// calcular desconto
					desconto_item_modificado(dijit.byId("desconto_item").getValue());
				}
				
				function quantidade_item_modificada()
				{
					// calcular desconto
					desconto_item_modificado(dijit.byId("desconto_item").getValue());
				}
				
				function desconto_item_modificado()
				{
					var preco_item = dijit.byId("preco_item").getValue();
					var quantidade_item = dijit.byId("quantidade_item").getValue();
					var desconto_item = dijit.byId("desconto_item").getValue();
					
					var valor_total_desconto_item = (preco_item * (desconto_item / 100)) * quantidade_item;
					var valor_total_item_sem_desconto = preco_item * quantidade_item;
					var valor_total_item_com_desconto = valor_total_item_sem_desconto - valor_total_desconto_item;
					
					$("#valor_total_desconto_item").html("<th>Valor do desconto:</th> <td style=\"text-align: right;\">" + number_format(valor_total_desconto_item, 2, ",", ".") + "</td><br />");
					$("#valor_total_item_sem_desconto").html("<th>Total sem desconto:</th> <td style=\"text-align: right;\">" + number_format(valor_total_item_sem_desconto, 2, ",", ".") + "</td><br />");
					$("#valor_total_item_com_desconto").html("<th>Total com desconto:</th> <td style=\"text-align: right;\">" + number_format(valor_total_item_com_desconto, 2, ",", ".") + "</td><br />");
				}
			</script>
		';
		
		// exibir campos para adição de item **
		
		// ** exibir itens adicionados
		
		$conteudo .= heading('Acessórios Fibrafort Adicionados', 3);
		
		if ($itens_adicionados)
		{
			$conteudo .= '
				<table cellspacing="0" class="normal" id="itens_adicionados">
					<thead>
						<tr>
							<th>Código</th>
							<th>Descrição</th>
							<th>U.M.</th>
							<th>Preço</th>
							<th>Qtd.</th>
							<th>Desc. Tot.</th>
							<th>Tot. S/ Desc.</th>
							<th>Tot. C/ Desc.</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					
					<tbody>
			';
			
			foreach ($itens_adicionados as $item)
			{
				$conteudo .= '
					<tr>
						<td>' . $item->codigo_real . '</td>
						<td>' . $item->descricao . '</td>
						<td>' . $item->unidade_medida . '</td>
						<td>' . number_format($item->preco, 2, ',', '.') . '</td>
						<td>' . number_format($item->quantidade, 2, ',', '.') . '</td>
						<td>' . number_format($item->valor_total_desconto, 2, ',', '.') . '</td>
						<td>' . number_format($item->valor_total_com_ipi, 2, ',', '.') . '</td>
						<td>' . number_format($item->valor_total_com_ipi_e_desconto, 2, ',', '.') . '</td>
						<td>' . anchor('processar_pedido/excluir_item/' . $id_pedido . '/' . $item->id . '/adicionar_acessorios_ff', '<img src="' . base_url() . 'misc/imagens/cross.png" alt="Excluir Item" />', 'class="excluir_item" title="Excluir Item"') . '</td>
					</tr>
				';
				
				$valor_total_desconto += $item->valor_total_desconto;
				$valor_total_com_ipi += $item->valor_total_com_ipi;
				$valor_total_com_ipi_e_desconto += $item->valor_total_com_ipi_e_desconto;
			}
			
			$conteudo .= '
				<tr style="font-weight: bold;">
					<td colspan="5">&nbsp;</td>
					<td>' . number_format($valor_total_desconto, 2, ',', '.') . '</td>
					<td>' . number_format($valor_total_com_ipi, 2, ',', '.') . '</td>
					<td style="color: #060; font-size: 16px;">' . number_format($valor_total_com_ipi_e_desconto, 2, ',', '.') . '</td>
					<td>&nbsp;</td>
				</tr>
			';
			
			$conteudo .= '</tbody></table>';
		}
		else
		{
			$conteudo .= '<p>Nenhum acessório foi adicionado.</p>';
		}
		
		// exibir itens adicionados **
		
		// ** opções do formulário
		
		$conteudo .= '<div style="clear: left;"></div>';
		$conteudo .= '<p>' . form_submit('voltar', 'Voltar', 'class="voltar"') . ' ' . form_submit('continuar', 'Continuar', 'class="continuar"') . '</p>';
		$conteudo .= form_close();
		
		// opções do formulário **
		
		// ** visão geral
		
		$conteudo .= '
			<script type="text/javascript">
				$(document).ready(function() {
					$.get("' . site_url('processar_pedido/obter_visao_geral_ajax/' . $id_pedido) . '", function(dados) {
						$("#conteudo").prepend(dados);
						
						alinhar_conteudo_tabelas();
					});
				});
			</script>
		';
		
		// visão geral **
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function adicionar_acessorios_nff($id_pedido = NULL)
	{
		$pedido = $this->db->from('pedidos')->where('id', $id_pedido)->get()->row();
		
		$pedido = $this->db->from('pedidos')->where('id', $id_pedido)->get()->row();
		$casco = $this->db->from('itens_pedidos')->where(array('id_pedido' => $id_pedido, 'tipo_produto' => 'casco'))->get()->row();
		
		// ** validar formulário
		
		if ($_POST)
		{
			if ($this->input->post('voltar'))
			{
				redirect('processar_pedido/adicionar_motores/' . $id_pedido);
			}
			else if ($this->input->post('adicionar_item'))
			{
				if (!$this->input->post('codigo_produto'))
				{
					$erro = 'Selecione um acessório.';
				}
				else if (!is_numeric($this->input->post('preco_item')))
				{
					$erro = 'Digite um preço válido.';
				}
				else if (!is_numeric($this->input->post('quantidade_item')))
				{
					$erro = 'Digite uma quantidade válida.';
				}
				else if (!is_numeric($this->input->post('desconto_item')))
				{
					$erro = 'Digite um desconto válido.';
				}
				else
				{
					$this->db->delete('itens_pedidos', array('id_pedido' => $id_pedido, 'tipo_produto' => 'acessorio_nff', 'codigo' => $this->input->post('codigo_produto')));
					
					$produto = $this->db->from('produtos')->where('id', $this->input->post('codigo_produto'))->get()->row_array();
					$unidade_medida = $this->db->from('unidades_medida')->where('id', $produto->id_unidade_medida)->get()->row();
					
					$this->db->insert('itens_pedidos', array(
						'timestamp' => time(),
						'tipo' => 'imediato',
						'id_pedido' => $id_pedido,
						'tipo_produto' => 'acessorio_nff',
						'codigo' => $produto['id'],
						'codigo_real' => $produto['codigo'],
						'descricao' => $produto['descricao'],
						'unidade_medida' => $unidade_medida->descricao,
						'preco' => $this->input->post('preco_item'),
						'codigo_tabela_precos' => ($produto['preco'] == $this->input->post('preco_produto')) ? 1 : NULL,
						'ipi' => 0,
						'desconto' => $this->input->post('desconto_item'),
						'quantidade' => $this->input->post('quantidade_item')
					));
					
					$this->_calcular_valores_item_pedido($this->db->insert_id());
					
					$sucesso = 'Acessório adicionado com sucesso. ' . form_submit('adicionar_novo_item', 'Adicionar Novo Acessório', 'class="adicionar_novo_item"');
					
					unset($_POST);
				}
			}
			else if ($this->input->post('continuar'))
			{
				redirect('processar_pedido/visao_geral/' . $id_pedido);
			}
		}
		
		// validar formulário **
		
		// ** exibir campos para adição de item
		
		$conteudo .= heading('Configurando o Seu ' . ($pedido->tipo == 'pedido' ? 'Pedido' : 'Orçamento'), 2);
		
		$conteudo .= $this->_gerar_passo_a_passo($id_pedido, 4);
		
		$conteudo .= heading('Passo 4 - Adicionar Acessórios Não Fibrafort', 3);
		
		if ($erro)
		{
			$conteudo .= '<p class="erro">' . $erro . '</p>';
		}
		else if ($sucesso)
		{
			$conteudo .= '<p class="sucesso">' . $sucesso . '</p>';
		}
		
		$conteudo .= form_open(current_url() . '/#itens_adicionados');
		
		$conteudo .= '<div id="campos_item">';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Acessório:' . br() . form_dropdown('codigo_produto', $this->_obter_acessorios_nff($pedido->codigo_usuario, $casco->codigo_real), $this->input->post('codigo_produto'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" ' . ($this->input->post('codigo_produto') ? NULL : 'displayedValue="Selecione um acessório..."') . ' id="codigo_produto" onChange="codigo_produto_modificado" intermediateChanges="true" style="width: 400px;"')) . '</p>';
		$conteudo .= '<div style="clear: left;"></div>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Preço:' . br() . form_input('preco_item', NULL, 'dojoType="dijit.form.CurrencyTextBox" currency="R$ " displayedValue="R$ ' . ($this->input->post('preco_item') ? number_format($this->input->post('preco_item'), 2, ',', '') : '0,00') . '" promptMessage="Para centavos use a vírgula. Exemplo: 1423,20" id="preco_item" onChange="preco_item_modificado" style="width: 120px;"')) . '<br /><span id="preco_item_sugerido" style="font-size: 11px;"></span></p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Quantidade:' . br() . form_input('quantidade_item', NULL, 'dojoType="dijit.form.NumberSpinner" constraints="{min: 1}" displayedValue="' . ($this->input->post('quantidade_item') ? $this->input->post('quantidade_item') : 1) . '" id="quantidade_item" onChange="quantidade_item_modificada" style="width: 120px;"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Desconto (%):' . br() . form_input('desconto_item', NULL, 'dojoType="dijit.form.NumberSpinner" constraints="{min: 0, max: 100, places: 2}" displayedValue="' . ($this->input->post('desconto_item') ? number_format($this->input->post('desconto_item'), 2, ',', '') : '0,00') . '" promptMessage="Para decimais use a vírgula. Exemplo: 13,20" id="desconto_item" onChange="desconto_item_modificado" style="width: 120px;"')) . '</p>';
		$conteudo .= '<div style="clear: left;"></div>';
		/*$conteudo .= '
			<table cellspacing="0" class="info">
				<tr id="preco_item_sugerido"></tr>
				<tr id="preco_item_digitado"></tr>
				<tr id="valor_total_item_sem_desconto"></tr>
				<tr id="valor_total_desconto_item"></tr>
				<tr id="valor_total_item_com_desconto"></tr>
			</table>
		';*/
		$conteudo .= '<div style="clear: left;"></div>';
		$conteudo .= '<p>' . form_submit('adicionar_item', 'Adicionar Acessório', 'class="adicionar_item"') . '</p>';
		$conteudo .= '</div>';
		
		$itens_adicionados = $this->db->from('itens_pedidos')->where(array('id_pedido' => $id_pedido, 'tipo_produto' => 'acessorio_nff'))->order_by('id', 'desc')->get()->result();
		$conteudo .= '
			<script type="text/javascript">
				var itens_adicionados = ' . json_encode($itens_adicionados) . ';
				
				$(document).ready(function() {
					if ($("input[name=adicionar_novo_item]").size())
					{
						$("#campos_item").hide();
					}
					
					$("input[name=adicionar_novo_item]").click(function() {
						$(this).parents("p").hide();
						
						$("#campos_item").show();
						
						return false;
					});
					
					$("input[name=adicionar_item]").click(function() {
						var codigo_produto = dijit.byId("codigo_produto").getValue();
						
						var retorno = true;
						
						$.each(itens_adicionados, function(indice, valor) {
							if (valor.codigo == codigo_produto)
							{
								retorno = confirm("O acessório " + valor.descricao + " já foi adicionado e seu o preço, quantidade e desconto serão atualizados.\n\nDeseja continuar?");
							}
						});
						
						return retorno;
					});
					
					$(".excluir_item").click(function() {
						if (!confirm("Você deseja excluir este item?"))
						{
							return false;
						}
					});
				});
				
				function codigo_produto_modificado()
				{
					var descricao_produto = dijit.byId("codigo_produto").getDisplayedValue();
					
					var _descricao_produto = descricao_produto.split(" - ");
					
					// preço do produto (vem do cadastro de produtos)
					// preço do item (o usuário escolhe)
					var preco_produto = _descricao_produto[_descricao_produto.length - 1];
					
					dijit.byId("preco_item").setValue(preco_produto);
					preco_item_modificado();
					
					$("#preco_item_sugerido").html("<strong>Sugerido:</strong> " + preco_produto.replace("R$ ", "")); //$("#preco_item_sugerido").html("<th>Preço sugerido:</th> <td style=\"text-align: right;\">" + preco_produto.replace("R$ ", "") + "</td><br />");
				}
				
				function preco_item_modificado()
				{
					var preco_item = dijit.byId("preco_item").getValue();
					
					$("#preco_item_digitado").html("<th>Preço digitado:</th> <td style=\"text-align: right;\">" + number_format(preco_item, 2, ",", ".") + "</td><br />");
					
					// calcular desconto
					desconto_item_modificado(dijit.byId("desconto_item").getValue());
				}
				
				function quantidade_item_modificada()
				{
					// calcular desconto
					desconto_item_modificado(dijit.byId("desconto_item").getValue());
				}
				
				function desconto_item_modificado()
				{
					var preco_item = dijit.byId("preco_item").getValue();
					var quantidade_item = dijit.byId("quantidade_item").getValue();
					var desconto_item = dijit.byId("desconto_item").getValue();
					
					var valor_total_desconto_item = (preco_item * (desconto_item / 100)) * quantidade_item;
					var valor_total_item_sem_desconto = preco_item * quantidade_item;
					var valor_total_item_com_desconto = valor_total_item_sem_desconto - valor_total_desconto_item;
					
					$("#valor_total_desconto_item").html("<th>Valor do desconto:</th> <td style=\"text-align: right;\">" + number_format(valor_total_desconto_item, 2, ",", ".") + "</td><br />");
					$("#valor_total_item_sem_desconto").html("<th>Total sem desconto:</th> <td style=\"text-align: right;\">" + number_format(valor_total_item_sem_desconto, 2, ",", ".") + "</td><br />");
					$("#valor_total_item_com_desconto").html("<th>Total com desconto:</th> <td style=\"text-align: right;\">" + number_format(valor_total_item_com_desconto, 2, ",", ".") + "</td><br />");
				}
			</script>
		';
		
		// exibir campos para adição de item **
		
		// ** exibir itens adicionados
		
		$conteudo .= heading('Acessórios Não Fibrafort Adicionados', 3);
		
		if ($itens_adicionados)
		{
			$conteudo .= '
				<table cellspacing="0" class="normal" id="itens_adicionados">
					<thead>
						<tr>
							<th>Código</th>
							<th>Descrição</th>
							<th>U.M.</th>
							<th>Preço</th>
							<th>Qtd.</th>
							<th>Desc. Tot.</th>
							<th>Tot. S/ Desc.</th>
							<th>Tot. C/ Desc.</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					
					<tbody>
			';
			
			foreach ($itens_adicionados as $item)
			{
				$conteudo .= '
					<tr>
						<td>' . $item->codigo_real . '</td>
						<td>' . $item->descricao . '</td>
						<td>' . $item->unidade_medida . '</td>
						<td>' . number_format($item->preco, 2, ',', '.') . '</td>
						<td>' . number_format($item->quantidade, 2, ',', '.') . '</td>
						<td>' . number_format($item->valor_total_desconto, 2, ',', '.') . '</td>
						<td>' . number_format($item->valor_total_com_ipi, 2, ',', '.') . '</td>
						<td>' . number_format($item->valor_total_com_ipi_e_desconto, 2, ',', '.') . '</td>
						<td>' . anchor('processar_pedido/excluir_item/' . $id_pedido . '/' . $item->id . '/adicionar_acessorios_nff', '<img src="' . base_url() . 'misc/imagens/cross.png" alt="Excluir Item" />', 'class="excluir_item" title="Excluir Item"') . '</td>
					</tr>
				';
				
				$valor_total_desconto += $item->valor_total_desconto;
				$valor_total_com_ipi += $item->valor_total_com_ipi;
				$valor_total_com_ipi_e_desconto += $item->valor_total_com_ipi_e_desconto;
			}
			
			$conteudo .= '
				<tr style="font-weight: bold;">
					<td colspan="5">&nbsp;</td>
					<td>' . number_format($valor_total_desconto, 2, ',', '.') . '</td>
					<td>' . number_format($valor_total_com_ipi, 2, ',', '.') . '</td>
					<td style="color: #060; font-size: 16px;">' . number_format($valor_total_com_ipi_e_desconto, 2, ',', '.') . '</td>
					<td>&nbsp;</td>
				</tr>
			';
			
			$conteudo .= '</tbody></table>';
		}
		else
		{
			$conteudo .= '<p>Nenhum acessório foi adicionado.</p>';
		}
		
		// exibir itens adicionados **
		
		// ** opções do formulário
		
		$conteudo .= '<div style="clear: left;"></div>';
		$conteudo .= '<p>' . form_submit('voltar', 'Voltar', 'class="voltar"') . ' ' . form_submit('continuar', 'Continuar', 'class="continuar"') . '</p>';
		$conteudo .= form_close();
		
		// opções do formulário **
		
		// ** visão geral
		
		$conteudo .= '
			<script type="text/javascript">
				$(document).ready(function() {
					$.get("' . site_url('processar_pedido/obter_visao_geral_ajax/' . $id_pedido) . '", function(dados) {
						$("#conteudo").prepend(dados);
						
						alinhar_conteudo_tabelas();
					});
				});
			</script>
		';
		
		// visão geral **
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function visao_geral($id_pedido = NULL)
	{
		$pedido = $this->db->from('pedidos')->where('id', $id_pedido)->get()->row();
		
		if ($_POST)
		{
			if ($this->input->post('voltar'))
			{
				redirect('processar_pedido/adicionar_acessorios_nff/' . $id_pedido);
			}
			else if ($this->input->post('continuar'))
			{
				$this->db->update('pedidos', array(
					'timestamp' => time(),
					'status' => 'concluido',
					'condicao_pagamento' => $this->input->post('condicao_pagamento'),
					'prazo_entrega' => $this->input->post('prazo_entrega'),
					'frete' => $this->input->post('frete'),
					'valor_frete' => $this->input->post('valor_frete')
				), array('id' => $id_pedido));
				
				redirect('processar_pedido/gerar_proposta/' . $id_pedido);
			}
		}
		else
		{
			$_POST = array(
				'condicao_pagamento' => $pedido->condicao_pagamento,
				'prazo_entrega' => $pedido->prazo_entrega,
				'frete' => $pedido->frete,
				'valor_frete' => $pedido->valor_frete
			);
		}
		
		$conteudo .= heading('Configurando o Seu ' . ($pedido->tipo == 'pedido' ? 'Pedido' : 'Orçamento'), 2);
		
		$conteudo .= $this->_gerar_passo_a_passo($id_pedido, 5);
		
		$conteudo .= heading('Passo 5 - Visão Geral', 3);
		
		$conteudo .= form_open(current_url());
		
		$conteudo .= '<div id="visao_geral" style="float: left; margin-right: 20px;"></div>';
		
		$conteudo .= '<div style="background-color: #EEE; float: left; overflow: hidden; margin-top: 10px; padding: 10px; -webkit-border-radius: 5px;">';
		$conteudo .= '<h3 style="margin: 0;">Informações Complementares</h3>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Condição de pagamento:' . br() . form_textarea(array('name' => 'condicao_pagamento', 'value' => $this->input->post('condicao_pagamento'), 'dojoType' => 'dijit.form.Textarea', 'id' => 'condicao_pagamento', 'style' => 'height: 100px; width: 400px;'))) . '<div dojoType="dijit.Tooltip" connectId="condicao_pagamento" position="above">O campo se expande automaticamente.</div></p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Prazo de entrega:' . br() . form_textarea(array('name' => 'prazo_entrega', 'value' => $this->input->post('prazo_entrega'), 'dojoType' => 'dijit.form.Textarea', 'id' => 'prazo_entrega', 'style' => 'height: 100px; width: 400px;'))) . '<div dojoType="dijit.Tooltip" connectId="prazo_entrega" position="above">O campo se expande automaticamente.</div></p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Frete:' . br() . form_textarea(array('name' => 'frete', 'value' => $this->input->post('frete'), 'dojoType' => 'dijit.form.Textarea', 'id' => 'frete', 'style' => 'height: 100px; width: 400px;'))) . '<div dojoType="dijit.Tooltip" connectId="frete" position="above">O campo se expande automaticamente.</div></p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Valor do frete:' . br() . form_input('valor_frete', NULL, 'dojoType="dijit.form.CurrencyTextBox" currency="R$ " displayedValue="R$ ' . ($this->input->post('valor_frete') ? number_format($this->input->post('valor_frete'), 2, ',', '') : '0,00') . '" promptMessage="Para centavos use a vírgula. Exemplo: 1423,20" id="valor_frete" style="width: 120px;"')) . '</p>';
		$conteudo .= '</div>';
		
		$conteudo .= '<div style="clear: left;"></div>';
		
		$conteudo .= heading('Estas informações estão corretas?', 3);
		
		$conteudo .= '<p>' . form_submit('voltar', 'Não, voltar para corrigir', 'class="voltar"') . ' ' . form_submit('continuar', 'Sim, gerar proposta', 'class="continuar"') . '</p>';
		$conteudo .= form_close();
		
		$conteudo .= '
			<script type="text/javascript">
				$(document).ready(function() {
					$.get("' . site_url('processar_pedido/obter_visao_geral_ajax/' . $id_pedido . '/complexa') . '", function(dados) {
						$("#visao_geral").html(dados);
						
						alinhar_conteudo_tabelas();
					});
				});
			</script>
		';
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function obter_visao_geral_ajax($id_pedido = NULL, $tipo = 'simples')
	{
		$pedido = $this->db->from('pedidos')->where('id', $id_pedido)->get()->row();
		
		if ($pedido->codigo_cliente)
		{
			$cliente = $this->db_cliente->obter_cliente($pedido->codigo_cliente, $pedido->loja_cliente);
		}
		else if ($pedido->id_prospect)
		{
			$prospect = $this->db->from('prospects')->where('id', $pedido->id_prospect)->get()->row();
		}
		
		$casco = $this->db->from('itens_pedidos')->where(array('id_pedido' => $id_pedido, 'tipo_produto' => 'casco'))->get()->row();
		
		$valor_total += $casco->valor_total_com_ipi_e_desconto;
		
		$motores = $this->db->from('itens_pedidos')->where(array('id_pedido' => $id_pedido, 'tipo_produto' => 'motor'))->order_by('id', 'desc')->get()->result();
		if ($motores)
		{
			foreach ($motores as $indice => $motor)
			{
				$_motores .= '
					<tr>
						<th>Motor' . (count($motores) > 1 ? $indice + 1 : NULL) . ':</th>
						<td>' . $motor->codigo_real . ' - ' . $motor->descricao . '</td>
						<td style="text-align: right;">' . number_format($motor->valor_total_com_ipi_e_desconto, 2, ',', '.') . '</td>
					</tr>
				';
				
				$valor_total += $motor->valor_total_com_ipi_e_desconto;
			}
		}
		else
		{
			$_motores = '
				<tr>
					<th>Motor:</th>
					<td>Nenhum adicionado.</td>
					<td>0,00</td>
				</tr>
			';
		}
		
		$acessorios_ff = $this->db->from('itens_pedidos')->where(array('id_pedido' => $id_pedido, 'tipo_produto' => 'acessorio_ff'))->order_by('id', 'desc')->get()->result();
		if ($acessorios_ff)
		{
			foreach ($acessorios_ff as $acessorio)
			{
				$_acessorios_ff .= '
					<tr>
						<td colspan="2" style="border-left: 1px solid #DDD;">' . $acessorio->codigo_real . ' - ' . $acessorio->descricao . '</td>
						<td style="text-align: right;">' . number_format($acessorio->valor_total_com_ipi_e_desconto, 2, ',', '.') . '</td>
					</tr>
				';
				
				$valor_total += $acessorio->valor_total_com_ipi_e_desconto;
			}
		}
		else
		{
			$_acessorios_ff = '<tr><td colspan="3" style="border-left: 1px solid #DDD;">Nenhum adicionado.</td></tr>';
		}
		
		$acessorios_nff = $this->db->from('itens_pedidos')->where(array('id_pedido' => $id_pedido, 'tipo_produto' => 'acessorio_nff'))->order_by('id', 'desc')->get()->result();
		if ($acessorios_nff)
		{
			foreach ($acessorios_nff as $acessorio)
			{
				$_acessorios_nff .= '
					<tr>
						<td colspan="2" style="border-left: 1px solid #DDD;">' . $acessorio->codigo_real . ' - ' . $acessorio->descricao . '</td>
						<td style="text-align: right;">' . number_format($acessorio->valor_total_com_ipi_e_desconto, 2, ',', '.') . '</td>
					</tr>
				';
				
				$valor_total += $acessorio->valor_total_com_ipi_e_desconto;
			}
		}
		else
		{
			$_acessorios_nff = '</tr><td colspan="3" style="border-left: 1px solid #DDD;">Nenhum adicionado.</td></tr>';
		}
		
		//
		
		$conteudo .= '
			<div style="' . ($tipo == 'simples' ? 'background-color: #EEE; float: right; font-size: 11px; margin-top: 20px; max-width: 260px; padding: 10px; -webkit-border-radius: 5px;' : NULL). '">
				' . ($tipo == 'simples' ? '<h3 style="margin: 0;">Visão Geral</h3>' : NULL). '
				
				<table cellspacing="0" class="info" style="background-color: #FFF; margin-right: 0;">
					<tr>
						<th>' . ($cliente ? 'Cliente' : ($prospect ? 'Prospect' : 'Cliente/Prospect')) . ':</th>
						<td colspan="2">' . ($cliente ? $cliente['nome'] : ($prospect ? $prospect->nome : 'Nenhum selecionado.')) . '</td>
					</tr>
					
					<tr><td colspan="3" style="border-left: 1px solid #DDD;">&nbsp;</td></tr>
					
					<tr>
						<th>Casco:</th>
						<td colspan="2">' . ($casco ? $casco->codigo_real . ' - ' . $casco->descricao : 'Nenhum adicionado.') . '</td>
						<!--<td style="text-align: right;">' . number_format($casco->valor_total_com_ipi_e_desconto, 2, ',', '.') . '</td>-->
					</tr>
					
					<tr><td colspan="3" style="border-left: 1px solid #DDD;">&nbsp;</td></tr>
					
					' . $_motores . '
					
					<tr><td colspan="3" style="border-left: 1px solid #DDD;">&nbsp;</td></tr>
					
					<!--<tr>
						<th colspan="3" style="border-right: 1px solid #DDD;">Acessórios Fibrafort:</th>
					</tr>
					' . $_acessorios_ff . '-->
					
					<tr><td colspan="3" style="border-left: 1px solid #DDD;">&nbsp;</td></tr>
					
					<th colspan="3" style="border-right: 1px solid #DDD;">Acessórios Não Fibrafort:</th>
					' . $_acessorios_nff . '
					
					<tr><td colspan="3" style="border-left: 1px solid #DDD;">&nbsp;</td></tr>
					<tr>
						<th>Total:</th>
						<td colspan="2" style="text-align: right;"><strong>R$ ' . number_format($valor_total, 2, ',', '.') . '</strong></td>
					</tr>
				</table>
			</div>
		';
		
		echo $conteudo;
	}
	
	function gerar_proposta($id_pedido = NULL)
	{
		$pedido = $this->db->from('pedidos')->where('id', $id_pedido)->get()->row();
		
		$revenda = $this->db->from('usuarios')->where(array('grupo' => 'revendas', 'codigo' => $pedido->codigo_usuario))->get()->row();
		$vendedor = $this->db->from('usuarios')->where(array('id' => $pedido->id_usuario))->get()->row();
		
		$filial = $this->db->from('filiais')->where('id', $vendedor->id_filial)->get()->row();
		if (!$filial)
		{
			$filial = $this->db->from('filiais')->where('codigo_revenda', $revenda->codigo)->order_by('id', 'asc')->limit(1)->get()->row();
		}
		
		$prospect = $this->db->from('prospects')->where('id', $pedido->id_prospect)->get()->row();
		
		$casco = $this->db->from('itens_pedidos')->where(array('id_pedido' => $id_pedido, 'tipo_produto' => 'casco'))->get()->row();
		$_casco = $this->db->from('cascos')->where('codigo', $casco->codigo_real)->get()->row();
		
		$motores = $this->db->from('itens_pedidos')->where(array('id_pedido' => $id_pedido, 'tipo_produto' => 'motor'))->get()->result();
		
		$acessorios_nff = $this->db->from('itens_pedidos')->where(array('id_pedido' => $id_pedido, 'tipo_produto' => 'acessorio_nff'))->get()->result();
		
		$_acessorios_nff = $this->db->from('produtos')->where(array('codigo_revenda' => $revenda->codigo, 'codigo_produto' => $casco->codigo_real, 'id_grupo_produtos !=' => 1))->get()->result();
		
		//
		
		$conteudo = '<p class="nao_exibir_impressao" style="margin-top: 20px; float: right;"><a class="botao_d" href="#" onclick="print(); return false;" style="margin-right: 0;">Imprimir</a></p>';
		
		if (ereg('processar_pedido/consultar', $_SERVER['HTTP_REFERER']))
		{
			$conteudo .= '<p class="nao_exibir_impressao" style="margin-top: 20px; float: right;"><a class="botao_d" href="#" onclick="history.go(-1); return false;">Voltar</a></p>';
		}
		else if (ereg('processar_pedido/gerar_proposta', $_SERVER['HTTP_REFERER']))
		{
			$conteudo .= '<p class="nao_exibir_impressao" style="margin-top: 20px; float: right;"><a class="botao_d" href="#" onclick="history.go(-2); return false;">Voltar</a></p>';
		}
		
		if ($pedido->tipo == 'orcamento')
		{
			$conteudo .= '<p class="nao_exibir_impressao" style="margin-top: 20px; float: right;"><a class="botao_a" href="' . site_url('processar_pedido/converter/' . $id_pedido) . '">Converter Em Pedido</a></p>';
			$conteudo .= '<p class="nao_exibir_impressao" style="margin-top: 20px; float: right;"><a class="botao_b" href="' . site_url('processar_pedido/selecionar_cliente_prospect/' . $id_pedido) . '">Editar Orçamento</a></p>';
		}
		
		if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')))
		{
			$conteudo .= '<p class="nao_exibir_impressao" style="margin-top: 20px; float: right;"><a class="botao_c" href="' . site_url('processar_pedido/excluir/' . $id_pedido) . '" onclick="if (!confirm(\'Você realmente deseja excluir este ' . ($pedido->tipo == 'pedido' ? 'pedido' : 'orçamento') . '?\')) return false;">Excluir</a></p>';
		}
		
		$conteudo .= '<div class="nao_exibir_impressao" style="height: 20px;"></div>';
		
		$conteudo .= '<img src="' . base_url() . 'misc/logo_empresa.png" style="float: left; margin-right: 20px;" />';
		
		$conteudo .= '<img src="' . base_url() . 'uploads/' . $filial->imagem . '" />';
		
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= '<p style="text-align: right;"><strong>' . element($filial->estado, $this->_obter_estados()) . ' (' . $filial->estado . '), ' . date('d') . ' de ' . element(date('m'), $this->_obter_meses()) . ' de ' . date('Y') . '.</strong></p>';
		
		$conteudo .= '<p><strong>Prezado(a) ' . $prospect->nome . ',</strong></p>';
		
		$conteudo .= '<p>Conforme solicitado, apresento proposta de fornececimento da <strong>' . $_casco->descricao . '</strong>. Seguem abaixo, discriminadas as especificações técnicas, preços, condições de pagamento e prazo para entrega.</p>';
		
		$conteudo .= '<h3 class="titulo_proposta">Especificações Técnicas</h3>';
		
		$conteudo .= '<p>' . $_casco->especificacoes_tecnicas . '</p>';
		
		$conteudo .= '<h3 class="titulo_proposta">Itens de Série</h3>';
		
		$conteudo .= '<p>' . $_casco->itens_serie . '</p>';
		
		$conteudo .= '<h3 class="titulo_proposta">Kit Essencial de Montagem</h3>';
		
		$conteudo .= '<p>' . $_casco->kit_essencial_montagem . '</p>';
		
		$conteudo .= '<h3 class="titulo_proposta">Motorização</h3>';
		
		foreach ($motores as $motor)
		{
			$conteudo .= '<p><span style="float: left;">' . $motor->descricao . '</span> <span style="float: right;">' . number_format($motor->valor_total_com_ipi_e_desconto, 2, ',', '.') . '</span></p>';
			
			$conteudo .= '<div style="clear: both;"></div>';
			
			$valor_motores += $motor->valor_total_com_ipi_e_desconto;
		}
		
		$conteudo .= '<p><strong style="float: left;">Subtotal do conjunto acima:</strong> <strong style="float: right;">R$ ' . number_format($valor_motores, 2, ',', '.') . '</strong></p>';
		
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= '<h3 class="titulo_proposta">Opcionais Inclusos</h3>';
		
		$acessorios_nff_inclusos = array();
		
		if ($acessorios_nff)
		{
			foreach ($acessorios_nff as $acessorio)
			{
				$conteudo .= '<p><span style="float: left;">' . $acessorio->descricao . '</span> <span style="float: right;">' . number_format($acessorio->valor_total_com_ipi_e_desconto, 2, ',', '.') . '</span></p>';
				
				$conteudo .= '<div style="clear: both;"></div>';
				
				$acessorios_nff_inclusos[] = $acessorio->codigo;
				
				$valor_acessorios_nff += $acessorio->valor_total_com_ipi_e_desconto;
			}
		}
		else
		{
			$conteudo .= '<p>Não há opcionais inclusos.<p>';
		}
		
		$conteudo .= '<p><strong style="float: left;">Subtotal do conjunto acima:</strong> <strong style="float: right;">R$ ' . number_format($valor_acessorios_nff, 2, ',', '.') . '</strong></p>';
		
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= '<h3 class="titulo_proposta">Opcionais Não Inclusos</h3>';
		
		if ($_acessorios_nff)
		{
			foreach ($_acessorios_nff as $acessorio)
			{
				if (in_array($acessorio->id, $acessorios_nff_inclusos)) {
					continue;
				}
				
				$conteudo .= '<p><span style="float: left;">' . $acessorio->descricao . '</span> <span style="color: #999; float: right;">' . number_format($acessorio->preco, 2, ',', '.') . '</span></p>';
				
				$conteudo .= '<div style="clear: both;"></div>';
				
				$possui_acessorios_nff_nao_inclusos = TRUE;
			}
		}
		
		if (!$_acessorios_nff || !$possui_acessorios_nff_nao_inclusos)
		{
			$conteudo .= '<p>Não há opcionais não inclusos.<p>';
		}
		
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= '<h3 class="titulo_proposta">Valor Total</h3>';
		
		$conteudo .= '<p><span style="float: left;">Casco e motorização</span> <span style="float: right;">' . number_format($valor_motores, 2, ',', '.') . '</span></p>';
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= '<p><span style="float: left;">Opcionais inclusos</span> <span style="float: right;">' . number_format($valor_acessorios_nff, 2, ',', '.') . '</span></p>';
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= '<p><span style="float: left;">Frete</span> <span style="float: right;">' . number_format($pedido->valor_frete, 2, ',', '.') . '</span></p>';
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= '<p><strong style="float: left;">Valor total</strong> <strong style="float: right;">R$ ' . number_format($valor_motores + $valor_acessorios_nff + $pedido->valor_frete, 2, ',', '.') . '</strong></p>';
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= '<h3 class="titulo_proposta">Condição de Pagamento</h3>';
		
		$conteudo .= '<p>' . nl2br($pedido->condicao_pagamento) . '</p>';
		
		$conteudo .= '<h3 class="titulo_proposta">Prazo de Entrega</h3>';
		
		$conteudo .= '<p>' . nl2br($pedido->prazo_entrega) . '</p>';
		
		$conteudo .= '<h3 class="titulo_proposta">Frete</h3>';
		
		$conteudo .= '<p>' . nl2br($pedido->frete) . '</p>';
		
		$conteudo .= '<h3 class="titulo_proposta">Garantias</h3>';
		
		$conteudo .= '<p><span style="float: left;">Casco (garantia estrutural exclusiva)</span> <span style="float: right;">3 anos</span></p>';
		$conteudo .= '<div style="clear: both;"></div>';
		
		foreach ($motores as $motor)
		{
			$_motor = $this->db->from('produtos')->where('id', $motor->codigo)->get()->row();
			
			$conteudo .= '<p><span style="float: left;">Motor - ' . $motor->descricao . '</span> <span style="float: right;">' . $_motor->garantia_anos . ' anos</span></p>';
			
			$conteudo .= '<div style="clear: both;"></div>';
			
			$valor_motores += $motor->valor_total_com_ipi_e_desconto;
		}
		
		$conteudo .= '<p><span style="float: left;">Opcionais inclusos</span> <span style="float: right;">1 ano</span></p>';
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= '
			<div style="float: left; margin-right: 40px; margin-top: 80px; width: 300px;">
				<p style="border-top: 1px solid #000; padding-top: 5px; text-align: center;">' . $vendedor->nome_real . ' - Revenda ' . $revenda->nome_real . '</p>
			</div>
		';
		
		$conteudo .= '
			<div style="float: left; margin-top: 80px; width: 300px;">
				<p style="border-top: 1px solid #000; padding-top: 5px; text-align: center;">' . $prospect->nome . ' - ' . $prospect->cpf . '</p>
			</div>
		';
		
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= '
			<p style="border: 1px solid #DDD; font-size: 11px; margin-top: 40px; padding: 5px; text-align: center;">
				' . $revenda->nome_real . ' - ' . $filial->nome . ' - ' . $filial->endereco . ' nº ' . $filial->numero . ' - ' . $filial->cidade . ' - ' . element($filial->estado, $this->_obter_estados()) . ' (' . $filial->estado . ')<br />
				CEP ' . $filial->cep . ' - Vendas: ' . $filial->telefone . '<br />
				Site: ' . $filial->site . ' - E-mail: ' . $filial->email . '
			</p>
		';
		
		$conteudo .= '<div style="clear: both;"></div>';
		
		//
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function converter($id_pedido = NULL)
	{
		$this->db->update('pedidos', array('tipo' => 'pedido', 'status' => 'aguardando_analise_comercial'), array('id' => $id_pedido));
		
		redirect('processar_pedido/gerar_proposta/' . $id_pedido);
	}
	
	function excluir_item($id_pedido = NULL, $id_item_pedido = NULL, $funcao = NULL)
	{
		// TODO: verificar se o item pode sr excluído
		
		$item_pedido = $this->db->from('itens_pedidos')->where('id', $id_item_pedido)->get()->row();
		
		if ($item_pedido->tipo_produto == 'casco')
		{
			$this->db->delete('itens_pedidos', array('id_pedido' => $id_pedido));
		}
		else
		{
			$this->db->delete('itens_pedidos', array('id' => $id_item_pedido));
		}
		
		redirect('processar_pedido/' . $funcao . '/' . $id_pedido . '/#itens_adicionados');
	}
	
	function obter_informacoes_cliente_ajax($codigo_loja_cliente = NULL)
	{
		$dados = array(
			'cliente_encontrado' => FALSE,
			'html' => NULL
		);
		
		$codigo_loja_cliente = explode('_', $codigo_loja_cliente);
		$codigo_cliente = $codigo_loja_cliente[0];
		$loja_cliente = $codigo_loja_cliente[1];
		
		if ($codigo_cliente && $loja_cliente)
		{
			$cliente = $this->db_cliente->obter_cliente($codigo_cliente, $loja_cliente);
		}
		
		if ($cliente)
		{
			$dados['cliente_encontrado'] = TRUE;
			
			$dados['html'] = '
				<p><strong>' . $cliente['nome'] . '</strong> - ' . $cliente['cpf'] . '</p>
				
				<p style="float: left; margin-right: 10px;"><strong>Última compra:</strong></p>
				
				<div style="clear: both;"></div>
				
				<ul style="float: left; margin-top: 5px; margin-right: 10px;">
					<li>Forma de pagamento: ' . ($cliente['ultimo_pedido']['descricao_forma_pagamento'] ? $cliente['ultimo_pedido']['descricao_forma_pagamento'] : '-') . '</li>
					<li>Desconto: R$ ' . number_format($cliente['ultimo_pedido']['valor_desconto'], 2, ',', '.') . '</li>
					<li>Data: ' . ($cliente['ultimo_pedido']['emissao_timestamp'] ? date('d/m/Y', $cliente['ultimo_pedido']['emissao_timestamp']) : '-') . '</li>
					' . ($cliente['ultimo_pedido']['codigo_pedido'] ? '<li>' . anchor('pedidos/ver_detalhes/' . $cliente['ultimo_pedido']['codigo_pedido'], 'Ver detalhes &raquo;', 'target="_blank"') . '</li>' : NULL) . '
				</ul>
				
				<!--<ul style="float: left; margin-right: 10px;">
					<li><strong>Limite de crédito:</strong> R$ ' . number_format($cliente['limite_credito'], 2, ',', '.') . '</li>
					<li><strong>Títulos em aberto:</strong> R$ ' . number_format($cliente['total_titulos_em_aberto'], 2, ',', '.') . '</li>
					<li><strong>Títulos vencidos:</strong> R$ ' . number_format($cliente['total_titulos_vencidos'], 2, ',', '.') . '</li>
				</ul>
				
				<ul style="float: left; margin-right: 10px;">
					<li><strong>Endereço:</strong> ' . $cliente['endereco'] . '</li>
					<li><strong>Bairro:</strong> ' . $cliente['bairro'] . '</li>
					<li><strong>CEP:</strong> ' . $cliente['cep'] . '</li>
				</ul>
				
				<ul style="float: left; margin-right: 10px;">
					<li><strong>Cidade:</strong> ' . $cliente['cidade'] . '</li>
					<li><strong>Estado:</strong> ' . $cliente['estado'] . '</li>
				</ul>-->
				
				<div style="clear: both;"></div>
			';
		}
		
		echo json_encode($dados);
	}
	
	function obter_informacoes_prospect_ajax($id_prospect = NULL)
	{
		$dados = array(
			'prospect_encontrado' => FALSE,
			'html' => NULL
		);
		
		if ($id_prospect)
		{
			$prospect = $this->db->from('prospects')->where('id', $id_prospect)->get()->row();
		}
		
		if ($prospect)
		{
			$dados['prospect_encontrado'] = TRUE;
			
			$dados['html'] = '
				<p><strong>' . $prospect->nome . '</strong> - ' . $prospect->cpf . '</p>
				
				<ul style="float: left; margin-right: 10px;">
					<li><strong>Endereço:</strong> ' . $prospect->endereco . '</li>
					<li><strong>Bairro:</strong> ' . $prospect->bairro . '</li>
					<li><strong>CEP:</strong> ' . $prospect->cep . '</li>
				</ul>
				
				<ul style="float: left; margin-right: 10px;">
					<li><strong>Cidade:</strong> ' . $prospect->cidade . '</li>
					<li><strong>Estado:</strong> ' . $prospect->estado . '</li>
				</ul>
				
				<div style="clear: both;"></div>
			';
		}
		
		echo json_encode($dados);
	}
	
	//
	
	function _obter_clientes($codigo_usuario = NULL)
	{
		$clientes = $this->db_cliente->obter_clientes($codigo_usuario);
		$_clientes = array();
		
		foreach ($clientes as $cliente)
		{
			$_clientes[$cliente['codigo'] . '|' . $cliente['loja']] = $cliente['nome'] . ' - ' . $cliente['cpf'];
		}
		
		return $_clientes;
	}
	
	function _obter_prospects($codigo_usuario = NULL)
	{
		$prospects = $this->db->from('prospects')->where(array('status' => 'ativo', 'codigo_usuario' => $codigo_usuario))->get()->result();
		$_prospects = array();
		
		foreach ($prospects as $prospect)
		{
			$_prospects[$prospect->id] = $prospect->nome . ' - ' . $prospect->cpf;
		}
		
		return $_prospects;
	}
	
	function _calcular_valores_item_pedido($id_item_pedido = NULL)
	{
		$item_pedido = $this->db->from('itens_pedidos')->where('id', $id_item_pedido)->get()->row();
		
		$preco = $item_pedido->preco;
		$quantidade = $item_pedido->quantidade;
		
		// ipi
		$valor_ipi = $preco * ($item_pedido->ipi / 100);
		$valor_total_ipi = $valor_ipi * $quantidade;
		
		// desconto
		$valor_desconto = $preco * ($item_pedido->desconto / 100);
		$valor_total_desconto = $valor_desconto * $quantidade;
		
		// preço
		$preco_com_ipi = $preco + $valor_ipi;
		$preco_com_ipi_e_desconto = $preco_com_ipi - $valor_desconto;
		
		// total
		$valor_total = $preco * $quantidade;
		$valor_total_com_ipi = $preco_com_ipi * $quantidade;
		$valor_total_com_ipi_e_desconto = $preco_com_ipi_e_desconto * $quantidade;
		
		$this->db->update('itens_pedidos', array(
			'preco' => $preco,
			'preco_com_ipi' => $preco_com_ipi,
			'preco_com_ipi_e_desconto' => $preco_com_ipi_e_desconto,
			'valor_ipi' => $valor_ipi,
			'valor_desconto' => $valor_desconto,
			'valor_total' => $valor_total,
			'valor_total_ipi' => $valor_total_ipi,
			'valor_total_desconto' => $valor_total_desconto,
			'valor_total_com_ipi' => $valor_total_com_ipi,
			'valor_total_com_ipi_e_desconto' => $valor_total_com_ipi_e_desconto,
		), array('id' => $item_pedido->id));
	}
	
	function _obter_cascos()
	{
		$codigo_grupo_produtos = '0101';
		$codigo_tabela_precos = '021'; // 021 = revenda p/ cliente, 020 = fibrafort p/ revenda
		
		$cascos = $this->db_cliente->obter_produtos($codigo_grupo_produtos);
		$_cascos = array();
		
		foreach ($cascos as $casco)
		{
			$_casco = $this->db->from('cascos')->where('codigo', $casco['codigo'])->get()->row();
			
			if ($_casco->imagem && $_casco->especificacoes_tecnicas && $_casco->itens_serie && $_casco->kit_essencial_montagem)
			{
				$_cascos[$casco['codigo']] = $casco['codigo'] . ' - ' . $casco['descricao']/* . ' - R$ ' . number_format($casco['preco'] ? $casco['preco'] : 0, 2, ',', '.')*/;
			}
		}
		
		return $_cascos;
	}
	
	function _obter_motores($codigo_revenda = NULL, $codigo_produto = NULL)
	{
		/*
		TODO: remover, no momento os motores estão cadastrados somente para a revenda motoryama
		if ($codigo_revenda)
		{
			$this->db->where('codigo_revenda', $codigo_revenda);
		}*/
		
		if ($codigo_produto)
		{
			$this->db->where('codigo_produto', $codigo_produto);
		}
		
		$motores = $this->db->from('produtos')->where('id_grupo_produtos', 1 /* 1 = motores */)->get()->result();
		$_motores = array();
		
		foreach ($motores as $motor)
		{
			$_motores[$motor->id] = $motor->codigo . ' - ' . $motor->descricao/* . ' - R$ ' . number_format($motor->preco, 2, ',', '.')*/;
		}
		
		return $_motores;
	}
	
	function _obter_acessorios_ff($codigo_produto = NULL)
	{
		$codigo_tabela_precos = '021'; // 021 = revenda p/ cliente, 020 = fibrafort p/ revenda
		
		$acessorios_ff = $this->db_cliente->obter_opcionais_produtos($codigo_produto, $codigo_tabela_precos);
		$_acessorios_ff = array();
		
		foreach ($acessorios_ff as $acessorio)
		{
			$_acessorios_ff[$acessorio['codigo']] = $acessorio['codigo'] . ' - ' . $acessorio['descricao'] . ' - R$ ' . number_format($acessorio['preco'] ? $acessorio['preco'] : 0, 2, ',', '.');
		}
		
		return $_acessorios_ff;
	}
	
	function _obter_acessorios_nff($codigo_revenda = NULL, $codigo_produto = NULL)
	{
		if ($codigo_revenda)
		{
			$this->db->where('codigo_revenda', $codigo_revenda);
		}
		
		if ($codigo_produto)
		{
			$this->db->where('codigo_produto', $codigo_produto);
		}
		
		$acessorios_nff = $this->db->from('produtos')->where('id_grupo_produtos !=', 1 /* 1 = motores */)->get()->result();
		$_acessorios_nff = array();
		
		foreach ($acessorios_nff as $acessorio)
		{
			$_acessorios_nff[$acessorio->id] = $acessorio->codigo . ' - ' . $acessorio->descricao . ' - R$ ' . number_format($acessorio->preco, 2, ',', '.');
		}
		
		return $_acessorios_nff;
	}
	
	//
	
	function _gerar_passo_a_passo($id_pedido = NULL, $passo = NULL)
	{
		$conteudo = $this->_gerar_bola_passo_a_passo('&nbsp;<br />Prospect', 1, 'processar_pedido/selecionar_cliente_prospect/' . $id_pedido, $passo);
		$conteudo .= $this->_gerar_bola_passo_a_passo('&nbsp;<br />Casco', 2, 'processar_pedido/adicionar_casco/' . $id_pedido, $passo);
		$conteudo .= $this->_gerar_bola_passo_a_passo('&nbsp;<br />Motores', 3, 'processar_pedido/adicionar_motores/' . $id_pedido, $passo);
		//$conteudo .= $this->_gerar_bola_passo_a_passo('Acessórios<br />Fibrafort', 4, 'processar_pedido/adicionar_acessorios_ff/' . $id_pedido, $passo);
		$conteudo .= $this->_gerar_bola_passo_a_passo('Acess. Não<br />Fibrafort', 4, 'processar_pedido/adicionar_acessorios_nff/' . $id_pedido, $passo);
		$conteudo .= $this->_gerar_bola_passo_a_passo('&nbsp;<br />Visão Geral', 5, 'processar_pedido/visao_geral/' . $id_pedido, $passo);
		$conteudo .= $this->_gerar_bola_passo_a_passo('&nbsp;<br />Proposta', 6, 'processar_pedido/gerar_proposta/' . $id_pedido, $passo);
		
		$conteudo .= '<div style="clear: left;"></div>';
		
		return $conteudo;
	}
	
	function _gerar_bola_passo_a_passo($titulo, $numero, $url, $passo)
	{
		return '
			' . ($numero < $passo ? '<a href="' . site_url($url) . '" style="text-decoration: none;">' : NULL) . '
				<div style="color: #' . ($numero == $passo ? 'CCC' : 'EEE') . '; float: left; margin-right: 10px; font-size: 12px; margin-top: 20px; text-align: center; width: 80px;">
					<strong>' . $titulo . '</strong>
					
					<div style="border: 5px solid #' . ($numero == $passo ? 'CCC' : 'EEE') . '; background-color: #FFF; font-size: 40px; height: 50px; margin: 0 auto; margin-top: 5px; width: 50px; border-radius: 50px;">
						' . $numero . '
					</div>
				</div>
			' . ($numero < $passo ? '</a>' : NULL) . '
		';
	}
	
}
