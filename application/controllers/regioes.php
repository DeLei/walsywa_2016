<?php

class Regioes extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('cliente');
	}
	
	function index()
	{
		$regioes = $this->db->from('regioes')->get()->result();
		
		$conteudo = heading('Regiões', 2);
		
		$conteudo .= '<p>' . anchor('regioes/criar', 'Cadastrar Nova Região') . '</p>';
		
		$conteudo .= '
			<table cellspacing="0">
				<thead>
					<tr>
						<th>Nome</th>
						<th>Cidades</th>
						<th>Estados</th>
						<th>Opções</th>
					</tr>
				</thead>
				
				<tbody>
		';
		
		foreach ($regioes as $regiao)
		{
			switch ($regiao->tipo)
			{
				case 'cidades':
					$cidades = $this->db->from('cidades_regioes')->where('id_regiao', $regiao->id)->order_by('cidade', 'asc')->get()->result();
					$_cidades = array();
					
					foreach ($cidades as $cidade)
					{
						$_cidades[] = $cidade->cidade;
					}
				break;
				
				case 'estados':
					$estados = $this->db->from('estados_regioes')->where('id_regiao', $regiao->id)->order_by('estado', 'asc')->get()->result();
					$_estados = array();
					
					foreach ($estados as $estado)
					{
						$_estados[] = $estado->estado;
					}
				break;
			}
			
			$conteudo .= '
				<tr>
					<td>' . $regiao->nome . '</td>
					<td>' . ($regiao->tipo == 'cidades' ? implode(', ', $_cidades) : '-') . '</td>
					<td>' . ($regiao->tipo == 'estados' ? implode(', ', $_estados) : '-') . '</td>
					<td>' . anchor('regioes/editar/' . $regiao->id, 'Editar') . '</td>
				</tr>
			';
		}
		
		$conteudo .= '</tbody></table>';
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function criar()
	{
		if ($_POST)
		{
			if (!$this->input->post('nome'))
			{
				$erro = 'Digite o nome da região.';
			}
			else if ($this->db->from('regioes')->where('nome', $this->input->post('nome'))->get()->row())
			{
				$erro = 'O nome da região digitado já existe.';
			}
			else
			{
				switch ($this->input->post('tipo'))
				{
					case 'cidades':
						$cidades = $this->input->post('cidades');
						
						foreach ($cidades as $cidade)
						{
							$cidade = $this->db->from('cidades_regioes')->where('cidade', $cidade)->get()->row();
							
							if ($cidade)
							{
								$regiao = $this->db->from('regioes')->where('id', $cidade->id_regiao)->get()->row();
								
								$erro = 'A cidade <strong>' . $cidade->cidade . '</strong> já está cadastrada na região <strong>' . $regiao->nome . '</strong>.';
								
								break;
							}
						}
					break;
					
					case 'estados':
						$estados = $this->input->post('estados');
						
						foreach ($estados as $estado)
						{
							$estado = $this->db->from('estados_regioes')->where('estado', $estado)->get()->row();
							
							if ($estado)
							{
								$regiao = $this->db->from('regioes')->where('id', $estado->id_regiao)->get()->row();
								
								$erro = 'O estado <strong>' . $estado->estado . '</strong> já está cadastrado na região <strong>' . $regiao->nome . '</strong>.';
								
								break;
							}
						}
					break;
				}
				
				if (!$erro)
				{
					$this->db->insert('regioes', array(
						'timestamp' => time(),
						'tipo' => $this->input->post('tipo'),
						'nome' => $this->input->post('nome')
					));
					
					$id_regiao = $this->db->insert_id();
					
					if ($cidades)
					{
						foreach ($cidades as $cidade)
						{
							if (!trim($cidade)) continue;
							
							$this->db->insert('cidades_regioes', array(
								'id_regiao' => $id_regiao,
								'cidade' => $cidade
							));
						}
					}
					else if ($estados)
					{
						foreach ($estados as $estado)
						{
							if (!trim($estado)) continue;
							
							$this->db->insert('estados_regioes', array(
								'id_regiao' => $id_regiao,
								'estado' => $estado
							));
						}
					}
					
					redirect('regioes');
				}
			}
		}
		
		$conteudo .= heading('Cadastrar Região', 2);
		
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		
		$conteudo .= form_open(current_url());
		$conteudo .= '<p>' . form_label('Tipo:' . br() . form_dropdown('tipo', $this->_obter_tipos(), $this->input->post('tipo'))) . '</p>';
		$conteudo .= '<p>' . form_label('Nome da região:' . br() . form_input('nome', $this->input->post('nome'), 'size="40"')) . '</p>';
		$conteudo .= '<p>' . form_label('Cidades:' . br() . form_dropdown('cidades[]', $this->_obter_cidades(), $this->input->post('cidades'), 'multiple="multiple" class="multiselect" style="height: 240px; width: 580px;"')) . '</p>';
		$conteudo .= '<p>' . form_label('Estados:' . br() . form_dropdown('estados[]', $this->_obter_estados(), $this->input->post('estados'), 'multiple="multiple" class="multiselect" style="height: 240px; width: 580px;"')) . '</p>';
		$conteudo .= '<p>' . form_submit('concluir', 'Concluir') . ' ou ' . anchor('regioes', 'Cancelar') . '</p>';
		$conteudo .= form_close();
		
		$conteudo .= '
			<script type="text/javascript">
				$(document).ready(function() {
					verificar_tipo();
					
					$("select[name=tipo]").change(function() {
						verificar_tipo();
					});
				});
				
				function verificar_tipo()
				{
					switch ($("select[name=tipo]").val())
					{
						case "cidades":
							$("select[name^=cidades]").parents("p").show();
							$("select[name^=estados]").parents("p").hide();
						break;
						
						case "estados":
							$("select[name^=cidades]").parents("p").hide();
							$("select[name^=estados]").parents("p").show();
						break;
					}
				}
			</script>
		';
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function editar($id_regiao = NULL)
	{
		$regiao = $this->db->from('regioes')->where('id', $id_regiao)->get()->row_array();
		$cidades_regiao = $this->db->from('cidades_regioes')->where('id_regiao', $id_regiao)->get()->result();
		$estados_regiao = $this->db->from('estados_regioes')->where('id_regiao', $id_regiao)->get()->result();
		
		if (!$regiao)
		{
			redirect('regioes');
		}
		
		if ($_POST)
		{
			if (!$this->input->post('nome'))
			{
				$erro = 'Digite o nome da região.';
			}
			else if ($this->db->from('regioes')->where(array('id !=' => $id_regiao, 'nome' => $this->input->post('nome')))->get()->row())
			{
				$erro = 'O nome da região digitado já existe.';
			}
			else
			{
				switch ($this->input->post('tipo'))
				{
					case 'cidades':
						$cidades = $this->input->post('cidades');
						
						foreach ($cidades as $cidade)
						{
							$cidade = $this->db->from('cidades_regioes')->where(array('id_regiao !=' => $id_regiao, 'cidade' => $cidade))->get()->row();
							
							if ($cidade)
							{
								$_regiao = $this->db->from('regioes')->where('id', $cidade->id_regiao)->get()->row();
								
								$erro = 'A cidade <strong>' . $cidade->cidade . '</strong> já está cadastrada na região <strong>' . $_regiao->nome . '</strong>.';
								
								break;
							}
						}
					break;
					
					case 'estados':
						$estados = $this->input->post('estados');
						
						foreach ($estados as $estado)
						{
							$estado = $this->db->from('estados_regioes')->where(array('id_regiao !=' => $id_regiao, 'estado' => $estado))->get()->row();
							
							if ($estado)
							{
								$_regiao = $this->db->from('regioes')->where('id', $estado->id_regiao)->get()->row();
								
								$erro = 'O estado <strong>' . $estado->estado . '</strong> já está cadastrado na região <strong>' . $_regiao->nome . '</strong>.';
								
								break;
							}
						}
					break;
				}
				
				if (!$erro)
				{
					$this->db->update('regioes', array(
						'tipo' => $this->input->post('tipo'),
						'nome' => $this->input->post('nome')
					), array('id' => $id_regiao));
					
					if ($cidades)
					{
						$this->db->delete('cidades_regioes', array('id_regiao' => $id_regiao));
						
						foreach ($cidades as $cidade)
						{
							if (!trim($cidade)) continue;
							
							$this->db->insert('cidades_regioes', array(
								'id_regiao' => $id_regiao,
								'cidade' => $cidade
							));
						}
					}
					else if ($estados)
					{
						$this->db->delete('estados_regioes', array('id_regiao' => $id_regiao));
						
						foreach ($estados as $estado)
						{
							if (!trim($estado)) continue;
							
							$this->db->insert('estados_regioes', array(
								'id_regiao' => $id_regiao,
								'estado' => $estado
							));
						}
					}
					
					redirect('regioes');
				}
			}
		}
		else
		{
			$_POST = $regiao;
			
			foreach ($cidades_regiao as $cidade_regiao)
			{
				$_POST['cidades'][] = $cidade_regiao->cidade;
			}
			
			foreach ($estados_regiao as $estado_regiao)
			{
				$_POST['estados'][] = $estado_regiao->estado;
			}
		}
		
		$conteudo .= heading('Editar Região', 2);
		
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		
		$conteudo .= form_open(current_url());
		$conteudo .= '<p>' . form_label('Tipo:' . br() . form_dropdown('tipo', $this->_obter_tipos(), $this->input->post('tipo'))) . '</p>';
		$conteudo .= '<p>' . form_label('Nome da região:' . br() . form_input('nome', $this->input->post('nome'), 'size="40"')) . '</p>';
		$conteudo .= '<p>' . form_label('Cidades:' . br() . form_dropdown('cidades[]', $this->_obter_cidades($id_regiao), $this->input->post('cidades'), 'multiple="multiple" class="multiselect" style="height: 240px; width: 580px;"')) . '</p>';
		$conteudo .= '<p>' . form_label('Estados:' . br() . form_dropdown('estados[]', $this->_obter_estados($id_regiao), $this->input->post('estados'), 'multiple="multiple" class="multiselect" style="height: 240px; width: 580px;"')) . '</p>';
		$conteudo .= '<p>' . form_submit('concluir', 'Concluir') . ' ou ' . anchor('regioes', 'Cancelar') . '</p>';
		$conteudo .= form_close();
		
		$conteudo .= '
			<script type="text/javascript">
				$(document).ready(function() {
					verificar_tipo();
					
					$("select[name=tipo]").change(function() {
						verificar_tipo();
					});
				});
				
				function verificar_tipo()
				{
					switch ($("select[name=tipo]").val())
					{
						case "cidades":
							$("select[name^=cidades]").parents("p").show();
							$("select[name^=estados]").parents("p").hide();
						break;
						
						case "estados":
							$("select[name^=cidades]").parents("p").hide();
							$("select[name^=estados]").parents("p").show();
						break;
					}
				}
			</script>
		';
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	//
	
	function _obter_tipos()
	{
		return array('cidades' => 'Cidades', 'estados' => 'Estados');
	}
	
	function _obter_cidades($desconsiderar_id_regiao = NULL)
	{
		$cidades = $this->cliente->obter_cidades();
		$_cidades = array();
		
		foreach ($cidades as $cidade)
		{
			if ($this->db->from('cidades_regioes')->where(array('id_regiao !=' => $desconsiderar_id_regiao ? $desconsiderar_id_regiao : 0, 'cidade' => $cidade))->get()->row())
			{
				continue;
			}
			
			$_cidades[$cidade] = $cidade;
		}
		
		return $_cidades;
	}
	
	function _obter_estados($desconsiderar_id_regiao = NULL)
	{
		$estados = $this->cliente->obter_estados();
		$_estados = array();
		
		foreach ($estados as $estado)
		{
			if ($this->db->from('estados_regioes')->where(array('id_regiao !=' => $desconsiderar_id_regiao ? $desconsiderar_id_regiao : 0, 'estado' => $estado))->get()->row())
			{
				continue;
			}
			
			$_estados[$estado] = $estado;
		}
		
		return $_estados;
	}
	
}