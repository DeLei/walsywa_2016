<?php

class Criar_pedidos extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('autocomplete');
		
	}
	
	function index()
	{
		redirect('criar_pedidos/informacoes_cliente');
	}
	
	
	// Etapa 1
	function informacoes_cliente($tipo = 'pedido', $codigo_usuario = NULL, $id_pedido = NULL, $copiar = NULL, $onde = NULL, $id_pro_cli= NULL)
	{
		
		//Obter Dados
		// Obter Prospect e Cliente em Orçamento
		if($tipo == 'orcamento' && $id_pro_cli)
		{
			
			$codigo_cliente = explode('-', $id_pro_cli);
			
			//Obter Cliente
			//Se $codigo_cliente[1] receber o codigo da LOJA, recebe os dados do cliente, se não, recebe os dados do prospect
			if($codigo_cliente[1])
			{
				$cliente = $this->db_cliente->obter_cliente($codigo_cliente[0], $codigo_cliente[1]);
				
				$_POST['codigo_loja_cliente'] = $codigo_cliente[0] . '|' . $codigo_cliente[1];
				$_POST['_codigo_loja_cliente'] = $cliente['codigo'] . ' - ' . utf8_encode($cliente['nome']) . ' - ' . $cliente['cpf'];
			}
			else
			{
				//Obter Prospect
				$prospect = $this->db->from('prospects')->where('id', $id_pro_cli)->get()->row();
				
				$_POST['id_prospect'] = $prospect->id;
				$_POST['_id_prospect'] = $prospect->nome . ' - ' . $prospect->cpf;
			
			}

		}
		
		//Obter Cliente em Pedidos
		if($tipo == 'pedido' && $id_pro_cli)
		{
			$codigo_cliente = explode('-', $id_pro_cli);

			$cliente = $this->db_cliente->obter_cliente($codigo_cliente[0], $codigo_cliente[1]);
			
			$_POST['codigo_loja_cliente'] = $codigo_cliente[0] . '|' . $codigo_cliente[1];
			$_POST['_codigo_loja_cliente'] = $cliente['codigo'] . ' - ' . $cliente['nome'] . ' - ' . $cliente['cpf'];
			
		}
		
		
		
		//** Editar
		if($id_pedido)
		{
		
			//Se Onde, Copiar Pedido Analizado, se não, Copiar Pedido Aguardadno analize
			if($onde)
			{
				//Obter Pedido para ser Editado
				
				$pedido = $this->db_cliente->obter_pedido($id_pedido);
				
				
				
				$editar['codigo_usuario'] 				= $pedido['codigo_representante'];
				$editar['codigo_loja_cliente'] 			= $pedido['cliente']['codigo'] . '|' . $pedido['cliente']['loja'];
				$editar['_codigo_loja_cliente'] 		= $pedido['cliente']['codigo'] . ' - ' . $pedido['cliente']['nome'] . ' - ' . $pedido['cliente']['cpf'];
				$editar['tipo'] 						= $pedido['tipo'];
				$editar['id_prospect'] 					= NULL;
				$editar['nome_prospect'] 				= NULL;
				$editar['codigo_forma_pagamento'] 		= $pedido['codigo_forma_pagamento'];
				$editar['tipo_frete'] 					= $pedido['tipo_frete'];
				$editar['id_feira'] 					= NULL;
				$editar['codigo_transportadora'] 		= $pedido['codigo_transportadora'];
				$editar['codigo_ordem_compra'] 			= $pedido['codigo_ordem_compra'];
				$editar['data_entrega'] 				= NULL;
				$editar['observacao_pedido_imediato'] 	= NULL;
				//$editar['unidade'] 						= $pedido['unidade'];
				//$editar['descricao_unidade'] 			= $this->obter_descricao_unidade($pedido['unidade']);
				$editar['codigo_tabela_precos'] 		= $pedido['tabela_precos'];
				
				if($pedido['tipo_pedido'] != 'B' OR $pedido['tipo_pedido'] != 'V' OR $pedido['tipo_pedido'] != 'X')
				{
					$pedido['tipo_pedido'] = 'V';
				}
				
				$editar['tipo_pedido'] 					= $pedido['tipo_pedido'];
				$tipo_pedido = array('V' => 'Venda', 'B' => 'Bonificação', 'X' => 'Especial');
				$editar['nome_tipo_pedido']				= $tipo_pedido[$pedido['tipo_pedido']];
				
				
				$produtos = $pedido['itens'];
				foreach($produtos as $indice => $produto)
				{
					
					$dados_produto = $this->db_cliente->obter_produto($produto['codigo_produto'], $pedido['tabela_precos']);
					
					$editar_produtos[$indice]['codigo']								= $dados_produto['codigo'];
					$editar_produtos[$indice]['codigo_real']						= $dados_produto['codigo_real'];
					$editar_produtos[$indice]['descricao']							= $dados_produto['descricao'];
					$editar_produtos[$indice]['unidade_medida']						= $dados_produto['unidade_medida'];
					$editar_produtos[$indice]['ipi']								= $dados_produto['ipi'];
					$editar_produtos[$indice]['peso']								= $dados_produto['peso'];
					$editar_produtos[$indice]['preco']								= $produto['preco_produto'];
					$editar_produtos[$indice]['tabela_precos']						= $this->db_cliente->obter_tabela_precos($pedido['tabela_precos']);
					$editar_produtos[$indice]['quantidade_pedido']					= $produto['quantidade_vendida_produto'];
					
					
										
					
					$editar_produtos[$indice]['total_ipi']							= (( $dados_produto['ipi'] / 100) * ($produto['preco_produto'] * $produto['quantidade_vendida_produto'])) + ($produto['preco_produto'] * $produto['quantidade_vendida_produto']);

					
					$peso_total += $dados_produto['peso'];
					$valor_total += $produto['preco_produto'];
					$valor_total_ipi += $produto['preco_produto'] + $dados_produto['ipi'];
					
				}
				
				$editar['valor_frete'] 					= $this->obter_valor_frete($peso_total, $pedido['cliente']['codigo'] . '|' . $pedido['cliente']['loja'], TRUE);
				$editar['valor_total'] 					= $valor_total;
				$editar['peso_total_geral'] 			= $peso_total;
				$editar['valor_total_ipi'] 				= $valor_total_ipi;
				$editar['valor_total_frete'] 			= $valor_total_ipi + $editar['valor_frete'];
				$editar['produtos']						= $editar_produtos;
				

			}
			else
			{
			
				//Obter Pedido para ser Editado
				
				$pedido = $this->db_cliente->obter_pedidos_pdr($id_pedido, FALSE);
				$produtos = $this->db_cliente->obter_pedidos_pdr($id_pedido, TRUE);
				
				$editar = array();
				
				//Copiar Pedido
				if(!$copiar)
				{
					$editar['id_pedido'] = $pedido['id_pedido'];
				}
				
				$editar['codigo_usuario'] = $pedido['id_usuario'];
				$editar['codigo_loja_cliente'] = $pedido['codigo_cliente'] . '|' . $pedido['loja_cliente'];
				$cliente = $this->db_cliente->obter_cliente($pedido['codigo_cliente'] , $pedido['loja_cliente']);
				$editar['_codigo_loja_cliente'] 		= $pedido['codigo_cliente'] . ' - ' . $cliente['nome'] . ' - ' . $cliente['cpf'];
				$editar['nome_cliente'] = $cliente['nome'];
				$editar['tipo'] = $tipo;
				$editar['id_prospect'] = $pedido['id_prospects'];
				$prospect = $this->db->from('prospects')->where('id', $pedido['id_prospects'])->get()->row();
				$editar['nome_prospect'] = $prospect->nome;
				//$editar['unidade'] = $pedido['filial'];
				//$editar['descricao_unidade'] = $this->obter_descricao_unidade($pedido['filial']);
				$editar['codigo_tabela_precos'] = $pedido['tabela_precos'];
				$editar['tipo_pedido'] = $pedido['tipo_pedido'];
				$tipo_pedido = array('V' => 'Venda', 'B' => 'Bonificação', 'X' => 'Especial');
				$editar['nome_tipo_pedido']	= $tipo_pedido[$pedido['tipo_pedido']];
				$editar['total_st'] = $pedido['substituicao_tributaria'];
				$editar['codigo_forma_pagamento'] = $pedido['condicao_pagamento'];
				$editar['tipo_frete'] = $pedido['tipo_frete'];
				$editar['id_feira'] = $pedido['id_feira'];
				$editar['codigo_transportadora'] = $pedido['codigo_transportadora'];
				$editar['codigo_ordem_compra'] = $pedido['ordem_compra'];
				$editar['data_entrega'] = date('d/m/Y', strtotime($pedido['data_entrega']));
				$editar['observacao_pedido_imediato'] = $pedido['obs_pedido'];
				$editar['autorizado'] = $pedido['autorizado'];
				$editar['cultura'] = $pedido['cultura'];
				
				if($pedido['tipo_pedido'] != 'B' OR $pedido['tipo_pedido'] != 'V' OR $pedido['tipo_pedido'] != 'X')
				{
					$pedido['tipo_pedido'] = 'V';
				}
				
				$editar['tipo_pedido'] 					= $pedido['tipo_pedido'];
				$tipo_pedido = array('V' => 'Venda', 'B' => 'Bonificação', 'X' => 'Especial');
				
				$editar['nome_tipo_pedido']				= $tipo_pedido[$pedido['tipo_pedido']];
				
				
				
				foreach($produtos as $indice => $produto)
				{
					$editar_produtos[$indice]['codigo']								= $produto['codigo_produto'];
					$editar_produtos[$indice]['codigo_real']						= $produto['codigo_produto'];
					$editar_produtos[$indice]['descricao']							= $produto['descricao_produto'];
					$editar_produtos[$indice]['unidade_medida']						= $produto['unidade_medida'];
					$editar_produtos[$indice]['ipi']								= $produto['ipi'];
					$editar_produtos[$indice]['peso']								= $produto['peso_unitario'];
					$editar_produtos[$indice]['st']									= $produto['substituicao_tributaria'];
					$editar_produtos[$indice]['preco']								= $produto['preco_unitario'];
					$editar_produtos[$indice]['tabela_precos']						= $this->db_cliente->obter_tabela_precos($produto['tabela_precos']);
					$editar_produtos[$indice]['quantidade_pedido']					= $produto['quantidade'];
					$editar_produtos[$indice]['desconto']							= $produto['desconto'];
					$editar_produtos[$indice]['validacao_desconto']					= $produto['valor_desconto'];
					$editar_produtos[$indice]['valor_st']							= (($produto['substituicao_tributaria'] / 100) * ($produto['preco_unitario'] * $produto['quantidade']) );
					$editar_produtos[$indice]['total_ipi']							= (( $produto['ipi'] / 100) * ($produto['preco_unitario'] * $produto['quantidade'])) + ($produto['preco_unitario'] * $produto['quantidade']);
					$editar_produtos[$indice]['valor_desconto']						= (($produto['desconto'] / 100) * ($produto['preco_unitario'] * $produto['quantidade']));
					
					$peso_total += $produto['peso_unitario'];
					$valor_total += $produto['preco_unitario'];
					$valor_total_ipi += $produto['preco_unitario'] + $produto['ipi'];
					$valor_total_desconto += $produto['desconto'];
					
		
				}
				
		
				$editar['valor_frete'] 					= $this->obter_valor_frete($peso_total, $pedido['codigo_cliente'] . '|' . $pedido['loja_cliente'], TRUE);
				$editar['valor_total'] 					= $valor_total;
				$editar['peso_total_geral'] 			= $peso_total;
				$editar['valor_total_ipi'] 				= $valor_total_ipi;
				$editar['valor_total_frete'] 			= $valor_total_ipi + $editar['valor_frete'];
				$editar['total_desconto']				= $valor_total_desconto;
				$editar['produtos']						= $editar_produtos;
				
			
				
			
			}
			
			
			
			$this->session->set_userdata('sessao_pedido', $editar);
			redirect('criar_pedidos/adicionar_produtos');
			
		}
		
		//**
		
		//----------------------------------
	
	
		$sessao_tipo = $this->session->userdata('sessao_pedido');
		if($sessao_tipo['tipo'] != $tipo){
			$this->session->unset_userdata('sessao_pedido');
		}
	
		
	
		if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores')))
		{
			// se nenhum código de usuário foi informado vamos pegar o primeiro código disponível no dropdown
			if (!$codigo_usuario)
			{
				$codigo_usuario = $this->_obter_indice_primeiro_usuario_usuarios('codigo');
			}
			
			$_codigo_usuario = $codigo_usuario;
		}
		else
		{
			$_codigo_usuario = $this->session->userdata('codigo_usuario');
		}
		
		
		$codigo_usuario = $codigo_usuario ? $codigo_usuario : $_codigo_usuario;
		
		
		//----------------------------------------
	

		if($_POST)
		{
		
			if(!$this->input->post('codigo_loja_cliente'))
			{
				$cliente_prospect = $this->input->post('id_prospect');
				$_POST['codigo_loja_cliente'] = 000010;
			}
			else
			{
				$cliente_prospect = $this->input->post('codigo_loja_cliente');
			}
		
			if (!$cliente_prospect)
			{
				if($this->input->post('ClienteOuProspet') == 'prospect')
				{
					$erro = 'Selecione um prospect.';
				}
				else
				{
					$erro = 'Selecione um cliente.';
				}
			}
			else
			{
			
				$_codigo_usuario = $this->input->post('codigo_usuario');
				if(!$codigo_usuario)
				{
					$codigo_usuario = $_codigo_usuario;
				}
				
				//Obter Nomes
				$codigo_loja_cliente = explode('|', $this->input->post('codigo_loja_cliente'));
				$cliente = $this->db_cliente->obter_cliente($codigo_loja_cliente[0], $codigo_loja_cliente[1]);
				$prospect = $this->db->from('prospects')->where('id', $this->input->post('id_prospect'))->get()->row();
				//$unidade = $this->db->from('unidades')->where('filial', $this->input->post('unidade'))->get()->row();
				
				if($this->input->post('tipo_pedido') == 'V')
				{
					$nome_tipo_pedido = 'Venda';
				}else if($this->input->post('tipo_pedido') == 'B'){
					$nome_tipo_pedido = 'Bonificação';
				}else if($this->input->post('tipo_pedido') == 'X'){
					$nome_tipo_pedido = 'Especial';
				}
			
				$representante = $this->db_cliente->obter_representante($codigo_usuario);
			
				$dados_sessao = array(
					'codigo_usuario'  => $codigo_usuario,
					'codigo_loja_cliente'  => $this->input->post('codigo_loja_cliente'),
					'_codigo_loja_cliente'  => $this->input->post('_codigo_loja_cliente'),
					'nome_cliente' => $cliente['nome'],
					'tipo' => $tipo,
					'id_prospect'  => $this->input->post('id_prospect'),
					'nome_prospect' => $this->input->post('_id_prospect'),
					'nome_prospects' => $prospect->nome,
					'unidade' => $this->input->post('unidade'),
					'descricao_unidade' => $unidade->descricao,
					'codigo_tabela_precos' => $representante['tabela'],
					'tipo_pedido' => $this->input->post('tipo_pedido'),
					'nome_tipo_pedido' => $nome_tipo_pedido
				);
				
				
				
				$this->session->set_userdata('sessao_pedido', $dados_sessao);
			
				$sessao_pedido = $this->session->userdata('sessao_pedido');
				
				redirect('criar_pedidos/adicionar_produtos');
			}
		}
		
		
		// recebendo sessão
		$sessao_pedido = $this->session->userdata('sessao_pedido');
		
		if($codigo_usuario == $sessao_pedido['codigo_usuario'])
		{
			if($sessao_pedido['codigo_usuario'] && $sessao_pedido['codigo_loja_cliente'])
			{
				$_POST['codigo_usuario'] = $sessao_pedido['codigo_usuario'];
				$_POST['codigo_loja_cliente'] = $sessao_pedido['codigo_loja_cliente'];
				$_POST['_codigo_loja_cliente'] = $sessao_pedido['_codigo_loja_cliente'];
			}
		}	
		
		
		if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores'))) 
		{	
			$dropdown_representantes = $this->_obter_dropdown_representantes('codigo', array('name' => 'codigo_usuario', 'value' => $this->input->post('codigo_usuario') ? $this->input->post('codigo_usuario') : $codigo_usuario, 'id' => 'usuarios'), FALSE);
		}	
	
		/*
		//Unidades
		$representante = $this->db_cliente->obter_representante($codigo_usuario);
		if($representante['grupo'])
		{
			$unidades = $this->obter_unidades($representante['grupo']);
		}
		*/
		
		//Tipo de Pedido
		$tipo_pedido = array('V' => 'Venda', 'B' => 'Bonificação', 'X' => 'Especial');
		
		
	
		$this->load->view('layout', array('conteudo' => $this->load->view('criar_pedidos/informacoes_cliente', 
					array
					(
						'erro' => $erro,
						'dropdown_representantes' => $dropdown_representantes,
						//'unidades' => $unidades,
						'tipo' => $tipo,
						'codigo_usuario' => $codigo_usuario,
						'loja_cliente' => $loja_cliente,
						'tipo' => $tipo,
						'pedido' => $sessao_tipo,
						'tipo_pedido' => $tipo_pedido
					), 
					TRUE
				)
			)
		);
		
	}
	
	//Etapa 2
	function adicionar_produtos()
	{
	
		$sessao_pedido = $this->session->userdata('sessao_pedido');
		
		/*
		echo '<pre>';
		print_r($sessao_pedido);
		echo '</pre>';
		*/
		
		
		if($sessao_pedido['codigo_usuario'] && $sessao_pedido['codigo_loja_cliente'])
		{
		
			if($_POST)
			{
				if($this->input->post('adicionar_produto'))
				{
					if(!$this->input->post('codigo_produto'))
					{
						$erro = "Selecione um produto.";
					}
					else if(!$this->input->post('quantidade'))
					{
						$erro = "Insira uma quantidade.";
					}
					else
					{
						// Inserindo Quantidade e Produtos na sessao
						
						$dados_produto = $this->db_cliente->obter_produto($this->input->post('codigo_produto'), $this->input->post('codigo_tabela_precos'));
						
						$_POST['desconto'] = str_replace(",", ".", $this->input->post('desconto'));
						
						if($dados_produto['validacao_desconto']==NULL)
						{
							$dados_produto['validacao_desconto'] = 0;
						}
						
						if(($sessao_pedido['tipo_pedido'] != 'X') AND ($this->input->post('desconto') > $dados_produto['validacao_desconto']))
						{
							$erro = "O Desconto máximo permitido é <b>" . $dados_produto['validacao_desconto'] . "%</b>.";
						}
						else
						{

							$dados_produto['quantidade_pedido'] = $this->input->post('quantidade');
							$dados_produto['desconto'] = $this->input->post('desconto');
							$dados_produto['preco'] = str_replace(",",".",$dados_produto['preco']);
							$dados_produto['valor_st'] =  (($dados_produto['st'] / 100) * ($dados_produto['preco'] * $dados_produto['quantidade_pedido']) );
							$dados_produto['total_ipi'] = (($dados_produto['ipi'] / 100) * ($dados_produto['preco'] * $dados_produto['quantidade_pedido'])) + ($dados_produto['preco'] * $dados_produto['quantidade_pedido']);
							$dados_produto['valor_desconto'] = (($dados_produto['desconto'] / 100) * ($dados_produto['preco'] * $dados_produto['quantidade_pedido']));
							
							
							// Juntando Produtos já existentes
							$repetido = FALSE;
							
							if($sessao_pedido['produtos'])
							{
								
								foreach($sessao_pedido['produtos'] as $indice => $produto)
								{
									if($produto['codigo'] == $dados_produto['codigo'] && $produto['tabela_precos']['codigo'] == $dados_produto['tabela_precos']['codigo'])
									{
										
										echo '
											<script type="text/javascript">
												alert("Produto já adicionado. As quantidades serão somadas.");
											</script>
										';
										
										$dados_produto['quantidade_pedido'] = $dados_produto['quantidade_pedido'] + $produto['quantidade_pedido'];
										$dados_produto['valor_st'] =  (($dados_produto['st'] / 100) * ($dados_produto['preco'] * $dados_produto['quantidade_pedido']) );
										$dados_produto['total_ipi'] = (($dados_produto['ipi'] / 100) * ($dados_produto['preco'] * $dados_produto['quantidade_pedido'])) + ($dados_produto['preco'] * $dados_produto['quantidade_pedido']);
										$dados_produto['valor_desconto'] = (($dados_produto['desconto'] / 100) * ($dados_produto['preco'] * $dados_produto['quantidade_pedido']));
										
										$sessao_pedido['produtos'][$indice] = $dados_produto;
										
										$repetido = TRUE;
									}
								}
							}
							
							if(!$repetido)
							{
								$sessao_pedido['produtos'][] = $dados_produto;
							}
							//--
							
							$dados_sessao = $sessao_pedido;
							
							$this->session->set_userdata('sessao_pedido', $dados_sessao);
							
							
							//Limpando Campo do Produto Adicionado
							
							$_POST['codigo_produto'] = "";
							$_POST['_codigo_produto'] = "";
							$_POST['quantidade'] = "";
							$_POST['desconto'] = "";
						}
					}
				}
				
				if($this->input->post('continuar'))
				{
					if(count($sessao_pedido['produtos']) > 0)
					{
					
						redirect('criar_pedidos/outras_informacoes');
					}
					else
					{
						$erro = "Adicione um ou mais produtos no pedido.";
					}
				}
				
			}
			
			$codigo_tabela_precos = $this->input->post("codigo_tabela_precos");
			//$codigo_tabela_precos = $sessao_pedido["codigo_tabela_precos"];
			
					//print_r($sessao_pedido);
			
			
					
					
					if($sessao_pedido['produtos'])
					{
					
						$produtos = array_reverse($sessao_pedido['produtos']);
						
						//* Lista de Produtos
			
						$lista_produtos .= '<table class="novo_grid" style="margin-top: 20px">
							<thead>
								<th>Item</th>
								<th>Código</th>
								<th>Descrição</th>
								<th>U.M.</th>
								<th>Tabela</th>
								<th>Preço Unit. (R$)</th>
								<th>Quant.</th>
								<th>Total (R$)</th>
								<th>IPI (%)</th>
								<th>Total c/ IPI (R$)</th>
								<th>ST (R$)</th>
								<th>Desconto (R$)</th>
								<th>Total Geral (R$)</th>
								<th>Opções</th>
							</thead>
							
							<tbody>';
					
						
						$item = count($produtos);
						foreach($produtos as $indice => $produto)
						{
		
							$lista_produtos .= '<tr>
								<td class="center">' . str_pad($item--, 2, 0, STR_PAD_LEFT) . '</td>
								<td class="center">' . $produto['codigo'] . '</td>
								<td>' . $produto['descricao'] . '</td>
								<td>' . $produto['unidade_medida'] . '</td>
								<td>' . $produto['tabela_precos']['descricao'] . '</td>
								<td class="right">' . number_format($produto['preco'], 2, ',', '.') . '</td>
								
								<td style="text-align:center"><a href="' . $item . '" class="alterar_quantidade">' . $produto['quantidade_pedido'] . '</a></td>
								
								<td class="right">' . number_format($produto['preco'] * $produto['quantidade_pedido'], 2, ',', '.')  . '</td>
								<td class="right">' . $produto['ipi'] . '</td>
								<td class="right">' . number_format($produto['total_ipi'], 2, ',', '.') . '</td>
								<td class="right">' . number_format($produto['valor_st'], 2, ',', '.') . '</td>
								<td class="right">' . number_format($produto['valor_desconto'], 2, ',', '.') . '</td>
								<td class="right">' . number_format(($produto['total_ipi'] + $produto['valor_st']) - $produto['valor_desconto'], 2, ',', '.') . '</td>
								<td class="center"><a href="' . $indice . '" class="exlcuir_produto" title="' . $produto['descricao'] . '">Excluir</a></td>
							</tr>';
			
							$peso_total_geral += $produto['peso'] * $produto['quantidade_pedido'];
							$valor_total += ($produto['total_ipi'] + $produto['valor_st']) - $produto['valor_desconto'];
							$valor_total_ipi += $produto['ipi'] + ($produto['preco'] * $produto['quantidade_pedido']);
							$total_st += $produto['valor_st'];
							$total_desconto += $produto['valor_desconto'];

						}
					}

				$lista_produtos .= '</tbody>
				</table>';
				
				
				$valor_frete = $this->obter_valor_frete($peso_total_geral, $sessao_pedido['codigo_loja_cliente'], TRUE);
				$valor_total_frete = $valor_total_ipi + $valor_frete;
				
				// Inserindo Totais na Sessão
				$dados_sessao = $sessao_pedido;
			
				$dados_sessao['total_st'] = $total_st;
				$dados_sessao['valor_frete'] = $valor_frete;
				$dados_sessao['valor_total'] = $valor_total;
				$dados_sessao['peso_total_geral'] = $peso_total_geral;
				$dados_sessao['valor_total_ipi'] = $valor_total_ipi;
				$dados_sessao['valor_total_frete'] = $valor_total_frete;
				$dados_sessao['total_desconto'] = $total_desconto;
				
				$this->session->set_userdata('sessao_pedido', $dados_sessao);
				//--
			
				$lista_produtos .= '
					<script type="text/javascript">
						$(document).ready(function(){
						
							// Excluir Produto
							$(".exlcuir_produto").live("click", function(e){
								e.preventDefault();
							
								if (confirm("Tem certeza que deseja excluir o produto \"" + $(this).attr("title") + "\" ?")) {  
									$.ajax({
										type: "POST",
										url: "' . site_url('criar_pedidos/excluir_produto') . '",
										data: {indice : $(this).attr("href")},
										success: function(data){
											
											if(data == "1")
											{
												location.href = "' . site_url('criar_pedidos/adicionar_produtos') . '";
											}
											else
											{
												alert("Não foi possível excluir o produto.");
											}
											
										}
									});
								}  
							});
							
							// Alterar Quantidade
							$(".alterar_quantidade").live("click", function(e){
								e.preventDefault();
							
									var quantidade = prompt("Digite a nova quantidade:");
									
	
									if(quantidade)
									{
										quantidade = str_replace(",",".",quantidade);
										var regex=/^[\.0-9]+$/;
										if(regex.test(quantidade))
										{
											if(fmod(quantidade, 0.5) != 0)
											{
												$.ajax({
													type: "POST",
													url: "' . site_url('criar_pedidos/alterar_quantidade') . '",
													data: {indice : $(this).attr("href"), quantidade : quantidade},
													success: function(data){
														
														if(data == "1")
														{
															location.href = "' . site_url('criar_pedidos/adicionar_produtos') . '";
														}
														else
														{
															alert("Não foi possível alterar a quantidade do produto.");
														}
														
													}
												});
											}else{
												alert("O valor digitado deve ser multiplo de 0,5.");
											}	
										}
										else
										{
											alert("O valor digitado não é valido.");
										}
									}
								
							});
							
						});
					</script>
				';
			
			//*
		
		
		
			$codigo_usuario = $sessao_pedido['codigo_usuario'];
			$codigo_loja_cliente = $sessao_pedido['codigo_loja_cliente'];
			$usuario = $this->db->from('usuarios')->where('codigo', $sessao_pedido['codigo_usuario'])->get()->row();
			$tabela_precos = $this->db_cliente->obter_tabela_precos($codigo_tabela_precos);
			
			/*print_r($tabela_precos);
			echo "<br />";
			print_r($this->_obter_tabelas_precos($codigo_usuario));
			*/
			$this->load->view('layout', array('conteudo' => $this->load->view('criar_pedidos/adicionar_produtos', 
						array
						(
							'erro' => $erro,
							'codigo_usuario' => $codigo_usuario,
							'codigo_tabela_precos' => $codigo_tabela_precos,
							'tabela_precos' => $tabela_precos,
							'codigo_loja_cliente' => $codigo_loja_cliente,
							'tabelas_precos' => $this->_obter_tabelas_precos($codigo_usuario),
							'lista_produtos' => $lista_produtos,
							'valor_frete' => $valor_frete,
							'total_st' => $total_st,
							'total_desconto' => $total_desconto,
							'valor_total' => $valor_total,
							'peso_total_geral' => $peso_total_geral,
							'valor_total_ipi' => $valor_total_ipi,
							'valor_total_frete' => $valor_total_frete,
							'pedido' => $sessao_pedido,
							'usuario' => $usuario
						), 
						TRUE
					)
				)
			);
		
		}
		else
		{
			redirect('criar_pedidos');
		}
		
	}
	
	//Etapa 3
	function outras_informacoes()
	{
		
		$sessao_pedido = $this->session->userdata('sessao_pedido');
		
		/*
		echo '<pre>';
		print_r($sessao_pedido);
		echo '</pre>';
		*/
		
		
		if(!$sessao_pedido['produtos'])
		{
			redirect('criar_pedidos/adicionar_produtos');
		}
		
		
		//print_r($sessao_pedido);
		
		if($_POST)
		{
			if($this->input->post('continuar'))
			{
			
				if(!$this->input->post("autorizado") AND ($sessao_pedido['tipo_pedido'] != 'V'))
				{
					$erro = " Preencha o campo <b>Pedido autorizado por</b>.";
				}
				else if(!$this->input->post("codigo_forma_pagamento"))
				{
					$erro = "Selecione uma forma de pagamento.";
				}
				else if ($this->input->post('tipo_frete') == 'FOB' && !$this->input->post('codigo_transportadora'))
				{
					$erro = 'Selecione uma transportadora.';
				}
				else if ($this->input->post('tipo_frete') == 'Redespacho' && !$this->input->post('codigo_transportadora'))
				{
					$erro = 'Selecione uma transportadora.';
				}
				else
				{

					$dados_sessao = $sessao_pedido;
				
					$dados_sessao['codigo_forma_pagamento'] = $this->input->post('codigo_forma_pagamento');
					$dados_sessao['tipo_frete'] = $this->input->post('tipo_frete');
					$dados_sessao['id_feira'] = $this->input->post('id_feira');
					$dados_sessao['codigo_transportadora'] = $this->input->post('codigo_transportadora');
					$dados_sessao['codigo_ordem_compra'] = $this->input->post('codigo_ordem_compra');
					$dados_sessao['data_entrega'] = $this->input->post('data_entrega');
					$dados_sessao['observacao_pedido_imediato'] = $this->input->post('observacao_pedido_imediato');		
					$dados_sessao['autorizado'] = $this->input->post('autorizado');	
					$dados_sessao['cultura'] = $this->input->post('cultura');							

					$this->session->set_userdata('sessao_pedido', $dados_sessao);
					
					redirect('criar_pedidos/confirmacao');
					
				}
				
			}
		}
		
		// Obtendo Posts pela Sessao
		$_POST['codigo_forma_pagamento'] = $sessao_pedido['codigo_forma_pagamento'];
		$_POST['tipo_frete'] = $sessao_pedido['tipo_frete'];
		$_POST['id_feira'] = $sessao_pedido['id_feira'];
		$_POST['codigo_transportadora'] = $sessao_pedido['codigo_transportadora'];
		$_POST['codigo_ordem_compra'] = $sessao_pedido['codigo_ordem_compra'];
		$_POST['data_entrega'] = ($sessao_pedido['data_entrega'] == '31/12/1969' ? '' : $sessao_pedido['data_entrega']);
		$_POST['observacao_pedido_imediato'] = $sessao_pedido['observacao_pedido_imediato'];
		
		$usuario = $this->db->from('usuarios')->where('codigo', $sessao_pedido['codigo_usuario'])->get()->row();
		
		$tabela_precos = $this->db_cliente->obter_tabela_precos($sessao_pedido["codigo_tabela_precos"]);
		
		$cultura = array(
			'1' => 'Arroz',
			'2' => 'Banana',
			'3' => 'Hortifruti',
			'4' => 'Milho',
			'5' => 'Outros'
		);
	
		$this->load->view('layout', array('conteudo' => $this->load->view('criar_pedidos/outras_informacoes', 
					array
					(
						'erro' => $erro,
						'obter_formas_pagamento' => $this->_obter_formas_pagamento(),
						'obter_transportadoras' => $this->_obter_transportadoras(),
						'eventos' => hnordt_gerar_feiras_ativas(), // Evento,
						'pedido' => $sessao_pedido,
						'usuario' => $usuario,
						'tabela_precos' => $tabela_precos,
						'cultura' => $cultura
					), 
					TRUE
				)
			)
		);
	}
	
	
	// Etapa 4
	function confirmacao()
	{
	
		$pedido = $this->session->userdata('sessao_pedido');
		
		/*
		echo '<pre>';
		print_r($pedido);
		echo '</pre>';
		*/
		
		
		if(!$pedido['codigo_forma_pagamento'] || !$pedido['tipo_frete'])
		{
			redirect('criar_pedidos/outras_informacoes');
		}
		
		
		if($this->input->post('confirmar'))
		{
			
			// Se existir id_pedido editar, se não inserir
			if($pedido['id_pedido'])
			{
				$id_pedido = $this->db_cliente->editar_pedido($pedido);
				
				if($id_pedido)
				{
					$this->session->unset_userdata('sessao_pedido');
					redirect('pedidos/ver_detalhes/' . $id_pedido);
					//redirect('criar_pedidos');
				}
			}
			else
			{
				$id_pedido = $this->db_cliente->exportar_pedido($pedido);
				
				if($id_pedido)
				{
					$this->session->unset_userdata('sessao_pedido');
					redirect('pedidos/ver_detalhes/' . $id_pedido);
					//redirect('criar_pedidos');
				}
			}
			
		}
		
		
		$produtos = $pedido['produtos'];
		$usuario = $this->db->from('usuarios')->where('codigo', $pedido['codigo_usuario'])->get()->row();
		$formas_pagamento = $this->db_cliente->obter_formas_pagamento($pedido['codigo_forma_pagamento']);
		$evento = $this->db->from('feiras')->where('id', $pedido['id_feira'])->get()->row();
		$transportadora = $this->db_cliente->obter_transportadoras($pedido['codigo_transportadora']);
		
		$tabela_precos = $this->db_cliente->obter_tabela_precos($pedido["codigo_tabela_precos"]);
		
		$this->load->view('layout', array('conteudo' => $this->load->view('criar_pedidos/confirmacao', 
					array
					(
						'pedido' => $pedido,
						'produto' => $produto,
						'usuario' => $usuario,
						'formas_pagamento' => $formas_pagamento[0]['descricao'],
						'evento' => $evento,
						'transportadora' => $transportadora[0]['nome'],
						'produtos' => $produtos,
						'tabela_precos' => $tabela_precos
					), 
					TRUE
				)
			)
		);
		
	}
	
	
	//----------------------------------------------------------------------------------------------------------------------------------
	
	
	function excluir_produto()
	{
	
		$indice = $_POST['indice'];
		
		if($indice != '')
		{
		
			$sessao_pedido = $this->session->userdata('sessao_pedido');
			//$produtos = $sessao_pedido['produtos'];
			$produtos = array_reverse($sessao_pedido['produtos']);
			
			unset($produtos[$indice]);
			
			$sessao_pedido['produtos'] = $produtos;

						
			$dados_sessao = $sessao_pedido;
			
			$this->session->set_userdata('sessao_pedido', $dados_sessao);
			
			echo '1';
		
		}
		else
		{
			echo '0';
		}
	}
	
	function alterar_quantidade()
	{
		$indice = $_POST['indice'];
		$quantidade = $_POST['quantidade'];
		
		if(($indice != '') && ($quantidade != ''))
		{
		
			$sessao_pedido = $this->session->userdata('sessao_pedido');
			$produtos = $sessao_pedido['produtos'];
			
			$produtos[$indice]['quantidade_pedido'] = $quantidade;

			// Calculando Totais
			$produtos[$indice]['valor_st'] =  (($produtos[$indice]['st'] / 100) * ($produtos[$indice]['preco'] * $produtos[$indice]['quantidade_pedido']) );
			$produtos[$indice]['total_ipi'] = (($produtos[$indice]['ipi'] / 100) * ($produtos[$indice]['preco'] * $produtos[$indice]['quantidade_pedido'])) + ($produtos[$indice]['preco'] * $produtos[$indice]['quantidade_pedido']);
			$produtos[$indice]['valor_desconto'] = (($produtos[$indice]['desconto'] / 100) * ($produtos[$indice]['preco'] * $produtos[$indice]['quantidade_pedido']));
			
			$sessao_pedido['produtos'] = $produtos;
		
			$dados_sessao = $sessao_pedido;
			
			$this->session->set_userdata('sessao_pedido', $dados_sessao);
			
			echo '1';

		}
		else
		{
			echo '0';
		}
		
	}
	
	function alterar_frete()
	{
		$frete = $_POST['frete'];
		
		if($frete)
		{
			$sessao_pedido = $this->session->userdata('sessao_pedido');
			$valor_frete = $sessao_pedido['valor_frete'];
			
			$sessao_pedido['valor_frete'] = $frete;
			$sessao_pedido['valor_total_frete'] = $sessao_pedido['valor_frete'] + $sessao_pedido['valor_total_ipi'];	
			
			$dados_sessao = $sessao_pedido;
			
			$this->session->set_userdata('sessao_pedido', $dados_sessao);
		
			echo '1';
		}
		else
		{
			echo '0';
		}
		
	}
	
	function obter_clientes($codigo_do_representante = NULL)
	{
		$palavras_chave = addslashes($_GET['term']);
		
		$dados = array();
		
		$clientes = $this->autocomplete->obter_clientes($palavras_chave, $codigo_do_representante);
		
		foreach ($clientes as $cliente)
		{
			$dados[] = array('label' => $cliente['codigo'] . ' - ' . $cliente['nome'] . ' - ' . $cliente['cpf'], 'value' => $cliente['codigo'] . '|' . $cliente['loja']);
		}
		
		
		echo json_encode($dados);
	}
	
	function obter_prospects($codigo_do_representante = NULL)
	{
		$palavras_chave = addslashes($_GET['term']);
		
		$dados = array();
		
		$prospects = $this->autocomplete->obter_prospects($palavras_chave, $codigo_do_representante);
		
		foreach ($prospects  as $prospect)
		{
			
			if(!$prospect['nome'])
			{
				$prospect['nome'] = $prospect['nome_fantasia'];
			}
		
			$dados[] = array('label' => $prospect['nome'] . ' - ' . $prospect['cpf'], 'value' => $prospect['id']);
		}
		
		
		echo json_encode($dados);
	}
	
	function obter_produtos($codigo_tabela_precos = NULL, $unidade = NULL)
	{
		$palavras_chave = addslashes($_GET['term']);
		
		$dados = array();
		
		$_produtos = array();
		
		$produtos = $this->autocomplete->obter_produtos_tabela_preco($palavras_chave, NULL, $codigo_tabela_precos, $unidade);

		if($produtos)
		{
			foreach ($produtos as $produto)
			{
				$dados[] = array('label' => $produto['codigo'] . ' - ' . $produto['descricao']  . ' - Estoque atual: ' .  number_format(($produto['estoque_atual'] > 0 ? $produto['estoque_atual'] : 0), 2, ',', '.') . ' - Desconto máximo: ' . number_format($produto['validacao_desconto'], 2, ',', '.') . '%', 'value' => $produto['codigo']);
			}
		}
		
		
		echo json_encode($dados);
		
	}
	
	function _obter_tabelas_precos($codigo_usuario)
	{
		$tabelas_precos = $this->db_cliente->obter_tabelas_precos();
		$_tabelas_precos = array();
		
		$_tabelas_precos[] = "Selecione";
		
		$tabela_precos_usuario = $this->db->from('usuarios')->where('codigo', $codigo_usuario)->get()->row()->tabela_precos;
		$_tabela_precos_usuario = unserialize($tabela_precos_usuario);
		
		if($_tabela_precos_usuario)
		{
			foreach ($tabelas_precos as $tabela_precos)
			{
				
				foreach($_tabela_precos_usuario as $codigo_tabela_precos)
				{
					if($codigo_tabela_precos == $tabela_precos['codigo'])
					{
						$_tabelas_precos[$tabela_precos['codigo']] = $tabela_precos['descricao'];
					}
				}

			}
		}
		
		return $_tabelas_precos;
	}
	
	function _obter_formas_pagamento()
	{
		$formas_pagamento = $this->db_cliente->obter_formas_pagamento();
		$_formas_pagamento = array();
		
		foreach ($formas_pagamento as $forma_pagamento)
		{
			$_formas_pagamento[$forma_pagamento['codigo']] = $forma_pagamento['descricao'];
		}
		
		return $_formas_pagamento;
	}
	
	function _obter_transportadoras()
	{
		$transportadoras = $this->db_cliente->obter_transportadoras();
		$_transportadoras = array();
		
		foreach ($transportadoras as $transportadora)
		{
			$_transportadoras[$transportadora['codigo']] = $transportadora['nome'];
		}
		
		return $_transportadoras;
	}
	
	function obter_valor_frete($total_peso, $cliente, $teste_tabela_preco = FALSE)
	{
		
		$cliente = explode('|', $cliente);
		
		if(!$teste_tabela_preco)
		{
			$tabela_preco = $this->db_cliente->obter_tabela_preco($cliente[0], $cliente[1]);
		}
		else
		{
			$tabela_preco = 'a';
		}
		
		if($tabela_preco['tabela_frete'] == 'N')
		{
			$valor_frete = 0;
		}
		else
		{
		
			//Valor do Frete
			if($total_peso <= 600)
			{
				$frete = $this->db->select(strtolower($tabela_preco['tabela_frete']))->from('tabela_frete')->where(array('kg' => 1))->get()->row_array();
			
				$valor_frete = $frete[strtolower($tabela_preco['tabela_frete'])];
			}
			else if($total_peso >= 601 && $total_peso <= 999)
			{
				$frete = $this->db->select(strtolower($tabela_preco['tabela_frete']))->from('tabela_frete')->where(array('kg' => 601))->get()->row_array();
			
				if($tabela_preco['tabela_frete'] == 'I')
				{
					$valor_frete = $frete[strtolower($tabela_preco['tabela_frete'])];
				}
				else
				{
					$valor_frete = $total_peso * $frete[strtolower($tabela_preco['tabela_frete'])];
				}
			}
			else if($total_peso >= 1000 && $total_peso <= 1999)
			{
				$frete = $this->db->select(strtolower($tabela_preco['tabela_frete']))->from('tabela_frete')->where(array('kg' => 1000))->get()->row_array();
			
				$valor_frete = $total_peso * $frete[strtolower($tabela_preco['tabela_frete'])];
			}
			else if($total_peso >= 2000 && $total_peso <= 2999)
			{
				$frete = $this->db->select(strtolower($tabela_preco['tabela_frete']))->from('tabela_frete')->where(array('kg' => 2000))->get()->row_array();
			
				$valor_frete = $total_peso * $frete[strtolower($tabela_preco['tabela_frete'])];
			}
			else if($total_peso >= 3000 && $total_peso <= 9999)
			{
				$frete = $this->db->select(strtolower($tabela_preco['tabela_frete']))->from('tabela_frete')->where(array('kg' => 3000))->get()->row_array();
			
				$valor_frete = $total_peso * $frete[strtolower($tabela_preco['tabela_frete'])];
			}
			else if($total_peso >= 10000)
			{
				$frete = $this->db->select(strtolower($tabela_preco['tabela_frete']))->from('tabela_frete')->where(array('kg' => 10000))->get()->row_array();
			
				$valor_frete = $total_peso * $frete[strtolower($tabela_preco['tabela_frete'])];
			}
		}
	
		return $valor_frete;
		
	}
	
	
	function cancelar_pedido()
	{
		
		$sessao_pedido = $this->session->userdata('sessao_pedido');
	
		$this->session->unset_userdata('sessao_pedido');
		redirect('criar_pedidos/informacoes_cliente/' . $sessao_pedido['tipo']);
	}
	
	
	function obter_informacoes_cliente_ajax($codigo_loja_cliente = NULL)
	{
		$dados = array(
			'cliente_encontrado' => FALSE,
			'html' => NULL
		);
		
		$codigo_loja_cliente = explode('_', $codigo_loja_cliente);
		$codigo_cliente = $codigo_loja_cliente[0];
		$loja_cliente = $codigo_loja_cliente[1];
		
		if ($codigo_cliente && $loja_cliente)
		{
			$cliente = $this->db_cliente->obter_cliente($codigo_cliente, $loja_cliente);
		}
		
		if ($cliente)
		{
			$dados['cliente_encontrado'] = TRUE;
			
			$dados['html'] = '
				<p><strong>' . $cliente['nome'] . '</strong> - ' . $cliente['cpf'] . '</p>
				
				<ul style="float: left; margin-right: 10px;">
					<li><strong>Limite de crédito:</strong> R$ ' . number_format($cliente['limite_credito'], 2, ',', '.') . '</li>
					<li><strong>Títulos em aberto:</strong> R$ ' . number_format($cliente['total_titulos_em_aberto'], 2, ',', '.') . '</li>
					<li><strong>Títulos vencidos:</strong> R$ ' . number_format($cliente['total_titulos_vencidos'], 2, ',', '.') . '</li>
				</ul>
				
				<ul style="float: left; margin-right: 10px;">
					<li><strong>Endereço:</strong> ' . $cliente['endereco'] . '</li>
					<li><strong>Bairro:</strong> ' . $cliente['bairro'] . '</li>
					<li><strong>CEP:</strong> ' . $cliente['cep'] . '</li>
				</ul>
				
				<ul style="float: left; margin-right: 10px;">
					<li><strong>Cidade:</strong> ' . $cliente['cidade'] . '</li>
					<li><strong>Estado:</strong> ' . $cliente['estado'] . '</li>
				</ul>
				
				<div style="clear: both;"></div>
			';
		}
		
		echo json_encode($dados);
	}
	
	//Recebe os dados do pedidos da versão off-line e grava no portal
	function grava_pedidos_offline(){
		
				
		$arquivo = fopen("teste.txt","a+");
		fwrite($arquivo,"TESTE\n".$_POST["valor"]);
		echo "Isso eh um teste: ".$_POST["valor"];
	
	}
	
	function obter_descricao_unidade($id)
	{
		$unidade = $this->db->from('unidades')->where('filial', $id)->get()->row();
		
		return $unidade->descricao;
	}
	
	function obter_unidades($grupo)
	{
		$unidades = $this->db->from('unidades')->where('grupo', $grupo)->get()->result();
		
		if($unidades)
		{
			foreach($unidades as $unidade)
			{
				$_unidades[$unidade->filial] = $unidade->filial . ' - ' . $unidade->descricao;
			}
		
			return form_dropdown('unidade', $_unidades);
		}
		else
		{
			$_unidades[] = 'Não existe unidades';
			return form_dropdown('unidade', $_unidades);
		}
		
	}
	
	function obter_preco_produto()
	{
		$codigo_produto = $_POST['codigo_produto'];
		$codigo_tabela_precos = $_POST['codigo_tabela_precos'];
	
		if($codigo_produto && $codigo_tabela_precos)
		{
			$dados_produto = $this->db_cliente->obter_produto($codigo_produto, $codigo_tabela_precos);
			echo $dados_produto['preco'];
		}
		else
		{
			echo 0;
		}
		
		
	}
	
	
}