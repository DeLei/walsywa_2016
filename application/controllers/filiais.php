<?php

class Filiais extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	
	function index()
	{
		$filiais = $this->db->from('filiais')->get()->result();
		
		$conteudo = heading('Filiais', 2);
		$conteudo .= '<p>' . anchor('filiais/criar', 'Cadastrar Nova Filial') . '</p>';
		$conteudo .= '<table cellspacing="0" class="novo_grid" style="margin-top: 10px;"><thead><tr><th>Criação</th><th>Status</th><th>Revenda</th><th>Nome</th><th>Opções</th></tr></thead><tbody>';
		foreach ($filiais as $filial)
		{
			$revenda = $this->db_cliente->obter_representante($filial->codigo_revenda);
			
			$conteudo .= '<tr><td class="center">' . date('d/m/Y', $filial->timestamp) . '</td><td class="center">' . element($filial->status, $this->_obter_status()) . '</td><td>' . $revenda['nome'] . '</td><td>' . $filial->nome . '</td><td class="center">' . anchor('filiais/editar/' . $filial->id, 'Editar') . '</td></tr>';
		}
		$conteudo .= '</tbody></table>';
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function criar()
	{
		// ** VALIDAR FORM
		if ($_POST)
		{
			if (!$this->input->post('codigo_revenda'))
			{
				$erro = 'Selecione uma revenda.';
			}
			else if (!$this->input->post('nome'))
			{
				$erro = 'Digite um nome.';
			}
			else if ($this->db->from('filiais')->where(array('codigo_revenda' => $this->input->post('codigo_revenda'), 'nome' => $this->input->post('nome')))->get()->row())
			{
				$erro = 'O nome digitado já existe.';
			}
			else
			{
				$this->db->insert('filiais', array(
					'timestamp' => time(),
					'status' => 'ativa',
					'codigo_revenda' => $this->input->post('codigo_revenda'),
					'nome' => $this->input->post('nome')
				));
				
				redirect('filiais');
			}
		}
		// VALIDAR FORM **
		
		// ** EXIBIR FORM
		$conteudo .= heading('Cadastrar Filial', 2);
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		$conteudo .= form_open_multipart(current_url());
		$conteudo .= '<p>' . form_label('Revenda:' . br() . form_dropdown('codigo_revenda', $this->_obter_revendas(), $this->input->post('codigo_revenda'), 'style="width: 300px;"')) . '</p>';
		$conteudo .= '<p>' . form_label('Nome:' . br() . form_input('nome', $this->input->post('nome'), 'size="50"')) . '</p>';
		$conteudo .= '<p>' . form_submit('concluir', 'Concluir') . ' ou ' . anchor('filiais', 'Cancelar') . '</p>';
		$conteudo .= form_close();
		// EXIBIR FORM **
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function editar($id = NULL)
	{
		if (!$id)
		{
			redirect();	
		}
		
		$filial = $this->db->from('filiais')->where('id', $id)->get()->row();
		
		// ** VALIDAR FORM
		if ($_POST)
		{
			if (!$this->input->post('codigo_revenda'))
			{
				$erro = 'Selecione uma revenda.';
			}
			else if (!$this->input->post('nome'))
			{
				$erro = 'Digite um nome.';
			}
			else if ($this->db->from('filiais')->where(array('codigo_revenda' => $this->input->post('codigo_revenda'), 'nome' => $this->input->post('nome')))->get()->row())
			{
				$erro = 'O nome digitado já existe.';
			}
			else
			{
				$this->db->update('filiais', array(
					'status' => $this->input->post('status'),
					'codigo_revenda' => $this->input->post('codigo_revenda'),
					'nome' => $this->input->post('nome')
				), array('id' => $id));
				
				redirect('filiais');
			}
		}
		else
		{
			foreach ($filial as $indice => $valor)
			{
				$_POST[$indice] = $valor;
			}
		}
		// VALIDAR FORM **
		
		// ** EXIBIR FORM
		$conteudo .= heading('Editar Filial', 2);
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		$conteudo .= form_open_multipart(current_url());
		$conteudo .= '<p>' . form_label('Status:' . br() . form_dropdown('status', $this->_obter_status(), $this->input->post('status'))) . '</p>';
		$conteudo .= '<p>' . form_label('Revenda:' . br() . form_dropdown('codigo_revenda', $this->_obter_revendas(), $this->input->post('codigo_revenda'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" style="width: 300px;"')) . '</p>';
		$conteudo .= '<p>' . form_label('Nome:' . br() . form_input('nome', $this->input->post('nome'), 'size="50"')) . '</p>';
		$conteudo .= '<p>' . form_submit('concluir', 'Concluir') . ' ou ' . anchor('filiais', 'Cancelar') . '</p>';
		$conteudo .= form_close();
		// EXIBIR FORM **
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	//
	
	function _obter_status()
	{
		return array('ativa' => 'Ativa', 'inativa' => 'Inativa');
	}
	
	function _obter_revendas()
	{
		$revendas = $this->db_cliente->obter_representantes();
		$_revendas = array();
		
		foreach ($revendas as $revenda)
		{
			if ($this->db->from('usuarios')->where(array('grupo' => 'revendas', 'codigo' => $revenda['codigo']))->get()->row())
			{
				$_revendas[$revenda['codigo']] = $revenda['nome'] . ' - ' . $revenda['codigo'];
			}
		}
		
		return $_revendas;
	}
	
}