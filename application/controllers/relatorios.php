<?php

class Relatorios extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	
	private function retorna_total_registro_acessos()
	{
		$r = $this->db->select("distinct( id_usuario)
				FROM (`acessos_usuarios`)
				GROUP BY  `id_usuario`")
			->get()
			->result();
		$total_registro = 0;
		foreach ($r as $acesso){
			$total_registro ++;
		}
		return($total_registro);
	}
	
	function resultados_representantes()
	{
		$conteudo = heading('Resultados do Representante', 2);
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		$conteudo .= form_open(current_url());
		$conteudo .= '<p>' . form_label('Representante: ' . br() . form_dropdown('codigo_usuario', $this->_obter_representantes(), $this->input->post('codigo_usuario'))) . '</p>';
		$conteudo .= '<p>' . form_label('Data:' . br() . form_input('data_inicial', $this->input->post('data_inicial'), 'class="datepicker data_inicial" size="6"')) . ' à ' . form_input('data_final', $this->input->post('data_final'), 'class="datepicker" size="6"') . '</p>';
		$conteudo .= '<p>' . form_submit('consultar', 'Consultar') . '</p>';
		$conteudo .= form_close();
		
		if ($_POST)
		{
			if ($this->input->post('data_inicial'))
			{
				$data_inicial = explode('/', $this->input->post('data_inicial'));
				$inicio_timestamp = mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
			}
			
			if ($this->input->post('data_final'))
			{
				$data_final = explode('/', $this->input->post('data_final'));
				$fim_timestamp = mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]);
			}
			
			$conteudo .= '
				<div class="topo_grid"></div>
				<table cellspacing="0" class="novo_grid">
					<thead>
						<tr>
							<th>Cód. Repres.</th>
							<th>Nome</th>
							<th>Metas</th>
							<th>Realizado</th>
							<th>%</th>
							<th>Premiações</th>
							<th>Positivação Clientes</th>
							<th>Orçamentos Convertidos</th>
						</tr>
					</thead>
					
					<tbody>
			';
			
			//
			
			if ($this->input->post('codigo_usuario'))
			{
				$representantes[$this->input->post('codigo_usuario')] = NULL;
			}
			else
			{
				$representantes = $this->_obter_representantes();
			}
			$_representantes = array();
			foreach ($representantes as $indice => $valor)
			{
				$_representantes[] = $this->db->from('usuarios')->where('id', $indice)->get()->row();
			}
			$representantes = $_representantes;
			
			//
			
			foreach ($representantes as $representante)
			{
				$total_meta_vendas = $this->db_cliente->obter_total_meta_vendas($representante->codigo, $inicio_timestamp, $fim_timestamp);
				$total_vendas = $this->db_cliente->obter_total_vendas($representante->codigo, $inicio_timestamp, $fim_timestamp);
				$total_premiacao_vendas = $this->db_cliente->obter_total_premiacao_vendas($representante->codigo, $inicio_timestamp, $fim_timestamp);
				$total_prospects_convertidos = $this->db->from('prospects')->where(array('status' => 'convertido_cliente', 'timestamp >=' => ($inicio_timestamp ? $inicio_timestamp : 0), 'timestamp <=' => ($fim_timestamp ? $fim_timestamp : 0)))->get()->num_rows();
				
				$conteudo .= '
					<tr>
						<td class="right">' . $representante->codigo . '</td>
						<td>' . $representante->nome . '</td>
						<td class="right">' . number_format($total_meta_vendas, 2, ',', '.') . '</td>
						<td class="right">' . number_format($total_vendas, 2, ',', '.') . '</td>
						<td class="right">' . number_format($total_meta_vendas > 0 ? (($total_vendas / $total_meta_vendas) * 100) : 0, 2, ',', '.') . '</td>
						<td class="right">' . number_format($total_premiacao_vendas, 2, ',', '.') . '</td>
						<td class="right">' . $total_prospects_convertidos . '</td>
						<td class="right">{0}</td>
					</tr>
				';
			}
			
			$conteudo .= '</tbody></table>';
		}
		
		$conteudo .= '<div class="rodape_grid"></div>';
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function relatorio_acessos()
	{
		$conteudo .= '<div class="caixa"><ul>';
		$conteudo .= '<li>' . anchor('#', 'Imprimir', 'onclick="print(); return false;"') . '</li>';
		$conteudo .= '</ul></div><br>';
		
		$conteudo .= heading('Relatório de Acessos', 2).'<br>';
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		//$conteudo .= form_open(current_url());
		
		//if($this->input->get('data_inicial') || $this->input->get('data_final'))
		if(isset($_POST) || isset($_GET))
		{
			$filtros = array(
				array(
					'nome' => 'usuario',
					'descricao' => 'Usuário',
					'tipo' => 'texto',
					'campo_mysql' => 'usuarios.nome',
					'ordenar' => 1
				),
				/*
				array(
					'nome' => 'primeiro_acesso',
					'descricao' => 'Primeiro acesso',
					'tipo' => 'data'
				),
				*/
				array(
					'nome' => 'ultimo_acesso',
					'descricao' => 'Último acesso',
					'tipo' => 'data'
				)
			);

			
			$this->filtragem_mysql($filtros);
			
			// RETORNA CONSULTA COM TODOS OS USUARIOS ENCONTRADOS
			//$this->filtragem_mysql($filtros);

			$this->db->group_by("id_usuario"); 
			$_acessos = $this->db
				->select('COUNT(acessos_usuarios.id_usuario) as ranking, acessos_usuarios.id_usuario, acessos_usuarios.timestamp, usuarios.nome, acessos_usuarios.ip_externo, acessos_usuarios.data_hora_login, acessos_usuarios.data_hora_logout')
				->from('acessos_usuarios')
				->join('usuarios',
					'usuarios.id = acessos_usuarios.id_usuario'
				)
				//->where("ip_externo !=", "''")
				->order_by("timestamp", "desc")
				->limit(20, $this->input->get('per_page'))
				->get()
				->result();
				
			//echo $this->db->last_query();
			//die();
			//echo "<pre>";
			//echo var_dump($_acessos);
			//echo "</pre>";
			//die();
			
			
			$acessos = null;
			
			if($_GET['primeiro_acesso_inicial'] && $_GET['primeiro_acesso_final'])
			{
				foreach($_acessos as $acesso)
				{
					$primeiro_acesso = $this->db->from('acessos_usuarios')->where(array('id_usuario' => $acesso->id_usuario))->order_by("timestamp", "asc")->limit(1)->get()->row()->timestamp;
					$primeiro_acesso_inicial = $this->_data2timestamp($_GET['primeiro_acesso_inicial']);
					$primeiro_acesso_final = $this->_data2timestamp($_GET['primeiro_acesso_final'], TRUE);
					
					if($primeiro_acesso >= $primeiro_acesso_inicial AND $primeiro_acesso <= $primeiro_acesso_final)
					{
						$acessos[] = $acesso;
					}
					
				}
			}
			else if($_GET['ultimo_acesso_inicial'] && $_GET['ultimo_acesso_final'])
			{
				foreach($_acessos as $acesso)
				{
					$ultimo_acesso = $this->db->from('acessos_usuarios')->where(array('id_usuario' => $acesso->id_usuario))->order_by("timestamp", "desc")->limit(1)->get()->row()->timestamp;
					$ultimo_acesso_inicial = $this->_data2timestamp($_GET['ultimo_acesso_inicial']);
					$ultimo_acesso_final = $this->_data2timestamp($_GET['ultimo_acesso_final'], TRUE);
					
					if($ultimo_acesso >= $ultimo_acesso_inicial AND $ultimo_acesso <= $ultimo_acesso_final)
					{
						$acessos[] = $acesso;
					}
					
				}
			}
			else
			{
			
				$acessos = $_acessos;
			}
			
			
			$paginacao = $this->paginacao(count($acessos));
			
			$this->load->view('layout', 
				array(
					'conteudo' => $this->load->view('relatorios/relatorio_acessos', 
						array(
							'acessos' => $acessos,  
							'filtragem' => $this->filtragem($filtros), 
							'paginacao' => $paginacao, 
							'total' => count($acessos)
						), TRUE
					)
				)
			);
			
			//$consulta = $this->retorna_relatorios_acessos($acessos, $limit_consulta);
			//$paginacao = $this->paginacao_total($consulta[2], $limit_consulta);
			
			
		}
		
		/*
		$conteudo .= '<div class="topo_grid">';
		$conteudo .= '	<p><strong>Filtros</strong></p>';
		
		$conteudo .= form_open(current_url(), array('method' => 'get'));
		
		$conteudo .= $this->filtragem($filtros);
		
		$conteudo .= '<div style="clear: both;"></div>';
	
		$conteudo .= form_close();
		
		$conteudo .= '</div>';
		
		$conteudo .= $consulta[0];
		
		$conteudo .= $paginacao;
		
		$this->load->view('layout',array('conteudo' => $conteudo));
		*/
	}
	
	private function retorna_registro_acesso($acesso, $nome, $ranking)
	{
		return('
			<tr>
				<td class="center">' . $ranking . 'º</td>
				<td>' . $nome . '</td>
				<td class="right">' . $this->db->select('COUNT(id_usuario) as total')->from('acessos_usuarios')->where(array('id_usuario' => $acesso->id_usuario))->get()->row()->total . '</td>
				<td class="center">' . date('d/m/y H:i:s', $this->db->from('acessos_usuarios')->where(array('id_usuario' => $acesso->id_usuario))->order_by("timestamp", "asc")->limit(1)->get()->row()->timestamp) . '</td>
				<td class="center">' . date('d/m/y H:i:s', $this->db->from('acessos_usuarios')->where(array('id_usuario' => $acesso->id_usuario))->order_by("timestamp", "desc")->limit(1)->get()->row()->timestamp) . '</td>
				<td class="nao_exibir_impressao center">' . anchor('relatorios/ver_acessos/' . $acesso->id_usuario, 'Ver Detalhes') .  '</td>
			</tr>');
	}
	
	private function retorna_relatorios_acessos($acessos, $limit_consulta)
	{
		if($this->input->get('data_inicial') != '' && $this->input->get('data_final') != '')
		{
			$tabela_cabecalho  = $this->input->get('data_inicial');
			$tabela_cabecalho .= $this->input->get('data_final');
		}
		
		$tabela_cabecalho .=
			'<table class="novo_grid">
				<thead>
					<tr>
						<th>Raking</th>
						<th>Usuário</th>
						<th>Acessos Totais</th>
						<th>Primeiro Acesso</th>
						<th>Último Acesso</th>
						<th class="nao_exibir_impressao">Opções</th>
					</tr>
				</thead>
				<tbody>';
		
		$i = $this->paginacao_contador_asc();
		$limite_imprimir = ($i + $limit_consulta) -1;
		$tabela_corpo = '';
		$ranking = 0;
		$total_registro_nome = 1;
		foreach ($acessos as $acesso)
		{
			$ranking ++;
			
			if($this->input->get('usuario'))
			{
				$j = $this->retorna_busca_por_nome($acesso, $this->input->get('usuario'));
				if($j[0]['nome'])
				{
					if($total_registro_nome >= $i && $i <= $limite_imprimir)
					{
						$tabela_corpo .= $this->retorna_registro_acesso($acesso, $j[0]['nome'], $ranking);
						$i++;
					}
					$total_registro_nome ++;
				}
			}
			else if($this->input->get('data_inicial') != '' && $this->input->get('data_final') != '')
			{
				////////////////////////////////////////
				
				if ($this->input->get('data_inicial'))
				{
					$data_inicial = explode('/', $this->input->get('data_inicial'));
					$inicio_timestamp = mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
				}
				
				if ($this->input->get('data_final'))
				{
					$data_final = explode('/', $this->input->get('data_final'));
					$fim_timestamp = mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]);
				}
				
				//if ($data_inicial && $data_final)
					//$this->db->where(array('id_usuario' => $id, 'timestamp >= ' => $inicio_timestamp, 'timestamp <= ' => $fim_timestamp));
				/*
				$limit_consulta = 20;
				$acess = $this->db
					->from('acessos_usuarios')
					//->where('id_usuario', $id)
					->where(array('id_usuario' => $acesso->id_usuario, 'timestamp >= ' => $inicio_timestamp, 'timestamp <= ' => $fim_timestamp))
					->limit($limit_consulta, $this->input->get('per_page'))
					->order_by('timestamp', 'desc')->get()->result();
				*/
				
				$tabela_corpo .= '
					<tr>
						<td class="center">' . $ranking . 'º</td>
						<td>' . $nome . '</td>
						<td class="right">' . $this->db->select('COUNT(id_usuario) as total')->from('acessos_usuarios')->where(array('id_usuario' => $acess->id_usuario))->get()->row()->total . '</td>
						<td class="center">' . date('d/m/y H:i:s', $this->db->from('acessos_usuarios')->where(array('id_usuario' => $acess->id_usuario))->order_by("timestamp", "asc")->limit(1)->get()->row()->timestamp) . '</td>
						<td class="center">' . date('d/m/y H:i:s', $this->db->from('acessos_usuarios')->where(array('id_usuario' => $acess->id_usuario))->order_by("timestamp", "desc")->limit(1)->get()->row()->timestamp) . '</td>
						<td class="nao_exibir_impressao center">' . anchor('relatorios/ver_acessos/' . $acess->id_usuario, 'Ver Detalhes') .  '</td>
					</tr>';
				$total_registro_nome ++;
				
				////////////////////////////////////////
			}
			else if($ranking >= $i && $i <= $limite_imprimir)
				{
					$nome = $this->db->from('usuarios')->where(array('id' => $acesso->id_usuario))->get()->row()->nome;
					$tabela_corpo .= $this->retorna_registro_acesso($acesso, $nome, $i);
					$i++;
				}
		}
		
		$tabela_corpo.= '</tbody></table>';
		$conteudo[0] = $tabela_cabecalho . $tabela_corpo;
		
		if($this->input->get('usuario')){
			$conteudo[1] = $total_registro_nome;
			$conteudo[2] = $total_registro_nome -1;
		}
		else{
			$conteudo[1] = $ranking;
			$conteudo[2] = $ranking;
		}
		
		//if(!$conteudo[2])
		//	$conteudo[0] = null;
		
		//echo '<br>Total consultado:'.$conteudo[1];
		//echo 'Total de registro:'.$conteudo[2];
		
		return($conteudo);
	}
	
	private function retorna_busca_por_data($acesso, $inicio_timestamp, $fim_timestamp)
	{
		$query = $this->db->query("
			SELECT
				distinct(usuarios.id),
				usuarios.nome
			FROM usuarios, acessos_usuarios
			where usuarios.id = '". $acesso->id_usuario ."'
				AND timestamp >= '". $inicio_timestamp ."'
				AND timestamp <= '". $fim_timestamp ."'
			limit 1;");
		
		$j = $query->result_array();
		return($j);
	}

	private function retorna_busca_por_nome($acesso, $usuario_get)
	{/*
		$this->db->select('distinct(usuarios.id), usuarios.nome')
			->from('usuarios, acessos_usuarios');
			//->where(array('usuarios.id' => $acesso->id_usuario))
			//->like('usuarios.nome', $usuario_get);
			//->limit(1);
		$query = $this->db->get();
		return $query;*/
		//$query = $this->db->query("SELECT * FROM ".$this->tbPrincipal." where id = '".$id."' limit 1; ");
        //    return($query->result_array());
		
		$query = $this->db->query("
				SELECT
					distinct(usuarios.id),
					usuarios.nome
				FROM usuarios, acessos_usuarios
				where usuarios.id = '". $acesso->id_usuario ."'
					AND usuarios.nome like '%". $usuario_get ."%' 
				limit 1;");
		
        $j = $query->result_array();
		return($j);
	}
	
	function ver_acessos($id = NULL)
	{
		$usuario = $this->db->from('usuarios')->where('id', $id)->get()->row();
		
		$filtros = array(
			array(
				'nome' => 'numero',
				'descricao' => 'Número',
				'tipo' => 'texto',
				'campo_mysql' => 'numero',
				'ordenar' => 0
			),
			array(
				'nome' => 'data',
				'descricao' => 'Data',
				'tipo' => 'data',
				'campo_mysql' => 'timestamp',
				'ordenar' => 1
			)
		);
		
		$this->filtragem_mysql($filtros);
		
		//$acessos = $this->db->from('acessos_usuarios')->where('id_usuario', $usuario->id)->limit(20, $this->input->get('per_page'))->get()->result();
		$acessos = $this->db->from('acessos_usuarios')->where('id_usuario', $usuario->id)->order_by("timestamp", "desc")->limit(20, $this->input->get('per_page'))->get()->result();
		//echo $this->db->last_query();
		//die();
		//
		
		$this->filtragem_mysql($filtros);
		
		$total = $this->db->from('acessos_usuarios')->where('id_usuario', $usuario->id)->get()->num_rows();
		
		$paginacao = $this->paginacao($total);
		
		$this->load->view('layout', array('conteudo' => $this->load->view('relatorios/ver_acessos', array('acessos' => $acessos, 'filtragem' => $this->filtragem($filtros), 'paginacao' => $paginacao, 'total' => $total, 'usuario' => $usuario), TRUE)));
	}
	
	function _obter_representantes()
	{
		$representantes = $this->db->from('usuarios')->where('status', 'ativo')->where('grupo', 'representantes')->order_by('nome', 'asc')->get()->result();
		$_representantes = array();
		
		foreach ($representantes as $representante)
		{
			$_representantes[$representante->id] = $representante->nome . ' - ' . $representante->codigo;
		}
		
		return $_representantes;
	}
	
}