<?php

class Formulario_visita extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('autocomplete');
		$this->load->model('db_formulario_visita');
	}
	
	function index($codigo = NULL, $loja = NULL) {
		
		//Chamado 002663 - [CUSTOM] Melhoria busca cliente
		if($codigo && $loja) {
			
			$cliente = $this->db_cliente->obter_cliente($codigo, $loja);
			
			$_POST = array(
				'cliente_novo'			=>	'N',
				'input_cliente_novo'	=>	$cliente['nome'],
				'_input_cliente'		=>	$cliente['nome'],
				'codigo_cliente'		=>	$cliente['codigo'],
				'loja_cliente'			=>	$cliente['loja']
			);
			
		} else {
			$_POST = array(
				'edicao'				=>	false,
				'cliente_novo'			=>	'S',
				'classificacao_cliente' =>	'Prospect',
				'horario_proximo_passo' => 	'00:00'
				//'setor'					=>	'E'
			);
		}
		
		$codigo = $this->input->get('codigo') ? $this->input->get('codigo') : null;
		$salvo = $this->input->get('salvo') ? $this->input->get('salvo') : null;
		
		if(!is_null($codigo)){
		
			$form = $this->db_formulario_visita->obter_formulario_visita($codigo);
			//$clienteAC = $form['cliente'] . ' - ' . $form['nome_cliente'];
				$clienteAC = $form['nome_cliente'];
					
					
			$_POST = array(
				'edicao'				=>	true,
				'data_visitacao'		=>	protheus2data($form['data_visitacao']),
				'data_preenchimento'	=>	protheus2data($form['data_preenchimento']),
				'setor'					=>	$form['setor'],
				'cliente_novo'			=>	$form['cliente_novo'],
				'input_cliente_novo'	=>	$form['nome_cliente'],
				'_input_cliente'		=>	$clienteAC,
				'codigo_cliente'		=>	$form['cliente'],
				'loja_cliente'			=>	$form['loja_cliente'],
				'classificacao_cliente'	=>	$form['classificacao_cliente'],
				'telefone_cliente'		=>	"({$form['cliente_ddd']}) {$form['cliente_fone']}",
				'tipo_cliente'			=>	$form['tipo_cliente'],
				'cidade_regiao_cliente'	=>	$form['cidade_cliente'],
				'estado_cliente'		=>	$form['estado_cliente'],
				'cep_cliente'			=>	$form['cep_cliente'],
				'gerou_pedido_orcamento'=>	$form['gerou_pedido_orcamento'],
				'material_pdv'			=>	$form['material_pdv'],
				'obra'					=>	$form['obra'],
				'nome_obra'				=>	$form['nome_obra'],
				'escritorio'			=>	$form['escritorio'],
				'segmento'				=>	$form['segmento'],
				'fase_obra'				=>	$form['fase_obra'],
				'aplicacao'				=>	$form['aplicacao'],
				'viavel_proximo_passo'	=>	$form['viavel_proximo_passo'],
				'proximo_passo'			=>	$form['proximo_passo'],
				'viavel_proximo_passo'	=>	$form['viavel_proximo_passo'],
				'data_proximo_passo'	=>	protheus2data($form['data_proximo_passo']),
				'horario_proximo_passo'	=>	$form['hora_proximo_passo'],
				'resumo_reuniao'		=>	$form['resumo_reuniao'],
				'contatos'				=>	array(),
				'concorrentes'			=>	array(),
				'produtos'				=>	array(),
				'interesse'				=>	array(),
				'visita_coordenacao'	=> $form['tipo_visita'] == 'C' ? 'S' : 'N'
			);
			
		}
		
		$representante = $this->db_cliente->obter_representante($this->session->userdata('codigo_usuario'));
		
		$codigo_usuario		=	$this->session->userdata('codigo_usuario');
		$segmentos_obra		=	$this->db_formulario_visita->obter_segmentos_obra();	
		$tipos_cliente		=	$this->db_formulario_visita->obter_tipos_cliente();
		$tipos_contato		=	$this->db_formulario_visita->obter_tipos_contato();
				
		$this->load->view('layout', array('conteudo' => $this->load->view('formulario_visita/formulario', array(
			'codigo_usuario' 			=>		$codigo_usuario,
			'segmentos_obra'			=>		$segmentos_obra,
			'tipos_cliente'				=>		$tipos_cliente,
			'tipos_contato'				=>		$tipos_contato,
			'codigo_formulario'			=>		$codigo,
			'salvo'						=>		$salvo,
			'representante'				=>		$representante
		), TRUE)));
	}
	
	function obter_grid($codigo_formulario){
		
		$retorno = array(
			'contatos' 		=> $this->db_formulario_visita->obter_contatos_visita($codigo_formulario),
			'concorrentes' 	=> corrigir_acentos($this->db_formulario_visita->obter_concorrentes_formulario_visita($codigo_formulario)),
			'produtos' 		=> corrigir_acentos($this->db_formulario_visita->obter_produtos_apresentados($codigo_formulario)),
			'interesse' 	=> corrigir_acentos($this->db_formulario_visita->obter_interesse_treinamento($codigo_formulario))
		);

		echo json_encode($retorno);
	}
	
	function salvar_formulario() {
		
		$formulario_visita = array(
			'codigo_representante'					=>  $this->session->userdata('codigo_usuario'),
			'data_visitacao'						=>  $this->input->post('data_visitacao'),
			'data_preenchimento'					=> 	$this->input->post('data_preenchimento'),
			'setor'									=>	$this->input->post('setor'),
			'cliente_novo'							=>	$this->input->post('cliente_novo'),
			'telefone_cliente'						=>	$this->input->post('telefone_cliente'),
			'tipo_cliente'							=>	$this->input->post('tipo_cliente'),
			'cidade_regiao_cliente'					=>	$this->input->post('cidade_regiao_cliente'),
			'estado_cliente'						=>	$this->input->post('estado_cliente'),
			'cep_cliente'							=>	$this->input->post('cep_cliente'),
			'gerou_pedido_orcamento'				=>	$this->input->post('gerou_pedido_orcamento'),
			'grids'									=>	array(
				'grid_contatos'						=>  objectToArray(json_decode($this->input->post('tbl_contatos'))),
				'grid_concorrentes'					=>	objectToArray(json_decode($this->input->post('tbl_concorrentes'))),
				'grid_produtos'						=>	objectToArray(json_decode($this->input->post('tbl_produtos'))),
				'grid_interesse'					=>	objectToArray(json_decode($this->input->post('tbl_interesse')))
			)
		);
		if($this->input->post('codigo_formulario')){
			$formulario_visita['codigo_formulario'] = $this->input->post('codigo_formulario');
		}
		
		if($formulario_visita['cliente_novo'] == 'S') {
			$formulario_visita['cliente'] 								= $this->input->post('input_cliente_novo');
			$formulario_visita['classificacao_cliente']					= 'P';
		} else {
			$cli = (!$this->input->post('input_cliente')) ? $this->input->post('input_cliente_novo') : $this->input->post('input_cliente');
			$formulario_visita['cliente'] 					= $cli;
			$formulario_visita['codigo_cliente'] 			= $this->input->post('codigo_cliente');
			$formulario_visita['loja_cliente']              = $this->input->post('loja_cliente');
			$formulario_visita['classificacao_cliente'] 	= $this->input->post('classificacao_cliente');
		}
		
		$formulario_visita['obra']									=	$this->input->post('obra');
		$formulario_visita['nome_obra']								=	$this->input->post('nome_obra');
		$formulario_visita['escritorio']							=	$this->input->post('escritorio');
		$formulario_visita['segmento']								=	$this->input->post('segmento');
		$formulario_visita['fase_obra']								=	$this->input->post('fase_obra');
		$formulario_visita['aplicacao']								=	$this->input->post('aplicacao');
		$formulario_visita['viavel_proximo_passo']					=	$this->input->post('viavel_proximo_passo');
		$formulario_visita['proximo_passo']							=	$this->input->post('proximo_passo');
		$formulario_visita['data_proximo_passo']					=	$this->input->post('data_proximo_passo');
		$formulario_visita['horario_proximo_passo']					=	$this->input->post('horario_proximo_passo');
		$formulario_visita['resumo_reuniao']						=	$this->input->post('resumo_reuniao');

		$formulario_visita['material_pdv']							=	$this->input->post('material_pdv');
		
		$formulario_visita['tipo_visita']							=	($this->input->post('visita_coordenacao') == 'S' ? 'C' : 'N'); //C = Visita de coordenação | N = Visita Normal
		
		$this->validar_formulario_visita($formulario_visita);
	}
	
	function validar_formulario_visita($formulario_visita) {
		
		$formulario_visita['erros'] = array();
		
		$formulario_visita = $this->validar_campos_obrigatorios($formulario_visita);
				
		if($formulario_visita['data_visitacao']) {
			$formulario_visita = $this->validar_data_visitacao($formulario_visita);
		}		
		
		if($formulario_visita['setor'] == 'E') {
			$formulario_visita = $this->validar_campos_obrigatorios_engenharia($formulario_visita);
		}
				 
		if(count($formulario_visita['erros']) > 0) {
			$codigo_usuario		=	$this->session->userdata('codigo_usuario');
			$codigo_formulario	=	$formulario_visita['codigo_formulario'];
			$segmentos_obra		=	$this->db_formulario_visita->obter_segmentos_obra();
			$tipos_contato		=	$this->db_formulario_visita->obter_tipos_contato();	
			$tipos_cliente		=	$this->db_formulario_visita->obter_tipos_cliente();
		
		
			$this->load->view('layout', array('conteudo' => $this->load->view('formulario_visita/formulario', array(
				'codigo_usuario'		=>	$codigo_usuario, 
				'formulario_visita'		=>	$formulario_visita,
				'segmentos_obra'		=>	$segmentos_obra,
				'tipos_contato'			=>	$tipos_contato,
				'tipos_cliente'			=>	$tipos_cliente,
				'codigo_formulario'		=> 	$codigo_formulario,
				
			), TRUE)));
		} else {
			$form = $this->db_formulario_visita->exportar_formulario_visita($formulario_visita);
			if($form){
				redirect("formulario_visita/?codigo=$form&salvo=s");
			}
		}
	}	
	
	function validar_data_visitacao($formulario_visita) {
			
		//$data_atual = (string) Date('Y-m-d');
		$data_atual = DateTime::createFromFormat('d/m/Y', $formulario_visita['data_preenchimento']);
		
		//$data_temporaria = $formulario_visita['data_visitacao'];
		//$data_temporaria = str_replace('/', '-', $data_temporaria);
		
		//$data_visitacao  = $data_temporaria[6] . $data_temporaria[7] . $data_temporaria[8] . $data_temporaria[9] . '-' . $data_temporaria[3] . $data_temporaria[4] . '-' . $data_temporaria[0] . $data_temporaria[1]; 
		//$data_visitacao = new DateTime($data_visitacao);
		$data_visitacao = DateTime::createFromFormat('d/m/Y', $formulario_visita['data_visitacao']);
		
		$dias_diferenca  = $data_atual->diff($data_visitacao)->d;
		$meses_diferenca = $data_atual->diff($data_visitacao)->m;
		$anos_diferenca  = $data_atual->diff($data_visitacao)->y;
		
		if($dias_diferenca > 3 || $meses_diferenca != 0 || $anos_diferenca != 0) {
			array_push($formulario_visita['erros'], 'Data de Visitação inválida. O período máximo permitido é de três (03) dias para mais ou para menos.');
		}
		
		return $formulario_visita;
	}
	
	function validar_campos_obrigatorios($formulario_visita) {	
								
		if(!$formulario_visita['data_visitacao']) {
			array_push($formulario_visita['erros'], "Por favor, informe a Data de Visitação.");
		}
		
		if(!$formulario_visita['data_preenchimento']) {
			array_push($formulario_visita['erros'], "Por favor, informe uma Data de Preenchimento.");
		}
		
		if(!$formulario_visita['setor']) {
			array_push($formulario_visita['erros'], "Por favor, selecione um Setor.");
		}
		
		if(!$formulario_visita['cliente_novo']) {
			array_push($formulario_visita['erros'], "Por favor, informe se é um Cliente novo ou um Cliente existente.");
		}
		
		if(!$formulario_visita['cliente']) {
			array_push($formulario_visita['erros'], "Por favor, informe o Cliente.");
		}	
		
		if($formulario_visita['cliente_novo'] == 'N') {
			if(!$formulario_visita['codigo_cliente']) {
				array_push($formulario_visita['erros'], "Por favor, informe o Código do Cliente.");
			}	
		}		
		/*
		if(!$formulario_visita['classificacao_cliente']) {
			array_push($formulario_visita['erros'], "Por favor, informe a Classificação do Cliente.");
		}			
		
		if(!$formulario_visita['telefone_cliente']) {
			array_push($formulario_visita['erros'], "Por favor, informe o Telefone do Cliente.");
		}			
		
		if($formulario_visita['cliente_novo'] == 'S') {
			if(!$formulario_visita['tipo_cliente']) {
				array_push($formulario_visita['erros'], "Por favor, informe o Tipo do Cliente.");
			}				
		}
		
		if(!$formulario_visita['cidade_regiao_cliente']) {
			array_push($formulario_visita['erros'], "Por favor, informe a Cidade/Região do Cliente.");
		}	
		
		if(!$formulario_visita['estado_cliente']) {
			array_push($formulario_visita['erros'], "Por favor, informe o Estado do Cliente.");
		}	
		
		if(!$formulario_visita['cep_cliente']) {
			array_push($formulario_visita['erros'], "Por favor, informe o CEP do Cliente.");
		}		
		*/
		if(!$formulario_visita['gerou_pedido_orcamento']) {
			array_push($formulario_visita['erros'], "Por favor, informe se Gerou Pedido ou Orçamento");
		}		
		
		if($formulario_visita['setor'] == 'R') {
			if(!$formulario_visita['material_pdv']) {
				array_push($formulario_visita['erros'], "Por favor, selecione uma opção para o Material PDV");
			}	
		}
		
		return $formulario_visita;
	}
				
	function validar_campos_obrigatorios_engenharia($formulario_visita) {
		
		if(!$formulario_visita['obra']) {
			array_push($formulario_visita['erros'], "Por favor, informe a Obra.");
		}
		
		if($formulario_visita['obra'] == 'S') {
			if(!$formulario_visita['nome_obra']) {
				array_push($formulario_visita['erros'], "Por favor, informe o Nome da Obra.");
			}
			if(strlen($formulario_visita['nome_obra']) > 254) {
				array_push($formulario_visita['erros'], 'O campo do Nome da Obra não pode conter mais de 254 caracteres.');
			}	
		}
		
		if(!$formulario_visita['escritorio']) {
			array_push($formulario_visita['erros'], "Por favor, informe o Escritório.");
		}			
		
		if(!$formulario_visita['segmento']) {
			array_push($formulario_visita['erros'], "Por favor, selecione o Segmento.");
		}			
		
		if(!$formulario_visita['fase_obra']) {
			array_push($formulario_visita['erros'], "Por favor, selecione a Fase da Obra.");
		}		

		if(!$formulario_visita['aplicacao']) {
			array_push($formulario_visita['erros'], "Por favor, informe a Aplicação.");
		}			
		/*
		if(!$formulario_visita['viavel_proximo_passo']) {
			array_push($formulario_visita['erros'], "Por favor, selecione uma opção para Viável para o Próximo Passo.");
		}
		*/
		if(!$formulario_visita['proximo_passo']) {
			array_push($formulario_visita['erros'], "Por favor, informe o Próximo Passo.");
		}	
		
		if(!$formulario_visita['data_proximo_passo']) {
			array_push($formulario_visita['erros'], "Por favor, informe a Data do Próximo Passo.");
		}
		
		if(!$formulario_visita['horario_proximo_passo']) {
			array_push($formulario_visita['erros'], "Por favor, informe a Hora do Próximo Passo.");
		}
		
		if(strlen($formulario_visita['resumo_reuniao']) == 0) {
			array_push($formulario_visita['erros'], "Por favor, preencha o Resumo da Reunião.");
		}	
		
		if(strlen($formulario_visita['resumo_reuniao']) > 254) {
			array_push($formulario_visita['erros'], 'O campo de Resumo da Reunião não pode conter mais de 254 caracteres.');
		}	
				
		return $formulario_visita;
	}
		
	function obter_contatos_cliente($codigo_loja_cliente = NULL) {

		$dados = array(
            'cliente_encontrado' => FALSE
		);
	
        $codigo_loja_cliente = explode('_', $codigo_loja_cliente);
        $codigo_cliente = $codigo_loja_cliente[0];
        $loja_cliente = $codigo_loja_cliente[1];
		
		if ($codigo_cliente && $loja_cliente) {
            $contatos_cliente = $this->db_formulario_visita->obter_contatos_cliente($codigo_cliente, $loja_cliente);
        }
		
		if($contatos_cliente) {
			$dados['cliente_encontrado'] = TRUE;
			$dados['contatos_cliente'] = $contatos_cliente;
		}
		
		echo json_encode($dados);
	}	
	
	function obter_clientes_formulario_visita($coordenacao = false) {
		
		$palavra_chave = addslashes($_GET['term']);
		
		
		if(!in_array(trim($this->session->userdata('grupo_usuario')), array('gestores_comerciais'))){
			$codigo_representante = $this->session->userdata('codigo_usuario');
			$dados_representante = $this->db_cliente->obter_representante($codigo_representante);
		} else {
			$codigo_representante = null;
			$dados_representante = null;
		}

		$clientes = $this->db_formulario_visita->obter_clientes_formulario_visita($palavra_chave, $codigo_representante, $this->session->userdata('grupo_usuario'), $coordenacao,$dados_representante );
		
		foreach ($clientes as $cliente) {
            $dados[] = array('cor' => $cliente['cor_cliente'], 'label' => trim($cliente['codigo']) . ' - ' . trim(exibir_texto($cliente['nome'])) . ' - ' . trim($cliente['cpf'] . ' - ' . trim($cliente['estado'])), 'value' => trim($cliente['codigo']) . '|' . $cliente['loja']);
        }
				
		echo json_encode($dados);
	}	
	
	function obter_produtos_formulario_visita() {
		
		$palavra_chave = addslashes($_GET['term']);
		
		$produtos = $this->db_formulario_visita->obter_produtos_formulario_visita($palavra_chave);
		
		foreach ($produtos as $produto) {
            $dados[] = array('label' => trim($produto['codigo']) . ' - ' . trim(exibir_texto($produto['descricao'])), 'value' => trim($produto['codigo']));
        }
				
		echo json_encode($dados);
	}
	
	function obter_concorrentes() {
		
		$palavra_chave = addslashes($_GET['term']);
		
		$concorrentes = $this->db_formulario_visita->obter_concorrentes($palavra_chave);
		
		foreach ($concorrentes as $concorrente) {
            $dados[] = array('label' => trim($concorrente['codigo']) . ' - ' . trim(exibir_texto($concorrente['nome'])), 'value' => trim($concorrente['codigo']));
        }
			
		echo json_encode($dados);
	}
}