<?php

class Pedidos extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
		//ini_set("memory_limit","20M");	
	}
	
	function index()
	{	
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->config->load('db_cliente_' . $this->db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');
		
		$filtros = array(
			
			array(
				'nome' => 'status',
				'descricao' => 'Status',
				'tipo' => 'opcoes',
				'opcoes' => array('todos' => 'Todos', 'aguardando_faturamento' => 'Aguardando Faturamento', 'parcialmente_faturado' => 'Parcialmente Faturado', 'faturado' => 'Faturado')
			),
			array(
				'nome' => 'filial',
				'descricao' => 'Filial',
				'tipo' => 'opcoes',
				'campo_mssql' => 'C5_FILIAL',
				'opcoes' => $this->_obter_filiais(),
				'ordenar_ms' => 1
			),
			in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) ? array(
				'nome' => 'representante',
				'descricao' => 'Representante',
				'tipo' => 'texto',
				'campo_mssql' => 'A3_NOME',
				'ordenar_ms' => 2
			) : NULL,
			in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais')) ? array(
				'nome' => 'cliente',
				'descricao' => 'Cliente',
				'tipo' => 'texto',
				'campo_mssql' => 'A1_NOME',
				'ordenar_ms' => 3
			) : NULL,
			array(
				'nome' => 'codigo_do_pedido',
				'descricao' => 'Código do pedido',
				'tipo' => 'texto',
				'campo_mssql' => 'C6_NUM',
				'ordenar_ms' => 4
			),
			array(
				'nome' => 'emissao',
				'descricao' => 'Emissão',
				'tipo' => 'data',
				'campo_mssql' => 'C5_EMISSAO',
				'ordenar_ms' => 5
			),
			array(
				'nome' => 'hora_pedido',
				'descricao' => 'Hora Pedido',
				'tipo' => 'hora',
				'campo_mssql' => 'ZZ2_PED_HR',
			),
			array(
				'nome' => 'data_liberacao',
				'descricao' => 'Data Liberação',
				'tipo' => 'data',
				'campo_mssql' => 'ZZ2_PIK_DT',
			),
			array(
				'nome' => 'hora_liberacao',
				'descricao' => 'Hora Liberação',
				'tipo' => 'hora',
				'campo_mssql' => 'ZZ2_PIK_HR',
			),
			array(
				'nome' => 'data_separacao',
				'descricao' => 'Data Separação',
				'tipo' => 'data',
				'campo_mssql' => 'ZZ2_EX_DT',
			),
			array(
				'nome' => 'hora_separacao',
				'descricao' => 'Hora Separação',
				'tipo' => 'hora',
				'campo_mssql' => 'ZZ2_EX_HR',
			),
			array(
				'nome' => 'data_conferencia',
				'descricao' => 'Data Conferência',
				'tipo' => 'data',
				'campo_mssql' => 'ZZ2_DT_CON',
			),
			array(
				'nome' => 'hora_conferencia',
				'descricao' => 'Hora Conferência',
				'tipo' => 'hora',
				'campo_mssql' => 'ZZ2_HR_CON',
			),
			array(
				'nome' => 'data_faturamento',
				'descricao' => 'Data Faturamento',
				'tipo' => 'data',
				'campo_mssql' => 'ZZ2_DOC_DT',
			),
			array(
				'nome' => 'hora_faturamento',
				'descricao' => 'Hora Faturamento',
				'tipo' => 'hora',
				'campo_mssql' => 'ZZ2_DOC_HR',
			),
			array(
				'nome' => 'data_coleta',
				'descricao' => 'Data Coleta',
				'tipo' => 'data',
				'campo_mssql' => 'ZZ2_COL_DT',
			),
			array(
				'nome' => 'hora_coleta',
				'descricao' => 'Hora Coleta',
				'tipo' => 'hora',
				'campo_mssql' => 'ZZ2_COL_HR',
			),
		);
		
		// para que a paginação no mssql funcione, devemos ordenar de alguma forma, caso o usuário não escolher uma ordenação, vamos fazer manualmente
		if (!$this->input->get('ordenar_ms'))
		{
			// vamos setar o $_GET ao invés de usar o order_by()
			// usando o order_by a flexinha no layout indicando o que está sendo ordenado não apareceria para o usuário
			$_GET['ordenar_ms'] = 'C5_EMISSAO';
			$_GET['ordenar_ms_tipo'] = 'desc';
		}
		
		// ** FILTRAGEM PARA EXIBIÇÃO DOS RESULTADOS
		
		// o campo status precisa ser filtrado "manualmente" pois ele não existe no banco de dados
		if ($this->input->get('status'))
		{
			switch ($this->input->get('status'))
			{
				case 'aguardando_faturamento':
					$this->db_cliente->where('C6_QTDENT', 0);
				break;
				
				case 'parcialmente_faturado':
					$this->db_cliente->where('C6_QTDENT > 0 AND C6_QTDENT < C6_QTDVEN');
				break;
				
				case 'faturado':
					$this->db_cliente->where('C6_QTDENT > 0 AND C6_QTDENT = C6_QTDVEN');
				break;
			}
		}
		
		// caso o usuário logado for um representante, vamos mostrar somente os pedidos que ele mesmo fez
		if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores')))
		{
			$this->db_cliente->where('C5_VEND1', $this->session->userdata('codigo_usuario'));
		}
		else if($this->session->userdata('grupo_usuario') == 'supervisores'){
			$representantes = $this->db->select('codigo_representante')->from('representantes_supervisores')->where('id_supervisor', $this->session->userdata('id_usuario'))->get()->result();
			foreach($representantes as $rep){
				$reps[] = $rep->codigo_representante;
			}
			$reps[] = $this->session->userdata('codigo_usuario');
			$this->db_cliente->where_in('C5_VEND1', $reps);
		}
		
		if($_GET['my_submit']){
			if (($this->session->userdata('grupo_usuario') != 'gestores_comerciais') and ((!$_GET['codigo_do_pedido']) and (!$_GET['emissao_inicial']))){
				$erro = "Realize um filtro para efetuar a pesquisa.";
			}else{
				$this->filtragem_mssql($filtros);
				
				$pedidos = $this->db_cliente->distinct()->select(
						'C5_VEND1, C5_FILIAL, C6_NUM, C5_EMISSAO, C6_CLI, C6_LOJA, A1_NOME, A3_NOME,
						(CASE WHEN SUM(C6_QTDENT) <= 0 THEN \'Aguardando Faturamento\' WHEN SUM(C6_QTDENT) < SUM(C6_QTDVEN) THEN \'Parcialmente Faturado\' ELSE \'Faturado\' END) AS status, 
						SUM(C6_QTDVEN) "C6_QTDVEN", SUM(C6_QTDENT) "C6_QTDENT", SUM(C6_PRCVEN * C6_QTDVEN) "total", SUM(C6_PRCVEN * C6_QTDENT) AS total_parcial'
					)
								->from($this->_db_cliente['tabelas']['pedidos'] . ', ' . $this->_db_cliente['tabelas']['itens_pedidos'] . ', ' . $this->_db_cliente['tabelas']['clientes'] . ', ' . $this->_db_cliente['tabelas']['representantes'] . ', ' . $this->_db_cliente['tabelas']['posicionamento_pedido'])
								->where(array(
									$this->_db_cliente['tabelas']['itens_pedidos'] . '.C6_NUM = ' . $this->_db_cliente['tabelas']['pedidos'] . '.C5_NUM' => NULL,
									$this->_db_cliente['tabelas']['itens_pedidos'] . '.C6_FILIAL = ' . $this->_db_cliente['tabelas']['pedidos'] . '.C5_FILIAL' => NULL,
									$this->_db_cliente['tabelas']['clientes'] . '.A1_COD = ' . $this->_db_cliente['tabelas']['itens_pedidos'] . '.C6_CLI' => NULL,
									$this->_db_cliente['tabelas']['clientes'] . '.A1_LOJA = ' . $this->_db_cliente['tabelas']['itens_pedidos'] . '.C6_LOJA' => NULL,
									$this->_db_cliente['tabelas']['representantes'] . '.A3_COD = ' . $this->_db_cliente['tabelas']['pedidos'] . '.C5_VEND1' => NULL,
									$this->_db_cliente['tabelas']['itens_pedidos'] . '.D_E_L_E_T_ !=' => '*',
									$this->_db_cliente['tabelas']['clientes'] . '.D_E_L_E_T_ !=' => '*',
									$this->_db_cliente['tabelas']['pedidos'] . '.D_E_L_E_T_ !=' => '*',
									$this->_db_cliente['tabelas']['representantes'] . '.D_E_L_E_T_ !=' => '*',
									$this->_db_cliente['tabelas']['pedidos'] . '.C5_EMISSAO >=' => date('Ymd',mktime(0, 0, 0, date('m'), date('d'), date('Y')-1)),
									
									$this->_db_cliente['tabelas']['posicionamento_pedido'] . '.ZZ2_PEDIDO = ' . $this->_db_cliente['tabelas']['pedidos'] . '.C5_NUM' => NULL,
									$this->_db_cliente['tabelas']['posicionamento_pedido'] . '.ZZ2_FILIAL = ' . $this->_db_cliente['tabelas']['pedidos'] . '.C5_FILIAL' => NULL,
									//$this->_db_cliente['tabelas']['pedidos'] . '.C5_FILIAL' => '03'
								))
								->limit(20, $this->input->get('per_page'))
								->group_by('C5_VEND1, C5_FILIAL, C6_NUM, C5_EMISSAO, C6_CLI, C6_LOJA, A1_NOME, A3_NOME')
								->get()
								->result_array();
								
				
				// FILTRAGEM PARA EXIBIÇÃO DOS RESULTADOS **
				
				// ** FILTRAGEM PARA PAGINAÇÃO
				
				// o campo status precisa ser filtrado "manualmente" pois ele não existe no banco de dados
				if ($this->input->get('status')){
					switch ($this->input->get('status')){
						case 'aguardando_faturamento':
							$this->db_cliente->where('C6_QTDENT', 0);
						break;
						
						case 'parcialmente_faturado':
							$this->db_cliente->where('C6_QTDENT > 0 AND C6_QTDENT < C6_QTDVEN');
						break;
						
						case 'faturado':
							$this->db_cliente->where('C6_QTDENT > 0 AND C6_QTDENT = C6_QTDVEN');
						break;
					}
				}
			
			
				// caso o usuário logado for um representante, vamos mostrar somente os pedidos que ele mesmo fez
				if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores'))){
					$this->db_cliente->where('C5_VEND1', $this->session->userdata('codigo_usuario'));
				}
				else if($this->session->userdata('grupo_usuario') == 'supervisores'){
					$representantes = $this->db->select('codigo_representante')->from('representantes_supervisores')->where('id_supervisor', $this->session->userdata('id_usuario'))->get()->result();
					foreach($representantes as $rep){
						$reps[] = $rep->codigo_representante;
					}
					$reps[] = $this->session->userdata('codigo_usuario');
					$this->db_cliente->where_in('C5_VEND1', $reps);
				}
				if($_GET['my_submit']){
					$this->filtragem_mssql($filtros, FALSE);
					
					/*
					$total = $this->db_cliente->select('COUNT(DISTINCT C6_NUM) AS total')
									->from($this->_db_cliente['tabelas']['itens_pedidos'] . ', ' . $this->_db_cliente['tabelas']['pedidos'] . ', ' . $this->_db_cliente['tabelas']['clientes'] . ', ' . $this->_db_cliente['tabelas']['representantes'])
									->where(array(
										$this->_db_cliente['tabelas']['itens_pedidos'] . '.C6_NUM = ' . $this->_db_cliente['tabelas']['pedidos'] . '.C5_NUM' => NULL,
										$this->_db_cliente['tabelas']['itens_pedidos'] . '.C6_FILIAL = ' . $this->_db_cliente['tabelas']['pedidos'] . '.C5_FILIAL' => NULL,
										$this->_db_cliente['tabelas']['clientes'] . '.A1_COD = ' . $this->_db_cliente['tabelas']['itens_pedidos'] . '.C6_CLI' => NULL,
										$this->_db_cliente['tabelas']['clientes'] . '.A1_LOJA = ' . $this->_db_cliente['tabelas']['itens_pedidos'] . '.C6_LOJA' => NULL,
										$this->_db_cliente['tabelas']['representantes'] . '.A3_COD = ' . $this->_db_cliente['tabelas']['pedidos'] . '.C5_VEND1' => NULL,
										$this->_db_cliente['tabelas']['itens_pedidos'] . '.D_E_L_E_T_ !=' => '*',
										$this->_db_cliente['tabelas']['clientes'] . '.D_E_L_E_T_ !=' => '*',
										$this->_db_cliente['tabelas']['pedidos'] . '.D_E_L_E_T_ !=' => '*',
										//$this->_db_cliente['tabelas']['pedidos'] . '.C5_FILIAL' => '03'
									))
									->get()->result_array();
					
					$total = $total[0]['total'];
					*/
					
					$pedidos_total = $this->db_cliente->distinct()->select('C5_VEND1, C5_FILIAL, C6_NUM, C5_EMISSAO, C6_CLI, C6_LOJA, A1_NOME, A3_NOME, (CASE WHEN SUM(C6_QTDENT) <= 0 THEN \'Aguardando Faturamento\' WHEN SUM(C6_QTDENT) < SUM(C6_QTDVEN) THEN \'Parcialmente Faturado\' ELSE \'Faturado\' END) AS status, SUM(C6_QTDVEN) AS C6_QTDVEN, SUM(C6_QTDENT) AS C6_QTDENT, SUM(C6_PRCVEN * C6_QTDVEN) AS total, SUM(C6_PRCVEN * C6_QTDENT) AS total_parcial')
									->from($this->_db_cliente['tabelas']['pedidos'] . ', ' . $this->_db_cliente['tabelas']['itens_pedidos'] . ', ' . $this->_db_cliente['tabelas']['clientes'] . ', ' . $this->_db_cliente['tabelas']['representantes'] . ', ' . $this->_db_cliente['tabelas']['posicionamento_pedido'])
									->where(array(
										$this->_db_cliente['tabelas']['itens_pedidos'] . '.C6_NUM = ' . $this->_db_cliente['tabelas']['pedidos'] . '.C5_NUM' => NULL,
										$this->_db_cliente['tabelas']['itens_pedidos'] . '.C6_FILIAL = ' . $this->_db_cliente['tabelas']['pedidos'] . '.C5_FILIAL' => NULL,
										$this->_db_cliente['tabelas']['clientes'] . '.A1_COD = ' . $this->_db_cliente['tabelas']['itens_pedidos'] . '.C6_CLI' => NULL,
										$this->_db_cliente['tabelas']['clientes'] . '.A1_LOJA = ' . $this->_db_cliente['tabelas']['itens_pedidos'] . '.C6_LOJA' => NULL,
										$this->_db_cliente['tabelas']['representantes'] . '.A3_COD = ' . $this->_db_cliente['tabelas']['pedidos'] . '.C5_VEND1' => NULL,
										$this->_db_cliente['tabelas']['itens_pedidos'] . '.D_E_L_E_T_ !=' => '*',
										$this->_db_cliente['tabelas']['clientes'] . '.D_E_L_E_T_ !=' => '*',
										$this->_db_cliente['tabelas']['pedidos'] . '.D_E_L_E_T_ !=' => '*',
										$this->_db_cliente['tabelas']['pedidos'] . '.C5_EMISSAO >=' => date('Ymd',mktime(0, 0, 0, date('m'), date('d'), date('Y')-1)),
										
										$this->_db_cliente['tabelas']['posicionamento_pedido'] . '.ZZ2_PEDIDO = ' . $this->_db_cliente['tabelas']['pedidos'] . '.C5_NUM' => NULL,
										$this->_db_cliente['tabelas']['posicionamento_pedido'] . '.ZZ2_FILIAL = ' . $this->_db_cliente['tabelas']['pedidos'] . '.C5_FILIAL' => NULL,
										//$this->_db_cliente['tabelas']['pedidos'] . '.C5_FILIAL' => '03'
									))
									//->limit(20, $this->input->get('per_page'))
									->group_by('C5_VEND1, C5_FILIAL, C6_NUM, C5_EMISSAO, C6_CLI, C6_LOJA, A1_NOME, A3_NOME')
									->get()->result_array();
					
					$total = count($pedidos_total);
				}	
				//
				
				
				// o campo status precisa ser filtrado "manualmente" pois ele não existe no banco de dados
				if ($this->input->get('status')){
					switch ($this->input->get('status'))
					{
						case 'aguardando_faturamento':
							$this->db_cliente->where('C6_QTDENT', 0);
						break;
						
						case 'parcialmente_faturado':
							$this->db_cliente->where('C6_QTDENT > 0 AND C6_QTDENT < C6_QTDVEN');
						break;
						
						case 'faturado':
							$this->db_cliente->where('C6_QTDENT > 0 AND C6_QTDENT = C6_QTDVEN');
						break;
					}
				}
				
				// caso o usuário logado for um representante, vamos mostrar somente os pedidos que ele mesmo fez
				if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros'))){
					$this->db_cliente->where('C5_VEND1', $this->session->userdata('codigo_usuario'));
				}
				if($_GET['my_submit']){
					$this->filtragem_mssql($filtros, FALSE);
					
					$total_2 = $this->db_cliente->select('SUM(C6_PRCVEN * C6_QTDVEN) AS valor')
									->from($this->_db_cliente['tabelas']['itens_pedidos'] . ', ' . $this->_db_cliente['tabelas']['pedidos'] . ', ' . $this->_db_cliente['tabelas']['clientes'] . ', ' . $this->_db_cliente['tabelas']['representantes'] . ', ' . $this->_db_cliente['tabelas']['posicionamento_pedido'])
									->where(array(
										$this->_db_cliente['tabelas']['itens_pedidos'] . '.C6_NUM = ' . $this->_db_cliente['tabelas']['pedidos'] . '.C5_NUM' => NULL,
										$this->_db_cliente['tabelas']['itens_pedidos'] . '.C6_FILIAL = ' . $this->_db_cliente['tabelas']['pedidos'] . '.C5_FILIAL' => NULL,
										$this->_db_cliente['tabelas']['clientes'] . '.A1_COD = ' . $this->_db_cliente['tabelas']['itens_pedidos'] . '.C6_CLI' => NULL,
										$this->_db_cliente['tabelas']['clientes'] . '.A1_LOJA = ' . $this->_db_cliente['tabelas']['itens_pedidos'] . '.C6_LOJA' => NULL,
										$this->_db_cliente['tabelas']['representantes'] . '.A3_COD = ' . $this->_db_cliente['tabelas']['pedidos'] . '.C5_VEND1' => NULL,
										$this->_db_cliente['tabelas']['itens_pedidos'] . '.D_E_L_E_T_ !=' => '*',
										$this->_db_cliente['tabelas']['clientes'] . '.D_E_L_E_T_ !=' => '*',
										$this->_db_cliente['tabelas']['pedidos'] . '.D_E_L_E_T_ !=' => '*',
										
										$this->_db_cliente['tabelas']['posicionamento_pedido'] . '.ZZ2_PEDIDO = ' . $this->_db_cliente['tabelas']['pedidos'] . '.C5_NUM' => NULL,
										$this->_db_cliente['tabelas']['posicionamento_pedido'] . '.ZZ2_FILIAL = ' . $this->_db_cliente['tabelas']['pedidos'] . '.C5_FILIAL' => NULL,
										//$this->_db_cliente['tabelas']['pedidos'] . '.C5_FILIAL' => '03'
									))
									->get()->row_array();
				}
				// FILTRAGEM PARA PAGINAÇÃO **
			}
		}
		$this->load->view('layout', array('conteudo' => $this->load->view('pedidos/index', array('pedidos' => $pedidos, 'erro' => $erro, 'total' => $total, 'total_2' => $total_2, 'filtragem' => $this->filtragem($filtros), 'paginacao' => $this->paginacao($total)), TRUE)));
	}
	
	function aguardando_analise($orcamento = FALSE)
	{
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->config->load('db_cliente_' . $this->db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');

		
		$filtros = array(
			!$orcamento ? array(
				'nome' => 'status',
				'descricao' => 'Status',
				'tipo' => 'opcoes',
				//CUSTOM E0102/2014
				//'opcoes' => array('todos' => 'Todos', 'A' => 'Aguardando Análise Comercial', 'R' => 'Reprovado' ),
				'opcoes' => array('todos' => 'Todos', 'A' => 'Aguardando Análise Comercial', 'R' => 'Reprovado', 'L'=> 'Aguardando Importação' ,'I' => 'Importado' ),
				//FIM CUSTOM  E0102/2014
				'campo_mssql' => $this->_db_cliente['campos']['pedidos_dw']['status'],
				'ordenar_ms' => 0
			) : array(
				'nome' => 'status',
				'descricao' => 'Status',
				'tipo' => 'opcoes_orcamento',
				'opcoes' => array('todos' => 'Todos', 'A' => 'Aguardando ', 'C' => 'Transformado em Pedido'),
				'campo_mssql' => $this->_db_cliente['campos']['pedidos_dw']['status'],
				'ordenar_ms' => 0
			), 
			
			in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) ? array(
				'nome' => 'representante',
				'descricao' => 'Representante',
				'tipo' => 'texto',
				'campo_mssql' => $this->_db_cliente['campos']['representantes']['nome'],
				'ordenar_ms' => 1
			) : NULL,
			array(
				'nome' => 'cliente',
				'descricao' => 'Cliente',
				'tipo' => 'texto',
				'campo_mssql' => $this->_db_cliente['campos']['pedidos_dw']['nome_cliente'],
				'ordenar_ms' => 2
			),
			$orcamento ? array(
				'nome' => 'prospect',
				'descricao' => 'Prospect',
				'tipo' => 'texto',
				'campo_mssql' =>  $this->_db_cliente['campos']['pedidos_dw']['nome_prospects'],
				'ordenar_ms' => 2
			) : NULL,
			array(
				'nome' => 'ordem_de_compra',
				'descricao' => 'Ordem de compra',
				'tipo' => 'texto',
				'campo_mssql' => $this->_db_cliente['campos']['pedidos_dw']['ordem_compra'],
				'ordenar_ms' => 3
			),
			array(
				'nome' => 'emissao',
				'descricao' => 'Emissão',
				'tipo' => 'data',
				'campo_mssql' => $this->_db_cliente['campos']['pedidos_dw']['data_emissao'],
				'ordenar_ms' => 4
			)
		);
		
		// para que a paginação no mssql funcione, devemos ordenar de alguma forma, caso o usuário não escolher uma ordenação, vamos fazer manualmente
		if (!$this->input->get('ordenar_ms'))
		{
			// vamos setar o $_GET ao invés de usar o order_by()
			// usando o order_by a flexinha no layout indicando o que está sendo ordenado não apareceria para o usuário
			$_GET['ordenar_ms'] = $this->_db_cliente['campos']['pedidos_dw']['data_emissao'];
			$_GET['ordenar_ms_tipo'] = 'desc';
		}
		
		// ** FILTRAGEM PARA EXIBIÇÃO DOS RESULTADOS
		
		// caso o usuário logado for um representante, vamos mostrar somente os pedidos que ele mesmo fez
		if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores'))){
			$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['codigo_representante'], $this->session->userdata('codigo_usuario'));
		}//Caso for um supervisor logado, iremos listar os códigos dele e de seus representantes supervisionados
		else if($this->session->userdata('grupo_usuario') == 'supervisores'){
			$representantes = $this->db->select('codigo_representante')->from('representantes_supervisores')->where('id_supervisor', $this->session->userdata('id_usuario'))->get()->result();
			foreach($representantes as $rep){
				$reps[] = $rep->codigo_representante;
			}
			$reps[] = $this->session->userdata('codigo_usuario');
			$this->db_cliente->where_in($this->_db_cliente['campos']['pedidos_dw']['codigo_representante'], $reps);
		}
		if($_GET['my_submit']){
			$this->filtragem_mssql($filtros);
			
			if ($orcamento) 
			{ 
				$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['tipo'], 'O'); 
			}
			else 
			{ 
				$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['tipo'], 'P'); 
			} 
			
			if (!$orcamento) 
			{ 
			/*
				//CUSTOM E0102/2014
				$this->db_cliente->where($this->_db_cliente['tabelas']['pedidos_dw'] . '.' . $this->_db_cliente['campos']['pedidos_dw']['status'] . ' !=', 'L');
				$this->db_cliente->where($this->_db_cliente['tabelas']['pedidos_dw'] . '.' . $this->_db_cliente['campos']['pedidos_dw']['status'] . ' !=', 'I');
				//FIM CUSTOM E0102/2014
			*/
			}else{
				$this->db_cliente->where($this->_db_cliente['tabelas']['pedidos_dw'] . '.' . $this->_db_cliente['campos']['pedidos_dw']['status'] . ' !=', 'I');
			}
			
			$select[] = $this->_db_cliente['campos']['pedidos_dw']['id_pedido'];
			$select[] = $this->_db_cliente['campos']['pedidos_dw']['ordem_compra'];
			$select[] = $this->_db_cliente['campos']['pedidos_dw']['data_emissao'];
			$select[] = $this->_db_cliente['campos']['pedidos_dw']['codigo_cliente'];
			$select[] = $this->_db_cliente['campos']['pedidos_dw']['loja_cliente'];
			$select[] = $this->_db_cliente['campos']['pedidos_dw']['nome_cliente'];
			$select[] = $this->_db_cliente['campos']['pedidos_dw']['nome_prospects'];
			$select[] = $this->_db_cliente['campos']['representantes']['codigo'];			
			$select[] = $this->_db_cliente['campos']['representantes']['nome'];
			$select[] = $this->_db_cliente['campos']['pedidos_dw']['status'];
			$select[] = 'SUM(' . $this->_db_cliente['campos']['pedidos_dw']['quantidade'] . ') AS quantidade_vendida';
			//$select[] = 'SUM(' . $this->_db_cliente['campos']['pedidos_dw']['preco_unitario'] . ' * ' . $this->_db_cliente['campos']['pedidos_dw']['quantidade'] . ') + ' . $this->_db_cliente['campos']['pedidos_dw']['valor_frete'] . ' AS total';
			
			// total com calculo antigo, não é valido pois não tras calculos de ST pela regra e não há possibilidade de ser tratado com o SGDB.
			//$select[] = 'SUM((((' . $this->_db_cliente['campos']['pedidos_dw']['preco_unitario'] . ' - (' . $this->_db_cliente['campos']['pedidos_dw']['preco_unitario'] . ' * (' . $this->_db_cliente['campos']['pedidos_dw']['desconto'] . '/100))) * ((' . $this->_db_cliente['campos']['pedidos_dw']['ipi'] . '/100)+1))) * ' . $this->_db_cliente['campos']['pedidos_dw']['quantidade'] .') AS total';
			
			$select[] = 'SUM(' . $this->_db_cliente['campos']['pedidos_dw']['preco_unitario'] . ' * ' . $this->_db_cliente['campos']['pedidos_dw']['quantidade'] . ') AS peso_total';
			
			$group[] = $this->_db_cliente['campos']['pedidos_dw']['id_pedido'];
			$group[] = $this->_db_cliente['campos']['pedidos_dw']['ordem_compra'];
			$group[] = $this->_db_cliente['campos']['pedidos_dw']['data_emissao'];
			$group[] = $this->_db_cliente['campos']['pedidos_dw']['codigo_cliente'];
			$group[] = $this->_db_cliente['campos']['pedidos_dw']['loja_cliente'];
			$group[] = $this->_db_cliente['campos']['pedidos_dw']['nome_cliente'];			
			$group[] = $this->_db_cliente['campos']['pedidos_dw']['nome_prospects'];
			$group[] = $this->_db_cliente['campos']['representantes']['codigo'];
			$group[] = $this->_db_cliente['campos']['representantes']['nome'];
			$group[] = $this->_db_cliente['campos']['pedidos_dw']['status'];
			$group[] = $this->_db_cliente['campos']['pedidos_dw']['valor_frete'];
			
			$pedidos = $this->db_cliente->select(implode(', ', $select))
							->from($this->_db_cliente['tabelas']['pedidos_dw'])
							->join($this->_db_cliente['tabelas']['representantes'], $this->_db_cliente['campos']['representantes']['codigo'] . ' = ' . $this->_db_cliente['campos']['pedidos_dw']['codigo_representante'])
							->where(array($this->_db_cliente['tabelas']['representantes'] . '.' . $this->_db_cliente['campos']['representantes']['delecao']  . ' !=' => '*'))
							->limit(20, $this->input->get('per_page'))
							->group_by(implode(', ', $group))
							->get()->result_array();
							
			//debug_pre($this->db_cliente->last_query());
			// ROTINA DE CALCULO DO TOTAL DOS ITENS PAGINADOS.
			
			// obtem a lista somente com os códigos dos pedidos paginados
			$_totais_pedidos = array();
			$_lista_idpedidos = array();
			foreach($pedidos as $key_pedido => $_pedido) {
				$_totais_pedidos[$_pedido[$this->_db_cliente['campos']['pedidos_dw']['id_pedido']]] = 0;
				$_lista_idpedidos[] = $_pedido[$this->_db_cliente['campos']['pedidos_dw']['id_pedido']];
			}	
			
			
			
			if ($orcamento) 
			{ 
				$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['tipo'], 'O'); 
			}
			else 
			{ 
				$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['tipo'], 'P'); 
			} 
			
			
			// obtem a lista de produtos dos pedidos paginados
			$selectItens[] = $this->_db_cliente['campos']['pedidos_dw']['id_pedido'] . ' as id_pedido';
			$selectItens[] = $this->_db_cliente['campos']['pedidos_dw']['preco_unitario'] . ' as preco_unitario';			
			$selectItens[] = $this->_db_cliente['campos']['pedidos_dw']['quantidade'] . ' as quantidade';
			$selectItens[] = $this->_db_cliente['campos']['pedidos_dw']['codigo_produto'] . ' as codigo_produto';
			$selectItens[] = $this->_db_cliente['campos']['pedidos_dw']['codigo_cliente'] . ' as codigo_cliente';
			$selectItens[] = $this->_db_cliente['campos']['pedidos_dw']['loja_cliente'] . ' as loja_cliente';
			$selectItens[] = $this->_db_cliente['campos']['pedidos_dw']['ipi'] . ' as ipi';
			$selectItens[] = $this->_db_cliente['campos']['pedidos_dw']['desconto'] . ' as desconto';
			$selectItens[] = $this->_db_cliente['campos']['pedidos_dw']['substituicao_tributaria'] . ' as substituicao_tributaria';
			
			$this->db_cliente->select(implode(', ', $selectItens))
							->from($this->_db_cliente['tabelas']['pedidos_dw'])
							->join($this->_db_cliente['tabelas']['representantes'], $this->_db_cliente['campos']['representantes']['codigo'] . ' = ' . $this->_db_cliente['campos']['pedidos_dw']['codigo_representante'])
							->where(array($this->_db_cliente['tabelas']['representantes'] . '.' . $this->_db_cliente['campos']['representantes']['delecao']  . ' !=' => '*'))
							->limit(20, $this->input->get('per_page'));
			
			if (count($_lista_idpedidos) > 0) {			
				$this->db_cliente->where_in($this->_db_cliente['campos']['pedidos_dw']['id_pedido'], $_lista_idpedidos);
			}
			
			/* Correção erro ao realizar a paginação de Orçamentos */
			// para que a paginação no mssql funcione, devemos ordenar de alguma forma, caso o usuário não escolher uma ordenação, vamos fazer manualmente
			if (!$this->input->get('ordenar_ms'))
			{
				// vamos setar o $_GET ao invés de usar o order_by()
				// usando o order_by a flexinha no layout indicando o que está sendo ordenado não apareceria para o usuário
				$_GET['ordenar_ms'] = $this->_db_cliente['campos']['pedidos_dw']['data_emissao'];
				$_GET['ordenar_ms_tipo'] = 'desc';
			}else{
				$this->db_cliente->order_by($_GET['ordenar_ms'], $_GET['ordenar_ms_tipo']);
				
			}
			/*Fim correção erro ao realizar a paginação de Orçamentos*/
			
			$itensPedidosLista = $this->db_cliente->get()->result_array();
			
			foreach($itensPedidosLista as $produto) {						
				// ** calcular st			
				$total_dos_produtos_sem_ipi = $produto['preco_unitario'] * $produto['quantidade'];
				$total_dos_produtos_com_ipi = $total_dos_produtos_sem_ipi + ($total_dos_produtos_sem_ipi * ($produto['ipi'] / 100));		
				
				$this->load->model('db_cliente', 'calc_db_cliente');
				
				//WebService
				//$cliente = $this->calc_db_cliente->obter_cliente($produto['codigo_cliente'], $produto['loja_cliente']);
				//$array = $this->calc_db_cliente->obter_impostos_item($produto['codigo_cliente'], $produto['loja_cliente'], $cliente['tipo'], $produto['quantidade'], $produto['preco_unitario'], $produto['codigo_produto'], $produto['filial']);
				//				$array = $this->calc_db_cliente->calcular_st('010', $produto['codigo_produto'], $produto['codigo_cliente'], $produto['loja_cliente'], $total_dos_produtos_sem_ipi, $total_dos_produtos_com_ipi);
				//$valor_st = $array['st'];
				// calcular st **
				$valor_st = $produto['substituicao_tributaria'];
				
				
				$unitario_com_desconto = aplicar_desconto($produto['preco_unitario'],$produto['desconto']);
				//$total_valor_produto = $produto['preco_unitario'] * $produto['quantidade']; 
				$total_valor_produto = $unitario_com_desconto * $produto['quantidade']; 
				$total_final_produto = ((($produto['ipi']?$produto['ipi']:1) * $total_valor_produto) / 100) + $total_valor_produto + ($valor_st?$valor_st:0);
				$_totais_pedidos[$produto['id_pedido']] += $total_final_produto;
				
			}	
						
			foreach($pedidos as $key_pedido => $_pedido) {
				$pedidos[$key_pedido]['total'] = $_totais_pedidos[$_pedido[$this->_db_cliente['campos']['pedidos_dw']['id_pedido']]];
			}	
			//debug_pre($pedidos);
			
			// FIM ROTINA DE CALCULO DO TOTAL DOS ITENS PAGINADOS.
			
		}
		// FILTRAGEM PARA EXIBIÇÃO DOS RESULTADOS **
		
		// ** FILTRAGEM PARA PAGINAÇÃO
		
		// o campo status precisa ser filtrado "manualmente" pois ele não existe no banco de dados
		
		// caso o usuário logado for um representante, vamos mostrar somente os pedidos que ele mesmo fez
		if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores')))
		{
			$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['codigo_representante'], $this->session->userdata('codigo_usuario'));
		}else if($this->session->userdata('grupo_usuario') == 'supervisores'){
			$representantes = $this->db->select('codigo_representante')->from('representantes_supervisores')->where('id_supervisor', $this->session->userdata('id_usuario'))->get()->result();
			foreach($representantes as $rep){
				$reps[] = $rep->codigo_representante;
			}
			$reps[] = $this->session->userdata('codigo_usuario');
			$this->db_cliente->where_in($this->_db_cliente['campos']['pedidos_dw']['codigo_representante'], $reps);
		}

		
		if($_GET['my_submit']){
			// FALSE = não usar order_by
			$this->filtragem_mssql($filtros, FALSE);
			
			if ($orcamento) 
			{ 
				$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['tipo'], 'O'); 
			}
			else 
			{ 
				$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['tipo'], 'P'); 
			} 
			
			if (!$orcamento) 
			{ 
				//CUSTOM E0102/2014
				//$this->db_cliente->where($this->_db_cliente['tabelas']['pedidos_dw'] . '.' . $this->_db_cliente['campos']['pedidos_dw']['status'] . ' !=', 'L');
				//$this->db_cliente->where($this->_db_cliente['tabelas']['pedidos_dw'] . '.' . $this->_db_cliente['campos']['pedidos_dw']['status'] . ' !=', 'I');
				//FIM CUSTOM E0102/2014
			}else{
				$this->db_cliente->where($this->_db_cliente['tabelas']['pedidos_dw'] . '.' . $this->_db_cliente['campos']['pedidos_dw']['status'] . ' !=', 'I');
			}				
			$total = $this->db_cliente->select('COUNT(DISTINCT ' . $this->_db_cliente['campos']['pedidos_dw']['id_pedido'] . ') AS quantidade, SUM(' . $this->_db_cliente['campos']['pedidos_dw']['preco_unitario'] . ' * ' . $this->_db_cliente['campos']['pedidos_dw']['quantidade'] . ') AS valor')
							->from($this->_db_cliente['tabelas']['pedidos_dw'])
							->join($this->_db_cliente['tabelas']['representantes'], $this->_db_cliente['campos']['representantes']['codigo'] . ' = ' . $this->_db_cliente['campos']['pedidos_dw']['codigo_representante'])
							->where(array($this->_db_cliente['tabelas']['representantes'] . '.' . $this->_db_cliente['campos']['representantes']['delecao']  . ' !=' => '*'))
							->get()->row_array();
			
			
			if (!$orcamento) 
			{ 
				$this->db_cliente->where($this->_db_cliente['tabelas']['pedidos_dw'] . '.' . $this->_db_cliente['campos']['pedidos_dw']['status'] . ' !=', 'L');
				$this->db_cliente->where($this->_db_cliente['tabelas']['pedidos_dw'] . '.' . $this->_db_cliente['campos']['pedidos_dw']['status'] . ' !=', 'I');
			}else{
				$this->db_cliente->where($this->_db_cliente['tabelas']['pedidos_dw'] . '.' . $this->_db_cliente['campos']['pedidos_dw']['status'] . ' !=', 'I');
			}
			
			

			$frete = $this->db_cliente->select('SUM(DISTINCT ' . $this->_db_cliente['campos']['pedidos_dw']['valor_frete'] . ') as valor')
							->from($this->_db_cliente['tabelas']['pedidos_dw'])
							->where(array($this->_db_cliente['tabelas']['pedidos_dw'] . '.' .$this->_db_cliente['campos']['pedidos_dw']['delecao']  . ' !=' => '*'))
							->get()->row_array();
							
			
			$total['valor'] += $frete['valor'];
		}	
		// FILTRAGEM PARA PAGINAÇÃO **
		
		$this->load->view('layout', array('conteudo' => $this->load->view('pedidos/aguardando_analise', array('pedidos' => $pedidos, 'total' => $total, 'filtragem' => $this->filtragem($filtros), 'paginacao' => $this->paginacao($total['quantidade']), 'orcamento' => $orcamento), TRUE)));
	}

	
	/*function enviar_email($codigo = NULL)
	{
		$dados = array(
			'erro' => NULL
		);
		
		if (!$this->input->post('emails'))
		{
			$dados['erro'] = 'Digite um e-mail.';
		}
		else
		{
			$emails = explode(', ', $this->input->post('emails'));
			
			foreach ($emails as $email)
			{
				if (!valid_email($email))
				{
					$dados['erro'] = 'O e-mail é "' . $email . '" inválido.';
					break;
				}
			}
			
			if (!$dados['erro'])
			{

				$html .= $this->ver_detalhes($codigo, TRUE);

				
				enviar_email($emails, 'Portal do Representante', $html);
				
			}
		}
		
		echo json_encode($dados);
	}
	*/
	
	function ver_pdf_teste($codigo = NULL, $filial = NULL)
	{	
		$empresa = $this->db->from('empresas')->where('id', 1)->get()->row();
		
		$header = '<table style="border:none;">
						<tr>
							<td rowspan=3><img src="'.base_url() . 'third_party/img/logo.jpg" width="120px" height="52px" style="margin-right: 10px; float:left;"></td>
							<td>'. $empresa->razao_social .' CNPJ: '. $empresa->cnpj .'</td>
						</tr>
						<tr>
							<td>'. $empresa->endereco .' - '. $empresa->bairro .' - '.$empresa->cep .' - '.$empresa->cidade .' - '. $empresa->estado .'</td>
						</tr>
						<tr>
							<td>Telefone: '. $empresa->telefone_1 .' - E-mail: '.$empresa->email .'</td>
						</tr>
					</table>';
		$corpo = $this->ver_detalhes($codigo, TRUE, $filial);
		//$body = $this->print_pedido($corpo);
		//print_r($corpo);
		//$this->load->view('pdf',array('header'=>$header,'html'=>$body));
		
		$this->load->view('pdf',array('header'=>$header,'html'=>$this->load->view('pedidos/ver_detalhes', 
			$corpo, TRUE)));
	}
	
	function ver_pdf($tipo, $codigo = NULL, $filial = NULL){
		
		$empresa = $this->db->from('empresas')->where('id', 1)->get()->row();
		$header = '<table style="border:none;">
						<tr>
							<td rowspan=3><img src="'.base_url() . 'third_party/img/logo.jpg" width="140px" height="52px" style="margin-right: 10px; float:left;"></td>
							<td>'. $empresa->razao_social .' CNPJ: '. $empresa->cnpj .'</td>
						</tr>
						<tr>
							<td>'. $empresa->endereco .' - '. $empresa->bairro .' - '.$empresa->cep .' - '.$empresa->cidade .' - '. $empresa->estado .'</td>
						</tr>
						<tr>
							<td>Telefone: '. $empresa->telefone_1 .' - E-mail: '.$empresa->email .'</td>
						</tr>
					</table>';
		$corpo = $this->ver_detalhes($tipo,$codigo, TRUE, $filial);
		//debug_pre($corpo);
		
		$body = $this->print_pedido($corpo);
		
		$this->load->view('pdf',array('header'=>$header,'html'=>$body));
	}
	
	function enviar_email($tipo,$codigo = NULL, $filial = NULL)
	{

		$dados = array(
			'erro' => NULL
		);
		
		if (!($this->input->post('emails')))
		{
			$dados['erro'] = 'Digite um e-mail.';
		}
		else
		{
			
			$emails = explode(', ', $this->input->post('emails'));
			$emails = array_map('trim',$emails);
			
			foreach ($emails as $email)
			{
				if (!valid_email($email))
				{
					$dados['erro'] = 'O e-mail é "' . $email . '" inválido.';
					break;
				}
			}
			
			if (!$dados['erro'])
			{
				
				//$pagina = utf8_encode($pagina);
				// converte o conteudo para uft-8
				
				// cria o objeto
				//seta o cabeçario
				$empresa = $this->db->from('empresas')->where('id', 1)->get()->row();
				$header = '<table style="border:none;">
								<tr>
									<td rowspan=3><img src="'.base_url() . 'third_party/img/logo.jpg" width="80px" height="52px" style="margin-right: 10px; float:left;"></td>
									<td>'. $empresa->razao_social .' CNPJ: '. $empresa->cnpj .'</td>
								</tr>
								<tr>
									<td>'. $empresa->endereco .' - '. $empresa->bairro .' - '.$empresa->cep .' - '.$empresa->cidade .' - '. $empresa->estado .'</td>
								</tr>
								<tr>
									<td>Telefone: '. $empresa->telefone_1 .' - E-mail: '.$empresa->email .'</td>
								</tr>
							</table>';
				$corpo = $this->ver_detalhes($tipo,$codigo, TRUE, $filial);
				$body = $this->print_pedido($corpo);
				
				if ($this->session->userdata('espelho_impressao_tipo') == 'pedido' || $this->session->userdata('espelho_impressao_tipo') == 'P' || $this->session->userdata('espelho_impressao_tipo') == 'p' ) {
					$tipo_pedido_solicitacao = 'Pedido';
					$anexo_tipo_solicitado = 'pedido';
				} else {
					$tipo_pedido_solicitacao = 'Orçamento';
					$anexo_tipo_solicitado = 'orcamento';
				}
				$html .= '<p>'.$tipo_pedido_solicitacao.' <b>' . $codigo . '</b> em anexo:</p>';
			
				$mpdf = new Pdf();
				$mpdf->SetHTMLHeader($header);
				$mpdf->WriteHTML($body);
				$save_file = "pdf/".$anexo_tipo_solicitado."_" . date('d_m_Y') . "_" . time() . ".pdf";
				//$mpdf->Output(); //Neu xuat ra file 
				$mpdf->Output($save_file, 'F');	
				//$anexo = mpdf("teste24");
				enviar_email($emails, 'Portal do Representante', $html, $save_file);
				
			}
		}
		
		echo json_encode($dados);
	}
	
	function print_pedido($array = null)
	{
		if($array == null){
			$corpo = '<table><tr><td>Erro<td></tr></table>';
		}else{
			$pedido = $array['pedido'];
			$produtos= $array['produtos'];
			$itens= $array['itens'];
			$empresa= $array['empresa'];
			$id_pedido= $array['id_pedido'];
			$obter_formas_pagamento = $array['obter_formas_pagamento'];
			$obter_transportadoras = $array['obter_transportadoras'];
			//'eventos' = $array['pedido']
			ob_start();  //inicia o buffer
			
			?>	
			
				<div style="clear: left;"></div>
				<br />
				<h2>Espelho do <?=($pedido['tipo'] == 'P' || $pedido['tipo'] == 'pedido' ? 'Pedido' : 'Orçamento')?></h2>


				<h4 style="margin-top: 15px;">Informações Gerais</h4>
				<table cellspacing="0px"  style="border-top:solid 1px #d8d8d8; border-right:solid 1px #d8d8d8;" >
					<tr>

						<?php 
						$representante = $this->db_cliente->obter_representante($pedido['codigo_representante']);
						if($representante)
						{	?>
							
								<th style="width: 85px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Representante:</th>
								<td style="width: 210px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=$representante['codigo'] . ' - ' . utf8_encode($representante['nome'])?></td>
							
						<?php
						} ?>
						
						
							<th style="width: 85px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">O.C.:</th>
							<td style="width: 100px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=($pedido['ordem_compra'] ? $pedido['ordem_compra'] : $pedido['codigo_ordem_compra'])?></td>
						
							<th style="width: 100px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Número:</th>
							<td style="width: 100px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=($pedido['id_pedido'] ? $pedido['id_pedido'] : $pedido['codigo'])?></td>
							

						</tr>
						<tr>
							<!--<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Tipo de frete:</th>
							
							<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">
								<?
									if($pedido['tipo_frete'] == 'F')
									{
										echo 'FOB';
									}
									else if($pedido['tipo_frete'] == 'C')
									{
										echo 'CIF';
									}
									else if($pedido['tipo_frete'] == 'R')
									{
										echo 'Redespacho';
									}
									else
									{
										echo $pedido['tipo_frete'];
									}
								?>
							</td>-->
				<?php
					if($pedido['codigo_forma_pagamento'])
					{
						$forma_pagamento = $this->db_cliente->obter_forma_pagamento($pedido ? $pedido['codigo_forma_pagamento'] : $pedido['codigo_forma_pagamento']);
					}
					else
					{
						$forma_pagamento = $this->db_cliente->obter_forma_pagamento($pedido ? $pedido['condicao_pagamento'] : $pedido['condicao_pagamento']);
					}
				?>
			
						<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Emissão:</th>
						<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">
							<?php
								if($pedido['emissao_timestamp'])
								{
									echo date('d/m/Y', $pedido['emissao_timestamp']);
								}
								else
								{
									if($pedido['data_emissao'])
									{
										echo date('d/m/Y', strtotime($pedido['data_emissao']));
									}
									else
									{
										echo 'não há';
									}
								}
							?>
						</td>
					
						<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Forma pgto.:</th>
						<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=utf8_encode($forma_pagamento['descricao'])?></td>
						
						<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Filial:</th>
						<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=($pedido['filial'] ? $pedido['filial'] : $pedido['filial'])?></td>
					</tr>
				</table>
				<div style="clear: left;"></div>

				<?php

				if(($pedido['codigo_cliente'] && $pedido['loja_cliente']) || ($pedido['cliente']['codigo'] && $pedido['cliente']['loja']))
				{
					
					
					if($pedido['codigo_cliente'] && $pedido['loja_cliente'])
					{
						$cliente = $this->db_cliente->obter_cliente($pedido['codigo_cliente'], $pedido['loja_cliente']);
					}
					else
					{
						$cliente = $this->db_cliente->obter_cliente($pedido['cliente']['codigo'], $pedido['cliente']['loja']);
					}
						
					if($cliente)
					{
					?>


					<h4 style="margin-top: 15px;">Informações do Cliente</h4>
				<table cellspacing="0px"  style="border-top:solid 1px #d8d8d8; border-right:solid 1px #d8d8d8;" >
					<tr>
						<th style="width: 85px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Nome:</th>
						<td style="width: 180px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=utf8_encode($cliente['nome'])?></td>
					
						<th style="width: 85px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Endereço:</th>
						<td style="width: 165px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=utf8_encode($cliente['endereco'])?></td>
					
						<th style="width: 100px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">E-mail:</th>
						<td style="width: 100px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=utf8_encode($cliente['email'])?></td>
					</tr>
						
					<tr>
						<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">CPF/CNPJ:</th>
						<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=$cliente['cpf']?></td>
					
						<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Tel.:</th>
						<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=$cliente['telefone']?></td>
					
						<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Contato:</th>
						<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=utf8_encode($cliente['pessoa_contato'])?></td>
					</tr>
					<tr>
						<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Cód./Loja:</th>
						<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=$cliente['codigo']?> / <?=$cliente['loja']?></td>
						
						<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Bairro/CEP:</th>
						<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=utf8_encode($cliente['bairro'])?> / <?=$cliente['cep']?></td>
					
						<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Cidade/UF:</th>
						<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=utf8_encode($cliente['cidade'])?> / <?=$cliente['estado']?></td>
					</tr>
				</table>
					<?php
					}
				}
				else
				{

					$prospect = $this->db->from('prospects')->where('id', $pedido['id_prospects'])->get()->row();

					//Informações do Prospect
				?>
				<h4 style="margin-top: 15px;">Informações do Prospect</h4>
							
							
						<table cellspacing="0px"  style="border-top:solid 1px #d8d8d8; border-right:solid 1px #d8d8d8;" >
							<tr>
								<th style="width: 85px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Nome:</th>
								<td style="width: 180px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=utf8_encode($prospect->nome)?></td>
							
								<th style="width: 85px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">CPF/CNPJ:</th>
								<td style="width: 165px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=$prospect->cpf?></td>
							
								<th style="width: 100px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Telefone:</th>
								<td style="width: 100px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=$prospect->telefone_contato_1?></td>
							</tr>
							<tr>
								<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Código:</th>
								<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=$prospect->id?></td>
							
								<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Cidade/UF:</th>
								<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=utf8_encode($prospect->cidade) . '/' . $prospect->estado?></td>
							
								<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">E-mail:</th>
								<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=utf8_encode($prospect->email_contato_1)?></td>
							</tr>
							<tr>
								<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Endereço:</th>
								<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=utf8_encode($prospect->endereco)?></td>
							
								<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Bairro:</th>
								<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=utf8_encode($prospect->bairro)?></td>
							
								<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">CEP:</th>
								<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=$prospect->cep?></td>
							</tr>
						</table>
				<?php

				}

				?>

				<div style="clear: both;"></div>


				<?php

					if($produtos)
					{
				?>

				<h4 style="margin-top: 15px;">Produtos</h4>

				<table class="novo_grid info largura_tabela  tabela_produtos" cellspacing="0px"  style="border-top:solid 1px #d8d8d8; border-right:solid 1px #d8d8d8;" >
					<thead>
						<tr>
						<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Item</th>
						<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Código</th>
						<th style="width: 140px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Descrição</th>
						<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">U.M.</th>
						<th style="width: 120px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Tabela</th>
						<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Preço Unit. (R$)</th>
						<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Desconto Esp. (%)</th>
						<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Preço Venda (R$)</th>
						<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Filial</th>
						<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Quant.</th>
						<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Total (R$)</th>
						<th style="width: 50px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">IPI (%)</th>
						<th style="width: 50px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">ST (R$)</th>
						<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Total c/ IPI, ST e Desc. (R$)</th>
						<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Peso Total (KG)</th>
						</tr>
					</thead>
					
					<tbody>
					
					<?php
						$item = count($produtos);
						foreach($produtos as $produto)
						{
						if($item%2==0){
							$aux = true;
						}else{
							$aux = false;
						}
						
						if ($produto['desconto'] > 0) {
							$valor_unitario_produto_desconto = ($produto['preco_unitario'] - (($produto['preco_unitario'] * $produto['desconto']) / 100));
						}else{
							$valor_unitario_produto_desconto = $produto['preco_unitario'];
						}
						// ** calcular st
						
						$item_pedido = $produto; 
						$total_dos_produtos_sem_ipi = $valor_unitario_produto_desconto * $produto['quantidade'];
						$total_dos_produtos_com_ipi = $total_dos_produtos_sem_ipi + ($total_dos_produtos_sem_ipi * ($produto['ipi'] / 100));

		//WebService
		//$array = $this->db_cliente->obter_impostos_item($pedido['codigo_cliente'], $pedido['loja_cliente'], $cliente['tipo'], $produto['quantidade'], $produto['preco_unitario'], $produto['codigo_produto'], $produto['filial']);
		//				$array = $this->db_cliente->calcular_st('010', $produto['codigo_produto'], $pedido['codigo_cliente'], $pedido['loja_cliente'], $total_dos_produtos_sem_ipi, $total_dos_produtos_com_ipi);
					//	$valor_st = $array['st'];
						$valor_st = $produto['substituicao_tributaria'];
						// calcular st **
	
					$total_valor_produto = $valor_unitario_produto_desconto * $produto['quantidade']; 
					$total_final_produto = ((($produto['ipi']?$produto['ipi']:0) * $total_valor_produto) / 100) + $total_valor_produto + ($valor_st?$valor_st:0);
						
					?>
					
							
							<tr <?=($aux?' bgcolor="#DDD"':null)?>>
								<td class="center" style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=str_pad($item--, 2, 0, STR_PAD_LEFT)?></td>
								<td class="center" style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=utf8_encode($produto['codigo_produto'])?></td>
								<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=utf8_encode($produto['descricao_produto'])?></td>
								<td class="right" style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=utf8_encode($produto['unidade_medida'])?></td>
								<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">
									<?php
										
										$tabela = $this->db_cliente->obter_tabela_precos($produto['tabela_precos']);
										
										echo utf8_encode($tabela['descricao']);
										
									?>
								</td>
								<td class="right" style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=number_format($produto['preco_unitario'], 5, ',', '.')?></td>
								<td class="right" style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=number_format($produto['desconto'], 5, ',', '.')?></td>
								<td class="right" style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=number_format($valor_unitario_produto_desconto, 5, ',', '.')?></td>
								<td class="right" style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=$produto['filial']?></td>
								<td class="right" style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=$produto['quantidade']?></td>
								<td class="right" style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=number_format($total_valor_produto, 5, ',', '.')?></td>
								<td class="right" style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=$produto['ipi']?></td>
								<td class="right" style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=number_format($produto['substituicao_tributaria'], 5, ',', '.')?></td>
								<td class="right" style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=number_format($total_final_produto, 5, ',', '.')?></td>
								<td class="right" style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=number_format($produto['peso_unitario'] * $produto['quantidade'], 5, ',', '.')?></td>
							</tr>
							
					<?php
						}
					?>
					
					</tbody>
				</table>


				<?php
					//Totais
					$total_valor_pedido = 0;
					foreach ($produtos as $item_pedido)
					{
						//Chamado 1379 - Variável produto alterada para item_pedido:
						if ($item_pedido['desconto'] > 0) {
							$valor_unitario_produto_desconto = ($item_pedido['preco_unitario'] - (($item_pedido['preco_unitario'] * $item_pedido['desconto']) / 100));
						}else{
							$valor_unitario_produto_desconto = $item_pedido['preco_unitario'];
						}
						// ** calcular st
						
						$total_dos_produtos_sem_ipi = $valor_unitario_produto_desconto * $item_pedido['quantidade'];
						$total_dos_produtos_com_ipi = $total_dos_produtos_sem_ipi + ($total_dos_produtos_sem_ipi * ($item_pedido['ipi'] / 100));
						/*
						//WebService
						$array = $this->db_cliente->obter_impostos_item($pedido['codigo_cliente'], $pedido['loja_cliente'], $cliente['tipo'], $item_pedido['quantidade'], $item_pedido['preco_unitario'], $item_pedido['codigo_produto'], $item_pedido['filial']);
						//				$array = $this->db_cliente->calcular_st('010', $item_pedido['codigo_produto'], $pedido['codigo_cliente'], $pedido['loja_cliente'], $total_dos_produtos_sem_ipi, $total_dos_produtos_com_ipi);
						$valor_st = $array['st'];						
						*/
						// calcular st **
						$valor_st = $item_pedido['substituicao_tributaria'];
						$total_valor_produto = $valor_unitario_produto_desconto * $item_pedido['quantidade']; 
						$total_final_produto = ((($item_pedido['ipi']?$item_pedido['ipi']:0) * $total_valor_produto) / 100) + $total_valor_produto + ($valor_st?$valor_st:0);
						
						$total_quantidade += $item_pedido['quantidade'];
						$total_peso += $item_pedido['peso_unitario'] * $item_pedido['quantidade'];
						$total_valor += $total_valor_produto;
						$total_valor_pedido += $total_final_produto;
						$frete = $item_pedido['valor_frete'];
					}
				?>

				<div style="clear:both"></div>

				<h4 style="margin-top: 15px;">Totais</h4>

				<table cellspacing="0px"  class="novo_grid info largura_tabela" style="border-top:solid 1px #d8d8d8; border-right:solid 1px #d8d8d8;">
					<thead>
						<tr>
						<th style="width: 200px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Quant. Vend.</th>
						<th style="width: 200px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Peso Total (KG)</th>
						<!--<th>Frete (R$)</th>
						<th>Frete por KG (R$):</th>-->
						<th style="width: 200px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Produtos (R$)</th>
						<th style="width: 200px; border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Total do Pedido (R$)</th>
						</tr>
					</thead>
					
					<tbody>
						<tr>
							<td  style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px; text-align: right;">
								<?=number_format($total_quantidade, 0, NULL, '.')?>
							</td>
							<td  style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px; text-align: right;">
								<?=number_format($total_peso, 5, ',', '.')?>
							</td>
							<!--<td style="text-align: right;">
								<?//=number_format($frete, 2, ',', '.')?>
							</td>
							<td style="text-align: right;">
								<?//=number_format($frete / $total_peso, 2, ',', '.')?>
							</td>-->
							<td  style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px; text-align: right;">
								<?=number_format($total_valor , 5, ',', '.')?>
							</td>
							<td  style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px; font-weight: bold; text-align: right;">
								<?=number_format($frete + $total_valor_pedido, 5, ',', '.')?>
							</td>
						</tr>
					</tbody>
				</table>
				<?php
					}
					else if($itens)
					{
				?>
					<h4 style="margin-top: 15px;">Produtos</h4>

					<table class="novo_grid info largura_tabela  tabela_produtos" cellspacing="0px" style="border-top: solid 1px #d8d8d8; border-right:solid 1px #d8d8d8;" >
						<thead>
							<tr>
							<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Item</th>
							<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Código</th>
							<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Descrição</th>
							<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">U.M.</th>
							<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Preço Unit. (R$)</th>
							<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Desconto Esp. (%)</th>
							<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Preço Venda (R$)</th>
							<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Filial</th>
							<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Quant. Vend.</th>
							<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Total (R$)</th>
							<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Quant. Fat.</th>
							<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Faturado (R$)</th>
							</tr>
						</thead>
						
						<tbody>
						
						<?php
							$it = 1;
							foreach($itens as $item)
							{
								if($it%2==0){
									$aux = true;
								}else{
									$aux = false;
								}
								
								if ($item['desconto'] > 0) {
									$valor_unitario_produto_desconto = ($item['preco_produto'] - (($item['preco_produto'] * $item['desconto']) / 100));
								}else{
									$valor_unitario_produto_desconto = $item['preco_produto'];
								}
						?>
							
							<tr <?=($aux?' bgcolor="#DDD"':null)?>>
									<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;" class="right"><?=utf8_encode($item['codigo_item'])?></td>
									<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;" class="right"><?=utf8_encode($item['codigo_produto'])?></td>
									<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=utf8_encode($item['descricao_produto'])?></td>
									<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;" class="center"><?=utf8_encode($item['unidade_medida_produto'])?></td>
									<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;" class="right"><?=number_format(str_replace(",", ".", $item['preco_produto']), 5, ',', '.')?></td>
									<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;" class="right"><?=number_format($item['desconto'], 5, ',', '.')?></td>
									<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;" class="right"><?=number_format($valor_unitario_produto_desconto, 5, ',', '.')?></td>
									<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;" class="right"><?=$item['filial']?></td>
									<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;" class="right"><?=$item['quantidade_vendida_produto']?></td>
									<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;" class="right"><?=number_format(str_replace(",", ".", $valor_unitario_produto_desconto) * $item['quantidade_vendida_produto'],5, ',', '.')?></td>
									<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;" class="right"><?=$item['quantidade_faturada_produto']?></td>
									<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;" class="right"><?=number_format(str_replace(",", ".", $valor_unitario_produto_desconto) * $item['quantidade_faturada_produto'],5, ',', '.')?></td>
								</tr>
						<?php
								$quant_vend += str_replace(",", ".", $item['quantidade_vendida_produto']);
								$quant_fat += str_replace(",", ".", $item['quantidade_faturada_produto']);
								$valor_parcial += str_replace(",", ".", $valor_unitario_produto_desconto) * $item['quantidade_faturada_produto'];
								$valor_total += str_replace(",", ".", $valor_unitario_produto_desconto) * $item['quantidade_vendida_produto'];
								
								$it++;
							}
						?>
						
						</tbody>
					</table>
					
					<div style="clear:both"></div>

					<h4 style="margin-top: 15px;">Totais</h4>

					<table cellspacing="0px"  class="novo_grid info largura_tabela" style="border-top:solid 1px #d8d8d8; border-right:solid 1px #d8d8d8;">
						<thead>
							<tr>
								<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Quant. Vend.</th>
								<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Quant. Fat.</th>
								<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Valor Parcial</th>
								<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Valor Total</th>
							</tr>
						</thead>
						
						<tbody>
							<tr>
								<td  style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px; text-align: right;">
									<?=$quant_vend?>
								</td>
								<td  style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px; text-align: right;">
									<?=$quant_fat?>
								</td>
								<td  style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px; text-align: right;">
									<?=number_format($valor_parcial, 5, ',', '.')?>
								</td>
								<td  style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px; --text-align: right;">
									<?=number_format($valor_total, 5, ',', '.')?>
								</td>
							</tr>
						</tbody>
					</table>
					
					
					
				<?php
					if ($pedido['notas_fiscais'])
					{
				?>
					<div style="clear:both"></div>
					<h4 style="margin-top: 15px;">Notas Fiscais</h4>

					<table cellspacing="0px"  class="novo_grid info largura_tabela" style="border-top:solid 1px #d8d8d8; border-right:solid 1px #d8d8d8;">
						<thead>
							<tr>
							<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Código N.F.</th>
							<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Data Emissão</th>
							<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Total (R$)</th>
							<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">ICMS (R$)</th>
							<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">IPI (R$)</th>
							<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Frete</th>
							<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Transp.</th>
							<th style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;">Tel. Transp.</th>
							
							</tr>
						</thead>
						
						<tbody>
						
							<?php
								foreach ($pedido['notas_fiscais'] as $nota_fiscal)
								{
							?>
									<tr>
										
										<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;" class="center"><?=utf8_encode($nota_fiscal['codigo'])?></td>
										<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;" class="center"><?=date('d/m/Y', $nota_fiscal['emissao_timestamp'])?></td>
										<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;" class="right"><?=number_format(str_replace(",", ".", $nota_fiscal['valor_total']), 5, ',', '.')?></td>
										<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;" class="right"><?=number_format(str_replace(",", ".", $nota_fiscal['valor_icms']), 5, ',', '.')?></td>
										<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;" class="right"><?=number_format(str_replace(",", ".",($nota_fiscal['valor_ipi']?$nota_fiscal['valor_ipi']:0)), 5, ',', '.')?></td>
										<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;" class="center"><?=$nota_fiscal['tipo_frete']?></td>
										<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;"><?=utf8_encode($nota_fiscal['transportadora']['nome'])?></td>
										<td style="border-left: solid 1px #d8d8d8; border-bottom: solid 1px #d8d8d8; padding: 2px 2px 2px 2px;" class="right"><?=$nota_fiscal['transportadora']['telefone']?></td>
										
							
									</tr>
							<?php
								}
							?>
						</tbody>
					</table>

				<?php
					}
					
				}
				echo '<br><b>Observação: </b><br>'.$pedido['obs_pedido'];
				$this->session->set_userdata('espelho_impressao_tipo', $pedido['tipo']);
				
				?>
				<?php if ($pedido['vencimento_shazam']) { ?>
					<div style="margin-top: 12px; display: inline-table">
						<strong>Observação de Desconto:</strong> <br />Foram utilizados descontos especiais e irão expirar em <strong> <?php echo protheus2data($pedido['vencimento_shazam']) ?> </strong>
					</div>
				<?php }	?>

				<?php
			$corpo = ob_get_clean();
			// pega o conteudo do buffer, insere na variavel e limpa a memória

		}
		return $corpo;
	}
	
	function gerar_pdf($codigo = NULL, $id = NULL) {
		$arquivo_temporario = tempnam("/tmp", "dompdf_");
		file_put_contents($arquivo_temporario, $this->ver_detalhes($codigo, $id, TRUE));
		header('location: ' . base_url() . 'misc/dompdf/dompdf.php?input_file=' . rawurlencode($arquivo_temporario)) . '&paper=letter&output_file=' . rawurlencode('pedido.pdf');
	}
	
	function gerar_html($codigo = NULL, $id = NULL)
	{
		echo $this->ver_detalhes($codigo, $id, TRUE);
	}
	
	function obter_alertas()
	{
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->config->load('db_cliente_' . $this->db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');
	
		// caso o usuário logado for um representante, vamos mostrar somente os pedidos que ele mesmo fez
		if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')))
		{
			$this->db_cliente->where('ZW_IDUSER', $this->session->userdata('codigo_usuario'));
		}
		//SELECT * FROM SZW010 WHERE ZW_STATUS = 'A' AND D_E_L_E_T_ = '' AND ZW_IDUSER = '000001' GROUP_BY ZW_IDPED
		
		$this->db_cliente->select('ZW_IDPED')->from($this->_db_cliente['tabelas']['pedidos_dw']);
		$total_pedidos_aguardando_analise_comercial = $this->db_cliente->where(array(
								
								'ZW_STATUS =' => 'A',
								//'D_E_L_E_T_ !=' => '*'
								
							))->group_by('ZW_IDPED')->get()->num_rows();
	
		//$dados['pedidos'] = '<p style="color: #0FF; font-size: 12px;"><strong>Pedidos</strong> - ' . anchor('pedidos/aguardando_analise?status=A&representante=&cliente=&ordem_de_compra=&emissao_inicial=&emissao_final=&my_submit=Filtrar#filtros', 'Ver todos &raquo;', 'style="color: #FF0;"') . '</p>';
		$dados['pedidos'] = '<p style="color: #0FF; font-size: 12px;"><strong>Existem '.$total_pedidos_aguardando_analise_comercial.' pedidos aguardando análise.</strong> - ' 
			. anchor('pedidos/aguardando_analise?status=A&representante=&cliente=&ordem_de_compra=&emissao_inicial=&emissao_final=&my_submit=Filtrar#filtros', 'Ver todos &raquo;', 'style="color: #FF0;"') 
			. '</p>';
		
		//if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros'))) {
		//	$total_pedidos_aguardando_analise_comercial = $this->db->from('pedidos')->where('status', 'aguardando_analise_comercial')->get()->num_rows();
		//} else {
		//	$total_pedidos_aguardando_analise_comercial = $this->db->from('pedidos')->where(array('codigo_usuario' => $this->session->userdata('codigo_usuario'), 'status' => 'aguardando_analise_comercial'))->get()->num_rows();
		//}
		//$dados['num_pedidos'] = $total_pedidos_aguardando_analise_comercial;
		//$dados['pedidos'] .= '<p style="margin-top: 10px;"><strong>Aguardando análise comercial:</strong> ' . $total_pedidos_aguardando_analise_comercial . '</p>';
		/*
		$pedido_nao_concluido = $this->db->from('pedidos')->where(array('id_usuario' => $this->session->userdata('id_usuario'), 'status' => NULL))->get()->row();
		
		if ($pedido_nao_concluido)
		{
			$dados['pedidos'] .= '<p><strong>Aviso:</strong> há um pedido não concluído - ' . anchor('pedidos/criar/pedido', '+ Opções &raquo;') . '</p>';
		}
		else
		{
			$dados['pedidos'] .= '<p>&nbsp;</p>';
		}*/
		echo json_encode($dados);
	}
	
	function redirecionar_pedido_nao_concluido()
	{
		$this->db->where('id_usuario', $this->session->userdata('id_usuario'));
		$this->db->where('tipo', 'pedido');
		$this->db->where('status', NULL);
		
		$pedido = $this->db->from('pedidos')->get()->row();
		
		foreach ($pedido as $indice => $valor)
		{
			$pedido->$indice = $valor ? $valor : 0;
		}
		
		redirect(array(
			'pedidos', 'cadastrar', $pedido->tipo, $pedido->codigo_usuario, $pedido->codigo_cliente, $pedido->loja_cliente, $pedido->id_prospect, $pedido->id
		));
	}
	
	function copiar_ajax($id, $codigo = NULL)
	{
		$this->db->where('id_usuario', $this->session->userdata('id_usuario'));
		$this->db->where('tipo', 'pedido');
		$this->db->where('status', NULL);
		
		$pedido = $this->db->from('pedidos')->get()->row();
		
		if ($pedido)
		{
			echo '<p class="erro" style="margin: 0;">Você não pode copiar um pedido enquanto houver um pedido aguardando conclusão.' . br(2) . anchor('pedidos/ver_detalhes/0/' . $pedido->id, 'Clique aqui para ver espelho do pedido aguardando conclusão.') . '</p>';
		}
		else
		{
			if ($id)
			{
				$pedido = $this->db->from('pedidos')->where('id', $id)->get()->row();
				$itens_pedido = $this->db->from('itens_pedidos')->where('id_pedido', $id)->get()->result();
				
				$_pedido = array(
					'id_usuario' => $this->session->userdata('id_usuario'),
					'codigo_usuario' => !in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) ? $this->session->userdata('codigo_usuario') : NULL,
					'tipo' => 'pedido'
				);
				
				foreach ($pedido as $indice => $valor)
				{
					if (!in_array($indice, array('id', 'timestamp', 'id_usuario', 'codigo_usuario', 'tipo', 'status', 'codigo_ordem_compra', 'data_entrega_timestamp', 'observacao', 'motivo_reprovacao')))
					{
						$_pedido[$indice] = $valor;
					}
				}
				
				$this->db->insert('pedidos', $_pedido);
				$id_pedido = $this->db->insert_id();
				
				foreach ($itens_pedido as $item_pedido)
				{
					$_item_pedido = array(
						'id_pedido' => $id_pedido
					);
					
					foreach ($item_pedido as $indice => $valor)
					{
						if (!in_array($indice, array('id', 'id_pedido')))
						{
							$_item_pedido[$indice] = $valor;
						}
					}
					
					$this->db->insert('itens_pedidos', $_item_pedido);
				}
			}
			else if ($codigo)
			{
				$pedido = $this->db_cliente->obter_pedido($codigo);
				
				$this->db->insert('pedidos', array(
					'id_usuario' => $this->session->userdata('id_usuario'),
					'codigo_usuario' => !in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) ? $this->session->userdata('codigo_usuario') : NULL,
					'tipo' => 'pedido',
					'codigo_cliente' => $pedido['cliente']['codigo'],
					'loja_cliente' => $pedido['cliente']['loja'],
					'codigo_forma_pagamento' => $pedido['codigo_forma_pagamento'],
					'codigo_transportadora' => $pedido['codigo_transportadora']
				));
				$id_pedido = $this->db->insert_id();
				
				foreach ($pedido['itens'] as $item_pedido)
				{
					$this->db->insert('itens_pedidos', array(
						'timestamp' => time(),
						'id_pedido' => $id_pedido,
						'codigo' => $item_pedido['codigo_produto'],
						'descricao' => $item_pedido['descricao_produto'],
						'unidade_medida' => $item_pedido['unidade_medida_produto'],
						'preco' => $item_pedido['preco_produto'],
						'quantidade' => $item_pedido['quantidade_vendida_produto']
					));
				}
			}
		}
	}
	
	function obter_pedidos_aguardando_analise_comercial_ajax()
	{
		$pedidos = $this->db->from('pedidos')->where(array('timestamp >' => $this->session->userdata('ultima_checagem_pedidos_aguardando_analise_comercial'), 'status' => 'aguardando_analise_comercial'))->order_by('id', 'desc')->get()->result();
		
		foreach ($pedidos as $pedido)
		{
			$_pedido = $this->db->select('SUM(quantidade) AS quantidade_vendida, SUM(preco * quantidade) AS valor_total')->from('itens_pedidos')->where('id_pedido', $pedido->id)->get()->row();
			
			$_pedidos[] = array_map('trim', array(
				'imagem' => img(base_url() . 'misc/imagens/pedidos/status_' . $pedido->status . '.png'),
				'status' => element($pedido->status, $this->_obter_status()),
				'codigo' => 'não há',
				'id' => $pedido->id,
				'emissao' => date('d/m/Y', $pedido->timestamp),
				'nome_cliente' => $this->db_cliente->obter_nome_cliente($pedido->codigo_cliente, $pedido->loja_cliente),
				'quantidade_vendida' => number_format($_pedido->quantidade_vendida, 0, NULL, '.'),
				'quantidade_faturada' => 0,
				'valor_total' => number_format($_pedido->valor_total, 5, ',', '.'),
				'valor_parcial' => 0
			));
		}
		
		echo json_encode($_pedidos);
		
		$this->session->set_userdata('ultima_checagem_pedidos_aguardando_analise_comercial', time());
	}
	
	function editar_codigo_usuario_criar($novo_codigo = NULL)
	{
		$this->session->set_userdata('codigo_usuario_criar_pedido', $novo_codigo);
		
		redirect('pedidos/criar/pedido');
	}
	
	function obter_informacoes_cliente_ajax($codigo_loja_cliente = NULL)
	{
		$dados = array(
			'cliente_encontrado' => FALSE,
			'html' => NULL
		);
		
		$codigo_loja_cliente = explode('_', $codigo_loja_cliente);
		$codigo_cliente = $codigo_loja_cliente[0];
		$loja_cliente = $codigo_loja_cliente[1];
		
		if ($codigo_cliente && $loja_cliente)
		{
			$cliente = $this->db_cliente->obter_cliente($codigo_cliente, $loja_cliente);
		}
		
		if ($cliente)
		{
			$dados['cliente_encontrado'] = TRUE;
			
			$dados['html'] = '
				<p><strong>' . $cliente['nome'] . '</strong> - ' . $cliente['cpf'] . '</p>
				
				<p style="float: left; margin-right: 10px;"><strong>Última compra:</strong></p>
				
				<div style="clear: both;"></div>
				
				<ul style="float: left; margin-top: 5px; margin-right: 10px;">
					<li>Forma de pagamento: ' . ($cliente['ultimo_pedido']['descricao_forma_pagamento'] ? $cliente['ultimo_pedido']['descricao_forma_pagamento'] : '-') . '</li>
					<li>Desconto: R$ ' . number_format($cliente['ultimo_pedido']['valor_desconto'], 5, ',', '.') . '</li>
					<li>Data: ' . ($cliente['ultimo_pedido']['emissao_timestamp'] ? date('d/m/Y', $cliente['ultimo_pedido']['emissao_timestamp']) : '-') . '</li>
					' . ($cliente['ultimo_pedido']['codigo_pedido'] ? '<li>' . anchor('pedidos/ver_detalhes/' . $cliente['ultimo_pedido']['codigo_pedido'], 'Ver detalhes &raquo;', 'target="_blank"') . '</li>' : NULL) . '
				</ul>
				
				<!--<ul style="float: left; margin-right: 10px;">
					<li><strong>Limite de crédito:</strong> R$ ' . number_format($cliente['limite_credito'], 5, ',', '.') . '</li>
					<li><strong>Títulos em aberto:</strong> R$ ' . number_format($cliente['total_titulos_em_aberto'], 5, ',', '.') . '</li>
					<li><strong>Títulos vencidos:</strong> R$ ' . number_format($cliente['total_titulos_vencidos'], 5, ',', '.') . '</li>
				</ul>
				
				<ul style="float: left; margin-right: 10px;">
					<li><strong>Endereço:</strong> ' . $cliente['endereco'] . '</li>
					<li><strong>Bairro:</strong> ' . $cliente['bairro'] . '</li>
					<li><strong>CEP:</strong> ' . $cliente['cep'] . '</li>
				</ul>
				
				<ul style="float: left; margin-right: 10px;">
					<li><strong>Cidade:</strong> ' . $cliente['cidade'] . '</li>
					<li><strong>Estado:</strong> ' . $cliente['estado'] . '</li>
				</ul>-->
				
				<div style="clear: both;"></div>
			';
		}
		
		echo json_encode($dados);
	}
	
	function obter_informacoes_prospect_ajax($id_prospect = NULL)
	{
		$dados = array(
			'prospect_encontrado' => FALSE,
			'html' => NULL
		);
		
		if ($id_prospect)
		{
			$prospect = $this->db->from('prospects')->where('id', $id_prospect)->get()->row();
		}
		
		if ($prospect)
		{
			$dados['prospect_encontrado'] = TRUE;
			
			$dados['html'] = '
				<p><strong>' . $prospect->nome . '</strong> - ' . $prospect->cpf . '</p>
				
				<ul style="float: left; margin-right: 10px;">
					<li><strong>Endereço:</strong> ' . $prospect->endereco . '</li>
					<li><strong>Bairro:</strong> ' . $prospect->bairro . '</li>
					<li><strong>CEP:</strong> ' . $prospect->cep . '</li>
				</ul>
				
				<ul style="float: left; margin-right: 10px;">
					<li><strong>Cidade:</strong> ' . $prospect->cidade . '</li>
					<li><strong>Estado:</strong> ' . $prospect->estado . '</li>
				</ul>
				
				<div style="clear: both;"></div>
			';
		}
		
		echo json_encode($dados);
	}
	
	function criar($tipo = 'pedido', $codigo_usuario = NULL, $codigo_cliente = NULL, $loja_cliente = NULL, $id_prospect = NULL, $id_pedido = NULL)
	{
	
		// Sessão - Buscar dados pedidos
		if($id_pedido)
		{
			$busca_pedido = $this->db->from('pedidos')->where('id', $id_pedido)->get()->row();
			
			if($busca_pedido)
			{
				$dados_pedidos = $this->session->userdata('dados_pedidos');

				$_POST['codigo_usuario'] = $dados_pedidos['codigo_usuario'];
				$_POST['codigo_loja_cliente'] = $dados_pedidos['codigo_loja_cliente'];
				$_POST['id_feira'] = $dados_pedidos['id_feira'];
				$_POST['codigo_forma_pagamento'] = $dados_pedidos['codigo_forma_pagamento'];
				$_POST['tipo_frete'] = $dados_pedidos['tipo_frete'];
				$_POST['codigo_transportadora'] = $dados_pedidos['codigo_transportadora'];
				$_POST['codigo_ordem_compra'] = $dados_pedidos['codigo_ordem_compra'];
				$_POST['observacao_pedido_imediato'] = $dados_pedidos['observacao_pedido_imediato'];
				
				
			}
			else
			{	
				$this->session->unset_userdata('dados_pedidos');
			}
		
		}
		
	
		if (!in_array($tipo, array('pedido', 'orcamento')))
		{
			redirect();
		}
		
		// ** obter pedido
		
		if (!$id_pedido)
		{
			$this->db->where('id_usuario', $this->session->userdata('id_usuario'));
			$this->db->where('tipo', $tipo);
			$this->db->where('status', NULL);
		}
		else
		{
			$this->db->where('id', $id_pedido);
		}
		$pedido = $this->db->from('pedidos')->get()->row();
		
		if (!$pedido)
		{
			$this->db->insert('pedidos', array(
				'id_usuario' => $this->session->userdata('id_usuario'),
				'tipo' => $tipo
			));
			$pedido = $this->db->from('pedidos')->where('id', $this->db->insert_id())->get()->row();
		}
		else if ($pedido && !$_POST && !$id_pedido)
		{
			//redirect('pedidos/ver_detalhes/0/' . $pedido->id);
		}
		
		if ($codigo_cliente && $codigo_cliente != '0' && !$_POST)
		{
			$this->db->update('pedidos', array(
				'codigo_cliente' => $codigo_cliente,
				'loja_cliente' => $loja_cliente,
			), array('id' => $pedido->id));
			$pedido = $this->db->from('pedidos')->where('id', $pedido->id)->get()->row();
		}
		if ($id_prospect && $id_prospect != '0' && !$_POST)
		{
			$this->db->update('pedidos', array(
				'id_prospect' => $id_prospect,
			), array('id' => $pedido->id));
			$pedido = $this->db->from('pedidos')->where('id', $pedido->id)->get()->row();
		}
		
		$itens_pedido = $this->db->from('itens_pedidos')->where('id_pedido', $pedido->id)->get()->row();
		
		// obter pedido **
		
		// se o usuário apenas modificou o grupo não há necessidade de fazer validações
		if (!$_POST['grupo_produtos_modificado'] && !$_POST['somente_postar'])
		{
			if($_POST["continuar"] || $_POST["adicionar_item"])
			{
				// validar
				if ($_POST)
				{
					if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) && !$this->input->post('codigo_usuario'))
					{
						$erro = 'Selecione um representante.';
					}
					else if ($pedido->tipo == 'pedido' && !$this->input->post('codigo_loja_cliente'))
					{
						$erro = 'Selecione um cliente.';
					}
					else if ($pedido->tipo == 'orcamento' && !$this->input->post('codigo_loja_cliente') && !$this->input->post('id_prospect'))
					{
						$erro = 'Selecione um cliente ou um prospect.';
					}
					else if ($tipo == 'pedido' && !$this->input->post('codigo_forma_pagamento'))
					{
						$erro = 'Selecione uma forma de pagamento.';
					}
					else if ($tipo == 'pedido' && $this->input->post('tipo_frete') == 'FOB' && !$this->input->post('codigo_transportadora'))
					{
						$erro = 'Selecione uma transportadora.';
					}
					else if ($tipo == 'pedido' && $this->input->post('tipo_frete') == 'Redespacho' && !$this->input->post('codigo_transportadora'))
					{
						$erro = 'Selecione uma transportadora.';
					}
					else if ($this->input->post('data_entrega') && !$this->_validar_data($this->input->post('data_entrega')))
					{
						$erro = 'Digite uma data de entrega válida.';
					}
					else if (!$itens_pedido)
					{
						$erro = 'Adicione um ou mais itens no pedido.';
					}
					else
					{
						if ($this->input->post('codigo_loja_cliente'))
						{
							$codigo_loja_cliente = explode('|', $this->input->post('codigo_loja_cliente'));
							$codigo_cliente = $codigo_loja_cliente[0];
							$loja_cliente = $codigo_loja_cliente[1];
							
							$cliente = $this->db_cliente->obter_cliente($codigo_cliente, $loja_cliente);
						}
						else if ($this->input->post('id_prospect'))
						{
							$prospect = $this->db->from('prospects')->where('id', $this->input->post('id_prospect'))->get()->row();
						}
						
						$this->db->update('pedidos', array(
							'timestamp' => $pedido->timestamp ? $pedido->timestamp : time(),
							'status' => ($pedido->tipo == 'orcamento') ? 'concluido' : $pedido->status,
							'codigo_usuario' => $this->input->post('codigo_usuario') ? $this->input->post('codigo_usuario') : $this->session->userdata('codigo_usuario'),
							'codigo_cliente' => !$this->input->post('criar_para') || $this->input->post('criar_para') == 'cliente' ? $cliente['codigo'] : NULL,
							'loja_cliente' => !$this->input->post('criar_para') || $this->input->post('criar_para') == 'cliente' ? $cliente['loja'] : NULL,
							'id_prospect' => !$this->input->post('criar_para') || $this->input->post('criar_para') == 'prospect' ? $prospect->id : NULL,
							'codigo_forma_pagamento' => $this->input->post('codigo_forma_pagamento'),
							'tipo_frete' => $this->input->post('tipo_frete'),
							'codigo_transportadora' => in_array($this->input->post('tipo_frete'), array('FOB', 'Redespacho')) ? $this->input->post('codigo_transportadora') : NULL,
							'codigo_ordem_compra' => $this->input->post('codigo_ordem_compra'),
							'data_entrega_timestamp' => strtotime($this->input->post('data_entrega')),
							'observacao_pedido_imediato' => $this->input->post('observacao_pedido_imediato'),
							'observacao_pedido_programado' => $this->input->post('observacao_pedido_programado'),
							'id_feira' => $this->input->post('id_feira'),
							'peso_total' => $this->input->post('peso_total'),
							'frete' => $this->input->post('frete')
						), array('id' => $pedido->id));
						
						
						
						// - Session - Dados do Pedido
						$sessao_dados_pedidos = array(
								   'codigo_usuario' => $this->input->post('codigo_usuario'),
								   'codigo_loja_cliente' => $this->input->post('codigo_loja_cliente'),
								   'id_feira' => $this->input->post('id_feira'),
								   'codigo_forma_pagamento' => $this->input->post('codigo_forma_pagamento'),
								   'tipo_frete' => $this->input->post('tipo_frete'),
								   'codigo_transportadora' => $this->input->post('codigo_transportadora'),
								   'codigo_ordem_compra' => $this->input->post('codigo_ordem_compra'),
								   'observacao_pedido_imediato' => $this->input->post('observacao_pedido_imediato')
							   );

						$this->session->set_userdata('dados_pedidos', $sessao_dados_pedidos);

						redirect('pedidos/ver_detalhes/0/' . $pedido->id);
					}
				}
				else
				{
					foreach ($pedido as $indice => $valor)
					{
						if ($indice == 'data_entrega_timestamp' && $valor)
						{
							$_POST['data_entrega'] = date('Y-m-d', $valor);
						}
						else if ($indice == 'codigo_cliente' && $valor)
						{
							$_POST['codigo_loja_cliente'] = $pedido->codigo_cliente . '|' . $pedido->loja_cliente;
						}
						else
						{
							$_POST[$indice] = $valor;
						}
					}
					
					//$criar_para = $pedido->id_prospect && $pedido->id_prospect != '0' ? 'prospect' : 'cliente';
					$criar_para = $pedido->codigo_cliente ? 'cliente' : 'prospect';
					$_POST['criar_para'] = $_POST['criar_para'] ? $_POST['criar_para'] : $criar_para;
				}
			}
		}
		
		$conteudo .= form_open(current_url());
		
		$conteudo .= '<div id="informacoes_cliente_prospect" style="background-color: #EEE; border: 1px solid #DDD; display: none; float: right; padding: 0 10px 10px 10px; -webkit-border-radius: 5px;"></div>';
		
		$conteudo .= heading('Cadastrar ' . ($tipo == 'orcamento' ? 'Orçamento' : 'Pedido'), 2);
		
		if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores')))
		{
			// se nenhum código de usuário foi informado vamos pegar o primeiro código disponível no dropdown
			if (!$codigo_usuario)
			{
				$codigo_usuario = $this->_obter_indice_primeiro_usuario_usuarios('codigo');
			}
			
			$_codigo_usuario = $codigo_usuario;
		}
		else
		{
			$_codigo_usuario = $this->session->userdata('codigo_usuario');
		}
		
		$codigo_usuario = $codigo_usuario ? $codigo_usuario : $_codigo_usuario;
		
		if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores'))) {
			/*$conteudo .= '
				<p>' . form_label('Representante: ' . br() . form_dropdown('codigo_usuario', $this->_obter_representantes(FALSE), $this->input->post('codigo_usuario') ? $this->input->post('codigo_usuario') : $codigo_usuario, 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" onChange="editar_codigo_usuario"')) . '</p>
				
				<script type="text/javascript">
					function editar_codigo_usuario(novo_codigo)
					{
						window.location = "' . site_url('pedidos/criar/' . $tipo) . '/" + novo_codigo + "/' . ($codigo_cliente ? $codigo_cliente : 0) . '/' . ($loja_cliente ? $loja_cliente : 0) . '/' . ($id_prospect ? $id_prospect : 0) . '/' . ($id_pedido ? $id_pedido : 0) . '";
					}
				</script>
			';*/
			
			$conteudo .= '
				
				<p>Usuário:</p>
				
				' . $this->_obter_dropdown_representantes('codigo', array('name' => 'codigo_usuario', 'value' => $this->input->post('codigo_usuario') ? $this->input->post('codigo_usuario') : $codigo_usuario, 'id' => 'usuarios'), FALSE) . '
			
				<script type="text/javascript">

						$(document).ready(function(){
								
								$("#usuarios").change(function(){
										var valor = $(this).val();
										window.location = "' . site_url('pedidos/criar/' . $tipo) . '/" + novo_codigo + "/' . ($codigo_cliente ? $codigo_cliente : 0) . '/' . ($loja_cliente ? $loja_cliente : 0) . '/' . ($id_prospect ? $id_prospect : 0) . '/' . ($id_pedido ? $id_pedido : 0) . '";
								});
								
						});

				</script>
				
			';
		}
		
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		
		$conteudo .= heading('Informações Gerais', 3);
		
		$conteudo .= '<div style="float: left; margin-right: 10px;">';
		if ($pedido->tipo == 'orcamento')
		{
			$conteudo .= '<p>Cadastrar para:<br />' . form_label(form_radio('criar_para', 'prospect', !$_POST['criar_para'] || $_POST['criar_para'] == 'prospect' ? TRUE : FALSE) . ' Prospect') . ' ' . form_label(form_radio('criar_para', 'cliente', $_POST['criar_para'] == 'cliente' ? TRUE : FALSE) . ' Cliente') . '</p>';
			$conteudo .= '
				<script type="text/javascript">
					$(document).ready(function() {
						verificar_criar_para();
						
						$("input[name=criar_para]").click(function() {
							verificar_criar_para();
						});
					});
					
					function verificar_criar_para()
					{
						if ($("input[name=criar_para]:checked").val() == "cliente")
						{
							$("#cliente").show();
							$("#prospect").hide();
							
							if ($("input[name=codigo_loja_cliente]").val()) 
								obter_informacoes_cliente($("input[name=codigo_loja_cliente]").val());
							else
								$("#informacoes_cliente_prospect").hide();
						}
						else
						{
							$("#cliente").hide();
							$("#prospect").show();
							
							if ($("input[name=id_prospect]").val())
								obter_informacoes_prospect($("input[name=id_prospect]").val());
							else
								$("#informacoes_cliente_prospect").hide();
						}
					}
				</script>
			';
		}
		
		$conteudo .= '<p style="float: left; margin-right: 10px;" id="cliente">Cliente:<br><input type="text" style="width: 250px" name="codigo_loja_cliente" class="autocomplete" value="' . $this->input->post("_codigo_loja_cliente") . '" alt="' . $this->input->post("codigo_loja_cliente") . '" data-url="' . site_url('orcamentos/obter_clientes' . ($this->session->userdata('codigo_usuario_consultar_orcamentos') ? ('/' . $this->session->userdata('codigo_usuario_consultar_orcamentos')) : NULL)) . '"></p>';
		
		
		if ($pedido->tipo == 'orcamento')
		{
		
			$conteudo .= '<p style="float: left; margin-right: 10px;" id="prospect">Prospect:<br><input type="text" style="width: 250px" name="id_prospect" class="autocomplete" value="' . $this->input->post("_id_prospect") . '" alt="' . $this->input->post("id_prospect") . '" data-url="' . site_url('orcamentos/obter_prospects' . ($this->session->userdata('codigo_usuario_consultar_orcamentos') ? ('/' . $this->session->userdata('codigo_usuario_consultar_orcamentos')) : NULL)) . '"></p>';
		
		}
		$conteudo .= '</div>';
		
		// ** tipo de desconto
		//$conteudo .= '<p style="float: left; margin-right: 10px;">Desconto:' . br() . form_radio('tipo_desconto', 'sem_desconto', !$this->input->post('tipo_desconto') || $this->input->post('tipo_desconto') == 'sem_desconto' ? TRUE : FALSE, 'dojoType="dijit.form.RadioButton"') . ' Sem desconto ' . form_radio('tipo_desconto', 'por_item', $this->input->post('tipo_desconto') == 'por_item' ? TRUE : FALSE, 'dojoType="dijit.form.RadioButton"') . ' Por item ' . form_radio('tipo_desconto', 'geral', $this->input->post('tipo_desconto') == 'geral' ? TRUE : FALSE, 'dojoType="dijit.form.RadioButton"') . ' Geral</p>';
		
		$conteudo .= '
			<script type="text/javascript">
				
				/*
				$(document).ready(function() {
					verificar_tipo_desconto();
					
					$("input[name=tipo_desconto]").live("click", function() {
						verificar_tipo_desconto();
					});
				});
				
				function verificar_tipo_desconto()
				{
					var tipo_desconto = $("input[name=tipo_desconto]:checked").val();
					
					switch (tipo_desconto)
					{
						case "sem_desconto":
							$("#desconto").val("").parents("p").hide();
							$("#valor_desconto").val("").parents("p").hide();
							
							$("#desconto_total").val("").parents("p").hide();
							$("#valor_desconto_total").val("").parents("p").hide();
							
							calcular_descontos();
						break;
						
						case "por_item":
							$("#desconto").parents("p").show();
							$("#valor_desconto").parents("p").show();
							
							$("#valor_desconto_total").val("").parents("p").hide();
							$("#desconto_total").val("").parents("p").hide();
							
							calcular_descontos();
						break;
						
						case "geral":
							$("#desconto").val("").parents("p").hide();
							$("#valor_desconto").val("").parents("p").hide();
							
							$("#valor_desconto_total").parents("p").show();
							$("#desconto_total").parents("p").show();
							
							 calcular_descontos();
						break;
					}
				}
				*/
				
				function calcular_descontos()
				{
					$.post("' . site_url('pedidos/calcular_descontos/' . $pedido->id) . '", $("form").serialize(), function() {
						obter_itens();
					});
				}
			</script>
		';
		// tipo de desconto **
		
		$conteudo .= '<input type="hidden" name="somente_postar" value="" /><div id="somente_postar"></div>';
		
		$conteudo .= hnordt_gerar_feiras_ativas();
		
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= '
			<div style="clear: both;"></div>
			
			<script type="text/javascript">
				$(document).ready(function() {
					setTimeout(function() {
						obter_informacoes_cliente($("input[name=codigo_loja_cliente]").val());
						
						obter_informacoes_prospect($("input[name=id_prospect]").val());
					}, 1000);
				});
				
				function obter_informacoes_cliente(codigo_loja_cliente)
				{
				
					$.get("' . site_url('pedidos/obter_informacoes_cliente_ajax') . '/" + codigo_loja_cliente.replace("|", "_"), function(dados) {
						if (dados.cliente_encontrado)
						{
							$("#informacoes_cliente_prospect").slideDown().html(dados.html);
						}
						else
						{
							$("#informacoes_cliente_prospect").hide();
						}
					}, "json");
					
					
					//Calcular Frete
					var peso_total = $("#valor_peso_total").val();
					
					$.ajax({
						type: "POST",
						url: "' . site_url('pedidos/obter_valor_frete_ajax') . '/",
						data: {peso_total : peso_total, cliente : codigo_loja_cliente},
						success: function(dados){
							dados = dados.split("|");
						
							$("#valor_frete").html("R$ " + dados[0]);
							$("#frete").val(dados[1]);
							
							//frete por kg
							var valor_peso_total = $("#valor_peso_total").val();
							var valor_frete = dados[1];
							var frete_kg = valor_frete / valor_peso_total;
							$("#frete_kg").html("R$ " + number_format(frete_kg, 5, ",", "."));
							
							
							//Frete + Valor Pedido
							$("#total_pedido_frete").html("R$ " + number_format((parseFloat(valor_frete) + parseFloat($("#valor_total_pedido").val())), 5, ",", "."));
							$("#valor_total_pedido_frete").val(parseFloat(valor_frete) + parseFloat($("#valor_total_pedido").val()));
							
						}
					});
					
					
				}
				
				function obter_informacoes_prospect(id_prospect)
				{
					$.get("' . site_url('pedidos/obter_informacoes_prospect_ajax') . '/" + id_prospect, function(dados) {
						if (dados.prospect_encontrado)
						{
							$("#informacoes_cliente_prospect").slideDown().html(dados.html);
						}
						else
						{
							$("#informacoes_cliente_prospect").hide();
						}
					}, "json");
				}
			</script>
		';
		
		$conteudo .= '<h3 id="adicionar_item">Adicionar Item</h3>';
		
		$grupos_produtos = $this->_obter_grupos_produtos();
		$quantidade_grupos_produtos = count($grupos_produtos);
		
		if ($quantidade_grupos_produtos == 0)
		{
			$_POST['codigo_grupo_produtos'] = NULL;
			
			$conteudo .= '
				<script type="text/javascript">
					$(document).ready(function() {
						$("#codigo_grupo_produtos").parents("p").hide();
					});
				</script>
			';
		}
		else if ($quantidade_grupos_produtos == 1)
		{
			$_grupos_produtos = array_keys($grupos_produtos);
			
			$_POST['codigo_grupo_produtos'] = $_grupos_produtos[0];
			
			$conteudo .= '
				<script type="text/javascript">
					$(document).ready(function() {
						$("#codigo_grupo_produtos").parents("p").hide();
					});
				</script>
			';
		}
		
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Grupo de produtos:' . br() . form_dropdown('codigo_grupo_produtos', $grupos_produtos, $this->input->post('codigo_grupo_produtos'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" id="codigo_grupo_produtos" onChange="modificar_grupo_produtos" intermediateChanges="true" ' . (!$_POST['codigo_grupo_produtos'] ? 'displayedValue=""' : NULL))) . '<input type="hidden" name="grupo_produtos_modificado" value="0" /></p>';
		$conteudo .= '
			<script type="text/javascript">
				function modificar_grupo_produtos(valor) {
					$("input[name=grupo_produtos_modificado]").val("1");
					$("form").submit();
				}
			</script>
		';
		
		if ($_POST['grupo_produtos_modificado'] || $_POST['somente_postar']) {
			$conteudo .= '
				<script type="text/javascript">
					$(document).ready(function() {
						$("html, body").animate({ scrollTop: $("#somente_postar").offset().top }, 0);
					});
				</script>
			';
		}
		
		/*
		$conteudo .= '
			<script type="text/javascript">
				$(document).ready(function(){
					$("#tabela_precos").change(function(){
						
					});
				});
			</script>
		';
		*/
	
		//$conteudo .= '<p style="float: left; margin-right: 10px;" id="codigo_tabela_precos">' . form_label('Tabela de preços:' . br() . form_dropdown('codigo_tabela_precos', $this->_obter_tabelas_precos(), $this->input->post('codigo_tabela_precos'), 'id="tabela_precos" dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false"')) . '</p>';
		
		$conteudo .= '<p style="float: left; margin-right: 10px;" id="codigo_tabela_precos">' . form_label('Tabela de preços:' . br() . form_dropdown('codigo_tabela_precos', $this->_obter_tabelas_precos($codigo_usuario), $this->input->post('codigo_tabela_precos'), 'id="tabela_precos" onChange="submit()"')) . '</p>';
		
		
		if ($quantidade_grupos_produtos == 0 || $_POST['codigo_grupo_produtos']) {
			//$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Produto:' . br() . form_dropdown('codigo_produto', $this->_obter_produtos($_POST['codigo_grupo_produtos']), $this->input->post('codigo_produto'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" id="codigo_produto" style="width: 400px;"')) . '</p>';
			$itens_pedido = $this->db->from('itens_pedidos')->where('id_pedido', $pedido->id)->order_by('id', 'asc')->get()->result_array();
			
			// opcionais, somente exibir se existirem opcionais a serem exibidos
			if ($itens_pedido && $this->_obter_opcionais_produto($itens_pedido[0]['codigo']))
			{
				$conteudo .= '<p style="color: #060; font-weight: bold; font-size: 14px;">O produto "' . $itens_pedido[0]['descricao'] . '" foi adicionado. Agora você pode adicionar os opcionais.</p>';
				$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Opcionais para o produto ' . $itens_pedido[0]['descricao'] . ':' . br() . form_dropdown('codigo_produto', $this->_obter_opcionais_produto($itens_pedido[0]['codigo']), $this->input->post('codigo_produto'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" id="codigo_produto" style="width: 400px;"')) . '</p>';
			}
			else
			{
				//$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Produto:' . br() . form_dropdown('codigo_produto', $this->_obter_produtos($_POST['codigo_grupo_produtos']), $this->input->post('codigo_produto'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" id="codigo_produto" style="width: 600px;"')) . '</p>';
			
				$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Produto:' . br() . form_dropdown('codigo_produto', $this->_obter_produtos_tabela_precos($_POST['codigo_grupo_produtos'], $_POST['codigo_tabela_precos']), $this->input->post('codigo_produto'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" id="codigo_produto" style="width: 600px;"')) . '</p>';
			
			}
		}
		
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Quantidade:' . br() . form_input('quantidade', $this->input->post('quantidade'), 'dojoType="dijit.form.NumberSpinner" constraints="{min: 1}"')) . '</p>';
		
		//$conteudo .= '<p style="float: left; margin-right: 10px;">&nbsp;' . br() . form_label(form_radio('usar_tabela_precos', 'nao', TRUE, 'dojoType="dijit.form.RadioButton"') . ' Digitar preço') . ' ' . form_label(form_radio('usar_tabela_precos', 'sim', FALSE, 'dojoType="dijit.form.RadioButton"') . ' Usar tabelas de preços') . '</p>';
		/*
		$conteudo .= '
			<script type="text/javascript">
				$(document).ready(function() {
					$("input[name=usar_tabela_precos]").live("click", function() {
						if ($(this).val() == "sim") {
							$("#codigo_tabela_precos").show();
							$("#preco").hide();
						} else {
							$("#codigo_tabela_precos").hide();
							$("#preco").show();
						}
					});
				});
			</script>
		';
		*/
		$conteudo .= '<div style="clear: both;"></div>';
		//$conteudo .= '<p style="float: left; margin-right: 10px;" id="codigo_tabela_precos">' . form_label('Tabela de preços:' . br() . form_dropdown('codigo_tabela_precos', $this->_obter_tabelas_precos(), $this->input->post('codigo_tabela_precos'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false"')) . '</p>';
		//$conteudo .= '<p style="float: left; margin-right: 10px;" id="preco">' . form_label('Preço:' . br() . form_input('preco', $this->input->post('preco'), 'dojoType="dijit.form.CurrencyTextBox" currency="R$ " displayedValue="R$ 0,00" promptMessage="Para centavos use a vírgula. Exemplo: 1423,20" id="preco"')) . '</p>';
		//$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Quantidade:' . br() . form_input('quantidade', $this->input->post('quantidade'), 'dojoType="dijit.form.NumberSpinner" constraints="{min: 1}"')) . '</p>';
		//$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Desconto (%):' . br() . form_input('desconto', $this->input->post('desconto'), 'dojoType="dijit.form.NumberSpinner" constraints="{min: 0, places: 2}" promptMessage="Para decimais use a vírgula. Exemplo: 13,20" id="desconto" onChange="calcular_valor_desconto"')) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '<p>' . form_submit('adicionar_item', 'Adicionar Item') . '</p>';
		$conteudo .= '
			<script type="text/javascript">
				$(document).ready(function() {
					obter_itens();
					
					$("input[name=adicionar_item]").live("click", function() {
						var _this = this;
						var valor = $(this).val();
						
						$(this).val("Carregando...").attr("disabled", "disabled");
						
						$.post("' . site_url('pedidos/adicionar_item_ajax/' . $pedido->id) . '", $("form").serialize(), function(dados) {
							if (dados.erro)
							{
								alert(dados.erro);
							}
							else
							{
								if (dados.mensagem) alert(dados.mensagem);
								
								obter_itens();
								
								$("input[name=somente_postar]").val("1");
								$("form").submit();
							}
							
							$(_this).val(valor).removeAttr("disabled");
						}, "json");
						
						return false;
					});
					
					$(".editar_preco_item").live("click", function() {
						var _this = this;
						
						var preco = prompt("Digite o novo preço: (exemplo: 5.65)");
						
						$.post("' . site_url('pedidos/editar_item_ajax/' . $pedido->id) . '", { id: $(this).attr("alt"), preco: preco }, function(dados) {
							if (dados.erro)
							{
								alert(dados.erro);
							}
							else
							{
								 if (dados.mensagem) alert(dados.mensagem);
								 
								 obter_itens();
							}
						}, "json");
						
						return false;
					});
					
					$(".editar_quantidade_item").live("click", function() {
						var _this = this;
						
						var quantidade = prompt("Digite a nova quantidade:");
						
						$.post("' . site_url('pedidos/editar_item_ajax/' . $pedido->id) . '", { id: $(this).attr("alt"), quantidade: quantidade }, function(dados) {
							if (dados.erro)
							{
								alert(dados.erro);
							}
							else
							{
								 if (dados.mensagem) alert(dados.mensagem);
								 
								 obter_itens();
							}
						}, "json");
						
						return false;
					});
					
					$(".transferir_quantidade_item").live("click", function() {
						var _this = this;
						
						var quantidade = prompt("Digite a quantidade a ser transferida:");
						
						$.post("' . site_url('pedidos/transferir_quantidade_item_ajax/' . $pedido->id) . '", { id: $(this).attr("alt"), quantidade: quantidade }, function(dados) {
							if (dados.erro)
							{
								alert(dados.erro);
							}
							else
							{
								 if (dados.mensagem) alert(dados.mensagem);
								 
								 obter_itens();
							}
						}, "json");
						
						return false;
					});
					
					$(".excluir_item").live("click", function() {
						if (!confirm("Você realmente deseja excluir este item?")) {
							return false;
						}
						
						$.post("' . site_url('pedidos/excluir_item_ajax/' . $pedido->id) . '", { id: $(this).attr("alt") }, function(dados) {
							obter_itens();
							
							$("input[name=somente_postar]").val("1");
							$("form").submit();
						}, "json");
						
						return false;
					});
				});
				
				function obter_itens()
				{
					$.get("' . site_url('pedidos/obter_itens_ajax/' . $pedido->id) . '", function(dados) {
						$("#tabela_itens_pedido tbody").empty();
						
						var codigo = 0;
						$.each(dados.itens, function(indice, valor) {
							if (new Number(valor._quantidade) > 0) {
								var borda = codigo && codigo != valor.codigo? "" : "";
								
								$("#tabela_itens_pedido tbody").prepend("<tr style=\"" + borda + " background-color: " + valor.cor + ";\"><td style=\"text-align: right;\">" + valor.codigo_real + "</td> <td>" + valor.descricao + "</td> <td>" + valor.unidade_medida + "</td> <td>" + valor.codigo_tabela_precos + "</td> <td>" + valor.preco + "</td> <td><a href=\"#\" class=\"editar_quantidade_item\" alt=\"" + valor.id + "\">" + valor.quantidade + "</a></td> <td>" + valor.valor_preco_quant + "</td> <td>" + valor.ipi + "</td> <td>" + valor.valor_total_com_ipi + "</td> <td>" + valor.st + "</td> <td>" + valor.total_geral + "</td> <td>" + valor.peso + "</td>  <td> <a href=\"#\" class=\"excluir_item\" alt=\"" + valor.id + "\"><img src=\"' . (base_url() . 'misc/imagens/cross.png') . '\" title=\"Excluir\" /></a></td></tr>");
								
								codigo = valor.codigo;
							}
						});
						
						//$("#tabela_itens_pedido tbody").append("<tr><td colspan=\"8\"><strong>Totais</strong></td><td><strong>" + dados.totais.total_st + "</strong></td>  <td><strong>" + dados.totais.total_desconto + "</strong></td> <td><strong style=\"color: #060; font-size: 16px;\" id=\"valor_total_com_ipi_e_desconto\">" + dados.totais.valor_total_com_ipi_e_desconto + "</strong></td> <td>" + dados.totais.peso_total + "</td> </tr>");
						
						$("#peso_total").html(dados.totais.peso_total);
						$("#valor_peso_total").val(dados.totais.valor_peso_total);
						
						$("#total_pedido").html("R$ " + dados.totais.valor_total_com_ipi_e_desconto);
						
						$("#valor_total_pedido").val(dados.totais.valor_total_com_ipi_e_desconto_number);
						
						$("#valor_frete").html(dados.totais.valor_frete);
						
						/*var i = 1;
						var qtd_i = $("#tabela_itens_pedido tbody tr").size();
						$("#tabela_itens_pedido tbody tr").each(function() {
							if (i % 2 == 0 && i != qtd_i)
							{
								$(this).find("td").css("borderBottom", "2px solid #000");
							}
							
							i++;
						});*/
						
						alinhar_conteudo_tabelas();
					}, "json");
				}
				
			</script>
		';
		
		$conteudo .= heading('Itens Adicionados', 3);
		$conteudo .= '
			<div class="ajuda"><p>Clique na quantidade para editar.</p></div>
			
			<table cellspacing="0" class="normal" id="tabela_itens_pedido" style="font-size: 11px;">
				<thead>
					<tr>
		
						<th>Código</th>
						<th>Descrição</th>
						<th>U.M.</th>
						<th>Tabela</th>
						<th>Preço Unit.</th>
						<th>Quant.</th>
						<th>Total</th>
						<th>IPI (%)</th>
						<th>Total c/ IPI (R$)</th>
						<th>ST (R$)</th>
						<th>TOTAL GERAL (R$)</th>
						<th>Peso Total</th>
						
						<th>&nbsp;</th>
					</tr>
				</thead>
				
				<tbody>
					
				</tbody>
			</table>
		';
		
		
		
		// -Totais dos Itens
		$conteudo .= '<br />';
		$conteudo .= '<p>Peso total: <strong><span id="peso_total"></span> KG</strong></p>';
		$conteudo .= '<input type="hidden" name="peso_total" value="" id="valor_peso_total" />';
		
		$conteudo .= '<p>Valor do Frete por KG: <strong><span id="frete_kg"></span></strong></p>';
		
		$conteudo .= '<p>Valor do Frete: <strong><span id="valor_frete"></span></strong></p>';
		$conteudo .= '<input type="hidden" name="frete" value="" id="frete" />';
		
		$conteudo .= '<p style="font-size: 16px"><strong>Total do Pedido: <span id="total_pedido"></span></strong></p>';
		$conteudo .= '<input type="hidden" name="valor_total_pedido" value="" id="valor_total_pedido" />';
		
		$conteudo .= '<p style="font-size: 16px"><strong>Total do Pedido + Frete: <span id="total_pedido_frete"></span></strong></p><br /><br />';
		$conteudo .= '<input type="hidden" name="valor_total_pedido_frete" value="" id="valor_total_pedido_frete" />';
		
		
		
		
		$conteudo .= heading('Outras Informações', 3);
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Condição de pagamento:' . br() . form_dropdown('codigo_forma_pagamento', $this->_obter_formas_pagamento(), $this->input->post('codigo_forma_pagamento'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" ' . (!$_POST['codigo_forma_pagamento'] ? 'displayedValue=""' : NULL))) . '</p>';
		
		$conteudo .= '<div id="campo_condicoes_pagamento"></div>';
		
		//$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Desconto total (%):' . br() . form_input('desconto_total', $this->input->post('desconto_total'), 'dojoType="dijit.form.NumberSpinner" constraints="{min: 0, places: 2}" promptMessage="Para decimais use a vírgula. Exemplo: 13,20" id="desconto_total" onChange="calcular_descontos"')) . '</p>';
		if ($pedido->tipo != 'orcamento')
		{
			$conteudo .= '<p style="float: left; margin-right: 10px;">Transportadora:' . br() . form_dropdown('tipo_frete', array('Redespacho' => 'Redespacho', 'CIF' => 'CIF', 'FOB' => 'FOB'), $this->input->post('tipo_frete'), 'dojoType="dojox.form.DropDownSelect" id="tipo_frete" style="width: 70px;" onChange="verificar_tipo_frete"') . ' <span id="codigo_transportadora">' . form_dropdown('codigo_transportadora', $this->_obter_transportadoras(), $this->input->post('codigo_transportadora'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" ' . (!$_POST['codigo_transportadora'] ? 'displayedValue=""' : NULL)) . '</span></p>';
			$conteudo .= '
				<script type="text/javascript">
					$(document).ready(function() {
						verificar_tipo_frete($("select[name=tipo_frete]").val());
					});
					
					function verificar_tipo_frete(tipo_frete)
					{
						if (tipo_frete == "CIF")
						{
							$("#codigo_transportadora").hide();
						}
						else
						{
							$("#codigo_transportadora").show();
						}
					}
				</script>
			';
		}
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Ordem de compra:' . br() . form_input('codigo_ordem_compra', $this->input->post('codigo_ordem_compra'), 'dojoType="dijit.form.TextBox"')) . '</p>';
		//$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Data de entrega prevista:' . br() . form_input('data_entrega', $this->input->post('data_entrega'), 'dojoType="dijit.form.DateTextBox" constraints="{ min: \'' . ($id_pedido ? $this->input->post('data_entrega') : date('Y-m-d')) . '\' }"')) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Observação:' . br() . form_textarea(array('name' => 'observacao_pedido_imediato', 'value' => $this->input->post('observacao_pedido_imediato'), 'style' => 'height: 100px; width: 400px;', 'dojoType' => 'dijit.form.Textarea', 'id' => 'observacao_pedido_imediato'))) . '<div dojoType="dijit.Tooltip" connectId="observacao_pedido_imediato" position="above">O campo se expande automaticamente.</div></p>';
		//$conteudo .= '<p style="float: left; margin-right: 10px; margin-top: 0;">' . form_label('Observação (Ped. Programado):' . br() . form_textarea(array('name' => 'observacao_pedido_programado', 'value' => $this->input->post('observacao_pedido_programado'), 'style' => 'height: 100px; width: 400px;', 'dojoType' => 'dijit.form.Textarea', 'id' => 'observacao_pedido_programado'))) . '<div dojoType="dijit.Tooltip" connectId="observacao_pedido_programado" position="above">O campo se expande automaticamente.</div></p>';
		$conteudo .= '<div style="clear: both;"></div>';
		
		//$conteudo .= heading('Opções', 3);
		$conteudo .= '<div class="ajuda" style="margin-top: 30px; display: none;"><p>Ao clicar no botão Continuar, seu pedido será salvo e você poderá editar ou concluir o pedido.</p></div>
		<p>' . form_submit('continuar', 'Continuar') . ' ou ' . anchor('pedidos', 'Cancelar') . '</p>';
		
		$conteudo .= form_close();
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function calcular_descontos($id_pedido)
	{
		$pedido = $this->db->from('pedidos')->where('id', $id_pedido)->get()->row();
		$itens = $this->db->from('itens_pedidos')->where('id_pedido', $pedido->id)->get()->result();
		
		switch($this->input->post('tipo_desconto'))
		{
			case 'sem_desconto':
				foreach ($itens as $item)
				{
					$this->db->update('itens_pedidos', array(
						'desconto' => 0
					), array('id' => $item->id));
					$this->_calcular_valores_item_pedido($item->id);
				}
			break;
			
			case 'geral':
				foreach ($itens as $item)
				{
					$this->db->update('itens_pedidos', array(
						'desconto' => $this->input->post('desconto_total')
					), array('id' => $item->id));
					$this->_calcular_valores_item_pedido($item->id);
				}
			break;
		}
	}
	
	function obter_data_entrega_prevista_produto($codigo_produto, $quantidade)
	{
		$this->db_cliente->obter_data_entrega_prevista_produto($codigo_produto, $quantidade);
	}
	
	function obter_itens_ajax($id_pedido)
	{
		if (!$id_pedido)
		{
			redirect();
		}
		
		$pedido = $this->db->from('pedidos')->where('id', $id_pedido)->get()->row();
		
		if (!$pedido)
		{
			redirect();
		}
		
		$itens = $this->db->from('itens_pedidos')->where('id_pedido', $pedido->id)->order_by('id ASC')->get()->result();
		
		foreach ($itens as &$item)
		{

			$valor_total += $item->valor_total;
			//$valor_total_ipi += $item->valor_total_ipi;
			$valor_total_desconto += $item->valor_total_desconto;
			//$valor_total_com_ipi += $item->valor_total_com_ipi;
			//$valor_total_com_ipi_e_desconto += $item->valor_total_com_ipi_e_desconto;
			
			//
			
			$data_entrega_prevista = time();
			
			$total = $data_entrega_prevista[0];
			$data_entrega = $data_entrega_prevista[1];
			
			if ($total >= $item->quantidade)
			{
				$_data_entrega = date('d/m/Y', $data_entrega);
			}
			else
			{
				if ($total && $data_entrega)
					$_data_entrega = number_format($total, 0, ',', '.') . ' p/ ' . date('d/m/Y', $data_entrega) . ', ' . number_format(($item->quantidade - $total), 0, ',', '.') . ' sem previsão';
				else
					$_data_entrega = 'sem previsão';
			}
			
			$item->data_entrega = ($item->tipo == 'imediato' || !$item->quantidade) ? '-'  : $_data_entrega;
			
			//
			
			$preco = $item->preco;
			$valor_total_ipi = $item->valor_total_ipi;
			$quantidade = $item->quantidade;
			
			$item->cor = ($item->tipo == 'imediato') ? '#EBFFEB' : '#FFF2F2';
			//$item->tipo = '<img src="' . (base_url() . 'misc/imagens/pedidos/pedido_' . $item->tipo . '.png') . '" style="height: 12px; width: 12px;" />';
			$item->unidade_medida = $item->unidade_medida ? $item->unidade_medida : '-';
			$item->preco = number_format($preco, 5, ',', '.');
			$item->preco_com_ipi = number_format($item->preco_com_ipi, 5, ',', '.');
			$item->quantidade = number_format($item->quantidade, 0, NULL, '.');
			$item->_quantidade = $item->quantidade;
			//$item->valor_total_ipi = number_format($item->valor_total_ipi, 2, ',', '.');
			$item->valor_total_desconto = number_format($item->valor_total_desconto, 5, ',', '.');
			
			//Tabela de precos
			$tabela_precos = $this->db_cliente->obter_tabela_precos($item->codigo_tabela_precos);
			$item->codigo_tabela_precos = $tabela_precos['descricao'];
		
			
			//$item->valor_total_com_ipi_e_desconto = number_format($item->valor_total_com_ipi_e_desconto, 2, ',', '.');

			//Peso
			$total_peso += ($item->quantidade * $item->peso);
			$item->peso = number_format($item->quantidade * $item->peso, 0, ',', '.');

			
			//Itens novos
			$item->valor_preco_quant =  number_format($preco * $quantidade, 5, ',', '.');
			
			
			$item->valor_total_com_ipi =  '<span title="' . $valor_total_ipi . ' + (' . $preco . ' * ' . $quantidade . ')' . '">' . number_format($valor_total_ipi + ($preco * $quantidade), 5, ',', '.') . '</span>';
			
			//$item->valor_total_com_ipi = $item->quantidade . ' * ' . $item->valor_total_ipi . ' + ' .$item->valor_preco_quant;
			$st = $item->st;
			$item->st = number_format((($preco * $quantidade) * $st) / 100, 5, ',', '.');
			$total_st += (($preco * $quantidade) * $st) / 100;
			
			
			//Desconto
			$total_desconto += (($preco * $quantidade) * $item->desconto) / 100;
			//$total_desconto_ .= ($preco * $quantidade) . ' * ' . $item->desconto . " / 100 - ";
			
			$desconto = (($preco * $quantidade) * $item->desconto) / 100;
			$item->desconto =  number_format((($preco * $quantidade) * $item->desconto) / 100, 5, ',', '.');
			
			
			$item->total_geral = number_format((($valor_total_ipi + ($preco * $quantidade)) + ((($preco * $quantidade) * $st) / 100)) - $desconto , 5, ',', '.');

			$valor_total_com_ipi_e_desconto += (($valor_total_ipi + ($preco * $quantidade)) + ((($preco * $quantidade) * $st) / 100)) - $desconto;
	
			
			
		}

		
		echo json_encode(array('itens' => $itens, 'totais' => array(
			'valor_total' => number_format($valor_total, 5, ',', '.'),
			'valor_total_ipi' => number_format($valor_total_ipi, 5, ',', '.'),
			'valor_total_desconto' => number_format($valor_total_desconto, 5, ',', '.'),
			'valor_total_com_ipi' => number_format($valor_total_com_ipi, 5, ',', '.'),
			'valor_total_com_ipi_e_desconto' => number_format($valor_total_com_ipi_e_desconto, 5, ',', '.'),
			'valor_total_com_ipi_e_desconto_number' => $valor_total_com_ipi_e_desconto,
			'total_st' => number_format($total_st, 5, ',', '.'),
			'peso_total' => number_format($total_peso, 0, ',', '.'),
			'valor_peso_total' => $total_peso,
			'total_desconto' => number_format($total_desconto, 5, ',', '.')
		)));
	}
	
	function adicionar_item_ajax($id_pedido = NULL)
	{
		if (!$id_pedido)
		{
			redirect();
		}
		
		$pedido = $this->db->from('pedidos')->where('id', $id_pedido)->get()->row();
		
		if (!$pedido)
		{
			redirect();
		}
		
		// TODO: verificar se o item pode ser adicionado
		
		$dados = array(
			'erro' => NULL,
			'mensagem' => NULL
		);
		
		if (!$this->input->post('codigo_produto'))
		{
			$dados['erro'] = 'Selecione um produto.';
		}
		else if (!$this->input->post('codigo_tabela_precos') && !$this->input->post('preco'))
		{
			$dados['erro'] = 'Selecione uma tabela de preços ou digite um preço.';
		}
		
		//else if (/*$this->input->post('preco') &&*/ (!is_numeric($this->input->post('preco')) || $this->input->post('preco') <= 0))
		//{
			//$dados['erro'] = 'Digite um preço válido.';
		//}
		
		else if (!is_numeric($this->input->post('quantidade')))
		{
			$dados['erro'] = 'Digite uma quantidade válida.';
		}
		else
		{
			$itens = $this->db->from('itens_pedidos')->where('id_pedido', $pedido->id)->get()->result();
			
			// se houver um opcional
			if ($itens && $this->db_cliente->obter_opcional_produto($this->input->post('codigo_produto'))) {
				$produto = $this->db_cliente->obter_opcional_produto($this->input->post('codigo_produto'));
			} else {
				$produto = $this->db_cliente->obter_produto($this->input->post('codigo_produto'), $this->input->post('codigo_tabela_precos'));
			}
			
			if (!$produto)
			{
				$dados['erro'] = 'Produto não encontrado.';
			}
			else
			{
				$item_imediato_pedido = $this->db->from('itens_pedidos')->where(array('id_pedido' => $pedido->id, 'tipo' => 'imediato', 'codigo' => $produto['codigo']))->get()->row();
				$item_programado_pedido = $this->db->from('itens_pedidos')->where(array('id_pedido' => $pedido->id, 'tipo' => 'programado', 'codigo' => $produto['codigo']))->get()->row();
				
				if ($item_imediato_pedido || $item_programado_pedido)
				{
					$quantidade = ($item_imediato_pedido->quantidade + $item_programado_pedido->quantidade) + $this->input->post('quantidade');
					
					/*
					$quantidade_disponivel_estoque = $this->obter_quantidade_disponivel_estoque_produto($produto['codigo']);
					
					// somar a quantidade do item imediato na quantidade disponível no estoque
					// pois vamos recalcular posteriormente
					$quantidade_disponivel_estoque += $item_imediato_pedido->quantidade;
					
					if ($quantidade <= $quantidade_disponivel_estoque)
					{
						$quantidade_imediata = $quantidade;
						$quantidade_programada = 0;
					}
					else
					{
						$quantidade_imediata = $quantidade_disponivel_estoque;
						$quantidade_programada = $quantidade - $quantidade_disponivel_estoque;
					}
					*/
					
					$quantidade_imediata = $quantidade;
					
					$this->db->update('itens_pedidos', array(
						'quantidade' => $quantidade_imediata,
						'desconto' => $this->input->post('desconto')
					), array('id' => $item_imediato_pedido->id));
					$this->_calcular_valores_item_pedido($item_imediato_pedido->id);
					
					/*
					$this->db->update('itens_pedidos', array(
						'quantidade' => $quantidade_programada,
						'desconto' => $this->input->post('desconto')
					), array('id' => $item_programado_pedido->id));
					$this->_calcular_valores_item_pedido($item_programado_pedido->id);
					*/
					
					$dados['mensagem'] = 'Este produto já foi adicionado. As quantidades foram somadas e o desconto atualizado.';
				}
				else
				{
				
					$quantidade_imediata = $this->input->post('quantidade');
					
					/*
					$quantidade = $this->input->post('quantidade');
					$quantidade_disponivel_estoque = $this->obter_quantidade_disponivel_estoque_produto($produto['codigo']);
					
					if ($quantidade <= $quantidade_disponivel_estoque)
					{
						$quantidade_imediata = $quantidade;
						$quantidade_programada = 0;
					}
					else
					{
						$quantidade_imediata = $quantidade_disponivel_estoque;
						$quantidade_programada = $quantidade - $quantidade_disponivel_estoque;
					}
					
					// vamor inserir primeiro a quantidade programada
					// dessa forma os itens imediatos aparecem primeiro na hora de exibir os itens
					$this->db->insert('itens_pedidos', array(
						'timestamp' => time(),
						'tipo' => 'programado',
						'id_pedido' => $pedido->id,
						'codigo' => $produto['codigo'],
						'codigo_real' => $produto['codigo_real'],
						'descricao' => $produto['descricao'],
						'unidade_medida' => $produto['unidade_medida'],
						'preco' => ($this->input->post('usar_tabela_precos') == 'nao') ? $this->input->post('preco') : $produto['preco'],
						'codigo_tabela_precos' => ($this->input->post('usar_tabela_precos') == 'sim') ? $this->input->post('codigo_tabela_precos') : NULL,
						'ipi' => $produto['ipi'],
						'desconto' => $this->input->post('desconto'),
						'quantidade' => $quantidade_programada
					));
					$this->_calcular_valores_item_pedido($this->db->insert_id());
					*/
					
					$this->db->insert('itens_pedidos', array(
						'timestamp' => time(),
						'tipo' => 'imediato',
						'id_pedido' => $pedido->id,
						'codigo' => $produto['codigo'],
						'codigo_real' => $produto['codigo_real'],
						'descricao' => $produto['descricao'],
						'unidade_medida' => $produto['unidade_medida'],
						'preco' => ($this->input->post('usar_tabela_precos') == 'nao') ? $this->input->post('preco') : $produto['preco'],
						'codigo_tabela_precos' => ($this->input->post('codigo_tabela_precos')) ? $this->input->post('codigo_tabela_precos') : NULL,
						'ipi' => $produto['ipi'],
						'desconto' => $this->input->post('desconto'),
						'quantidade' => $quantidade_imediata,
						'peso' => $produto['peso']
					));
					$this->_calcular_valores_item_pedido($this->db->insert_id());
				}
			}
		}
		
		echo json_encode($dados);
	}
	
	// todo: remover var_dump
	function obter_quantidade_disponivel_estoque_produto($codigo_produto)
	{
		$quantidade_disponivel_estoque = $this->db_cliente->obter_quantidade_disponivel_estoque_produto($codigo_produto);
		
		$produto = $this->db->select('SUM(itens_pedidos.quantidade) AS quantidade')->from('itens_pedidos')->join('pedidos', 'pedidos.id = itens_pedidos.id_pedido')->where('itens_pedidos.codigo = "' . $codigo_produto . '" AND itens_pedidos.tipo = "imediato" AND (pedidos.status IS NULL OR pedidos.status IN ("aguardando_analise_comercial", "aguardando_faturamento", "parcialmente_faturado"))')->get()->row();
		$quantidade_disponivel_estoque -= $produto->quantidade;
		
		if ($quantidade_disponivel_estoque < 0)
		{
			$quantidade_disponivel_estoque = 0;
		}
		
		return $quantidade_disponivel_estoque;
	}
	
	function _calcular_valores_item_pedido($id_item_pedido = NULL)
	{
		$item_pedido = $this->db->from('itens_pedidos')->where('id', $id_item_pedido)->get()->row();
		
		$preco = $item_pedido->preco;
		$quantidade = $item_pedido->quantidade;
		
		// ipi
		$valor_ipi = $preco * ($item_pedido->ipi / 100);
		$valor_total_ipi = $valor_ipi * $quantidade;
		
		// desconto
		$valor_desconto = $preco * ($item_pedido->desconto / 100);
		$valor_total_desconto = $valor_desconto * $quantidade;
		
		// preço
		$preco_com_ipi = $preco + $valor_ipi;
		$preco_com_ipi_e_desconto = $preco_com_ipi - $valor_desconto;
		
		// total
		$valor_total = $preco * $quantidade;
		$valor_total_com_ipi = $preco_com_ipi * $quantidade;
		$valor_total_com_ipi_e_desconto = $preco_com_ipi_e_desconto * $quantidade;
		
		$this->db->update('itens_pedidos', array(
			'preco' => $preco,
			'preco_com_ipi' => $preco_com_ipi,
			'preco_com_ipi_e_desconto' => $preco_com_ipi_e_desconto,
			'valor_ipi' => $valor_ipi,
			'valor_desconto' => $valor_desconto,
			'valor_total' => $valor_total,
			'valor_total_ipi' => $valor_total_ipi,
			'valor_total_desconto' => $valor_total_desconto,
			'valor_total_com_ipi' => $valor_total_com_ipi,
			'valor_total_com_ipi_e_desconto' => $valor_total_com_ipi_e_desconto,
		), array('id' => $item_pedido->id));
	}
	
	function editar_item_ajax($id_pedido = NULL)
	{
		if (!$id_pedido)
		{
			redirect();
		}
		
		$pedido = $this->db->from('pedidos')->where('id', $id_pedido)->get()->row();
		
		if (!$pedido)
		{
			redirect();
		}
		
		// TODO: verificar se o item pode ser editado
		
		$dados = array(
			'erro' => NULL,
			'mensagem' => NULL
		);
		
		if ($this->input->post('preco') && !is_numeric($this->input->post('preco')))
		{
			$dados['erro'] = 'Digite um preço válido.';
		}
		else if ($this->input->post('quantidade') && !is_numeric($this->input->post('quantidade')))
		{
			$dados['erro'] = 'Digite uma quantidade válida.';
		}
		else
		{
			$item_pedido = $this->db->from('itens_pedidos')->where(array('id' => $this->input->post('id')))->get()->row();
			
			$item_imediato_pedido = $this->db->from('itens_pedidos')->where(array('id_pedido' => $pedido->id, 'tipo' => 'imediato', 'codigo' => $item_pedido->codigo))->get()->row();
			$item_programado_pedido = $this->db->from('itens_pedidos')->where(array('id_pedido' => $pedido->id, 'tipo' => 'programado', 'codigo' => $item_pedido->codigo))->get()->row();
			
			if ($item_pedido->tipo == 'programado')
			{
				$quantidade_programada = $this->input->post('quantidade');
				
				$this->db->update('itens_pedidos', array(
					'preco' => $this->input->post('preco') ? $this->input->post('preco') : $item_programado_pedido->preco,
					'codigo_tabela_precos' => $this->input->post('preco') ? NULL : $item_programado_pedido->codigo_tabela_precos,
					'quantidade' => $this->input->post('quantidade') ? $quantidade_programada :  $item_programado_pedido->quantidade
				), array('id' => $item_programado_pedido->id));
				$this->_calcular_valores_item_pedido($item_programado_pedido->id);
			}
			else if ($item_pedido->tipo == 'imediato')
			{
				$quantidade = $this->input->post('quantidade');
				$quantidade_disponivel_estoque = $this->obter_quantidade_disponivel_estoque_produto($produto['codigo']);
				
				// somar a quantidade do item imediato na quantidade disponível no estoque
				// pois vamos recalcular posteriormente
				$quantidade_disponivel_estoque += $item_imediato_pedido->quantidade;
				
				if ($quantidade <= $quantidade_disponivel_estoque)
				{
					$quantidade_imediata = $quantidade;
					$quantidade_programada = $item_programado_pedido->quantidade;
				}
				else
				{
					$quantidade_imediata = $quantidade_disponivel_estoque;
					$quantidade_programada = ($quantidade - $quantidade_disponivel_estoque) + $item_programado_pedido->quantidade;
				}
				
				$this->db->update('itens_pedidos', array(
					'preco' => $this->input->post('preco') ? $this->input->post('preco') : $item_imediato_pedido->preco,
					'codigo_tabela_precos' => $this->input->post('preco') ? NULL : $item_imediato_pedido->codigo_tabela_precos,
					'quantidade' => $this->input->post('quantidade') ? $quantidade_imediata : $item_imediato_pedido->quantidade
				), array('id' => $item_imediato_pedido->id));
				$this->_calcular_valores_item_pedido($item_imediato_pedido->id);
				
				$this->db->update('itens_pedidos', array(
					'preco' => $this->input->post('preco') ? $this->input->post('preco') : $item_programado_pedido->preco,
					'codigo_tabela_precos' => $this->input->post('preco') ? NULL : $item_programado_pedido->codigo_tabela_precos,
					'quantidade' => $this->input->post('quantidade') ? $quantidade_programada :  $item_programado_pedido->quantidade
				), array('id' => $item_programado_pedido->id));
				$this->_calcular_valores_item_pedido($item_programado_pedido->id);
			}
		}
		
		echo json_encode($dados);
	}
	
	function transferir_quantidade_item_ajax($id_pedido = NULL)
	{
		if (!$id_pedido)
		{
			redirect();
		}
		
		$pedido = $this->db->from('pedidos')->where('id', $id_pedido)->get()->row();
		
		if (!$pedido)
		{
			redirect();
		}
		
		// TODO: verificar se o item pode ser editado
		
		$dados = array(
			'erro' => NULL,
			'mensagem' => NULL
		);
		
		if ($this->input->post('quantidade') && !is_numeric($this->input->post('quantidade')))
		{
			$dados['erro'] = 'Digite uma quantidade válida.';
		}
		else
		{
			$item_pedido = $this->db->from('itens_pedidos')->where(array('id' => $this->input->post('id')))->get()->row();
			
			$item_imediato_pedido = $this->db->from('itens_pedidos')->where(array('id_pedido' => $pedido->id, 'tipo' => 'imediato', 'codigo' => $item_pedido->codigo))->get()->row();
			$item_programado_pedido = $this->db->from('itens_pedidos')->where(array('id_pedido' => $pedido->id, 'tipo' => 'programado', 'codigo' => $item_pedido->codigo))->get()->row();
			
			// transferir quantidade imediata para quantidade programada
			if ($item_pedido->tipo == 'imediato')
			{
				if ($item_pedido->quantidade == 0)
				{
					$dados['erro'] = 'Não há quantidade disponível para transferência.';
				}
				else if ($this->input->post('quantidade') > $item_pedido->quantidade)
				{
					$dados['erro'] = 'A quantidade disponível para transferência neste item é ' . number_format($item_pedido->quantidade, 0, ',', '.') . '.';
				}
				else
				{
					$this->db->update('itens_pedidos', array(
						'quantidade' => $item_imediato_pedido->quantidade - $this->input->post('quantidade')
					), array('id' => $item_imediato_pedido->id));
					$this->_calcular_valores_item_pedido($item_imediato_pedido->id);
					
					$this->db->update('itens_pedidos', array(
						'quantidade' => $item_programado_pedido->quantidade + $this->input->post('quantidade')
					), array('id' => $item_programado_pedido->id));
					$this->_calcular_valores_item_pedido($item_programado_pedido->id);
				}
			}
			
			// transferir quantidade programada para quantidade imediata
			else
			{
				$quantidade_disponivel_estoque = $this->obter_quantidade_disponivel_estoque_produto($item_pedido->codigo);
				
				if ($item_pedido->quantidade == 0)
				{
					$dados['erro'] = 'Não há quantidade disponível para transferência.';
				}
				else if ($quantidade_disponivel_estoque == 0)
				{
					$dados['erro'] = 'Não há quantidade disponível no estoque para transferência.';
				}
				else if ($this->input->post('quantidade') > $item_pedido->quantidade)
				{
					$dados['erro'] = 'A quantidade disponível para transferência neste item é ' . number_format($item_pedido->quantidade, 0, ',', '.') . '.';
				}
				else if ($this->input->post('quantidade') > $quantidade_disponivel_estoque)
				{
					$dados['erro'] = 'A quantidade disponível no estoque é ' . number_format($quantidade_disponivel_estoque, 0, ',', '.') . '.';
				}
				else
				{
					$this->db->update('itens_pedidos', array(
						'quantidade' => $item_imediato_pedido->quantidade + $this->input->post('quantidade')
					), array('id' => $item_imediato_pedido->id));
					$this->_calcular_valores_item_pedido($item_imediato_pedido->id);
					
					$this->db->update('itens_pedidos', array(
						'quantidade' => $item_programado_pedido->quantidade - $this->input->post('quantidade')
					), array('id' => $item_programado_pedido->id));
					$this->_calcular_valores_item_pedido($item_programado_pedido->id);
				}
			}
		}
		
		echo json_encode($dados);
	}
	
	function excluir_item_ajax($id_pedido = NULL)
	{
		if (!$id_pedido)
		{
			redirect();
		}
		
		$pedido = $this->db->from('pedidos')->where('id', $id_pedido)->get()->row();
		
		if (!$pedido)
		{
			redirect();
		}
		
		// TODO: verificar se o item pode ser excluído
		
		/*$item_pedido = $this->db->from('itens_pedidos')->where('id', $this->input->post('id'))->get()->row();
		
		$this->db->delete('itens_pedidos', array('id_pedido' => $id_pedido, 'codigo' => $item_pedido->codigo));*/
		
		$this->db->delete('itens_pedidos', array('id' => $this->input->post('id')));
	}
	
	// criada para redirecionar o usuário do "cancelamento" da transformação de orçamento em pedido para a conversão do prospect em cliente
	function converter_prospect($id_prospect, $id_orcamento)
	{
		$this->session->set_userdata('id_orcamento_aguardando_transformacao_pedido', $id_orcamento);
		
		redirect('prospects/ver_detalhes/' . $id_prospect);
	}
	
	
	function ver_detalhes_pdr($id_pedido = NULL, $enviar_email = FALSE)
	{
		
	
		if($id_pedido)
		{
			
			$pedido = $this->db_cliente->obter_pedidos_pdr($id_pedido, FALSE);
			$pedidos = $this->db_cliente->obter_pedidos_pdr($id_pedido, TRUE);
			
		}
		
		
		
		//Pedido
		if($pedido['tipo'] == 'P')
		{
			//Botoes de Copiar e editar
			$conteudo .= '<p style="margin-bottom: 10px;">';
			
			$conteudo .= anchor('criar_pedidos/informacoes_cliente/pedido/' . $pedido['codigo_representante']. '/' . $pedido['id_pedido'] . '/' . TRUE, 'Copiar Pedido', 'class="botao_a copiar_pedido"');
			
			if ($this->session->userdata('grupo_usuario') == 'gestores_comerciais' || $this->session->userdata('grupo_usuario') == 'supervisores')
			{
			
			
				//$conteudo .= anchor('criar_pedidos/informacoes_cliente/pedido/' . $pedido['codigo_representante']. '/' . $pedido['id_pedido'], 'Editar Pedido', 'class="botao_b"');
				
				$conteudo .= anchor('pedidos/aprovar_pedido/' . $pedido['id_pedido'], 'Aprovar Pedido', 'class="botao_d" onclick="return confirm(\'Tem certeza que deseja aprovar esse pedido?\')" ');
				
				if($pedido['status'] != 'R')
				{
					$conteudo .= anchor('pedidos/reprovar_pedido/' . $pedido['id_pedido'], 'Reprovar Pedido', 'class="botao_c"');	
				}
				
				
				
			}
			
			$conteudo .= '</p>';
			$conteudo .= '<div style="clear:both"></div>';
			//-----------
		}
		else if($pedido['tipo'] == 'O')
		{
			// Orçmento
		
			//Botoes de Copiar e editar
			$conteudo .= '<p style="margin-bottom: 10px;">';
			$conteudo .= anchor('criar_pedidos/informacoes_cliente/orcamento/' . $pedido['codigo_representante']. '/' . $pedido['id_pedido'], 'Editar Orçamento', 'class="botao_b"');
			$conteudo .= anchor('pedidos/transformar_pedido/' . $pedido['id_pedido'], 'Transformar em Pedido', 'class="botao_a" onclick="return confirm(\'Tem certeza que deseja transformar esse orçamento em pedido?\')"');
			$conteudo .= '</p>';
			$conteudo .= '<div style="clear:both"></div>';
			//-----------
		}
			
		
		
		
		$conteudo .= '
					<div class="caixa_icones nao_exibir_impressao">
						<ul>
							<li style="background: url(' . base_url() . 'misc/icones/printer1.png) no-repeat;">' . anchor('pedidos/imprimir', 'Imprimir') . '</li>
							<li style="background: url(' . base_url() . 'misc/icones/mail_replay.png) no-repeat;">' . anchor('#', 'Enviar Por E-mail', 'class="enviar_por_email"') . '</li>
						</ul>
					</div>
				';
				
		
		$conteudo .= '
				<script type="text/javascript">
					$(document).ready(function() {
	
						
						$(".enviar_por_email").click(function() {
							$.fn.colorbox({ html: "<h3 style=\"margin: 0;\">Enviar ' . ($pedido['tipo'] == 'orcamento' ? 'Orçamento' : 'Pedido') . ' por E-mail</h3><p>Digite o e-mail:<br /><input name=\"email\" type=\"text\" size=\"40\" /></p><p>Cópia 1:<br /><input name=\"email_2\" type=\"text\" size=\"40\" /></p><p>Cópia 2:<br /><input name=\"email_3\" type=\"text\" size=\"40\" /></p><p><input type=\"submit\" value=\"Enviar\" class=\"enviar_email\" /> <input type=\"submit\" value=\"Cancelar\" onclick=\"$.fn.colorbox.close();\" /></p>" });
							
							return false;
						});

						
						$(".enviar_email").live("click", function() {
							var _this = this;
							var email = $("input[name=email]").val();
							var email_2 = $("input[name=email_2]").val();
							var email_3 = $("input[name=email_3]").val();
							
							$(this).attr("disabled", "disabled").val("Carregando...");
							
							var emails = email;
							
							if (email_2)
							{
								emails += ", " + email_2;
							}
							
							if (email_3)
							{
								emails += ", " + email_3;
							}
							
							$.post("' . site_url('pedidos/enviar_email/' . $pedido['id_pedido']) . '", { emails: emails }, function(dados) {
								if (dados.erro)
								{
									alert(dados.erro);
								}
								else
								{
									alert("Sucesso!");
									
									$.fn.colorbox.close();
								}
								
								$(_this).removeAttr("disabled").val("Enviar");
							}, "json");
						});
					});
				</script>
			';
		
		
		$conteudo .= '<h2>Espelho do Pedido</h2>';
		
		$conteudo .= '<br /><br />';
		
		$conteudo .= '<h4>Informações Gerais</h4>';
		
		//Informações Gerais
		$forma_pagamento = $this->db_cliente->obter_forma_pagamento($pedido ? $pedido['condicao_pagamento'] : $pedido['condicao_pagamento']);
		
		$conteudo .= '
			<table cellspacing="0" class="info">
				<tr>
					<th>O.C.:</th>
					<td>' . $pedido['ordem_compra'] . '</td>
				</tr>
				
				<tr>
					<th>Número:</th>
					<td>' . $pedido['id_pedido'] . '</td>
				</tr>
				
			' . (!$obter_html ? '</table>
			
			<table cellspacing="0" class="info">' : NULL) . '
				<tr>
					<th>Tipo de frete:</th>
					<td>' . $pedido['tipo_frete'] . '</td>
				</tr>
				
				<tr>
					<th>Emissão:</th>
					<td>' . ($pedido['data_emissao'] ? date('d/m/Y', strtotime($pedido['data_emissao'])) : 'não há') . '</td>
				</tr>
				
			' . (!$obter_html ? '</table>
			
			<table cellspacing="0" class="info">' : NULL) . '
				
				<tr>
					<th>Forma pgto.:</th>
					<td>' . $forma_pagamento['descricao'] . '</td>
				</tr>
				
				<tr>
					<th>Data ent. prevista:</th>
					<td>' . ($pedido['data_entrega'] ? date('d/m/Y', strtotime($pedido['data_entrega'])) : 'N/D') . '</td>
				</tr>
				
			' . (!$obter_html ? '</table>
			
			
			
			<table cellspacing="0" class="info">' : NULL) . '
				
				' . ($pedido['motivo_reprovacao'] ? '<tr>
					<th>Motivo da reprovação:</th>
					<td>' . $pedido['motivo_reprovacao'] . '</td>
				</tr>' : NULL) . '
			</table>
			
			<div style="clear: left;"></div>
		';
		
		
		if($pedido['codigo_cliente'] && $pedido['loja_cliente'])
		{
			//Informações deo Cliente
			$conteudo .= '<br /><br />';
			$conteudo .= '<h4>Informações do Cliente</h4>';
			$conteudo .= '<br />';
			
			$cliente = $this->db_cliente->obter_cliente($pedido['codigo_cliente'], $pedido['loja_cliente']);
			
			$conteudo .= '
					<table cellspacing="0" class="info">
						<tr>
							<th>Nome:</th>
							<td>' . $cliente['nome'] . '</td>
						</tr>
						<tr>
							<th>CPF/CNPJ:</th>
							<td>' . $cliente['cpf'] . '</td>
						</tr>
						<tr>
							<th>Telefone:</th>
							<td>' . $cliente['telefone'] . '</td>
						</tr>
					' . (!$obter_html ? '</table>
				
				<table cellspacing="0" class="info">' : NULL) . '
						<tr>
							<th>Código:</th>
							<td>' . $cliente['codigo'] . '</td>
						</tr>
						<tr>
							<th>Loja:</th>
							<td>' . $cliente['loja'] . '</td>
						</tr>
						<tr>
							<th>E-mail:</th>
							<td>' . $cliente['email'] . '</td>
						</tr>
					' . (!$obter_html ? '</table>
				
				<table cellspacing="0" class="info">' : NULL) . '
						<tr>
							<th>Endereço:</th>
							<td>' . $cliente['endereco'] . '</td>
						</tr>
						<tr>
							<th>Bairro:</th>
							<td>' . $cliente['bairro'] . '</td>
						</tr>
						<tr>
							<th>CEP:</th>
							<td>' . $cliente['cep'] . '</td>
						</tr>
					' . (!$obter_html ? '</table>
				
				<table cellspacing="0" class="info">' : NULL) . '
						<tr>
							<th>Cidade:</th>
							<td>' . $cliente['cidade'] . '</td>
						</tr>
						<tr>
							<th>Estado:</th>
							<td>' . $cliente['estado'] . '</td>
						</tr>
						<tr>
							<th>Contato:</th>
							<td>' . $cliente['pessoa_contato'] . '</td>
						</tr>
					</table>
					
					<div style="clear: both;"></div>
				';
		}
		else
		{
			//Informações do Prospect
			$conteudo .= '<br /><br />';
			$conteudo .= '<h4>Informações do Prospect</h4>';
			$conteudo .= '<br />';
			
			$prospect = $this->db->from('prospects')->where('id', $pedido['id_prospects'])->get()->row();
	
			
			$conteudo .= '
					<table cellspacing="0" class="info">
						<tr>
							<th>Nome:</th>
							<td>' . $prospect->nome . '</td>
						</tr>
						<tr>
							<th>CPF/CNPJ:</th>
							<td>' . $prospect->cpf . '</td>
						</tr>
						<tr>
							<th>Telefone:</th>
							<td>' . $prospect->telefone_contato_1 . '</td>
						</tr>
					' . (!$obter_html ? '</table>
				
				<table cellspacing="0" class="info">' : NULL) . '
						<tr>
							<th>Código:</th>
							<td>' . $prospect->id. '</td>
						</tr>
						
						<tr>
							<th>Cidade/Estado:</th>
							<td>' . $prospect->cidade . '/' . $prospect->estado . '</td>
						</tr>
	
						<tr>
							<th>E-mail:</th>
							<td>' . $prospect->email_contato_1 . '</td>
						</tr>
					' . (!$obter_html ? '</table>
				
				<table cellspacing="0" class="info">' : NULL) . '
						<tr>
							<th>Endereço:</th>
							<td>' . $prospect->endereco . '</td>
						</tr>
						<tr>
							<th>Bairro:</th>
							<td>' . $prospect->bairro . '</td>
						</tr>
						<tr>
							<th>CEP:</th>
							<td>' . $prospect->cep . '</td>
						</tr>
					' . (!$obter_html ? '</table>
				
				<table cellspacing="0" class="info">' : NULL) . '

					</table>
					
					<div style="clear: both;"></div>
				';
		}
			
		//Representantes
		$representante = $this->db_cliente->obter_representante($pedido['codigo_representante']);
		
		$conteudo .= heading('Informações do Representante', 3);
		
		$conteudo .= '
			<table cellspacing="0" class="info">
				<tr>
					<th>Nome:</th>
					<td>' . $representante['nome'] . '</td>
				</tr>
				<tr>
					<th>Código:</th>
					<td>' . $representante['codigo'] . '</td>
				</tr>
				<tr>
					<th>CPF/CNPJ:</th>
					<td>' . $representante['cpf'] . '</td>
				</tr>
			' . (!$obter_html ? '</table>
			
			<table cellspacing="0" class="info">' : NULL) . '
				<tr>
					<th>Telefone:</th>
					<td>' . $representante['telefone'] . '</td>
				</tr>
				<tr>
					<th>E-mail:</th>
					<td>' . $representante['email'] . '</td>
				</tr>
				<tr>
					<th>Endereço:</th>
					<td>' . $representante['endereco'] . '</td>
				</tr>
			' . (!$obter_html ? '</table>
			
			<table cellspacing="0" class="info">' : NULL) . '
				<tr>
					<th>Bairro:</th>
					<td>' . $representante['bairro'] . '</td>
				</tr>
				<tr>
					<th>CEP:</th>
					<td>' . $representante['cep'] . '</td>
				</tr>
				<tr>
					<th>Cidade/Estado:</th>
					<td>' . $representante['cidade'] . '/' . $representante['estado'] . '</td>
				</tr>
			' . (!$obter_html ? '</table>
			
			<table cellspacing="0" class="info">' : NULL) . '
		
			</table>
			
			<div style="clear: both;"></div>
		';
		
		
		//Produtos
		$conteudo .= '<br /><br />';
		$conteudo .= '<h4>Produtos</h4>';
		$conteudo .= '<br />';

		
		//---------------------------------
		
		
		if($pedidos)
		{
			
			$conteudo .= '<table class="novo_grid" style="margin-top: 20px">
							<thead>
									<th>Item</th>
									<th>Código</th>
									<th>Descrição</th>
									<th>U.M.</th>
									<th>Tabela</th>
									<th>Preço Unit. (R$)</th>
									<th>Quant.</th>
									<th>Total (R$)</th>
									<th>IPI (%)</th>
									<th>Total c/ IPI (R$)</th>
									<th>ST (R$)</th>
									<th>Peso Total (KG)</th>
							</thead>
							
							<tbody>';
			
			$item = count($pedidos);
			foreach($pedidos as $produto)
			{
				

				$conteudo .= '<tr>
					<td class="center">' . str_pad($item--, 2, 0, STR_PAD_LEFT) . '</td>
					<td class="center">' . $produto['codigo_produto'] . '</td>
					<td>' . $produto['descricao_produto'] . '</td>
					<td>' . $produto['unidade_medida'] . '</td>
					<td>' . $produto['tabela_precos'] . '</td>
					<td class="right">' . number_format($produto['preco_unitario'], 5, ',', '.') . '</td>
					
					<td class="center">' . $produto['quantidade'] . '</td>
					
					<td class="right">' . number_format($produto['preco_unitario'] * $produto['quantidade'], 5, ',', '.')  . '</td>
					<td class="right">' . $produto['ipi'] . '</td>
					<td class="right">' . number_format($produto['ipi'] + ($produto['preco_unitario'] * $produto['quantidade']),5, ',', '.') . '</td>
					<td class="right">' . number_format(0, 5, ',', '.') . '</td>
					<td class="right">' . number_format($produto['peso_unitario'] * $produto['quantidade'], 0, '', '.') . '</td>
				</tr>';

			}
			
			
			$conteudo .= '</tbody>
			</table>';
		}
		
		//----------------------
		

		
		
		
		
		//Totais
		foreach ($pedidos as $item_pedido)
		{
			$total_quantidade += $item_pedido['quantidade'];
			$total_peso += $item_pedido['peso_unitario'] * $item_pedido['quantidade'];
			$total_valor += $item_pedido['preco_unitario'] * $item_pedido['quantidade'];
			$frete = $item_pedido['valor_frete'];
		}
		
		
		$conteudo .= heading('Totais', 3);
		$conteudo .= '
			<table cellspacing="0" class="info">
				<tr>
					<th>Quant. vend.:</th>
					<td class="right">' . number_format($total_quantidade, 0, NULL, '.') . '</td>
				</tr>
			
				<tr>
					<th>Peso total (KG):</th>
					<td class="right">' . number_format($total_peso, 0, NULL, '.') . '</td>
				</tr>

			
				<tr>
					<th>Valor do Frete (R$):</th>
					<td class="right">' . number_format($frete, 5, ',', '.')  . '</td>
				</tr>
				
				<!--
				<tr>
					<th>Valor do Frete por KG (R$):</th>
					<td class="right">  /*number_format($frete / $total_peso, 2, ",", ".")*/   </td>
				</tr>-->
				
				<!--
				<tr>
					<th>Valor dos produtos (R$):</th>
					<td class="right"> /* number_format($total_valor , 2, ",", ".") */ </td>
				</tr>-->
				
				<tr>
					<th>Valor do pedido (R$):</th>
					<td class="right">' . number_format($frete + $total_valor, 5, ',', '.')  . '</td>
				</tr>
				
			</table>
			
			<div style="clear: both;"></div>
		';
		
		##Impressao Pedido Quantidade Vendida
    $impressao['pedido_quantidade_vendida'] = number_format($total_quantidade, 0, NULL, '.') ;
    
    ##Impressao Pedido Quantidade Faturada
    $impressao['pedido_quantidade_faturada'] = number_format($pedido['quantidade_faturada'], 0, NULL, '.');
    
    ##Impressao Pedido Peso Total
    $impressao['pedido_peso_total'] = number_format($total_peso, 0, NULL, '.');
    
    ##Impressao Pedido Valor Frete KG
    $impressao['pedido_valor_frete_kg'] = number_format($valor_frete_kg, 5, ',', '.');
    
    ##Impressao Pedido Valor Frete
    $impressao['pedido_valor_frete'] = number_format($frete, 5, ',', '.');
    
    ##Impressao Pedido Valor Total
    $impressao['pedido_valor_total'] = number_format($frete + $total_valor, 5, ',', '.');
    
    ##Impressao Pedido Valor Parcial
    $impressao['pedido_valor_parcial'] =  number_format($total_valor , 5, ',', '.');
    
    ##Impressao Pedido Valor Total do Pedido Mais o Frete
    $impressao['pedido_total_mais_frete'] = number_format($frete + $total_valor, 5, ',', '.');

	##Impressao Pedido Observacao
    $pedido['obs_pedido'] ? $impressao['observacao'] = $pedido['obs_pedido'] : $impressao['observacao'] = 'N/D';
		
		if($enviar_email)
		{
			return $conteudo;
		}
		
		$this->load->view('layout', array('conteudo' => $conteudo));
		
		
					
				
				
		
	##Impressao Tipo de Espelho
	$pedido['tipo'] == 'orcamento' ? $impressao['tipo'] = 'Orçamento' : $impressao['tipo'] = 'Pedido';
	
	##Impressao Preço Médio/Kg
    $impressao['preco_medio'] = ($total_valor != 0 ? number_format($total_peso / $total_valor, 5, ',', '.') : number_format($total_peso, 5, ',', '.'));

	##Impressao Status
    $impressao['status'] = element($pedido['status'], $this->_obter_status());

	##Impressao Ordem de Compra
    $impressao['ordem_compra'] = $pedido['ordem_compra']; 			

	##Impressao Código
    $impressao['codigo'] = $pedido['id_pedido'];
				

	##Impressao Tipo Frete
    $impressao['tipo_frete'] = $pedido['tipo_frete'];
 
	##Impressao Emissao
    $pedido['data_emissao'] ? $impressao['emissao'] = date('d/m/Y', strtotime($pedido['data_emissao'])) : $impressao['emissao'] = 'não há';

	##Impressao Forma de Pagamento
    $impressao['forma_pagamento'] = $forma_pagamento['descricao'];				

	##Impressao Data Prevista para Entrega
    $_pedido->data_entrega_timestamp ? $impressao['data_entrega'] =  date('d/m/Y', $_pedido->data_entrega_timestamp) : $impressao['data_entrega'] = 'N/D';

##Cliente
    ##Impressao Cliente Código
    $impressao['cliente_codigo'] = $cliente['codigo'];
    
    ##Impressao Cliente Nome
    $impressao['cliente_nome'] = $cliente['nome'];
    
    ##Impressao Cliente CPF
    $impressao['cliente_cpf'] = $cliente['cpf'];
    
    ##Impressao Cliente Endereço
    $impressao['cliente_endereco'] = $cliente['endereco'];
    
    ##Impressao Cliente Bairro
    $impressao['cliente_bairro'] = $cliente['bairro'];
    
    ##Impressao Cliente Cidade
    $impressao['cliente_cidade'] = $cliente['cidade'];
    
    ##Impressao Cliente Estado
    $impressao['cliente_estado'] = $cliente['estado'];
    
    ##Impressao Cliente CEP
    $impressao['cliente_cep'] = $cliente['cep'];
    
    ##Impressao Cliente Contato
    $impressao['cliente_contato'] = $cliente['pessoa_contato'];
    
    ##Impressao Cliente Loja
    $impressao['cliente_loja'] = $cliente['loja'];
    
    ##Impressao Cliente Telefone
    $impressao['cliente_telefone'] = $cliente['telefone'];
    
    ##Impressao Cliente email
    $impressao['cliente_email'] = $cliente['email'];

	 	##Impressao Prospect Nome
	    $impressao['prospect_nome'] = $prospect->nome;

	 	##Impressao Prospect cpf
	    $impressao['prospect_cpf'] = $prospect->cpf ;
	    
	    ##Impressao Prospect telefone_contato_1
	    $impressao['prospect_telefone_contato_1'] = $prospect->telefone_contato_1 ;
	    
	    ##Impressao Prospect Id
	    $impressao['prospect_id'] = $prospect->id;
	    
	    ##Impressao Prospect cidade
	    $impressao['prospect_cidade'] = $prospect->cidade;
	    
	    ##Impressao Prospect estado
	    $impressao['prospect_estado'] = $prospect->estado ;
	    
	    ##Impressao Prospect email_contato_1
	    $impressao['prospect_email_contato_1'] = $prospect->email_contato_1 ;
	    
	    ##Impressao Prospect endereço
	    $impressao['prospect_endereco'] = $prospect->endereco ;
	    
	    ##Impressao Prospect bairro
	    $impressao['prospect_bairro'] = $prospect->bairro;
	    
	    ##Impressao Prospect cep
	    $impressao['prospect_cep'] = $prospect->cep ;
	  
	    
    
##Representante    
    ##Impressao Representante Nome
    $impressao['representante_nome'] = $representante['nome'];
    
    ##Impressao Representante Código
    $impressao['representante_codigo'] = $representante['codigo'];
    
    ##Impressao Representante cpf
    $impressao['representante_cpf'] = $representante['cpf'];
    
    ##Impressao Representante Telefone
    $impressao['representante_telefone'] = $representante['telefone'];
    
    ##Impressao Representante email
    $impressao['representante_email'] = $representante['email'];
    
    	##Impressao Representante endereco
    $impressao['representante_endereco'] =  $representante['endereco'];
    
    ##Impressao Representante bairro
    $impressao['representante_bairro'] = $representante['bairro'];
    
    ##Impressao Representante cep
    $impressao['representante_cep'] = $representante['cep'];
    
    ##Impressao Representante cidade
    $impressao['representante_cidade'] = $representante['cidade'];
    
    ##Impressao Representante estado
    $impressao['representante_estado'] = $representante['estado'];

			##Impressao Transportadora nome
    		$impressao['transportadora_nome'] =  $transportadora['nome'];
    		
    		##Impressao Transportadora codigo
    		$impressao['transportadora_codigo'] =  $transportadora['codigo'];
    		
    		##Impressao Transportadora cnpj
    		$impressao['transportadora_cnpj'] =  $transportadora['cnpj'];
    		
    		##Impressao Transportadora telefone
    		$impressao['transportadora_telefone'] =  $transportadora['telefone'];
    		
    		##Impressao Transportadora endereco
    		$impressao['transportadora_endereco'] =  $transportadora['endereco'];
    		
    		##Impressao Transportadora bairro
    		$impressao['transportadora_bairro'] =  $transportadora['bairro'];
    		
    		##Impressao Transportadora cep
    		$impressao['transportadora_cep'] =  $transportadora['cep'];
    		
    		##Impressao Transportadora cidade
    		$impressao['transportadora_cidade'] =  $transportadora['cidade'];
    		
    		##Impressao Transportadora estado
    		$impressao['transportadora_estado'] =  $transportadora['estado'];

		$item = count($pedidos);
		foreach($pedidos as $produto){
			$items['codigo_item']  = str_pad($item--, 2, 0, STR_PAD_LEFT);
			$items['codigo_produto'] = $produto['codigo_produto'];
			$items['descricao_produto']= $produto['descricao_produto'];
			$items['preco_produto']= $produto['preco_unitario'];
			$items['quantidade_vendida_produto']= $produto['quantidade'];
			$itens[] = $items;
		}
		
	##Impressao Itens do Pedido
    $impressao['pedido_itens'] = $itens;

	

	##Impressao Notas Fiscais
    $impressao['notas_fiscais'] = $pedido['notas_fiscais'];

	if($this->session->userdata('imprimir_pedido')){	
		$this->session->unset_userdata('imprimir_pedido');
	}

	$this->session->set_userdata('imprimir_pedido', $impressao);	
	
	}
	
	function valida_acesso($codigo_pedido){
		$acessos = $this->db->select()
							->from('controle_acessos_pedidos')
							->where('id_usuario', $this->session->userdata('id_usuario'))
							->where('dia_acesso', date('Y-m-d'))
							->get()->num_rows();
							
		$pedido = $this->db->select()
						   ->from('controle_acessos_pedidos')
						   ->where('codigo_pedido', $codigo_pedido)
						   ->where('id_usuario', $this->session->userdata('id_usuario'))
						   ->where('dia_acesso', date('Y-m-d'))
						   ->get()->num_rows();
		if (!$pedido){
			if($acessos < 10){
				$this->db->insert('controle_acessos_pedidos',
							array('id_usuario' => $this->session->userdata('id_usuario'),
								  'dia_acesso' => date('Y-m-d'),
								  'codigo_pedido' => $codigo_pedido));
			}else{
				redirect('pedidos/index?erro=esgotado');
			}
		}
	}
	
	function ver_detalhes($tipo = 'pedido',$id_pedido = NULL, $imprimir = FALSE, $filial = NULL, $id_pedido_2 = NULL, $tipo_pedido_2 = NULL){
		
		$pedido = $this->db_cliente->obter_pedidos_pdr($id_pedido, FALSE, $filial, $tipo);
		
		if ($pedido){
			$produtos = $this->db_cliente->obter_pedidos_pdr($id_pedido, TRUE, $filial, $tipo);
			
			$local_pedido = 'szw';
		}else{
			if ($this->session->userdata('grupo_usuario') != 'gestores_comerciais'){
				$this->valida_acesso($id_pedido);
			}
			
			$pedido = $this->db_cliente->obter_pedido($id_pedido, 0, 0, $filial);
			$posicionamento = $this->db_cliente->obter_posicionamento_pedido($id_pedido, $filial);
			$itens = $pedido['itens'];
			$local_pedido = 'sc5_6';
		}
		
		$empresa = $this->db->from('empresas')->where('id', 1)->get()->row();
		
		if($produtos){
			foreach($produtos as $indice => $produto){
				$produtos[$indice]['valor_st']					= (($produto['substituicao_tributaria'] / 100) * ($produto['preco_unitario'] * $produto['quantidade']) );
				$produtos[$indice]['total_ipi']					= (( $produto['ipi'] / 100) * ($produto['preco_unitario'] * $produto['quantidade'])) + ($produto['preco_unitario'] * $produto['quantidade']);
				$produtos[$indice]['valor_desconto']			= (($produto['desconto'] / 100) * ($produto['preco_unitario'] * $produto['quantidade']));
				$produtos[$indice]['peso_unitario']				= $produto['peso_unitario'];
				$produtos[$indice]['substituicao_tributaria']	= $produto['substituicao_tributaria'];
				
				$valor_total += $produto['preco_unitario'];
				$valor_total_ipi += $produtos[$indice]['total_ipi'];
				$valor_total_desconto += $produtos[$indice]['valor_desconto'];
				$valor_total_st += $produtos[$indice]['valor_st'];
			}
		}
		
		$pedido['valor_total'] = $valor_total;
		$pedido['valor_total_ipi'] = $valor_total_ipi;
		$pedido['valor_total_desconto'] = $valor_total_desconto;
		$pedido['valor_total_st'] = $valor_total_st;
		
		if($imprimir){
			/*return $this->load->view('layout', 
						array
						(
							'pedido' => $pedido,
							'produtos' => $produtos,
							'itens' => $itens,
							'empresa' => $empresa,
							'id_pedido' => $id_pedido
						), 
						TRUE);
			*/
			
			//echo '<pre>';print_r($pedidos);echo '</pre>';
			//echo $modent;
			
			
			return array(
							'pedido' => $pedido,
							'produtos' => $produtos,
							'itens' => $itens,
							'empresa' => $empresa,
							'id_pedido' => $id_pedido,
							'obter_formas_pagamento' => $this->_obter_formas_pagamento(),
							'obter_transportadoras' => $this->_obter_transportadoras()
							//'modent' => $modent
							//'eventos' => hnordt_gerar_feiras_ativas() // Evento,
						);
		}else{
			//Layout
			// PEDIDO IMEDIATO 1
			// PEDIDO PROGRAMADO 2
			if(!$id_pedido_2 && !$tipo_pedido_2){
				$id_pedido_2 = false;
				$tipo_pedido_2 = false;
			}
			
			$this->load->view('layout', array('conteudo' => $this->load->view('pedidos/ver_detalhes', 
						array
						(
							'local_pedido' => $local_pedido,
							'pedido' => $pedido,
							'posicionamento' => $posicionamento,
							'id_pedido_2' => $id_pedido_2,
							'tipo_pedido_2' => $tipo_pedido_2,
							'produtos' => $produtos,
							'itens' => $itens,
							'empresa' => $empresa,
							'id_pedido' => $id_pedido,
							'obter_formas_pagamento' => $this->_obter_formas_pagamento(),
							'obter_transportadoras' => $this->_obter_transportadoras(),
							'eventos' => hnordt_gerar_feiras_ativas(), // Evento,
							'modent' => $produtos[0]['modent']
                                                    //CUSTOM: Aristides - 21/06/2013
                                                        //'erro' => $erro
                                                    //FIM CUSTOM
						), 
						TRUE
					)
				)
			);
		}
	}
	
	
	function concluir($id = NULL)
	{
		// TODO: permissões
		
		if (!$id)
		{
			redirect('pedidos');	
		}
		
		$pedido = $this->db->from('pedidos')->where('id', $id)->get()->row();
		
		if (!$pedido || $pedido->status)
		{
			redirect('pedidos');	
		}
		
		$this->db->update('pedidos', array('status' => 'aguardando_analise_comercial'), array('id' => $pedido->id));
		
		$this->session->set_flashdata('opcao_criar_pedido_ver_detalhes_pedido', TRUE);
		
		redirect('pedidos/ver_detalhes/0/' . $pedido->id);
	}
	
	function aprovar($id = NULL)
	{
		// TODO: permissões
		
		if (!$id)
		{
			redirect('pedidos');	
		}
		
		$pedido = $this->db->from('pedidos')->where('id', $id)->get()->row();
		
		if (!$pedido || $pedido->status != 'aguardando_analise_comercial')
		{
			redirect('pedidos');	
		}
		
		$this->db->delete('pedidos', array('id' => $pedido->id));
		
		// TODO: protheus
		
		redirect('pedidos');
	}
	
	function transformar_pedido($id_pedido = NULL)
	{
		// TODO: permissões
		
		if (!$id_pedido)
		{
			redirect('pedidos');	
		}
		
		$confirmacao = $this->db_cliente->transformar_pedido($id_pedido);
		
		if($confirmacao)
		{
			redirect('pedidos/ver_detalhes/pedido/' . $id_pedido);
		}
	}
	
	function aprovar_pedido($id_pedido = NULL)
	{
		// TODO: permissões
		
		if (!$id_pedido)
		{
			redirect('pedidos');	
		}
		
		$confirmacao = $this->db_cliente->aprovar_pedido($id_pedido);
		
		if($confirmacao)
		{
			redirect('pedidos/aguardando_analise');
		}
	}
	
	function reprovar_pedido($id_pedido = NULL)
	{
	
		if (!$id_pedido)
		{
			redirect('pedidos');	
		}
	
		if($_POST)
		{
			if (!$this->input->post('motivo_reprovacao'))
			{
				$erro = 'Digite o motivo da reprovação.';
			}
			else
			{
				$confirmacao = $this->db_cliente->reprovar_pedido($id_pedido, $this->input->post('motivo_reprovacao'));
		
				if($confirmacao)
				{
					redirect('pedidos/aguardando_analise');
				}
			}
		}
	
		// VALIDAR FORM **
		
		// ** EXIBIR FORM
		$conteudo .= heading('Reprovar ' . ($pedido->tipo == 'orcamento' ? 'Orçamento' : 'Pedido'), 2);
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		$conteudo .= form_open(current_url());
		$conteudo .= '<p>' . form_label('Motivo da reprovação:' . br() . form_textarea(array('cols' => 60, 'rows' => 8, 'name' => 'motivo_reprovacao', 'value' => $this->input->post('motivo_reprovacao')))) . '</p>';
		$conteudo .= '<p>' . form_submit('concluir', 'Concluir') . ' ou ' . anchor('pedidos/ver_detalhes/0/' . $pedido->id, 'Cancelar') . '</p>';
		$conteudo .= form_close();
		// EXIBIR FORM **
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function reprovar($id = NULL)
	{
		// TODO: permissões
		
		if (!$id)
		{
			redirect('pedidos');	
		}
		
		$pedido = $this->db->from('pedidos')->where('id', $id)->get()->row();
		
		if (!$pedido || ($pedido->tipo == 'pedido' && $pedido->status != 'aguardando_analise_comercial'))
		{
			redirect('pedidos');	
		}
		
		// ** VALIDAR FORM
		if ($_POST)
		{
			if (!$this->input->post('motivo_reprovacao'))
			{
				$erro = 'Digite o motivo da reprovação.';
			}
			else
			{
				$this->db->update('pedidos', array('status' => 'reprovado_pelo_comercial', 'motivo_reprovacao' => $this->input->post('motivo_reprovacao')), array('id' => $pedido->id));
				
				redirect('pedidos/ver_detalhes/0/' . $pedido->id);
			}
		}
		// VALIDAR FORM **
		
		// ** EXIBIR FORM
		$conteudo .= heading('Reprovar ' . ($pedido->tipo == 'orcamento' ? 'Orçamento' : 'Pedido'), 2);
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		$conteudo .= form_open(current_url());
		$conteudo .= '<p>' . form_label('Motivo da reprovação:' . br() . form_textarea(array('cols' => 60, 'rows' => 8, 'name' => 'motivo_reprovacao', 'value' => $this->input->post('motivo_reprovacao')))) . '</p>';
		$conteudo .= '<p>' . form_submit('concluir', 'Concluir') . ' ou ' . anchor('pedidos/ver_detalhes/0/' . $pedido->id, 'Cancelar') . '</p>';
		$conteudo .= form_close();
		// EXIBIR FORM **
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function editar_codigo_usuario_consultar_pedidos($novo_codigo = NULL)
	{
		$this->session->set_userdata('codigo_usuario_consultar_pedidos', $novo_codigo);
		
		redirect('pedidos');
	}
	
	// TODO: remover
	function consultar_pedidos($codigo_cliente = NULL, $loja_cliente = NULL)
	{
		redirect('pedidos/index/' . $codigo_cliente . '/' . $loja_cliente);
	}
	
	function editar_codigo_usuario_consultar_notas_fiscais($novo_codigo = NULL)
	{
		$this->session->set_userdata('codigo_usuario_consultar_notas_fiscais', $novo_codigo);
		
		redirect('pedidos/consultar_notas_fiscais');
	}
	
	function consultar_notas_fiscais($codigo_cliente = NULL, $loja_cliente = NULL)
	{
		// ** VALIDAR FORM
		if ($_POST)
		{
			if (!$this->input->post('cliente'))
			{
				$erro = 'Selecione um cliente.';
			}
			else if ($this->input->post('data_inicial') && !$this->_validar_data($this->input->post('data_inicial')))
			{
				$erro = 'Digite uma data inicial válida.';
			}
			else if ($this->input->post('data_final') && !$this->_validar_data($this->input->post('data_final')))
			{
				$erro = 'Digite uma data final válida.';
			}
			else
			{
				if ($this->input->post('data_inicial')) {
					$data_inicial = explode('/', $this->input->post('data_inicial'));
					$data_inicial = $data_inicial[2] . $data_inicial[1] . $data_inicial[0];
				}
				
				if ($this->input->post('data_final')) {
					$data_final = explode('/', $this->input->post('data_final'));
					$data_final = $data_final[2] . $data_final[1] . $data_final[0];
				}
				
				$codigo_loja_cliente = explode('|', $this->input->post('codigo_loja_cliente'));
				
				$notas_fiscais = $this->db_cliente->obter_notas_fiscais($codigo_loja_cliente[0], $codigo_loja_cliente[1], $data_inicial, $data_final);
			}
		}
		else if ($codigo_cliente && $loja_cliente)
		{
			$cliente = $this->db_cliente->obter_cliente($codigo_cliente, $loja_cliente);
			
			if ($cliente)
			{
				$notas_fiscais = $this->db_cliente->obter_notas_fiscais($cliente['codigo'], $cliente['loja']);
				
				$_POST['cliente'] = $cliente['nome'];
				$_POST['codigo_loja_cliente'] = $cliente['codigo'] . '|' . $cliente['loja'];
			}
		}
		// VALIDAR FORM **
		
		$conteudo .= '
			<div class="caixa_b">
				<p>' . form_label('Representante: ' . form_dropdown('codigo_usuario_consultar_notas_fiscais', $this->_obter_representantes(), $this->session->userdata('codigo_usuario_consultar_notas_fiscais'), 'onchange="window.location = \'' . site_url('pedidos/editar_codigo_usuario_consultar_notas_fiscais') . '/\' + $(this).val();"')) . '</p>
			</div>
		';
		
		// ** EXIBIR FORM
		$conteudo .= heading('Consultar Notas Fiscais', 2);
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		$conteudo .= form_open(current_url());
		$conteudo .= '<p>' . form_label('Cliente:' . br() . form_input('cliente', $this->input->post('cliente'), 'alt="obter_clientes_consultar_notas_fiscais" class="autocomplete" size="40"') . form_hidden('codigo_loja_cliente', $this->input->post('codigo_loja_cliente'))) . '</p>';
		$conteudo .= '<p>' . form_label('Data:' . br() . form_input('data_inicial', $this->input->post('data_inicial'), 'class="datepicker data_inicial" size="6"')) . ' à ' . form_input('data_final', $this->input->post('data_final'), 'class="datepicker" size="6"') . '</p>';
		$conteudo .= '<p>' . form_submit('consultar', 'Consultar') . '</p>';
		$conteudo .= form_close();
		// EXIBIR FORM **
		
		if (isset($notas_fiscais))
		{
			
			if ($this->input->post('consultar'))
			{
		
				// ** EXIBIR NOTAS FISCAIS
				$conteudo .= heading('Resultados da Consulta', 3);
				$conteudo .= '<table cellspacing="0"><thead><tr><th>Código N.F.</th><th>Data Emissão</th><th>Total (R$)</th><th>ICMS (R$)</th><th>IPI (R$)</th><th>Frete</th><th>Transp.</th><th>Tel. Transp.</th><th>Opções</th></tr></thead><tbody>';
				foreach ($notas_fiscais as $nota_fiscal)
				{
					$conteudo .= '<tr><td>' . $nota_fiscal['codigo'] . '</td><td>' . date('d/m/Y', $nota_fiscal['emissao_timestamp']) . '</td><td>' . number_format($nota_fiscal['valor_total'], 5, ',', '.') . '</td><td>' . number_format($nota_fiscal['valor_icms'], 5, ',', '.') . '</td><td>' . number_format($nota_fiscal['valor_ipi'], 5, ',', '.') . '</td><td>' . $nota_fiscal['tipo_frete'] . '</td><td>' . $nota_fiscal['transportadora']['nome'] . '</td><td>' . $nota_fiscal['transportadora']['telefone'] . '</td><td><a href="#nota_fiscal_' . $nota_fiscal['codigo'] . '" class="colorbox_inline">Ver Títulos</a>';
					$conteudo .= '<div style="display: none;"><div id="nota_fiscal_' . $nota_fiscal['codigo'] . '" style="overflow: hidden;"><table cellspacing="0" style="width: 800px;"><thead><tr><th>Código N.F.</th><th>Parcela</th><th>Status</th><th>Valor (R$)</th><th>Vencimento</th><th>Pagamento</th></tr></thead><tbody>';
					foreach ($nota_fiscal['titulos'] as $titulo)
					{
						$conteudo .= '<tr><td>' . $nota_fiscal['codigo'] . '</td><td>' . $titulo['parcela'] . '</td><td>' . $titulo['status'] . '</td><td>' . number_format($titulo['valor'],5, ',', '.') . '</td><td>' . date('d/m/Y', $titulo['vencimento_timestamp']) . '</td><td>' . ($titulo['pagamento_timestamp'] ? date('d/m/Y', $titulo['pagamento_timestamp']) : 'não há') . '</td></tr>';
					}
					$conteudo .= '</tbody></table></div></div>';
					$conteudo .= '</td></tr>';
				}
				$conteudo .= '</tbody></table>';
				// EXIBIR NOTAS FISCAIS **
			
			}
		}
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function consultar_titulos_vencidos($codigo_cliente = NULL, $loja_cliente = NULL)
	{
		// ** VALIDAR FORM
		if ($_POST)
		{
			if ($this->input->post('data_vencimento_inicial') && !$this->_validar_data($this->input->post('data_vencimento_inicial')))
			{
				$erro = 'Digite uma data de vencimento inicial válida.';
			}
			else if ($this->input->post('data_vencimento_final') && !$this->_validar_data($this->input->post('data_vencimento_final')))
			{
				$erro = 'Digite uma data de vencimento final válida.';
			}
			else
			{
				if ($this->input->post('data_vencimento_inicial')) {
					$data_vencimento_inicial = explode('/', $this->input->post('data_vencimento_inicial'));
					$data_vencimento_inicial = $data_vencimento_inicial[2] . $data_vencimento_inicial[1] . $data_vencimento_inicial[0];
				}
				
				if ($this->input->post('data_vencimento_final')) {
					$data_vencimento_final = explode('/', $this->input->post('data_vencimento_final'));
					$data_vencimento_final = $data_vencimento_final[2] . $data_vencimento_final[1] . $data_vencimento_final[0];
				}
				
				$titulos = $this->db_cliente->obter_titulos_vencidos(NULL, NULL, $data_vencimento_inicial, $data_vencimento_final);
			}
		}
		else if ($codigo_cliente && $loja_cliente)
		{
			$cliente = $this->db_cliente->obter_cliente($codigo_cliente, $loja_cliente);
			
			if ($cliente)
			{
				$titulos = $this->db_cliente->obter_titulos_vencidos($cliente['codigo'], $cliente['loja']);
				
				$_POST['cliente'] = $cliente['nome'];
				$_POST['codigo_loja_cliente'] = $cliente['codigo'] . '|' . $cliente['loja'];
			}
		}
		// VALIDAR FORM **
		
		// ** EXIBIR FORM
		$conteudo .= heading('Consultar Títulos Vencidos', 2);
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		$conteudo .= form_open(current_url());
		if ($cliente)
		{
			$conteudo .= '<p>Cliente:' . br() .  $cliente['nome'] . ' - ' . $cliente['cpf'] . '</p>';
		}
		$conteudo .= '<p>' . form_label('Data de vencimento:' . br() . form_input('data_vencimento_inicial', $this->input->post('data_vencimento_inicial'), 'class="datepicker data_inicial" size="6"')) . ' à ' . form_input('data_vencimento_final', $this->input->post('data_vencimento_final'), 'class="datepicker" size="6"') . '</p>';
		$conteudo .= '<p>' . form_submit('consultar', 'Consultar') . '</p>';
		$conteudo .= form_close();
		// EXIBIR FORM **
		
		if (isset($titulos))
		{
			
			if($this->input->post('consultar'))
			{
		
				// ** EXIBIR TÍTULOS
				$conteudo .= heading('Resultados da Consulta', 3);
				$conteudo .= '<table cellspacing="0"><thead><tr><th>Cliente</th><th>Código N.F.</th><th>Parcela</th><th>Status</th><th>Valor (R$)</th><th>Vencimento</th><th>Pagamento</th></tr></thead><tbody>';
				foreach ($titulos as $titulo)
				{
					$conteudo .= '<tr><td>' . $titulo['cliente']['nome'] . '</td><td>' . $titulo['codigo'] . '</td><td>' . $titulo['parcela'] . '</td><td>' . $titulo['status'] . '</td><td>' . number_format($titulo['valor'], 5, ',', '.') . '</td><td>' . date('d/m/Y', $titulo['vencimento_timestamp']) . '</td><td>' . ($titulo['pagamento_timestamp'] ? date('d/m/Y', $titulo['pagamento_timestamp']) : 'não há') . '</td></tr>';
				}
				$conteudo .= '</tbody></table>';
				// EXIBIR TÍTULOS **
			
			}
		}
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function obter_valor_frete_ajax()
	{

		$total_peso = $_POST["peso_total"];
		$cliente = explode('|', $_POST["cliente"]);
		
		$tabela_preco = $this->db_cliente->obter_tabela_preco($cliente[0], $cliente[1]);
		
		//Valor do Frete
		if($total_peso <= 600)
		{
			$frete = $this->db->select(strtolower($tabela_preco['tabela_frete']))->from('tabela_frete')->where(array('kg' => 1))->get()->row_array();
		
			$valor_frete = $frete[strtolower($tabela_preco['tabela_frete'])];
		}
		else if($total_peso >= 601 && $total_peso <= 999)
		{
			$frete = $this->db->select(strtolower($tabela_preco['tabela_frete']))->from('tabela_frete')->where(array('kg' => 601))->get()->row_array();
		
			$valor_frete = $total_peso * $frete[strtolower($tabela_preco['tabela_frete'])];
		}
		else if($total_peso >= 1000 && $total_peso <= 1999)
		{
			$frete = $this->db->select(strtolower($tabela_preco['tabela_frete']))->from('tabela_frete')->where(array('kg' => 1000))->get()->row_array();
		
			$valor_frete = $total_peso * $frete[strtolower($tabela_preco['tabela_frete'])];
		}
		else if($total_peso >= 2000 && $total_peso <= 2999)
		{
			$frete = $this->db->select(strtolower($tabela_preco['tabela_frete']))->from('tabela_frete')->where(array('kg' => 2000))->get()->row_array();
		
			$valor_frete = $total_peso * $frete[strtolower($tabela_preco['tabela_frete'])];
		}
		else if($total_peso >= 3000 && $total_peso <= 9999)
		{
			$frete = $this->db->select(strtolower($tabela_preco['tabela_frete']))->from('tabela_frete')->where(array('kg' => 3000))->get()->row_array();
		
			$valor_frete = $total_peso * $frete[strtolower($tabela_preco['tabela_frete'])];
		}
		else if($total_peso >= 10000)
		{
			$frete = $this->db->select(strtolower($tabela_preco['tabela_frete']))->from('tabela_frete')->where(array('kg' => 10000))->get()->row_array();
		
			$valor_frete = $total_peso * $frete[strtolower($tabela_preco['tabela_frete'])];
		}
	
		echo number_format($valor_frete, 5, ',', '.') . '|' . $valor_frete;
		
	}
	
	//
	
	function _obter_status($opcao_todos = FALSE)
	{
		$status = array('aguardando_analise_comercial' => 'Aguardando Análise Comercial', 'aguardando_faturamento' => 'Aguardando Faturamento', 'parcialmente_faturado' => 'Parcialmente Faturado', 'faturado' => 'Faturado', 'faturado_residuo_eliminado' => 'Faturado (Resíduo Eliminado)', 'reprovado_pelo_comercial' => 'Reprovado Pelo Comercial');
		
		if ($opcao_todos)
		{
			$status = array_merge(array('todos' => 'Todos'), $status);
		}
		
		return $status;
	}
	
	function _obter_clientes($codigo_usuario = NULL)
	{
		$clientes = $this->db_cliente->obter_clientes($codigo_usuario);
		$_clientes = array();
		
		foreach ($clientes as $cliente)
		{
			$_clientes[$cliente['codigo'] . '|' . $cliente['loja']] = $cliente['codigo'] . ' - ' . $cliente['nome'] . ' - ' . $cliente['cpf'];
		}
		
		return $_clientes;
	}
	
	function _obter_prospects($codigo_usuario = NULL)
	{
		$prospects = $this->db->from('prospects')->where(array('status' => 'ativo', 'codigo_usuario' => $codigo_usuario))->get()->result();
		$_prospects = array();
		
		foreach ($prospects as $prospect)
		{
			$_prospects[$prospect->id] = $prospect->nome . ' - ' . $prospect->cpf;
		}
		
		return $_prospects;
	}
	
	function _obter_representantes($opcao_todos = TRUE)
	{
		$representantes = $this->db->from('usuarios')->or_where(array('grupo' => 'representantes', 'grupo' => 'revendas'))->order_by('nome', 'asc')->get()->result();
		$_representantes = $opcao_todos ? array(0 => 'Todos') : array();
		
		foreach ($representantes as $representante)
		{
			$_representantes[$representante->codigo] = $representante->codigo . ' - ' . $representante->nome;
		}
		
		return $_representantes;
	}
	
	function _obter_formas_pagamento()
	{
		$formas_pagamento = $this->db_cliente->obter_formas_pagamento();
		$_formas_pagamento = array();
		
		foreach ($formas_pagamento as $forma_pagamento)
		{
			$_formas_pagamento[$forma_pagamento['codigo']] = $forma_pagamento['descricao'];
		}
		
		return $_formas_pagamento;
	}
	
	function _obter_transportadoras()
	{
		$transportadoras = $this->db_cliente->obter_transportadoras();
		$_transportadoras = array();
		
		foreach ($transportadoras as $transportadora)
		{
			$_transportadoras[$transportadora['codigo']] = $transportadora['nome'];
		}
		
		return $_transportadoras;
	}
	
	function _obter_grupos_produtos()
	{
		$grupos_produtos = $this->db_cliente->obter_grupos_produtos();
		$_grupos_produtos = array();
		
		foreach ($grupos_produtos as $grupo_produtos)
		{
			$_grupos_produtos[$grupo_produtos['codigo']] = $grupo_produtos['codigo'] . ' - ' . $grupo_produtos['descricao'];
		}
		
		return $_grupos_produtos;
	}
	
	function _obter_produtos_tabela_precos($codigo_grupo_produtos = NULL, $codigo_tabela_precos = NULL)
	{	
		if($codigo_tabela_precos == NULL)
		{
			$_codigo_tabela_precos = $this->db_cliente->obter_primeiro_item_tabela_precos();		
			$codigo_tabela_precos = $_codigo_tabela_precos['codigo'];
		}
	
		$produtos = $this->db_cliente->obter_produtos_tabela_precos($codigo_grupo_produtos, $codigo_tabela_precos);
		
		$_produtos = array();
		
		if($produtos)
		{
			foreach ($produtos as $produto)
			{
				$_produtos[$produto['codigo']] = $produto['codigo_real'] . ' - ' . $produto['descricao'] . ' - Estoque atual: ' .  number_format(($produto['estoque_atual'] > 0 ? $produto['estoque_atual'] : 0), 5, ',', '.');
			}
		}
		
		return $_produtos;
		
	}
	
	function _obter_produtos($codigo_grupo_produtos = NULL)
	{
		/*if (!$codigo_grupo_produtos)
		{
			return array();
		}*/
		
		$produtos = $this->db_cliente->obter_produtos($codigo_grupo_produtos);
		$_produtos = array();
		
		foreach ($produtos as $produto)
		{																																						
			$_produtos[$produto['codigo']] = $produto['codigo_real'] . ' - ' . $produto['descricao'] . ' - Estoque atual: ' . number_format(($produto['estoque_atual'] > 0 ? $produto['estoque_atual'] : 0), 5, ',', '.');
		}
		
		return $_produtos;
	}
	
	function _obter_opcionais_produto($codigo_produto = NULL)
	{
		$produtos = $this->db_cliente->obter_opcionais_produto($codigo_produto);
		$_produtos = array();
		
		foreach ($produtos as $produto)
		{
			$_produtos[$produto['codigo']] = $produto['codigo'] . ' - ' . $produto['descricao'];
		}
		
		return $_produtos;
	}
	
	function _obter_tabelas_precos($codigo_usuario)
	{
		$tabelas_precos = $this->db_cliente->obter_tabelas_precos();
		$_tabelas_precos = array();
		
		$tabela_precos_usuario = $this->db->from('usuarios')->where('codigo', $codigo_usuario)->get()->row()->tabela_precos;
		$_tabela_precos_usuario = unserialize($tabela_precos_usuario);
		
		if($_tabela_precos_usuario)
		{
			foreach ($tabelas_precos as $tabela_precos)
			{
				
				foreach($_tabela_precos_usuario as $codigo_tabela_precos)
				{
					if($codigo_tabela_precos['codigo'] == $tabela_precos['codigo'])
					{
						$_tabelas_precos[$tabela_precos['codigo']] = $tabela_precos['descricao'];
					}
				}

			}
		}
		
		return $_tabelas_precos;
	}
	
	function cancelar_pedido($id_pedido){
		
		//anchor('pedidos/ver_detalhes/0/' . $pedido->id, 'Cancelar')
		$this->db->delete('itens_pedidos', array('id_pedido' => $id_pedido)); 
		$this->db->delete('pedidos', array('id' => $id_pedido)); 
		redirect('pedidos');
		
	}
	
	function obter_clientes($codigo_do_representante = NULL)
	{
		$palavras_chave = addslashes($_GET['term']);
		
		$dados = array();
		
		$clientes = $this->autocomplete->obter_clientes($palavras_chave, $codigo_do_representante);
		
		foreach ($clientes as $cliente)
		{
			$dados[] = array('label' => $cliente['nome'] . ' - ' . $cliente['cpf'], 'value' => $cliente['codigo'] . '|' . $cliente['loja']);
		}
		
		
		echo json_encode($dados);
	}
	
	function obter_prospects($codigo_do_representante = NULL)
	{
		$palavras_chave = addslashes($_GET['term']);
		
		$dados = array();
		
		$prospects = $this->autocomplete->obter_prospects($palavras_chave, $codigo_do_representante);
		
		foreach ($prospects as $prospect)
		{
			$dados[] = array('label' => $prospect['nome'] . ' - ' . $prospect['cpf'], 'value' => $prospect['id']);
		}
		
		
		echo json_encode($dados);
	}
	
	
	
	
	
	function imprimir(){

		if($this->session->userdata('imprimir_pedido'))
		{
		
			$array = $this->session->userdata('imprimir_pedido');
			$empresa = $this->db->from('empresas')->where('id', 1)->get()->row_array();
			
			$conteudo = '<style type="text/css">
				#logo img{
				width:100px;
				}
			</style>';
			$conteudo .= '<style type="text/css" media="print">
			/* CSS Document */
			
			body {
			 width: 500 pt;
			 height: 500 pt;
			 overflow: none;
			 display: block;
			 font-size:10px;
			
			}
			#geral{
			overflow: visible;
			margin: 0 auto;
			margin-top:30px;
			}
			
			table{
			width:100%;
			}
			
			#logo{
			text-align:top;
			}
			
			#logo img{
			width:100px;
			}
			
			.categoria{
			font-style: italic;
			
			}
			
			table.zebra ,table.zebra td
			{
			border:1px solid black;
			border-spacing:0px;
			padding:3px;
			}
			table.zebra th
			{
			background-color:rgb(149,149,149);
			color:black;
			}
			
			.corpo{
			 padding-top:35px;
			}
			
			#cabecario tr td{
			text-align:left;
			padding-top:0px;
			}
			
			#cabecario tr .right{
			text-align: right;
			}
			
			#titulo{
			width:60%;
			font: normal bold 14px bold ;
			}
			
			#postitulo td{
			padding-top:15px;
			}
			
			
			td{
			font: normal normal normal x-small normal Lucida Sans Unicode;
			page-break-before:auto;
			
			}
			
			td.center{
				text-align:center;
			}
			
			td.right{
				text-align:right;
			}
			
			th{
			text-align:center;
			}
			
			td.linha_baixa{
				border-bottom: 2px solid #000;
			}
			tr.vai_cor{
				background-color: #EEE;
			}
			#tab_head { 
			display: table-header-group;
			}
			#tab_head td	{position: static; } 
			#tab_head tr	{position: static; } /*prevent problem if print after scrolling table*/ 
			
			</style>';
			    
			
						
						
			$conteudo .= 
					'<script type="text/javascript">
						$(document).ready(function(){
							print();
						});
					</script>';
			    
			//[codigo_cliente] => DW1000 [razao_social] => MJGF Serviços Administrativos Ltda. [nome_fantasia] => Developweb [cnpj] => 05.026.699/0001-82 [inscricao_estadual] => [cep] => 88302-001 [endereco] => Rua: Brusque [numero] => 737 [bairro] => Centro [cidade] => Itajaí [estado] => SC [telefone_1] => (47) 3348-5829 [email] => contato@developweb.com.br [observacao] => [telefone_2] => (47) 3348-5510 )
			$conteudo.='<div id="geral">
			  <div>
			    <table id="cabecario">
			      <thead id="tab_head">
			  	   	<tr><td id="logo" rowspan="3"  class="linha_baixa"><img src="'.base_url().'/third_party/img/logo.jpg"/></td><td colspan="2">'.$empresa['razao_social'].' - CNPJ: '.$empresa['cnpj'].'</td></tr>
			  		<tr><td colspan="3" >'.$empresa['endereco'].', '.$empresa['numero'].' - '.$empresa['bairro'].' | '.$empresa['cidade'].' - '.$empresa['estado'].' | CEP:'.$empresa['cep'].'</td></tr>
			  		<tr><td colspan="3" class="linha_baixa" > Telefone: '.$empresa['telefone_1'].' | email: '.$empresa['email'].'</td></tr> 
			  		
			  		</thead>
			  		
			  		<tbody>
			        <tr><td id="titulo" colspan="3"><br />Espelho do '.$array['tipo'].'<br /><br /></td><td class="right"><b>Emissão: </b>'.$array['emissao'].'<br /></td></tr>
			  		<tr id="postitulo"><td><b>Status:</b>'.$array['status'].' </td><td><b>Código: </b>'.$array['codigo'].'</td><td><b>Preço Médio/Kg: </b>'.$array['preco_medio'].'</td></tr>
			  		 <tr><td><b>O.C.: </b>'.$array['ordem_compra'].'</td><td><b>Tipo de Frete: </b>'.$array['tipo_frete'].'</td><td><b>Forma Pagamento: </b>'.$array['forma_pagamento'].'</td><td><b>Peso total: </b>' . $array['pedido_peso_total'].' Kg</td></tr>
			      <tbody>
			    </table>
			  </div>';

			if($array['cliente_codigo'] && $array['cliente_loja']){
			$conteudo.= '
			  <div class="corpo">
			  <strong class="categoria">Cliente</strong>
				<table>  
			     <tr><td><b>Nome: </b>'.$array['cliente_codigo'].' - '.$array['cliente_nome'].'</td><td><b>Loja: </b>'.$array['cliente_loja'].'</td><td><b>CPF/CNPJ: </b>'.$array['cliente_cpf'].'</td></tr>
			     <tr><td><b>Endereço: </b>'.$array['cliente_endereco'].' - '.$array['cliente_bairro'].'</td><td><b>Cidade: </b>'.$array['cliente_cidade'].' - '.$array['cliente_estado'].'</td><td><b>CEP: </b>'.$array['cliente_cep'].'</td></tr>
			     <tr><td><b>Telefone: </b>'.$array['cliente_telefone'].'</td><td colspan="2"><b>e-mail: </b>'.$array['cliente_email'].'</td></tr>
			    </table>
			  </div>';
			}else{
			$conteudo.= '
			  <div class="corpo">
			  <strong class="categoria">Prospect</strong>
				<table>  
			     <tr><td><b>Nome: </b>'.$array['prospect_nome'].'</td><td><b>CPF/CNPJ: </b>'.$array['prospect_cpf'].'</td></tr>
			     <tr><td><b>Endereço: </b>'.$array['prospect_endereco'].' - '.$array['prospect_numero'].'</td><td><b>Cidade: </b>'.$array['prospect_cidade'].' - '.$array['prospect_estado'].'</td><td><b>CEP: </b>'.$array['prospect_cep'].'</td></tr>
			     <tr><td><b>Telefone: </b>'.$array['prospect_telefone_contato_1'].'</td><td colspan="2"><b>e-mail: </b>'.$array['prospect_email_contato_1'].'</td></tr>
			    </table>
			  </div>';
			}
			
			
			#  
			##  #informações sobre o representante
			#
			//print_r($array);
			  $conteudo .='<div class="corpo">
			  <strong class="categoria">Representante</strong>
			    <table>  
			     <tr><td><b>Representante: </b>'.$array['representante_codigo'].' - '.$array['representante_nome'].'</td><td><b>CPF/CNPJ: </b>'.$array['representante_cpf'].'</td></tr>
			     <tr><td><b>Telefone: </b>'.$array['representante_telefone'].'</td><td colspan="2"><b>e-mail: </b>'.$array['representante_email'].'</td></tr>
			    </table>
				</div>';
				
		 $conteudo.= '<div class="corpo">
			   <strong >Itens do Pedido</strong>
				  <table class="zebra">
			      <thead>
			        <tr><th>Item</th><th>Produto</th><th>Descrição</th><th>Preço (R$)</th><th>Quant. Vend.</th><th>Total (R$)</th></tr>
			      </thead>
			      <tbody>';
				$pedido['itens'] = $array['pedido_itens'];
				$i = 1;
			      foreach ($pedido['itens'] as $item_pedido)
					{
						
						if ($i%2 ==1){
							$style = 'style="background-color:#DDD"';	
						}else{
							$style = null;	
						}
						
						$conteudo .= '<tr '.$style.' ><td class="center">' . ($item_pedido['codigo_item']?$item_pedido['codigo_item']:$item_pedido['item']). '</td><td class="right">' . $item_pedido['codigo_produto'] . '</td><td>' . $item_pedido['descricao_produto'] . '</td><td class="right">' . number_format($item_pedido['preco_produto'], 5, ',', '.') . '</td><td class="right">' . number_format($item_pedido['quantidade_vendida_produto'], 0, NULL, '.') . '</td><td class="right">'.number_format(($item_pedido['preco_produto'] * $item_pedido['quantidade_vendida_produto']), 5, ',', '.').'</td></tr>';
						$i++;
					}
					
			     $conteudo .='</tbody>
			    </table>
				</div>';
			     
			     $conteudo .='<div class="corpo">
						  <strong class="categoria">Totais</strong>
						  <table>
						    <tr><td><b>Quant. vend.:</b>' .$array['pedido_quantidade_vendida']. '</td><td><b>Quant. fat.: </b>'.$array['pedido_quantidade_faturada'].'</td><td><b>Peso total: </b>' .$array['pedido_peso_total']. ' Kg</td></tr>
						    <tr><td><b>Valor do frete/Kg:</b> R$ ' .$array['pedido_valor_frete_kg']. '</td><td><b>Valor do frete: </b>R$ '.$array['pedido_valor_frete'].'</td>
						    
						    <tr><td><b>Valor parcial:</b> R$ ' .$array['pedido_valor_parcial']. '</td><td><b>Valor total: </b>R$ '.$array['pedido_valor_total'].'</td><td><b>Valor total + frete: </b>R$ ' .$array['pedido_total_mais_frete']. '</td></tr>
						    
						    <tr><td colspan="2"><b>Obs.:</b>'.$array['observacao'].'</td></tr>

	
						  </table>
						  </div>
						  </div>
						  
						';
				}
    //echo $conteudo;
    $this->load->view('layout', array('conteudo' => $conteudo));
  }
  
	function obter_tabela_preco($codigo, $loja)
	{
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->config->load('db_cliente_' . $this->db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');
		
		$campos = array('tabela_frete');
		
		foreach ($campos as $campo)
		{
			$select[] = $this->_db_cliente['campos']['clientes'][$campo] . ' AS ' . $campo;
		}
		
		if ($this->_db_cliente['campos']['clientes']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['clientes']['delecao'], '');
		}
		
		return $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['clientes'])->where(array($this->_db_cliente['campos']['clientes']['codigo'] => $codigo, $this->_db_cliente['campos']['clientes']['loja'] => $loja))->get()->row_array();
		
	}
}