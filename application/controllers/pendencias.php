<?php

class Pendencias extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

function obter_alertas()
	{
		
		if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros'))) {
			$total_pendencias_hoje = $this->db->from('pendencias')->where('(status = "em_aberto" OR status = "aguardando_encerramento")')->get()->num_rows();
		} else {			
			$total_pendencias_hoje = $this->db
							->from('pendencias')
							->join('usuarios_pendencias', 'usuarios_pendencias.id_pendencia = pendencias.id')
							->where('(pendencias.id_usuario = ' . $this->session->userdata('id_usuario') . ' OR usuarios_pendencias.id_usuario = ' . $this->session->userdata('id_usuario') . ') AND (status = "em_aberto" OR status = "aguardando_encerramento")')->group_by('usuarios_pendencias.id_pendencia')->order_by('pendencias.id', 'desc')->get()->num_rows();
		}
		
		$dados['num_pendencias'] = $total_pendencias_hoje;
		$dados['pendencias'] = '<p style="color: #0FF; font-size: 12px;"><strong>Pendências (' . $total_pendencias_hoje . ')</strong> - ' . anchor('pendencias', 'Ver todas &raquo;', 'style="color: #FF0;"') . '</p>';
		
		if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros'))) {
			$ultima_mensagem = $this->db->from('pendencias')->join('mensagens_pendencias', 'mensagens_pendencias.id_pendencia = pendencias.id')->where('(status = "em_aberto" OR status = "aguardando_encerramento")')->order_by('mensagens_pendencias.id', 'desc')->limit(1)->get()->row();
		} else {
			$ultima_mensagem = $this->db
								->from('pendencias')
								->join('mensagens_pendencias', 'mensagens_pendencias.id_pendencia = pendencias.id')
								->join('usuarios_pendencias', 'usuarios_pendencias.id_pendencia = pendencias.id')
								->where('(pendencias.id_usuario = ' . $this->session->userdata('id_usuario') . ' OR usuarios_pendencias.id_usuario = ' . $this->session->userdata('id_usuario') . ') AND (status = "em_aberto" OR status = "aguardando_encerramento")')
								->group_by('usuarios_pendencias.id_pendencia')
								->order_by('mensagens_pendencias.id', 'desc')->limit(1)->get()->row();			
		}
			
		if ($ultima_mensagem)
		{
			$ultima_mensagem->conteudo = strip_tags($ultima_mensagem->conteudo);
			
			if (strlen($ultima_mensagem->conteudo) > 15)
			{
				$conteudo_ultima_mensagem = substr($ultima_mensagem->conteudo, 0, 15) . '...';
			}
			else
			{
				$conteudo_ultima_mensagem = $ultima_mensagem->conteudo;
			}
			
			$dados['pendencias'] .= '<p style=""><strong>Última mensagem:</strong> ' . $conteudo_ultima_mensagem . ' - Enviada há ' . $this->_obter_descricao_tempo_segundos(time() - $ultima_mensagem->ultima_mensagem_timestamp) . ' - ' . anchor('pendencias/ver_detalhes/' . $ultima_mensagem->id_pendencia, '+ Opções &raquo;') . '</p>';
		}
		else
		{
			$dados['pendencias'] .= '<p style=""><strong>Última mensagem:</strong> não há</p>';
		}
		
		if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros'))) {
			$pendencia_mais_antiga = $this->db->from('pendencias')->where('(status = "em_aberto" OR status = "aguardando_encerramento")')->order_by('pendencias.id', 'desc')->limit(1)->get()->row();
		} else {
			$pendencia_mais_antiga = $this->db->from('pendencias')->join('usuarios_pendencias', 'usuarios_pendencias.id_pendencia = pendencias.id')->where('(pendencias.id_usuario = ' . $this->session->userdata('id_usuario') . ' OR usuarios_pendencias.id_usuario = ' . $this->session->userdata('id_usuario') . ') AND (status = "em_aberto" OR status = "aguardando_encerramento")')->order_by('pendencias.id', 'desc')->limit(1)->get()->row();
		}
		
		if ($pendencia_mais_antiga)
		{
			if (strlen($pendencia_mais_antiga->titulo) > 15)
			{
				$titulo_pendencia_mais_antiga = substr($pendencia_mais_antiga->titulo, 0, 15) . '...';
			}
			else
			{
				$titulo_pendencia_mais_antiga = $pendencia_mais_antiga->titulo;
			}
			
			$dados['pendencias'] .= '<p><strong>Pendência mais antiga:</strong> ' . $titulo_pendencia_mais_antiga . ' - Criada há ' . $this->_obter_descricao_tempo_segundos(time() - $pendencia_mais_antiga->timestamp) . ' - ' . anchor('pendencias/ver_detalhes/' . $pendencia_mais_antiga->id, '+ Opções &raquo;') . '</p>';
		}
		else
		{
			$dados['pendencias'] .= '<p><strong>Pendência mais antiga:</strong> não há</p>';
		}
		echo json_encode($dados);
	}

    function download($upload) {
        $this->load->helper('download');

        force_download($upload, file_get_contents('./uploads/' . $upload));
    }

    function ver_detalhes($id = NULL) {
        if (!$id) {
            redirect('pendencias');
        }

        $pendencia = $this->db->from('pendencias')->where('id', $id)->get()->row();

        if (!$pendencia) {
            redirect('pendencias');
        }

        $usuario = $this->db->from('usuarios')->where('id', $pendencia->id_usuario)->get()->row();
        $usuarios = $this->db->from('usuarios_pendencias')->where('id_pendencia', $pendencia->id)->get()->result();

        if (count($usuarios) == 1) {
            $_usuario = $this->db->from('usuarios')->where('id', $usuarios[0]->id_usuario)->get()->row();
        }

        if ($_POST) {
            if ($this->input->post('conteudo_mensagem')) {
            	$query = $this->db->query("SELECT count(*) as ok FROM mensagens_pendencias WHERE conteudo = '".$this->input->post('conteudo_mensagem')."'");
            	$row = $query->row();
            	if(!$row->ok > 0){
                $this->db->insert('mensagens_pendencias', array(
                    'timestamp' => time(),
                    'id_pendencia' => $pendencia->id,
                    'id_usuario' => $this->session->userdata('id_usuario'),
                    'conteudo' => $this->input->post('conteudo_mensagem')
                ));

                $this->db->update('pendencias', array(
                    'ultima_mensagem_timestamp' => time(),
                    'id_usuario_ultima_mensagem' => $this->session->userdata('id_usuario'),
                    'nome_usuario_ultima_mensagem' => $this->session->userdata('nome_usuario')
                        ), array('id' => $pendencia->id));
            	}
            }

            $conteudo = '
				
				<script type="text/javascript">
				$(document).ready(function() {
        				$("textarea").val("");
				});
				</script>
				<script type="text/javascript">
					$(document).ready(function() {
						$("html, body").animate({ scrollTop: $("#mensagens").offset().top }, 1000);
					});
				</script>	
			';
        }

        $conteudo .= '<div class="caixa"><ul>';
        if ($pendencia->status == 'em_aberto' && $this->session->userdata('id_usuario') == $pendencia->id_usuario) {
            $conteudo .= '<li>' . anchor('pendencias/solicitar_encerramento/' . $pendencia->id, 'Solicitar Encerramento') . '</li>';
        } else if ($pendencia->status == 'aguardando_encerramento' && $this->session->userdata('grupo_usuario') == 'gestores_comerciais') {
            $conteudo .= '<li>' . anchor('pendencias/encerrar/' . $pendencia->id, 'Encerrar Pendência') . '</li>';
        }
        $conteudo .= '<li><a class="colorbox_inline" href="#visualizacoes_pendencia">Consultar Visualizações da Pendência</a></li>';
        $conteudo .= '<li>' . anchor('#', 'Voltar', 'onclick="history.go(-1); return false;"') . '</li>';
        $conteudo .= '</ul></div>';

        $conteudo .= heading('Detalhes da Pendência' . ($pendencia->prazo_solucao ? ' - Prazo para solução: <span style="font-weight: bold; color: #' . (date('Ymd', $pendencia->prazo_solucao_timestamp) >= date('Ymd') ? '060' : '900') . ';">' . $pendencia->prazo_solucao . '</span>' : NULL), 2);

        $conteudo .= heading('Informações Gerais', 3);

        $conteudo .= '
			<table cellspacing="0" class="info">
				<tr>
					<th>Criação:</th>
					<td>' . date('d/m/Y H:i:s', $pendencia->timestamp) . '</td>
				</tr>
				<tr>
					<th>Status:</th>
					<td>' . element($pendencia->status, $this->_obter_status()) . '</td>
				</tr>
			</table>
			
			<table cellspacing="0" class="info">
				<tr>
					<th>Encerramento:</th>
					<td>' . ($pendencia->encerramento_timestamp ? date('d/m/Y H:i:s', $pendencia->encerramento_timestamp) : 'não há') . '</td>
				</tr>
				<tr>
					<th>De:</th>
					<td>' . $usuario->nome . '</td>
				</tr>
			</table>
			
			<table cellspacing="0" class="info">
				<tr>
					<th>Última mensagem:</th>
					<td>' . ($pendencia->ultima_mensagem_timestamp ? date('d/m/Y H:i:s', $pendencia->ultima_mensagem_timestamp) : 'não há') . '</td>
				</tr>
				<tr>
					<th>Para:</th>
					<td>' .(count($usuarios) == 1 ? $_usuario->nome : 'Todos'). '</td>
				</tr>
			</table>
			
			<div style="clear: both;"></div>
			
			<table cellspacing="0" class="info">
				<tr>
					<th>Prioridade:</th>
					<td>' . element($pendencia->prioridade, $this->_obter_prioridades()) . '</td>
				</tr>
				<tr>
					<th>Título:</th>
					<td colspan="3">' . $pendencia->titulo . '</td>
				</tr>
			</table>
			
			<div style="clear: both;"></div>
		';

        $uploads = unserialize($pendencia->uploads);

        if ($uploads) {
            foreach ($uploads as $upload) {
                $arquivos[] = anchor('pendencias/download/' . $upload, $upload);
            }

            $conteudo .= '
				<div style="background-color: #f9f9f9; border: 1px solid #ddd; margin-top: 10px; padding: 0 10px 10px 10px;">
					<p><strong>Arquivos</strong></p>
					
					<p>' . implode(', ', $arquivos) . '</p>
				</div>
				
				<div style="clear: both;"></div>
			';
        }

        $conteudo .= '<div style="background-color: #FFFFE5; border: 1px solid #F90; margin-top: 10px; padding: 10px;">' . $pendencia->descricao . '</div>';

        
     $conteudo .= br().'<div id="mensagem">'.heading('Mensagens:', 3);

        $mensagens = $this->db->from('mensagens_pendencias')->where('id_pendencia', $pendencia->id)->order_by('id','desc')->get()->result();

        $conteudo.="<div id='caixa_dialogo'>";
        if ($mensagens) {
            $i = count($mensagens);

            foreach ($mensagens as $mensagem) {
                $usuario = $this->db->from('usuarios')->where('id', $mensagem->id_usuario)->get()->row();

                $conteudo .= '
					<div style="border-top: 1px dotted #DDD; margin-top: 10px; padding-top: 10px;">
						<h4>' . $usuario->nome . ' disse (' . date('d/m/Y H:i:s', $mensagem->timestamp) . '):</h4>
						<div style="margin-top: 10px;">' . $mensagem->conteudo . '</div>
					</div>
				';

                $i--;
            }
        } else {
            $conteudo .= '<p>Não há mensagens para exibir.</p>';
        }
        $conteudo .= '</div>';         
       
        
        if ($pendencia->status != 'encerrada') {
            $conteudo .= '<div class="nao_exibir_impressao">';

            $conteudo .= form_open(current_url());
            $conteudo .= '<p>' . form_textarea(array('class' => 'editor', 'cols' => 40, 'rows' => 1, 'name' => 'conteudo_mensagem', 'value' => $this->input->post('conteudo_mensagem'))) . '</p>';
            $conteudo .= '<p>' . form_submit('enviar', 'Enviar') . '</p>';
            $conteudo .= form_close();

            $conteudo .= '</div></div>';
        }

       

        $conteudo .= '<div style="display: none;"><div id="visualizacoes_pendencia">';

        $conteudo .= '<h3 style="margin: 0;">Visualizações da Pendência</h3>';

        $_usuarios = array($pendencia->id_usuario);
        foreach ($usuarios as $usuario) {
            $_usuarios[] = $usuario->id_usuario;
        }

        $acessos_usuarios = $this->db->from('acessos_usuarios')->where(array('timestamp >=' => $pendencia->timestamp, 'timestamp <=' => $pendencia->encerramento_timestamp ? $pendencia->encerramento_timestamp : time()))->where_in('id_usuario', $_usuarios)->get()->result();
        $conteudo .= '<div style="width: 600px; overflow: hidden;"><table cellspacing="0" class="novo_grid" style="margin-top: 10px;"><thead><tr><th>Data</th><th>Usuário</th></tr></thead><tbody>';
        foreach ($acessos_usuarios as $acesso_usuario) {
            $usuario = $this->db->from('usuarios')->where('id', $acesso_usuario->id_usuario)->get()->row();

            $conteudo .= '<tr><td>' . date('d/m/Y H:i:s', $acesso_usuario->timestamp) . '</td><td>' . $usuario->nome . '</td></tr>';
        }
        $conteudo .= '</tbody></table></div>';

        $conteudo .= '</div></div>';

        $this->load->view('layout', array('conteudo' => $conteudo));
    }

    function index() {
        /*
          if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros'))) {
          $pendencias = $this->db->from('pendencias')->get()->result();
          }
          else
          {
          $pendencias = $this->db->from('pendencias')->join('usuarios_pendencias', 'usuarios_pendencias.id_pendencia = pendencias.id')->where('pendencias.id_usuario = ' . $this->session->userdata('id_usuario') . ' OR usuarios_pendencias.id_usuario = ' . $this->session->userdata('id_usuario'))->get()->result();
          }



          foreach ($pendencias as $chavePendencia =>$pendencia){
          $usuario = $this->db->from('usuarios')->where('id', $pendencia->id_usuario)->get()->row();
          $usuarios = $this->db->from('usuarios_pendencias')->where('id_pendencia', $pendencia->id)->get()->result();

          if (count($usuarios) == 1)
          {
          $_usuario = $this->db->from('usuarios')->where('id', $usuarios[0]->id_usuario)->get()->row();
          }

          foreach ($usuarios as $_usuario)
          {
          $_usuario = $this->db->from('usuarios')->where('id', $_usuario->id_usuario)->get()->row();

          $_usuarios[] = $_usuario->nome;


          $pendencia->uploads = unserialize($pendencia->uploads) ? img(base_url().'misc/icones/attach.png') : '&nbsp;';
          $pendencia->prioridade = element($pendencia->prioridade, $this->_obter_prioridades());
          }

          ###
          ###
          $total_de_pendencias =  $this->db->from('pendencias')->where('status !=', 'convertido_cliente')->get()->num_rows();
          $resultados_por_pagina = 20;

         */

		
        $filtros = array(
            array(
                'nome' => 'status',
                'descricao' => 'Status',
                'tipo' => 'opcoes',
                'opcoes' => array('todos' => 'Todas', 'em_aberto' => 'Em Aberto', 'aguardando_encerramento' => 'Aguardando Encerramento', 'encerrada' => 'Encerrada'),
                'campo_mysql' => 'pendencias.status',
                'ordenar' => 0
            ),
            array(
                'nome' => 'criacao',
                'descricao' => 'Criação',
                'tipo' => 'data',
                'campo_mysql' => 'pendencias.timestamp',
                'ordenar' => 2
            ),
            array(
                'nome' => 'titulo',
                'descricao' => 'Título',
                'tipo' => 'texto',
                'campo_mysql' => 'pendencias.titulo',
                'ordenar' => 3
            ),
            array(
                'nome' => 'prazo_solucao',
                'descricao' => 'Prazo Solução',
                'tipo' => 'data',
                'campo_mysql' => 'pendencias.prazo_solucao_timestamp',
                'ordenar' => 4
            ),
            array(
                'nome' => 'prioridade',
                'descricao' => 'Prioridade',
                'tipo' => 'opcoes',
                'opcoes' => array('todos' => 'Todas', 'baixa' => 'Baixa', 'media' => 'Média', 'alta' => 'Alta'),
                'campo_mysql' => 'pendencias.prioridade',
                'ordenar' => 5
            ),
            array(
                'nome' => 'de',
                'descricao' => 'De',
                'tipo' => 'texto',
                'campo_mysql' => 'pendencias.nome_real_usuario',
                'ordenar' => 6
            ),
            array(
                'nome' => 'para',
                'descricao' => 'Para',
                'tipo' => 'texto',
                'campo_mysql' => 'usuarios.nome_real',
                'ordenar' => 7
            ),
            array(
                'nome' => 'ultima_mensagem_timestamp',
                'descricao' => 'Última Mensagem',
                'tipo' => 'data',
                'campo_mysql' => 'pendencias.ultima_mensagem_timestamp',
                'ordenar' =>8
            )
        );
		if($_GET['my_submit'])
		{
			// caso o usuário logado for um representante, vamos mostrar somente os prospects que ele mesmo fez
			if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')))
			{
				$this->db->where('(pendencias.id_usuario = ' . $this->session->userdata('id_usuario') . ' OR usuarios_pendencias.id_usuario = ' . $this->session->userdata('id_usuario') . ')');
			}
	 
		
				$this->filtragem_mysql($filtros);
				
				$this->db->select('usuarios.status as usuario_status, pendencias.*,GROUP_CONCAT(usuarios.nome_real) as para');
				$this->db->from('pendencias');
				$this->db->join('usuarios_pendencias', 'usuarios_pendencias.id_pendencia = pendencias.id');
				$this->db->join('usuarios', 'usuarios.id = usuarios_pendencias.id_usuario');
				$this->db->group_by('usuarios_pendencias.id_pendencia');
				$pendencias = $this->db->order_by('prazo_solucao_timestamp','desc')->limit(20, $this->input->get('per_page'))->get()->result();
				
				//print_r($pendencias);
				//$pendencias = $this->db->select('usuarios.statu as usuario_status, pendencias.*')->from('pendencias')->join('usuarios', 'usuarios.id = pendencias.id_usuario', 'inner')->order_by('prazo_solucao_timestamp','desc')->limit(20, $this->input->get('per_page'))->get()->result();

				//echo $this->db->last_query();die();
				
			// caso o usuário logado for um representante, vamos mostrar somente os prospects que ele mesmo fez
				if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')))
				{
					$this->db->where('(pendencias.id_usuario = ' . $this->session->userdata('id_usuario') . ' OR usuarios_pendencias.id_usuario = ' . $this->session->userdata('id_usuario') . ')');
				}
				$this->filtragem_mysql($filtros);
				
				//$total = $this->db->select('usuarios.status as usuario_status, pendencias.*')->from('pendencias')->join('usuarios', 'usuarios.id = pendencias.id_usuario', 'inner')->where('pendencias.id >', 0)->get()->num_rows();
				$this->db->select('usuarios.status as usuario_status, pendencias.*');
				$this->db->from('pendencias');
				$this->db->join('usuarios_pendencias', 'usuarios_pendencias.id_pendencia = pendencias.id', 'inner');
				$this->db->group_by('pendencias.id');
				$total = $this->db->join('usuarios', 'usuarios.id = usuarios_pendencias.id_usuario', 'inner')->get()->num_rows();;
				
				
				$paginacao = $this->paginacao($total);


				foreach ($pendencias as $chavePendencia => $pendencia) {
					$usuario = $this->db->from('usuarios')->where('id', $pendencia->id_usuario)->get()->row();
				   
						$pendencia->id_usuario =  $usuario->nome;
						$pendencia->uploads = unserialize($pendencia->uploads) ? img(base_url() . 'misc/icones/attach.png') : NULL;
						//$pendencia->prioridade = element($pendencia->prioridade, $this->_obter_prioridades());
					
					
					
				}
		}
        $this->load->view('layout', array('conteudo' => $this->load->view('pendencias/index', array('pendencias' => $pendencias, 'filtragem' => $this->filtragem($filtros), 'paginacao' => $paginacao, 'usuario' => $_usuarios, 'total'=>$total), TRUE)));
    }

    function criar() {
        // ** VALIDAR FORM
		
        if ($_POST) {
            if (!$this->input->post('para')) {
                $erro = 'Selecione um usuário.';
            } else if (!$this->input->post('titulo')) {
                $erro = 'Digite um título.';
            } else if (!$this->input->post('prazo_solucao')){
				$erro = 'Digite um prazo de solução';
            } else if(!$this->_validar_data($this->input->post('prazo_solucao'))){
                $erro = 'Digite uma data válida para o campo Prazo da solução.';
			}else if (!$this->input->post('descricao')) {
                $erro = 'Digite uma descrição.';
            } else {
                $upload = hnordt_gerar_upload();

                if (is_string($upload)) {
                    $erro = $upload;
                } else {
                    if ($upload) {
                        foreach ($upload as $_up) {
                            $uploads[] = $_up['file_name'];
                        }
                    }

                    // criar pendência
                    $this->db->insert('pendencias', array(
                        'timestamp' => time(),
                        'id_usuario' => $this->session->userdata('id_usuario'),
						'nome_real_usuario' => $this->session->userdata('nome_real_usuario'),
                        'status' => 'em_aberto',
                        'prioridade' => $this->input->post('prioridade'),
                        'titulo' => $this->input->post('titulo'),
                        'descricao' => $this->input->post('descricao'),
                        'prazo_solucao' => $this->input->post('prazo_solucao'),
                        'prazo_solucao_timestamp' => hnordt_converter_data_timestamp($this->input->post('prazo_solucao')),
                        'uploads' => serialize($uploads)
                    ));

                    $id_pendencia = $this->db->insert_id();

                    // adicionar recebedores
                    if ($this->input->post('para') != 'todos') {
                        $this->db->insert('usuarios_pendencias', array(
                            'id_pendencia' => $id_pendencia,
                            'id_usuario' => $this->input->post('para')
                        ));
                    } else {
                        $usuarios = $this->_obter_usuarios();

                        foreach ($usuarios as $id_usuario => $usuario) {
                            if (!$id_usuario) {
                                continue;
                            }

                            $this->db->insert('usuarios_pendencias', array(
                                'id_pendencia' => $id_pendencia,
                                'id_usuario' => $id_usuario
                            ));
                        }
                    }

                    redirect('pendencias');
                }
            }
        }
        // VALIDAR FORM **
        // ** EXIBIR FORM
		$conteudo .= '<script type="text/javascript">
						function mostra_esconde(){
							if($("input[name=para]:checked").val() == \'usuario_especifico\'){
								$("#id_usuario").show();
							}else{
								$("#id_usuario").hide();
							}
						}	
							
						$(document).ready(function(){
							mostra_esconde();
							$("input[name=para]").change(mostra_esconde);
						});
						
						
							
					</script>';
		
        $conteudo .= heading('Criar Pendência', 2);
        if ($erro)
            $conteudo .= '<p class="erro">' . $erro . '</p>';
        $conteudo .= form_open_multipart(current_url());
        //$conteudo .= '<p>Para:' . br() . form_label(form_radio('para', 'todos', !$this->input->post('para') || $this->input->post('para') == 'todos' ? TRUE : FALSE) . ' Todos') . br() . form_label(form_radio('para', 'usuario_especifico', $this->input->post('para') == 'usuario_especifico' ? TRUE : FALSE) . ' Usuário específico: ') . form_input('usuario', $this->input->post('usuario'), 'alt="obter_usuarios" class="autocomplete" size="40" data-url="'.site_url('pendencias/obter_usuarios' ).'"\'') . form_hidden('id_usuario', $this->input->post('id_usuario')) . '</p>';
        //$conteudo .= '<p>Para:' . br() . form_label(form_radio('para', 'todos', !$this->input->post('para') || $this->input->post('para') == 'todos' ? TRUE : FALSE) . ' Todos') . br() . form_label(form_radio('para', 'usuario_especifico', $this->input->post('para') == 'usuario_especifico' ? TRUE : FALSE) . ' Usuário específico: ') . form_input('id_usuario', $this->input->post('id_usuario'), 'alt="obter_usuarios" class="autocomplete" size="40" data-url="'.site_url('usuarios/obter_usuarios').'"\''). form_hidden('id_usuario', $this->input->post('id_usuario')) . '</p>';
		//$conteudo .=  '<p>Para:' . br() . form_label(form_radio('para', 'todos', !$this->input->post('para') || $this->input->post('para') == 'todos' ? TRUE : FALSE) . ' Todos') . br() . form_label(form_radio('para', 'usuario_especifico', $this->input->post('para') == 'usuario_especifico' ? TRUE : FALSE) . ' Usuário específico: ') . '<input id="id_usuario" type="text" style="width: 323px" name="id_usuario" class="autocomplete" value="'.$this->input->post("_id_usuario").'" alt="'.$this->input->post("id_usuario").'" data-url="'.site_url('usuarios/obter_usuarios').'"></p>';
		$conteudo .= '<p>Para:'.form_dropdown('para', $this->obter_destinatarios(),$this->input->post('para')).'</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Prioriade:' . br() . form_dropdown('prioridade', $this->_obter_prioridades(), $this->input->post('prioridade'))) . '</p>';
        $conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Título:' . br() . form_input('titulo', $this->input->post('titulo'), 'size="60"')) . '</p>';
        $conteudo .= '<div style="clear: both;"></div>'; 

        $conteudo .= hnordt_gerar_campos_upload();

        $conteudo .= '<p>' . form_label('Prazo para solução:' . br() . form_input('prazo_solucao', $this->input->post('prazo_solucao'), 'size="6" class="datepicker_data_minima_hoje"')) . '</p>';

        //$conteudo .= '<p>' . 'Descrição:' . br() . '<div dojoType="dijit.Editor" extraPlugins="[\'foreColor\', \'fontSize\']" onchange="$(\'input[name=descricao]\').val(arguments[0]);" style="height: 200px;">' . $this->input->post('descricao') . '</div>' . form_hidden('descricao', $this->input->post('descricao')) . '</p>';
        $conteudo .= '<p>' . form_label('Conteúdo:' . br() . form_textarea(array('class' => 'editor', 'cols' => 60, 'rows' => 8, 'name' => 'descricao', 'value' => $this->input->post('descricao')))) . '</p>';
        $conteudo .= '<p>' . form_submit('concluir', 'Concluir') . ' ou ' . anchor('pendencias', 'Cancelar') . '</p>';
        $conteudo .= form_close();
        // EXIBIR FORM **

        $this->load->view('layout', array('conteudo' => $conteudo));
    }

    function solicitar_encerramento($id = NULL) {
        // TODO: permissões

        if (!$id) {
            redirect('pendencias');
        }

        $pendencia = $this->db->from('pendencias')->where('id', $id)->get()->row();

        if (!$pendencia || $pendencia->status != 'em_aberto') {
            redirect('pendencias');
        }

        $this->db->update('pendencias', array('status' => 'aguardando_encerramento'), array('id' => $pendencia->id));

        redirect('pendencias/ver_detalhes/' . $pendencia->id);
    }

    function encerrar($id = NULL) {
        // TODO: permissões

        if (!$id) {
            redirect('pendencias');
        }

        $pendencia = $this->db->from('pendencias')->where('id', $id)->get()->row();

        if (!$pendencia || $pendencia->status != 'aguardando_encerramento') {
            redirect('pendencias');
        }

        $this->db->update('pendencias', array('status' => 'encerrada', 'encerramento_timestamp' => time()), array('id' => $pendencia->id));

        redirect('pendencias/ver_detalhes/' . $pendencia->id);
    }

    //

    function _obter_status() {
        return array('em_aberto' => 'Em Aberto', 'aguardando_encerramento' => 'Aguardando Encerramento', 'encerrada' => 'Encerrada');
    }

    function _obter_prioridades() {
        return array('baixa' => 'Baixa', 'media' => 'Média', 'alta' => 'Alta');
    }

    function _obter_usuarios() {
        $usuarios = $this->db->from('usuarios')->where('status', 'ativo')->order_by('grupo', 'desc')->order_by('nome', 'asc')->get()->result();
        $_usuarios = array(0 => 'Todos');

        foreach ($usuarios as $usuario) {
            $_usuarios[$usuario->id] = $usuario->nome . ' (' . element($usuario->grupo, $this->_obter_grupos()) . ')';
        }

        return $_usuarios;
    }
	
	function obter_destinatarios() {
			
		$_usuarios[0] = 'Selecione...';	
		
		$this->db->from('usuarios')->where(array('status'=> 'ativo'))->where_in('grupo',array('gestores_comerciais', 'supervisores'));
       
		if($this->session->userdata('grupo_usuario') <> 'representantes')
		{
			$this->db->or_where_in('grupo', 'representantes');
			$_usuarios['Todos'] = array('todos' => 'Todos');
		}
		$usuarios = $this->db->order_by('grupo', 'desc')->order_by('nome', 'asc')->get()->result();
		
		

        foreach ($usuarios as $usuario) {
			if($usuario->grupo == 'gestores_comerciais')
			{
				$usuario->grupo = 'Gestores Comerciais';
			}else if($usuario->grupo == 'supervisores')
			{
				$usuario->grupo = 'Supervisores';
			}else{
				$usuario->grupo = 'Representantes';
			}
            $_usuarios[$usuario->grupo] [$usuario->id] = ucwords($usuario->nome);
        }

        return $_usuarios;
    }

    function _obter_grupos() {
        return array('administradores' => 'Administradores', 'gestores_comerciais' => 'Gestores Comerciais', 'gestores_financeiros' => 'Gestores Financeiros', 'representantes' => 'Representantes', 'prepostos' => 'Prepostos', 'supervisores' => 'Supervisores');
    }
	
	

}