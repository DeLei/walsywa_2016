<?php
class Clientes extends MY_Controller 
{
	
	//Define o tempo em que o cliente se torna inativo não realizando compra neste periodo
	private $periodo_atividade;
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->library("mascara");
		
		$this->periodo_atividade = date('Ymd', strtotime('-6 months'));
	}
	
	function editar_codigo_usuario($novo_codigo = NULL)
	{
		$this->session->set_userdata('codigo_usuario_clientes', $novo_codigo);
		
		redirect('clientes');
	}
	
	function index(){
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->config->load('db_cliente_' . $this->db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');
		
		$filtros = array(
			in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros','engenharia')) ? array(
				'nome' => 'representante',
				'descricao' => 'Representante',
				'tipo' => 'texto',
				'campo_mssql' => 'A3_NOME',
				'ordenar_ms' => 1
			) : NULL,
			array(
				'nome' => 'codigo',
				'descricao' => 'Código',
				'tipo' => 'faixa_codigo',
				'campo_mssql' => 'A1_COD',
				'ordenar_ms' => 2
			),
			array(
				'nome' => 'loja',
				'descricao' => 'Loja',
				'tipo' => 'texto',
				'campo_mssql' => 'A1_LOJA',
				'ordenar_ms' => 3
			),
			array(
				'nome' => 'nome',
				'descricao' => 'Nome',
				'tipo' => 'texto',
				'campo_mssql' => 'A1_NOME',
				'ordenar_ms' => 4
			),
			array(
				'nome' => 'fantasia',
				'descricao' => 'Nome fantasia',
				'tipo' => 'texto',
				'campo_mssql' => 'A1_NREDUZ',
				'ordenar_ms' => 5
			),
			array(
				'nome' => 'cpf',
				'descricao' => 'CPF/CNPJ(Números)',
				'tipo' => 'texto',
				'campo_mssql' => 'A1_CGC'
			),
			array(
				'nome' => 'telefone',
				'descricao' => 'Telefone',
				'tipo' => 'texto',
				'campo_mssql' => 'A1_TEL'
			),
			array(
				'nome' => 'cep',
				'descricao' => 'CEP',
				'tipo' => 'cep',
				'campo_mssql' => 'A1_CEP',
			),
                    
                //CUSTOM: MPD-220 - 07/08/2013
			array(
				'nome' => 'ultimo_pedido',
				'descricao' => 'Última Compra',
				'tipo' => 'data',
				'campo_mssql' => 'A1_ULTCOM',
				'ordenar_ms' => 6
			),
			array(
				'nome' => 'status',
				'descricao' => 'Status',
				'tipo' => 'opcoes',
				'opcoes' => array('todos' => 'Todos', 'ativo' => 'Ativo', 'inativo' => 'Inativo')
			)
                //FIM CUSTOM: MPD-220 - 07/08/2013
                    
		);
		
		if($_POST['my_submit'] || $_GET['my_submit']){

            /*
				Consulta de clientes obrigava representante a preencher todos os filtros, o código abaixo foi comentado para que isso não ocorresse mais.
			
			if (($this->session->userdata('grupo_usuario') != 'gestores_comerciais') and ((!$_GET['codigo']) and (!$_GET['loja']) and (!$_GET['nome']) 
			and (!$_GET['fantasia']) and (!$_GET['cpf']) and (!$_GET['telefone']))){
				$erro = "Realize um filtro para efetuar a pesquisa.";
			}else{ */
                            
                                //DATA INICIAL NÃO PODE SER MAIOR QUE A FINAL
                                if($this->input->get('ultimo_pedido_inicial') > $this->input->get('ultimo_pedido_final'))
                                {
                                    $erro = "A Data Inicial da ÚLTIMA COMPRA deve ser menor do que a Data Final";
                                }
                    
                    //CUSTOM: ORDENAÇÃO DECRESCENTE DA ÚLTIMA COMPRA, QUANDO EXISTIR ESSE FILTRO - 20/08/2013
                                if(!$this->input->get('ordenar_ms') AND ($this->input->get('ultimo_pedido_inicial') OR $this->input->get('ultimo_pedido_final')))
                                {
					$_GET['ordenar_ms'] = 'A1_ULTCOM';
					$_GET['ordenar_ms_tipo'] = 'desc';                                    
                                }
                    //FIM CUSTOM: ORDENAÇÃO DECRESCENTE DA ÚLTIMA COMPRA, QUANDO EXISTIR ESSE FILTRO - 20/08/2013
                            
				// para que a paginação no mssql funcione, devemos ordenar de alguma forma, caso o usuário não escolher uma ordenação, vamos fazer manualmente
				else if(!$this->input->get('ordenar_ms'))
				{
					// vamos setar o $_GET ao invés de usar o order_by()
					// usando o order_by a flexinha no layout indicando o que está sendo ordenado não apareceria para o usuário
					$_GET['ordenar_ms'] = 'A1_COD';
					$_GET['ordenar_ms_tipo'] = 'asc';
				}
				
				// ** FILTRAGEM PARA EXIBIÇÃO DOS RESULTADOS
				
				//Se o usuário logado não for gestor comercial, listará apenas os clientes cadastrados a menos de 1 ano
				if ($this->session->userdata('grupo_usuario') != 'gestores_comerciais'){
					$this->db_cliente->where($this->_db_cliente['campos']['clientes']['ultima_compra']. ' <=', date('Ymd'));
					$this->db_cliente->where($this->_db_cliente['campos']['clientes']['ultima_compra']. ' >=', date('Ymd', strtotime("-1 year", time())));
				}
				
				// caso o usuário logado for um representante, vamos mostrar somente os pedidos que ele mesmo fez
				if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros','engenharia','supervisores')))
				{
					$this->db_cliente->where('A1_VEND', $this->session->userdata('codigo_usuario'));
				}else if($this->session->userdata('grupo_usuario') == 'supervisores'){
					$representantes = $this->db->select('codigo_representante')->from('representantes_supervisores')->where('id_supervisor', $this->session->userdata('id_usuario'))->get()->result();
					foreach($representantes as $rep){
						$reps[] = $rep->codigo_representante;
					}
					$reps[] = $this->session->userdata('codigo_usuario');
					$this->db_cliente->where_in('A1_VEND', $reps);
				}
				
				
				if($this->input->get('status') != 'todos'){
					if($this->input->get('status') == 'ativo'){
						$this->db_cliente->where('A1_ULTCOM  >= "' . $this->periodo_atividade . '"');
					} else {
						$this->db_cliente->where('(A1_ULTCOM <= "' . $this->periodo_atividade . '" OR A1_ULTCOM IS NULL OR A1_ULTCOM = \'\')');
					}
				}
				$this->filtragem_mssql($filtros);
                //CUSTOM: MPD-220 - 07/08/2013
				
				$this->db_cliente->select(
					'CASE 
						WHEN A1_ULTCOM <= "' . $this->periodo_atividade . '" THEN \'INATIVO\'
						WHEN A1_ULTCOM IS NULL OR A1_ULTCOM = \'\' THEN \'INATIVO\'
						ELSE \'ATIVO\'
					END STATUS'
				);
				
				$clientes = $this->db_cliente->select('A1_COD, A1_LOJA, A1_NOME, A1_NREDUZ, A1_CGC, A1_TEL, A3_NOME, A1_ULTCOM')
                //FIM CUSTOM: MPD-220 - 07/08/2013
				
				->join($this->_db_cliente['tabelas']['representantes'], 'A3_COD = A1_VEND')
				->from($this->_db_cliente['tabelas']['clientes'])->where(array(
					$this->_db_cliente['tabelas']['clientes'] . '.D_E_L_E_T_ !=' => '*',
					$this->_db_cliente['tabelas']['representantes'] . '.D_E_L_E_T_ !=' => '*',
				))
				->limit(15, $this->input->get('per_page'))
				->get()
				->result_array();
								
				//debug_pre($this->db_cliente->last_query());	
				
				// FILTRAGEM PARA EXIBIÇÃO DOS RESULTADOS **
                                
				// ** FILTRAGEM PARA PAGINAÇÃO
				
				// o campo status precisa ser filtrado "manualmente" pois ele não existe no banco de dados
				// caso o usuário logado for um representante, vamos mostrar somente os pedidos que ele mesmo fez
				
				/*
				if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros','engenharia','supervisores')))
				{
					$this->db_cliente->where('A1_VEND', $this->session->userdata('codigo_usuario'));
				}else if($this->session->userdata('grupo_usuario') == 'supervisores'){
					$representantes = $this->db->select('codigo_representante')->from('representantes_supervisores')->where('id_supervisor', $this->session->userdata('id_usuario'))->get()->result();
					foreach($representantes as $rep){
						$reps[] = $rep->codigo_representante;
					}
					$reps[] = $this->session->userdata('codigo_usuario');
					$this->db_cliente->where_in('A1_VEND', $reps);
				}
				
				$this->filtragem_mssql($filtros, FALSE);
				
				$total = $this->db_cliente->select("COUNT(*) as total")->from($this->_db_cliente['tabelas']['clientes'])->join($this->_db_cliente['tabelas']['representantes'], 'A3_COD = A1_VEND')->where(array(
							$this->_db_cliente['tabelas']['clientes'] . '.D_E_L_E_T_ !=' => '*',
							$this->_db_cliente['tabelas']['representantes'] . '.D_E_L_E_T_ !=' => '*',
						))->get()->row();
				$total = $total->total;
				*/
				
				// FILTRAGEM PARA PAGINAÇÃO **
			//}
		}
		
		$this->load->view('layout', array('conteudo' => $this->load->view('clientes/index', array(
			'clientes' => $clientes,
			'erro' => $erro,
			//'total' => $total, 
			'filtragem' => $this->filtragem($filtros), 
			//'paginacao' => $this->paginacao($total)
		), TRUE)));
	}

	function valida_acesso($codigo_cliente){
		$acessos = $this->db->select()
							->from('controle_acessos_clientes')
							->where('id_usuario', $this->session->userdata('id_usuario'))
							->where('dia_acesso', date('Y-m-d'))
							->get()->num_rows();
							
		$cliente = $this->db->select()
						   ->from('controle_acessos_clientes')
						   ->where('codigo_cliente', $codigo_cliente)
						   ->where('id_usuario', $this->session->userdata('id_usuario'))
						   ->where('dia_acesso', date('Y-m-d'))
						   ->get()->num_rows();
		if (!$cliente){
			if($acessos < 15){
				$this->db->insert('controle_acessos_clientes',
							array('id_usuario' => $this->session->userdata('id_usuario'),
								  'dia_acesso' => date('Y-m-d'),
								  'codigo_cliente' => $codigo_cliente));
			}else{
				redirect('clientes/index?erro=esgotado');
			}
		}
	}
	
	function ver_detalhes($codigo = NULL, $loja = NULL)
	{
		
		if (!isset($codigo) || !isset($loja))
		{
			redirect();	
		}
		
		if ($this->session->userdata('grupo_usuario') != 'gestores_comerciais'){
			$this->valida_acesso($codigo);
		}
		
		/* Regra em que apenas pode ser visto 15x o cadastro de clientes no dia */
		$visualizacao_clientes = $this->db
			->from('visualizacao_clientes')
			->where('id_usuario', $this->session->userdata('id_usuario'))
			->get()->row();
		
		if ($visualizacao_clientes) {
			if ($visualizacao_clientes->data_visualizacao == date('Ymd', time())) {
				if ($visualizacao_clientes->visualizacoes_clientes >= 15) {
					redirect('clientes');	
				}
				$this->db
					->where('id_usuario', $this->session->userdata('id_usuario'))
					->update('visualizacao_clientes', array(
						'visualizacoes_clientes ' => $visualizacao_clientes->visualizacoes_clientes + 1,
					));
			} else {
				$this->db->delete('visualizacao_clientes', array('id_usuario' => $this->session->userdata('id_usuario')));
				$this->db->insert('visualizacao_clientes', 
					array(
					'id_usuario' => $this->session->userdata('id_usuario'), 
					'visualizacoes_clientes' => 1,
					'data_visualizacao' => date('Ymd', time()),
					));
			}
		} else {
			$this->db->insert('visualizacao_clientes', 
				array(
				'id_usuario' => $this->session->userdata('id_usuario'), 
				'visualizacoes_clientes' => 1,
				'data_visualizacao' => date('Ymd', time()),
				));
		}
		
		// TODO: representantes/prepostos só podem ver SEUS clientes
		$cliente = $this->db_cliente->obter_cliente($codigo, $loja);
		
		if (!$cliente)
		{
			redirect();	
		}
		
		$conteudo = '
			<div class="caixa">
				<ul>
					<li>' . anchor('#', 'Voltar', 'onclick="history.go(-1); return false;"') . '</li>';
		if(!$this->session->userdata('grupo_usuario')=='engenharia')
		{		
			$conteudo .= '
					<li>' . anchor('criar_pedidos/informacoes_cliente/pedido/' . $cliente['codigo_representante'] . '/0/0/0/' . $cliente['codigo'] . '-' . $cliente['loja'], 'Cadastrar Pedido') . '</li>
					<li>' . anchor('criar_pedidos/informacoes_cliente/orcamento/' . $cliente['codigo_representante'] . '/0/0/0/' . $cliente['codigo'] . '-' . $cliente['loja'], 'Cadastrar Orçamento') . '</li>
					<li>' . anchor('pedidos/?cliente=' . utf8_encode($cliente['nome']).'&my_submit=Filtrar#filtros', 'Consultar Pedidos') . '</li>
					<li>' . anchor('consultas/notas_fiscais/?cliente=' . utf8_encode($cliente['nome']).'&my_submit=Filtrar#filtros', 'Consultar Notas Fiscais') . '</li>
					<li>' . anchor('consultas/titulos_vencidos/?cliente=' . utf8_encode($cliente['nome']).'&my_submit=Filtrar#filtros', 'Consultar Títulos Vencidos').'</li>
					' . ($cliente['ultimo_pedido']['codigo'] ? '<li>' . anchor('pedidos/ver_detalhes/' . $cliente['ultimo_pedido']['codigo'].'&my_submit=Filtrar#filtros', 'Detalhes do Último Pedido') . '</li>' : NULL);
		}
		$conteudo .= '<li>' . anchor('compromissos/ver_agendamentos/cliente/' . $codigo . '/' . $loja, 'Ver Todos os Agendamentos') . '</li>
					  <li>' . anchor('clientes/atendimentos/' . $codigo, 'Consultar Atendimentos') . '</li>	
				</ul>
			</div>
		';
		
		//Obter Classes
		$classes = $this->db->select()->from("descontos_classe")->get()->result();
		foreach($classes as $classe){
			$listagem[$classe->codigo_classe] = $classe->nome_classe;
		}
		//------
		
		$conteudo .= heading('Detalhes do Cliente', 2);
		
		//Cor do Status
		if($cliente['status'] == 'ativo')
		{
			$cor_status = 'green';
		}
		else
		{
			$cor_status = 'red';
		}
		
		$conteudo .= '<div style="margin-top:15px"><strong style="font-size:15px">' . $cliente['codigo'] . ' - ' . utf8_encode($cliente['nome']) . '</strong> (' . $this->mascara->formatar_cnpj_cpf($cliente['cpf']) . ') <span style="color:' . $cor_status . '">(' . strtoupper($cliente['status']) . ')</span></div>';
		$conteudo .= '<hr />';
		
		//$conteudo .= '<table><tr><td valign="top">';
		
		$conteudo .= '
			<table cellspacing="0" class="info" style="font-size: 11px">
				<tr>
					<th>CPF/CNPJ:</th>
					<td>' . $this->mascara->formatar_cnpj_cpf($cliente['cpf']) . '</td>
					
					<th>Desde:</th>
					<td>' . ($cliente['data_cadastro_timestamp'] ? date('d/m/Y', $cliente['data_cadastro_timestamp']) : 'não há') . '</td>
					
					<th>Loja:</th>
					<td>' . $cliente['loja'] . '</td>
				</tr>
				
				<tr>
					<th>Endereço:</th>
					<td>'; 
		$conteudo .= utf8_encode($cliente['endereco']);
		
		if($cliente['numero'])
		{
			$conteudo .= ', ' . $cliente['numero'];
		}
		
		$conteudo .= '</td>
					
					<th>Bairro:</th>
					<td colspan="3">' . utf8_encode($cliente['bairro']) . '</td>

				</tr>
				
				<tr>
					<th>Cidade:</th>
					<td>' . utf8_encode($cliente['cidade']) . '</td>
					
					<th>Estado:</th>
					<td colspan="3">' . $cliente['estado'] . '</td>
				</tr>
				
				<tr>
					<th>Telefone:</th>
					<td>' . $cliente['telefone'] . '</td>
					
					<th>CEP:</th>
					<td colspan="3">' . $cliente['cep'] . '</td>
				</tr>
				
				
				<tr>
					<th>Classe:</th>
					<td>' .	($cliente['categoria'] ? element($cliente['categoria'], $listagem) : 'Sem Classe') . '</td>
					
					<th>E-mail:</th>
					<td colspan="3">' . mailto($cliente['email']) . '</td>
				</tr>';
		/*
		echo '<pre>';
		print_r($cliente);
		echo '</pre>';
		*/
		$conteudo .= '<tr><td colspan="6">
		<script type="text/javascript">
			$(document).ready(function(){
				$("#mapa").goMap({
					address: "' . utf8_encode($cliente['endereco']) . ', ' . utf8_encode($cliente['cidade']) . ' - ' . $cliente['estado'] . '",
					zoom: 15, 
					maptype: "ROADMAP"
				});
				
				$.goMap.createMarker({  
						address: "' . utf8_encode($cliente['endereco']) . ', ' . utf8_encode($cliente['cidade']) . ' - ' . $cliente['estado'] . '"
				}); 
				
			});
		</script>
		<div id="mapa" style="height:150px; width:100%; float:left; border:1px solid #CCCCCC; margin-top: 10px"></div>';
		
		$conteudo .= '</td></tr></table>';
		
		$conteudo .= '<br /><div style="clear:left"></div>';
		
$conteudo .= '<table>
		<tr><th style="padding-top: 0px;">';
		
			$conteudo .= '<h3 style="padding-left:1px;float:left">Informações Financeiras<h3>';//heading('Informações Financeiras', 3);
			if ($cliente['limite_credito'] > 0)
			{
				$limite_credito = '<strong style="color: #060;">R$ ' . number_format($cliente['limite_credito'], 2, ',', '.') . '</strong>';
			}
			else
			{
				$limite_credito = '<strong style="color: #900;">R$ ' . number_format($cliente['limite_credito'], 2, ',', '.') . '</strong>';
			}
			
			if ($cliente['total_titulos_vencidos'] <= 0)
			{
				$titulos_vencidos = '<strong style="color: #060;">R$ ' . number_format($cliente['total_titulos_vencidos'], 2, ',', '.') . '</strong>';
			}
			else
			{
				$titulos_vencidos = '<strong style="color: #900;">R$ ' . number_format($cliente['total_titulos_vencidos'], 2, ',', '.') . '</strong>';
			}
			
			$limite_disponivel = $cliente['total_titulos_em_aberto'] - $cliente['limite_credito'];
			
			$conteudo .= '
				<table cellspacing="0" class="info" style="font-size: 11px">
					<tr>
						<th>Limite de crédito:</th>
						<td>R$ ' . $limite_credito . '</td>
					</tr>
					<tr>
						<th>Títulos em aberto:</th>
						<td>R$ ' . number_format($cliente['total_titulos_em_aberto'], 2, ',', '.') . '</td>
					</tr>
					<tr>
						<th>Títulos vencidos:</th>
						<td>' . $titulos_vencidos . '</td>
					</tr>
					
					<tr>
						<th>Limite de crédito disponível:</th>
						<td>R$ ' . number_format($limite_disponivel , 2, ',', '.') . '</td>
					</tr>
				</table>';
			
			if ($cliente['ultimo_pedido']['emissao_timestamp'])
			{
				$data_ultimo_pedido = date('d/m/Y', $cliente['ultimo_pedido']['emissao_timestamp']);
			}
			else
			{
				$data_ultimo_pedido = " -- ";
			}
			
			$conteudo .= '<br /><br /><div style="clear:both"></div>';
$conteudo .= '</th>';
$conteudo .= '<td style="">';
		//---------------
		/*
		echo '<pre>';
		print_r($cliente);
		echo '</pre>';
		*/
		
			$conteudo .= heading('Informações do Último Pedido', 3);
			
			$conteudo .= '
			<table cellspacing="0" class="info" style="font-size: 11px">
				<tr>
					<th>Data do último pedido:</th>
					<td>' . $data_ultimo_pedido . '</td>
				</tr>
				<tr>
					<th>Valor do último pedido:</th>
					<td>' .  ($cliente['ultimo_pedido']['codigo_pedido'] ? anchor('pedidos/ver_detalhes/0/' . $cliente['ultimo_pedido']['codigo_pedido'] . '/0/' . $cliente['ultimo_pedido']['filial'], 'R$ ' . number_format(($cliente['ultimo_pedido']['valor']?$cliente['ultimo_pedido']['valor']:0), 2, ',', '.')) : number_format(($cliente['ultimo_pedido']['valor']?$cliente['ultimo_pedido']['valor']:0), 2, ',', '.')) . '</td>
				</tr>
				<tr>
					<th>Forma de Pagamento:</th>
					<td>' . ($cliente['ultimo_pedido']['descricao_forma_pagamento'] ? utf8_encode($cliente['ultimo_pedido']['descricao_forma_pagamento']) : " -- ") . '</td>
				</tr>
			';
			
			//Média de Faturamento - ultimos 12 meses
			
			if($cliente['ultimo_pedido']['emissao_timestamp'])
			{
				$mes_inicial = mktime(0, 0, 0, date('m')-12);
				$mes_final = time();
				
				$codigo_cliente = $cliente['codigo'].'|'.$cliente['loja'];
				
				$total_faturamento = $this->db_cliente->obter_faturamento(NULL, $mes_inicial, $mes_final, NULL, $codigo_cliente);

				$media_faturamento = $total_faturamento[0]['valor_total'] / 12;
			}
			
			
			
			$conteudo .= '
			<tr>
				<th>Média de Faturamento (ùltimos 12 meses):</th>
				<td>R$ ' . number_format($media_faturamento, 2, ',', '.') . '</td>
			</tr></table>';
$conteudo .= '</td>';
$conteudo .= '</tr>
			</table>';
		
		//---------------
		
		//------
		/*
		$conteudo .= heading('Detalhes do Cliente', 2);
		
		$conteudo .= heading('Informações Gerais', 3);
		
		
		$conteudo .= '
			<table cellspacing="0" class="info" style="font-size: 11px">
				<tr>
					<th>Nome:</th>
					<td>' . $cliente['nome'] . '</td>
				</tr>
				<tr>
					<th>CPF/CNPJ:</th>
					<td>' . $cliente['cpf'] . '</td>
				</tr>
			</table>
			
			<table cellspacing="0" class="info" style="font-size: 11px">
				<tr>
					<th>Código:</th>
					<td>' . $cliente['codigo'] . '</td>
				</tr>
				<tr>
					<th>Loja:</th>
					<td>' . $cliente['loja'] . '</td>
				</tr>
			</table>
			
			<table cellspacing="0" class="info" style="font-size: 11px">
				<tr>
					<th>Status:</th>
					<td>' . $cliente['status'] . '</td>
				</tr>
				<tr>
					<th>Contato:</th>
					<td>' . $cliente['pessoa_contato'] . '</td>
				</tr>
			</table>
			
			<table cellspacing="0" class="info" style="font-size: 11px">
				<tr>
					<th>Cliente desde:</th>
					<td>' . ($cliente['data_cadastro_timestamp'] ? date('d/m/Y', $cliente['data_cadastro_timestamp']) : 'não há') . '</td>
				</tr>
			</table>
			
			<div style="clear: left;"></div>
		';
		
		$conteudo .= heading('Informações Financeiras', 3);
		
		if ($cliente['limite_credito'] > 0)
		{
			$limite_credito = '<strong style="color: #060;">R$ ' . number_format($cliente['limite_credito'], 2, ',', '.') . '</strong>';
		}
		else
		{
			$limite_credito = '<strong style="color: #900;">R$ ' . number_format($cliente['limite_credito'], 2, ',', '.') . '</strong>';
		}
		
		if ($cliente['total_titulos_vencidos'] <= 0)
		{
			$titulos_vencidos = '<strong style="color: #060;">R$ ' . number_format($cliente['total_titulos_vencidos'], 2, ',', '.') . '</strong>';
		}
		else
		{
			$titulos_vencidos = '<strong style="color: #900;">R$ ' . number_format($cliente['total_titulos_vencidos'], 2, ',', '.') . '</strong>';
		}
		
		$conteudo .= '
			<table cellspacing="0" class="info" style="font-size: 11px">
				<tr>
					<th>Limite de crédito:</th>
					<td>' . $limite_credito . '</td>
				</tr>
				<tr>
					<th>Títulos em aberto:</th>
					<td>R$ ' . number_format($cliente['total_titulos_em_aberto'], 2, ',', '.') . '</td>
				</tr>
				<tr>
					<th>Títulos vencidos:</th>
					<td>' . $titulos_vencidos . '</td>
				</tr>
			</table>
		';
		
		if ($cliente['ultimo_pedido']['emissao_timestamp'])
		{
			$conteudo .= '
				<table cellspacing="0" class="info" style="font-size: 11px">
					<tr>
						<th>Data do último pedido:</th>
						<td>' . date('d/m/Y', $cliente['ultimo_pedido']['emissao_timestamp']) . '</td>
					</tr>
					<tr>
						<th>Valor da último pedido:</th>
						<td>R$ ' . number_format($cliente['ultimo_pedido']['valor'], 2, ',', '.') . '</td>
					</tr>
				</table>
			';
		}
		
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= heading('Endereço', 3);
		
		$conteudo .= '
			<table cellspacing="0" class="info" style="font-size: 11px">
				<tr>
					<th>Endereço:</th>
					<td>' . $cliente['endereco'] . '</td>
				</tr>
				<tr>
					<th>Cidade:</th>
					<td>' . $cliente['cidade'] . '</td>
				</tr>
			</table>
			
			<table cellspacing="0" class="info" style="font-size: 11px">
				<tr>
					<th>Bairro:</th>
					<td>' . $cliente['bairro'] . '</td>
				</tr>
				<tr>
					<th>Estado:</th>
					<td>' . $cliente['estado'] . '</td>
				</tr>
			</table>
			
			<table cellspacing="0" class="info" style="font-size: 11px">
				<tr>
					<th>CEP:</th>
					<td>' . $cliente['cep'] . '</td>
				</tr>
			</table>
			
			<div style="clear: both;"></div>
		';
		
		$conteudo .= heading('Contato', 3);
		
		$conteudo .= '
			<table cellspacing="0" class="info" style="font-size: 11px">
				<tr>
					<th>Telefone:</th>
					<td>' . $cliente['telefone'] . '</td>
				</tr>
				<tr>
					<th>E-mail:</th>
					<td>' . mailto($cliente['email']) . '</td>
				</tr>
			</table>
			
			<div style="clear: both;"></div>
		';
		*/
		
		
		
		$conteudo .= '<br /><br /><div style="clear:both"></div>';
		$conteudo .= heading('Históricos', 3);
		$conteudo .= '<p>' . anchor('#', 'Cadastrar Novo Histórico', 'onclick="$(this).parents(\'p\').hide().next().show().prev().prev().hide(); return false;"') . '</p>';
		$conteudo .= form_open(site_url('clientes/criar_historico_ajax/' . $cliente['codigo'] . '/' . $cliente['loja']), 'style="display: none;"');
		$conteudo .= '
			<div style="float: left;">
				<h3>Cadastrar Novo Histórico</h3>
				<p style="float: left; margin-right: 10px;">' . form_label('Pessoa de contato:' . br() . form_input('pessoa_contato', utf8_encode($cliente['nome']))) . '</p>
				<p style="float: left; margin-right: 10px;">' . form_label('Cargo:' . br() . form_input('cargo', NULL, 'size="10"')) . '</p>
				<p style="float: left; margin-right: 10px;">' . form_label('E-mail:' . br() . form_input('email', $cliente['email'])) . '</p>
				<div style="clear: both;"></div>
				<p>' . form_label('Descrição do histórico:' . br() . form_textarea(array('cols' => 51, 'rows' => 8, 'name' => 'descricao'))) . '</p>
				<p>' . form_label(form_checkbox('criar_compromisso', 'sim') . ' Agendar compromisso') . '</p>
				<p>' . form_submit('concluir', 'Concluir') . ' ou ' . anchor('#', 'Cancelar', 'onclick="$(this).parents(\'form\').hide().prev().show().prev().show(); return false;"') . '</p>
			</div>
			
			<div id="agendar_compromisso" style="border-left: 1px solid #DDD; float: left;  margin-left: 20px; padding-left: 20px; display: none;">
				<h3>Agendar Compromisso</h3>
				<p style="float: left; margin-right: 10px;">' . form_label('Data:' . br() . form_input('data_inicial_compromisso', NULL, 'class="datepicker" size="6"')) . '</p>
				<p style="float: left; margin-right: 10px;">' . form_label('Hora:' . br() . form_input('horario_inicial_compromisso', NULL, 'alt="time" size="1"')) . '</p>
				<div style="clear: both;"></div>
				<p>' . form_label('Descrição do compromisso:' . br() . form_textarea(array('cols' => 40, 'rows' => in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) ? 5 : 8, 'name' => 'descricao_compromisso'))) . '</p>
			</div>
			
			<div style="clear: both;"></div>
		';
		$conteudo .= form_close();
		
		////////////////////////////////////////////
		$filtros = array(
			array(
				'nome' => 'contato',
				'descricao' => 'Contato',
				'tipo' => 'texto',
				'campo_mysql' => 'pessoa_contato',
				'ordenar' => 1
			),
			array(
				'nome' => 'protocolo',
				'descricao' => 'Protocolo',
				'tipo' => 'texto',
				'campo_mysql' => 'protocolo',
				'ordenar' => 2
			),
			array(
				'nome' => 'criacao',
				'descricao' => 'Data',
				'tipo' => 'data',
				'campo_mysql' => 'timestamp',
				'ordenar' => 0
			)
		);
		$conteudo .= '<h3>Históricos</h3>';
		$conteudo .= '<div class="topo_grid">';
		$conteudo .= '	<p><strong>Filtros</strong></p>';
		
		$conteudo .= form_open(current_url(), array('method' => 'get'));
		
		$conteudo .= $this->filtragem($filtros);
		
		$conteudo .= '<div style="clear: both;"></div>';
	
		$conteudo .= form_close();
		
		$conteudo .= '</div>';
		
		////////////////////////////////////////////
		
		$this->filtragem_mysql($filtros);
		$limit_consulta = 20;
		$historicos = $this->db
			->from('historicos_clientes')
			->where(array('codigo_cliente' => $cliente['codigo'], 'loja_cliente' => $cliente['loja']))
			->limit($limit_consulta, $this->input->get('per_page'))
			->order_by('id', 'desc')->get()->result();
		
		//$conteudo .= $this->db->last_query();
		
		// RETORNA PAGINACAO PRONTA
		$paginacao = $this->paginacao_rapida();
		$contador = $this->paginacao_contador_desc($this->get_total_registro_paginacao(), $limit_consulta);
		$total_registro = $this->get_total_registro_paginacao();
		
		////////////////////////////////////////////
		
		$conteudo .= '
			<script type="text/javascript">
				$(document).ready(function() {
					$("input[name=criar_compromisso]").click(function() {
						$("#agendar_compromisso").toggle();
					});
					
					$("input[name=concluir]").click(function() {
						var _this = this;
						
						$(this).attr("disabled", "disabled").val("Carregando...");
						
						$.post($(this).parents("form").attr("action"), $(this).parents("form").serialize(), function(dados) {
							if (dados.erro)
							{
								alert(dados.erro);
							}
							else
							{
								window.location = window.location;
							}
							
							$(_this).removeAttr("disabled").val("Concluir");
						}, "json");
					});
				});
			</script>
		';
		
		$conteudo .= $this->retorna_historico($historicos, $total_registro);
		$conteudo .= $paginacao;
		$this->load->view('layout', array('conteudo' => $conteudo));
		
		//para-aprovacao.. $this->load->view('clientes/tabela_detalhes', array('historicos' => $historicos, 'total_registro' => $total_registro));
		
	}
	
	private function retorna_historico($historicos, $total_registro){
		$conteudo = '';
		$tabela_cabecalho = '
			<table cellspacing="0" class="novo_grid">
			<thead>
				<tr>
					<th>Data</th>
					<th>Contato</th>
					<th>Cargo</th>
					<th>E-mail</th>
					<th>Protocolo</th>
					<th>Opções</th>
					</tr>
				</thead>
			<tbody>';
		
		$entrou = false;
		$tabela_corpo = '';
		foreach ($historicos as $historico)
		{
			$entrou = true;
			$tabela_corpo .=
				'<tr>
					<td>' . date('d/m/Y H:i', $historico->timestamp) . '</td>
					<td>' . $historico->pessoa_contato . '</td>
					<td>' . $historico->cargo . '</td>
					<td>' . mailto($historico->email) . '</td>
					<td>' . $historico->protocolo . '</td>
					<td>
						<a class="colorbox_inline" href="#historico_' . $historico->id . '">Ver Detalhes</a><div style="display: none;">
						<div id="historico_' . $historico->id . '" style="max-width: 740px;">
							<table cellspacing="5">
								<tr>
									<td><strong>Cliente:</strong></td>
									<td>' . $cliente['nome'] . '</td></tr><tr><td><strong>Contato:</strong></td>
									<td>' . $historico->pessoa_contato . '</td></tr><tr><td><strong>Cargo:</strong></td>
									<td>' . $historico->cargo . '</td></tr><tr><td><strong>E-mail:</strong></td>
									<td>' . mailto($historico->email) . '</td>
								</tr>
							</table>
							<p>' . nl2br($historico->descricao) . '</p>
						</div>
					</div>
					</td>
				</tr>';
		}
		
		// HISTÓRICOS **
		if($entrou){
			$tabela_corpo.= '</tbody></table>';
			//$conteudo .= $tabela_cabecalho . $tabela_corpo . '<center><br>'.$total_registro.' registros encontrados.<br>' . $this->paginacao($total_registro).' </center>';
			$conteudo .= $tabela_cabecalho . $tabela_corpo;// . $this->paginacao_rapida();
		}
		//else $conteudo .= '<center><br>Nenhum registro encontrado.</center>';
		
		return($conteudo);
	}
	
	function criar_historico_ajax($codigo_cliente = NULL, $loja_cliente = NULL)
	{
		if (!$codigo_cliente || !$loja_cliente)
		{
			redirect();	
		}
		
		// TODO: representantes/prepostos só podem criar históricos para SEUS clientes
		
		$cliente = $this->db_cliente->obter_cliente($codigo_cliente, $loja_cliente);
		
		if (!$cliente)
		{
			redirect();	
		}
		
		$dados = array(
			'erro' => NULL
		);
		
		if (!$this->input->post('pessoa_contato'))
		{
			$dados['erro'] = 'Digite uma pessoa de contato.';
		}
		else if ($this->input->post('email') && !valid_email($this->input->post('email')))
		{
			$dados['erro'] = 'Digite um e-mail válido.';
		}
		else if (!$this->input->post('descricao'))
		{
			$dados['erro'] = 'Digite uma descrição para o histórico.';
		}
		else
		{
			if ($this->input->post('criar_compromisso'))
			{
				/*
				if (!$this->input->post('nome_compromisso'))
				{
					$dados['erro'] = 'Digite um nome para o compromisso.';
				}
				*/
				if (!$this->input->post('descricao_compromisso'))
				{
					$dados['erro'] = 'Digite uma descrição para o compromisso.';
				}
				else if (!$this->_validar_data($this->input->post('data_inicial_compromisso')))
				{
					$dados['erro'] = 'Digite uma data inicial válida.';
				}
				else if (!$this->_validar_horario($this->input->post('horario_inicial_compromisso')))
				{
					$dados['erro'] = 'Digite um horário inicial válido.';
				}
				else
				{
					$data_inicial = explode('/', $this->input->post('data_inicial_compromisso'));
					$horario_inicial = explode(':', $this->input->post('horario_inicial_compromisso'));
					
					$inicio_timestamp = mktime($horario_inicial[0], $horario_inicial[1], 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
					
					if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')))
					{
						$this->db->insert('compromissos', array(
							'timestamp' => time(),
							'id_usuario' => $this->input->post('id_usuario_compromisso') ? $this->input->post('id_usuario_compromisso') : $this->session->userdata('id_usuario'),
							'status' => 'agendado',
							'tipo' => 'Cliente',
							'codigo_tipo' => $cliente['codigo'] . '|' . $cliente['loja'],
							'detalhes' => TRUE,
							'id_representante' => $cliente['codigo_representante'],
							'id_usuario_cad' => $this->db->from('usuarios')->where(array('codigo' => $cliente['codigo_representante'], 'grupo' => 'representantes'))->get()->row()->id,
							'nome' => $cliente['nome'],
							'descricao' => $this->input->post('descricao_compromisso'),
							'inicio_timestamp' => $inicio_timestamp
						));
					}
					else
					{
						$this->db->insert('compromissos', array(
							'timestamp' => time(),
							'id_usuario' => $this->input->post('id_usuario_compromisso') ? $this->input->post('id_usuario_compromisso') : $this->session->userdata('id_usuario'),
							'status' => 'agendado',
							'tipo' => 'Cliente',
							'codigo_tipo' => $cliente['codigo'] . '|' . $cliente['loja'],
							'id_representante' => $cliente['codigo_representante'],
							'nome' => $cliente['nome'],
							'descricao' => $this->input->post('descricao_compromisso'),
							'inicio_timestamp' => $inicio_timestamp
						));
					}
				}
			}
			
			if (!$dados['erro'])
			{
				$i = $this->db->from('historicos_clientes')->get()->num_rows();
				$i += $this->db->from('historicos_prospects')->get()->num_rows();
				
				$protocolo = date('Y') . '/' . ($this->session->userdata('codigo_usuario') ? $this->session->userdata('codigo_usuario') : 0) . '.' . $i;
				
				$this->db->insert('historicos_clientes', array(
					'timestamp' => time(),
					'codigo_cliente' => $cliente['codigo'],
					'loja_cliente' => $cliente['loja'],
					'pessoa_contato' => $this->input->post('pessoa_contato'),
					'cargo' => $this->input->post('cargo'),
					'email' => $this->input->post('email'),
					'descricao' => $this->input->post('descricao'),
					'protocolo' => $protocolo
				));
			}
		}
		
		echo json_encode($dados);
	}
	
	//
	
	function _obter_representantes()
	{
		$representantes = $this->db->from('usuarios')->where('status', 'ativo')->where('grupo', 'representantes')->order_by('nome', 'asc')->get()->result();
		$_representantes = array(0 => 'Todos');
		
		foreach ($representantes as $representante)
		{
			$_representantes[$representante->codigo] = $representante->codigo . ' - ' . $representante->nome;
		}
		
		return $_representantes;
	}
	
	function _obter_usuarios()
	{
		$usuarios = $this->db->from('usuarios')->where(array('status' => 'ativo', 'grupo' => 'representantes'))->order_by('grupo', 'desc')->order_by('nome', 'asc')->get()->result();
		$_usuarios = array();
		
		foreach ($usuarios as $usuario)
		{
			$_usuarios[$usuario->id] = $usuario->nome . ' (grupo ' . element($usuario->grupo, $this->_obter_grupos()) . ')';
		}
		
		return $_usuarios;
	}
	
	public function atendimentos($cliente)
	{
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->config->load('db_cliente_' . $this->db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');
		
		$filtros = array(
			
			array(
				'nome' => 'status',
				'descricao' => 'Status',
				'tipo' => 'opcoes',
				'campo_mssql' => '',//$this->_db_cliente['campos']['atendimentos']['operacao'],
				'opcoes' => array(
					'todos' => 'TODAS',
					'CANCELADO' => 'CANCELADO',
					'NF. EMITIDA' => 'NF. EMITIDA',
					'ATENDIMENTO' => 'ATENDIMENTO',
					'ORCAMENTO' => 'ORÇAMENTO',
					'FATURAMENTO' => 'FATURAMENTO'
					),
				//'ordenar_ms' => 3
			),
			array(
				'nome' => 'numero',
				'descricao' => 'Número',
				'tipo' => 'texto',
				'campo_mssql' => $this->_db_cliente['campos']['atendimentos']['numero'],
				'ordenar_ms' => 1
			),
			array(
				'nome' => 'data_atendimento',
				'descricao' => 'Emissão',
				'tipo' => 'data',
				'campo_mssql' => $this->_db_cliente['campos']['atendimentos']['data_atendimento'],
				'ordenar_ms' => 2
			),
			
			array(
				'nome' => 'operacao',
				'descricao' => 'Operação',
				'tipo' => 'opcoes',
				'campo_mssql' => $this->_db_cliente['campos']['atendimentos']['operacao'],
				'opcoes' => array(
					'todos' => 'TODAS',
					1 => 'FATURAMENTO', 
					2 => 'ORÇAMENTO', 
					3 => 'ATENDIMENTO'
					),
				'ordenar_ms' => 3
			),
			array(
				'nome' => 'representante',
				'descricao' => 'Representante',
				'tipo' => 'texto',
				'campo_mssql' => $this->_db_cliente['campos']['representantes']['nome'],
				'ordenar_ms' => 4
			)
		);
		
			// para que a paginação no mssql funcione, devemos ordenar de alguma forma, caso o usuário não escolher uma ordenação, vamos fazer manualmente
			if (!$this->input->get('ordenar_ms'))
			{
				// vamos setar o $_GET ao invés de usar o order_by()
				// usando o order_by a flexinha no layout indicando o que está sendo ordenado não apareceria para o usuário
				$_GET['ordenar_ms'] = $this->_db_cliente['campos']['atendimentos']['data_atendimento'];
				$_GET['ordenar_ms_tipo'] = 'asc';
			}
			
			// ** FILTRAGEM PARA EXIBIÇÃO DOS RESULTADOS
			
			$this->db_cliente->select('SUA010.*, CASE_STATUS = CASE
												   WHEN UA_STATUS = \'CAN\' AND UA_CANC = \'S\' AND UA_CODCANC <> \'\' THEN (
												      \'CANCELADO\'
												   )
												   WHEN UA_OPER = 1 AND UA_STATUS = \'NF.\' AND UA_DOC <> \'\' THEN (
												      \'NF. EMITIDA\'
												   )
												   WHEN UA_OPER = 3 THEN (
												      \'ATENDIMENTO\'
												   )
												   WHEN UA_OPER = 2 THEN (
												      \'ORCAMENTO\'
												   )
												   WHEN UA_OPER = 1 THEN (
												      \'FATURAMENTO\'
												   )
												END');
			if ($this->input->get('status'))
			{
			
				$this->db_cliente->where('1 = CASE \''.$this->input->get('status').'\'
											WHEN \'CANCELADO\'
											THEN CASE WHEN UA_STATUS = \'CAN\' AND UA_CANC = \'S\' AND UA_CODCANC <> \'\' THEN 1 ELSE 0 END
											
											WHEN \'NF. EMITIDA\'
											THEN CASE WHEN UA_OPER = 1 AND UA_STATUS = \'NF.\' AND UA_DOC <> \'\' THEN 1 ELSE 0 END 
											
											WHEN \'ATENDIMENTO\' 
											THEN CASE WHEN UA_OPER = 3 THEN 1 ELSE 0 END  
											
											WHEN \'ORCAMENTO\'
											THEN CASE WHEN UA_OPER = 2 THEN 1 ELSE 0 END  
											
											WHEN \'FATURAMENTO\'
											THEN CASE WHEN UA_OPER = 1 THEN 1 ELSE 0 END 
											
											ELSE
											CASE 1 WHEN 1 THEN 1 ELSE 0 END 
										  END');
			}
			$this->filtragem_mssql($filtros);
			
			$atendimentos = $this->db_cliente->from($this->_db_cliente['tabelas']['atendimentos'])
			->join($this->_db_cliente['tabelas']['representantes'], $this->_db_cliente['campos']['representantes']['codigo'].' = '.$this->_db_cliente['campos']['atendimentos']['representante'])			
			->where(array(	$this->_db_cliente['tabelas']['atendimentos'].'.'.$this->_db_cliente['campos']['atendimentos']['delete'].' <> ' => '*',
							$this->_db_cliente['campos']['atendimentos']['cliente'] => $cliente,
					))
			->limit(20, $this->input->get('per_page'))
			->get()->result_array();
			//echo $this->db_cliente->last_query();
			// FILTRAGEM PARA EXIBIÇÃO DOS RESULTADOS **
			if ($this->input->get('status'))
			{
			
				$this->db_cliente->where('1 = CASE \''.$this->input->get('status').'\'
											WHEN \'CANCELADO\'
											THEN CASE WHEN UA_STATUS = \'CAN\' AND UA_CANC = \'S\' AND UA_CODCANC <> \'\' THEN 1 ELSE 0 END
											
											WHEN \'NF. EMITIDA\'
											THEN CASE WHEN UA_OPER = 1 AND UA_STATUS = \'NF.\' AND UA_DOC <> \'\' THEN 1 ELSE 0 END 
											
											WHEN \'ATENDIMENTO\' 
											THEN CASE WHEN UA_OPER = 3 THEN 1 ELSE 0 END  
											
											WHEN \'ORCAMENTO\'
											THEN CASE WHEN UA_OPER = 2 THEN 1 ELSE 0 END  
											
											WHEN \'FATURAMENTO\'
											THEN CASE WHEN UA_OPER = 1 THEN 1 ELSE 0 END 
											
											ELSE
											CASE 1 WHEN 1 THEN 1 ELSE 0 END 
										  END');
			}
			// ** FILTRAGEM PARA PAGINAÇÃO
			$this->filtragem_mssql($filtros, FALSE);
			
			$total = $this->db_cliente->from($this->_db_cliente['tabelas']['atendimentos'])
			->join($this->_db_cliente['tabelas']['representantes'], $this->_db_cliente['campos']['representantes']['codigo'].' = '.$this->_db_cliente['campos']['atendimentos']['representante'])			
			->where(array(	$this->_db_cliente['tabelas']['atendimentos'].'.'.$this->_db_cliente['campos']['atendimentos']['delete'].' <> ' => '*',
							$this->_db_cliente['campos']['atendimentos']['cliente'] => $cliente,
					))
			->get()->num_rows();
			//echo $this->db_cliente->last_query();
		
		/*
		echo '<pre>';
		print_r($atendimentos);
		echo '<br />';
		print_r($total);
		echo '</pre>';
		//die();
		*/
		$this->load->view('layout', array('conteudo' => $this->load->view('clientes/atendimentos', array(
			'atendimentos' => $atendimentos, 
			'total' => $total, 
			'filtragem' => $this->filtragem($filtros), 
			'paginacao' => $this->paginacao($total)
		), TRUE)));
	}
	
	public function detalhes_atendimento($cli, $num)
	{
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->config->load('db_cliente_' . $this->db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');
		
		$atendimentos = $this->db_cliente->from($this->_db_cliente['tabelas']['atendimentos'])
		->join($this->_db_cliente['tabelas']['representantes'], $this->_db_cliente['campos']['representantes']['codigo'].' = '.$this->_db_cliente['campos']['atendimentos']['representante'])			
		->join('SUH010','UH_MIDIA = UA_MIDIA', 'left')
		->join('DA0010','DA0_CODTAB = UA_TABELA', 'left')
		->join('SE4010','E4_CODIGO = UA_CONDPG', 'left')
		->join('SU7010','U7_COD = UA_OPERADO', 'left')
		->where(array(	$this->_db_cliente['tabelas']['atendimentos'].'.'.$this->_db_cliente['campos']['atendimentos']['delete'].' <> ' => '*',
						//'SUH010.'.$this->_db_cliente['campos']['atendimentos']['delete'].' <> ' => '*',
						$this->_db_cliente['tabelas']['atendimentos'].'.'.$this->_db_cliente['campos']['atendimentos']['cliente'] => $cli,
						$this->_db_cliente['campos']['atendimentos']['numero'] => $num
				))
		->get()->row_array();
		
	
		/*
		echo $this->db_cliente->last_query();
		echo '<pre>';
		print_r($atendimentos);
		echo '</pre>';
		die();
		*/
	
		
		$this->load->view('layout', array('conteudo' => $this->load->view('clientes/detalhes_atendimento', array(
			'atendimento' => $atendimentos), TRUE)));
	}
}