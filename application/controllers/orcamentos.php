<?php

class Orcamentos extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('autocomplete');
    }

    function editar_codigo_usuario_consultar_orcamentos($novo_codigo = NULL) {
        $this->session->set_userdata('codigo_usuario_consultar_orcamentos', $novo_codigo);

        redirect('orcamentos');
    }

#
#
#
#

    /* function index() {
      if (in_array($this->session->userdata('grupo_usuario'), array('representantes', 'prepostos'))) {
      $this->session->set_userdata('codigo_usuario_consultar_orcamentos', $this->session->userdata('codigo_usuario'));
      }

      ### ** VALIDAR FORM
      if ($this->input->post('tipo_consulta') == 'cliente' && !$this->input->post('codigo_loja_cliente')) {
      $erro = 'Selecione um cliente.';
      } else if ($this->input->post('tipo_consulta') == 'prospect' && !$this->input->post('id_prospect')) {
      $erro = 'Selecione um prospect.';
      } else if ($this->input->post('data_final') && !$this->_validar_data($this->input->post('data_final'))) {
      $erro = 'Digite uma data final válida.';
      } else {
      if ($this->input->post('codigo_loja_cliente') && $this->input->post('tipo_consulta') == 'cliente') {
      $codigo_loja_cliente = explode('|', $this->input->post('codigo_loja_cliente'));
      $codigo_cliente = $codigo_loja_cliente[0];
      $loja_cliente = $codigo_loja_cliente[1];
      }

      if ($this->input->post('id_prospect') && $this->input->post('tipo_consulta') == 'prospect') {
      $id_prospect = $this->input->post('id_prospect');
      }

      if ($this->input->post('codigo_loja_cliente') && $this->input->post('tipo_consulta') == 'cliente') {
      $codigo_loja_cliente = explode('|', $this->input->post('codigo_loja_cliente'));
      $codigo_cliente = $codigo_loja_cliente[0];
      $loja_cliente = $codigo_loja_cliente[1];
      }

      if ($this->input->post('data_inicial')) {
      $data_inicial = explode('/', $this->input->post('data_inicial'));
      $data_inicial_timestamp = mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
      }

      if ($this->input->post('data_final')) {
      $data_final = explode('/', $this->input->post('data_final'));
      $data_final_timestamp = mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]);

      if ($this->input->post('codigo_usuario_consultar_orcamentos')) {
      $codigo_usuario = $this->input->post('codigo_usuario_consultar_orcamentos');
      }
      }


      $orcamentos = $this->db_cliente->obter_pedidos($codigo_cliente, $loja_cliente, $data_inicial_timestamp, $data_final_timestamp, 'orcamento', $id_prospect, $codigo_usuario);


      ##   PAGINAÇÃO
      $total_de_pedidos = count($this->db_cliente->obter_pedidos($codigo_cliente, $loja_cliente, $data_inicial_timestamp, $data_final_timestamp, 'orcamento', $id_prospect));
      $resultados_por_pagina = 20;
      $paginacao = $this->paginacao($total_de_prospects, $resultados_por_pagina);


      ##    PREÇO TOTAL
      if ($orcamentos) {
      foreach ($orcamentos as $orcamento) {
      $_total = $this->db->select('SUM(preco * quantidade) AS valor_total')->from('itens_pedidos')->where('id_pedido', $orcamento['id'])->get()->row();
      $total += $_total->valor_total;
      }
      }
      $usuario['total'] = $total;

      ##    FILTRO
      $this->db_cliente = $this->load->database('db_cliente', TRUE);

      $filtros = array(
      array(
      'nome' => 'clientes_prospect',
      'descricao' => 'Cliente/Prospect',
      'tipo' => 'texto',
      'campo_mssql' => 'A1_NOME',
      'ordenar' => 0
      ),
      array(
      'nome' => 'emissao',
      'descricao' => 'Emissão',
      'tipo' => 'data',
      'campo_mssql' => 'C5_EMISSAO',
      'ordenar' => 1
      ),
      array(
      'nome' => 'quantidade',
      'descricao' => 'Quant. Vend',
      'tipo' => 'texto',
      'campo_mssql' => 'C6_QTDVEN',
      'ordenar' => 2
      ),
      array(
      'nome' => 'total',
      'descricao' => 'Total',
      'tipo' => 'texto',
      'campo_mssql' => 'C6_VALOR',
      'ordenar' => 3
      ));

      // para que a paginação no mssql funcione, devemos ordenar de alguma forma, caso o usuário não escolher uma ordenação, vamos fazer manualmente
      if (!$this->input->get('ordenar_ms'))
      {
      // vamos setar o $_GET ao invés de usar o order_by()
      // usando o order_by a flexinha no layout indicando o que está sendo ordenado não apareceria para o usuário
      $_GET['ordenar_ms'] = 'C5_EMISSAO';
      $_GET['ordenar_ms_tipo'] = 'desc';
      }

      $this->filtragem_mssql($filtros);

      $orcamentos = $this->db_cliente->select('C5_NUM, C5_XINFORM, C5_EMISSAO, C5_CLIENTE, C5_LOJACLI, A1_NOME, (CASE WHEN SUM(C6_QTDENT) <= 0 THEN "Aguardando Faturamento" WHEN SUM(C6_QTDENT) < SUM(C6_QTDVEN) THEN "Parcialmente Faturado" ELSE "Faturado" END) AS status, SUM(C6_QTDVEN) AS C6_QTDVEN, SUM(C6_QTDENT) AS C6_QTDENT, SUM(C6_PRCVEN * C6_QTDVEN) AS total, SUM(C6_PRCVEN * C6_QTDENT) AS total_parcial')
      ->from('SC5020')
      ->join('SC6020', 'C6_NUM = C5_NUM')
      ->join('SA1020', 'A1_COD = C5_CLIENTE AND A1_LOJA = C5_LOJACLI')
      ->where(array(
      'SC6020.D_E_L_E_T_ !=' => '*',
      'SC5020.D_E_L_E_T_ !=' => '*'
      ))
      ->limit(20, $this->input->get('per_page'))
      ->group_by('C5_NUM, C5_XINFORM, C5_EMISSAO, C5_CLIENTE, C5_LOJACLI, A1_NOME')
      ->get()->result_array();

      // FILTRAGEM PARA EXIBIÇÃO DOS RESULTADOS **
      }


      $usuario['grupo'] = in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros'));

      $usuario['representante'] = $this->_obter_dropdown_representantes('codigo', array('name' => 'codigo_usuario_consultar_orcamentos', 'value' => $this->session->userdata('codigo_usuario_consultar_orcamentos'), 'id' => 'usuarios'), TRUE);

      $this->load->view('layout', array('conteudo' => $this->load->view('orcamentos/index', array( 'orcamentos' => $orcamentos, 'usuario' => $usuario, 'filtragem' => $this->filtragem($filtros), 'paginacao' => $paginacao, $erro), TRUE)));
      }
     */

    function index() {
        $this->db_cliente = $this->load->database('db_cliente', TRUE);

        $filtros = array(
            array(
                'nome' => 'clientes_prospect',
                'descricao' => 'Cliente/Prospect',
                'tipo' => 'texto',
                'campo_mssql' => 'A1_NOME',
                'ordenar_ms' => 0
            ),
            array(
                'nome' => 'emissao',
                'descricao' => 'Emissão',
                'tipo' => 'data',
                'campo_mssql' => 'C5_EMISSAO',
                'ordenar_ms' => 1
            ),
            array(
                'nome' => 'quantidade',
                'descricao' => 'Quant. Vend',
                'tipo' => 'texto',
                'campo_mssql' => 'C6_QTDVEN',
                'ordenar_ms' => 2
            ),
            array(
                'nome' => 'total',
                'descricao' => 'Total',
                'tipo' => 'texto',
                'campo_mssql' => 'C6_VALOR',
                'ordenar_ms' => 3
                ));

        // para que a paginação no mssql funcione, devemos ordenar de alguma forma, caso o usuário não escolher uma ordenação, vamos fazer manualmente
        if (!$this->input->get('ordenar_ms')) {
            // vamos setar o $_GET ao invés de usar o order_by()
            // usando o order_by a flexinha no layout indicando o que está sendo ordenado não apareceria para o usuário
            $_GET['ordenar_ms'] = 'C5_EMISSAO';
            $_GET['ordenar_ms_tipo'] = 'desc';
        }

        $this->filtragem_mssql($filtros);

        $orcamentos = $this->db_cliente->select('C5_NUM, C5_XINFORM, C5_EMISSAO, C5_CLIENTE, C5_LOJACLI, A1_NOME, (CASE WHEN SUM(C6_QTDENT) <= 0 THEN "Aguardando Faturamento" WHEN SUM(C6_QTDENT) < SUM(C6_QTDVEN) THEN "Parcialmente Faturado" ELSE "Faturado" END) AS status, SUM(C6_QTDVEN) AS C6_QTDVEN, SUM(C6_QTDENT) AS C6_QTDENT, SUM(C6_PRCVEN * C6_QTDVEN) AS total, SUM(C6_PRCVEN * C6_QTDENT) AS total_parcial,C6_VALOR')
						->from('SC5020')
						->join('SC6020', 'C6_NUM = C5_NUM')
						->join('SA1020', 'A1_COD = C5_CLIENTE AND A1_LOJA = C5_LOJACLI')
						->where(array(
							'SC6020.D_E_L_E_T_ !=' => '*',
							'SC5020.D_E_L_E_T_ !=' => '*'
						))
						->limit(20, $this->input->get('per_page'))
						->group_by('C5_NUM, C5_XINFORM, C5_EMISSAO, C5_CLIENTE, C5_LOJACLI, A1_NOME,C6_QTDVEN,C6_VALOR')
						->get()->result_array();

        // FILTRAGEM PARA EXIBIÇÃO DOS RESULTADOS **


        $this->filtragem_mssql($filtros);

        $total_de_pedidos = $this->db_cliente->select('C5_NUM, C5_XINFORM, C5_EMISSAO, C5_CLIENTE, C5_LOJACLI, A1_NOME, (CASE WHEN SUM(C6_QTDENT) <= 0 THEN "Aguardando Faturamento" WHEN SUM(C6_QTDENT) < SUM(C6_QTDVEN) THEN "Parcialmente Faturado" ELSE "Faturado" END) AS status, SUM(C6_QTDVEN) AS C6_QTDVEN, SUM(C6_QTDENT) AS C6_QTDENT, SUM(C6_PRCVEN * C6_QTDVEN) AS total, SUM(C6_PRCVEN * C6_QTDENT) AS total_parcial')
						->from('SC5020')
						->join('SC6020', 'C6_NUM = C5_NUM')
						->join('SA1020', 'A1_COD = C5_CLIENTE AND A1_LOJA = C5_LOJACLI')
						->where(array(
							'SC6020.D_E_L_E_T_ !=' => '*',
							'SC5020.D_E_L_E_T_ !=' => '*'
						))
						->group_by('C5_NUM, C5_XINFORM, C5_EMISSAO, C5_CLIENTE, C5_LOJACLI, A1_NOME,C6_VALOR')
						->get()->num_rows();

        // FILTRAGEM PARA PAGINAÇÃO **

        $this->load->view('layout', array('conteudo' => $this->load->view('orcamentos/index', array('orcamentos' => $orcamentos, 'filtragem' => $this->filtragem($filtros), 'paginacao' => $this->paginacao($total_de_pedidos)), TRUE)));
    }

#
#

    function _obter_representantes($opcao_todos = TRUE) {
        $representantes = $this->db->from('usuarios')->where('status', 'ativo')->where('grupo', 'representantes')->order_by('nome', 'asc')->get()->result();
        $_representantes = $opcao_todos ? array(0 => 'Todos') : array();

        foreach ($representantes as $representante) {
            $_representantes[$representante->codigo] = $representante->codigo . ' - ' . $representante->nome;
        }

        return $_representantes;
    }

    function _obter_clientes($codigo_usuario = NULL) {
        $clientes = $this->db_cliente->obter_clientes($codigo_usuario);
        $_clientes = array();

        foreach ($clientes as $cliente) {
            $_clientes[$cliente['codigo'] . '|' . $cliente['loja']] = $cliente['nome'] . ' - ' . $cliente['cpf'];
        }

        return $_clientes;
    }

    function obter_clientes_json($codigo_usuario = NULL) {
        $clientes = $this->db_cliente->obter_clientes($codigo_usuario);

        foreach ($clientes as $indice => $valor) {
            $clientes[$indice]['_indice'] = $valor['codigo'] . '|' . $valor['loja'];
            $clientes[$indice]['_valor'] = $valor['nome'] . ' - ' . $valor['cpf'];
        }

        exit($this->_gerar_json_dojo_query_read_store($clientes));
    }

    function _obter_prospects($codigo_usuario = NULL) {
        if ($codigo_usuario) {
            $this->db->where('codigo_usuario', $codigo_usuario);
        }

        $prospects = $this->db->from('prospects')->where(array('status !=' => 'convertido_cliente'))->get()->result();
        $_prospects = array();

        foreach ($prospects as $prospect) {
            $_prospects[$prospect->id] = $prospect->nome . ' - ' . $prospect->cpf;
        }

        return $_prospects;
    }

    function obter_clientes($codigo_do_representante = NULL) {
        $palavras_chave = addslashes($_GET['term']);

        $dados = array();

        $clientes = $this->autocomplete->obter_clientes($palavras_chave, $codigo_do_representante);

        foreach ($clientes as $cliente) {
            $dados[] = array('label' => $cliente['nome'] . ' - ' . $cliente['cpf'], 'value' => $cliente['codigo'] . '|' . $cliente['loja']);
        }


        echo json_encode($dados);
    }

    function obter_prospects($codigo_do_representante = NULL) {
        $palavras_chave = addslashes($_GET['term']);

        $dados = array();

        $prospects = $this->autocomplete->obter_prospects($palavras_chave, $codigo_do_representante);

        foreach ($prospects as $prospect) {
            $dados[] = array('label' => $prospect['nome'] . ' - ' . $prospect['cpf'], 'value' => $prospect['id']);
        }


        echo json_encode($dados);
    }

}