<?php

class Compromissos extends MY_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function obter_alertas()
	{
		// verificar compromissos pendente
		$this->db->update('compromissos', array('status' => 'pendente'), '(status = "agendado" OR status = "reagendado") AND inicio_timestamp < ' . time());

		$dados['num_compromissos'] = $this->db->from('compromissos')->where('id_usuario_cad = ' . $this->session->userdata('id_usuario') . ' AND (status != "realizado") AND inicio_timestamp < ' . (mktime(0, 0, 0) + 3600 * 24))->get()->num_rows();

		//$dados['num_compromissos'] .= $this->db->last_query();

		//$total_compromissos_hoje = $this->db->from('compromissos')->where('id_usuario = ' . $this->session->userdata('id_usuario') . ' AND (status = "agendado" OR status = "reagendado" OR status = "pendente") AND inicio_timestamp >= ' . time() . ' AND inicio_timestamp < ' . (mktime(0, 0, 0) + 3600 * 24))->get()->num_rows();
		$total_compromissos_hoje = $this->db->from('compromissos')->where('id_usuario = ' . $this->session->userdata('id_usuario') . ' AND (status = "agendado" OR status = "reagendado" OR status = "pendente") AND inicio_timestamp < ' . (mktime(0, 0, 0) + 3600 * 24))->get()->num_rows();

		$dados['compromissos'] = '<p style="color: #0FF; font-size: 12px;"><strong>Existem ' . $total_compromissos_hoje . ' compromissos aguardando análise</strong> -  ' . anchor('compromissos?status=pendente&inicio_inicial=&inicio_final=&tipo=todos&para=&descricao=&my_submit=Filtrar#filtros', 'Ver todos &raquo;', 'style="color: #FF0;"') . '</p>';

		$proximo_compromisso = $this->db->from('compromissos')->where('id_usuario = ' . $this->session->userdata('id_usuario') . ' AND (status = "agendado" OR status = "reagendado" OR status = "pendente") AND inicio_timestamp >= ' . time())->order_by('inicio_timestamp', 'asc')->limit(1)->get()->row();

		if ($proximo_compromisso->inicio_timestamp > time())
		{
			$compromissos_alertados = $this->session->userdata('compromissos_alertados') ? $this->session->userdata('compromissos_alertados') : array();

			if ($proximo_compromisso->inicio_timestamp - time() <= 60 && !in_array($proximo_compromisso->id, $compromissos_alertados))
			{
				# REMOVIDO DADOS COMPROMISSO

				$dados['compromissos'] .= '
					<div style="display: none;">
						<div id="proximo_compromisso">
							<h2 style="margin: 0;">Você tem um compromisso!</h2>
							
							<ul>
								<li><strong>Nome:</strong> ' . $proximo_compromisso->nome . '</li>
								<li><strong>Início:</strong> ' . date('d/m/Y H:i', $proximo_compromisso->inicio_timestamp) . '</li>
								<li><strong>Descrição:</strong> ' . $proximo_compromisso->descricao . '</li>
							</ul>
							
							<h3>Opções</h3>
							
							<p><input type="submit" value="Adiar" class="adiar_v" /> <input type="submit" value="Pendente" class="marcar_como_pendente" /> <input type="submit" value="Realizado" class="marcar_como_realizado" /></p>
							
							<div style="visibility: hidden;">
			    				<div style="clear:both"></div>
					
								<p style="float:left; margin-right: 20px">
									' . form_label('Data:' . br() . form_input('data_inicial_adiar', $this->input->post('data_inicial'), 'class="datepicker" size="6" id="data_inicial_adiar" ')) . '
								</p>
								
								<p style="float:left; ">
									' . form_label('Hora:' . br() . form_input('horario_inicial_adiar', $this->input->post('horario_inicial'), 'alt="time" size="1" ')) . '
								</p>
								
								<div style="clear:both"></div>
				    			
				    			<p><input type="submit" value="Concluir" class="adiar_" /> <input type="submit" value="Cancelar" class="cancelar" /></p>
			    			</div>
						</div>
					</div>
					
					
					
					<script type="text/javascript">
					
						$(function() {
							$( "#data_inicial_adiar" ).datepicker({
								dateFormat: "dd/mm/yy",
								minDate: new Date()
								});
							$("input:text").setMask();
						});
						
						$(document).ready(function() {
							$(".adiar_v").live("click", function() {
								$(this).parents("p").hide().next().css("visibility", "visible");
							});
							
							$(".adiar_").live("click", function() {
								
								var validar_data = $("input[name=data_inicial_adiar]").val();
								var validar_hora = $("input[name=horario_inicial_adiar]").val();
								var regex_data = /^([0-2][0-9]|3[01])\/(0[1-9]|1[0-2])\/([12][0-9]{3})$/;
								var regex_hora = /^([01]?[0-9]|2[0-3]):[0-5]?[0-9]$/;
								
								var ex_data = validar_data.split("/");
								var ex_hora = validar_hora.split(":");
								
								if (!regex_data.test(validar_data)) {
									alert("Data incorreta!");
								}
								else if(!regex_hora.test(validar_hora))
								{
									alert("Hora incorreta!");
								}
								else if(' . $proximo_compromisso->inicio_timestamp . ' >= mktime(ex_hora[0], ex_hora[1], 0, ex_data[1], ex_data[0], ex_data[2])) {
									alert("A data digitada deve ser maior que a data atual!");
								}
								else
								{
									$.post("' . site_url('compromissos/adiar/' . $proximo_compromisso->id) . '/1", { data_inicial_adiar: $("input[name=data_inicial_adiar]").val(),  horario_inicial_adiar: $("input[name=horario_inicial_adiar]").val()}, function(dados) {
										window.location = "' . site_url('compromissos/ver_detalhes/' . $proximo_compromisso->id) . '";
										//alert("Dados: ");
									});
								}
								
								return false;
								
							});
							
							$(".cancelar").live("click", function() {
								$(this).parent().parent().css("visibility", "hidden").prev().show();
							});
							
							$(".marcar_como_realizado").live("click", function() {
								window.location = "' . site_url('compromissos/marcar_como_realizado/' . $proximo_compromisso->id) . '";
							});
							
							$(".marcar_como_pendente").live("click", function() {
								window.location = "' . site_url('compromissos/marcar_como_pendente/' . $proximo_compromisso->id) . '";
							});
							
							$.fn.colorbox({ inline: true, href: "#proximo_compromisso" });
						});
					</script>
				';

				# REMOVIDO DADOS COMPROMISSO

				$compromissos_alertados[] = $proximo_compromisso->id;
				$this->session->set_userdata('compromissos_alertados', $compromissos_alertados);
			}
			/*
			 if (strlen($proximo_compromisso->nome) > 15)
			 {
				$nome_proximo_compromisso = substr($proximo_compromisso->nome, 0, 15) . '...';
				}
				else
				{
				$nome_proximo_compromisso = $proximo_compromisso->nome;
				}*/

			//$dados['compromissos'] .= '<p style="margin-top: 10px;"><strong>Próximo:</strong> ' . $nome_proximo_compromisso . ' - Faltam ' . $this->_obter_descricao_tempo_segundos($proximo_compromisso->inicio_timestamp - time()) . ' - ' . anchor('compromissos/ver_detalhes/' . $proximo_compromisso->id, '+ Opções &raquo;') . '</p>';
		}
		/*
		 else
		 {
			$dados['compromissos'] .= '<p style="margin-top: 10px;"><strong>Próximo:</strong> não há</p>';
			}*/

		//$compromisso_mais_atrasado = $this->db->from('compromissos')->where(array('id_usuario' => $this->session->userdata('id_usuario'), 'status' => 'pendente'))->order_by('inicio_timestamp', 'asc')->limit(1)->get()->row();

		/*
		 if ($compromisso_mais_atrasado)
		 {
			if (strlen($compromisso_mais_atrasado->nome) > 15)
			{
			$nome_compromisso_mais_atrasado = substr($compromisso_mais_atrasado->nome, 0, 15) . '...';
			}
			else
			{
			$nome_compromisso_mais_atrasado = $compromisso_mais_atrasado->nome;
			}

			//$dados['compromissos'] .= '<p><strong>Pendente mais antigo:</strong> ' . $nome_compromisso_mais_atrasado . ' - Pendente há ' . $this->_obter_descricao_tempo_segundos(time() - $compromisso_mais_atrasado->inicio_timestamp) . ' - ' . anchor('compromissos/ver_detalhes/' . $compromisso_mais_atrasado->id, '+ Opções &raquo;') . '</p>';
			}
			else
			{
			$dados['compromissos'] .= '<p><strong>Pendente mais antigo:</strong> não há</p>';
			}*/
		echo json_encode($dados);
	}

	function adiar($id = NULL , $ref = null )
	{
		if (!$id)
		{
			redirect('compromissos');
		}

		//--
		if($ref)
		{
			/////////////////////////////////////////////
			$data_inicial = explode('/', $this->input->post('data_inicial_adiar'));
			$horario_inicial = explode(':', $this->input->post('horario_inicial_adiar'));
			$inicio_timestamp = mktime($horario_inicial[0], $horario_inicial[1], 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);

			/*echo '<script>';
			 echo 'alert("data_inicial:'.$data_inicial.'")';
			 echo 'alert("horario_inicial:'.$horario_inicial.'")';
			 echo 'alert("inicio_timestamp:'.$inicio_timestamp.'")';
			 echo '</script>';
			 */
			//--
			$compromisso = $this->db->from('compromissos')->where('id', $id)->get()->row();
			//$inicio_timestamp = /*$compromisso->inicio_timestamp*/ time() + $this->input->post('segundos');
			$this->db->update('compromissos', array('status' => 'reagendado', 'inicio_timestamp' => $inicio_timestamp), array('id' => $compromisso->id));
			//echo $this->db->last_query();
			$this->db->insert('reagendamentos_compromissos', array(
				'timestamp' => time(),
				'id_compromisso' => $compromisso->id,
				'id_usuario' => $this->session->userdata('id_usuario'),
				'inicio_timestamp' => $inicio_timestamp
			));
			/////////////////////////////////////////////
		}
		else{

			$data_inicial = explode('/', $this->input->post('data_inicial'));
			$horario_inicial = explode(':', $this->input->post('horario_inicial'));

			$inicio_timestamp = mktime($horario_inicial[0], $horario_inicial[1], 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);

			//--

			$compromisso = $this->db->from('compromissos')->where('id', $id)->get()->row();

			//$inicio_timestamp = /*$compromisso->inicio_timestamp*/ time() + $this->input->post('segundos');


			$this->db->update('compromissos', array('status' => 'reagendado', 'inicio_timestamp' => $inicio_timestamp), array('id' => $compromisso->id));

			//echo $this->db->last_query();

			$this->db->insert('reagendamentos_compromissos', array(
				'timestamp' => time(),
				'id_compromisso' => $compromisso->id,
				'id_usuario' => $this->session->userdata('id_usuario'),
				'inicio_timestamp' => $inicio_timestamp
			));
		}
		$compromissos_alertados = $this->session->userdata('compromissos_alertados');
		foreach($compromissos_alertados as $valor)
		{
			if($valor != $compromisso->id)
			{
				$comp_alertados[] = $valor;
			}
		}

		$this->session->set_userdata('compromissos_alertados',$comp_alertados);

	}

	function ver_detalhes($id = NULL)
	{
		if (!$id)
		{
			redirect('compromissos');
		}

		$compromisso = $this->db->from('compromissos')->where('id', $id)->get()->row();

		if (!$compromisso)
		{
			redirect('compromissos');
		}

		$usuario = $this->db->from('usuarios')->where('id', $compromisso->id_usuario)->get()->row();


		$conteudo .= '<div class="caixa"><ul>';
		$conteudo .= '<li>' . anchor('compromissos', 'Voltar', 'onclick="history.go(-1); return false;"') . '</li>';
		$conteudo .= '</ul></div>';

		/*
		 <p style="display:none">
			Adiar:<br />
			<select name="segundos" style="width: 200px;">
			<option value="' . (60 * 15) . '">15 minutos</option>
			<option value="' . (60 * 30) . '">30 minutos</option>
			<option value="' . (3600) . '">1 hora</option>
			<option value="' . (3600 * 2) . '">2 horas</option>
			<option value="' . (3600 * 24) . '">1 dia</option>
			<option value="' . (3600 * 24 * 2) . '">2 dias</option>
			<option value="' . (3600 * 24 * 7) . '">1 semana</option>
			</select>
			</p>
			*/

		$conteudo .= '
			<div style="display: none;">
				<div id="adiar">
					<h2 style="margin: 0;">Adiar Compromisso</h2>
					
					<ul>
						<li><strong>Nome:</strong> ' . $compromisso->nome . '</li>
						<li><strong>Início:</strong> ' . date('d/m/Y H:i', $compromisso->inicio_timestamp) . '</li>
						<li><strong>Descrição:</strong> ' . $compromisso->descricao . '</li>
					</ul>
					
					<div style="clear:both"></div>
					
					<p style="float:left; margin-right: 20px">
						' . form_label('Data:' . br() . form_input('data_inicial', $this->input->post('data_inicial'), 'class="datepicker" size="6"')) . '
					</p>
					
					<p style="float:left; ">
						' . form_label('Hora:' . br() . form_input('horario_inicial', $this->input->post('horario_inicial'), 'alt="time" size="1"')) . '
					</p>
					
					<div style="clear:both"></div>
		    		
	    			<p><input type="submit" value="Concluir" class="adiar" /> <input type="submit" value="Cancelar" onclick="$.fn.colorbox.close(); return false;" /></p>
				</div>
			</div>
			
			<script type="text/javascript">
				$(document).ready(function() {
					$(".adiar").live("click", function() {
					
						var validar_data = $("input[name=data_inicial]").val();
						var validar_hora = $("input[name=horario_inicial]").val();
						var regex_data = /^([0-2][0-9]|3[01])\/(0[1-9]|1[0-2])\/([12][0-9]{3})$/;
						var regex_hora = /^([01]?[0-9]|2[0-3]):[0-5]?[0-9]$/;
						
						var ex_data = validar_data.split("/");
						var ex_hora = validar_hora.split(":");
						
						if (!regex_data.test(validar_data)) {
							alert("Data incorreta!");
						}
						else if(!regex_hora.test(validar_hora))
						{
							alert("Hora incorreta!");
						}
						else if(' . $compromisso->inicio_timestamp . ' >= mktime(ex_hora[0], ex_hora[1], 0, ex_data[1], ex_data[0], ex_data[2])) {
							alert("A data digitada deve ser maior que a data atual!");
						}
						else
						{
							$.post("' . site_url('compromissos/adiar/' . $compromisso->id) . '", { data_inicial: $("input[name=data_inicial]").val(),  horario_inicial: $("input[name=horario_inicial]").val()}, function(dados) {
								window.location = "' . current_url() . '";
								//alert("Dados: " + dados);
							});
						}
						
						return false;
					});
				});
			</script>
		';

		$conteudo .= heading('Detalhes do Compromisso', 2);

		if ($compromisso->status != 'realizado')
		{
			$conteudo .= '
				<p>
					' . anchor('#', 'Adiar', 'class="botao_c" onclick="$.fn.colorbox({ inline: true, href: \'#adiar\' }); return false;"') . ' 
					 ' . anchor('compromissos/marcar_como_realizado/' . $compromisso->id, 'Realizado', 'class="botao_a"') . '
					 ' . anchor('compromissos/editar/' . $compromisso->id, 'Editar', 'class="botao_b"') . '
				</p>
				
				<div style="clear: both; height: 10px;"></div>
			';
		}

		if ($compromisso->detalhes)
		{
			$conteudo .= '
				<table cellspacing="0" class="info">
					<tr>
						<th>Início:</th>
						<td>' . date('d/m/Y H:i', $compromisso->inicio_timestamp) . '</td>
					</tr>
					<tr>
						<th>Status:</th>
						<td>' . element($compromisso->status, $this->_obter_status()) . '</td>
					</tr>
					<tr>
						<th>Usuário:</th>
						<td>' . $usuario->nome . '</td>
					</tr>
				</table>
				
				<table cellspacing="0" class="info">
					<tr>
						<th>Para:</th>
						<td>' . $this->db->from('usuarios')->where('id', $compromisso->id_usuario_cad)->get()->row()->nome . '</td>
					</tr>
					<tr>
						<th>Com:</th>
						<td>' . $compromisso->nome . '</td>
					</tr>
				</table>
				
				<div style="clear: both;"></div>
				
				<table cellspacing="0" class="info">
					<tr>
						<th valign="top">Descrição:</th>
						<td valign="top">' . $compromisso->descricao . '</td>
					</tr>
				</table>
				
				<div style="clear: both;"></div>
			';
		}
		else
		{
			$conteudo .= '
				<table cellspacing="0" class="info">
					<tr>
						<th>Início:</th>
						<td>' . date('d/m/Y H:i', $compromisso->inicio_timestamp) . '</td>
					</tr>
					<tr>
						<th>Status:</th>
						<td>' . element($compromisso->status, $this->_obter_status()) . '</td>
					</tr>
				</table>
				
				<table cellspacing="0" class="info">
					<tr>
						<th>Nome:</th>
						<td>' . $compromisso->nome . '</td>
					</tr>
					<tr>
						<th>Usuário:</th>
						<td>' . $usuario->nome . '</td>
					</tr>
				</table>
				
				<div style="clear: both;"></div>
				
				<table cellspacing="0" class="info">
					<tr>
						<th valign="top">Descrição:</th>
						<td valign="top">' . $compromisso->descricao . '</td>
					</tr>
				</table>
				
				<div style="clear: both;"></div>
			';
		}

		$reagendamentos = $this->db->from('reagendamentos_compromissos')->where('id_compromisso', $compromisso->id)->get()->result();

		if ($reagendamentos)
		{
			$conteudo .= '<div style="width: 580px;">';
			$conteudo .= heading('Reagendamentos', 3);
			$conteudo .= '<table cellspacing="0" class="novo_grid" style="margin-top: 15px;"><thead><tr><th>Reagendado em</th><th>Reagendado para</th></tr></thead><tbody>';
			foreach ($reagendamentos as $reagendamento)
			{
				$conteudo .= '<tr><td>' . date('d/m/Y H:i', $reagendamento->timestamp) . '</td><td>' . date('d/m/Y H:i', $reagendamento->inicio_timestamp) . '</td></tr>';
			}
			$conteudo .= '</tbody></table>';
			$conteudo .= '</div>';
		}

		$this->load->view('layout', array('conteudo' => $conteudo));
	}

	function index(){
		$filtros = array(
		array(
                'nome' => 'status',
                'descricao' => 'Status',
                'tipo' => 'opcoes',
        		'opcoes' => array('todos' => 'Todos', 'agendado' => 'Agendado', 'reagendado' => 'Reagendado', 'pendente' => 'Pendente', 'realizado'=>'Realizado'),
                'campo_mysql' => 'compromissos.status',
                'ordenar' => 0
		),
		array(
                'nome' => 'inicio',
                'descricao' => 'Inicio',
                'tipo' => 'data',
                'campo_mysql' => 'inicio_timestamp',
                'ordenar' => 1
		),
		array(
                'nome' => 'tipo',
                'descricao' => 'Tipo',
                'tipo' => 'opcoes',
                'opcoes' => array('todos' => 'Todas', 'Clientes' => 'Clientes', 'Prospects' => 'Prospects', 'Outros' => 'Outros'),
                'campo_mysql' => 'tipo',
                'ordenar' => 2
		),
		array(
                'nome' => 'para',
                'descricao' => 'Para',
                'tipo' => 'texto',
                'campo_mysql' => 'usuarios.nome',
                'ordenar' => 3
		),
		array(
                'nome' => 'com',
                'descricao' => 'Com',
                'tipo' => 'texto',
                'campo_mysql' => 'compromissos.nome',
                'ordenar' => 4
		),
		array(
                'nome' => 'descricao',
                'descricao' => 'Descrição',
                'tipo' => 'texto',
                'campo_mysql' => 'descricao',
                'ordenar' => 5
		)
		);
		// caso o usuário logado for um representante, vamos mostrar somente os prospects que ele mesmo fez
		//if($_GET['my_submit']){
		if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'supervisores')))
		{
			$this->db->where('usuarios.id' ,$this->session->userdata('id_usuario'));
		}


		$this->filtragem_mysql($filtros);
		$compromissos = $this->db->select('usuarios.status as usuario_status, usuarios.nome as usuario_nome, compromissos.*')
		->from('compromissos')
		->join('usuarios', 'usuarios.id = compromissos.id_usuario_cad', 'inner')->order_by('timestamp', 'desc')->limit(20, $this->input->get('per_page'))->get()->result();
		//}
		/*
		foreach ($compromissos as $compromisso){
		if(in_array($this->session->userdata('grupo_usuario'),array('gestores_comerciais', 'gestores_financeiros'))){
		$compromisso->link_imagem = ' | ' . anchor('noticias/editar/' . $noticia->id, 'Editar');
		}
		$noticia->status = element($noticia->status, $this->_obter_status());
		}
		*/
		// caso o usuário logado for um representante, vamos mostrar somente os prospects que ele mesmo fez
		//if($_GET['my_submit']){
		if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'supervisores')))
		{
			$this->db->where('usuarios.id' ,$this->session->userdata('id_usuario'));
		}

		$this->filtragem_mysql($filtros);
		$total = $this->db->from('compromissos')
		->join('usuarios', 'usuarios.id = compromissos.id_usuario', 'inner')
		->get()->num_rows();
		$paginacao = $this->paginacao($total);
		//}
		$this->load->view('layout', array('conteudo' => $this->load->view('compromissos/index', array('compromissos' => $compromissos, 'filtragem' => $this->filtragem($filtros), 'paginacao' => $paginacao,'total'=>$total), TRUE)));

	}

	function criar($cod_representante = NULL)
	{

		if(!$cod_representante)
		{
			if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'supervisores'))) {
				redirect('compromissos/usuario');
			}else{
				$cod_usuario = $this->session->userdata('id_usuario');
				$_representante = $this->db->from('usuarios')->where('id', $cod_usuario)->get()->row();
				$cod_representante = $_representante->codigo;
				$grupo_usuario = 'representantes';
				$cod_user_representante = $cod_usuario;
			}
		}
		else
		{
			$cod_user_representante = $cod_representante;
			$cod_usuario = $this->db->from('usuarios')->where(array('id' => $cod_representante, 'grupo' => 'representantes'))->get()->row()->codigo;
			$grupo_usuario = $this->db->from('usuarios')->where(array('id' => $cod_representante))->get()->row()->grupo;

			if($cod_usuario == NULL)
			{
				$cod_usuario = $cod_representante;
			}

			$cod_representante = $cod_usuario;
		}


		if ($_POST)
		{

			if($grupo_usuario != 'representantes')
			{
				$_POST["compromisso_para"] = 'Outros';
				$_POST["nome"] = $this->db->from('usuarios')->where('id', $cod_representante)->get()->row()->nome;
			}


			if (!$this->input->post('compromisso_para'))
			{
				$erro = 'Selecione uma opção para compromisso.';
			}
			else if (!$this->input->post('nome'))
			{
				$erro = 'Selecione ou Digite para quem será agendado o compromisso.';
			}
			else if (!$this->input->post('descricao'))
			{
				$erro = 'Digite uma descrição.';
			}
			else if (!$this->_validar_data($this->input->post('data_inicial')))
			{
				$erro = 'Digite uma data inicial válida.';
			}
			else if (!$this->_validar_horario($this->input->post('horario_inicial')))
			{
				$erro = 'Digite um horário inicial válido.';
			}
			else
			{
				$data_inicial = explode('/', $this->input->post('data_inicial'));
				$horario_inicial = explode(':', $this->input->post('horario_inicial'));

				$inicio_timestamp = mktime($horario_inicial[0], $horario_inicial[1], 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);


				if($this->input->post('compromisso_para') == "Cliente")
				{
					$codigo_tipo = $this->input->post('nome');
					$_cliente = explode('|', $this->input->post('nome'));
					$_POST['nome'] = $this->_obter_nome_cliente($_cliente[0], $_cliente[1]);
				}
				else if($this->input->post('compromisso_para') == "Prospects")
				{
					$codigo_tipo = $this->input->post('nome');
					$prospect = $this->db_cliente->obter_prospect($codigo_tipo);
					$_POST['nome'] = $prospect['nome'];

				}


				$_insert = array();

				if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'supervisores'))) {
					if($grupo_usuario == 'representantes')
					{
						//$_POST['nome'] =  $this->db->from('usuarios')->where('id', $cod_user_representante)->get()->row()->nome . ' - ' . $_POST['nome'];
						$_insert['detalhes'] = TRUE;
					}
				}


				$_insert['timestamp'] = time();
				$_insert['id_usuario'] = $this->input->post('id_usuario') ? $this->input->post('id_usuario') : $this->session->userdata('id_usuario');
				$_insert['status'] = 'agendado';
				$_insert['nome'] = $this->input->post('nome');
				$_insert['tipo'] = $this->input->post('compromisso_para');
				$_insert['codigo_tipo'] = $codigo_tipo;
				$_insert['descricao'] = $this->input->post('descricao');
				$_insert['id_usuario_cad'] = $cod_user_representante;
				$_insert['id_representante'] = $cod_representante;
				$_insert['inicio_timestamp'] = $inicio_timestamp;


				if($this->db->insert('compromissos', $_insert)){

					redirect('compromissos');

				}else{

					echo '
					<script type="text/javascript">
						<!--
							alert("Falha ao incluir compromisso!");
						//-->
					</script>
					';
					redirect('compromissos/ver_detalhes/' . $this->db->insert_id());
				}

				/*
				 $this->db->insert('compromissos', array(
					'timestamp' => time(),
					'id_usuario' => $this->input->post('id_usuario') ? $this->input->post('id_usuario') : $this->session->userdata('id_usuario'),
					'status' => 'agendado',
					'nome' => $this->input->post('nome'),
					'tipo' => $this->input->post('compromisso_para'),
					'codigo_tipo' => $codigo_tipo ,
					'descricao' => $this->input->post('descricao'),
					'id_usuario_cad' => $cod_user_representante,
					'id_representante' => $cod_representante,
					'inicio_timestamp' => $inicio_timestamp
					));
					*/

				//redirect('compromissos/ver_detalhes/' . $this->db->insert_id());
				//redirect('compromissos');

			}
		}
		$conteudo .= '
			<div class="caixa">
				<ul>
					<li>' . anchor('compromissos', 'Voltar', 'onclick="history.go(-1); return false;"') . '</li>
				</ul>
			</div>
		';
		$conteudo .= heading('Cadastrar Compromisso', 2);
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';

		$conteudo .= form_open(current_url());

		$conteudo .= '
			<script type="text/javascript">
				$(document).ready(function(){
				
					$("#representante").change(function(){
						$("#para").html();
					});
				

					$(".compromisso_para").click(function(){
						
						if($(this).val() == "Cliente")
						{
							var representante = "'. $cod_representante .'";
							
							$.ajax({
								type: "POST",
								url: "' . base_url() . 'index.php/compromissos/obter_clientes",
								data: {representante: representante, nome: "' . $this->input->post('nome') . '"},
								success: function(dados){
									$("#para").html(dados);
								}
							});
						}
						else if($(this).val() == "Prospects")
						{
							
							var representante = "' . $cod_representante . '";
							
							$.ajax({
								type: "POST",
								url: "' . base_url() . 'index.php/compromissos/obter_prospects",
								data: {representante: representante, nome: "' . $this->input->post('nome') . '"},
								success: function(dados){
									$("#para").html(dados);
								}
							});
							
							
						}
						else if($(this).val() == "Outros")
						{
							$.ajax({
								type: "POST",
								url: "' . base_url() . 'index.php/compromissos/obter_prospects",
								
								success: function(dados){
									$("#para").html("<p><label>Com: <br /><input type=\"text\" name=\"nome\" value=\"' . $_POST["nome"] . '\" size=\"77\" /></label></p>");
								}
							});
						}
						
					});';

		$_tipo = $this->input->post('compromisso_para');
			
		if($_tipo)
		{

			if($_tipo == "Cliente"){
				$_p = 0;
			}
			else if($_tipo == "Prospects")
			{
				$_p = 1;
			}
			else if($_tipo == "Outros")
			{
				$_p = 2;
			}

			$conteudo .= '$(".compromisso_para").eq("' . $_p . '").trigger("click");';

		}

		$conteudo .= '});
			</script>
		';







		if($grupo_usuario == 'representantes')
		{
			if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'supervisores')))
			{
				$_para_quem = $cod_user_representante;
				if($_para_quem)
				{
					$conteudo .= '<p>Para: ' . $this->db->from('usuarios')->where('id', $_para_quem)->get()->row()->nome  . '</p>';
				}
			}

			$conteudo .= '<p style="float:left; margin-right: 10px">' . form_label(form_radio('compromisso_para', 'Cliente', $this->input->post('compromisso_para') == 'Cliente' ? TRUE : FALSE, 'class="compromisso_para"') . ' Cliente') . '</p>';
			$conteudo .= '<p style="float:left; margin-right: 10px">' . form_label(form_radio('compromisso_para', 'Prospects', $this->input->post('compromisso_para') == 'Prospects' ? TRUE : FALSE, 'class="compromisso_para"') . ' Prospects') . '</p>';
			$conteudo .= '<p style="float:left; margin-right: 10px">' . form_label(form_radio('compromisso_para', 'Outros', $this->input->post('compromisso_para') == 'Outros' ? TRUE : FALSE, 'class="compromisso_para"') . ' Diversos/Outros') . '</p>';
		}
		else
		{
			$conteudo .= '<p>Para: ' . $this->db->from('usuarios')->where('id', $cod_representante)->get()->row()->nome . '</p>';
		}

		/*
		 else
		 {
			$conteudo .= '
			<script type="text/javascript">
			$(document).ready(function(){
			$(".compromisso_para").trigger("click");
			});
			</script>';
			}
			*/



		$conteudo .= '<div style="clear:both"></div>';

		$conteudo .= '<div id="para"></div>';

		/*
		 if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros'))) {
			$conteudo .= '<p>' . form_label('Para:' . br() . form_dropdown('id_usuario', $this->_obter_usuarios(), $this->input->post('id_usuario') ? $this->input->post('id_usuario') : $this->session->userdata('id_usuario'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" style="width: 400px;"')) . '</p>';
			$conteudo .= '<p>' . form_label('Nome do compromisso:' . br() . form_input('nome', $this->input->post('nome'))) . '</p>';
			} else {
			$conteudo .= '<p>' . form_label('Nome:' . br() . form_input('nome', $this->input->post('nome'))) . '</p>';
			}
			*/

		//$conteudo .= '<p>' . 'Descrição:' . br() . '<div dojoType="dijit.Editor" extraPlugins="[\'foreColor\', \'fontSize\']" onchange="$(\'input[name=descricao]\').val(arguments[0]);" >' . $this->input->post('descricao') . '</div>' . form_hidden('descricao', $this->input->post('descricao')) . '</p>';
		$conteudo .= '<p>' . form_label('Descrição:' . br() . form_textarea(array('class' => 'editor', 'cols' => 60, 'rows' => 8, 'name' => 'descricao', 'value' => $this->input->post('descricao')))) . '</p>';

		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Data:' . br() . form_input('data_inicial', $this->input->post('data_inicial'), 'class="datepicker_data_minima_hoje " size="6"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Hora:' . br() . form_input('horario_inicial', $this->input->post('horario_inicial'), 'alt="time" size="1"')) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '<p>' . form_submit('concluir', 'Concluir') . ' ou ' . anchor('compromissos', 'Cancelar') . '</p>';
		$conteudo .= form_close();

		$this->load->view('layout', array('conteudo' => $conteudo));
	}

	function editar($id = NULL)
	{
		if (!$id)
		{
			redirect();
		}

		$compromisso = $this->db->from('compromissos')->where('id', $id)->get()->row();

		$cod_representante = $compromisso->id_representante;
		$cod_user_representante = $compromisso->id_usuario_cad;

		$usuario = $this->db->from('usuarios')->where('id', $compromisso->id_usuario)->get()->row();

		if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'supervisores')))
		{
			$grupo_usuario = $this->db->from('usuarios')->where(array('id' => $cod_user_representante ))->get()->row()->grupo;
		}
		else
		{
			$grupo_usuario = 'representantes';
		}


		if ($_POST)
		{
			if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'supervisores'))) {

				if($grupo_usuario != 'representantes')
				{
					$_POST["nome"] = $compromisso->nome;
				}
			}


			if($grupo_usuario != 'representantes')
			{
				$_POST["compromisso_para"] = 'Outros';
			}

			if (!$this->input->post('nome'))
			{
				$erro = 'Digite um nome.';
			}
			else if (!$this->input->post('descricao'))
			{
				$erro = 'Digite uma descrição.';
			}
			else
			{
					
				if($this->input->post('compromisso_para') == "Cliente")
				{
					$codigo_tipo = $this->input->post('nome');
					$_cliente = explode('|', $this->input->post('nome'));
					$_POST['nome'] = $this->_obter_nome_cliente($_cliente[0], $_cliente[1]);
				}
				else if($this->input->post('compromisso_para') == "Prospects")
				{
					$codigo_tipo = $this->input->post('nome');
					$_POST['nome'] = $this->db->from('prospects')->where('id', $codigo_tipo)->get()->row()->nome;
				}
				else
				{
					$codigo_tipo = NULL;
				}

				/*
				 if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros'))) {
					if($grupo_usuario == 'representantes')
					{
					$_POST['nome'] =  $this->db->from('usuarios')->where('id', $cod_user_representante)->get()->row()->nome . ' - ' . $_POST['nome'];
					}
					}
					*/

					
				$this->db->update('compromissos', array(
					'id_usuario' => $this->input->post('id_usuario') ? $this->input->post('id_usuario') : $this->session->userdata('id_usuario'),
					'nome' => $this->input->post('nome'),
					'tipo' => $this->input->post('compromisso_para'),
					'codigo_tipo' => $codigo_tipo,
					'descricao' => $this->input->post('descricao'),
					'id_representante' => $cod_representante
				), array('id' => $id));

				redirect('compromissos/ver_detalhes/' . $compromisso->id);
			}
		}
		else
		{
			foreach ($compromisso as $indice => $valor)
			{
				$_POST[$indice] = $valor;
			}
		}

		$conteudo .= heading('Editar Compromisso', 2);
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		$conteudo .= form_open(current_url());

		$_POST["nome"] = $compromisso->codigo_tipo;

		$conteudo .= '
			<script type="text/javascript">
				$(document).ready(function(){
				
					$("#representante").change(function(){
						$("#para").html();
					});
				

					$(".compromisso_para").click(function(){
						
						if($(this).val() == "Cliente")
						{
							var representante = "' . $cod_representante . '";
							
							$.ajax({
								type: "POST",
								url: "' . base_url() . 'index.php/compromissos/obter_clientes",
								data: {representante: representante, nome: "' . $this->input->post('nome') . '"},
								success: function(dados){
									$("#para").html(dados);
								}
							});
						}
						else if($(this).val() == "Prospects")
						{
							
							var representante = "' . $cod_representante . '";
							
							$.ajax({
								type: "POST",
								url: "' . base_url() . 'index.php/compromissos/obter_prospects",
								data: {representante: representante, nome: "' . $this->input->post('nome') . '"},
								success: function(dados){
									$("#para").html(dados);
								}
							});
							
							
						}
						else if($(this).val() == "Outros")
						{
							$.ajax({
								type: "POST",
								url: "' . base_url() . 'index.php/compromissos/obter_prospects",
								
								success: function(dados){
									$("#para").html("<p><label>Para:<br /><input type=\"text\" name=\"nome\" value=\"' .  $compromisso->nome . '\" size=\"77\" /></label></p>");
								}
							});
						}
						
					});';
			
		$_tipo = $compromisso->tipo;
			
		if($_tipo == "Cliente"){
			$_p = 0;
		}
		else if($_tipo == "Prospects")
		{
			$_p = 1;
		}
		else if($_tipo == "Outros")
		{
			$_p = 2;
		}
			
		$conteudo .= '$(".compromisso_para").eq("' . $_p . '").trigger("click");
				
				});
			</script>
		';

		if($grupo_usuario == 'representantes')
		{
			if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'supervisores')))
			{
				$_para_quem = $cod_user_representante;
				if($_para_quem)
				{
					$conteudo .= '<p>Para: ' . $this->db->from('usuarios')->where('id', $_para_quem)->get()->row()->nome  . '</p>';
				}
			}

			$conteudo .= '<p style="float:left; margin-right: 10px">' . form_label(form_radio('compromisso_para', 'Cliente', $this->input->post('compromisso_para') == 'Cliente' ? TRUE : FALSE, 'class="compromisso_para"') . ' Cliente') . '</p>';
			$conteudo .= '<p style="float:left; margin-right: 10px">' . form_label(form_radio('compromisso_para', 'Prospects', $this->input->post('compromisso_para') == 'Prospects' ? TRUE : FALSE, 'class="compromisso_para"') . ' Prospects') . '</p>';
			$conteudo .= '<p style="float:left; margin-right: 10px">' . form_label(form_radio('compromisso_para', 'Outros', $this->input->post('compromisso_para') == 'Outros' ? TRUE : FALSE, 'class="compromisso_para"') . ' Diversos/Outros') . '</p>';
		}
		else
		{
			$conteudo .= '<p>Para: ' . $this->db->from('usuarios')->where('id', $cod_representante)->get()->row()->nome . '</p>';
		}


		$conteudo .= '<div style="clear:both"></div>';

		$conteudo .= '<div id="para"></div>';

		/*
		 if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros'))) {
			$conteudo .= '<p>' . form_label('Para:' . br() . form_dropdown('id_usuario', $this->_obter_usuarios(), $this->input->post('id_usuario') ? $this->input->post('id_usuario') : $this->session->userdata('id_usuario'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" style="width: 400px;"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Nome do compromisso:' . br() . form_input('nome', $this->input->post('nome'))) . '</p>';
			$conteudo .= '<div style="clear: both;"></div>';
			} else {
			$conteudo .= '<p>' . form_label('Nome:' . br() . form_input('nome', $this->input->post('nome'))) . '</p>';
			}
			*/

		//$conteudo .= '<p>' . 'Descrição:' . br() . '<div dojoType="dijit.Editor" extraPlugins="[\'foreColor\', \'fontSize\']" onchange="$(\'input[name=descricao]\').val(arguments[0]);" >' . $this->input->post('descricao') . '</div>' . form_hidden('descricao', $this->input->post('descricao')) . '</p>';
		$conteudo .= '<p>' . form_label('Conteúdo:' . br() . form_textarea(array('class' => 'editor', 'cols' => 60, 'rows' => 8, 'name' => 'descricao', 'value' => $this->input->post('descricao')))) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '<p>' . form_submit('concluir', 'Concluir') . ' ou ' . anchor('#', 'Cancelar', 'onclick="history.go(-1); return false;"') . '</p>';
		$conteudo .= form_close();

		$this->load->view('layout', array('conteudo' => $conteudo));
	}

	function marcar_como_realizado($id = NULL)
	{
		// TODO: permissões

		if (!$id)
		{
			redirect('compromissos');
		}

		$compromisso = $this->db->from('compromissos')->where('id', $id)->get()->row();

		$this->db->update('compromissos', array('status' => 'realizado'), array('id' => $compromisso->id));

		redirect('compromissos/ver_detalhes/' . $compromisso->id);
	}

	function marcar_como_pendente($id = NULL)
	{
		// TODO: permissões

		if (!$id)
		{
			redirect('compromissos');
		}

		$compromisso = $this->db->from('compromissos')->where('id', $id)->get()->row();

		$this->db->update('compromissos', array('status' => 'pendente'), array('id' => $compromisso->id));

		redirect('compromissos/ver_detalhes/' . $compromisso->id);
	}

	function usuario()
	{

		if($_POST)
		{

			redirect('compromissos/criar/' . $_POST['usuario']);

		}

		$conteudo .= '
			<div class="caixa">
				<ul>
					<li>' . anchor('compromissos', 'Voltar', 'onclick="history.go(-1); return false;"') . '</li>
				</ul>
			</div>
		';
		$conteudo .= heading('Cadastrar Compromisso', 2);

		$conteudo .= '
		<script type="text/javascript">
			$(document).ready(function(){
				$("option:selected").css({
					"color":"#088F00"
				})
			});
		</script>';

		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		$conteudo .= form_open(current_url());
		$id_usuario = $this->session->userdata('id_usuario');
		$conteudo .= '<p>' . form_label('Para quem:' . br() . form_dropdown('usuario', $this->_obter_usuarios($id_usuario), $id_usuario)) . '</p>';
		$conteudo .= '<p>' . form_submit('continuar', 'Continuar') . '</p>';
		$conteudo .= form_close();

		$this->load->view('layout', array('conteudo' => $conteudo));

	}

	function relatorio_usuarios()
	{

		if($_POST)
		{

			if($this->input->post('usuario'))
			{
				$this->db->where('id_usuario_cad', $this->input->post('usuario'));
			}

			if($this->input->post('status'))
			{
				if($this->input->post('status') != 'todos')
				{
					$this->db->where('status', $this->input->post('status'));
				}
			}

			//DATA
			if ($this->input->post('data_inicial'))
			{
				$data_inicial = explode('/', $this->input->post('data_inicial'));
				$this->db->where('inicio_timestamp >=', mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]));
			}

			if ($this->input->post('data_final'))
			{
				$data_final = explode('/', $this->input->post('data_final'));
				$this->db->where('inicio_timestamp <=', mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]));
			}

		}

		$compromissos = $this->db->from('compromissos')->get()->result();

		$conteudo .= heading('Relatório de Agendamento', 2);

		$conteudo .= form_open(current_url());
		$conteudo .= '<p style="float:left">' . form_label('Usuários:' . br() . form_dropdown('usuario', $this->_obter_usuarios(), $this->input->post('usuario'))) . '</p>';
		$conteudo .= '<p style="float:left; margin-left:20px">' . form_label('Status:' . br() . form_dropdown('status', $this->_obter_status(TRUE), $this->input->post('status'))) . '</p>';

		$conteudo .= '<div style="clear:both"></div>';

		$conteudo .= '<p style="margin-right: 10px;">' . form_label('Data: ' . br() . form_input('data_inicial', $this->input->post('data_inicial'), 'class="datepicker data_inicial" size="6"')) . ' à ' . form_input('data_final', $this->input->post('data_final'), 'class="datepicker" size="6"') . '</p>';
		$conteudo .= '<p>' . form_submit('gerar_relatorio', 'Gerrar Relatório') . '</p>';
		$conteudo .= form_close();

		if($compromissos)
		{
			$conteudo .= '<table class="hnordt">';
			$conteudo .= '<thead>
								<tr>
									<th>Data de Cadastro</th>
									<th>Tipo</th>
									<th>Nome</th>
									<th>Status</th>
									<th>Data do Compromisso</th>
								</tr>
						  </thead>';
			$conteudo .= '<tbody>';
			foreach($compromissos as $compromisso)
			{
				$conteudo .= '<tr>';
				$conteudo .= '<td>' . date('d/m/Y G:i:s', $compromisso->timestamp) . '</td>';
				$conteudo .= '<td>' . $compromisso->tipo . '</td>';
				$conteudo .= '<td>' . $compromisso->nome . '</td>';
				$conteudo .= '<td>' . $compromisso->status . '</td>';
				$conteudo .= '<td align="right">' . date('d/m/Y G:i:s', $compromisso->inicio_timestamp) . '</td>';
				$conteudo .= '</tr>';
			}
			$conteudo .= '</tbody>';
			$conteudo .= '</table>';
		}

		$this->load->view('layout', array('conteudo' => $conteudo));
	}


	function ver_agendamentos($tipo = NULL, $codigo = NULL, $loja = NULL)
	{



		//Buscando Compromissos

		if($tipo == 'cliente')
		{
			$compromissos = $this->db->from('compromissos')->where(array('tipo' => 'Cliente', 'codigo_tipo' => $codigo . '|' . $loja))->limit(20, $this->input->get('per_page'))->get()->result();

		}
		else
		{
			$compromissos = $this->db->from('compromissos')->where(array('tipo' => 'Prospects', 'codigo_tipo' => $codigo))->limit(20, $this->input->get('per_page'))->get()->result();
		}


		//Buscando Totais

		if($tipo == 'cliente')
		{
			$total = $this->db->from('compromissos')->where(array('tipo' => 'Cliente', 'codigo_tipo' => $codigo . '|' . $loja))->get()->num_rows();

			$nome = $this->_obter_nome_cliente($codigo, $loja);
		}
		else
		{
			$total = $this->db->from('compromissos')->where(array('tipo' => 'Prospects', 'codigo_tipo' => $codigo))->get()->num_rows();

			$nome = $this->db->from('prospects')->where('id', $codigo)->get()->row()->nome;
		}


		$paginacao = $this->paginacao($total);

		$this->load->view('layout', array('conteudo' => $this->load->view('compromissos/ver_agendamentos',
		array(
				'compromissos' => $compromissos,
				'nome' => $nome,
				'paginacao' => $paginacao, 
				'total' => $total
		), TRUE)
		));

	}

	//

	function _obter_representantes()
	{

		$representantes = $this->db->from('usuarios')->where('status', 'ativo')->where('grupo', 'representantes')->order_by('nome', 'asc')->get()->result();
		$_representantes = array();

		foreach ($representantes as $representante)
		{
			$_representantes[$representante->codigo] = $representante->codigo . ' - ' . $representante->nome;
		}

		return $_representantes;

	}

	function _obter_status($todos = NULL)
	{

		if($todos == TRUE)
		{
			$status['todos'] = 'Todos';
		}

		$status['agendado'] = 'Agendado';
		$status['reagendado'] = 'Reagendado';
		$status['realizado'] = 'Realizado';
		$status['pendente'] = 'Pendente';

		return $status;

		//return array('agendado' => 'Agendado', 'reagendado' => 'Reagendado', 'realizado' => 'Realizado', 'pendente' => 'Pendente');
	}

	function _obter_usuarios($id_usuario = NULL)
	{
		$usuarios = $this->db->from('usuarios')->where('status', 'ativo')->order_by('grupo', 'desc')->order_by('nome', 'asc')->get()->result();
		$_usuarios = array();

		foreach ($usuarios as $usuario)
		{
			if($id_usuario)
			{

				if($id_usuario == $usuario->id)
				{
					$_usuarios[$usuario->id] = $usuario->nome . ' (Para Mim)';
				}
				else
				{
					$_usuarios[$usuario->id] = $usuario->nome . ' (grupo ' . element($usuario->grupo, $this->_obter_grupos()) . ')';
				}
					
			}
			else
			{
				$_usuarios[$usuario->id] = $usuario->nome . ' (grupo ' . element($usuario->grupo, $this->_obter_grupos()) . ')';
			}
		}

		return $_usuarios;
	}

	function _obter_grupos()
	{
		return array('administradores' => 'Administradores', 'gestores_comerciais' => 'Gestores Comerciais', 'gestores_financeiros' => 'Gestores Financeiros', 'representantes' => 'Representantes', 'prepostos' => 'Prepostos', 'supervisores' => 'Supervisores');
	}

	function obter_prospects() {

		$representante        = $_POST["representante"];
		/*$this->db_cliente     = $this->load->database('db_cliente', TRUE);
		 $this->_db_cliente     = $this->config->item('db_cliente');*/

		if ($representante) {
			$prospects = $this->db_cliente->obter_prospects(' ',$representante);

			$_prospects = array();

			if ($prospects) {

				foreach ($prospects as $prospect) {
					$_prospects[$prospect['codigo']] = $prospect['nome'];
				}
			} else {
				$_prospects[] = "Nenhum registro foi encontrado";
				$desabilitar = "READONLY DISABLED";
			}

			echo '<p>' . form_label('Com: ' . br() . form_dropdown('nome', $_prospects, $_POST["nome"] ? $_POST["nome"] : $this->session->userdata('nome'), $desabilitar . ' dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" style="width: 400px;"')) . '</p>';
		}
	}	

	function obter_clientes(){

		$cod_representante = $_POST["representante"];

		if($cod_representante)
		{
			$clientes = $this->db_cliente->obter_clientes($cod_representante);

			$_clientes = array();

			if($clientes)
			{
					
				foreach($clientes as $cliente)
				{
					$_clientes[$cliente['codigo'] . '|' . $cliente['loja']] =  $cliente['nome'] . ' - Loja (' . $cliente['loja'] . ')';
				}
					
			}
			else
			{
				$_clientes[] = "Nenhum registro foi encontrado";
				$desabilitar = "READONLY DISABLED";
			}

			echo '<p>' . form_label('Com: ' . br() . form_dropdown('nome', $_clientes, $_POST["nome"] ? $_POST["nome"] : $this->session->userdata('nome'), $desabilitar . ' dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" style="width: 400px;"')) . '</p>';

		}
	}

	function _obter_nome_cliente($cod_cliente, $cod_loja)
	{
		$cliente = $this->db_cliente->obter_nome_cliente($cod_cliente, $cod_loja);

		return $cliente . ' - Loja (' . $cod_loja . ')';
	}


}