﻿<?php

class Pedidos extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
		
	}
	
	function index()
	{
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->config->load('db_cliente_' . $this->db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');
		
		$filtros = array(
			array(
				'nome' => 'status',
				'descricao' => 'Status',
				'tipo' => 'opcoes',
				'opcoes' => array('todos' => 'Todos', 'aguardando_faturamento' => 'Aguardando Faturamento', 'parcialmente_faturado' => 'Parcialmente Faturado', 'faturado' => 'Faturado')
			),
			in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) ? array(
				'nome' => 'representante',
				'descricao' => 'Representante',
				'tipo' => 'texto',
				'campo_mssql' => 'A3_NOME',
				'ordenar_ms' => 1
			) : NULL,
			array(
				'nome' => 'cliente',
				'descricao' => 'Cliente',
				'tipo' => 'texto',
				'campo_mssql' => 'A1_NOME',
				'ordenar_ms' => 2
			),
			/*array(
				'nome' => 'ordem_de_compra',
				'descricao' => 'Ordem de compra',
				'tipo' => 'texto',
				'campo_mssql' => 'C6_PEDCLI',
				'ordenar_ms' => 2
			),*/
			array(
				'nome' => 'codigo_do_pedido',
				'descricao' => 'Código do pedido',
				'tipo' => 'texto',
				'campo_mssql' => 'C6_NUM',
				'ordenar_ms' => 3
			),
			array(
				'nome' => 'emissao',
				'descricao' => 'Emissão',
				'tipo' => 'data',
				'campo_mssql' => 'C5_EMISSAO',
				'ordenar_ms' => 4
			)
		);
		
		// para que a paginação no mssql funcione, devemos ordenar de alguma forma, caso o usuário não escolher uma ordenação, vamos fazer manualmente
		if (!$this->input->get('ordenar_ms'))
		{
			// vamos setar o $_GET ao invés de usar o order_by()
			// usando o order_by a flexinha no layout indicando o que está sendo ordenado não apareceria para o usuário
			$_GET['ordenar_ms'] = 'C5_EMISSAO';
			$_GET['ordenar_ms_tipo'] = 'desc';
		}
		
		// ** FILTRAGEM PARA EXIBIÇÃO DOS RESULTADOS
		
		// o campo status precisa ser filtrado "manualmente" pois ele não existe no banco de dados
		if ($this->input->get('status'))
		{
			switch ($this->input->get('status'))
			{
				case 'aguardando_faturamento':
					$this->db_cliente->where('C6_QTDENT', 0);
				break;
				
				case 'parcialmente_faturado':
					$this->db_cliente->where('C6_QTDENT > 0 AND C6_QTDENT < C6_QTDVEN');
				break;
				
				case 'faturado':
					$this->db_cliente->where('C6_QTDENT > 0 AND C6_QTDENT = C6_QTDVEN');
				break;
			}
		}
		
		// caso o usuário logado for um representante, vamos mostrar somente os pedidos que ele mesmo fez
		if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')))
		{
			$this->db_cliente->where('C5_VEND1', $this->session->userdata('codigo_usuario'));
		}
		
		$this->filtragem_mssql($filtros);
		
		$pedidos = $this->db_cliente->distinct()->select('C5_VEND1, C6_NUM, C6_PEDCLI, C5_EMISSAO, C6_CLI, C6_LOJA, A1_NOME, A3_NOME, (CASE WHEN SUM(C6_QTDENT) <= 0 THEN "Aguardando Faturamento" WHEN SUM(C6_QTDENT) < SUM(C6_QTDVEN) THEN "Parcialmente Faturado" ELSE "Faturado" END) AS status, SUM(C6_QTDVEN) AS C6_QTDVEN, SUM(C6_QTDENT) AS C6_QTDENT, SUM(C6_PRCVEN * C6_QTDVEN) AS total, SUM(C6_PRCVEN * C6_QTDENT) AS total_parcial')
						->from($this->_db_cliente['tabelas']['pedidos'] . ', ' . $this->_db_cliente['tabelas']['itens_pedidos'] . ', ' . $this->_db_cliente['tabelas']['clientes'] . ', ' . $this->_db_cliente['tabelas']['representantes'])
						->where(array(
							$this->_db_cliente['tabelas']['itens_pedidos'] . '.C6_NUM = ' . $this->_db_cliente['tabelas']['pedidos'] . '.C5_NUM' => NULL,
							$this->_db_cliente['tabelas']['clientes'] . '.A1_COD = ' . $this->_db_cliente['tabelas']['itens_pedidos'] . '.C6_CLI' => NULL,
							$this->_db_cliente['tabelas']['clientes'] . '.A1_LOJA = ' . $this->_db_cliente['tabelas']['itens_pedidos'] . '.C6_LOJA' => NULL,
							$this->_db_cliente['tabelas']['representantes'] . '.A3_COD = ' . $this->_db_cliente['tabelas']['pedidos'] . '.C5_VEND1' => NULL,
							$this->_db_cliente['tabelas']['representantes'] . '.D_E_L_E_T_ !=' => '*',
							$this->_db_cliente['tabelas']['pedidos'] . '.D_E_L_E_T_ !=' => '*',
							$this->_db_cliente['tabelas']['pedidos'] . '.C5_FILIAL' => '01'
						))
						->limit(20, $this->input->get('per_page'))
						->group_by('C5_VEND1, C6_NUM, C6_PEDCLI, C5_EMISSAO, C6_CLI, C6_LOJA, A1_NOME, A3_NOME')
						->get()->result_array();
		
		// FILTRAGEM PARA EXIBIÇÃO DOS RESULTADOS **
		
		// ** FILTRAGEM PARA PAGINAÇÃO
		
		// o campo status precisa ser filtrado "manualmente" pois ele não existe no banco de dados
		if ($this->input->get('status'))
		{
			switch ($this->input->get('status'))
			{
				case 'aguardando_faturamento':
					$this->db_cliente->where('C6_QTDENT', 0);
				break;
				
				case 'parcialmente_faturado':
					$this->db_cliente->where('C6_QTDENT > 0 AND C6_QTDENT < C6_QTDVEN');
				break;
				
				case 'faturado':
					$this->db_cliente->where('C6_QTDENT > 0 AND C6_QTDENT = C6_QTDVEN');
				break;
			}
		}
		
		
		// caso o usuário logado for um representante, vamos mostrar somente os pedidos que ele mesmo fez
		if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')))
		{
			$this->db_cliente->where('C5_VEND1', $this->session->userdata('codigo_usuario'));
		}
		
		$this->filtragem_mssql($filtros, FALSE);
		
		
		$total = $this->db_cliente->select('COUNT(DISTINCT C6_NUM) AS total')
						->from($this->_db_cliente['tabelas']['itens_pedidos'] . ', ' . $this->_db_cliente['tabelas']['pedidos'] . ', ' . $this->_db_cliente['tabelas']['clientes'] . ', ' . $this->_db_cliente['tabelas']['representantes'])
						->where(array(
							$this->_db_cliente['tabelas']['itens_pedidos'] . '.C6_NUM = ' . $this->_db_cliente['tabelas']['pedidos'] . '.C5_NUM' => NULL,
							$this->_db_cliente['tabelas']['clientes'] . '.A1_COD = ' . $this->_db_cliente['tabelas']['itens_pedidos'] . '.C6_CLI' => NULL,
							$this->_db_cliente['tabelas']['clientes'] . '.A1_LOJA = ' . $this->_db_cliente['tabelas']['itens_pedidos'] . '.C6_LOJA' => NULL,
							$this->_db_cliente['tabelas']['representantes'] . '.A3_COD = ' . $this->_db_cliente['tabelas']['pedidos'] . '.C5_VEND1' => NULL,
							$this->_db_cliente['tabelas']['representantes'] . '.D_E_L_E_T_ !=' => '*',
							$this->_db_cliente['tabelas']['pedidos'] . '.D_E_L_E_T_ !=' => '*',
							$this->_db_cliente['tabelas']['pedidos'] . '.C5_FILIAL' => '01'
						))
						->get()->result_array();
		
		$total = $total[0]['total'];
		
		//
		
		
		// o campo status precisa ser filtrado "manualmente" pois ele não existe no banco de dados
		if ($this->input->get('status'))
		{
			switch ($this->input->get('status'))
			{
				case 'aguardando_faturamento':
					$this->db_cliente->where('C6_QTDENT', 0);
				break;
				
				case 'parcialmente_faturado':
					$this->db_cliente->where('C6_QTDENT > 0 AND C6_QTDENT < C6_QTDVEN');
				break;
				
				case 'faturado':
					$this->db_cliente->where('C6_QTDENT > 0 AND C6_QTDENT = C6_QTDVEN');
				break;
			}
		}
		
		// caso o usuário logado for um representante, vamos mostrar somente os pedidos que ele mesmo fez
		if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')))
		{
			$this->db_cliente->where('C5_VEND1', $this->session->userdata('codigo_usuario'));
		}
		
		$this->filtragem_mssql($filtros, FALSE);
		
		$total_2 = $this->db_cliente->select('SUM(C6_PRCVEN * C6_QTDVEN) AS valor')
						->from($this->_db_cliente['tabelas']['itens_pedidos'] . ', ' . $this->_db_cliente['tabelas']['pedidos'] . ', ' . $this->_db_cliente['tabelas']['clientes'] . ', ' . $this->_db_cliente['tabelas']['representantes'])
						->where(array(
							$this->_db_cliente['tabelas']['itens_pedidos'] . '.C6_NUM = ' . $this->_db_cliente['tabelas']['pedidos'] . '.C5_NUM' => NULL,
							$this->_db_cliente['tabelas']['clientes'] . '.A1_COD = ' . $this->_db_cliente['tabelas']['itens_pedidos'] . '.C6_CLI' => NULL,
							$this->_db_cliente['tabelas']['clientes'] . '.A1_LOJA = ' . $this->_db_cliente['tabelas']['itens_pedidos'] . '.C6_LOJA' => NULL,
							$this->_db_cliente['tabelas']['representantes'] . '.A3_COD = ' . $this->_db_cliente['tabelas']['pedidos'] . '.C5_VEND1' => NULL,
							$this->_db_cliente['tabelas']['representantes'] . '.D_E_L_E_T_ !=' => '*',
							$this->_db_cliente['tabelas']['pedidos'] . '.D_E_L_E_T_ !=' => '*',
							$this->_db_cliente['tabelas']['pedidos'] . '.C5_FILIAL' => '01'
						))
						->get()->row_array();
		
		
		// FILTRAGEM PARA PAGINAÇÃO **
		
		$this->load->view('layout', array('conteudo' => $this->load->view('pedidos/index', array('pedidos' => $pedidos, 'total' => $total, 'total_2' => $total_2, 'filtragem' => $this->filtragem($filtros), 'paginacao' => $this->paginacao($total)), TRUE)));
	
		//$this->output->enable_profiler();
	}
	
	function aguardando_analise($orcamento = FALSE)
	{
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->_db_cliente = $this->config->item('db_cliente');

		
		$filtros = array(
			!$orcamento ? array(
				'nome' => 'status',
				'descricao' => 'Status',
				'tipo' => 'opcoes',
				'opcoes' => array('todos' => 'Todos', 'A' => 'Aguardando Análise Comercial', 'R' => 'Reprovado'),
				'campo_mssql' => $this->_db_cliente['campos']['pedidos_dw']['status'],
				'ordenar_ms' => 0
			) : NULL, 
			in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) ? array(
				'nome' => 'representante',
				'descricao' => 'Representante',
				'tipo' => 'texto',
				'campo_mssql' => $this->_db_cliente['campos']['representantes']['nome'],
				'ordenar_ms' => 1
			) : NULL,
			array(
				'nome' => 'cliente',
				'descricao' => 'Cliente',
				'tipo' => 'texto',
				'campo_mssql' => $this->_db_cliente['campos']['pedidos_dw']['nome_cliente'],
				'ordenar_ms' => 2
			),
			$orcamento ? array(
				'nome' => 'prospect',
				'descricao' => 'Prospect',
				'tipo' => 'texto',
				'campo_mssql' =>  $this->_db_cliente['campos']['pedidos_dw']['nome_prospects'],
				'ordenar_ms' => 2
			) : NULL,
			array(
				'nome' => 'ordem_de_compra',
				'descricao' => 'Ordem de compra',
				'tipo' => 'texto',
				'campo_mssql' => $this->_db_cliente['campos']['pedidos_dw']['ordem_compra'],
				'ordenar_ms' => 3
			),
			array(
				'nome' => 'emissao',
				'descricao' => 'Emissão',
				'tipo' => 'data',
				'campo_mssql' => $this->_db_cliente['campos']['pedidos_dw']['data_emissao'],
				'ordenar_ms' => 4
			)
		);
		
		// para que a paginação no mssql funcione, devemos ordenar de alguma forma, caso o usuário não escolher uma ordenação, vamos fazer manualmente
		if (!$this->input->get('ordenar_ms'))
		{
			// vamos setar o $_GET ao invés de usar o order_by()
			// usando o order_by a flexinha no layout indicando o que está sendo ordenado não apareceria para o usuário
			$_GET['ordenar_ms'] = $this->_db_cliente['campos']['pedidos_dw']['data_emissao'];
			$_GET['ordenar_ms_tipo'] = 'desc';
		}
		
		// ** FILTRAGEM PARA EXIBIÇÃO DOS RESULTADOS
		
		// caso o usuário logado for um representante, vamos mostrar somente os pedidos que ele mesmo fez
		if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')))
		{
			$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['codigo_representante'], $this->session->userdata('codigo_usuario'));
		}
		
		$this->filtragem_mssql($filtros);
		
		if ($orcamento) 
 	 	{ 
			$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['tipo'], 'O'); 
 	 	}
		else 
 	 	{ 
			$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['tipo'], 'P'); 
 	 	} 
		
		$this->db_cliente->where($this->_db_cliente['tabelas']['pedidos_dw'] . '.' . $this->_db_cliente['campos']['pedidos_dw']['status'] . ' !=', 'L');
		$this->db_cliente->where($this->_db_cliente['tabelas']['pedidos_dw'] . '.' . $this->_db_cliente['campos']['pedidos_dw']['status'] . ' !=', 'I');
		
		
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['id_pedido'];
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['ordem_compra'];
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['data_emissao'];
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['codigo_cliente'];
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['loja_cliente'];
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['nome_cliente'];
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['nome_prospects'];
		$select[] = $this->_db_cliente['campos']['representantes']['nome'];
		$select[] = $this->_db_cliente['campos']['pedidos_dw']['status'];
		$select[] = 'SUM(' . $this->_db_cliente['campos']['pedidos_dw']['quantidade'] . ') AS ' . $this->_db_cliente['campos']['pedidos_dw']['quantidade'];
		//$select[] = 'SUM(' . $this->_db_cliente['campos']['pedidos_dw']['preco_unitario'] . ' * ' . $this->_db_cliente['campos']['pedidos_dw']['quantidade'] . ') + ' . $this->_db_cliente['campos']['pedidos_dw']['valor_frete'] . ' AS total';
		$select[] = 'SUM(' . $this->_db_cliente['campos']['pedidos_dw']['preco_unitario'] . ' * ' . $this->_db_cliente['campos']['pedidos_dw']['quantidade'] . ') AS peso_total';
		
		$select[] = '
		SUM(
			(' . $this->_db_cliente['campos']['pedidos_dw']['preco_unitario'] . ' * ' .  $this->_db_cliente['campos']['pedidos_dw']['quantidade'] . ') + 
			((' . $this->_db_cliente['campos']['pedidos_dw']['ipi'] . ' / 100) * (' . $this->_db_cliente['campos']['pedidos_dw']['preco_unitario'] . ' * ' .  $this->_db_cliente['campos']['pedidos_dw']['quantidade'] . ')) + 
			((' . $this->_db_cliente['campos']['pedidos_dw']['substituicao_tributaria'] . ' / 100) * (' . $this->_db_cliente['campos']['pedidos_dw']['preco_unitario'] . ' * ' .  $this->_db_cliente['campos']['pedidos_dw']['quantidade'] . ')) - 
			((' . $this->_db_cliente['campos']['pedidos_dw']['desconto'] . ' / 100) * (' . $this->_db_cliente['campos']['pedidos_dw']['preco_unitario'] . ' * ' .  $this->_db_cliente['campos']['pedidos_dw']['quantidade'] . '))
		) as total
		';

		$group[] = $this->_db_cliente['campos']['pedidos_dw']['id_pedido'];
		$group[] = $this->_db_cliente['campos']['pedidos_dw']['ordem_compra'];
		$group[] = $this->_db_cliente['campos']['pedidos_dw']['data_emissao'];
		$group[] = $this->_db_cliente['campos']['pedidos_dw']['codigo_cliente'];
		$group[] = $this->_db_cliente['campos']['pedidos_dw']['loja_cliente'];
		$group[] = $this->_db_cliente['campos']['pedidos_dw']['nome_cliente'];
		$group[] = $this->_db_cliente['campos']['pedidos_dw']['nome_prospects'];
		$group[] = $this->_db_cliente['campos']['representantes']['nome'];
		$group[] = $this->_db_cliente['campos']['pedidos_dw']['status'];
		$group[] = $this->_db_cliente['campos']['pedidos_dw']['valor_frete'];
		
		$pedidos = $this->db_cliente->select(implode(', ', $select))
						->from($this->_db_cliente['tabelas']['pedidos_dw'])
						->join($this->_db_cliente['tabelas']['representantes'], $this->_db_cliente['campos']['representantes']['codigo'] . ' = ' . $this->_db_cliente['campos']['pedidos_dw']['codigo_representante'])
						->where(array($this->_db_cliente['tabelas']['representantes'] . '.' . $this->_db_cliente['campos']['representantes']['delecao']  . ' !=' => '*'))
						->limit(20, $this->input->get('per_page'))
						->group_by(implode(', ', $group))
						->get()->result_array();
		
		
		// FILTRAGEM PARA EXIBIÇÃO DOS RESULTADOS **
		
		// ** FILTRAGEM PARA PAGINAÇÃO
		
		// o campo status precisa ser filtrado "manualmente" pois ele não existe no banco de dados
		
		// caso o usuário logado for um representante, vamos mostrar somente os pedidos que ele mesmo fez
		if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')))
		{
			$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['codigo_representante'], $this->session->userdata('codigo_usuario'));
		}
		
		// FALSE = não usar order_by
		$this->filtragem_mssql($filtros, FALSE);
		
		if ($orcamento) 
 	 	{ 
			$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['tipo'], 'O'); 
 	 	}
		else 
 	 	{ 
			$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['tipo'], 'P'); 
 	 	} 
		
		$this->db_cliente->where($this->_db_cliente['tabelas']['pedidos_dw'] . '.' . $this->_db_cliente['campos']['pedidos_dw']['status'] . ' !=', 'L');
		$this->db_cliente->where($this->_db_cliente['tabelas']['pedidos_dw'] . '.' . $this->_db_cliente['campos']['pedidos_dw']['status'] . ' !=', 'I');
		
		/*
		$total = $this->db_cliente->select('COUNT(DISTINCT ' . $this->_db_cliente['campos']['pedidos_dw']['valor_frete'] . ') AS quantidade, SUM(' . $this->_db_cliente['campos']['pedidos_dw']['preco_unitario'] . ' * ' . $this->_db_cliente['campos']['pedidos_dw']['quantidade'] . ') AS valor')
						->from($this->_db_cliente['tabelas']['pedidos_dw'])
						->join($this->_db_cliente['tabelas']['representantes'], $this->_db_cliente['campos']['representantes']['codigo'] . ' = ' . $this->_db_cliente['campos']['pedidos_dw']['codigo_representante'])
						->where(array($this->_db_cliente['tabelas']['representantes'] . '.' . $this->_db_cliente['campos']['representantes']['delecao']  . ' !=' => '*'))
						->get()->row_array();
						*/
		
		$total = $this->db_cliente->select(
							'SUM(
								(' . $this->_db_cliente['campos']['pedidos_dw']['preco_unitario'] . ' * ' .  $this->_db_cliente['campos']['pedidos_dw']['quantidade'] . ') + 
								((' . $this->_db_cliente['campos']['pedidos_dw']['ipi'] . ' / 100) * (' . $this->_db_cliente['campos']['pedidos_dw']['preco_unitario'] . ' * ' .  $this->_db_cliente['campos']['pedidos_dw']['quantidade'] . ')) + 
								((' . $this->_db_cliente['campos']['pedidos_dw']['substituicao_tributaria'] . ' / 100) * (' . $this->_db_cliente['campos']['pedidos_dw']['preco_unitario'] . ' * ' .  $this->_db_cliente['campos']['pedidos_dw']['quantidade'] . ')) - 
								((' . $this->_db_cliente['campos']['pedidos_dw']['desconto'] . ' / 100) * (' . $this->_db_cliente['campos']['pedidos_dw']['preco_unitario'] . ' * ' .  $this->_db_cliente['campos']['pedidos_dw']['quantidade'] . '))
								)as valor,
							COUNT(DISTINCT ' . $this->_db_cliente['campos']['pedidos_dw']['id_pedido'] . ') as quantidade'
						)
						->from($this->_db_cliente['tabelas']['pedidos_dw'])
						->join($this->_db_cliente['tabelas']['representantes'], $this->_db_cliente['campos']['representantes']['codigo'] . ' = ' . $this->_db_cliente['campos']['pedidos_dw']['codigo_representante'])
						->where(array($this->_db_cliente['tabelas']['representantes'] . '.' . $this->_db_cliente['campos']['representantes']['delecao']  . ' !=' => '*'))
						->get()->row_array();
		
	
		
		
		// FILTRAGEM PARA PAGINAÇÃO **
		
		$this->load->view('layout', array('conteudo' => $this->load->view('pedidos/aguardando_analise', array('pedidos' => $pedidos, 'total' => $total, 'filtragem' => $this->filtragem($filtros), 'paginacao' => $this->paginacao($total['quantidade']), 'orcamento' => $orcamento), TRUE)));
	}
	
	function enviar_email($codigo = NULL, $id = NULL)
	{
		$dados = array(
			'erro' => NULL
		);
		
		if (!$this->input->post('emails'))
		{
			$dados['erro'] = 'Digite um e-mail.';
		}
		else
		{
			$emails = explode(', ', $this->input->post('emails'));
			
			foreach ($emails as $email)
			{
				if (!valid_email($email))
				{
					$dados['erro'] = 'O e-mail é "' . $email . '" inválido.';
					break;
				}
			}
			
			if (!$dados['erro'])
			{
				if($id)
				{
					$html = $this->ver_detalhes($codigo, $id, TRUE);
				}
				else
				{
					$html = $this->ver_detalhes_pdr($codigo, TRUE);
				}
				
				enviar_email($emails, 'Portal do Representante', $html);
				
			}
		}
		
		echo json_encode($dados);
	}
	
	function gerar_pdf($codigo = NULL, $id = NULL) {
		$arquivo_temporario = tempnam("/tmp", "dompdf_");
		file_put_contents($arquivo_temporario, $this->ver_detalhes($codigo, $id, TRUE));
		header('location: ' . base_url() . 'misc/dompdf/dompdf.php?input_file=' . rawurlencode($arquivo_temporario)) . '&paper=letter&output_file=' . rawurlencode('pedido.pdf');
	}
	
	function gerar_html($codigo = NULL, $id = NULL)
	{
		echo $this->ver_detalhes($codigo, $id, TRUE);
	}
	
function obter_alertas()
	{
		echo '<p style="color: #FFF; font-size: 12px;"><strong style="color: #0FF;">Pedidos </strong>- ';
		
		if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros'))) {
			$total_pedidos_aguardando_analise_comercial = $this->db->from('pedidos')->where('status', 'aguardando_analise_comercial')->get()->num_rows();
		} else {
			$total_pedidos_aguardando_analise_comercial = $this->db->from('pedidos')->where(array('codigo_usuario' => $this->session->userdata('codigo_usuario'), 'status' => 'aguardando_analise_comercial'))->get()->num_rows();
		}
		
		echo 'Aguardando Análise (<b>' . $total_pedidos_aguardando_analise_comercial.'</b>) - <b>'.anchor('pedidos/aguardando_analise', ' &raquo; Ver todos &raquo;', 'style="color: #FF0;"') . '</b></p>' ;
		
		/*
		$pedido_nao_concluido = $this->db->from('pedidos')->where(array('id_usuario' => $this->session->userdata('id_usuario'), 'status' => NULL))->get()->row();
		
		if ($pedido_nao_concluido)
		{
			echo '<p><strong>Aviso:</strong> há um pedido não concluído - ' . anchor('pedidos/criar/pedido', '+ Opções &raquo;') . '</p>';
		}
		else
		{
			echo '<p>&nbsp;</p>';
		}*/
	}
	
	function redirecionar_pedido_nao_concluido()
	{
		$this->db->where('id_usuario', $this->session->userdata('id_usuario'));
		$this->db->where('tipo', 'pedido');
		$this->db->where('status', NULL);
		
		$pedido = $this->db->from('pedidos')->get()->row();
		
		foreach ($pedido as $indice => $valor)
		{
			$pedido->$indice = $valor ? $valor : 0;
		}
		
		redirect(array(
			'pedidos', 'criar', $pedido->tipo, $pedido->codigo_usuario, $pedido->codigo_cliente, $pedido->loja_cliente, $pedido->id_prospect, $pedido->id
		));
	}
	
	function copiar_ajax($id, $codigo = NULL)
	{
		$this->db->where('id_usuario', $this->session->userdata('id_usuario'));
		$this->db->where('tipo', 'pedido');
		$this->db->where('status', NULL);
		
		$pedido = $this->db->from('pedidos')->get()->row();
		
		if ($pedido)
		{
			echo '<p class="erro" style="margin: 0;">Você não pode copiar um pedido enquanto houver um pedido aguardando conclusão.' . br(2) . anchor('pedidos/ver_detalhes/0/' . $pedido->id, 'Clique aqui para ver espelho do pedido aguardando conclusão.') . '</p>';
		}
		else
		{
			if ($id)
			{
				$pedido = $this->db->from('pedidos')->where('id', $id)->get()->row();
				$itens_pedido = $this->db->from('itens_pedidos')->where('id_pedido', $id)->get()->result();
				
				$_pedido = array(
					'id_usuario' => $this->session->userdata('id_usuario'),
					'codigo_usuario' => !in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) ? $this->session->userdata('codigo_usuario') : NULL,
					'tipo' => 'pedido'
				);
				
				foreach ($pedido as $indice => $valor)
				{
					if (!in_array($indice, array('id', 'timestamp', 'id_usuario', 'codigo_usuario', 'tipo', 'status', 'codigo_ordem_compra', 'data_entrega_timestamp', 'observacao', 'motivo_reprovacao')))
					{
						$_pedido[$indice] = $valor;
					}
				}
				
				$this->db->insert('pedidos', $_pedido);
				$id_pedido = $this->db->insert_id();
				
				foreach ($itens_pedido as $item_pedido)
				{
					$_item_pedido = array(
						'id_pedido' => $id_pedido
					);
					
					foreach ($item_pedido as $indice => $valor)
					{
						if (!in_array($indice, array('id', 'id_pedido')))
						{
							$_item_pedido[$indice] = $valor;
						}
					}
					
					$this->db->insert('itens_pedidos', $_item_pedido);
				}
			}
			else if ($codigo)
			{
				$pedido = $this->db_cliente->obter_pedido($codigo);
				
				$this->db->insert('pedidos', array(
					'id_usuario' => $this->session->userdata('id_usuario'),
					'codigo_usuario' => !in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) ? $this->session->userdata('codigo_usuario') : NULL,
					'tipo' => 'pedido',
					'codigo_cliente' => $pedido['cliente']['codigo'],
					'loja_cliente' => $pedido['cliente']['loja'],
					'codigo_forma_pagamento' => $pedido['codigo_forma_pagamento'],
					'codigo_transportadora' => $pedido['codigo_transportadora']
				));
				$id_pedido = $this->db->insert_id();
				
				foreach ($pedido['itens'] as $item_pedido)
				{
					$this->db->insert('itens_pedidos', array(
						'timestamp' => time(),
						'id_pedido' => $id_pedido,
						'codigo' => $item_pedido['codigo_produto'],
						'descricao' => $item_pedido['descricao_produto'],
						'unidade_medida' => $item_pedido['unidade_medida_produto'],
						'preco' => $item_pedido['preco_produto'],
						'quantidade' => $item_pedido['quantidade_vendida_produto']
					));
				}
			}
		}
	}
	
	function obter_pedidos_aguardando_analise_comercial_ajax()
	{
		$pedidos = $this->db->from('pedidos')->where(array('timestamp >' => $this->session->userdata('ultima_checagem_pedidos_aguardando_analise_comercial'), 'status' => 'aguardando_analise_comercial'))->order_by('id', 'desc')->get()->result();
		
		foreach ($pedidos as $pedido)
		{
			$_pedido = $this->db->select('SUM(quantidade) AS quantidade_vendida, SUM(preco * quantidade) AS valor_total')->from('itens_pedidos')->where('id_pedido', $pedido->id)->get()->row();
			
			$_pedidos[] = array_map('trim', array(
				'imagem' => img(base_url() . 'misc/imagens/pedidos/status_' . $pedido->status . '.png'),
				'status' => element($pedido->status, $this->_obter_status()),
				'codigo' => 'não há',
				'id' => $pedido->id,
				'emissao' => date('d/m/Y', $pedido->timestamp),
				'nome_cliente' => $this->db_cliente->obter_nome_cliente($pedido->codigo_cliente, $pedido->loja_cliente),
				'quantidade_vendida' => number_format($_pedido->quantidade_vendida, 0, NULL, '.'),
				'quantidade_faturada' => 0,
				'valor_total' => number_format($_pedido->valor_total, 2, ',', '.'),
				'valor_parcial' => 0
			));
		}
		
		echo json_encode($_pedidos);
		
		$this->session->set_userdata('ultima_checagem_pedidos_aguardando_analise_comercial', time());
	}
	
	function editar_codigo_usuario_criar($novo_codigo = NULL)
	{
		$this->session->set_userdata('codigo_usuario_criar_pedido', $novo_codigo);
		
		redirect('pedidos/criar/pedido');
	}
	
	function obter_informacoes_cliente_ajax($codigo_loja_cliente = NULL)
	{
		$dados = array(
			'cliente_encontrado' => FALSE,
			'html' => NULL
		);
		
		$codigo_loja_cliente = explode('_', $codigo_loja_cliente);
		$codigo_cliente = $codigo_loja_cliente[0];
		$loja_cliente = $codigo_loja_cliente[1];
		
		if ($codigo_cliente && $loja_cliente)
		{
			$cliente = $this->db_cliente->obter_cliente($codigo_cliente, $loja_cliente);
		}
		
		if ($cliente)
		{
			$dados['cliente_encontrado'] = TRUE;
			
			$dados['html'] = '
				<p><strong>' . $cliente['nome'] . '</strong> - ' . $cliente['cpf'] . '</p>
				
				<p style="float: left; margin-right: 10px;"><strong>Última compra:</strong></p>
				
				<div style="clear: both;"></div>
				
				<ul style="float: left; margin-top: 5px; margin-right: 10px;">
					<li>Forma de pagamento: ' . ($cliente['ultimo_pedido']['descricao_forma_pagamento'] ? $cliente['ultimo_pedido']['descricao_forma_pagamento'] : '-') . '</li>
					<li>Desconto: R$ ' . number_format($cliente['ultimo_pedido']['valor_desconto'], 2, ',', '.') . '</li>
					<li>Data: ' . ($cliente['ultimo_pedido']['emissao_timestamp'] ? date('d/m/Y', $cliente['ultimo_pedido']['emissao_timestamp']) : '-') . '</li>
					' . ($cliente['ultimo_pedido']['codigo_pedido'] ? '<li>' . anchor('pedidos/ver_detalhes/' . $cliente['ultimo_pedido']['codigo_pedido'], 'Ver detalhes &raquo;', 'target="_blank"') . '</li>' : NULL) . '
				</ul>
				
				<!--<ul style="float: left; margin-right: 10px;">
					<li><strong>Limite de crédito:</strong> R$ ' . number_format($cliente['limite_credito'], 2, ',', '.') . '</li>
					<li><strong>Títulos em aberto:</strong> R$ ' . number_format($cliente['total_titulos_em_aberto'], 2, ',', '.') . '</li>
					<li><strong>Títulos vencidos:</strong> R$ ' . number_format($cliente['total_titulos_vencidos'], 2, ',', '.') . '</li>
				</ul>
				
				<ul style="float: left; margin-right: 10px;">
					<li><strong>Endereço:</strong> ' . $cliente['endereco'] . '</li>
					<li><strong>Bairro:</strong> ' . $cliente['bairro'] . '</li>
					<li><strong>CEP:</strong> ' . $cliente['cep'] . '</li>
				</ul>
				
				<ul style="float: left; margin-right: 10px;">
					<li><strong>Cidade:</strong> ' . $cliente['cidade'] . '</li>
					<li><strong>Estado:</strong> ' . $cliente['estado'] . '</li>
				</ul>-->
				
				<div style="clear: both;"></div>
			';
		}
		
		echo json_encode($dados);
	}
	
	function obter_informacoes_prospect_ajax($id_prospect = NULL)
	{
		$dados = array(
			'prospect_encontrado' => FALSE,
			'html' => NULL
		);
		
		if ($id_prospect)
		{
			$prospect = $this->db->from('prospects')->where('id', $id_prospect)->get()->row();
		}
		
		if ($prospect)
		{
			$dados['prospect_encontrado'] = TRUE;
			
			$dados['html'] = '
				<p><strong>' . $prospect->nome . '</strong> - ' . $prospect->cpf . '</p>
				
				<ul style="float: left; margin-right: 10px;">
					<li><strong>Endereço:</strong> ' . $prospect->endereco . '</li>
					<li><strong>Bairro:</strong> ' . $prospect->bairro . '</li>
					<li><strong>CEP:</strong> ' . $prospect->cep . '</li>
				</ul>
				
				<ul style="float: left; margin-right: 10px;">
					<li><strong>Cidade:</strong> ' . $prospect->cidade . '</li>
					<li><strong>Estado:</strong> ' . $prospect->estado . '</li>
				</ul>
				
				<div style="clear: both;"></div>
			';
		}
		
		echo json_encode($dados);
	}
	
	function criar($tipo = 'pedido', $codigo_usuario = NULL, $codigo_cliente = NULL, $loja_cliente = NULL, $id_prospect = NULL, $id_pedido = NULL)
	{
	
		// Sessão - Buscar dados pedidos
		if($id_pedido)
		{
			$busca_pedido = $this->db->from('pedidos')->where('id', $id_pedido)->get()->row();
			
			if($busca_pedido)
			{
				$dados_pedidos = $this->session->userdata('dados_pedidos');

				$_POST['codigo_usuario'] = $dados_pedidos['codigo_usuario'];
				$_POST['codigo_loja_cliente'] = $dados_pedidos['codigo_loja_cliente'];
				$_POST['id_feira'] = $dados_pedidos['id_feira'];
				$_POST['codigo_forma_pagamento'] = $dados_pedidos['codigo_forma_pagamento'];
				$_POST['tipo_frete'] = $dados_pedidos['tipo_frete'];
				$_POST['codigo_transportadora'] = $dados_pedidos['codigo_transportadora'];
				$_POST['codigo_ordem_compra'] = $dados_pedidos['codigo_ordem_compra'];
				$_POST['observacao_pedido_imediato'] = $dados_pedidos['observacao_pedido_imediato'];
				
				
			}
			else
			{	
				$this->session->unset_userdata('dados_pedidos');
			}
		
		}
		
	
		if (!in_array($tipo, array('pedido', 'orcamento')))
		{
			redirect();
		}
		
		// ** obter pedido
		
		if (!$id_pedido)
		{
			$this->db->where('id_usuario', $this->session->userdata('id_usuario'));
			$this->db->where('tipo', $tipo);
			$this->db->where('status', NULL);
		}
		else
		{
			$this->db->where('id', $id_pedido);
		}
		$pedido = $this->db->from('pedidos')->get()->row();
		
		if (!$pedido)
		{
			$this->db->insert('pedidos', array(
				'id_usuario' => $this->session->userdata('id_usuario'),
				'tipo' => $tipo
			));
			$pedido = $this->db->from('pedidos')->where('id', $this->db->insert_id())->get()->row();
		}
		else if ($pedido && !$_POST && !$id_pedido)
		{
			//redirect('pedidos/ver_detalhes/0/' . $pedido->id);
		}
		
		if ($codigo_cliente && $codigo_cliente != '0' && !$_POST)
		{
			$this->db->update('pedidos', array(
				'codigo_cliente' => $codigo_cliente,
				'loja_cliente' => $loja_cliente,
			), array('id' => $pedido->id));
			$pedido = $this->db->from('pedidos')->where('id', $pedido->id)->get()->row();
		}
		if ($id_prospect && $id_prospect != '0' && !$_POST)
		{
			$this->db->update('pedidos', array(
				'id_prospect' => $id_prospect,
			), array('id' => $pedido->id));
			$pedido = $this->db->from('pedidos')->where('id', $pedido->id)->get()->row();
		}
		
		$itens_pedido = $this->db->from('itens_pedidos')->where('id_pedido', $pedido->id)->get()->row();
		
		// obter pedido **
		
		// se o usuário apenas modificou o grupo não há necessidade de fazer validações
		if (!$_POST['grupo_produtos_modificado'] && !$_POST['somente_postar'])
		{
			if($_POST["continuar"] || $_POST["adicionar_item"])
			{
				// validar
				if ($_POST)
				{
					if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) && !$this->input->post('codigo_usuario'))
					{
						$erro = 'Selecione um representante.';
					}
					else if ($pedido->tipo == 'pedido' && !$this->input->post('codigo_loja_cliente'))
					{
						$erro = 'Selecione um cliente.';
					}
					else if ($pedido->tipo == 'orcamento' && !$this->input->post('codigo_loja_cliente') && !$this->input->post('id_prospect'))
					{
						$erro = 'Selecione um cliente ou um prospect.';
					}
					else if ($tipo == 'pedido' && !$this->input->post('codigo_forma_pagamento'))
					{
						$erro = 'Selecione uma forma de pagamento.';
					}
					else if ($tipo == 'pedido' && $this->input->post('tipo_frete') == 'FOB' && !$this->input->post('codigo_transportadora'))
					{
						$erro = 'Selecione uma transportadora.';
					}
					else if ($tipo == 'pedido' && $this->input->post('tipo_frete') == 'Redespacho' && !$this->input->post('codigo_transportadora'))
					{
						$erro = 'Selecione uma transportadora.';
					}
					else if ($this->input->post('data_entrega') && !$this->_validar_data($this->input->post('data_entrega')))
					{
						$erro = 'Digite uma data de entrega válida.';
					}
					else if (!$itens_pedido)
					{
						$erro = 'Adicione um ou mais itens no pedido.';
					}
					else
					{
						if ($this->input->post('codigo_loja_cliente'))
						{
							$codigo_loja_cliente = explode('|', $this->input->post('codigo_loja_cliente'));
							$codigo_cliente = $codigo_loja_cliente[0];
							$loja_cliente = $codigo_loja_cliente[1];
							
							$cliente = $this->db_cliente->obter_cliente($codigo_cliente, $loja_cliente);
						}
						else if ($this->input->post('id_prospect'))
						{
							$prospect = $this->db->from('prospects')->where('id', $this->input->post('id_prospect'))->get()->row();
						}
						
						$this->db->update('pedidos', array(
							'timestamp' => $pedido->timestamp ? $pedido->timestamp : time(),
							'status' => ($pedido->tipo == 'orcamento') ? 'concluido' : $pedido->status,
							'codigo_usuario' => $this->input->post('codigo_usuario') ? $this->input->post('codigo_usuario') : $this->session->userdata('codigo_usuario'),
							'codigo_cliente' => !$this->input->post('criar_para') || $this->input->post('criar_para') == 'cliente' ? $cliente['codigo'] : NULL,
							'loja_cliente' => !$this->input->post('criar_para') || $this->input->post('criar_para') == 'cliente' ? $cliente['loja'] : NULL,
							'id_prospect' => !$this->input->post('criar_para') || $this->input->post('criar_para') == 'prospect' ? $prospect->id : NULL,
							'codigo_forma_pagamento' => $this->input->post('codigo_forma_pagamento'),
							'tipo_frete' => $this->input->post('tipo_frete'),
							'codigo_transportadora' => in_array($this->input->post('tipo_frete'), array('FOB', 'Redespacho')) ? $this->input->post('codigo_transportadora') : NULL,
							'codigo_ordem_compra' => $this->input->post('codigo_ordem_compra'),
							'data_entrega_timestamp' => strtotime($this->input->post('data_entrega')),
							'observacao_pedido_imediato' => $this->input->post('observacao_pedido_imediato'),
							'observacao_pedido_programado' => $this->input->post('observacao_pedido_programado'),
							'id_feira' => $this->input->post('id_feira'),
							'peso_total' => $this->input->post('peso_total'),
							'frete' => $this->input->post('frete')
						), array('id' => $pedido->id));
						
						
						
						// - Session - Dados do Pedido
						$sessao_dados_pedidos = array(
								   'codigo_usuario' => $this->input->post('codigo_usuario'),
								   'codigo_loja_cliente' => $this->input->post('codigo_loja_cliente'),
								   'id_feira' => $this->input->post('id_feira'),
								   'codigo_forma_pagamento' => $this->input->post('codigo_forma_pagamento'),
								   'tipo_frete' => $this->input->post('tipo_frete'),
								   'codigo_transportadora' => $this->input->post('codigo_transportadora'),
								   'codigo_ordem_compra' => $this->input->post('codigo_ordem_compra'),
								   'observacao_pedido_imediato' => $this->input->post('observacao_pedido_imediato')
							   );

						$this->session->set_userdata('dados_pedidos', $sessao_dados_pedidos);

						redirect('pedidos/ver_detalhes/0/' . $pedido->id);
					}
				}
				else
				{
					foreach ($pedido as $indice => $valor)
					{
						if ($indice == 'data_entrega_timestamp' && $valor)
						{
							$_POST['data_entrega'] = date('Y-m-d', $valor);
						}
						else if ($indice == 'codigo_cliente' && $valor)
						{
							$_POST['codigo_loja_cliente'] = $pedido->codigo_cliente . '|' . $pedido->loja_cliente;
						}
						else
						{
							$_POST[$indice] = $valor;
						}
					}
					
					//$criar_para = $pedido->id_prospect && $pedido->id_prospect != '0' ? 'prospect' : 'cliente';
					$criar_para = $pedido->codigo_cliente ? 'cliente' : 'prospect';
					$_POST['criar_para'] = $_POST['criar_para'] ? $_POST['criar_para'] : $criar_para;
				}
			}
		}
		
		$conteudo .= form_open(current_url());
		
		$conteudo .= '<div id="informacoes_cliente_prospect" style="background-color: #EEE; border: 1px solid #DDD; display: none; float: right; padding: 0 10px 10px 10px; -webkit-border-radius: 5px;"></div>';
		
		$conteudo .= heading('Criar ' . ($tipo == 'orcamento' ? 'Orçamento' : 'Pedido'), 2);
		
		if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores')))
		{
			// se nenhum código de usuário foi informado vamos pegar o primeiro código disponível no dropdown
			if (!$codigo_usuario)
			{
				$codigo_usuario = $this->_obter_indice_primeiro_usuario_usuarios('codigo');
			}
			
			$_codigo_usuario = $codigo_usuario;
		}
		else
		{
			$_codigo_usuario = $this->session->userdata('codigo_usuario');
		}
		
		$codigo_usuario = $codigo_usuario ? $codigo_usuario : $_codigo_usuario;
		
		if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores'))) {
			/*$conteudo .= '
				<p>' . form_label('Representante: ' . br() . form_dropdown('codigo_usuario', $this->_obter_representantes(FALSE), $this->input->post('codigo_usuario') ? $this->input->post('codigo_usuario') : $codigo_usuario, 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" onChange="editar_codigo_usuario"')) . '</p>
				
				<script type="text/javascript">
					function editar_codigo_usuario(novo_codigo)
					{
						window.location = "' . site_url('pedidos/criar/' . $tipo) . '/" + novo_codigo + "/' . ($codigo_cliente ? $codigo_cliente : 0) . '/' . ($loja_cliente ? $loja_cliente : 0) . '/' . ($id_prospect ? $id_prospect : 0) . '/' . ($id_pedido ? $id_pedido : 0) . '";
					}
				</script>
			';*/
			
			$conteudo .= '
				
				<p>Usuário:</p>
				
				' . $this->_obter_dropdown_representantes('codigo', array('name' => 'codigo_usuario', 'value' => $this->input->post('codigo_usuario') ? $this->input->post('codigo_usuario') : $codigo_usuario, 'id' => 'usuarios'), FALSE) . '
			
				<script type="text/javascript">

						$(document).ready(function(){
								
								$("#usuarios").change(function(){
										var valor = $(this).val();
										window.location = "' . site_url('pedidos/criar/' . $tipo) . '/" + novo_codigo + "/' . ($codigo_cliente ? $codigo_cliente : 0) . '/' . ($loja_cliente ? $loja_cliente : 0) . '/' . ($id_prospect ? $id_prospect : 0) . '/' . ($id_pedido ? $id_pedido : 0) . '";
								});
								
						});

				</script>
				
			';
		}
		
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		
		$conteudo .= heading('Informações Gerais', 3);
		
		$conteudo .= '<div style="float: left; margin-right: 10px;">';
		if ($pedido->tipo == 'orcamento')
		{
			$conteudo .= '<p>Criar para:<br />' . form_label(form_radio('criar_para', 'prospect', !$_POST['criar_para'] || $_POST['criar_para'] == 'prospect' ? TRUE : FALSE) . ' Prospect') . ' ' . form_label(form_radio('criar_para', 'cliente', $_POST['criar_para'] == 'cliente' ? TRUE : FALSE) . ' Cliente') . '</p>';
			$conteudo .= '
				<script type="text/javascript">
					$(document).ready(function() {
						verificar_criar_para();
						
						$("input[name=criar_para]").click(function() {
							verificar_criar_para();
						});
					});
					
					function verificar_criar_para()
					{
						if ($("input[name=criar_para]:checked").val() == "cliente")
						{
							$("#cliente").show();
							$("#prospect").hide();
							
							if ($("input[name=codigo_loja_cliente]").val()) 
								obter_informacoes_cliente($("input[name=codigo_loja_cliente]").val());
							else
								$("#informacoes_cliente_prospect").hide();
						}
						else
						{
							$("#cliente").hide();
							$("#prospect").show();
							
							if ($("input[name=id_prospect]").val())
								obter_informacoes_prospect($("input[name=id_prospect]").val());
							else
								$("#informacoes_cliente_prospect").hide();
						}
					}
				</script>
			';
		}
		
		$conteudo .= '<p style="float: left; margin-right: 10px;" id="cliente">Cliente:<br><input type="text" style="width: 250px" name="codigo_loja_cliente" class="autocomplete" value="' . $this->input->post("_codigo_loja_cliente") . '" alt="' . $this->input->post("codigo_loja_cliente") . '" data-url="' . site_url('orcamentos/obter_clientes' . ($this->session->userdata('codigo_usuario_consultar_orcamentos') ? ('/' . $this->session->userdata('codigo_usuario_consultar_orcamentos')) : NULL)) . '"></p>';
		
		
		if ($pedido->tipo == 'orcamento')
		{
		
			$conteudo .= '<p style="float: left; margin-right: 10px;" id="prospect">Prospect:<br><input type="text" style="width: 250px" name="id_prospect" class="autocomplete" value="' . $this->input->post("_id_prospect") . '" alt="' . $this->input->post("id_prospect") . '" data-url="' . site_url('orcamentos/obter_prospects' . ($this->session->userdata('codigo_usuario_consultar_orcamentos') ? ('/' . $this->session->userdata('codigo_usuario_consultar_orcamentos')) : NULL)) . '"></p>';
		
		}
		$conteudo .= '</div>';
		
		// ** tipo de desconto
		//$conteudo .= '<p style="float: left; margin-right: 10px;">Desconto:' . br() . form_radio('tipo_desconto', 'sem_desconto', !$this->input->post('tipo_desconto') || $this->input->post('tipo_desconto') == 'sem_desconto' ? TRUE : FALSE, 'dojoType="dijit.form.RadioButton"') . ' Sem desconto ' . form_radio('tipo_desconto', 'por_item', $this->input->post('tipo_desconto') == 'por_item' ? TRUE : FALSE, 'dojoType="dijit.form.RadioButton"') . ' Por item ' . form_radio('tipo_desconto', 'geral', $this->input->post('tipo_desconto') == 'geral' ? TRUE : FALSE, 'dojoType="dijit.form.RadioButton"') . ' Geral</p>';
		
		$conteudo .= '
			<script type="text/javascript">
				
				/*
				$(document).ready(function() {
					verificar_tipo_desconto();
					
					$("input[name=tipo_desconto]").live("click", function() {
						verificar_tipo_desconto();
					});
				});
				
				function verificar_tipo_desconto()
				{
					var tipo_desconto = $("input[name=tipo_desconto]:checked").val();
					
					switch (tipo_desconto)
					{
						case "sem_desconto":
							$("#desconto").val("").parents("p").hide();
							$("#valor_desconto").val("").parents("p").hide();
							
							$("#desconto_total").val("").parents("p").hide();
							$("#valor_desconto_total").val("").parents("p").hide();
							
							calcular_descontos();
						break;
						
						case "por_item":
							$("#desconto").parents("p").show();
							$("#valor_desconto").parents("p").show();
							
							$("#valor_desconto_total").val("").parents("p").hide();
							$("#desconto_total").val("").parents("p").hide();
							
							calcular_descontos();
						break;
						
						case "geral":
							$("#desconto").val("").parents("p").hide();
							$("#valor_desconto").val("").parents("p").hide();
							
							$("#valor_desconto_total").parents("p").show();
							$("#desconto_total").parents("p").show();
							
							 calcular_descontos();
						break;
					}
				}
				*/
				
				function calcular_descontos()
				{
					$.post("' . site_url('pedidos/calcular_descontos/' . $pedido->id) . '", $("form").serialize(), function() {
						obter_itens();
					});
				}
			</script>
		';
		// tipo de desconto **
		
		$conteudo .= '<input type="hidden" name="somente_postar" value="" /><div id="somente_postar"></div>';
		
		$conteudo .= hnordt_gerar_feiras_ativas();
		
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= '
			<div style="clear: both;"></div>
			
			<script type="text/javascript">
				$(document).ready(function() {
					setTimeout(function() {
						obter_informacoes_cliente($("input[name=codigo_loja_cliente]").val());
						
						obter_informacoes_prospect($("input[name=id_prospect]").val());
					}, 1000);
				});
				
				function obter_informacoes_cliente(codigo_loja_cliente)
				{
				
					$.get("' . site_url('pedidos/obter_informacoes_cliente_ajax') . '/" + codigo_loja_cliente.replace("|", "_"), function(dados) {
						if (dados.cliente_encontrado)
						{
							$("#informacoes_cliente_prospect").slideDown().html(dados.html);
						}
						else
						{
							$("#informacoes_cliente_prospect").hide();
						}
					}, "json");
					
					
					//Calcular Frete
					var peso_total = $("#valor_peso_total").val();
					
					$.ajax({
						type: "POST",
						url: "' . site_url('pedidos/obter_valor_frete_ajax') . '/",
						data: {peso_total : peso_total, cliente : codigo_loja_cliente},
						success: function(dados){
							dados = dados.split("|");
						
							$("#valor_frete").html("R$ " + dados[0]);
							$("#frete").val(dados[1]);
							
							//frete por kg
							var valor_peso_total = $("#valor_peso_total").val();
							var valor_frete = dados[1];
							var frete_kg = valor_frete / valor_peso_total;
							$("#frete_kg").html("R$ " + number_format(frete_kg, 2, ",", "."));
							
							
							//Frete + Valor Pedido
							$("#total_pedido_frete").html("R$ " + number_format((parseFloat(valor_frete) + parseFloat($("#valor_total_pedido").val())), 2, ",", "."));
							$("#valor_total_pedido_frete").val(parseFloat(valor_frete) + parseFloat($("#valor_total_pedido").val()));
							
						}
					});
					
					
				}
				
				function obter_informacoes_prospect(id_prospect)
				{
					$.get("' . site_url('pedidos/obter_informacoes_prospect_ajax') . '/" + id_prospect, function(dados) {
						if (dados.prospect_encontrado)
						{
							$("#informacoes_cliente_prospect").slideDown().html(dados.html);
						}
						else
						{
							$("#informacoes_cliente_prospect").hide();
						}
					}, "json");
				}
			</script>
		';
		
		$conteudo .= '<h3 id="adicionar_item">Adicionar Item</h3>';
		
		$grupos_produtos = $this->_obter_grupos_produtos();
		$quantidade_grupos_produtos = count($grupos_produtos);
		
		if ($quantidade_grupos_produtos == 0)
		{
			$_POST['codigo_grupo_produtos'] = NULL;
			
			$conteudo .= '
				<script type="text/javascript">
					$(document).ready(function() {
						$("#codigo_grupo_produtos").parents("p").hide();
					});
				</script>
			';
		}
		else if ($quantidade_grupos_produtos == 1)
		{
			$_grupos_produtos = array_keys($grupos_produtos);
			
			$_POST['codigo_grupo_produtos'] = $_grupos_produtos[0];
			
			$conteudo .= '
				<script type="text/javascript">
					$(document).ready(function() {
						$("#codigo_grupo_produtos").parents("p").hide();
					});
				</script>
			';
		}
		
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Grupo de produtos:' . br() . form_dropdown('codigo_grupo_produtos', $grupos_produtos, $this->input->post('codigo_grupo_produtos'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" id="codigo_grupo_produtos" onChange="modificar_grupo_produtos" intermediateChanges="true" ' . (!$_POST['codigo_grupo_produtos'] ? 'displayedValue=""' : NULL))) . '<input type="hidden" name="grupo_produtos_modificado" value="0" /></p>';
		$conteudo .= '
			<script type="text/javascript">
				function modificar_grupo_produtos(valor) {
					$("input[name=grupo_produtos_modificado]").val("1");
					$("form").submit();
				}
			</script>
		';
		
		if ($_POST['grupo_produtos_modificado'] || $_POST['somente_postar']) {
			$conteudo .= '
				<script type="text/javascript">
					$(document).ready(function() {
						$("html, body").animate({ scrollTop: $("#somente_postar").offset().top }, 0);
					});
				</script>
			';
		}
		
		/*
		$conteudo .= '
			<script type="text/javascript">
				$(document).ready(function(){
					$("#tabela_precos").change(function(){
						
					});
				});
			</script>
		';
		*/
	
		//$conteudo .= '<p style="float: left; margin-right: 10px;" id="codigo_tabela_precos">' . form_label('Tabela de preços:' . br() . form_dropdown('codigo_tabela_precos', $this->_obter_tabelas_precos(), $this->input->post('codigo_tabela_precos'), 'id="tabela_precos" dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false"')) . '</p>';
		
		$conteudo .= '<p style="float: left; margin-right: 10px;" id="codigo_tabela_precos">' . form_label('Tabela de preços:' . br() . form_dropdown('codigo_tabela_precos', $this->_obter_tabelas_precos($codigo_usuario), $this->input->post('codigo_tabela_precos'), 'id="tabela_precos" onChange="submit()"')) . '</p>';
		
		
		if ($quantidade_grupos_produtos == 0 || $_POST['codigo_grupo_produtos']) {
			//$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Produto:' . br() . form_dropdown('codigo_produto', $this->_obter_produtos($_POST['codigo_grupo_produtos']), $this->input->post('codigo_produto'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" id="codigo_produto" style="width: 400px;"')) . '</p>';
			$itens_pedido = $this->db->from('itens_pedidos')->where('id_pedido', $pedido->id)->order_by('id', 'asc')->get()->result_array();
			
			// opcionais, somente exibir se existirem opcionais a serem exibidos
			if ($itens_pedido && $this->_obter_opcionais_produto($itens_pedido[0]['codigo']))
			{
				$conteudo .= '<p style="color: #060; font-weight: bold; font-size: 14px;">O produto "' . $itens_pedido[0]['descricao'] . '" foi adicionado. Agora você pode adicionar os opcionais.</p>';
				$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Opcionais para o produto ' . $itens_pedido[0]['descricao'] . ':' . br() . form_dropdown('codigo_produto', $this->_obter_opcionais_produto($itens_pedido[0]['codigo']), $this->input->post('codigo_produto'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" id="codigo_produto" style="width: 400px;"')) . '</p>';
			}
			else
			{
				//$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Produto:' . br() . form_dropdown('codigo_produto', $this->_obter_produtos($_POST['codigo_grupo_produtos']), $this->input->post('codigo_produto'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" id="codigo_produto" style="width: 600px;"')) . '</p>';
			
				$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Produto:' . br() . form_dropdown('codigo_produto', $this->_obter_produtos_tabela_precos($_POST['codigo_grupo_produtos'], $_POST['codigo_tabela_precos']), $this->input->post('codigo_produto'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" id="codigo_produto" style="width: 600px;"')) . '</p>';
			
			}
		}
		
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Quantidade:' . br() . form_input('quantidade', $this->input->post('quantidade'), 'dojoType="dijit.form.NumberSpinner" constraints="{min: 1}"')) . '</p>';
		
		//$conteudo .= '<p style="float: left; margin-right: 10px;">&nbsp;' . br() . form_label(form_radio('usar_tabela_precos', 'nao', TRUE, 'dojoType="dijit.form.RadioButton"') . ' Digitar preço') . ' ' . form_label(form_radio('usar_tabela_precos', 'sim', FALSE, 'dojoType="dijit.form.RadioButton"') . ' Usar tabelas de preços') . '</p>';
		/*
		$conteudo .= '
			<script type="text/javascript">
				$(document).ready(function() {
					$("input[name=usar_tabela_precos]").live("click", function() {
						if ($(this).val() == "sim") {
							$("#codigo_tabela_precos").show();
							$("#preco").hide();
						} else {
							$("#codigo_tabela_precos").hide();
							$("#preco").show();
						}
					});
				});
			</script>
		';
		*/
		$conteudo .= '<div style="clear: both;"></div>';
		//$conteudo .= '<p style="float: left; margin-right: 10px;" id="codigo_tabela_precos">' . form_label('Tabela de preços:' . br() . form_dropdown('codigo_tabela_precos', $this->_obter_tabelas_precos(), $this->input->post('codigo_tabela_precos'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false"')) . '</p>';
		//$conteudo .= '<p style="float: left; margin-right: 10px;" id="preco">' . form_label('Preço:' . br() . form_input('preco', $this->input->post('preco'), 'dojoType="dijit.form.CurrencyTextBox" currency="R$ " displayedValue="R$ 0,00" promptMessage="Para centavos use a vírgula. Exemplo: 1423,20" id="preco"')) . '</p>';
		//$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Quantidade:' . br() . form_input('quantidade', $this->input->post('quantidade'), 'dojoType="dijit.form.NumberSpinner" constraints="{min: 1}"')) . '</p>';
		//$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Desconto (%):' . br() . form_input('desconto', $this->input->post('desconto'), 'dojoType="dijit.form.NumberSpinner" constraints="{min: 0, places: 2}" promptMessage="Para decimais use a vírgula. Exemplo: 13,20" id="desconto" onChange="calcular_valor_desconto"')) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '<p>' . form_submit('adicionar_item', 'Adicionar Item') . '</p>';
		$conteudo .= '
			<script type="text/javascript">
				$(document).ready(function() {
					obter_itens();
					
					$("input[name=adicionar_item]").live("click", function() {
						var _this = this;
						var valor = $(this).val();
						
						$(this).val("Carregando...").attr("disabled", "disabled");
						
						$.post("' . site_url('pedidos/adicionar_item_ajax/' . $pedido->id) . '", $("form").serialize(), function(dados) {
							if (dados.erro)
							{
								alert(dados.erro);
							}
							else
							{
								if (dados.mensagem) alert(dados.mensagem);
								
								obter_itens();
								
								$("input[name=somente_postar]").val("1");
								$("form").submit();
							}
							
							$(_this).val(valor).removeAttr("disabled");
						}, "json");
						
						return false;
					});
					
					$(".editar_preco_item").live("click", function() {
						var _this = this;
						
						var preco = prompt("Digite o novo preço: (exemplo: 5.65)");
						
						$.post("' . site_url('pedidos/editar_item_ajax/' . $pedido->id) . '", { id: $(this).attr("alt"), preco: preco }, function(dados) {
							if (dados.erro)
							{
								alert(dados.erro);
							}
							else
							{
								 if (dados.mensagem) alert(dados.mensagem);
								 
								 obter_itens();
							}
						}, "json");
						
						return false;
					});
					
					$(".editar_quantidade_item").live("click", function() {
						var _this = this;
						
						var quantidade = prompt("Digite a nova quantidade:");
						
						$.post("' . site_url('pedidos/editar_item_ajax/' . $pedido->id) . '", { id: $(this).attr("alt"), quantidade: quantidade }, function(dados) {
							if (dados.erro)
							{
								alert(dados.erro);
							}
							else
							{
								 if (dados.mensagem) alert(dados.mensagem);
								 
								 obter_itens();
							}
						}, "json");
						
						return false;
					});
					
					$(".transferir_quantidade_item").live("click", function() {
						var _this = this;
						
						var quantidade = prompt("Digite a quantidade a ser transferida:");
						
						$.post("' . site_url('pedidos/transferir_quantidade_item_ajax/' . $pedido->id) . '", { id: $(this).attr("alt"), quantidade: quantidade }, function(dados) {
							if (dados.erro)
							{
								alert(dados.erro);
							}
							else
							{
								 if (dados.mensagem) alert(dados.mensagem);
								 
								 obter_itens();
							}
						}, "json");
						
						return false;
					});
					
					$(".excluir_item").live("click", function() {
						if (!confirm("Você realmente deseja excluir este item?")) {
							return false;
						}
						
						$.post("' . site_url('pedidos/excluir_item_ajax/' . $pedido->id) . '", { id: $(this).attr("alt") }, function(dados) {
							obter_itens();
							
							$("input[name=somente_postar]").val("1");
							$("form").submit();
						}, "json");
						
						return false;
					});
				});
				
				function obter_itens()
				{
					$.get("' . site_url('pedidos/obter_itens_ajax/' . $pedido->id) . '", function(dados) {
						$("#tabela_itens_pedido tbody").empty();
						
						var codigo = 0;
						$.each(dados.itens, function(indice, valor) {
							if (new Number(valor._quantidade) > 0) {
								var borda = codigo && codigo != valor.codigo? "" : "";
								
								$("#tabela_itens_pedido tbody").prepend("<tr style=\"" + borda + " background-color: " + valor.cor + ";\"><td style=\"text-align: right;\">" + valor.codigo_real + "</td> <td>" + valor.descricao + "</td> <td>" + valor.unidade_medida + "</td> <td>" + valor.codigo_tabela_precos + "</td> <td>" + valor.preco + "</td> <td><a href=\"#\" class=\"editar_quantidade_item\" alt=\"" + valor.id + "\">" + valor.quantidade + "</a></td> <td>" + valor.valor_preco_quant + "</td> <td>" + valor.ipi + "</td> <td>" + valor.valor_total_com_ipi + "</td> <td>" + valor.st + "</td> <td>" + valor.total_geral + "</td> <td>" + valor.peso + "</td>  <td> <a href=\"#\" class=\"excluir_item\" alt=\"" + valor.id + "\"><img src=\"' . (base_url() . 'misc/imagens/cross.png') . '\" title=\"Excluir\" /></a></td></tr>");
								
								codigo = valor.codigo;
							}
						});
						
						//$("#tabela_itens_pedido tbody").append("<tr><td colspan=\"8\"><strong>Totais</strong></td><td><strong>" + dados.totais.total_st + "</strong></td>  <td><strong>" + dados.totais.total_desconto + "</strong></td> <td><strong style=\"color: #060; font-size: 16px;\" id=\"valor_total_com_ipi_e_desconto\">" + dados.totais.valor_total_com_ipi_e_desconto + "</strong></td> <td>" + dados.totais.peso_total + "</td> </tr>");
						
						$("#peso_total").html(dados.totais.peso_total);
						$("#valor_peso_total").val(dados.totais.valor_peso_total);
						
						$("#total_pedido").html("R$ " + dados.totais.valor_total_com_ipi_e_desconto);
						
						$("#valor_total_pedido").val(dados.totais.valor_total_com_ipi_e_desconto_number);
						
						$("#valor_frete").html(dados.totais.valor_frete);
						
						/*var i = 1;
						var qtd_i = $("#tabela_itens_pedido tbody tr").size();
						$("#tabela_itens_pedido tbody tr").each(function() {
							if (i % 2 == 0 && i != qtd_i)
							{
								$(this).find("td").css("borderBottom", "2px solid #000");
							}
							
							i++;
						});*/
						
						alinhar_conteudo_tabelas();
					}, "json");
				}
				
			</script>
		';
		
		$conteudo .= heading('Itens Adicionados', 3);
		$conteudo .= '
			<div class="ajuda"><p>Clique na quantidade para editar.</p></div>
			
			<table cellspacing="0" class="normal" id="tabela_itens_pedido" style="font-size: 11px;">
				<thead>
					<tr>
		
						<th>Código</th>
						<th>Descrição</th>
						<th>U.M.</th>
						<th>Tabela</th>
						<th>Preço Unit.</th>
						<th>Quant.</th>
						<th>Total</th>
						<th>IPI (%)</th>
						<th>Total c/ IPI (R$)</th>
						<th>ST (R$)</th>
						<th>TOTAL GERAL (R$)</th>
						<th>Peso Total</th>
						
						<th>&nbsp;</th>
					</tr>
				</thead>
				
				<tbody>
					
				</tbody>
			</table>
		';
		
		
		
		// -Totais dos Itens
		$conteudo .= '<br />';
		$conteudo .= '<p>Peso total: <strong><span id="peso_total"></span> KG</strong></p>';
		$conteudo .= '<input type="hidden" name="peso_total" value="" id="valor_peso_total" />';
		
		$conteudo .= '<p>Valor do Frete por KG: <strong><span id="frete_kg"></span></strong></p>';
		
		$conteudo .= '<p>Valor do Frete: <strong><span id="valor_frete"></span></strong></p>';
		$conteudo .= '<input type="hidden" name="frete" value="" id="frete" />';
		
		$conteudo .= '<p style="font-size: 16px"><strong>Total do Pedido: <span id="total_pedido"></span></strong></p>';
		$conteudo .= '<input type="hidden" name="valor_total_pedido" value="" id="valor_total_pedido" />';
		
		$conteudo .= '<p style="font-size: 16px"><strong>Total do Pedido + Frete: <span id="total_pedido_frete"></span></strong></p><br /><br />';
		$conteudo .= '<input type="hidden" name="valor_total_pedido_frete" value="" id="valor_total_pedido_frete" />';
		
		
		
		
		$conteudo .= heading('Outras Informações', 3);
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Condição de pagamento:' . br() . form_dropdown('codigo_forma_pagamento', $this->_obter_formas_pagamento(), $this->input->post('codigo_forma_pagamento'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" ' . (!$_POST['codigo_forma_pagamento'] ? 'displayedValue=""' : NULL))) . '</p>';
		
		$conteudo .= '<div id="campo_condicoes_pagamento"></div>';
		
		//$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Desconto total (%):' . br() . form_input('desconto_total', $this->input->post('desconto_total'), 'dojoType="dijit.form.NumberSpinner" constraints="{min: 0, places: 2}" promptMessage="Para decimais use a vírgula. Exemplo: 13,20" id="desconto_total" onChange="calcular_descontos"')) . '</p>';
		if ($pedido->tipo != 'orcamento')
		{
			$conteudo .= '<p style="float: left; margin-right: 10px;">Transportadora:' . br() . form_dropdown('tipo_frete', array('Redespacho' => 'Redespacho', 'CIF' => 'CIF', 'FOB' => 'FOB'), $this->input->post('tipo_frete'), 'dojoType="dojox.form.DropDownSelect" id="tipo_frete" style="width: 70px;" onChange="verificar_tipo_frete"') . ' <span id="codigo_transportadora">' . form_dropdown('codigo_transportadora', $this->_obter_transportadoras(), $this->input->post('codigo_transportadora'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" ' . (!$_POST['codigo_transportadora'] ? 'displayedValue=""' : NULL)) . '</span></p>';
			$conteudo .= '
				<script type="text/javascript">
					$(document).ready(function() {
						verificar_tipo_frete($("select[name=tipo_frete]").val());
					});
					
					function verificar_tipo_frete(tipo_frete)
					{
						if (tipo_frete == "CIF")
						{
							$("#codigo_transportadora").hide();
						}
						else
						{
							$("#codigo_transportadora").show();
						}
					}
				</script>
			';
		}
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Ordem de compra:' . br() . form_input('codigo_ordem_compra', $this->input->post('codigo_ordem_compra'), 'dojoType="dijit.form.TextBox"')) . '</p>';
		//$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Data de entrega prevista:' . br() . form_input('data_entrega', $this->input->post('data_entrega'), 'dojoType="dijit.form.DateTextBox" constraints="{ min: \'' . ($id_pedido ? $this->input->post('data_entrega') : date('Y-m-d')) . '\' }"')) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Observação:' . br() . form_textarea(array('name' => 'observacao_pedido_imediato', 'value' => $this->input->post('observacao_pedido_imediato'), 'style' => 'height: 100px; width: 400px;', 'dojoType' => 'dijit.form.Textarea', 'id' => 'observacao_pedido_imediato'))) . '<div dojoType="dijit.Tooltip" connectId="observacao_pedido_imediato" position="above">O campo se expande automaticamente.</div></p>';
		//$conteudo .= '<p style="float: left; margin-right: 10px; margin-top: 0;">' . form_label('Observação (Ped. Programado):' . br() . form_textarea(array('name' => 'observacao_pedido_programado', 'value' => $this->input->post('observacao_pedido_programado'), 'style' => 'height: 100px; width: 400px;', 'dojoType' => 'dijit.form.Textarea', 'id' => 'observacao_pedido_programado'))) . '<div dojoType="dijit.Tooltip" connectId="observacao_pedido_programado" position="above">O campo se expande automaticamente.</div></p>';
		$conteudo .= '<div style="clear: both;"></div>';
		
		//$conteudo .= heading('Opções', 3);
		$conteudo .= '<div class="ajuda" style="margin-top: 30px; display: none;"><p>Ao clicar no botão Continuar, seu pedido será salvo e você poderá editar ou concluir o pedido.</p></div>
		<p>' . form_submit('continuar', 'Continuar') . ' ou ' . anchor('pedidos', 'Cancelar') . '</p>';
		
		$conteudo .= form_close();
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function calcular_descontos($id_pedido)
	{
		$pedido = $this->db->from('pedidos')->where('id', $id_pedido)->get()->row();
		$itens = $this->db->from('itens_pedidos')->where('id_pedido', $pedido->id)->get()->result();
		
		switch($this->input->post('tipo_desconto'))
		{
			case 'sem_desconto':
				foreach ($itens as $item)
				{
					$this->db->update('itens_pedidos', array(
						'desconto' => 0
					), array('id' => $item->id));
					$this->_calcular_valores_item_pedido($item->id);
				}
			break;
			
			case 'geral':
				foreach ($itens as $item)
				{
					$this->db->update('itens_pedidos', array(
						'desconto' => $this->input->post('desconto_total')
					), array('id' => $item->id));
					$this->_calcular_valores_item_pedido($item->id);
				}
			break;
		}
	}
	
	function obter_data_entrega_prevista_produto($codigo_produto, $quantidade)
	{
		$this->db_cliente->obter_data_entrega_prevista_produto($codigo_produto, $quantidade);
	}
	
	function obter_itens_ajax($id_pedido)
	{
		if (!$id_pedido)
		{
			redirect();
		}
		
		$pedido = $this->db->from('pedidos')->where('id', $id_pedido)->get()->row();
		
		if (!$pedido)
		{
			redirect();
		}
		
		$itens = $this->db->from('itens_pedidos')->where('id_pedido', $pedido->id)->order_by('id ASC')->get()->result();
		
		foreach ($itens as &$item)
		{

			$valor_total += $item->valor_total;
			//$valor_total_ipi += $item->valor_total_ipi;
			$valor_total_desconto += $item->valor_total_desconto;
			//$valor_total_com_ipi += $item->valor_total_com_ipi;
			//$valor_total_com_ipi_e_desconto += $item->valor_total_com_ipi_e_desconto;
			
			//
			
			$data_entrega_prevista = time();
			
			$total = $data_entrega_prevista[0];
			$data_entrega = $data_entrega_prevista[1];
			
			if ($total >= $item->quantidade)
			{
				$_data_entrega = date('d/m/Y', $data_entrega);
			}
			else
			{
				if ($total && $data_entrega)
					$_data_entrega = number_format($total, 0, ',', '.') . ' p/ ' . date('d/m/Y', $data_entrega) . ', ' . number_format(($item->quantidade - $total), 0, ',', '.') . ' sem previsão';
				else
					$_data_entrega = 'sem previsão';
			}
			
			$item->data_entrega = ($item->tipo == 'imediato' || !$item->quantidade) ? '-'  : $_data_entrega;
			
			//
			
			$preco = $item->preco;
			$valor_total_ipi = $item->valor_total_ipi;
			$quantidade = $item->quantidade;
			
			$item->cor = ($item->tipo == 'imediato') ? '#EBFFEB' : '#FFF2F2';
			//$item->tipo = '<img src="' . (base_url() . 'misc/imagens/pedidos/pedido_' . $item->tipo . '.png') . '" style="height: 12px; width: 12px;" />';
			$item->unidade_medida = $item->unidade_medida ? $item->unidade_medida : '-';
			$item->preco = number_format($preco, 2, ',', '.');
			$item->preco_com_ipi = number_format($item->preco_com_ipi, 2, ',', '.');
			$item->quantidade = number_format($item->quantidade, 0, NULL, '.');
			$item->_quantidade = $item->quantidade;
			//$item->valor_total_ipi = number_format($item->valor_total_ipi, 2, ',', '.');
			$item->valor_total_desconto = number_format($item->valor_total_desconto, 2, ',', '.');
			
			//Tabela de precos
			$tabela_precos = $this->db_cliente->obter_tabela_precos($item->codigo_tabela_precos);
			$item->codigo_tabela_precos = $tabela_precos['descricao'];
		
			
			//$item->valor_total_com_ipi_e_desconto = number_format($item->valor_total_com_ipi_e_desconto, 2, ',', '.');

			//Peso
			$total_peso += ($item->quantidade * $item->peso);
			$item->peso = number_format($item->quantidade * $item->peso, 0, ',', '.');

			
			//Itens novos
			$item->valor_preco_quant =  number_format($preco * $quantidade, 2, ',', '.');
			
			
			$item->valor_total_com_ipi =  '<span title="' . $valor_total_ipi . ' + (' . $preco . ' * ' . $quantidade . ')' . '">' . number_format($valor_total_ipi + ($preco * $quantidade), 2, ',', '.') . '</span>';
			
			//$item->valor_total_com_ipi = $item->quantidade . ' * ' . $item->valor_total_ipi . ' + ' .$item->valor_preco_quant;
			$st = $item->st;
			$item->st = number_format((($preco * $quantidade) * $st) / 100, 2, ',', '.');
			$total_st += (($preco * $quantidade) * $st) / 100;
			
			
			//Desconto
			$total_desconto += (($preco * $quantidade) * $item->desconto) / 100;
			//$total_desconto_ .= ($preco * $quantidade) . ' * ' . $item->desconto . " / 100 - ";
			
			$desconto = (($preco * $quantidade) * $item->desconto) / 100;
			$item->desconto =  number_format((($preco * $quantidade) * $item->desconto) / 100, 2, ',', '.');
			
			
			$item->total_geral = number_format((($valor_total_ipi + ($preco * $quantidade)) + ((($preco * $quantidade) * $st) / 100)) - $desconto , 2, ',', '.');

			$valor_total_com_ipi_e_desconto += (($valor_total_ipi + ($preco * $quantidade)) + ((($preco * $quantidade) * $st) / 100)) - $desconto;
	
			
			
		}

		
		echo json_encode(array('itens' => $itens, 'totais' => array(
			'valor_total' => number_format($valor_total, 2, ',', '.'),
			'valor_total_ipi' => number_format($valor_total_ipi, 2, ',', '.'),
			'valor_total_desconto' => number_format($valor_total_desconto, 2, ',', '.'),
			'valor_total_com_ipi' => number_format($valor_total_com_ipi, 2, ',', '.'),
			'valor_total_com_ipi_e_desconto' => number_format($valor_total_com_ipi_e_desconto, 2, ',', '.'),
			'valor_total_com_ipi_e_desconto_number' => $valor_total_com_ipi_e_desconto,
			'total_st' => number_format($total_st, 2, ',', '.'),
			'peso_total' => number_format($total_peso, 0, ',', '.'),
			'valor_peso_total' => $total_peso,
			'total_desconto' => number_format($total_desconto, 2, ',', '.')
		)));
	}
	
	function adicionar_item_ajax($id_pedido = NULL)
	{
		if (!$id_pedido)
		{
			redirect();
		}
		
		$pedido = $this->db->from('pedidos')->where('id', $id_pedido)->get()->row();
		
		if (!$pedido)
		{
			redirect();
		}
		
		// TODO: verificar se o item pode ser adicionado
		
		$dados = array(
			'erro' => NULL,
			'mensagem' => NULL
		);
		
		if (!$this->input->post('codigo_produto'))
		{
			$dados['erro'] = 'Selecione um produto.';
		}
		else if (!$this->input->post('codigo_tabela_precos') && !$this->input->post('preco'))
		{
			$dados['erro'] = 'Selecione uma tabela de preços ou digite um preço.';
		}
		
		//else if (/*$this->input->post('preco') &&*/ (!is_numeric($this->input->post('preco')) || $this->input->post('preco') <= 0))
		//{
			//$dados['erro'] = 'Digite um preço válido.';
		//}
		
		else if (!is_numeric($this->input->post('quantidade')))
		{
			$dados['erro'] = 'Digite uma quantidade válida.';
		}
		else
		{
			$itens = $this->db->from('itens_pedidos')->where('id_pedido', $pedido->id)->get()->result();
			
			// se houver um opcional
			if ($itens && $this->db_cliente->obter_opcional_produto($this->input->post('codigo_produto'))) {
				$produto = $this->db_cliente->obter_opcional_produto($this->input->post('codigo_produto'));
			} else {
				$produto = $this->db_cliente->obter_produto($this->input->post('codigo_produto'), $this->input->post('codigo_tabela_precos'));
			}
			
			if (!$produto)
			{
				$dados['erro'] = 'Produto não encontrado.';
			}
			else
			{
				$item_imediato_pedido = $this->db->from('itens_pedidos')->where(array('id_pedido' => $pedido->id, 'tipo' => 'imediato', 'codigo' => $produto['codigo']))->get()->row();
				$item_programado_pedido = $this->db->from('itens_pedidos')->where(array('id_pedido' => $pedido->id, 'tipo' => 'programado', 'codigo' => $produto['codigo']))->get()->row();
				
				if ($item_imediato_pedido || $item_programado_pedido)
				{
					$quantidade = ($item_imediato_pedido->quantidade + $item_programado_pedido->quantidade) + $this->input->post('quantidade');
					
					/*
					$quantidade_disponivel_estoque = $this->obter_quantidade_disponivel_estoque_produto($produto['codigo']);
					
					// somar a quantidade do item imediato na quantidade disponível no estoque
					// pois vamos recalcular posteriormente
					$quantidade_disponivel_estoque += $item_imediato_pedido->quantidade;
					
					if ($quantidade <= $quantidade_disponivel_estoque)
					{
						$quantidade_imediata = $quantidade;
						$quantidade_programada = 0;
					}
					else
					{
						$quantidade_imediata = $quantidade_disponivel_estoque;
						$quantidade_programada = $quantidade - $quantidade_disponivel_estoque;
					}
					*/
					
					$quantidade_imediata = $quantidade;
					
					$this->db->update('itens_pedidos', array(
						'quantidade' => $quantidade_imediata,
						'desconto' => $this->input->post('desconto')
					), array('id' => $item_imediato_pedido->id));
					$this->_calcular_valores_item_pedido($item_imediato_pedido->id);
					
					/*
					$this->db->update('itens_pedidos', array(
						'quantidade' => $quantidade_programada,
						'desconto' => $this->input->post('desconto')
					), array('id' => $item_programado_pedido->id));
					$this->_calcular_valores_item_pedido($item_programado_pedido->id);
					*/
					
					$dados['mensagem'] = 'Este produto já foi adicionado. As quantidades foram somadas e o desconto atualizado.';
				}
				else
				{
				
					$quantidade_imediata = $this->input->post('quantidade');
					
					/*
					$quantidade = $this->input->post('quantidade');
					$quantidade_disponivel_estoque = $this->obter_quantidade_disponivel_estoque_produto($produto['codigo']);
					
					if ($quantidade <= $quantidade_disponivel_estoque)
					{
						$quantidade_imediata = $quantidade;
						$quantidade_programada = 0;
					}
					else
					{
						$quantidade_imediata = $quantidade_disponivel_estoque;
						$quantidade_programada = $quantidade - $quantidade_disponivel_estoque;
					}
					
					// vamor inserir primeiro a quantidade programada
					// dessa forma os itens imediatos aparecem primeiro na hora de exibir os itens
					$this->db->insert('itens_pedidos', array(
						'timestamp' => time(),
						'tipo' => 'programado',
						'id_pedido' => $pedido->id,
						'codigo' => $produto['codigo'],
						'codigo_real' => $produto['codigo_real'],
						'descricao' => $produto['descricao'],
						'unidade_medida' => $produto['unidade_medida'],
						'preco' => ($this->input->post('usar_tabela_precos') == 'nao') ? $this->input->post('preco') : $produto['preco'],
						'codigo_tabela_precos' => ($this->input->post('usar_tabela_precos') == 'sim') ? $this->input->post('codigo_tabela_precos') : NULL,
						'ipi' => $produto['ipi'],
						'desconto' => $this->input->post('desconto'),
						'quantidade' => $quantidade_programada
					));
					$this->_calcular_valores_item_pedido($this->db->insert_id());
					*/
					
					$this->db->insert('itens_pedidos', array(
						'timestamp' => time(),
						'tipo' => 'imediato',
						'id_pedido' => $pedido->id,
						'codigo' => $produto['codigo'],
						'codigo_real' => $produto['codigo_real'],
						'descricao' => $produto['descricao'],
						'unidade_medida' => $produto['unidade_medida'],
						'preco' => ($this->input->post('usar_tabela_precos') == 'nao') ? $this->input->post('preco') : $produto['preco'],
						'codigo_tabela_precos' => ($this->input->post('codigo_tabela_precos')) ? $this->input->post('codigo_tabela_precos') : NULL,
						'ipi' => $produto['ipi'],
						'desconto' => $this->input->post('desconto'),
						'quantidade' => $quantidade_imediata,
						'peso' => $produto['peso']
					));
					$this->_calcular_valores_item_pedido($this->db->insert_id());
				}
			}
		}
		
		echo json_encode($dados);
	}
	
	// todo: remover var_dump
	function obter_quantidade_disponivel_estoque_produto($codigo_produto)
	{
		$quantidade_disponivel_estoque = $this->db_cliente->obter_quantidade_disponivel_estoque_produto($codigo_produto);
		
		$produto = $this->db->select('SUM(itens_pedidos.quantidade) AS quantidade')->from('itens_pedidos')->join('pedidos', 'pedidos.id = itens_pedidos.id_pedido')->where('itens_pedidos.codigo = "' . $codigo_produto . '" AND itens_pedidos.tipo = "imediato" AND (pedidos.status IS NULL OR pedidos.status IN ("aguardando_analise_comercial", "aguardando_faturamento", "parcialmente_faturado"))')->get()->row();
		$quantidade_disponivel_estoque -= $produto->quantidade;
		
		if ($quantidade_disponivel_estoque < 0)
		{
			$quantidade_disponivel_estoque = 0;
		}
		
		return $quantidade_disponivel_estoque;
	}
	
	function _calcular_valores_item_pedido($id_item_pedido = NULL)
	{
		$item_pedido = $this->db->from('itens_pedidos')->where('id', $id_item_pedido)->get()->row();
		
		$preco = $item_pedido->preco;
		$quantidade = $item_pedido->quantidade;
		
		// ipi
		$valor_ipi = $preco * ($item_pedido->ipi / 100);
		$valor_total_ipi = $valor_ipi * $quantidade;
		
		// desconto
		$valor_desconto = $preco * ($item_pedido->desconto / 100);
		$valor_total_desconto = $valor_desconto * $quantidade;
		
		// preço
		$preco_com_ipi = $preco + $valor_ipi;
		$preco_com_ipi_e_desconto = $preco_com_ipi - $valor_desconto;
		
		// total
		$valor_total = $preco * $quantidade;
		$valor_total_com_ipi = $preco_com_ipi * $quantidade;
		$valor_total_com_ipi_e_desconto = $preco_com_ipi_e_desconto * $quantidade;
		
		$this->db->update('itens_pedidos', array(
			'preco' => $preco,
			'preco_com_ipi' => $preco_com_ipi,
			'preco_com_ipi_e_desconto' => $preco_com_ipi_e_desconto,
			'valor_ipi' => $valor_ipi,
			'valor_desconto' => $valor_desconto,
			'valor_total' => $valor_total,
			'valor_total_ipi' => $valor_total_ipi,
			'valor_total_desconto' => $valor_total_desconto,
			'valor_total_com_ipi' => $valor_total_com_ipi,
			'valor_total_com_ipi_e_desconto' => $valor_total_com_ipi_e_desconto,
		), array('id' => $item_pedido->id));
	}
	
	function editar_item_ajax($id_pedido = NULL)
	{
		if (!$id_pedido)
		{
			redirect();
		}
		
		$pedido = $this->db->from('pedidos')->where('id', $id_pedido)->get()->row();
		
		if (!$pedido)
		{
			redirect();
		}
		
		// TODO: verificar se o item pode ser editado
		
		$dados = array(
			'erro' => NULL,
			'mensagem' => NULL
		);
		
		if ($this->input->post('preco') && !is_numeric($this->input->post('preco')))
		{
			$dados['erro'] = 'Digite um preço válido.';
		}
		else if ($this->input->post('quantidade') && !is_numeric($this->input->post('quantidade')))
		{
			$dados['erro'] = 'Digite uma quantidade válida.';
		}
		else
		{
			$item_pedido = $this->db->from('itens_pedidos')->where(array('id' => $this->input->post('id')))->get()->row();
			
			$item_imediato_pedido = $this->db->from('itens_pedidos')->where(array('id_pedido' => $pedido->id, 'tipo' => 'imediato', 'codigo' => $item_pedido->codigo))->get()->row();
			$item_programado_pedido = $this->db->from('itens_pedidos')->where(array('id_pedido' => $pedido->id, 'tipo' => 'programado', 'codigo' => $item_pedido->codigo))->get()->row();
			
			if ($item_pedido->tipo == 'programado')
			{
				$quantidade_programada = $this->input->post('quantidade');
				
				$this->db->update('itens_pedidos', array(
					'preco' => $this->input->post('preco') ? $this->input->post('preco') : $item_programado_pedido->preco,
					'codigo_tabela_precos' => $this->input->post('preco') ? NULL : $item_programado_pedido->codigo_tabela_precos,
					'quantidade' => $this->input->post('quantidade') ? $quantidade_programada :  $item_programado_pedido->quantidade
				), array('id' => $item_programado_pedido->id));
				$this->_calcular_valores_item_pedido($item_programado_pedido->id);
			}
			else if ($item_pedido->tipo == 'imediato')
			{
				$quantidade = $this->input->post('quantidade');
				$quantidade_disponivel_estoque = $this->obter_quantidade_disponivel_estoque_produto($produto['codigo']);
				
				// somar a quantidade do item imediato na quantidade disponível no estoque
				// pois vamos recalcular posteriormente
				$quantidade_disponivel_estoque += $item_imediato_pedido->quantidade;
				
				if ($quantidade <= $quantidade_disponivel_estoque)
				{
					$quantidade_imediata = $quantidade;
					$quantidade_programada = $item_programado_pedido->quantidade;
				}
				else
				{
					$quantidade_imediata = $quantidade_disponivel_estoque;
					$quantidade_programada = ($quantidade - $quantidade_disponivel_estoque) + $item_programado_pedido->quantidade;
				}
				
				$this->db->update('itens_pedidos', array(
					'preco' => $this->input->post('preco') ? $this->input->post('preco') : $item_imediato_pedido->preco,
					'codigo_tabela_precos' => $this->input->post('preco') ? NULL : $item_imediato_pedido->codigo_tabela_precos,
					'quantidade' => $this->input->post('quantidade') ? $quantidade_imediata : $item_imediato_pedido->quantidade
				), array('id' => $item_imediato_pedido->id));
				$this->_calcular_valores_item_pedido($item_imediato_pedido->id);
				
				$this->db->update('itens_pedidos', array(
					'preco' => $this->input->post('preco') ? $this->input->post('preco') : $item_programado_pedido->preco,
					'codigo_tabela_precos' => $this->input->post('preco') ? NULL : $item_programado_pedido->codigo_tabela_precos,
					'quantidade' => $this->input->post('quantidade') ? $quantidade_programada :  $item_programado_pedido->quantidade
				), array('id' => $item_programado_pedido->id));
				$this->_calcular_valores_item_pedido($item_programado_pedido->id);
			}
		}
		
		echo json_encode($dados);
	}
	
	function transferir_quantidade_item_ajax($id_pedido = NULL)
	{
		if (!$id_pedido)
		{
			redirect();
		}
		
		$pedido = $this->db->from('pedidos')->where('id', $id_pedido)->get()->row();
		
		if (!$pedido)
		{
			redirect();
		}
		
		// TODO: verificar se o item pode ser editado
		
		$dados = array(
			'erro' => NULL,
			'mensagem' => NULL
		);
		
		if ($this->input->post('quantidade') && !is_numeric($this->input->post('quantidade')))
		{
			$dados['erro'] = 'Digite uma quantidade válida.';
		}
		else
		{
			$item_pedido = $this->db->from('itens_pedidos')->where(array('id' => $this->input->post('id')))->get()->row();
			
			$item_imediato_pedido = $this->db->from('itens_pedidos')->where(array('id_pedido' => $pedido->id, 'tipo' => 'imediato', 'codigo' => $item_pedido->codigo))->get()->row();
			$item_programado_pedido = $this->db->from('itens_pedidos')->where(array('id_pedido' => $pedido->id, 'tipo' => 'programado', 'codigo' => $item_pedido->codigo))->get()->row();
			
			// transferir quantidade imediata para quantidade programada
			if ($item_pedido->tipo == 'imediato')
			{
				if ($item_pedido->quantidade == 0)
				{
					$dados['erro'] = 'Não há quantidade disponível para transferência.';
				}
				else if ($this->input->post('quantidade') > $item_pedido->quantidade)
				{
					$dados['erro'] = 'A quantidade disponível para transferência neste item é ' . number_format($item_pedido->quantidade, 0, ',', '.') . '.';
				}
				else
				{
					$this->db->update('itens_pedidos', array(
						'quantidade' => $item_imediato_pedido->quantidade - $this->input->post('quantidade')
					), array('id' => $item_imediato_pedido->id));
					$this->_calcular_valores_item_pedido($item_imediato_pedido->id);
					
					$this->db->update('itens_pedidos', array(
						'quantidade' => $item_programado_pedido->quantidade + $this->input->post('quantidade')
					), array('id' => $item_programado_pedido->id));
					$this->_calcular_valores_item_pedido($item_programado_pedido->id);
				}
			}
			
			// transferir quantidade programada para quantidade imediata
			else
			{
				$quantidade_disponivel_estoque = $this->obter_quantidade_disponivel_estoque_produto($item_pedido->codigo);
				
				if ($item_pedido->quantidade == 0)
				{
					$dados['erro'] = 'Não há quantidade disponível para transferência.';
				}
				else if ($quantidade_disponivel_estoque == 0)
				{
					$dados['erro'] = 'Não há quantidade disponível no estoque para transferência.';
				}
				else if ($this->input->post('quantidade') > $item_pedido->quantidade)
				{
					$dados['erro'] = 'A quantidade disponível para transferência neste item é ' . number_format($item_pedido->quantidade, 0, ',', '.') . '.';
				}
				else if ($this->input->post('quantidade') > $quantidade_disponivel_estoque)
				{
					$dados['erro'] = 'A quantidade disponível no estoque é ' . number_format($quantidade_disponivel_estoque, 0, ',', '.') . '.';
				}
				else
				{
					$this->db->update('itens_pedidos', array(
						'quantidade' => $item_imediato_pedido->quantidade + $this->input->post('quantidade')
					), array('id' => $item_imediato_pedido->id));
					$this->_calcular_valores_item_pedido($item_imediato_pedido->id);
					
					$this->db->update('itens_pedidos', array(
						'quantidade' => $item_programado_pedido->quantidade - $this->input->post('quantidade')
					), array('id' => $item_programado_pedido->id));
					$this->_calcular_valores_item_pedido($item_programado_pedido->id);
				}
			}
		}
		
		echo json_encode($dados);
	}
	
	function excluir_item_ajax($id_pedido = NULL)
	{
		if (!$id_pedido)
		{
			redirect();
		}
		
		$pedido = $this->db->from('pedidos')->where('id', $id_pedido)->get()->row();
		
		if (!$pedido)
		{
			redirect();
		}
		
		// TODO: verificar se o item pode ser excluído
		
		/*$item_pedido = $this->db->from('itens_pedidos')->where('id', $this->input->post('id'))->get()->row();
		
		$this->db->delete('itens_pedidos', array('id_pedido' => $id_pedido, 'codigo' => $item_pedido->codigo));*/
		
		$this->db->delete('itens_pedidos', array('id' => $this->input->post('id')));
	}
	
	// criada para redirecionar o usuário do "cancelamento" da transformação de orçamento em pedido para a conversão do prospect em cliente
	function converter_prospect($id_prospect, $id_orcamento)
	{
		$this->session->set_userdata('id_orcamento_aguardando_transformacao_pedido', $id_orcamento);
		
		redirect('prospects/ver_detalhes/' . $id_prospect);
	}
	
	
	function ver_detalhes_pdr($id_pedido = NULL, $enviar_email = FALSE)
	{
		
	
		if($id_pedido)
		{
			
			$pedido = $this->db_cliente->obter_pedidos_pdr($id_pedido, FALSE);
			$pedidos = $this->db_cliente->obter_pedidos_pdr($id_pedido, TRUE);
			
		}
		
		
		
		//Pedido
		if($pedido['tipo'] == 'P')
		{
			//Botoes de Copiar e editar
			$conteudo .= '<p style="margin-bottom: 10px;">';
			
			//$conteudo .= anchor('criar_pedidos/informacoes_cliente/pedido/' . $pedido['codigo_representante']. '/' . $pedido['id_pedido'] . '/' . TRUE, 'Copiar Pedido', 'class="botao_a copiar_pedido"');
			
			if ($this->session->userdata('grupo_usuario') == 'gestores_comerciais' || $this->session->userdata('grupo_usuario') == 'supervisores')
			{
			
			
				$conteudo .= anchor('criar_pedidos/informacoes_cliente/pedido/' . $pedido['codigo_representante']. '/' . $pedido['id_pedido'], 'Editar Pedido', 'class="botao_b"');
				
				$conteudo .= anchor('pedidos/aprovar_pedido/' . $pedido['id_pedido'], 'Aprovar Pedido', 'class="botao_d" onclick="return confirm(\'Tem certeza que deseja aprovar esse pedido?\')" ');
				
				if($pedido['status'] != 'R')
				{
					$conteudo .= anchor('pedidos/reprovar_pedido/' . $pedido['id_pedido'], 'Reprovar Pedido', 'class="botao_c"');	
				}
				
				
				
			}
			
			$conteudo .= '</p>';
			$conteudo .= '<div style="clear:both"></div>';
			//-----------
		}
		else if($pedido['tipo'] == 'O')
		{
			// Orçmento
		
			//Botoes de Copiar e editar
			$conteudo .= '<p style="margin-bottom: 10px;">';
			$conteudo .= anchor('criar_pedidos/informacoes_cliente/orcamento/' . $pedido['codigo_representante']. '/' . $pedido['id_pedido'], 'Editar Orçamento', 'class="botao_b"');
			$conteudo .= anchor('pedidos/transformar_pedido/' . $pedido['id_pedido'], 'Transformar em Pedido', 'class="botao_a" onclick="return confirm(\'Tem certeza que deseja transformar esse orçamento em pedido?\')"');
			$conteudo .= '</p>';
			$conteudo .= '<div style="clear:both"></div>';
			//-----------
		}
			
		
		
		
		$conteudo .= '
					<div class="caixa_icones nao_exibir_impressao">
						<ul>
							<li style="background: url(' . base_url() . 'misc/icones/printer1.png) no-repeat;">' . anchor('pedidos/imprimir', 'Imprimir') . '</li>
							<li style="background: url(' . base_url() . 'misc/icones/mail_replay.png) no-repeat;">' . anchor('#', 'Enviar Por E-mail', 'class="enviar_por_email"') . '</li>
						</ul>
					</div>
				';
				
		
		$conteudo .= '
				<script type="text/javascript">
					$(document).ready(function() {
	
						
						$(".enviar_por_email").click(function() {
							$.fn.colorbox({ html: "<h3 style=\"margin: 0;\">Enviar ' . ($pedido['tipo'] == 'orcamento' ? 'Orçamento' : 'Pedido') . ' por E-mail</h3><p>Digite o e-mail:<br /><input name=\"email\" type=\"text\" size=\"40\" /></p><p>Cópia 1:<br /><input name=\"email_2\" type=\"text\" size=\"40\" /></p><p>Cópia 2:<br /><input name=\"email_3\" type=\"text\" size=\"40\" /></p><p><input type=\"submit\" value=\"Enviar\" class=\"enviar_email\" /> <input type=\"submit\" value=\"Cancelar\" onclick=\"$.fn.colorbox.close();\" /></p>" });
							
							return false;
						});

						
						$(".enviar_email").live("click", function() {
							var _this = this;
							var email = $("input[name=email]").val();
							var email_2 = $("input[name=email_2]").val();
							var email_3 = $("input[name=email_3]").val();
							
							$(this).attr("disabled", "disabled").val("Carregando...");
							
							var emails = email;
							
							if (email_2)
							{
								emails += ", " + email_2;
							}
							
							if (email_3)
							{
								emails += ", " + email_3;
							}
							
							$.post("' . site_url('pedidos/enviar_email/' . $pedido['id_pedido']) . '", { emails: emails }, function(dados) {
								if (dados.erro)
								{
									alert(dados.erro);
								}
								else
								{
									alert("Sucesso!");
									
									$.fn.colorbox.close();
								}
								
								$(_this).removeAttr("disabled").val("Enviar");
							}, "json");
						});
					});
				</script>
			';
		
		
		$conteudo .= '<h2>Espelho do Pedido</h2>';
		
		$conteudo .= '<br /><br />';
		
		$conteudo .= '<h4>Informações Gerais</h4>';
		
		//Informações Gerais
		$forma_pagamento = $this->db_cliente->obter_forma_pagamento($pedido ? $pedido['condicao_pagamento'] : $pedido['condicao_pagamento']);
		
		$conteudo .= '
			<table cellspacing="0" class="info">
				<tr>
					<th>O.C.:</th>
					<td>' . $pedido['ordem_compra'] . '</td>
				</tr>
				
				<tr>
					<th>Número:</th>
					<td>' . $pedido['id_pedido'] . '</td>
				</tr>
				
			' . (!$obter_html ? '</table>
			
			<table cellspacing="0" class="info">' : NULL) . '
				<tr>
					<th>Tipo de frete:</th>
					<td>' . $pedido['tipo_frete'] . '</td>
				</tr>
				
				<tr>
					<th>Emissão:</th>
					<td>' . ($pedido['data_emissao'] ? date('d/m/Y', strtotime($pedido['data_emissao'])) : 'não há') . '</td>
				</tr>
				
			' . (!$obter_html ? '</table>
			
			<table cellspacing="0" class="info">' : NULL) . '
				
				<tr>
					<th>Forma pgto.:</th>
					<td>' . $forma_pagamento['descricao'] . '</td>
				</tr>
				
				<tr>
					<th>Data ent. prevista:</th>
					<td>' . ($pedido['data_entrega'] ? date('d/m/Y', strtotime($pedido['data_entrega'])) : 'N/D') . '</td>
				</tr>
				
			' . (!$obter_html ? '</table>
			
			
			
			<table cellspacing="0" class="info">' : NULL) . '
				
				' . ($pedido['motivo_reprovacao'] ? '<tr>
					<th>Motivo da reprovação:</th>
					<td>' . $pedido['motivo_reprovacao'] . '</td>
				</tr>' : NULL) . '
			</table>
			
			<div style="clear: left;"></div>
		';
		
		
		if($pedido['codigo_cliente'] && $pedido['loja_cliente'])
		{
			//Informações deo Cliente
			$conteudo .= '<br /><br />';
			$conteudo .= '<h4>Informações do Cliente</h4>';
			$conteudo .= '<br />';
			
			$cliente = $this->db_cliente->obter_cliente($pedido['codigo_cliente'], $pedido['loja_cliente']);
			
			$conteudo .= '
					<table cellspacing="0" class="info">
						<tr>
							<th>Nome:</th>
							<td>' . $cliente['nome'] . '</td>
						</tr>
						<tr>
							<th>CPF/CNPJ:</th>
							<td>' . $cliente['cpf'] . '</td>
						</tr>
						<tr>
							<th>Telefone:</th>
							<td>' . $cliente['telefone'] . '</td>
						</tr>
					' . (!$obter_html ? '</table>
				
				<table cellspacing="0" class="info">' : NULL) . '
						<tr>
							<th>Código:</th>
							<td>' . $cliente['codigo'] . '</td>
						</tr>
						<tr>
							<th>Loja:</th>
							<td>' . $cliente['loja'] . '</td>
						</tr>
						<tr>
							<th>E-mail:</th>
							<td>' . $cliente['email'] . '</td>
						</tr>
					' . (!$obter_html ? '</table>
				
				<table cellspacing="0" class="info">' : NULL) . '
						<tr>
							<th>Endereço:</th>
							<td>' . $cliente['endereco'] . '</td>
						</tr>
						<tr>
							<th>Bairro:</th>
							<td>' . $cliente['bairro'] . '</td>
						</tr>
						<tr>
							<th>CEP:</th>
							<td>' . $cliente['cep'] . '</td>
						</tr>
					' . (!$obter_html ? '</table>
				
				<table cellspacing="0" class="info">' : NULL) . '
						<tr>
							<th>Cidade:</th>
							<td>' . $cliente['cidade'] . '</td>
						</tr>
						<tr>
							<th>Estado:</th>
							<td>' . $cliente['estado'] . '</td>
						</tr>
						<tr>
							<th>Contato:</th>
							<td>' . $cliente['pessoa_contato'] . '</td>
						</tr>
					</table>
					
					<div style="clear: both;"></div>
				';
		}
		else
		{
			//Informações do Prospect
			$conteudo .= '<br /><br />';
			$conteudo .= '<h4>Informações do Prospect</h4>';
			$conteudo .= '<br />';
			
			$prospect = $this->db->from('prospects')->where('id', $pedido['id_prospects'])->get()->row();
	
			
			$conteudo .= '
					<table cellspacing="0" class="info">
						<tr>
							<th>Nome:</th>
							<td>' . $prospect->nome . '</td>
						</tr>
						<tr>
							<th>CPF/CNPJ:</th>
							<td>' . $prospect->cpf . '</td>
						</tr>
						<tr>
							<th>Telefone:</th>
							<td>' . $prospect->telefone_contato_1 . '</td>
						</tr>
					' . (!$obter_html ? '</table>
				
				<table cellspacing="0" class="info">' : NULL) . '
						<tr>
							<th>Código:</th>
							<td>' . $prospect->id. '</td>
						</tr>
						
						<tr>
							<th>Cidade/Estado:</th>
							<td>' . $prospect->cidade . '/' . $prospect->estado . '</td>
						</tr>
	
						<tr>
							<th>E-mail:</th>
							<td>' . $prospect->email_contato_1 . '</td>
						</tr>
					' . (!$obter_html ? '</table>
				
				<table cellspacing="0" class="info">' : NULL) . '
						<tr>
							<th>Endereço:</th>
							<td>' . $prospect->endereco . '</td>
						</tr>
						<tr>
							<th>Bairro:</th>
							<td>' . $prospect->bairro . '</td>
						</tr>
						<tr>
							<th>CEP:</th>
							<td>' . $prospect->cep . '</td>
						</tr>
					' . (!$obter_html ? '</table>
				
				<table cellspacing="0" class="info">' : NULL) . '

					</table>
					
					<div style="clear: both;"></div>
				';
		}
			
		//Representantes
		$representante = $this->db_cliente->obter_representante($pedido['codigo_representante']);
		
		$conteudo .= heading('Informações do Representante', 3);
		
		$conteudo .= '
			<table cellspacing="0" class="info">
				<tr>
					<th>Nome:</th>
					<td>' . $representante['nome'] . '</td>
				</tr>
				<tr>
					<th>Código:</th>
					<td>' . $representante['codigo'] . '</td>
				</tr>
				<tr>
					<th>CPF/CNPJ:</th>
					<td>' . $representante['cpf'] . '</td>
				</tr>
			' . (!$obter_html ? '</table>
			
			<table cellspacing="0" class="info">' : NULL) . '
				<tr>
					<th>Telefone:</th>
					<td>' . $representante['telefone'] . '</td>
				</tr>
				<tr>
					<th>E-mail:</th>
					<td>' . $representante['email'] . '</td>
				</tr>
				<tr>
					<th>Endereço:</th>
					<td>' . $representante['endereco'] . '</td>
				</tr>
			' . (!$obter_html ? '</table>
			
			<table cellspacing="0" class="info">' : NULL) . '
				<tr>
					<th>Bairro:</th>
					<td>' . $representante['bairro'] . '</td>
				</tr>
				<tr>
					<th>CEP:</th>
					<td>' . $representante['cep'] . '</td>
				</tr>
				<tr>
					<th>Cidade/Estado:</th>
					<td>' . $representante['cidade'] . '/' . $representante['estado'] . '</td>
				</tr>
			' . (!$obter_html ? '</table>
			
			<table cellspacing="0" class="info">' : NULL) . '
		
			</table>
			
			<div style="clear: both;"></div>
		';
		
		
		//Produtos
		$conteudo .= '<br /><br />';
		$conteudo .= '<h4>Produtos</h4>';
		$conteudo .= '<br />';

		
		//---------------------------------
		
		
		if($pedidos)
		{
			
			$conteudo .= '<table class="novo_grid" style="margin-top: 20px">
							<thead>
								<th>Item</th>
								<th>Código</th>
								<th>Descrição</th>
								<th>U.M.</th>
								<th>Tabela</th>
								<th>Preço Unit. (R$)</th>
								<th>Quant.</th>
								<th>Total (R$)</th>
								<th>IPI (%)</th>
								<th>Total c/ IPI (R$)</th>
								<th>ST (R$)</th>
								<th>Desconto (R$)</th>
								<th>Total Geral (R$)</th>
							</thead>
							
							<tbody>';
			
			$item = count($pedidos);
			foreach($pedidos as $produto)
			{
				
				$total_ipi = (($produto['ipi'] / 100) * ($produto['preco_unitario'] * $produto['quantidade'])) + ($produto['preco_unitario'] * $produto['quantidade']);
				$valor_st = (($produto['substituicao_tributaria'] / 100) * ($produto['preco_unitario'] * $produto['quantidade']));
				$valor_desconto = (($produto['desconto'] / 100) * ($produto['preco_unitario'] * $produto['quantidade']));

				$conteudo .= '<tr>
					<td class="center">' . str_pad($item--, 2, 0, STR_PAD_LEFT) . '</td>
					<td class="center">' . $produto['codigo_produto'] . '</td>
					<td>' . $produto['descricao_produto'] . '</td>
					<td>' . $produto['unidade_medida'] . '</td>
					<td>' . $produto['tabela_precos'] . '</td>
					<td class="right">' . number_format($produto['preco_unitario'], 2, ',', '.') . '</td>
					
					<td class="center">' . $produto['quantidade'] . '</td>
					
					<td class="right">' . number_format($produto['preco_unitario'] * $produto['quantidade'], 2, ',', '.')  . '</td>
					<td class="right">' . $produto['ipi'] . '</td>
					<td class="right">' . number_format($total_ipi, 2, ',', '.') . '</td>
					<td class="right">' . number_format($valor_st, 2, ',', '.') . '</td>
					<td class="right">' . number_format($valor_desconto, 2, ',', '.') . '</td>
					<td class="right">' . number_format(($total_ipi + $valor_st) - $valor_desconto, 2, ',', '.') . '</td>
				</tr>';
				
				// Totais
				$total_quantidade += $produto['quantidade'];
				$total_valor += ($total_ipi + $valor_st) - $valor_desconto;
				
				
			}
			
			
			$conteudo .= '</tbody>
			</table>';
			
			
		}
		
		//----------------------
		
		
		
		$conteudo .= heading('Totais', 3);
		$conteudo .= '
			<table cellspacing="0" class="info">
				<tr>
					<th>Quant. vend.:</th>
					<td class="right">' . number_format($total_quantidade, 0, NULL, '.') . '</td>
				</tr>
				
				<tr>
					<th>Valor do pedido (R$):</th>
					<td class="right">' . number_format($total_valor, 2, ',', '.')  . '</td>
				</tr>
				
			</table>
			
			<div style="clear: both;"></div>
		';
		
		##Impressao Pedido Quantidade Vendida
    $impressao['pedido_quantidade_vendida'] = number_format($total_quantidade, 0, NULL, '.') ;
    
    ##Impressao Pedido Quantidade Faturada
    $impressao['pedido_quantidade_faturada'] = number_format($pedido['quantidade_faturada'], 0, NULL, '.');
    
    ##Impressao Pedido Peso Total
    $impressao['pedido_peso_total'] = number_format($total_peso, 0, NULL, '.');
    
    ##Impressao Pedido Valor Frete KG
    $impressao['pedido_valor_frete_kg'] = number_format($valor_frete_kg, 2, ',', '.');
    
    ##Impressao Pedido Valor Frete
    $impressao['pedido_valor_frete'] = number_format($frete, 2, ',', '.');
    
    ##Impressao Pedido Valor Total
    $impressao['pedido_valor_total'] = number_format($frete + $total_valor, 2, ',', '.');
    
    ##Impressao Pedido Valor Parcial
    $impressao['pedido_valor_parcial'] =  number_format($total_valor , 2, ',', '.');
    
    ##Impressao Pedido Valor Total do Pedido Mais o Frete
    $impressao['pedido_total_mais_frete'] = number_format($frete + $total_valor, 2, ',', '.');

	##Impressao Pedido Observacao
    $pedido['obs_pedido'] ? $impressao['observacao'] = $pedido['obs_pedido'] : $impressao['observacao'] = 'N/D';
		
		if($enviar_email)
		{
			return $conteudo;
		}
		
		$this->load->view('layout', array('conteudo' => $conteudo));
		
		
					
				
				
		
	##Impressao Tipo de Espelho
	$pedido['tipo'] == 'orcamento' ? $impressao['tipo'] = 'Orçamento' : $impressao['tipo'] = 'Pedido';
	
	##Impressao Preço Médio/Kg
    $impressao['preco_medio'] = ($total_valor != 0 ? number_format($total_peso / $total_valor, 2, ',', '.') : number_format($total_peso, 2, ',', '.'));

	##Impressao Status
    $impressao['status'] = element($pedido['status'], $this->_obter_status());

	##Impressao Ordem de Compra
    $impressao['ordem_compra'] = $pedido['ordem_compra']; 			

	##Impressao Código
    $impressao['codigo'] = $pedido['id_pedido'];
				

	##Impressao Tipo Frete
    $impressao['tipo_frete'] = $pedido['tipo_frete'];
 
	##Impressao Emissao
    $pedido['data_emissao'] ? $impressao['emissao'] = date('d/m/Y', strtotime($pedido['data_emissao'])) : $impressao['emissao'] = 'não há';

	##Impressao Forma de Pagamento
    $impressao['forma_pagamento'] = $forma_pagamento['descricao'];				

	##Impressao Data Prevista para Entrega
    $_pedido->data_entrega_timestamp ? $impressao['data_entrega'] =  date('d/m/Y', $_pedido->data_entrega_timestamp) : $impressao['data_entrega'] = 'N/D';

##Cliente
    ##Impressao Cliente Código
    $impressao['cliente_codigo'] = $cliente['codigo'];
    
    ##Impressao Cliente Nome
    $impressao['cliente_nome'] = $cliente['nome'];
    
    ##Impressao Cliente CPF
    $impressao['cliente_cpf'] = $cliente['cpf'];
    
    ##Impressao Cliente Endereço
    $impressao['cliente_endereco'] = $cliente['endereco'];
    
    ##Impressao Cliente Bairro
    $impressao['cliente_bairro'] = $cliente['bairro'];
    
    ##Impressao Cliente Cidade
    $impressao['cliente_cidade'] = $cliente['cidade'];
    
    ##Impressao Cliente Estado
    $impressao['cliente_estado'] = $cliente['estado'];
    
    ##Impressao Cliente CEP
    $impressao['cliente_cep'] = $cliente['cep'];
    
    ##Impressao Cliente Contato
    $impressao['cliente_contato'] = $cliente['pessoa_contato'];
    
    ##Impressao Cliente Loja
    $impressao['cliente_loja'] = $cliente['loja'];
    
    ##Impressao Cliente Telefone
    $impressao['cliente_telefone'] = $cliente['telefone'];
    
    ##Impressao Cliente email
    $impressao['cliente_email'] = $cliente['email'];

	 	##Impressao Prospect Nome
	    $impressao['prospect_nome'] = $prospect->nome;

	 	##Impressao Prospect cpf
	    $impressao['prospect_cpf'] = $prospect->cpf ;
	    
	    ##Impressao Prospect telefone_contato_1
	    $impressao['prospect_telefone_contato_1'] = $prospect->telefone_contato_1 ;
	    
	    ##Impressao Prospect Id
	    $impressao['prospect_id'] = $prospect->id;
	    
	    ##Impressao Prospect cidade
	    $impressao['prospect_cidade'] = $prospect->cidade;
	    
	    ##Impressao Prospect estado
	    $impressao['prospect_estado'] = $prospect->estado ;
	    
	    ##Impressao Prospect email_contato_1
	    $impressao['prospect_email_contato_1'] = $prospect->email_contato_1 ;
	    
	    ##Impressao Prospect endereço
	    $impressao['prospect_endereco'] = $prospect->endereco ;
	    
	    ##Impressao Prospect bairro
	    $impressao['prospect_bairro'] = $prospect->bairro;
	    
	    ##Impressao Prospect cep
	    $impressao['prospect_cep'] = $prospect->cep ;
	  
	    
    
##Representante    
    ##Impressao Representante Nome
    $impressao['representante_nome'] = $representante['nome'];
    
    ##Impressao Representante Código
    $impressao['representante_codigo'] = $representante['codigo'];
    
    ##Impressao Representante cpf
    $impressao['representante_cpf'] = $representante['cpf'];
    
    ##Impressao Representante Telefone
    $impressao['representante_telefone'] = $representante['telefone'];
    
    ##Impressao Representante email
    $impressao['representante_email'] = $representante['email'];
    
    	##Impressao Representante endereco
    $impressao['representante_endereco'] =  $representante['endereco'];
    
    ##Impressao Representante bairro
    $impressao['representante_bairro'] = $representante['bairro'];
    
    ##Impressao Representante cep
    $impressao['representante_cep'] = $representante['cep'];
    
    ##Impressao Representante cidade
    $impressao['representante_cidade'] = $representante['cidade'];
    
    ##Impressao Representante estado
    $impressao['representante_estado'] = $representante['estado'];

			##Impressao Transportadora nome
    		$impressao['transportadora_nome'] =  $transportadora['nome'];
    		
    		##Impressao Transportadora codigo
    		$impressao['transportadora_codigo'] =  $transportadora['codigo'];
    		
    		##Impressao Transportadora cnpj
    		$impressao['transportadora_cnpj'] =  $transportadora['cnpj'];
    		
    		##Impressao Transportadora telefone
    		$impressao['transportadora_telefone'] =  $transportadora['telefone'];
    		
    		##Impressao Transportadora endereco
    		$impressao['transportadora_endereco'] =  $transportadora['endereco'];
    		
    		##Impressao Transportadora bairro
    		$impressao['transportadora_bairro'] =  $transportadora['bairro'];
    		
    		##Impressao Transportadora cep
    		$impressao['transportadora_cep'] =  $transportadora['cep'];
    		
    		##Impressao Transportadora cidade
    		$impressao['transportadora_cidade'] =  $transportadora['cidade'];
    		
    		##Impressao Transportadora estado
    		$impressao['transportadora_estado'] =  $transportadora['estado'];

		$item = count($pedidos);
		foreach($pedidos as $produto){
			$items['codigo_item']  = str_pad($item--, 2, 0, STR_PAD_LEFT);
			$items['codigo_produto'] = $produto['codigo_produto'];
			$items['descricao_produto']= $produto['descricao_produto'];
			$items['preco_produto']= $produto['preco_unitario'];
			$items['quantidade_vendida_produto']= $produto['quantidade'];
			$itens[] = $items;
		}
		
	##Impressao Itens do Pedido
    $impressao['pedido_itens'] = $itens;

	

	##Impressao Notas Fiscais
    $impressao['notas_fiscais'] = $pedido['notas_fiscais'];

	if($this->session->userdata('imprimir_pedido')){	
		$this->session->unset_userdata('imprimir_pedido');
	}

	$this->session->set_userdata('imprimir_pedido', $impressao);	
	
	}
	
	
	function ver_detalhes($codigo = NULL, $id = NULL, $obter_html = FALSE)
	{
	
		if ($id && !$this->db->from('pedidos')->where('id', $id)->get()->row())
		{
			$conteudo = '
				<h2>Espelho do Pedido</h2>
				
				<p>O pedido que você estava visualizando foi aprovado e agora está <strong>aguardando faturamento</strong>.</p>
				
				<p>' . anchor('pedidos', 'Clique aqui para ver os pedidos dos últimos 7 dias.') . '</p>
			';
			
			$this->load->view('layout', array('conteudo' => $conteudo));
			
			return;
		}
		
		if (!$codigo && !$id)
		{
			redirect();	
		}
		
		// TODO: representantes/prepostos só podem ver SEUS pedidos
		
		if ($codigo != '0')
		{
			$pedido = $this->db_cliente->obter_pedido($codigo);
		}
		else
		{
			$pedido = $this->db_cliente->obter_pedido(NULL, NULL, $id);
			$_pedido = $this->db->from('pedidos')->where('id', $id)->get()->row();
		}
		
		if (!$pedido)
		{
			redirect();	
		}
		
		if (!$obter_html)
		{
			if ($pedido['status'] || $pedido['tipo'] == 'orcamento')
			{
				$conteudo = '
					<div class="caixa_icones nao_exibir_impressao">
						<ul>
							<!--<li style="background: url(' . base_url() . 'misc/icones/printer1.png) no-repeat;">' . anchor('#', 'Imprimir', 'onclick="print(); return false;"') . '</li>-->
							<li style="background: url(' . base_url() . 'misc/icones/printer1.png) no-repeat;">' . anchor('pedidos/imprimir/', 'Imprimir') . '</li>
							<!--<li style="background: url(' . base_url() . 'misc/icones/pdf.png) no-repeat;">' . anchor('#', 'Gerar PDF', 'class="gerar_pdf"') . '</li>-->
							<li style="background: url(' . base_url() . 'misc/icones/mail_replay.png) no-repeat;">' . anchor('#', 'Enviar Por E-mail', 'class="enviar_por_email"') . '</li>
						</ul>
					</div>
				';
			}	
		}
		else
		{
			$conteudo = '
				<html>
					<head>
						' . meta('content-type', 'text/html; charset=utf-8', 'equiv') . '
						
						<style type="text/css">
							* {
								margin: 0;
								padding: 0;
							}
							
							a {
								color: #069;
								text-decoration: none;
							}
							
							a:hover {
								text-decoration: underline;
							}
							
							body {
								background-color: #FFF;
								color: #000;
								font: normal 12px Verdana, sans-serif;
								padding: 15px;
							}
							
							h2 {
								font: bold 16px "Lucida Sans", sans-serif;
							}
							
							h3 {
								font: bold 12px Verdana, sans-serif;
								margin-top: 20px;
							}
							
							p {
								margin-top: 10px;
							}
							
							table {
								font-size: 10px;
								margin-top: 10px;
							}
							
							table.normal {
								border-collapse: collapse;
								margin-right: 10px;
								text-align: left;
							}
							
							table.normal tbody tr td {
								border: 1px solid #DDD;
								padding: 3px;
							} 
							
							table.normal thead tr th {
								background: -webkit-gradient(linear, 0% 0%, 0% 50%, from(#FFF), to(#DDD));
								border: 1px solid #DDD;
								font-weight: bold;
								padding: 3px;
							}
							
							table.info {
								border-collapse: collapse;
								text-align: left;
							}
							
							table.info td {
								border: 1px solid #DDD;
								border-left: none;
								padding: 3px;
							}
							
							table.info th {
								border: 1px solid #DDD;
								border-right: none;
								padding: 3px;
								text-align: left;
							}
							
							ul {
								list-style: disc inside;
								margin-top: 10px; 
							}
							
							ul li {
								margin: 5px 0 0 5px;
							}
						</style>
					</head>
					
					<body>
			';
		}
		
		$conteudo .= heading('Espelho do ' . ($pedido['tipo'] == 'orcamento' ? 'Orçamento' : 'Pedido'), 2);
	##Impressao Tipo de Espelho
	$pedido['tipo'] == 'orcamento' ? $impressao['tipo'] = 'Orçamento' : $impressao['tipo'] = 'Pedido';
		
		
		
		$conteudo .= '<div class="nao_exibir_impressao">';
		
		if (!$obter_html)
		{
			if ($_pedido && $_pedido->status && $this->db->from('itens_pedidos')->where(array('id_pedido' => $_pedido->id, 'preco_abaixo_minimo' => 1))->get()->row())
			{
				$conteudo .= '<div style="clear: left;"></div>';
				$conteudo .= '<p class="erro">Este pedido possui itens com <strong>preço abaixo do mínimo permitido</strong>.</p>';
			}
			
			if ($pedido['tipo'] == 'orcamento')
			{
				if ($pedido['status'] == 'concluido')
				{
					$conteudo .= '
						<p style="margin-bottom: 10px;">
							' . anchor('pedidos/criar/orcamento/' . ($_pedido->codigo_usuario ? $_pedido->codigo_usuario : 0) . '/' . ($pedido['cliente']['codigo'] ? $pedido['cliente']['codigo'] : 0) . '/' . ($pedido['cliente']['loja'] ? $pedido['cliente']['loja'] : 0) . '/0/' . $id, 'Editar Orçamento', 'class="botao_b"') . '
							' . anchor('pedidos/transformar_pedido/' . $id, 'Transformar Em Pedido', 'class="botao_a ' . (!$pedido['cliente']['codigo'] ? 'cancelar_transformacao_pedido' : NULL) . '"') . ' 
						</p>
						
						<script type="text/javascript">
							$(document).ready(function() {
								$(".cancelar_transformacao_pedido").click(function() {
									$.fn.colorbox({ html: "<p class=\"erro\" style=\"margin: 0;\">Você não pode transformar orçamentos para prospects em pedidos.<br /><strong>Você deve primeiro converter o prospect em cliente.</strong><br /><br /><a href=\"' . site_url('pedidos/converter_prospect/' . $pedido['prospect']->id) . '/' . $_pedido->id . '\">Clique aqui para converter o prospect em cliente.</a></p>" });
									
									return false;
								});
							});
						</script>
					';
				}
				else
				{
					$conteudo .= '
						<p style="margin-bottom: 10px;">
							' . anchor('pedidos/criar/orcamento/' . ($_pedido->codigo_usuario ? $_pedido->codigo_usuario : 0) . '/' . ($pedido['cliente']['codigo'] ? $pedido['cliente']['codigo'] : 0) . '/' . ($pedido['cliente']['loja'] ? $pedido['cliente']['loja'] : 0) . '/0/' . $id, 'Editar Orçamento', 'class="botao_b"') . '
						</p>
					';
				}
			}
			else if (!$pedido['status'])
			{
				$conteudo .= '<p class="erro">Este pedido ainda não foi concluído.</p>';
				
				$conteudo .= '<p style="margin-bottom: 10px;">' . anchor('pedidos/criar/pedido/' . ($_pedido->codigo_usuario ? $_pedido->codigo_usuario : 0) . '/' . ($pedido['cliente']['codigo'] ? $pedido['cliente']['codigo'] : 0) . '/' . ($pedido['cliente']['loja'] ? $pedido['cliente']['loja'] : 0) . '/0/' . $id, 'Editar Pedido', 'class="botao_b"') . ' ' . anchor('pedidos/concluir/' . $id, 'Concluir Pedido', 'class="botao_a"') . anchor('pedidos/cancelar_pedido/' . $_pedido->id, 'Cancelar Pedido', 'class="botao_c"') .'</p>';
			}
			else if ($pedido['status'] == 'aguardando_analise_comercial' && $this->session->userdata('grupo_usuario') == 'gestores_comerciais')
			{
				$conteudo .= '<p style="margin-bottom: 10px;">';
				$conteudo .= anchor('pedidos/criar/pedido/' . ($_pedido->codigo_usuario ? $_pedido->codigo_usuario : 0) . '/' . ($pedido['cliente']['codigo'] ? $pedido['cliente']['codigo'] : 0) . '/' . ($pedido['cliente']['loja'] ? $pedido['cliente']['loja'] : 0) . '/0/' . $id, 'Editar Pedido', 'class="botao_b"'); 
				$conteudo .= anchor('pedidos/aprovar/' . $id, 'Aprovar Pedido', 'class="botao_a"');
				$conteudo .= anchor('pedidos/reprovar/' . $id, 'Reprovar Pedido', 'class="botao_c"');
				if ($this->session->flashdata('opcao_criar_pedido_ver_detalhes_pedido'))
				{
					$conteudo .= anchor('pedidos/criar/novo', 'Criar Novo Pedido', 'class="botao_d"');
				}
				$conteudo .= '</p>';
			}
			else
			{
				$conteudo .= '<p style="margin-bottom: 10px;">';
				//$conteudo .= anchor('pedidos/copiar_ajax/' . ($id ? $id : 0) . '/' . ($codigo ? $codigo : 0), 'Copiar Pedido', 'class="botao_a copiar_pedido"');
				//$conteudo .= anchor('criar_pedidos/informacoes_cliente/pedido/' .$pedido['cliente']['codigo_representante'] . '/' . $pedido['codigo'] . '/' . TRUE . '/A', 'Copiar Pedido', 'class="botao_a"');
				if ($this->session->flashdata('opcao_criar_pedido_ver_detalhes_pedido'))
				{
					$conteudo .= anchor('pedidos/criar/novo', 'Criar Novo Pedido', 'class="botao_d"');
				}
				$conteudo .= '</p>';
			}
			
			$conteudo .= '
				<script type="text/javascript">
					$(document).ready(function() {
						$(".copiar_pedido").click(function() {
							$.get($(this).attr("href"), function(dados) {
								if (dados) {
									$.fn.colorbox({ html: dados });
								} else {
									window.location = "' . site_url('pedidos/redirecionar_pedido_nao_concluido') . '";
								}
							});
							
							return false;
						});
						
						$(".enviar_por_email").click(function() {
							$.fn.colorbox({ html: "<h3 style=\"margin: 0;\">Enviar ' . ($pedido['tipo'] == 'orcamento' ? 'Orçamento' : 'Pedido') . ' por E-mail</h3><p>Digite o e-mail:<br /><input name=\"email\" type=\"text\" size=\"40\" /></p><p>Cópia 1:<br /><input name=\"email_2\" type=\"text\" size=\"40\" /></p><p>Cópia 2:<br /><input name=\"email_3\" type=\"text\" size=\"40\" /></p><p><input type=\"submit\" value=\"Enviar\" class=\"enviar_email\" /> <input type=\"submit\" value=\"Cancelar\" onclick=\"$.fn.colorbox.close();\" /></p>" });
							
							return false;
						});
						
						$(".gerar_pdf").click(function() {
							//$.post("' . base_url() . 'misc/dompdf", { html: "teste", nome_arquivo: "teste" });
							
							return false;
						});
						
						$(".enviar_email").live("click", function() {
							var _this = this;
							var email = $("input[name=email]").val();
							var email_2 = $("input[name=email_2]").val();
							var email_3 = $("input[name=email_3]").val();
							
							$(this).attr("disabled", "disabled").val("Carregando...");
							
							var emails = email;
							
							if (email_2)
							{
								emails += ", " + email_2;
							}
							
							if (email_3)
							{
								emails += ", " + email_3;
							}
							
							$.post("' . site_url('pedidos/enviar_email/' . $codigo . '/' . $id) . '", { emails: emails }, function(dados) {
								if (dados.erro)
								{
									alert(dados.erro);
								}
								else
								{
									alert("Sucesso!");
									
									$.fn.colorbox.close();
								}
								
								$(_this).removeAttr("disabled").val("Enviar");
							}, "json");
						});
					});
				</script>
			';
		}
		
		
		$conteudo .= '</div>';
		
		$conteudo .= '<div style="clear: left;"></div>';
		
		$conteudo .= heading('Informações Gerais', 3);
		
		$forma_pagamento = $this->db_cliente->obter_forma_pagamento($_pedido ? $_pedido->codigo_forma_pagamento : $pedido['codigo_forma_pagamento']);
		
		//$peso_total = $this->db_cliente->obter_peso_total_pedido($_pedido ? $_pedido->id : NULL, $_pedido ? NULL : $pedido['codigo']);
	
		$preco_medio = 0;
		if ($pedido['valor_total'] && $peso_total)
		{
			$preco_medio = $pedido['valor_total'] / $peso_total;
	##Impressao Preço Médio
    $impressao['preco_medio'] = $preco_medio;
		}
		

		
		$conteudo .= '
			<table cellspacing="0" class="info">
				<tr>
					<th>Status:</th>
					<td>' . element($pedido['status'], $this->_obter_status());
	##Impressao Status
    $impressao['status'] = element($pedido['status'], $this->_obter_status());
    	
    	$conteudo .= '</td>
					
				</tr>
				<tr>
					<th>O.C.:</th>
					<td>' . $pedido['codigo_ordem_compra'];
	##Impressao Ordem de Compra
    $impressao['ordem_compra'] = $pedido['codigo_ordem_compra'];			
					
					
		$conteudo .='</td>
				</tr>
			' . (!$obter_html ? '</table>
			
			<table cellspacing="0" class="info">' : NULL) . '
				<tr>
					<th>Código:</th>
					<td>' . $pedido['codigo'];
	##Impressao Código
    $impressao['codigo'] = $pedido['codigo'];

		$conteudo .= '</td>
							
				</tr>
				<tr>
					<th>Tipo de frete:</th>
					<td>' . $pedido['tipo_frete'];
	##Impressao Tipo Frete
    $impressao['tipo_frete'] = $pedido['tipo_frete'];
    
		$conteudo .='</td>
				</tr>
			' . (!$obter_html ? '</table>
			
			<table cellspacing="0" class="info">' : NULL) . '
				<tr>
					<th>Emissão:</th>
					<td>' . ($pedido['emissao_timestamp'] ? date('d/m/Y', $pedido['emissao_timestamp']) : 'não há') ;
	##Impressao Emissao
    $pedido['emissao_timestamp'] ? $impressao['emissao'] = date('d/m/Y', $pedido['emissao_timestamp']) : $impressao['emissao'] ='não há';
		
    	$conteudo .='</td>
				</tr>
				<tr>
					<th>Forma pgto.:</th>
					<td>' . $forma_pagamento['descricao'];
	##Impressao Forma de Pagamento
    $impressao['forma_pagamento'] = $forma_pagamento['descricao'];				

    
    	$conteudo .='</td>
				</tr>
			' . (!$obter_html ? '</table>
			
			
			
			<table cellspacing="0" class="info">' : NULL) . '
				<tr>
					<th>Data ent. prevista:</th>
					<td>' . ($_pedido->data_entrega_timestamp ? date('d/m/Y', $_pedido->data_entrega_timestamp) : 'N/D');
	##Impressao Data Prevista para Entrega
    $_pedido->data_entrega_timestamp ? $impressao['data_entrega'] =  date('d/m/Y', $_pedido->data_entrega_timestamp) : $impressao['data_entrega'] = 'N/D';
    
   		$conteudo .= '</td>
				</tr>
				' . ($_pedido->motivo_reprovacao ? '<tr>
					<th>Motivo da reprovação:</th>
					<td>' . $_pedido->motivo_reprovacao . '</td>
				</tr>' : NULL) . '
			</table>
			
			<div style="clear: left;"></div>
		';
		
		
    
    
    
    
    
    
  
    
    
   
    
   
    
    
		
		
		/*
		
		<table cellspacing="0" class="info">' : NULL) . '
				<tr>
					<th>Preço médio:</th>
					<td>' . number_format(round($preco_medio, 2), 2, ',', '.') . '</td>
				</tr>
				<tr>
					<th>Peso total:</th>
					<td>' . number_format($peso_total, 2, ',', '.') . ' KG</td>
				</tr>
			' . (!$obter_html ? '</table>
		
		*/
		
		
		
		
		if ($pedido['cliente']['codigo'])
		{
			$conteudo .= heading('Informações do Cliente', 3);
			
			if ($_pedido)
			{
				$cliente = $this->db_cliente->obter_cliente($_pedido->codigo_cliente, $_pedido->loja_cliente);
			}
			else
			{
				$cliente = $this->db_cliente->obter_cliente($pedido['cliente']['codigo'], $pedido['cliente']['loja']);
			}
			
			$conteudo .= '
				<table cellspacing="0" class="info">
					<tr>
						<th>Nome:</th>
						<td>' . $pedido['cliente']['nome'] . '</td>
					</tr>
					<tr>
						<th>CPF/CNPJ:</th>
						<td>' . $pedido['cliente']['cpf'] . '</td>
					</tr>
					<tr>
						<th>Telefone:</th>
						<td>' . $cliente['telefone'] . '</td>
					</tr>
				' . (!$obter_html ? '</table>
			
			<table cellspacing="0" class="info">' : NULL) . '
					<tr>
						<th>Código:</th>
						<td>' . $pedido['cliente']['codigo'] . '</td>
					</tr>
					<tr>
						<th>Loja:</th>
						<td>' . $pedido['cliente']['loja'] . '</td>
					</tr>
					<tr>
						<th>E-mail:</th>
						<td>' . $cliente['email'] . '</td>
					</tr>
				' . (!$obter_html ? '</table>
			
			<table cellspacing="0" class="info">' : NULL) . '
					<tr>
						<th>Endereço:</th>
						<td>' . $cliente['endereco'] . '</td>
					</tr>
					<tr>
						<th>Bairro:</th>
						<td>' . $cliente['bairro'] . '</td>
					</tr>
					<tr>
						<th>CEP:</th>
						<td>' . $cliente['cep'] . '</td>
					</tr>
				' . (!$obter_html ? '</table>
			
			<table cellspacing="0" class="info">' : NULL) . '
					<tr>
						<th>Cidade:</th>
						<td>' . $cliente['cidade'] . '</td>
					</tr>
					<tr>
						<th>Estado:</th>
						<td>' . $cliente['estado'] . '</td>
					</tr>
					<tr>
						<th>Contato:</th>
						<td>' . $cliente['pessoa_contato'] . '</td>
					</tr>
				</table>
				
				<div style="clear: both;"></div>
			';
			
##Cliente
    ##Impressao Cliente Código
    $impressao['cliente_codigo'] = $pedido['cliente']['codigo'];
    
    ##Impressao Cliente Nome
    $impressao['cliente_nome'] = $pedido['cliente']['nome'];
    
    ##Impressao Cliente CPF
    $impressao['cliente_cpf'] = $pedido['cliente']['cpf'];
    
    ##Impressao Cliente Endereço
    $impressao['cliente_endereco'] = $cliente['endereco'];
    
    ##Impressao Cliente Bairro
    $impressao['cliente_bairro'] = $cliente['bairro'];
    
    ##Impressao Cliente Cidade
    $impressao['cliente_cidade'] = $cliente['cidade'];
    
    ##Impressao Cliente Estado
    $impressao['cliente_estado'] = $cliente['estado'];
    
    ##Impressao Cliente CEP
    $impressao['cliente_cep'] = $cliente['cep'];
    
    ##Impressao Cliente Contato
    $impressao['cliente_contato'] = $cliente['pessoa_contato'];
    
    ##Impressao Cliente Loja
    $impressao['cliente_loja'] = $pedido['cliente']['loja'];
    
    ##Impressao Cliente Telefone
    $impressao['cliente_telefone'] = $cliente['telefone'];
    
    ##Impressao Cliente email
    $impressao['cliente_email'] = $cliente['email'];
			
		}
		else
		{
			$conteudo .= heading('Informações do Prospect', 3);
			
			$conteudo .= '
				<table cellspacing="0" class="info">
					<tr>
						<th>Nome:</th>
						<td>' . $pedido['prospect']->nome;
	 	##Impressao Prospect Nome
	    $impressao['prospect_nome'] = $pedido['prospect']->nome;
			
	    	$conteudo.='</td>
					</tr>
					<tr>
						<th>CPF/CNPJ:</th>
						<td>' . $pedido['prospect']->cpf ;
	 	##Impressao Prospect cpf
	    $impressao['prospect_cpf'] = $pedido['prospect']->cpf;
			
	    	$conteudo.='</td>
					</tr>
				</table>
				
				<div style="clear: both;"></div>
			';
		}
		
		$representante = $this->db_cliente->obter_representante($pedido['cliente']['codigo_representante']);
		
		$conteudo .= heading('Informações do Representante', 3);
		
		$conteudo .= '
			<table cellspacing="0" class="info">
				<tr>
					<th>Nome:</th>
					<td>' . $representante['nome'] . '</td>
				</tr>
				<tr>
					<th>Código:</th>
					<td>' . $representante['codigo'] . '</td>
				</tr>
				<tr>
					<th>CPF/CNPJ:</th>
					<td>' . $representante['cpf'] . '</td>
				</tr>
			' . (!$obter_html ? '</table>
			
			<table cellspacing="0" class="info">' : NULL) . '
				<tr>
					<th>Telefone:</th>
					<td>' . $representante['telefone'] . '</td>
				</tr>
				<tr>
					<th>E-mail:</th>
					<td>' . $representante['email'] . '</td>
				</tr>
				<tr>
					<th>Endereço:</th>
					<td>' . $representante['endereco'] . '</td>
				</tr>
			' . (!$obter_html ? '</table>
			
			<table cellspacing="0" class="info">' : NULL) . '
				<tr>
					<th>Bairro:</th>
					<td>' . $representante['bairro'] . '</td>
				</tr>
				<tr>
					<th>CEP:</th>
					<td>' . $representante['cep'] . '</td>
				</tr>
				<tr>
					<th>Cidade:</th>
					<td>' . $representante['cidade'] . '</td>
				</tr>
			' . (!$obter_html ? '</table>
			
			<table cellspacing="0" class="info">' : NULL) . '
				<tr>
					<th>Estado:</th>
					<td>' . $representante['estado'] . '</td>
				</tr>
			</table>
			
			<div style="clear: both;"></div>
		';
		
		    
##Representante    
    ##Impressao Representante Nome
    $impressao['representante_nome'] = $representante['nome'];
    
    ##Impressao Representante Código
    $impressao['representante_codigo'] = $representante['codigo'];
    
    ##Impressao Representante cpf
    $impressao['representante_cpf'] = $representante['cpf'];
    
    ##Impressao Representante Telefone
    $impressao['representante_telefone'] = $representante['telefone'];
    
    ##Impressao Representante email
    $impressao['representante_email'] = $representante['email'];
    
    	##Impressao Representante endereco
    $impressao['representante_endereco'] =  $representante['endereco'];
    
    ##Impressao Representante bairro
    $impressao['representante_bairro'] = $representante['bairro'];
    
    ##Impressao Representante cep
    $impressao['representante_cep'] = $representante['cep'];
    
    ##Impressao Representante cidade
    $impressao['representante_cidade'] = $representante['cidade'];
    
    ##Impressao Representante estado
    $impressao['representante_estado'] = $representante['estado'];
    
		
		$transportadora = $this->db_cliente->obter_transportadora($pedido['codigo_transportadora']);
		
		if ($transportadora)
		{
			$conteudo .= heading('Informações da Transportadora', 3);
			
			$conteudo .= '
				<table cellspacing="0" class="info">
					<tr>
						<th>Nome:</th>
						<td>' . $transportadora['nome'] . '</td>
					</tr>
					<tr>
						<th>Código:</th>
						<td>' . $transportadora['codigo'] . '</td>
					</tr>
					<tr>
					<th>CPF/CNPJ:</th>
						<td>' . $transportadora['cnpj'] . '</td>
					</tr>
					' . (!$obter_html ? '</table>
				
				<table cellspacing="0" class="info">' : NULL) . '
					<tr>
						<th>Telefone:</th>
						<td>' . $transportadora['telefone'] . '</td>
					</tr>
					<tr>
						<th>Endereço:</th>
						<td>' . $transportadora['endereco'] . '</td>
					</tr>
					<tr>
						<th>Bairro:</th>
						<td>' . $transportadora['bairro'] . '</td>
					</tr>
				' . (!$obter_html ? '</table>
				
				<table cellspacing="0" class="info">' : NULL) . '
					<tr>
						<th>CEP:</th>
						<td>' . $transportadora['cep'] . '</td>
					</tr>
					<tr>
						<th>Cidade:</th>
						<td>' . $transportadora['cidade'] . '</td>
					</tr>
					<tr>
						<th>Estado:</th>
						<td>' . $transportadora['estado'] . '</td>
					</tr>
				</table>
				
				<div style="clear: both;"></div>
			';
			
			##Impressao Transportadora nome
    		$impressao['transportadora_nome'] =  $transportadora['nome'];
    		
    		##Impressao Transportadora codigo
    		$impressao['transportadora_codigo'] =  $transportadora['codigo'];
    		
    		##Impressao Transportadora cnpj
    		$impressao['transportadora_cnpj'] =  $transportadora['cnpj'];
    		
    		##Impressao Transportadora telefone
    		$impressao['transportadora_telefone'] =  $transportadora['telefone'];
    		
    		##Impressao Transportadora endereco
    		$impressao['transportadora_endereco'] =  $transportadora['endereco'];
    		
    		##Impressao Transportadora bairro
    		$impressao['transportadora_bairro'] =  $transportadora['bairro'];
    		
    		##Impressao Transportadora cep
    		$impressao['transportadora_cep'] =  $transportadora['cep'];
    		
    		##Impressao Transportadora cidade
    		$impressao['transportadora_cidade'] =  $transportadora['cidade'];
    		
    		##Impressao Transportadora estado
    		$impressao['transportadora_estado'] =  $transportadora['estado'];
		}
		
		$conteudo .= heading('Itens do Pedido', 3);
		
		$conteudo .= '<table style="font-size:9px;" cellspacing="0" class="normal"><thead><tr><th>Item</th><th>Produto</th><th>Descrição</th><th>Preço (R$)</th><th>Quant. Vend.</th><th>Total (R$)</th><th>Quant. Fat.</th><th>Faturado (R$)</th></tr></thead><tbody>';
		foreach ($pedido['itens'] as $item_pedido)
		{
			if ($item_pedido['preco_abaixo_minimo'])
			{
				$style = 'font-weight: bold; color: #900;';
			}
			else if ($item_pedido['quantidade_vendida_produto'] != $item_pedido['quantidade_faturada_produto'])
			{
				$style = 'background-color: #FFE5E5;';
			}
			
			$conteudo .= '<tr style="' . $style . '"><td>' . $item_pedido['codigo_item'] . '</td><td>' . $item_pedido['codigo_produto'] . '</td><td>' . $item_pedido['descricao_produto'] . '</td><td>' . number_format($item_pedido['preco_produto'], 2, ',', '.') . '</td><td>' . number_format($item_pedido['quantidade_vendida_produto'], 0, NULL, '.') . '</td><td>'.number_format(($item_pedido['preco_produto'] * $item_pedido['quantidade_vendida_produto']), 2, ',', '.').'</td><td>' . number_format($item_pedido['quantidade_faturada_produto'], 0, NULL, '.') . '</td><td>' . number_format($item_pedido['valor_parcial_item'], 2, ',', '.') . '</td></tr>';
		}
		
	##Impressao Itens do Pedido
    $impressao['pedido_itens'] = $pedido['itens'];
		
		$conteudo .= '</tbody></table>';
		
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= heading('Totais', 3);
		
		
		
		//Total antigo
		/*
		<table cellspacing="0" class="info">
				<tr>
					<th>Quant. vend.:</th>
					<td>' . number_format($pedido['quantidade_vendida'], 0, NULL, '.') . '</td>
				</tr>
				<tr>
					<th>Quant. fat.:</th>
					<td>' . number_format($pedido['quantidade_faturada'], 0, NULL, '.') . '</td>
				</tr>
			' . (!$obter_html ? '</table>
			
			<table cellspacing="0" class="info">' : NULL) . '
				<tr>
					<th>Valor total:</th>
					<td>R$ ' . number_format($pedido['valor_total'], 2, ',', '.') . '</td>
				</tr>
				<tr>
					<th>Valor parcial:</th>
					<td>R$ ' . number_format($pedido['valor_parcial'], 2, ',', '.') . '</td>
				</tr>
			</table>
			
			<div style="clear: both;"></div>
		*/

		
		if($_pedido->frete == 0 || $_pedido->peso_total == 0)
		{
			$valor_frete_kg = 0;
		}
		else
		{
			$valor_frete_kg = $_pedido->frete / $_pedido->peso_total;
		}

		
		
		$conteudo .= '
			
			
			<table cellspacing="0" class="info">
				<tr>
					<th>Quant. vend.:</th>
					<td>' . number_format($pedido['quantidade_vendida'], 0, NULL, '.') . '</td>
				</tr>
				
				<tr>
					<th>Quant. fat.:</th>
					<td>' . number_format($pedido['quantidade_faturada'], 0, NULL, '.') . '</td>
				</tr>
			
				<tr>
					<th>Peso total:</th>
					<td>' . number_format($_pedido->peso_total, 0, NULL, '.') . ' KG</td>
				</tr>
				
				<tr>
					<th>Valor do Frete por KG:</th>
					<td>R$ ' . number_format($valor_frete_kg, 2, ',', '.')  . '</td>
				</tr>
			
				<tr>
					<th>Valor do Frete:</th>
					<td>R$ ' . number_format($_pedido->frete, 2, ',', '.')  . '</td>
				</tr>
				
				<tr>
					<th>Valor total:</th>
					<td>R$ ' . number_format($pedido['valor_total'], 2, ',', '.') . '</td>
				</tr>
				
				<tr>
					<th>Valor parcial:</th>
					<td>R$ ' . number_format($pedido['valor_parcial'], 2, ',', '.') . '</td>
				</tr>
				
				<tr>
					<th>Total do Pedido + Frete:</th>
					<td>R$ ' . number_format($_pedido->frete + $pedido['valor_total'], 2, ',', '.')  . '</td>
				</tr>
				
			</table>
			
			<div style="clear: both;"></div>
		';
		
	##Impressao Pedido Quantidade Vendida
    $impressao['pedido_quantidade_vendida'] = number_format($pedido['quantidade_vendida'], 0, NULL, '.');
    
    ##Impressao Pedido Quantidade Faturada
    $impressao['pedido_quantidade_faturada'] = number_format($pedido['quantidade_faturada'], 0, NULL, '.');
    
    ##Impressao Pedido Peso Total
    $impressao['pedido_peso_total'] = number_format($_pedido->peso_total, 0, NULL, '.');
    
    ##Impressao Pedido Valor Frete KG
    $impressao['pedido_valor_frete_kg'] = number_format($valor_frete_kg, 2, ',', '.');
    
    ##Impressao Pedido Valor Frete
    $impressao['pedido_valor_frete'] = number_format($_pedido->frete, 2, ',', '.');
    
    ##Impressao Pedido Valor Total
    $impressao['pedido_valor_total'] = number_format($pedido['valor_total'], 2, ',', '.');
    
    ##Impressao Pedido Valor Parcial
    $impressao['pedido_valor_parcial'] = number_format($pedido['valor_parcial'], 2, ',', '.');
    
    ##Impressao Pedido Valor Total do Pedido Mais o Frete
    $impressao['pedido_total_mais_frete'] = number_format($_pedido->frete + $pedido['valor_total'], 2, ',', '.');
    
        
		
		//if ($_pedido->observacao)
		//{
			$conteudo .= '<table cellspacing="0" class="info">
				<tr>
					<th>Observação:</th>
					<td>' . ($_pedido->observacao ? $_pedido->observacao : 'N/D') . '</td>
				</tr>
			</table>
			
			<div style="clear: both;"></div>';
	##Impressao Pedido Observacao
    $_pedido->observacao ? $impressao['observacao'] = $_pedido->observacao : $impressao['observacao'] = 'N/D';
		//}
		
		if ($pedido['notas_fiscais'] && !$obter_html)
		{
			$conteudo .= heading('Notas Fiscais', 3);
			
			$conteudo .= '<table cellspacing="0" class="normal"><thead><tr><th>Código N.F.</th><th>Data Emissão</th><th>Total (R$)</th><th>ICMS (R$)</th><th>IPI (R$)</th><th>Frete</th><th>Transp.</th><th>Tel. Transp.</th><th class="nao_exibir_impressao">Opções</th></tr></thead><tbody>';
			foreach ($pedido['notas_fiscais'] as $nota_fiscal)
			{
				$conteudo .= '<tr><td>' . $nota_fiscal['codigo'] . '</td><td>' . date('d/m/Y', $nota_fiscal['emissao_timestamp']) . '</td><td>' . number_format($nota_fiscal['valor_total'], 2, ',', '.') . '</td><td>' . number_format($nota_fiscal['valor_icms'], 2, ',', '.') . '</td><td>' . number_format($nota_fiscal['valor_ipi'], 2, ',', '.') . '</td><td>' . $nota_fiscal['tipo_frete'] . '</td><td>' . $nota_fiscal['transportadora']['nome'] . '</td><td>' . $nota_fiscal['transportadora']['telefone'] . '</td><td class="nao_exibir_impressao"><a href="#nota_fiscal_' . $nota_fiscal['codigo'] . '" class="colorbox_inline">Ver Títulos</a>';
				$conteudo .= '<div style="display: none;"><div id="nota_fiscal_' . $nota_fiscal['codigo'] . '" style="overflow: hidden;"><table cellspacing="0" style="width: 800px;"><thead><tr><th>Código N.F.</th><th>Parcela</th><th>Status</th><th>Valor (R$)</th><th>Vencimento</th><th>Pagamento</th></tr></thead><tbody>';
				foreach ($nota_fiscal['titulos'] as $titulo)
				{
					$conteudo .= '<tr><td>' . $nota_fiscal['codigo'] . '</td><td>' . $titulo['parcela'] . '</td><td>' . $titulo['status'] . '</td><td>' . number_format($titulo['valor'], 2, ',', '.') . '</td><td>' . date('d/m/Y', $titulo['vencimento_timestamp']) . '</td><td>' . ($titulo['pagamento_timestamp'] ? date('d/m/Y', $titulo['pagamento_timestamp']) : 'não há') . '</td></tr>';
				}
				$conteudo .= '</tbody></table></div></div>';
				$conteudo .= '</td></tr>';
			}
			$conteudo .= '</tbody></table>';
	##Impressao Notas Fiscais
    $impressao['notas_fiscais'] = $pedido['notas_fiscais'];	
			
		}
		
		if ($obter_html)
		{
			$conteudo .= '</body></html>';
			
			return $conteudo;
		}
		else
		{
			$this->load->view('layout', array('conteudo' => $conteudo));
		}
	if($this->session->userdata('imprimir_pedido')){	
		$this->session->unset_userdata('imprimir_pedido');
	}

	$this->session->set_userdata('imprimir_pedido', $impressao);


	}
	
	
	function concluir($id = NULL)
	{
		// TODO: permissões
		
		if (!$id)
		{
			redirect('pedidos');	
		}
		
		$pedido = $this->db->from('pedidos')->where('id', $id)->get()->row();
		
		if (!$pedido || $pedido->status)
		{
			redirect('pedidos');	
		}
		
		$this->db->update('pedidos', array('status' => 'aguardando_analise_comercial'), array('id' => $pedido->id));
		
		$this->session->set_flashdata('opcao_criar_pedido_ver_detalhes_pedido', TRUE);
		
		redirect('pedidos/ver_detalhes/0/' . $pedido->id);
	}
	
	function aprovar($id = NULL)
	{
		// TODO: permissões
		
		if (!$id)
		{
			redirect('pedidos');	
		}
		
		$pedido = $this->db->from('pedidos')->where('id', $id)->get()->row();
		
		if (!$pedido || $pedido->status != 'aguardando_analise_comercial')
		{
			redirect('pedidos');	
		}
		
		$this->db->delete('pedidos', array('id' => $pedido->id));
		
		// TODO: protheus
		
		redirect('pedidos');
	}
	
	function transformar_pedido($id_pedido = NULL)
	{
		// TODO: permissões
		
		if (!$id_pedido)
		{
			redirect('pedidos');	
		}
		
		$confirmacao = $this->db_cliente->trasformar_pedido($id_pedido);
		
		if($confirmacao)
		{
			redirect('pedidos/ver_detalhes_pdr/' . $id_pedido);
		}
	}
	
	function aprovar_pedido($id_pedido = NULL)
	{
		// TODO: permissões
		
		if (!$id_pedido)
		{
			redirect('pedidos');	
		}
		
		$confirmacao = $this->db_cliente->aprovar_pedido($id_pedido);
		
		if($confirmacao)
		{
			redirect('pedidos/aguardando_analise');
		}
	}
	
	function reprovar_pedido($id_pedido = NULL)
	{
	
		if (!$id_pedido)
		{
			redirect('pedidos');	
		}
	
		if($_POST)
		{
			if (!$this->input->post('motivo_reprovacao'))
			{
				$erro = 'Digite o motivo da reprovação.';
			}
			else
			{
				$confirmacao = $this->db_cliente->reprovar_pedido($id_pedido, $this->input->post('motivo_reprovacao'));
		
				if($confirmacao)
				{
					redirect('pedidos/aguardando_analise');
				}
			}
		}
	
		// VALIDAR FORM **
		
		// ** EXIBIR FORM
		$conteudo .= heading('Reprovar ' . ($pedido->tipo == 'orcamento' ? 'Orçamento' : 'Pedido'), 2);
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		$conteudo .= form_open(current_url());
		$conteudo .= '<p>' . form_label('Motivo da reprovação:' . br() . form_textarea(array('cols' => 60, 'rows' => 8, 'name' => 'motivo_reprovacao', 'value' => $this->input->post('motivo_reprovacao')))) . '</p>';
		$conteudo .= '<p>' . form_submit('concluir', 'Concluir') . ' ou ' . anchor('pedidos/ver_detalhes/0/' . $pedido->id, 'Cancelar') . '</p>';
		$conteudo .= form_close();
		// EXIBIR FORM **
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function reprovar($id = NULL)
	{
		// TODO: permissões
		
		if (!$id)
		{
			redirect('pedidos');	
		}
		
		$pedido = $this->db->from('pedidos')->where('id', $id)->get()->row();
		
		if (!$pedido || ($pedido->tipo == 'pedido' && $pedido->status != 'aguardando_analise_comercial'))
		{
			redirect('pedidos');	
		}
		
		// ** VALIDAR FORM
		if ($_POST)
		{
			if (!$this->input->post('motivo_reprovacao'))
			{
				$erro = 'Digite o motivo da reprovação.';
			}
			else
			{
				$this->db->update('pedidos', array('status' => 'reprovado_pelo_comercial', 'motivo_reprovacao' => $this->input->post('motivo_reprovacao')), array('id' => $pedido->id));
				
				redirect('pedidos/ver_detalhes/0/' . $pedido->id);
			}
		}
		// VALIDAR FORM **
		
		// ** EXIBIR FORM
		$conteudo .= heading('Reprovar ' . ($pedido->tipo == 'orcamento' ? 'Orçamento' : 'Pedido'), 2);
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		$conteudo .= form_open(current_url());
		$conteudo .= '<p>' . form_label('Motivo da reprovação:' . br() . form_textarea(array('cols' => 60, 'rows' => 8, 'name' => 'motivo_reprovacao', 'value' => $this->input->post('motivo_reprovacao')))) . '</p>';
		$conteudo .= '<p>' . form_submit('concluir', 'Concluir') . ' ou ' . anchor('pedidos/ver_detalhes/0/' . $pedido->id, 'Cancelar') . '</p>';
		$conteudo .= form_close();
		// EXIBIR FORM **
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function editar_codigo_usuario_consultar_pedidos($novo_codigo = NULL)
	{
		$this->session->set_userdata('codigo_usuario_consultar_pedidos', $novo_codigo);
		
		redirect('pedidos');
	}
	
	// TODO: remover
	function consultar_pedidos($codigo_cliente = NULL, $loja_cliente = NULL)
	{
		redirect('pedidos/index/' . $codigo_cliente . '/' . $loja_cliente);
	}
	
	function editar_codigo_usuario_consultar_notas_fiscais($novo_codigo = NULL)
	{
		$this->session->set_userdata('codigo_usuario_consultar_notas_fiscais', $novo_codigo);
		
		redirect('pedidos/consultar_notas_fiscais');
	}
	
	function consultar_notas_fiscais($codigo_cliente = NULL, $loja_cliente = NULL)
	{
		// ** VALIDAR FORM
		if ($_POST)
		{
			if (!$this->input->post('cliente'))
			{
				$erro = 'Selecione um cliente.';
			}
			else if ($this->input->post('data_inicial') && !$this->_validar_data($this->input->post('data_inicial')))
			{
				$erro = 'Digite uma data inicial válida.';
			}
			else if ($this->input->post('data_final') && !$this->_validar_data($this->input->post('data_final')))
			{
				$erro = 'Digite uma data final válida.';
			}
			else
			{
				if ($this->input->post('data_inicial')) {
					$data_inicial = explode('/', $this->input->post('data_inicial'));
					$data_inicial = $data_inicial[2] . $data_inicial[1] . $data_inicial[0];
				}
				
				if ($this->input->post('data_final')) {
					$data_final = explode('/', $this->input->post('data_final'));
					$data_final = $data_final[2] . $data_final[1] . $data_final[0];
				}
				
				$codigo_loja_cliente = explode('|', $this->input->post('codigo_loja_cliente'));
				
				$notas_fiscais = $this->db_cliente->obter_notas_fiscais($codigo_loja_cliente[0], $codigo_loja_cliente[1], $data_inicial, $data_final);
			}
		}
		else if ($codigo_cliente && $loja_cliente)
		{
			$cliente = $this->db_cliente->obter_cliente($codigo_cliente, $loja_cliente);
			
			if ($cliente)
			{
				$notas_fiscais = $this->db_cliente->obter_notas_fiscais($cliente['codigo'], $cliente['loja']);
				
				$_POST['cliente'] = $cliente['nome'];
				$_POST['codigo_loja_cliente'] = $cliente['codigo'] . '|' . $cliente['loja'];
			}
		}
		// VALIDAR FORM **
		
		$conteudo .= '
			<div class="caixa_b">
				<p>' . form_label('Representante: ' . form_dropdown('codigo_usuario_consultar_notas_fiscais', $this->_obter_representantes(), $this->session->userdata('codigo_usuario_consultar_notas_fiscais'), 'onchange="window.location = \'' . site_url('pedidos/editar_codigo_usuario_consultar_notas_fiscais') . '/\' + $(this).val();"')) . '</p>
			</div>
		';
		
		// ** EXIBIR FORM
		$conteudo .= heading('Consultar Notas Fiscais', 2);
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		$conteudo .= form_open(current_url());
		$conteudo .= '<p>' . form_label('Cliente:' . br() . form_input('cliente', $this->input->post('cliente'), 'alt="obter_clientes_consultar_notas_fiscais" class="autocomplete" size="40"') . form_hidden('codigo_loja_cliente', $this->input->post('codigo_loja_cliente'))) . '</p>';
		$conteudo .= '<p>' . form_label('Data:' . br() . form_input('data_inicial', $this->input->post('data_inicial'), 'class="datepicker data_inicial" size="6"')) . ' à ' . form_input('data_final', $this->input->post('data_final'), 'class="datepicker" size="6"') . '</p>';
		$conteudo .= '<p>' . form_submit('consultar', 'Consultar') . '</p>';
		$conteudo .= form_close();
		// EXIBIR FORM **
		
		if (isset($notas_fiscais))
		{
			
			if ($this->input->post('consultar'))
			{
		
				// ** EXIBIR NOTAS FISCAIS
				$conteudo .= heading('Resultados da Consulta', 3);
				$conteudo .= '<table cellspacing="0"><thead><tr><th>Código N.F.</th><th>Data Emissão</th><th>Total (R$)</th><th>ICMS (R$)</th><th>IPI (R$)</th><th>Frete</th><th>Transp.</th><th>Tel. Transp.</th><th>Opções</th></tr></thead><tbody>';
				foreach ($notas_fiscais as $nota_fiscal)
				{
					$conteudo .= '<tr><td>' . $nota_fiscal['codigo'] . '</td><td>' . date('d/m/Y', $nota_fiscal['emissao_timestamp']) . '</td><td>' . number_format($nota_fiscal['valor_total'], 2, ',', '.') . '</td><td>' . number_format($nota_fiscal['valor_icms'], 2, ',', '.') . '</td><td>' . number_format($nota_fiscal['valor_ipi'], 2, ',', '.') . '</td><td>' . $nota_fiscal['tipo_frete'] . '</td><td>' . $nota_fiscal['transportadora']['nome'] . '</td><td>' . $nota_fiscal['transportadora']['telefone'] . '</td><td><a href="#nota_fiscal_' . $nota_fiscal['codigo'] . '" class="colorbox_inline">Ver Títulos</a>';
					$conteudo .= '<div style="display: none;"><div id="nota_fiscal_' . $nota_fiscal['codigo'] . '" style="overflow: hidden;"><table cellspacing="0" style="width: 800px;"><thead><tr><th>Código N.F.</th><th>Parcela</th><th>Status</th><th>Valor (R$)</th><th>Vencimento</th><th>Pagamento</th></tr></thead><tbody>';
					foreach ($nota_fiscal['titulos'] as $titulo)
					{
						$conteudo .= '<tr><td>' . $nota_fiscal['codigo'] . '</td><td>' . $titulo['parcela'] . '</td><td>' . $titulo['status'] . '</td><td>' . number_format($titulo['valor'], 2, ',', '.') . '</td><td>' . date('d/m/Y', $titulo['vencimento_timestamp']) . '</td><td>' . ($titulo['pagamento_timestamp'] ? date('d/m/Y', $titulo['pagamento_timestamp']) : 'não há') . '</td></tr>';
					}
					$conteudo .= '</tbody></table></div></div>';
					$conteudo .= '</td></tr>';
				}
				$conteudo .= '</tbody></table>';
				// EXIBIR NOTAS FISCAIS **
			
			}
		}
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function consultar_titulos_vencidos($codigo_cliente = NULL, $loja_cliente = NULL)
	{
		// ** VALIDAR FORM
		if ($_POST)
		{
			if ($this->input->post('data_vencimento_inicial') && !$this->_validar_data($this->input->post('data_vencimento_inicial')))
			{
				$erro = 'Digite uma data de vencimento inicial válida.';
			}
			else if ($this->input->post('data_vencimento_final') && !$this->_validar_data($this->input->post('data_vencimento_final')))
			{
				$erro = 'Digite uma data de vencimento final válida.';
			}
			else
			{
				if ($this->input->post('data_vencimento_inicial')) {
					$data_vencimento_inicial = explode('/', $this->input->post('data_vencimento_inicial'));
					$data_vencimento_inicial = $data_vencimento_inicial[2] . $data_vencimento_inicial[1] . $data_vencimento_inicial[0];
				}
				
				if ($this->input->post('data_vencimento_final')) {
					$data_vencimento_final = explode('/', $this->input->post('data_vencimento_final'));
					$data_vencimento_final = $data_vencimento_final[2] . $data_vencimento_final[1] . $data_vencimento_final[0];
				}
				
				$titulos = $this->db_cliente->obter_titulos_vencidos(NULL, NULL, $data_vencimento_inicial, $data_vencimento_final);
			}
		}
		else if ($codigo_cliente && $loja_cliente)
		{
			$cliente = $this->db_cliente->obter_cliente($codigo_cliente, $loja_cliente);
			
			if ($cliente)
			{
				$titulos = $this->db_cliente->obter_titulos_vencidos($cliente['codigo'], $cliente['loja']);
				
				$_POST['cliente'] = $cliente['nome'];
				$_POST['codigo_loja_cliente'] = $cliente['codigo'] . '|' . $cliente['loja'];
			}
		}
		// VALIDAR FORM **
		
		// ** EXIBIR FORM
		$conteudo .= heading('Consultar Títulos Vencidos', 2);
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		$conteudo .= form_open(current_url());
		if ($cliente)
		{
			$conteudo .= '<p>Cliente:' . br() .  $cliente['nome'] . ' - ' . $cliente['cpf'] . '</p>';
		}
		$conteudo .= '<p>' . form_label('Data de vencimento:' . br() . form_input('data_vencimento_inicial', $this->input->post('data_vencimento_inicial'), 'class="datepicker data_inicial" size="6"')) . ' à ' . form_input('data_vencimento_final', $this->input->post('data_vencimento_final'), 'class="datepicker" size="6"') . '</p>';
		$conteudo .= '<p>' . form_submit('consultar', 'Consultar') . '</p>';
		$conteudo .= form_close();
		// EXIBIR FORM **
		
		if (isset($titulos))
		{
			
			if($this->input->post('consultar'))
			{
		
				// ** EXIBIR TÍTULOS
				$conteudo .= heading('Resultados da Consulta', 3);
				$conteudo .= '<table cellspacing="0"><thead><tr><th>Cliente</th><th>Código N.F.</th><th>Parcela</th><th>Status</th><th>Valor (R$)</th><th>Vencimento</th><th>Pagamento</th></tr></thead><tbody>';
				foreach ($titulos as $titulo)
				{
					$conteudo .= '<tr><td>' . $titulo['cliente']['nome'] . '</td><td>' . $titulo['codigo'] . '</td><td>' . $titulo['parcela'] . '</td><td>' . $titulo['status'] . '</td><td>' . number_format($titulo['valor'], 2, ',', '.') . '</td><td>' . date('d/m/Y', $titulo['vencimento_timestamp']) . '</td><td>' . ($titulo['pagamento_timestamp'] ? date('d/m/Y', $titulo['pagamento_timestamp']) : 'não há') . '</td></tr>';
				}
				$conteudo .= '</tbody></table>';
				// EXIBIR TÍTULOS **
			
			}
		}
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function obter_valor_frete_ajax()
	{

		$total_peso = $_POST["peso_total"];
		$cliente = explode('|', $_POST["cliente"]);
		
		$tabela_preco = $this->db_cliente->obter_tabela_preco($cliente[0], $cliente[1]);
		
		//Valor do Frete
		if($total_peso <= 600)
		{
			$frete = $this->db->select(strtolower($tabela_preco['tabela_frete']))->from('tabela_frete')->where(array('kg' => 1))->get()->row_array();
		
			$valor_frete = $frete[strtolower($tabela_preco['tabela_frete'])];
		}
		else if($total_peso >= 601 && $total_peso <= 999)
		{
			$frete = $this->db->select(strtolower($tabela_preco['tabela_frete']))->from('tabela_frete')->where(array('kg' => 601))->get()->row_array();
		
			$valor_frete = $total_peso * $frete[strtolower($tabela_preco['tabela_frete'])];
		}
		else if($total_peso >= 1000 && $total_peso <= 1999)
		{
			$frete = $this->db->select(strtolower($tabela_preco['tabela_frete']))->from('tabela_frete')->where(array('kg' => 1000))->get()->row_array();
		
			$valor_frete = $total_peso * $frete[strtolower($tabela_preco['tabela_frete'])];
		}
		else if($total_peso >= 2000 && $total_peso <= 2999)
		{
			$frete = $this->db->select(strtolower($tabela_preco['tabela_frete']))->from('tabela_frete')->where(array('kg' => 2000))->get()->row_array();
		
			$valor_frete = $total_peso * $frete[strtolower($tabela_preco['tabela_frete'])];
		}
		else if($total_peso >= 3000 && $total_peso <= 9999)
		{
			$frete = $this->db->select(strtolower($tabela_preco['tabela_frete']))->from('tabela_frete')->where(array('kg' => 3000))->get()->row_array();
		
			$valor_frete = $total_peso * $frete[strtolower($tabela_preco['tabela_frete'])];
		}
		else if($total_peso >= 10000)
		{
			$frete = $this->db->select(strtolower($tabela_preco['tabela_frete']))->from('tabela_frete')->where(array('kg' => 10000))->get()->row_array();
		
			$valor_frete = $total_peso * $frete[strtolower($tabela_preco['tabela_frete'])];
		}
	
		echo number_format($valor_frete, 2, ',', '.') . '|' . $valor_frete;
		
	}
	
	//
	
	function _obter_status($opcao_todos = FALSE)
	{
		$status = array('aguardando_analise_comercial' => 'Aguardando Análise Comercial', 'aguardando_faturamento' => 'Aguardando Faturamento', 'parcialmente_faturado' => 'Parcialmente Faturado', 'faturado' => 'Faturado', 'faturado_residuo_eliminado' => 'Faturado (Resíduo Eliminado)', 'reprovado_pelo_comercial' => 'Reprovado Pelo Comercial');
		
		if ($opcao_todos)
		{
			$status = array_merge(array('todos' => 'Todos'), $status);
		}
		
		return $status;
	}
	
	function _obter_clientes($codigo_usuario = NULL)
	{
		$clientes = $this->db_cliente->obter_clientes($codigo_usuario);
		$_clientes = array();
		
		foreach ($clientes as $cliente)
		{
			$_clientes[$cliente['codigo'] . '|' . $cliente['loja']] = $cliente['codigo'] . ' - ' . $cliente['nome'] . ' - ' . $cliente['cpf'];
		}
		
		return $_clientes;
	}
	
	function _obter_prospects($codigo_usuario = NULL)
	{
		$prospects = $this->db->from('prospects')->where(array('status' => 'ativo', 'codigo_usuario' => $codigo_usuario))->get()->result();
		$_prospects = array();
		
		foreach ($prospects as $prospect)
		{
			$_prospects[$prospect->id] = $prospect->nome . ' - ' . $prospect->cpf;
		}
		
		return $_prospects;
	}
	
	function _obter_representantes($opcao_todos = TRUE)
	{
		$representantes = $this->db->from('usuarios')->or_where(array('grupo' => 'representantes', 'grupo' => 'revendas'))->order_by('nome', 'asc')->get()->result();
		$_representantes = $opcao_todos ? array(0 => 'Todos') : array();
		
		foreach ($representantes as $representante)
		{
			$_representantes[$representante->codigo] = $representante->codigo . ' - ' . $representante->nome;
		}
		
		return $_representantes;
	}
	
	function _obter_formas_pagamento()
	{
		$formas_pagamento = $this->db_cliente->obter_formas_pagamento();
		$_formas_pagamento = array();
		
		foreach ($formas_pagamento as $forma_pagamento)
		{
			$_formas_pagamento[$forma_pagamento['codigo']] = $forma_pagamento['descricao'];
		}
		
		return $_formas_pagamento;
	}
	
	function _obter_transportadoras()
	{
		$transportadoras = $this->db_cliente->obter_transportadoras();
		$_transportadoras = array();
		
		foreach ($transportadoras as $transportadora)
		{
			$_transportadoras[$transportadora['codigo']] = $transportadora['nome'];
		}
		
		return $_transportadoras;
	}
	
	function _obter_grupos_produtos()
	{
		$grupos_produtos = $this->db_cliente->obter_grupos_produtos();
		$_grupos_produtos = array();
		
		foreach ($grupos_produtos as $grupo_produtos)
		{
			$_grupos_produtos[$grupo_produtos['codigo']] = $grupo_produtos['codigo'] . ' - ' . $grupo_produtos['descricao'];
		}
		
		return $_grupos_produtos;
	}
	
	function _obter_produtos_tabela_precos($codigo_grupo_produtos = NULL, $codigo_tabela_precos = NULL)
	{
	
		if($codigo_tabela_precos == NULL)
		{
			$_codigo_tabela_precos = $this->db_cliente->obter_primeiro_item_tabela_precos();
		
			$codigo_tabela_precos = $_codigo_tabela_precos['codigo'];
		}
	
		$produtos = $this->db_cliente->obter_produtos_tabela_precos($codigo_grupo_produtos, $codigo_tabela_precos);
		
		$_produtos = array();
		
		if($produtos)
		{
			foreach ($produtos as $produto)
			{
				$_produtos[$produto['codigo']] = $produto['codigo_real'] . ' - ' . $produto['descricao'] . ' - Estoque atual: ' .  number_format(($produto['estoque_atual'] > 0 ? $produto['estoque_atual'] : 0), 2, ',', '.');
			}
		}
		
		return $_produtos;
		
	}
	
	function _obter_produtos($codigo_grupo_produtos = NULL)
	{
		/*if (!$codigo_grupo_produtos)
		{
			return array();
		}*/
		
		$produtos = $this->db_cliente->obter_produtos($codigo_grupo_produtos);
		$_produtos = array();
		
		foreach ($produtos as $produto)
		{																																						
			$_produtos[$produto['codigo']] = $produto['codigo_real'] . ' - ' . $produto['descricao'] . ' - Estoque atual: ' . number_format(($produto['estoque_atual'] > 0 ? $produto['estoque_atual'] : 0), 2, ',', '.');
		}
		
		return $_produtos;
	}
	
	function _obter_opcionais_produto($codigo_produto = NULL)
	{
		$produtos = $this->db_cliente->obter_opcionais_produto($codigo_produto);
		$_produtos = array();
		
		foreach ($produtos as $produto)
		{
			$_produtos[$produto['codigo']] = $produto['codigo'] . ' - ' . $produto['descricao'];
		}
		
		return $_produtos;
	}
	
	function _obter_tabelas_precos($codigo_usuario)
	{
		$tabelas_precos = $this->db_cliente->obter_tabelas_precos();
		$_tabelas_precos = array();
		
		$tabela_precos_usuario = $this->db->from('usuarios')->where('codigo', $codigo_usuario)->get()->row()->tabela_precos;
		$_tabela_precos_usuario = unserialize($tabela_precos_usuario);
		
		if($_tabela_precos_usuario)
		{
			foreach ($tabelas_precos as $tabela_precos)
			{
				
				foreach($_tabela_precos_usuario as $codigo_tabela_precos)
				{
					if($codigo_tabela_precos['codigo'] == $tabela_precos['codigo'])
					{
						$_tabelas_precos[$tabela_precos['codigo']] = $tabela_precos['descricao'];
					}
				}

			}
		}
		
		return $_tabelas_precos;
	}
	
	function cancelar_pedido($id_pedido){
		
		//anchor('pedidos/ver_detalhes/0/' . $pedido->id, 'Cancelar')
		$this->db->delete('itens_pedidos', array('id_pedido' => $id_pedido)); 
		$this->db->delete('pedidos', array('id' => $id_pedido)); 
		redirect('pedidos');
		
	}
	
	function obter_clientes($codigo_do_representante = NULL)
	{
		$palavras_chave = addslashes($_GET['term']);
		
		$dados = array();
		
		$clientes = $this->autocomplete->obter_clientes($palavras_chave, $codigo_do_representante);
		
		foreach ($clientes as $cliente)
		{
			$dados[] = array('label' => $cliente['nome'] . ' - ' . $cliente['cpf'], 'value' => $cliente['codigo'] . '|' . $cliente['loja']);
		}
		
		
		echo json_encode($dados);
	}
	
	function obter_prospects($codigo_do_representante = NULL)
	{
		$palavras_chave = addslashes($_GET['term']);
		
		$dados = array();
		
		$prospects = $this->autocomplete->obter_prospects($palavras_chave, $codigo_do_representante);
		
		foreach ($prospects as $prospect)
		{
			$dados[] = array('label' => $prospect['nome'] . ' - ' . $prospect['cpf'], 'value' => $prospect['id']);
		}
		
		
		echo json_encode($dados);
	}
	
	
	
	
	
	function imprimir(){

		if($this->session->userdata('imprimir_pedido'))
		{
		
			$array = $this->session->userdata('imprimir_pedido');
			$empresa = $this->db->from('empresas')->where('id', 1)->get()->row_array();
			
			$conteudo = '<style type="text/css">
				#logo img{
				width:150px;
				}
			</style>';
			$conteudo .= '<style type="text/css" media="print">
			/* CSS Document */
			
			body {
			 width: 500 pt;
			 height: 500 pt;
			 overflow: none;
			 display: block;
			 font-size:10px;
			
			}
			#geral{
			overflow: visible;
			margin: 0 auto;
			margin-top:30px;
			}
			
			table{
			width:100%;
			}
			
			#logo{
			text-align:top;
			}
			
			#logo img{
			width:120px;
			}
			
			.categoria{
			font-style: italic;
			
			}
			
			table.zebra ,table.zebra td
			{
			border:1px solid black;
			border-spacing:0px;
			padding:3px;
			}
			table.zebra th
			{
			background-color:rgb(149,149,149);
			color:black;
			}
			
			.corpo{
			 padding-top:35px;
			}
			
			#cabecario tr td{
			text-align:left;
			padding-top:0px;
			}
			
			#cabecario tr .right{
			text-align: right;
			}
			
			#titulo{
			width:60%;
			font: normal bold 14px bold ;
			}
			
			#postitulo td{
			padding-top:15px;
			}
			
			
			td{
			font: normal normal normal x-small normal Lucida Sans Unicode;
			page-break-before:auto;
			
			}
			
			td.center{
				text-align:center;
			}
			
			td.right{
				text-align:right;
			}
			
			th{
			text-align:center;
			}
			
			td.linha_baixa{
				border-bottom: 2px solid #000;
			}
			tr.vai_cor{
				background-color: #EEE;
			}
			#tab_head { 
			display: table-header-group;
			}
			#tab_head td	{position: static; } 
			#tab_head tr	{position: static; } /*prevent problem if print after scrolling table*/ 
			
			</style>';
			    
			
						
						
			$conteudo .= 
					'<script type="text/javascript">
						$(document).ready(function(){
							print();
						});
					</script>';
			    
			//[codigo_cliente] => DW1000 [razao_social] => MJGF Serviços Administrativos Ltda. [nome_fantasia] => Developweb [cnpj] => 05.026.699/0001-82 [inscricao_estadual] => [cep] => 88302-001 [endereco] => Rua: Brusque [numero] => 737 [bairro] => Centro [cidade] => Itajaí [estado] => SC [telefone_1] => (47) 3348-5829 [email] => contato@developweb.com.br [observacao] => [telefone_2] => (47) 3348-5510 )
			$conteudo.='<div id="geral">
			  <div>
			    <table id="cabecario">
			      <thead id="tab_head">
			  	   	<tr><td id="logo" rowspan="3"  class="linha_baixa"><img src="'.base_url().'/third_party/img/logo.png"/></td><td colspan="2">'.$empresa['razao_social'].' - CNPJ: '.$empresa['cnpj'].'</td></tr>
			  		<tr><td colspan="3" >'.$empresa['endereco'].', '.$empresa['numero'].' - '.$empresa['bairro'].' | '.$empresa['cidade'].' - '.$empresa['estado'].' | CEP:'.$empresa['cep'].'</td></tr>
			  		<tr><td colspan="3" class="linha_baixa" > Telefone: '.$empresa['telefone_1'].' | email: '.$empresa['email'].'</td></tr> 
			  		
			  		</thead>
			  		
			  		<tbody>
			        <tr><td id="titulo" colspan="3"><br />Espelho do '.$array['tipo'].'<br /><br /></td><td class="right"><b>Emissão: </b>'.$array['emissao'].'<br /></td></tr>
			  		<tr id="postitulo"><td><b>Status:</b>'.$array['status'].' </td><td><b>Código: </b>'.$array['codigo'].'</td><td><b>Preço Médio/Kg: </b>'.$array['preco_medio'].'</td></tr>
			  		 <tr><td><b>O.C.: </b>'.$array['ordem_compra'].'</td><td><b>Tipo de Frete: </b>'.$array['tipo_frete'].'</td><td><b>Forma Pagamento: </b>'.$array['forma_pagamento'].'</td><td><b>Peso total: </b>' . $array['pedido_peso_total'].' Kg</td></tr>
			      <tbody>
			    </table>
			  </div>';

			if($array['cliente_codigo'] && $array['cliente_loja']){
			$conteudo.= '
			  <div class="corpo">
			  <strong class="categoria">Cliente</strong>
				<table>  
			     <tr><td><b>Nome: </b>'.$array['cliente_codigo'].' - '.$array['cliente_nome'].'</td><td><b>Loja: </b>'.$array['cliente_loja'].'</td><td><b>CPF/CNPJ: </b>'.$array['cliente_cpf'].'</td></tr>
			     <tr><td><b>Endereço: </b>'.$array['cliente_endereco'].' - '.$array['cliente_bairro'].'</td><td><b>Cidade: </b>'.$array['cliente_cidade'].' - '.$array['cliente_estado'].'</td><td><b>CEP: </b>'.$array['cliente_cep'].'</td></tr>
			     <tr><td><b>Telefone: </b>'.$array['cliente_telefone'].'</td><td colspan="2"><b>e-mail: </b>'.$array['cliente_email'].'</td></tr>
			    </table>
			  </div>';
			}else{
			$conteudo.= '
			  <div class="corpo">
			  <strong class="categoria">Prospect</strong>
				<table>  
			     <tr><td><b>Nome: </b>'.$array['prospect_nome'].'</td><td><b>CPF/CNPJ: </b>'.$array['prospect_cpf'].'</td></tr>
			     <tr><td><b>Endereço: </b>'.$array['prospect_endereco'].' - '.$array['prospect_numero'].'</td><td><b>Cidade: </b>'.$array['prospect_cidade'].' - '.$array['prospect_estado'].'</td><td><b>CEP: </b>'.$array['prospect_cep'].'</td></tr>
			     <tr><td><b>Telefone: </b>'.$array['prospect_telefone_contato_1'].'</td><td colspan="2"><b>e-mail: </b>'.$array['prospect_email_contato_1'].'</td></tr>
			    </table>
			  </div>';
			}
			
			
			#  
			##  #informações sobre o representante
			#
			//print_r($array);
			  $conteudo .='<div class="corpo">
			  <strong class="categoria">Representante</strong>
			    <table>  
			     <tr><td><b>Representante: </b>'.$array['representante_codigo'].' - '.$array['representante_nome'].'</td><td><b>CPF/CNPJ: </b>'.$array['representante_cpf'].'</td></tr>
			     <tr><td><b>Telefone: </b>'.$array['representante_telefone'].'</td><td colspan="2"><b>e-mail: </b>'.$array['representante_email'].'</td></tr>
			    </table>
				</div>';
				
		 $conteudo.= '<div class="corpo">
			   <strong >Itens do Pedido</strong>
				  <table class="zebra">
			      <thead>
			        <tr><th>Item</th><th>Produto</th><th>Descrição</th><th>Preço (R$)</th><th>Quant. Vend.</th><th>Total (R$)</th></tr>
			      </thead>
			      <tbody>';
				$pedido['itens'] = $array['pedido_itens'];
				$i = 1;
			      foreach ($pedido['itens'] as $item_pedido)
					{
						
						if ($i%2 ==1){
							$style = 'style="background-color:#DDD"';	
						}else{
							$style = null;	
						}
						
						$conteudo .= '<tr '.$style.' ><td class="center">' . ($item_pedido['codigo_item']?$item_pedido['codigo_item']:$item_pedido['item']). '</td><td class="right">' . $item_pedido['codigo_produto'] . '</td><td>' . $item_pedido['descricao_produto'] . '</td><td class="right">' . number_format($item_pedido['preco_produto'], 2, ',', '.') . '</td><td class="right">' . number_format($item_pedido['quantidade_vendida_produto'], 0, NULL, '.') . '</td><td class="right">'.number_format(($item_pedido['preco_produto'] * $item_pedido['quantidade_vendida_produto']), 2, ',', '.').'</td></tr>';
						$i++;
					}
			    
			     $conteudo .='</tbody>
			    </table>
				</div>';
			     
			     $conteudo .='<div class="corpo">
						  <strong class="categoria">Totais</strong>
						  <table>
						    <tr><td><b>Quant. vend.:</b>' .$array['pedido_quantidade_vendida']. '</td><td><b>Quant. fat.: </b>'.$array['pedido_quantidade_faturada'].'</td><td><b>Peso total: </b>' .$array['pedido_peso_total']. ' Kg</td></tr>
						    <tr><td><b>Valor do frete/Kg:</b> R$ ' .$array['pedido_valor_frete_kg']. '</td><td><b>Valor do frete: </b>R$ '.$array['pedido_valor_frete'].'</td>
						    
						    <tr><td><b>Valor parcial:</b> R$ ' .$array['pedido_valor_parcial']. '</td><td><b>Valor total: </b>R$ '.$array['pedido_valor_total'].'</td><td><b>Valor total + frete: </b>R$ ' .$array['pedido_total_mais_frete']. '</td></tr>
						    
						    <tr><td colspan="2"><b>Obs.:</b>'.$array['observacao'].'</td></tr>

	
						  </table>
						  </div>
						  </div>
						  
						';
				}
    //echo $conteudo;
    $this->load->view('layout', array('conteudo' => $conteudo));
  }
  
	function obter_tabela_preco($codigo, $loja)
	{
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->config->load('db_cliente_' . $this->db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');
		
		$campos = array('tabela_frete');
		
		foreach ($campos as $campo)
		{
			$select[] = $this->_db_cliente['campos']['clientes'][$campo] . ' AS ' . $campo;
		}
		
		if ($this->_db_cliente['campos']['clientes']['delecao'])
		{
			$this->db_cliente->where($this->_db_cliente['campos']['clientes']['delecao'], '');
		}
		
		return $this->db_cliente->select(implode(', ', $select))->from($this->_db_cliente['tabelas']['clientes'])->where(array($this->_db_cliente['campos']['clientes']['codigo'] => $codigo, $this->_db_cliente['campos']['clientes']['loja'] => $loja))->get()->row_array();
		
	}
  
  
	
}




	