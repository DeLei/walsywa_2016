<?php

class Prospects extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	
	function index()
	{
		$filtros = array(
			array(
				'nome' => 'status',
				'descricao' => 'Status',
				'tipo' => 'opcoes',
				'opcoes' => array('todos' => 'Todos', 'ativo' => 'Ativo', 'inativo' => 'Inativo'),
				'campo_mysql' => 'status',
				'ordenar' => 1
			),
			array(
				'nome' => 'criacao',
				'descricao' => 'Criação',
				'tipo' => 'data',
				'campo_mysql' => 'timestamp',
				'ordenar' => 0
			),
			array(
				'nome' => 'tipo_pessoa',
				'descricao' => 'Tipo de pessoa',
				'tipo' => 'opcoes',
				'opcoes' => array('todos' => 'Todos', 'pessoa_fisica' => 'Pessoa Física', 'pessoa_juridica' => 'Pessoa Jurídica'),
				'campo_mysql' => 'tipo_pessoa',
				'ordenar' => 3
			),
			array(
				'nome' => 'nome',
				'descricao' => 'Nome',
				'tipo' => 'texto',
				'campo_mysql' => 'nome',
				'ordenar' => 4
			),
			array(
				'nome' => 'nome_fantasia',
				'descricao' => 'Fantasia',
				'tipo' => 'texto',
				'campo_mysql' => 'nome_fantasia',
				'ordenar' => 5
			),
			array(
				'nome' => 'cidade',
				'descricao' => 'Cidade',
				'tipo' => 'texto',
				'campo_mysql' => 'cidade',
				'ordenar' => 6
			),
			array(
				'nome' => 'telefone_contato_1',
				'descricao' => 'Telefone 1',
				'tipo' => 'texto',
				'atributos' => 'size="20" alt="phone"',
				'campo_mysql' => 'telefone_contato_1',
				'ordenar' => 7
			),
			array(
				'nome' => 'telefone_contato_2',
				'descricao' => 'Telefone 2',
				'tipo' => 'texto',
				'atributos' => 'size="20" alt="phone"',
				'campo_mysql' => 'telefone_contato_2',
				'ordenar' => 8
			),
		);
		
		if($_POST || $_GET)
		{
		
			// caso o usuário logado for um representante, vamos mostrar somente os prospects que ele mesmo fez
			if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'engenharia')))
			{
				$this->db->where('codigo_usuario', $this->session->userdata('codigo_usuario'));
			}
			
			$this->filtragem_mysql($filtros);
			
			$prospects = $this->db->from('prospects')->where('prospects.status !=', 'convertido_cliente')->limit(20, $this->input->get('per_page'))->get()->result();
			
			foreach ($prospects as $prospect)
			{
				$representante = $this->db_cliente->obter_representante($prospect->codigo_usuario);
				
				$prospect->nome_do_representante = $representante['nome'];
			}
			
			//
			
			// caso o usuário logado for um representante, vamos mostrar somente os prospects que ele mesmo fez
			if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros','engenharia')))
			{
				$this->db->where('codigo_usuario', $this->session->userdata('codigo_usuario'));
			}
			
			$this->filtragem_mysql($filtros);
			
			$total = $this->db->from('prospects')->where('prospects.status !=', 'convertido_cliente')->get()->num_rows();
			
			$paginacao = $this->paginacao($total);
		
		}
		
		$this->load->view('layout', array('conteudo' => $this->load->view('prospects/index', array('prospects' => $prospects, 'statuses' => $this->_obter_status(), 'tipos_de_pessoa' => $this->_obter_tipos_pessoa(), 'filtragem' => $this->filtragem($filtros), 'paginacao' => $paginacao, 'total' => $total), TRUE)));
	}
	
	function ver_detalhes($id = NULL)
	{
		if (!$id)
		{
			redirect('prospects');	
		}
		
		$prospect = $this->db->from('prospects')->where('id', $id)->get()->row();
		
		if (!$prospect)
		{
			redirect('prospects');	
		}
		
		$conteudo = '
			<div class="caixa">
				<ul>
					<li>' . anchor('prospects', 'Voltar', 'onclick="history.go(-1); return false;"') . '</li>';
		if(!$this->session->userdata('grupo_usuario') == 'engenharia')
		{
			$conteudo .= '					
					<li>' . anchor('criar_pedidos/informacoes_cliente/orcamento/' . $prospect->codigo_usuario . '/0/0/0/' . $prospect->id, 'Cadastrar Orçamento') . '</li>
					<li>' . anchor('prospects/editar/' . $prospect->tipo_pessoa . '/' . $prospect->id, 'Editar Prospect') . '</li>
					<li>' . anchor('prospects/converter_cliente/' . $prospect->id, 'Converter Em Cliente', 'onclick="if (!confirm(\'Deseja continuar?\')) return false;"') . '</li>';
		}			
			$conteudo .= '		
					<li>' . anchor('compromissos/ver_agendamentos/prospects/' . $prospect->id , 'Ver Todos os Agendamentos') . '</li>
				</ul>
			</div>
		';
		
		
		//----------------
		
		$conteudo .= heading('Detalhes do Prospect', 2);
		
		//Cor do Status
		if($prospect->status == 'ativo')
		{
			$cor_status = 'green';
		}
		else
		{
			$cor_status = 'red';
		}
		
		$conteudo .= '<div style="margin-top:15px"><strong style="font-size:15px">' . $prospect->nome . '</strong> (' . $prospect->cpf  . ') <span style="color:' . $cor_status . '">(' . strtoupper(element($prospect->status, $this->_obter_status())) . ')</span></div>';
		$conteudo .= '<hr />';
		
		//$conteudo .= '<table><tr><td valign="top">';
		
		if ($prospect->tipo_pessoa == 'pessoa_fisica')
		{
			
			$conteudo .= '
				<table cellspacing="0" class="info" style="font-size: 11px">
					<tr>
						<th>Nome:</th>
						<td>' . $prospect->nome . '</td>

						<th>RG:</th>
						<td colspan="3">' . $prospect->rg . '</td>
					</tr>

					<tr>
						<th>Endereço:</th>
						<td>' . $prospect->endereco . ', ' . $prospect->numero  . ($prospect->complemento ? ' - Complemento: ' . $prospect->complemento : "") . '</td>
						
						<th>Bairro:</th>
						<td>' . $prospect->bairro . '</td>

						<th>CEP:</th>
						<td>' . $prospect->cep . '</td>
					</tr>
					
					<tr>
						<th>Cidade:</strong></th>
						<td>' . $prospect->cidade . '</td>
		
						<th>Estado:</strong></th>
						<td colspan="3">' . element($prospect->estado, $this->_obter_estados()) . '</td>
					</tr>
					
					<tr>
						<th>Fone:</strong></th>
						<td>' . $prospect->telefone_contato_1 . '</td>
		
						<th>E-mail:</strong></th>
						<td colspan="3">' . mailto($prospect->email_contato_1) . '</td>
					</tr>
					
				';
			
		}
		else
		{
		
			$conteudo .= '
				<table cellspacing="0" class="info" style="font-size: 11px">
					<tr>
						<th>Nome fantasia:</th>
						<td colspan="5">' . $prospect->nome_fantasia . '</td>
					</tr>

					<tr>
						<th>Inscrição estadual:</th>
						<td>' . $prospect->rg . '</td>

						<th>Inscrição municipal:</th>
						<td colspan="3">' . $prospect->inscricao_municipal . '</td>
					</tr>

				
					
					<tr>
						<th>E-mail NFE:</th>
						<td colspan="5">' . auto_link($prospect->email_nfe, 'both', TRUE) . '</td>
					</tr>
					
					<tr>
						<th>Site:</th>
						<td colspan="5">' . auto_link($prospect->site, 'both', TRUE) . '</td>
					</tr>
					
				';
		}


		
		$conteudo .= '<tr><td colspan="6">
		<script type="text/javascript">
			$(document).ready(function(){
				$("#mapa").goMap({
					address: "' . $prospect->endereco. ', ' . $prospect->cidade . ' - ' . $prospect->estado . '",
					zoom: 15, 
					maptype: "ROADMAP"
				});
				
				$.goMap.createMarker({  
						address: "' . $prospect->endereco. ', ' . $prospect->cidade . ' - ' . $prospect->estado . '"
				}); 
				
				$(".visualizar_mapa").click(function(){
					var endereco = $(this).parents("tr").children(".endereco_mapa").html();
					var cidade = $(this).parents("tr").children(".cidade_mapa").html();
					var estado = $(this).parents("tr").children(".estado_mapa").html();
					
					//alert(endereco + ", " + cidade + " - " + estado);
					
					$("#mapa").goMap({
						address: endereco + ", " + cidade + " - " + estado,
						zoom: 15, 
						maptype: "ROADMAP"
					});
					
					$.goMap.createMarker({  
						address: endereco + ", " + cidade + " - " + estado,
					});
				});
			});
		</script>
		<div id="mapa" style="height:150px; width:400px; float:left; border:1px solid #CCCCCC; margin-top: 10px"></div></td></tr></table>';
		
		$conteudo .= '<div style="clear:left"></div>';
		
		if ($prospect->tipo_pessoa != 'pessoa_fisica')
		{
			if($prospect->cep|| $prospect->cep_cobranca)
			{
				$conteudo .= heading('Endereços', 3);
				
				$conteudo .= '<table cellspacing="0" class="info" style="font-size: 11px">';

				if($prospect->cep_cobranca)
				{
					
					$conteudo .= '<tr>
					
									<th>Endereço de Cobrança:</th>
									<td class="endereco_mapa">' . $prospect->endereco_cobranca . ', ' . $prospect->numero_cobranca . '</td>
									
									<th>Complemento:</th>
									<td>' . $prospect->complemento_cobranca . '</td>
									
									<th>Bairro:</th>
									<td>' . $prospect->bairro_cobranca . '</td>

									<th>CEP:</th>
									<td>' . $prospect->cep_cobranca . '</td>
									
									<th>Cidade:</th>
									<td class="cidade_mapa">' . $prospect->cidade_cobranca . '</td>
					
									<th>Estado:</th>
									<td class="estado_mapa">' . element($prospect->estado_cobranca, $this->_obter_estados()) . '</td>
									
									<th>Visualizar Mapa:</th>
									<td>' . form_radio('visualizar_mapa', '1', FALSE, 'class="visualizar_mapa"') . '</td>
									
								 </tr>';
					
				}
				
				if($prospect->cep)
				{
					$conteudo .= '<tr>
					
									<th>Endereço de Entrega:</th>
									<td class="endereco_mapa">' . $prospect->endereco. ', ' . $prospect->numero . '</td>
									
									<th>Complemento:</th>
									<td>' . $prospect->complemento . '</td>

									<th>Bairro:</th>
									<td>' . $prospect->bairro . '</td>

									<th>CEP:</th>
									<td>' . $prospect->cep . '</td>
									
									<th>Cidade:</th>
									<td class="cidade_mapa">' . $prospect->cidade . '</td>
					
									<th>Estado:</th>
									<td class="estado_mapa">' . element($prospect->estado, $this->_obter_estados()) . '</td>
									
									<th>Visualizar Mapa:</th>
									<td>' . form_radio('visualizar_mapa', '2', TRUE, 'class="visualizar_mapa"') . '</td>
									
								 </tr>';
				}
				
				$conteudo .= '</table>';
				
			}
		}
		
		$conteudo .= '<div style="clear:left"></div>';
		

		if($prospect->nome_contato_1 || $prospect->nome_contato_2 || $prospect->nome_contato_3)
		{
		
			$conteudo .= heading('Contatos', 3);
			
			$conteudo .= '<table cellspacing="0" class="info" style="font-size: 11px">';
			
			if($prospect->nome_contato_1)
			{
				$conteudo .= '<tr>
								<th>Nome:</th>
								<td>' . $prospect->nome_contato_1 . '</td>

								<th>Telefone:</th>
								<td>' . $prospect->telefone_contato_1 . '</td>

								<th>Cargo:</th>
								<td>' . $prospect->cargo_contato_1 . '</td>

								<th>E-mail:</th>
								<td>' . mailto($prospect->email_contato_1) . '</td>
							</tr>';
			}
			
			if($prospect->nome_contato_2)
			{
				$conteudo .= '<tr>
								<th>Nome:</th>
								<td>' . $prospect->nome_contato_2 . '</td>

								<th>Telefone:</th>
								<td>' . $prospect->telefone_contato_2 . '</td>

								<th>Cargo:</th>
								<td>' . $prospect->cargo_contato_2 . '</td>

								<th>E-mail:</th>
								<td>' . mailto($prospect->email_contato_2) . '</td>
							</tr>';
			}
			
			if($prospect->nome_contato_3)
			{
				$conteudo .= '<tr>
								<th>Nome:</th>
								<td>' . $prospect->nome_contato_3 . '</td>

								<th>Telefone:</th>
								<td>' . $prospect->telefone_contato_3 . '</td>

								<th>Cargo:</th>
								<td>' . $prospect->cargo_contato_3 . '</td>

								<th>E-mail:</th>
								<td>' . mailto($prospect->email_contato_3) . '</td>
							</tr>';
			}
						
			$conteudo .= '</table>';
		
		}
		
		$conteudo .= '<div style="clear:left"></div>';
		
		if($prospect->ref_razao_social_1 || $prospect->ref_razao_social_2 || $prospect->ref_razao_social_3)
		{
		
			$conteudo .= heading('Referências Comerciais', 3);
			
			$conteudo .= '<table cellspacing="0" class="info" style="font-size: 11px">';
		
			if($prospect->ref_razao_social_1)
			{
				
				$conteudo .= '<tr>
								<th>Razão Social:</th>
								<td>' . $prospect->ref_razao_social_1 . '</td>

								<th>Cidade:</th>
								<td>' . $prospect->ref_cidade_1 . '</td>

								<th>Estado:</th>
								<td>' . $prospect->ref_estado_1 . '</td>

								<th>Telefone:</th>
								<td>' . $prospect->ref_telefone_1 . '</td>
							</tr>';
				
			}
			
			if($prospect->ref_razao_social_2)
			{
				
				$conteudo .= '<tr>
								<th>Razão Social:</th>
								<td>' . $prospect->ref_razao_social_2 . '</td>

								<th>Cidade:</th>
								<td>' . $prospect->ref_cidade_2 . '</td>

								<th>Cidade:</th>
								<td>' . $prospect->ref_estado_2 . '</td>

								<th>Telefone:</th>
								<td>' . $prospect->ref_telefone_2 . '</td>
							</tr>';
				
			}
			
			if($prospect->ref_razao_social_3)
			{
				
				$conteudo .= '<tr>
								<th>Razão Social:</th>
								<td>' . $prospect->ref_razao_social_3 . '</td>

								<th>Cidade:</th>
								<td>' . $prospect->ref_cidade_3 . '</td>

								<th>Cidade:</th>
								<td>' . $prospect->ref_estado_3 . '</td>

								<th>Telefone:</th>
								<td>' . $prospect->ref_telefone_3 . '</td>
							</tr>';
				
			}
			
			$conteudo .= '</table>';
			
		}
		
		$conteudo .= '<div style="clear:left"></div>';
		
		if ($prospect->banco_1 || $prospect->banco_2)
		{
			$conteudo .= heading('Referências Bancárias', 3);
			
			$conteudo .= '
				<table cellspacing="0" class="info" style="font-size: 11px">
					<tr>
						<th>Banco 1:</th>
						<td>' . $prospect->banco_1 . '</td>

						<th>Agência 1:</th>
						<td>' . $prospect->agencia_1 . '</td>

						<th>Conta 1:</th>
						<td>' . $prospect->conta_1 . '</td>
					</tr>
					
					<tr>
						<th>Banco 2:</th>
						<td>' . $prospect->banco_2 . '</td>
	
						<th>Agência 2:</th>
						<td>' . $prospect->agencia_2 . '</td>

						<th>Conta 2:</th>
						<td>' . $prospect->conta_2 . '</td>
					</tr>
				</table>
				
				<div style="clear: both;"></div>
			';
		}
				
		
		$conteudo .= '<div style="clear:both; margin:10px"></div>';

		
		//----------------
		
		/*
		$conteudo .= heading('Detalhes do Prospect', 2);
		
		$conteudo .= heading('Informações Gerais', 3);
		
		
		if ($prospect->tipo_pessoa == 'pessoa_fisica')
		{
			$conteudo .= '
				<table cellspacing="0" class="info">
					<tr>
						<th>Nome:</th>
						<td>' . $prospect->nome . '</td>
					</tr>
					<tr>
						<th>CPF:</th>
						<td>' . $prospect->cpf . '</td>
					</tr>
				</table>
				
				<table cellspacing="0" class="info">
					<tr>
						<th>Status:</th>
						<td>' . element($prospect->status, $this->_obter_status()) . '</td>
					</tr>
					<tr>
						<th>RG:</th>
						<td>' . $prospect->rg . '</td>
					</tr>
				</table>
				
				<table cellspacing="0" class="info">
					<tr>
						<th>Site:</th>
						<td>' . auto_link($prospect->site, 'both', TRUE) . '</td>
					</tr>
				</table>
				
				<div style="clear: both;"></div>
			';
		}
		else
		{
			$conteudo .= '
				<table cellspacing="0" class="info">
					<tr>
						<th>Razão social:</th>
						<td>' . $prospect->nome . '</td>
					</tr>
					<tr>
						<th>CNPJ:</th>
						<td>' . $prospect->cpf . '</td>
					</tr>
				</table>
				
				<table cellspacing="0" class="info">
					<tr>
						<th>Status:</th>
						<td>' . element($prospect->status, $this->_obter_status()) . '</td>
					</tr>
					<tr>
						<th>Nome fantasia:</th>
						<td>' . $prospect->nome_fantasia . '</td>
					</tr>
				</table>
				
				<table cellspacing="0" class="info">
					<tr>
						<th>Inscrição estadual:</th>
						<td>' . $prospect->rg . '</td>
					</tr>
					<tr>
						<th>Site:</th>
						<td>' . auto_link($prospect->site, 'both', TRUE) . '</td>
					</tr>
				</table>
				
				<div style="clear: both;"></div>
			';
		}
		
		$conteudo .= heading('Endereço', 3);
		
		$conteudo .= '
			<table cellspacing="0" class="info">
				<tr>
					<th>Endereço:</th>
					<td>' . $prospect->endereco . '</td>
				</tr>
				<tr>
					<th>CEP:</th>
					<td>' . $prospect->cep . '</td>
				</tr>
			</table>
			
			<table cellspacing="0" class="info">
				<tr>
					<th>Nº:</th>
					<td>' . $prospect->numero . '</td>
				</tr>
				<tr>
					<th>Cidade:</strong></th>
					<td>' . $prospect->cidade . '</td>
				</tr>
			</table>
			
			<table cellspacing="0" class="info">
				<tr>
					<th>Bairro:</th>
					<td>' . $prospect->bairro . '</td>
				</tr>
				<tr>
					<th>Estado:</strong></th>
					<td>' . element($prospect->estado, $this->_obter_estados()) . '</td>
				</tr>
			</table>
			
			<div style="clear: both;"></div>
		';
		
		if ($prospect->tipo_pessoa != 'pessoa_fisica')
		{
			$conteudo .= heading('Contato 1', 3);
			
			$conteudo .= '
				<table cellspacing="0" class="info">
					<tr>
						<th>Nome:</th>
						<td>' . $prospect->nome_contato_1 . '</td>
					</tr>
					<tr>
						<th>Telefone:</th>
						<td>' . $prospect->telefone_contato_1 . '</td>
					</tr>
				</table>
				
				<table cellspacing="0" class="info">
					<tr>
						<th>Cargo:</th>
						<td>' . $prospect->cargo_contato_1 . '</td>
					</tr>
					<tr>
						<th>E-mail:</th>
						<td>' . mailto($prospect->email_contato_1) . '</td>
					</tr>
				</table>
				
				<div style="clear: both;"></div>
			';
		}
		else
		{
			$conteudo .= heading('Contato', 3);
			
			$conteudo .= '
				<table cellspacing="0" class="info">
					<tr>
						<th>Telefone 1:</th>
						<td>' . $prospect->telefone_contato_1 . '</td>
					</tr>
					<tr>
						<th>Telefone 2:</th>
						<td>' . $prospect->telefone_contato_2 . '</td>
					</tr>
					<tr>
						<th>E-mail:</th>
						<td>' . mailto($prospect->email_contato_1) . '</td>
					</tr>
				</table>
				
				<div style="clear: both;"></div>
			';
		}
		
		if ($prospect->tipo_pessoa != 'pessoa_fisica')
		{
			if ($prospect->nome_contato_2 || $prospect->cargo_contato_2 || $prospect->telefone_contato_2 || $prospect->email_contato_2) {
				$conteudo .= heading('Contato 2', 3);
				
				$conteudo .= '
					<table cellspacing="0" class="info">
						<tr>
							<th>Nome:</th>
							<td>' . $prospect->nome_contato_2 . '</td>
						</tr>
						<tr>
							<th>Telefone:</th>
							<td>' . $prospect->telefone_contato_2 . '</td>
						</tr>
					</table>
					
					<table cellspacing="0" class="info">
						<tr>
							<th>Cargo:</th>
							<td>' . $prospect->cargo_contato_2 . '</td>
						</tr>
						<tr>
							<th>E-mail:</th>
							<td>' . mailto($prospect->email_contato_2) . '</td>
						</tr>
					</table>
					
					<div style="clear: both;"></div>
				';
			}
			
			if ($prospect->nome_contato_3 || $prospect->cargo_contato_3 || $prospect->telefone_contato_3 || $prospect->email_contato_3) {
				$conteudo .= heading('Contato 3', 3);
				
				$conteudo .= '
					<table cellspacing="0" class="info">
						<tr>
							<th>Nome:</th>
							<td>' . $prospect->nome_contato_3 . '</td>
						</tr>
						<tr>
							<th>Telefone:</th>
							<td>' . $prospect->telefone_contato_3 . '</td>
						</tr>
					</table>
					
					<table cellspacing="0" class="info">
						<tr>
							<th>Cargo:</th>
							<td>' . $prospect->cargo_contato_3 . '</td>
						</tr>
						<tr>
							<th>E-mail:</th>
							<td>' . mailto($prospect->email_contato_3) . '</td>
						</tr>
					</table>
					
					<div style="clear: both;"></div>
				';
			}
			
			if ($prospect->banco_1 || $prospect->banco_2)
			{
				$conteudo .= heading('Referências Bancárias', 3);
				
				$conteudo .= '
					<table cellspacing="0" class="info">
						<tr>
							<th>Banco 1:</th>
							<td>' . $prospect->banco_1 . '</td>
						</tr>
						<tr>
							<th>Banco 2:</th>
							<td>' . $prospect->banco_2 . '</td>
						</tr>
					</table>
					
					<table cellspacing="0" class="info">
						<tr>
							<th>Agência 1:</th>
							<td>' . $prospect->agencia_1 . '</td>
						</tr>
						<tr>
							<th>Agência 2:</th>
							<td>' . $prospect->agencia_2 . '</td>
						</tr>
					</table>
					
					<table cellspacing="0" class="info">
						<tr>
							<th>Conta 1:</th>
							<td>' . $prospect->conta_1 . '</td>
						</tr>
						<tr>
							<th>Conta 2:</th>
							<td>' . $prospect->conta_2 . '</td>
						</tr>
					</table>
					
					<div style="clear: both;"></div>
				';
			}
		}
		*/
		
		if ($prospect->observacao)
		{
			$conteudo .= heading('Observação', 3);
			
			$conteudo .= '
				<table cellspacing="0" class="info" style="font-size: 11px">
					<tr>
						<td style="border-left: 1px solid #DDD;">' . $prospect->observacao . '</td>
					</tr>
				</table>
				
				<div style="clear: both;"></div>
			';
		}
		
		//$conteudo .= '</td><td valign="top">';

		//$conteudo .= '</td></table>';
		
		
		// ** HISTÓRICOS
		////////////////////////////////////////////
		$filtros = array(
			array(
				'nome' => 'contato',
				'descricao' => 'Contato',
				'tipo' => 'texto',
				'campo_mysql' => 'pessoa_contato',
				'ordenar' => 0
			),
			array(
				'nome' => 'protocolo',
				'descricao' => 'Protocolo',
				'tipo' => 'texto',
				'campo_mysql' => 'protocolo',
				'ordenar' => 1
			),
			array(
				'nome' => 'criacao',
				'descricao' => 'Data',
				'tipo' => 'data',
				'campo_mysql' => 'timestamp',
				'ordenar' => 2
			)
		);
		
		////////////////////////////////////////////
		
		$this->filtragem_mysql($filtros);
		$historicos = $this->db->from('historicos_prospects')->where(array('cpf' => $prospect->cpf))->order_by('id', 'desc')->get()->result();
		
		
		// RETORNA PAGINACAO PRONTA
		$paginacao = $this->paginacao_rapida();
		$contador = $this->paginacao_contador_desc($this->get_total_registro_paginacao(), $limit_consulta);
		
		$conteudo .= heading('Históricos', 3);
		
		if($prospect->status == 'ativo')
		{
			$conteudo .= '<p>' . anchor('#', 'Cadastrar Novo Histórico', 'onclick="$(this).parents(\'p\').hide().next().show().prev().prev().hide(); return false;"') . '</p>';
		}
		else
		{
			$conteudo .= '<p style="font-size: 10px; color: red; font-style:italic">Não é possível cadastrar histórico ou agendar compromisso quando o prospect está inativo.</p>';
		}
		
		
		
		//Obter Histórico
		$this->db->order_by("timestamp", "desc"); 
		$historico_prospects = $this->db->from('historicos_prospects')->where('id_prospect', $prospect->id)->get()->row();
		
		//print_r($historico_prospects);
		
		if($historico_prospects)
		{
			$contato_historico = $historico_prospects->pessoa_contato;
			$cargo_historico = $historico_prospects->cargo;
			$email_histrico = $historico_prospects->email;
		}
		else
		{
			$contato_historico = $prospect->nome_contato_1;
			$cargo_historico = $prospect->cargo_contato_1;
			$email_histrico = $prospect->email_contato_1;
		}
		
		$conteudo .= form_open(site_url('prospects/criar_historico_ajax/' . $prospect->id), 'style="display: none;"');
		$conteudo .= '
			<div style="float: left;">
				<h3>Cadastrar Novo Histórico</h3>
				<p style="margin-right: 10px;">' . form_label('Pessoa de contato:' . br() . form_input('pessoa_contato', $contato_historico ? $contato_historico : '', 'size="35"')) . '</p>
				<p style="margin-right: 10px;">' . form_label('Cargo:' . br() . form_input('cargo', $cargo_historico ? $cargo_historico : '', 'size="35"')) . '</p>
				<p style="margin-right: 10px;">' . form_label('E-mail:' . br() . form_input('email', $email_histrico ? $email_histrico : '', 'size="35"')) . '</p>
				<div style="clear: both;"></div>
				<p>' . form_label('Descrição do histórico:' . br() . form_textarea(array('cols' => 40, 'rows' => 8, 'name' => 'descricao'))) . '</p>
				<p>' . form_label(form_checkbox('criar_compromisso', 'sim') . ' Agendar compromisso') . '</p>
				<p>' . form_submit('concluir', 'Concluir') . ' ou ' . anchor('#', 'Cancelar', 'onclick="$(this).parents(\'form\').hide().prev().show().prev().show(); return false;"') . '</p>
			</div>
			
			<div id="agendar_compromisso" style="border-left: 1px solid #DDD; float: left;  margin-left: 20px; padding-left: 20px; display: none;">
				<h3>Agendar Compromisso</h3>
				
				<p style="float: left; margin-right: 10px;">' . form_label('Data:' . br() . form_input('data_inicial_compromisso', NULL, 'class="datepicker" size="6"')) . '</p>
				<p style="float: left; margin-right: 10px;">' . form_label('Hora:' . br() . form_input('horario_inicial_compromisso', NULL, 'alt="time" size="1"')) . '</p>
				<div style="clear: both;"></div>
				<p>' . form_label('Descrição do compromisso:' . br() . form_textarea(array('cols' => 60, 'rows' => in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) ? 5 : 8, 'name' => 'descricao_compromisso'))) . '</p>
			</div>
			
			<div style="clear: both;"></div>
		';
		
		$conteudo .= form_close();
		$conteudo .= '
			<script type="text/javascript">
				$(document).ready(function() {
					$("input[name=criar_compromisso]").click(function() {
						$("#agendar_compromisso").toggle();
					});
					
					$("input[name=concluir]").click(function() {
						var _this = this;
						
						$(this).attr("disabled", "disabled").val("Carregando...");
						
						$.post($(this).parents("form").attr("action"), $(this).parents("form").serialize(), function(dados) {
							if (dados.erro)
							{
								alert(dados.erro);
							}
							else
							{
								window.location = window.location;
							}
							
							$(_this).removeAttr("disabled").val("Concluir");
						}, "json");
					});
				});
			</script>
		';
		////////////////////////////////////////////
		$conteudo .= '<h3>Históricos</h3>';
		$conteudo .= '<div class="topo_grid">';
		$conteudo .= '<p><strong>Filtros</strong></p>';
		
		$conteudo .= form_open(current_url(), array('method' => 'get'));
		
		$conteudo .= $this->filtragem($filtros);
		
		$conteudo .= '<div style="clear: both;"></div>';
	
		$conteudo .= form_close();
		
		$conteudo .= '</div>';
		////////////////////////////////////////////
		$conteudo .= '<table cellspacing="0" class="novo_grid"><thead><tr><th>Data</th><th>Contato</th><th>Cargo</th><th>E-mail</th><th>Protocolo</th><th>Opções</th></tr></thead><tbody>';
		foreach ($historicos as $historico)
		{
			$conteudo .= '<tr><td>' . date('d/m/Y H:i', $historico->timestamp) . '</td><td>' . $historico->pessoa_contato . '</td><td>' . $historico->cargo . '</td><td>' . mailto($historico->email) . '</td><td>' . $historico->protocolo . '</td><td><a class="colorbox_inline" href="#historico_' . $historico->id . '">Ver Detalhes</a><div style="display: none;"><div id="historico_' . $historico->id . '" style="max-width: 740px;"><table cellspacing="5"><tr><td><strong>Prospect:</strong></td><td>' . $prospect->nome . '</td></tr><tr><td><strong>Contato:</strong></td><td>' . $historico->pessoa_contato . '</td></tr><tr><td><strong>Cargo:</strong></td><td>' . $historico->cargo . '</td></tr><tr><td><strong>E-mail:</strong></td><td>' . mailto($historico->email) . '</td></tr></table><p>' . nl2br($historico->descricao) . '</p></div></div></td></tr>';
		}
		$conteudo .= '</tbody></table>';
		// HISTÓRICOS **
		$conteudo .= $paginacao;
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function criar_historico_ajax($id_prospect = NULL)
	{
		if (!$id_prospect)
		{
			redirect();	
		}
		
		// TODO: representantes/prepostos só podem criar históricos para SEUS prospects
		
		$prospect = $this->db->from('prospects')->where('id', $id_prospect)->get()->row();
		
		if (!$prospect)
		{
			redirect();	
		}
		
		$dados = array(
			'erro' => NULL
		);
		
		if (!$this->input->post('pessoa_contato'))
		{
			$dados['erro'] = 'Digite uma pessoa de contato.';
		}
		else if ($this->input->post('email') && !valid_email($this->input->post('email')))
		{
			$dados['erro'] = 'Digite um e-mail válido.';
		}
		else if (!$this->input->post('descricao'))
		{
			$dados['erro'] = 'Digite uma descrição para o histórico.';
		}
		else
		{
			if ($this->input->post('criar_compromisso'))
			{
				/*
				if (!$this->input->post('nome_compromisso'))
				{
					$dados['erro'] = 'Digite um nome para o compromisso.';
				}
				else 
				*/
				if (!$this->input->post('descricao_compromisso'))
				{
					$dados['erro'] = 'Digite uma descrição para o compromisso.';
				}
				else if (!$this->_validar_data($this->input->post('data_inicial_compromisso')))
				{
					$dados['erro'] = 'Digite uma data inicial válida.';
				}
				else if (!$this->_validar_horario($this->input->post('horario_inicial_compromisso')))
				{
					$dados['erro'] = 'Digite um horário inicial válido.';
				}
				else
				{
					$data_inicial = explode('/', $this->input->post('data_inicial_compromisso'));
					$horario_inicial = explode(':', $this->input->post('horario_inicial_compromisso'));
					
					$inicio_timestamp = mktime($horario_inicial[0], $horario_inicial[1], 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
					
					if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')))
					{
					
						$this->db->insert('compromissos', array(
							'timestamp' => time(),
							'id_usuario' => $this->input->post('id_usuario_compromisso') ? $this->input->post('id_usuario_compromisso') : $this->session->userdata('id_usuario'),
							'status' => 'agendado',
							'tipo' => 'Prospects',
							'codigo_tipo' => $id_prospect,
							'detalhes' => TRUE,
							'id_representante' => $prospect->codigo_usuario,
							'id_usuario_cad' => $this->db->from('usuarios')->where(array('codigo' => $prospect->codigo_usuario, 'grupo' => 'representantes'))->get()->row()->id,
							'nome' => $prospect->nome,
							'descricao' => $this->input->post('descricao_compromisso'),
							'inicio_timestamp' => $inicio_timestamp
						));
						
					}
					else
					{
						
						$this->db->insert('compromissos', array(
							'timestamp' => time(),
							'id_usuario' => $this->input->post('id_usuario_compromisso') ? $this->input->post('id_usuario_compromisso') : $this->session->userdata('id_usuario'),
							'status' => 'agendado',
							'tipo' => 'Prospects',
							'codigo_tipo' => $id_prospect,
							'id_representante' => $prospect->codigo_usuario,
							'nome' => $prospect->nome,
							'descricao' => $this->input->post('descricao_compromisso'),
							'inicio_timestamp' => $inicio_timestamp
						));
						
					}
					
				}
			}
			
			if (!$dados['erro'])
			{
				$i = $this->db->from('historicos_clientes')->get()->num_rows();
				$i += $this->db->from('historicos_prospects')->get()->num_rows();
				
				$protocolo = date('Y') . '/' . ($this->session->userdata('codigo_usuario') ? $this->session->userdata('codigo_usuario') : 0) . '.' . $i;
				
				$this->db->insert('historicos_prospects', array(
					'timestamp' => time(),
					'id_prospect' => $prospect->id,
					'cpf' => $prospect->cpf,
					'pessoa_contato' => $this->input->post('pessoa_contato'),
					'cargo' => $this->input->post('cargo'),
					'email' => $this->input->post('email'),
					'descricao' => $this->input->post('descricao'),
					'protocolo' => $protocolo
				));
			}
		}
		
		echo json_encode($dados);
	}
	
	function criar($tipo_pessoa = NULL)
	{
		if (!$tipo_pessoa)
		{
			redirect();
		}
		
		$campos_obrigatorios = false;
		////////////////////////////////////////
		if ($_POST && $tipo_pessoa == 'pessoa_fisica')
		{
			if (/* ENDERECO */
				!$this->input->post('cep') || !$this->input->post('endereco') || !$this->input->post('numero') || !$this->input->post('complemento') || !$this->input->post('bairro') || !$this->input->post('cidade') || !$this->input->post('estado') 
				
				/* CONTATO */
				|| !$this->input->post('telefone_contato_1') || !$this->input->post('email_contato_1') 
				
				/* INFORMACOES GERAIS */
				|| !$this->input->post('nome')
			)
			{
				$erro = 'Os campos em asterisco são obrigatórios.';
				
				$campos_obrigatorios = true;
			}
			
		}
		else if ($_POST && $tipo_pessoa == 'pessoa_juridica')
		{//echo 'else';
			if( /* INFORMACOES GERAIS */
				!$this->input->post('nome_fantasia')
				/* ENDERECO */
				|| !$this->input->post('cep') || !$this->input->post('endereco') || !$this->input->post('numero') || !$this->input->post('complemento') || !$this->input->post('bairro') || !$this->input->post('cidade') || !$this->input->post('estado') 
				/* CONTATO */    
				|| !$this->input->post('telefone_contato_1') || !$this->input->post('email_contato_1') || !$this->input->post('nome_contato_1') || !$this->input->post('cargo_contato_1') 
			)
			{
				$erro = 'Os campos em asterisco são obrigatórios.';
				
				$campos_obrigatorios = true;
			}
			
		}
		////////////////////////////////////////
		
		// ** VALIDAR FORM
		if ($_POST && !$campos_obrigatorios)
		{
			if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) && !$this->input->post('codigo_usuario'))
			{
				$erro = 'Selecione um representante.';
			}
			/*else if (!$this->input->post('nome'))
			{
				$erro = 'Digite um nome.';
			}*/
			else if(($this->input->post('nome') == '') or ($this->db->from('prospects')->where('nome', $this->input->post('nome'))->get()->row()))
			{
				$erro = 'O <b>Nome/Razão Social</b> digitado é invalido ou já existe.';
			}
			else if ($this->input->post('cpf') && !$this->_validar_cpf($this->input->post('cpf')) && !$this->_validar_cnpj($this->input->post('cpf')))
			{
				$erro = 'Digite um CPF/CNPJ válido.';
			}
			else if ($this->input->post('cpf') && $this->db->from('prospects')->where('cpf', $this->input->post('cpf'))->get()->row())
			{
				$erro = 'O CPF/CNPJ digitado já existe.';
			}
			else if ($this->input->post('cep') && !$this->_validar_cep($this->input->post('cep')))
			{
				$erro = 'Digite um CEP válido.';
			}
			else if ($this->input->post('telefone_contato_1') && !$this->_validar_telefone($this->input->post('telefone_contato_1')))
			{
				$erro = 'Digite um telefone válido para o contato 1.';
			}
			else if ($this->input->post('email_contato_1') && !valid_email($this->input->post('email_contato_1')))
			{
				$erro = 'Digite um e-mail válido para o contato 1.';
			}
			else if ($this->input->post('telefone_contato_2') && !$this->_validar_telefone($this->input->post('telefone_contato_2')))
			{
				$erro = 'Digite um telefone válido para o contato 2.';
			}
			else if ($this->input->post('email_contato_2') && !valid_email($this->input->post('email_contato_2')))
			{
				$erro = 'Digite um e-mail válido para o contato 2.';
			}
			else if ($this->input->post('telefone_contato_3') && !$this->_validar_telefone($this->input->post('telefone_contato_3')))
			{
				$erro = 'Digite um telefone válido para o contato 3.';
			}
			else if ($this->input->post('email_contato_3') && !valid_email($this->input->post('email_contato_3')))
			{
				$erro = 'Digite um e-mail válido para o contato 3.';
			}
			else
			{
				$this->db->insert('prospects', array(
					'timestamp' => time(),
					'codigo_usuario' => $this->input->post('codigo_usuario') ? $this->input->post('codigo_usuario') : $this->session->userdata('codigo_usuario'),
					'status' => 'ativo',
					'tipo_pessoa' => $tipo_pessoa,
					'nome' => $this->input->post('nome'),
					'nome_fantasia' => $this->input->post('nome_fantasia'),
					'cpf' => $this->input->post('cpf'),
					'rg' => $this->input->post('rg'),
					'inscricao_municipal' => $this->input->post('inscricao_municipal'),
					'email_nfe' => $this->input->post('email_nfe'),
					
					'cep' => $this->input->post('cep'),
					'endereco' => $this->input->post('endereco'),
					'numero' => $this->input->post('numero'),
					'complemento' => $this->input->post('complemento'),
					'bairro' => $this->input->post('bairro'),
					'cidade' => $this->input->post('cidade'),
					'estado' => $this->input->post('estado'),
					
					'cep_entrega' => $this->input->post('cep_entrega'),
					'endereco_entrega' => $this->input->post('endereco_entrega'),
					'numero_entrega' => $this->input->post('numero_entrega'),
					'complemento_entrega' => $this->input->post('complemento_entrega'),
					'bairro_entrega' => $this->input->post('bairro_entrega'),
					'cidade_entrega' => $this->input->post('cidade_entrega'),
					'estado_entrega' => $this->input->post('estado_entrega'),
					
					'cep_cobranca' => $this->input->post('cep_cobranca'),
					'endereco_cobranca' => $this->input->post('endereco_cobranca'),
					'numero_cobranca' => $this->input->post('numero_cobranca'),
					'complemento_cobranca' => $this->input->post('complemento_cobranca'),
					'bairro_cobranca' => $this->input->post('bairro_cobranca'),
					'cidade_cobranca' => $this->input->post('cidade_cobranca'),
					'estado_cobranca' => $this->input->post('estado_cobranca'),
					
					'site' => prep_url($this->input->post('site')),
					'nome_contato_1' => $this->input->post('nome_contato_1'),
					'cargo_contato_1' => $this->input->post('cargo_contato_1'),
					'telefone_contato_1' => $this->input->post('telefone_contato_1'),
					'email_contato_1' => $this->input->post('email_contato_1'),
					'nome_contato_2' => $this->input->post('nome_contato_2'),
					'cargo_contato_2' => $this->input->post('cargo_contato_2'),
					'telefone_contato_2' => $this->input->post('telefone_contato_2'),
					'email_contato_2' => $this->input->post('email_contato_2'),
					'nome_contato_3' => $this->input->post('nome_contato_3'),
					'cargo_contato_3' => $this->input->post('cargo_contato_3'),
					'telefone_contato_3' => $this->input->post('telefone_contato_3'),
					'email_contato_3' => $this->input->post('email_contato_3'),
					
					'ref_razao_social_1' => $this->input->post('ref_razao_social_1'),
					'ref_cidade_1' => $this->input->post('ref_cidade_1'),
					'ref_estado_1' => $this->input->post('ref_estado_1'),
					'ref_telefone_1' => $this->input->post('ref_telefone_1'),
					
					'ref_razao_social_2' => $this->input->post('ref_razao_social_2'),
					'ref_cidade_2' => $this->input->post('ref_cidade_2'),
					'ref_estado_2' => $this->input->post('ref_estado_2'),
					'ref_telefone_2' => $this->input->post('ref_telefone_2'),
					
					'ref_razao_social_3' => $this->input->post('ref_razao_social_3'),
					'ref_cidade_3' => $this->input->post('ref_cidade_3'),
					'ref_estado_3' => $this->input->post('ref_estado_3'),
					'ref_telefone_3' => $this->input->post('ref_telefone_3'),
					
					'banco_1' => $this->input->post('banco_1'),
					'agencia_1' => $this->input->post('agencia_1'),
					'conta_1' => $this->input->post('conta_1'),
					'banco_2' => $this->input->post('banco_2'),
					'agencia_2' => $this->input->post('agencia_2'),
					'conta_2' => $this->input->post('conta_2'),
					'observacao' => $this->input->post('observacao'),
					'id_feira' => $this->input->post('id_feira')
				));
				
				redirect('prospects/ver_detalhes/' . $this->db->insert_id());
			}
		}
		// VALIDAR FORM **
		
		// caso nenhum representante tenha sido especificado
		// vamos obter o primeiro da lista
		if (!$this->input->post('codigo_usuario'))
		{
			$_POST['codigo_usuario'] = $this->_obter_indice_primeiro_usuario_usuarios('codigo');
		}
		
		$conteudo = '
			<div class="caixa">
				<ul>
					<li>' . anchor('prospects', 'Voltar', 'onclick="history.go(-1); return false;"') . '</li>
				</ul>
			</div>
		';
		// ** EXIBIR FORM
		$conteudo .= heading('Cadastrar Prospect', 2);
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		$conteudo .= form_open(current_url());
		
		$conteudo .= '<div class="box_prospects" style="border: 1px solid #CCCCCC; padding: 0 15px 15px 15px; margin: 10px 0; background-color: #F6F6F6">';
		if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')))
		{
			//$conteudo .= '<p>' . form_label('Representante:' . br() . form_input('usuario', $this->input->post('usuario'), 'alt="obter_representantes_db_cliente" class="autocomplete" size="40"') . form_hidden('codigo_usuario', $this->input->post('codigo_usuario'))) . '</p>';
			$conteudo .= '
				<p>Usuário:</p>
				
				<p>' . $this->_obter_dropdown_representantes('codigo', array('name' => 'codigo_usuario', 'value' => $this->input->post('codigo_usuario')), FALSE) . '</p> 
			';
		}
		
		$conteudo .= hnordt_gerar_feiras_ativas();
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '</div>';
		
		$conteudo .= '
		<script type="text/javascript">
			$(document).ready(function(){
				$(".box_prospects").mouseover(function(){
					$(this).css({
						"background-color" : "#fffec0",
						"border" : "1px solid #eef047"
					});
				});
				
				$(".box_prospects").mouseout(function(){
					$(this).css({
						"background-color" : "#F6F6F6",
						"border" : "1px solid #CCCCCC"
					});
				});
			});
		</script>
		';
		
		$conteudo .= '<div class="box_prospects" style="border: 1px solid #CCCCCC; padding: 15px; margin: 10px 0; background-color: #F6F6F6">';
		$conteudo .= heading('Informações Gerais', 4);
		if ($tipo_pessoa == 'pessoa_fisica')
		{
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*Nome:' . br() . form_input('nome', $this->input->post('nome'), 'size="40"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('CPF:' . br() . form_input('cpf', $this->input->post('cpf'), 'alt="cpf" size="10"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('RG:' . br() . form_input('rg', $this->input->post('rg'), 'size="10"')) . '</p>';
		}
		else
		{
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Razão social:' . br() . form_input('nome', $this->input->post('nome'), 'size="40"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*Nome fantasia:' . br() . form_input('nome_fantasia', $this->input->post('nome_fantasia'))) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('CNPJ:' . br() . form_input('cpf', $this->input->post('cpf'), 'alt="cnpj" size="14"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Inscrição estadual:' . br() . form_input('rg', $this->input->post('rg'), 'size="10"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Inscrição municipal:' . br() . form_input('inscricao_municipal', $this->input->post('inscricao_municipal'), 'size="10"')) . '</p>';
			
			$conteudo .= '<div style="clear:both"></div>';
			if ($tipo_pessoa != 'pessoa_fisica')
			{
				$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('E-mail NFE:' . br() . form_input('email_nfe', $this->input->post('email_nfe'), 'size="50"')) . '</p>';
				$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Site:' . br() . 'http://' . form_input('site', $this->input->post('site'), 'size="40"')) . '</p>';
			}
		}
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '</div>';
		
		if ($tipo_pessoa != 'pessoa_fisica')
		{

			$conteudo .= '<div class="box_prospects" style="border: 1px solid #CCCCCC; padding: 15px; margin: 10px 0; background-color: #F6F6F6">';
			$conteudo .= '
			<script type="text/javascript">
				$(document).ready(function(){

					$("#copiar_cobranca").click(function()
					{
						if ($(this).is(":checked"))
						{
							$("input[name=cep]").val($("input[name=cep_cobranca]").val());
							$("input[name=endereco]").val($("input[name=endereco_cobranca]").val());
							$("input[name=numero]").val($("input[name=numero_cobranca]").val());
							$("input[name=complemento]").val($("input[name=complemento_cobranca]").val());
							$("input[name=bairro]").val($("input[name=bairro_cobranca]").val());
							$("input[name=cidade]").val($("input[name=cidade_cobranca]").val());
							
							$("select[name=estado_cobranca] option:selected").each(function () {
								//alert($(this).val());
								
								var estado = $(this).val();
								
								$("select[name=estado] option").each(function () {
									if($(this).val() == estado){
										$(this).attr("selected", "selected");
									}
								});
								
							});


						}
						else
						{
							$("input[name=cep]").val("");
							$("input[name=endereco]").val("");
							$("input[name=numero]").val("");
							$("input[name=complemento]").val("");
							$("input[name=bairro]").val("");
							$("input[name=cidade]").val("");
						}
					});
				
				});
			</script>';
			
			
			$conteudo .= '<div style="clear: both;"></div>';
			/*$conteudo .= heading('Endereço de Cobrança', 4);
			$conteudo .= '<div dojoType="dijit.Tooltip" connectId="cep" position="below">Ao digitar o CEP os campos Endereço, Bairro, Cidade e Estado serão preenchidos automaticamente.</div>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*CEP:' . br() . form_input('cep_cobranca', $this->input->post('cep_cobranca'), 'id="cep" alt="cep" size="5"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*Endereço:' . br() . form_input('endereco_cobranca', $this->input->post('endereco_cobranca'), 'size="53"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*Nº:' . br() . form_input('numero_cobranca', $this->input->post('numero_cobranca'), 'size="1"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*Complemento:' . br() . form_input('complemento_cobranca', $this->input->post('complemento_cobranca'), 'size="9"')) . '</p>';
			
			$conteudo .= '<div style="clear: both;"></div>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*Bairro:' . br() . form_input('bairro_cobranca', $this->input->post('bairro_cobranca'), 'size="20"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*Cidade:' . br() . form_input('cidade_cobranca', $this->input->post('cidade_cobranca'), 'size="20"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*Estado:' . br() . form_dropdown('estado_cobranca', $this->_obter_estados(), $this->input->post('estado_cobranca'))) . '</p>';
			$conteudo .= '<div style="clear: both;"></div>';
			*/
			$conteudo .= '</div>';
			
			
			
			$conteudo .= '<div class="box_prospects" style="border: 1px solid #CCCCCC; padding: 15px; margin: 10px 0; background-color: #F6F6F6">';
			$conteudo .= heading('Endereço de Entrega', 4);
			//$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_checkbox('copiar_cobranca', 'sim', FALSE, 'id="copiar_cobranca"') . ' Copiar Endereço de Cobrança</p>';
			$conteudo .= '<div style="clear:both"></div>';
			$conteudo .= '<div dojoType="dijit.Tooltip" connectId="cep" position="below">Ao digitar o CEP os campos Endereço, Bairro, Cidade e Estado serão preenchidos automaticamente.</div>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*CEP:' . br() . form_input('cep', $this->input->post('cep'), 'id="cep" alt="cep" size="5"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*Endereço:' . br() . form_input('endereco', $this->input->post('endereco'), 'size="53"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*Nº:' . br() . form_input('numero', $this->input->post('numero'), 'size="1"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*Complemento:' . br() . form_input('complemento', $this->input->post('complemento'), 'size="9"')) . '</p>';
			
			$conteudo .= '<div style="clear: both;"></div>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*Bairro:' . br() . form_input('bairro', $this->input->post('bairro'), 'size="20"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*Cidade:' . br() . form_input('cidade', $this->input->post('cidade'), 'size="20"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*Estado:' . br() . form_dropdown('estado', $this->_obter_estados(), $this->input->post('estado'))) . '</p>';
			$conteudo .= '<div style="clear: both;"></div>';
			$conteudo .= '</div>';
			
		
		}
		else
		{
		
			$conteudo .= '<div class="box_prospects" style="border: 1px solid #CCCCCC; padding: 15px; margin: 10px 0; background-color: #F6F6F6">';
			$conteudo .= '<div style="clear: both;"></div>';
			$conteudo .= heading('Endereço', 4);
			$conteudo .= '<div dojoType="dijit.Tooltip" connectId="cep" position="below">Ao digitar o CEP os campos Endereço, Bairro, Cidade e Estado serão preenchidos automaticamente.</div>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*CEP:' . br() . form_input('cep', $this->input->post('cep'), 'id="cep" alt="cep" size="5"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*Endereço:' . br() . form_input('endereco', $this->input->post('endereco'), 'size="53"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*Nº:' . br() . form_input('numero', $this->input->post('numero'), 'size="1"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*Complemento:' . br() . form_input('complemento', $this->input->post('complemento'), 'size="9"')) . '</p>';
			
			$conteudo .= '<div style="clear: both;"></div>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*Bairro:' . br() . form_input('bairro', $this->input->post('bairro'), 'size="20"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*Cidade:' . br() . form_input('cidade', $this->input->post('cidade'), 'size="20"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*Estado:' . br() . form_dropdown('estado', $this->_obter_estados(), $this->input->post('estado'))) . '</p>';
			$conteudo .= '<div style="clear: both;"></div>';
			$conteudo .= '</div>';
		
		}
		
		
		
		
		
		$conteudo .= '<div class="box_prospects" style="border: 1px solid #CCCCCC; padding: 15px; margin: 10px 0; background-color: #F6F6F6">';
		if ($tipo_pessoa != 'pessoa_fisica')
		{
			$conteudo .= heading('Contatos', 4);
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*Nome:' . br() . form_input('nome_contato_1', $this->input->post('nome_contato_1'), 'size="40"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*Cargo:' . br() . form_input('cargo_contato_1', $this->input->post('cargo_contato_1'))) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*Telefone:' . br() . form_input('telefone_contato_1', $this->input->post('telefone_contato_1'), 'alt="phone" size="10"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*E-mail:' . br() . form_input('email_contato_1', $this->input->post('email_contato_1'), 'size="40"')) . '</p>';
			$conteudo .= '<div style="clear: both;"></div>';
		}
		else
		{
			$conteudo .= heading('Contato', 4);
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*Telefone:' . br() . form_input('telefone_contato_1', $this->input->post('telefone_contato_1'), 'alt="phone" size="10"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Telefone 2:' . br() . form_input('telefone_contato_2', $this->input->post('telefone_contato_2'), 'alt="phone" size="10"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('*E-mail:' . br() . form_input('email_contato_1', $this->input->post('email_contato_1'), 'size="40"')) . '</p>';
			$conteudo .= '<div style="clear: both;"></div>';
		}
		
		if ($tipo_pessoa != 'pessoa_fisica')
		{
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Nome:' . br() . form_input('nome_contato_2', $this->input->post('nome_contato_2'), 'size="40"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Cargo:' . br() . form_input('cargo_contato_2', $this->input->post('cargo_contato_2'))) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Telefone:' . br() . form_input('telefone_contato_2', $this->input->post('telefone_contato_2'), 'alt="phone" size="10"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('E-mail:' . br() . form_input('email_contato_2', $this->input->post('email_contato_2'), 'size="40"')) . '</p>';
			$conteudo .= '<div style="clear: both;"></div>';
			
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Nome:' . br() . form_input('nome_contato_3', $this->input->post('nome_contato_3'), 'size="40"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Cargo:' . br() . form_input('cargo_contato_3', $this->input->post('cargo_contato_3'))) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Telefone:' . br() . form_input('telefone_contato_3', $this->input->post('telefone_contato_3'), 'alt="phone" size="10"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('E-mail:' . br() . form_input('email_contato_3', $this->input->post('email_contato_3'), 'size="40"')) . '</p>';
			$conteudo .= '<div style="clear: both;"></div>';
		}
		$conteudo .= '</div>';
		
		
		$conteudo .= '<div class="box_prospects" style="border: 1px solid #CCCCCC; padding: 15px; margin: 10px 0; background-color: #F6F6F6">';
		$conteudo .= heading('Referências Comerciais', 4);
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Razão Social:' . br() . form_input('ref_razao_social_1', $this->input->post('ref_razao_social_1'), 'size="30"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Cidade:' . br() . form_input('ref_cidade_1', $this->input->post('ref_cidade_1'), 'size="20"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Estado:' . br() . form_dropdown('ref_estado_1', $this->_obter_estados(), $this->input->post('ref_estado_1'))) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Telefone:' . br() . form_input('ref_telefone_1', $this->input->post('ref_telefone_1'), 'alt="phone" size="10"')) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Razão Social:' . br() . form_input('ref_razao_social_2', $this->input->post('ref_razao_social_2'), 'size="30"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Cidade:' . br() . form_input('ref_cidade_2', $this->input->post('ref_cidade_2'), 'size="20"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Estado:' . br() . form_dropdown('ref_estado_2', $this->_obter_estados(), $this->input->post('ref_estado_2'))) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Telefone:' . br() . form_input('ref_telefone_2', $this->input->post('ref_telefone_2'), 'alt="phone" size="10"')) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Razão Social:' . br() . form_input('ref_razao_social_3', $this->input->post('ref_razao_social_3'), 'size="30"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Cidade:' . br() . form_input('ref_cidade_3', $this->input->post('ref_cidade_3'), 'size="20"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Estado:' . br() . form_dropdown('ref_estado_3', $this->_obter_estados(), $this->input->post('ref_estado_3'))) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Telefone:' . br() . form_input('ref_telefone_3', $this->input->post('ref_telefone_3'), 'alt="phone" size="10"')) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '</div>';
		
		
		$conteudo .= '<div class="box_prospects" style="border: 1px solid #CCCCCC; padding: 15px; margin: 10px 0; background-color: #F6F6F6">';
		$conteudo .= heading('Referências Bancárias', 4);
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Banco 1:' . br() . form_input('banco_1', $this->input->post('banco_1'), 'size="10"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Agência 1:' . br() . form_input('agencia_1', $this->input->post('agencia_1'), 'size="10"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Conta 1:' . br() . form_input('conta_1', $this->input->post('conta_1'), 'size="10"')) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Banco 2:' . br() . form_input('banco_2', $this->input->post('banco_2'), 'size="10"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Agência 2:' . br() . form_input('agencia_2', $this->input->post('agencia_2'), 'size="10"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Conta 2:' . br() . form_input('conta_2', $this->input->post('conta_2'), 'size="10"')) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '</div>';
		
		
		$conteudo .= '<div class="box_prospects" style="border: 1px solid #CCCCCC; padding: 15px; margin: 10px 0; background-color: #F6F6F6">';
		$conteudo .= heading('Outros', 4);
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Observação:' . br() . form_textarea(array('cols' => 60, 'name' => 'observacao', 'rows' => 5, 'value' => $this->input->post('observacao')))) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '</div>';
		
		$conteudo .= heading('Opções', 4);
		$conteudo .= '<p>' . form_submit('concluir', 'Concluir') . ' ou ' . anchor('prospects', 'Cancelar') . '</p>';
		$conteudo .= form_close();
		// EXIBIR FORM **
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function editar($tipo_pessoa = NULL, $id = NULL)
	{
		if (!$tipo_pessoa)
		{
			redirect();
		}
		
		if (!$id)
		{
			redirect();	
		}
		
		$prospect = $this->db->from('prospects')->where('id', $id)->get()->row();
		
		$representante = $this->db_cliente->obter_representante($prospect->codigo_usuario);
		
		// ** VALIDAR FORM
		if ($_POST)
		{
			if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) && !$this->input->post('codigo_usuario'))
			{
				$erro = 'Selecione um representante.';
			}
			else if (!$this->input->post('nome'))
			{
				$erro = 'Digite um nome.';
			}
			else if ($this->db->from('prospects')->where(array('id !=' => $id, 'nome' => $this->input->post('nome')))->get()->row())
			{
				$erro = 'O nome digitado já existe.';
			}
			else if ($this->input->post('cpf') && !$this->_validar_cpf($this->input->post('cpf')) && !$this->_validar_cnpj($this->input->post('cpf')))
			{
				$erro = 'Digite um CPF/CNPJ válido.';
			}
			else if ($this->input->post('cpf') && $this->db->from('prospects')->where(array('id !=' => $id, 'cpf' => $this->input->post('cpf')))->get()->row())
			{
				$erro = 'O CPF/CNPJ digitado já existe.';
			}
			else if ($this->input->post('cep') && !$this->_validar_cep($this->input->post('cep')))
			{
				$erro = 'Digite um CEP válido.';
			}
			else if ($this->input->post('telefone_contato_1') && !$this->_validar_telefone($this->input->post('telefone_contato_1')))
			{
				$erro = 'Digite um telefone válido para o contato 1.';
			}
			else if ($this->input->post('email_contato_1') && !valid_email($this->input->post('email_contato_1')))
			{
				$erro = 'Digite um e-mail válido para o contato 1.';
			}
			else if ($this->input->post('telefone_contato_2') && !$this->_validar_telefone($this->input->post('telefone_contato_2')))
			{
				$erro = 'Digite um telefone válido para o contato 2.';
			}
			else if ($this->input->post('email_contato_2') && !valid_email($this->input->post('email_contato_2')))
			{
				$erro = 'Digite um e-mail válido para o contato 2.';
			}
			else if ($this->input->post('telefone_contato_3') && !$this->_validar_telefone($this->input->post('telefone_contato_3')))
			{
				$erro = 'Digite um telefone válido para o contato 3.';
			}
			else if ($this->input->post('email_contato_3') && !valid_email($this->input->post('email_contato_3')))
			{
				$erro = 'Digite um e-mail válido para o contato 3.';
			}
			else
			{
				$this->db->update('prospects', array(
					'codigo_usuario' => $this->input->post('codigo_usuario') ? $this->input->post('codigo_usuario') : $this->session->userdata('codigo_usuario'),
					'status' => $this->input->post('status'),
					'nome' => $this->input->post('nome'),
					'nome_fantasia' => $this->input->post('nome_fantasia'),
					'cpf' => $this->input->post('cpf'),
					'rg' => $this->input->post('rg'),
					'cep' => $this->input->post('cep'),
					'endereco' => $this->input->post('endereco'),
					'numero' => $this->input->post('numero'),
					'complemento' => $this->input->post('complemento'),
					'bairro' => $this->input->post('bairro'),
					'cidade' => $this->input->post('cidade'),
					'estado' => $this->input->post('estado'),

					'cep_entrega' => $this->input->post('cep_entrega'),
					'endereco_entrega' => $this->input->post('endereco_entrega'),
					'numero_entrega' => $this->input->post('numero_entrega'),
					'complemento_entrega' => $this->input->post('complemento_entrega'),
					'bairro_entrega' => $this->input->post('bairro_entrega'),
					'cidade_entrega' => $this->input->post('cidade_entrega'),
					'estado_entrega' => $this->input->post('estado_entrega'),

					'cep_cobranca' => $this->input->post('cep_cobranca'),
					'endereco_cobranca' => $this->input->post('endereco_cobranca'),
					'numero_cobranca' => $this->input->post('numero_cobranca'),
					'complemento_cobranca' => $this->input->post('complemento_cobranca'),
					'bairro_cobranca' => $this->input->post('bairro_cobranca'),
					'cidade_cobranca' => $this->input->post('cidade_cobranca'),
					'estado_cobranca' => $this->input->post('estado_cobranca'),
					
					'site' => prep_url($this->input->post('site')),
					'nome_contato_1' => $this->input->post('nome_contato_1'),
					'cargo_contato_1' => $this->input->post('cargo_contato_1'),
					'telefone_contato_1' => $this->input->post('telefone_contato_1'),
					'email_contato_1' => $this->input->post('email_contato_1'),
					'nome_contato_2' => $this->input->post('nome_contato_2'),
					'cargo_contato_2' => $this->input->post('cargo_contato_2'),
					'telefone_contato_2' => $this->input->post('telefone_contato_2'),
					'email_contato_2' => $this->input->post('email_contato_2'),
					'nome_contato_3' => $this->input->post('nome_contato_3'),
					'cargo_contato_3' => $this->input->post('cargo_contato_3'),
					'telefone_contato_3' => $this->input->post('telefone_contato_3'),
					'email_contato_3' => $this->input->post('email_contato_3'),
					
					'ref_razao_social_1' => $this->input->post('ref_razao_social_1'),
					'ref_cidade_1' => $this->input->post('ref_cidade_1'),
					'ref_estado_1' => $this->input->post('ref_estado_1'),
					'ref_telefone_1' => $this->input->post('ref_telefone_1'),
					
					'ref_razao_social_2' => $this->input->post('ref_razao_social_2'),
					'ref_cidade_2' => $this->input->post('ref_cidade_2'),
					'ref_estado_2' => $this->input->post('ref_estado_2'),
					'ref_telefone_2' => $this->input->post('ref_telefone_2'),
					
					'ref_razao_social_3' => $this->input->post('ref_razao_social_3'),
					'ref_cidade_3' => $this->input->post('ref_cidade_3'),
					'ref_estado_3' => $this->input->post('ref_estado_3'),
					'ref_telefone_3' => $this->input->post('ref_telefone_3'),
					
					'banco_1' => $this->input->post('banco_1'),
					'agencia_1' => $this->input->post('agencia_1'),
					'conta_1' => $this->input->post('conta_1'),
					'banco_2' => $this->input->post('banco_2'),
					'agencia_2' => $this->input->post('agencia_2'),
					'conta_2' => $this->input->post('conta_2'),
					'observacao' => $this->input->post('observacao'),
					'id_feira' => $this->input->post('id_feira')
				), array('id' => $id));
				
				redirect('prospects/ver_detalhes/' . $id);
			}
		}
		else
		{
			foreach ($prospect as $indice => $valor)
			{
				$_POST[$indice] = $valor;
			}
		}
		// VALIDAR FORM **
		
		// ** EXIBIR FORM
		$conteudo .= heading('Editar Prospect', 2);
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		$conteudo .= form_open(current_url());
		
		$conteudo .= '
		<script type="text/javascript">
			$(document).ready(function(){
				$(".box_prospects").mouseover(function(){
					$(this).css({
						"background-color" : "#fffec0",
						"border" : "1px solid #eef047"
					});
				});
				
				$(".box_prospects").mouseout(function(){
					$(this).css({
						"background-color" : "#F6F6F6",
						"border" : "1px solid #CCCCCC"
					});
				});
			});
		</script>
		';
		
		$conteudo .= '<div class="box_prospects" style="border: 1px solid #CCCCCC; padding:  0 15px 15px 15px; margin: 10px 0; background-color: #F6F6F6">';
		if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros'))) {
			//$conteudo .= '<p>' . form_label('Representante:' . br() . form_input('usuario', $this->input->post('representante') ? $this->input->post('representante') : $representante['nome'], 'alt="obter_representantes" class="autocomplete" size="40"') . form_hidden('codigo_usuario', $this->input->post('codigo_usuario'))) . '</p>';
			$conteudo .= '
				<p>Usuário:</p>
				
				<p>' . $this->_obter_dropdown_representantes('codigo', array('name' => 'codigo_usuario', 'value' => $this->input->post('codigo_usuario')), FALSE) . '</p> 
			';
		}
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Status:' . br() . form_dropdown('status', $this->_obter_status(), $this->input->post('status'))) . '</p>';
		
		$conteudo .= hnordt_gerar_feiras_ativas();
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '</div>';
		
		
		
		$conteudo .= '<div class="box_prospects" style="border: 1px solid #CCCCCC; padding: 15px; margin: 10px 0; background-color: #F6F6F6">';
		$conteudo .= heading('Informações Gerais', 4);
		if ($tipo_pessoa == 'pessoa_fisica')
		{
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Nome:' . br() . form_input('nome', $this->input->post('nome'))) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('CPF:' . br() . form_input('cpf', $this->input->post('cpf'), 'alt="cpf" size="10"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('RG:' . br() . form_input('rg', $this->input->post('rg'), 'size="10"')) . '</p>';
		}
		else
		{
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Razão social:' . br() . form_input('nome', $this->input->post('nome'), 'size="40"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Nome fantasia:' . br() . form_input('nome_fantasia', $this->input->post('nome_fantasia'))) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('CNPJ:' . br() . form_input('cpf', $this->input->post('cpf'), 'alt="cnpj" size="14"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Inscrição estadual:' . br() . form_input('rg', $this->input->post('rg'), 'size="10"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Inscrição municipal:' . br() . form_input('inscricao_municipal', $this->input->post('inscricao_municipal'), 'size="10"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Site:' . br() . 'http://' . form_input('site', $this->input->post('site'), 'size="40"')) . '</p>';
			
		}
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '</div>';
		
		
		if ($tipo_pessoa != 'pessoa_fisica')
		{
			
			$conteudo .= '<div class="box_prospects" style="border: 1px solid #CCCCCC; padding: 15px; margin: 10px 0; background-color: #F6F6F6">';
		
			$conteudo .= heading('Endereço de Cobrança', 4);
			$conteudo .= '<div dojoType="dijit.Tooltip" connectId="cep" position="below">Ao digitar o CEP os campos Endereço, Bairro, Cidade e Estado serão preenchidos automaticamente.</div>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('CEP:' . br() . form_input('cep_cobranca', $this->input->post('cep_cobranca'), 'id="cep" alt="cep" size="5"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Endereço:' . br() . form_input('endereco_cobranca', $this->input->post('endereco_cobranca'), 'size="53"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Nº:' . br() . form_input('numero_cobranca', $this->input->post('numero_cobranca'), 'size="1"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Complemento:' . br() . form_input('complemento_cobranca', $this->input->post('complemento_cobranca'), 'size="9"')) . '</p>';

			$conteudo .= '<div style="clear: both;"></div>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Bairro:' . br() . form_input('bairro_cobranca', $this->input->post('bairro_cobranca'), 'size="20"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Cidade:' . br() . form_input('cidade_cobranca', $this->input->post('cidade_cobranca'), 'size="20"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Estado:' . br() . form_dropdown('estado_cobranca', $this->_obter_estados(), $this->input->post('estado_cobranca'))) . '</p>';
			$conteudo .= '<div style="clear: both;"></div>';
			
			$conteudo .= '</div>';
			
			$conteudo .= '
			<script type="text/javascript">
				$(document).ready(function(){

				$("#copiar_cobranca").click(function()
				{
					if ($(this).is(":checked"))
					{
						$("input[name=cep]").val($("input[name=cep_cobranca]").val());
						$("input[name=endereco]").val($("input[name=endereco_cobranca]").val());
						$("input[name=numero]").val($("input[name=numero_cobranca]").val());
						$("input[name=complemento]").val($("input[name=complemento_cobranca]").val());
						$("input[name=bairro]").val($("input[name=bairro_cobranca]").val());
						$("input[name=cidade]").val($("input[name=cidade_cobranca]").val());
						
						$("select[name=estado_cobranca] option:selected").each(function () {
							//alert($(this).val());
							
							var estado = $(this).val();
							
							$("select[name=estado] option").each(function () {
								if($(this).val() == estado){
									$(this).attr("selected", "selected");
								}
							});
							
						});


					}
					else
					{
						$("input[name=cep]").val("");
						$("input[name=endereco]").val("");
						$("input[name=numero]").val("");
						$("input[name=complemento]").val("");
						$("input[name=bairro]").val("");
						$("input[name=cidade]").val("");
					}
				});
			
			});
			</script>';
			
			$conteudo .= '<div class="box_prospects" style="border: 1px solid #CCCCCC; padding: 15px; margin: 10px 0; background-color: #F6F6F6">';
			$conteudo .= heading('Endereço de Entrega', 4);
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_checkbox('copiar_cobranca', 'sim', ($this->input->post('cep_cobranca') ? TRUE : FALSE), 'id="copiar_cobranca"') . ' Copiar Endereço de Entrega</p>';
			$conteudo .= '<div style="clear: both;"></div>';
			
			$conteudo .= '<div dojoType="dijit.Tooltip" connectId="cep" position="below">Ao digitar o CEP os campos Endereço, Bairro, Cidade e Estado serão preenchidos automaticamente.</div>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('CEP:' . br() . form_input('cep', $this->input->post('cep'), 'id="cep" alt="cep" size="5"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Endereço:' . br() . form_input('endereco', $this->input->post('endereco'), 'size="53"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Nº:' . br() . form_input('numero', $this->input->post('numero'), ' size="1"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Complemento:' . br() . form_input('complemento', $this->input->post('complemento'), ' size="9"')) . '</p>';
			
			$conteudo .= '<div style="clear: both;"></div>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Bairro:' . br() . form_input('bairro', $this->input->post('bairro'), 'size="20"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Cidade:' . br() . form_input('cidade', $this->input->post('cidade'), 'size="20"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Estado:' . br() . form_dropdown('estado', $this->_obter_estados(), $this->input->post('estado'))) . '</p>';
			$conteudo .= '<div style="clear: both;"></div>';
			$conteudo .= '</div>';
			
		
		}
		else
		{
		
			$conteudo .= '<div class="box_prospects" style="border: 1px solid #CCCCCC; padding: 15px; margin: 10px 0; background-color: #F6F6F6">';
			$conteudo .= heading('Endereço', 4);
			$conteudo .= '<div dojoType="dijit.Tooltip" connectId="cep" position="below">Ao digitar o CEP os campos Endereço, Bairro, Cidade e Estado serão preenchidos automaticamente.</div>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('CEP:' . br() . form_input('cep', $this->input->post('cep'), 'id="cep" alt="cep" size="5"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Endereço:' . br() . form_input('endereco', $this->input->post('endereco'), 'size="53"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Nº:' . br() . form_input('numero', $this->input->post('numero'), ' size="1"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Complemento:' . br() . form_input('complemento', $this->input->post('complemento'), ' size="9"')) . '</p>';
			
			$conteudo .= '<div style="clear: both;"></div>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Bairro:' . br() . form_input('bairro', $this->input->post('bairro'), 'size="20"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Cidade:' . br() . form_input('cidade', $this->input->post('cidade'), 'size="20"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Estado:' . br() . form_dropdown('estado', $this->_obter_estados(), $this->input->post('estado'))) . '</p>';
			$conteudo .= '<div style="clear: both;"></div>';
			$conteudo .= '</div>';
		
		}
		
		
		$conteudo .= '<div class="box_prospects" style="border: 1px solid #CCCCCC; padding: 15px; margin: 10px 0; background-color: #F6F6F6">';
		if ($tipo_pessoa != 'pessoa_fisica')
		{
			$conteudo .= heading('Contatos', 4);
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Nome:' . br() . form_input('nome_contato_1', $this->input->post('nome_contato_1'), 'size="40"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Cargo:' . br() . form_input('cargo_contato_1', $this->input->post('cargo_contato_1'))) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Telefone:' . br() . form_input('telefone_contato_1', $this->input->post('telefone_contato_1'), 'alt="phone" size="10"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('E-mail:' . br() . form_input('email_contato_1', $this->input->post('email_contato_1'), 'size="40"')) . '</p>';
			$conteudo .= '<div style="clear: both;"></div>';
		}
		else
		{
			$conteudo .= heading('Contato', 4);
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Telefone:' . br() . form_input('telefone_contato_1', $this->input->post('telefone_contato_1'), 'alt="phone" size="10"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Telefone 2:' . br() . form_input('telefone_contato_2', $this->input->post('telefone_contato_2'), 'alt="phone" size="10"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('E-mail:' . br() . form_input('email_contato_1', $this->input->post('email_contato_1'), 'size="40"')) . '</p>';
			$conteudo .= '<div style="clear: both;"></div>';
		}
		
		if ($tipo_pessoa != 'pessoa_fisica')
		{
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Nome:' . br() . form_input('nome_contato_2', $this->input->post('nome_contato_2'), 'size="40"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Cargo:' . br() . form_input('cargo_contato_2', $this->input->post('cargo_contato_2'))) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Telefone:' . br() . form_input('telefone_contato_2', $this->input->post('telefone_contato_2'), 'alt="phone" size="10"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('E-mail:' . br() . form_input('email_contato_2', $this->input->post('email_contato_2'), 'size="40"')) . '</p>';
			$conteudo .= '<div style="clear: both;"></div>';

			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Nome:' . br() . form_input('nome_contato_3', $this->input->post('nome_contato_3'), 'size="40"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Cargo:' . br() . form_input('cargo_contato_3', $this->input->post('cargo_contato_3'))) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Telefone:' . br() . form_input('telefone_contato_3', $this->input->post('telefone_contato_3'), 'alt="phone" size="10"')) . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('E-mail:' . br() . form_input('email_contato_3', $this->input->post('email_contato_3'), 'size="40"')) . '</p>';
			$conteudo .= '<div style="clear: both;"></div>';
		}
		$conteudo .= '</div>';
		
		$conteudo .= '<div class="box_prospects" style="border: 1px solid #CCCCCC; padding: 15px; margin: 10px 0; background-color: #F6F6F6">';
		$conteudo .= heading('Referências Comerciais', 4);
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Razão Social:' . br() . form_input('ref_razao_social_1', $this->input->post('ref_razao_social_1'), 'size="30"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Cidade:' . br() . form_input('ref_cidade_1', $this->input->post('ref_cidade_1'), 'size="20"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Estado:' . br() . form_dropdown('ref_estado_1', $this->_obter_estados(), $this->input->post('ref_estado_1'))) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Telefone:' . br() . form_input('ref_telefone_1', $this->input->post('ref_telefone_1'), 'alt="phone" size="10"')) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';

		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Razão Social:' . br() . form_input('ref_razao_social_2', $this->input->post('ref_razao_social_2'), 'size="30"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Cidade:' . br() . form_input('ref_cidade_2', $this->input->post('ref_cidade_2'), 'size="20"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Estado:' . br() . form_dropdown('ref_estado_2', $this->_obter_estados(), $this->input->post('ref_estado_2'))) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Telefone:' . br() . form_input('ref_telefone_2', $this->input->post('ref_telefone_2'), 'alt="phone" size="10"')) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';

		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Razão Social:' . br() . form_input('ref_razao_social_3', $this->input->post('ref_razao_social_3'), 'size="30"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Cidade:' . br() . form_input('ref_cidade_3', $this->input->post('ref_cidade_3'), 'size="20"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Estado:' . br() . form_dropdown('ref_estado_3', $this->_obter_estados(), $this->input->post('ref_estado_3'))) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Telefone:' . br() . form_input('ref_telefone_3', $this->input->post('ref_telefone_3'), 'alt="phone" size="10"')) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '</div>';
			
		$conteudo .= '<div class="box_prospects" style="border: 1px solid #CCCCCC; padding: 15px; margin: 10px 0; background-color: #F6F6F6">';
		$conteudo .= heading('Referências Bancárias', 4);
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Banco 1:' . br() . form_input('banco_1', $this->input->post('banco_1'), 'size="10"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Agência 1:' . br() . form_input('agencia_1', $this->input->post('agencia_1'), 'size="10"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Conta 1:' . br() . form_input('conta_1', $this->input->post('conta_1'), 'size="10"')) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Banco 2:' . br() . form_input('banco_2', $this->input->post('banco_2'), 'size="10"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Agência 2:' . br() . form_input('agencia_2', $this->input->post('agencia_2'), 'size="10"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Conta 2:' . br() . form_input('conta_2', $this->input->post('conta_2'), 'size="10"')) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '</div>';
		
		$conteudo .= '<div class="box_prospects" style="border: 1px solid #CCCCCC; padding: 15px; margin: 10px 0; background-color: #F6F6F6">';
		$conteudo .= heading('Outros', 4);
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Observação:' . br() . form_textarea(array('cols' => 60, 'name' => 'observacao', 'rows' => 5, 'value' => $this->input->post('observacao')))) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '</div>';
		
		
		$conteudo .= heading('Opções', 4);
		$conteudo .= '<p>' . form_submit('concluir', 'Concluir') . ' ou ' . anchor('prospects', 'Cancelar') . '</p>';
		$conteudo .= form_close();
		// EXIBIR FORM **
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function converter_cliente($id_prospect = NULL)
	{
		if (!$id_prospect)
		{
			redirect();	
		}
		
		$prospect = $this->db->from('prospects')->where('id', $id_prospect)->get()->row();
		
		if (!$prospect)
		{
			redirect();	
		}
		
		if ($prospect->status == 'convertido_cliente')
		{
			redirect();	
		}
		
		$dados = $this->db_cliente->criar_cliente(array(
			'codigo_representante' => $prospect->codigo_usuario,
			'tipo_pessoa' => $prospect->tipo_pessoa,
			'nome' => $prospect->nome,
			'nome_fantasia' => $prospect->nome_fantasia,
			'cpf' => $prospect->cpf,
			'rg' => $prospect->rg,
			'cep' => $prospect->cep,
			'endereco' => $prospect->endereco,
			'numero' => $prospect->numero,
			'bairro' => $prospect->bairro,
			'cidade' => $prospect->cidade,
			'estado' => $prospect->estado,
			'site' => $prospect->site,
			'pessoa_contato' => $prospect->nome_contato_1,
			'telefone' => $prospect->telefone_contato_1,
			'observacao' => $prospect->observacao
		));
		
		$codigo_cliente = $dados['codigo'];
		$loja_cliente = $dados['loja'];
		
		if ($codigo_cliente) {
			$this->db->update('prospects', array('status' => 'convertido_cliente', 'conversao_cliente_timestamp' => time(), 'codigo_cliente' => $codigo_cliente, 'loja_cliente' => $loja_cliente), array('id' => $prospect->id));
			
			// históricos
			$historicos = $this->db->from('historicos_prospects')->where('id_prospect', $prospect->id)->get()->result();
			foreach ($historicos as $historico)
			{
				$this->db->insert('historicos_clientes', array(
					'timestamp' => $historico->timestamp,
					'codigo_cliente' => $codigo_cliente,
					'loja_cliente' => $loja_cliente,
					'pessoa_contato' => $historico->pessoa_contato,
					'cargo' => $historico->cargo,
					'email' => $historico->email,
					'descricao' => $historico->descricao,
					'protocolo' => NULL // TODO
				));
			}
			
			if ($this->session->userdata('id_orcamento_aguardando_transformacao_pedido')) {
				$orcamento = $this->db->from('pedidos')->where('id', $this->session->userdata('id_orcamento_aguardando_transformacao_pedido'))->get()->row();
				
				// atualizar pedidos/orçamentos
				$this->db->update('pedidos', array('codigo_cliente' => $codigo_cliente, 'loja_cliente' => $loja_cliente, 'id_prospect' => NULL), array('id_prospect' => $prospect->id));
				
				$this->session->set_userdata('id_orcamento_aguardando_transformacao_pedido', NULL);
				
				// se o prospect do orçamento aguardando transformação for o prospect atual, vamos redirecioná-lo para o orçamento em questão
				// se o prospect do orçamento aguardando transformação não for o prospect atual, é provável que o usuário desistiu de converter o prospect anterior
				if ($orcamento->id_prospect == $prospect->id)
				{
					redirect('pedidos/ver_detalhes/0/' . $orcamento->id);
				}
				else
				{
					redirect('clientes/ver_detalhes/' . $codigo_cliente . '/' . $loja_cliente);
				}
			}
			else
			{
				// pedidos/orçamentos
				$this->db->update('pedidos', array('codigo_cliente' => $codigo_cliente, 'loja_cliente' => $loja_cliente, 'id_prospect' => NULL), array('id_prospect' => $prospect->id));
				
				redirect('clientes/ver_detalhes/' . $codigo_cliente . '/' . $loja_cliente);
			}
		}
		else
		{
			redirect('prospects/ver_detalhes/' . $prospect->id);
		}
	}
	
	function relatorio()
	{
	
		$dia = $dia ? $dia : date('d');
		$mes = $mes ? $mes : date('m');
		$ano = $ano ? $ano : date('Y');
	
		if($_GET)
		{
			
			if(!$this->input->get('representantes'))
			{
				$erro .= "Selecione um Representante.";
			}
			else if (!$this->input->get('data_inicial') && !$this->_validar_data($this->input->get('data_inicial')))
			{
				$erro = 'Digite uma data inicial válida.';
			}
			else if (!$this->input->get('data_final') && !$this->_validar_data($this->input->get('data_final')))
			{
				$erro = 'Digite uma data final válida.';
			}
			else
			{

					// Representantes
					$representantes = $this->input->get('representantes');
					if($representantes)
					{
						$or_where = array();
						foreach($representantes as $representante)
						{
							//$this->db->or_where('codigo_usuario', $representante);
							
							$or_where[] = 'codigo_usuario = "' . $representante . '"';
						}
						
						
						$_or_where_implode = implode(' OR ', $or_where);
						$_or_where = '(' . $_or_where_implode . ')';
					}
					
					// Status
					if($this->input->get('status'))
					{
						if($this->input->get('status') != 'todos')
						{
							$this->db->where('status', $this->input->get('status'));
						}
					}
				
					//Tipo Pessoa
					if($this->input->get('tipo_pessoa'))
					{
						if($this->input->get('tipo_pessoa') != 'todos')
						{
							$this->db->where('tipo_pessoa', $this->input->get('tipo_pessoa'));
						}
					}
					
					
					//Cidades
					if($this->input->get('cidade'))
					{
						if($this->input->get('cidade') != 'todos')
						{
							$this->db->where('cidade', $this->input->get('cidade'));
						}
					}
					
					//Estados
					if($this->input->get('estado'))
					{
						if($this->input->get('estado') != 'todos')
						{
							$this->db->where('estado', $this->input->get('estado'));
						}
					}
					
					//DATA
					if ($this->input->get('data_inicial'))
					{
						$data_inicial = explode('/', $this->input->get('data_inicial'));
						$this->db->where('timestamp >=', mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]));
					}
					
					if ($this->input->get('data_final'))
					{
						$data_final = explode('/', $this->input->get('data_final'));
						$this->db->where('timestamp <=', mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]));
					}
					
					$this->db->where('status != "convertido_cliente"');
					$this->db->group_by('codigo_usuario');
					$prospects = $this->db->from("prospects")->where($_or_where)->get()->result();
					
					
					if($prospects )
					{
					
						$relatorio .= '<p><strong>Período: </strong> ' . $this->input->get('data_inicial') . ' - ' . $this->input->get('data_final') . '</p>';
						$relatorio .= '<p><strong>Status: </strong> ' . ucfirst($this->input->get('status')) . '</p>';
						$relatorio .= '<p><strong>Pessoa Física/Jurídica: </strong> ';
						if($this->input->get('tipo_pessoa') == 'pessoa_fisica')
						{
							$relatorio .= 'Pessoa Física';
						}
						else if($this->input->get('tipo_pessoa') == 'pessoa_juridica')
						{
							$relatorio .= 'Pessoa Jurídica';
						}
						else
						{
							$relatorio .= ucfirst($this->input->get('tipo_pessoa'));
						}
						
						$relatorio .= '</p>';
						
						$relatorio .= '<p><strong>Cidade: </strong> ' . ucfirst($this->input->get('cidade')) . '</p>';
						$relatorio .= '<p><strong>Estado: </strong> ' . ucfirst($this->input->get('estado')) . '</p>';
						$relatorio .= '<br />';
					
						$relatorio .= '<table border="1" class="hnordt" style="width:100%">';
					
						foreach($prospects as $prospect)
						{	
							
							// Representantes
							$representantes = $this->input->get('representantes');
							if($representantes)
							{
								$or_where = array();
								foreach($representantes as $representante)
								{
									//$this->db->or_where('codigo_usuario', $representante);
									
									$or_where[] = 'codigo_usuario = "' . $representante . '"';
								}
								
								
								$_or_where_implode = implode(' OR ', $or_where);
								$_or_where = '(' . $_or_where_implode . ')';
							}
							
							// Status
							if($this->input->get('status'))
							{
								if($this->input->get('status') != 'todos')
								{
									$this->db->where('status', $this->input->get('status'));
								}
							}
						
							//Tipo Pessoa
							if($this->input->get('tipo_pessoa'))
							{
								if($this->input->get('tipo_pessoa') != 'todos')
								{
									$this->db->where('tipo_pessoa', $this->input->get('tipo_pessoa'));
								}
							}
							
							
							//Cidades
							if($this->input->get('cidade'))
							{
								if($this->input->get('cidade') != 'todos')
								{
									$this->db->where('cidade', $this->input->get('cidade'));
								}
							}
							
							//Estados
							if($this->input->get('estado'))
							{
								if($this->input->get('estado') != 'todos')
								{
									$this->db->where('estado', $this->input->get('estado'));
								}
							}
							
							//DATA
							if ($this->input->get('data_inicial'))
							{
								$data_inicial = explode('/', $this->input->get('data_inicial'));
								$this->db->where('timestamp >=', mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]));
							}
							
							if ($this->input->get('data_final'))
							{
								$data_final = explode('/', $this->input->get('data_final'));
								$this->db->where('timestamp <=', mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]));
							}
							
							//-----------------------------
							
								
							$this->db->where('codigo_usuario', $prospect->codigo_usuario);
							$this->db->where('status != "convertido_cliente"');
							$_prospects = $this->db->from("prospects")->where($_or_where)->get()->result();
							
							//-----------------------
							
							
							
							//cabeçalho
							$relatorio .= '
							<thead>
								<tr>
									<td colspan="9" style="padding: 10px">
										<strong>Representante:</strong> ' . $this->db->from('usuarios')->where('codigo', $prospect->codigo_usuario)->get()->row()->nome . '
										<br />
										<strong>Total de Prospects:</strong> ' . count($_prospects) . '
									</td>
								</tr>
							';
							
							$total_geral += count($_prospects);
							
							
							
							
							$relatorio .= '<tr>';
								$relatorio .= '<th>Data de Cadastro</th>';
								//$relatorio .= '<th>Representante</th>';
								$relatorio .= '<th>Nome</th>';
								$relatorio .= '<th>Tipo</th>';
								//$relatorio .= '<th>Status</th>';
								$relatorio .= '<th>Cidade</th>';
								$relatorio .= '<th>Estado</th>';
								$relatorio .= '<th>Telefone 1</th>';
								$relatorio .= '<th>Telefone 2</th>';
								$relatorio .= '<th>E-mail</th>';
							$relatorio .= '</tr>';
							$relatorio .= '</thead>';
							
							//-----------------------
							
							foreach($_prospects as $_prospect)
							{
								$relatorio .= '<tr>';
								
								$relatorio .= '<td class="center">' . date('d/m/Y G:i:s', $_prospect->timestamp) . '</td>';
								
								//$relatorio .= '<td>' . $this->db->from('usuarios')->where('codigo', $_prospect->codigo_usuario)->get()->row()->nome . '</td>';
								
								$relatorio .= '<td>' . $_prospect->nome . '</td>';
								$relatorio .= '<td>' . ($_prospect->tipo_pessoa == 'pessoa_fisica' ? 'Pessoa Física' : 'Pessoa Jurídica') . '</td>';
								//$relatorio .= '<td>' . $_prospect->status . '</td>';
								$relatorio .= '<td>' . $_prospect->cidade . '</td>';
								$relatorio .= '<td class="center">' . $_prospect->estado . '</td>';
								$relatorio .= '<td class="center">' . $_prospect->telefone_contato_1 . '</td>';
								$relatorio .= '<td class="center">' . $_prospect->telefone_contato_2 . '</td>';
								$relatorio .= '<td>' . $_prospect->email_contato_1. '</td>';
								
								
								$relatorio .= '</tr>';	
							}
							
							
						}
						
						$relatorio .= '<tr><td colspan="9"><br /></td></tr><tfoot><tr><td colspan="9"> Total Geral de Prospects: ' . $total_geral . '</td></tr></tfoot>';
						
						$relatorio .= '</table>';
						
					}
					else
					{
						$relatorio .= "Nenhum registro encontrado";
					}
					
					
			
			}
			
		}
		
		$conteudo .= '
			<div class="caixa">
				<ul>
					<li>' . anchor('#', 'Imprimir', 'onclick="print(); return false;"') . '</li>
				</ul>
			</div>
		';
		
		$conteudo .= heading('Relatóro de Prospecção', 2);
		
		
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		$conteudo .= '<br />';
		
		//$conteudo .= form_open(current_url(), 'class="nao_exibir_impressao"');
		$conteudo .= form_open(current_url(), array('method' => 'get'));
		
		$conteudo .= '<p>Representantes:' . br() . form_dropdown('representantes[]', $this->_obter_representantes(FALSE), $this->input->get('representantes'), ' multiple="multiple" class="multi_select" style="width: 490px;" ') . '</p>';
		
		$conteudo .= '<p style="float:left; margin-right: 20px">Status: ' . br() . form_dropdown('status', array('todos' => 'Todos', 'ativo' => 'Ativo', 'inativo' => 'Inativo'), $this->input->get('status')) . '</p>';
		
		//$conteudo .= '<p style="float:left; margin-right: 20px">Status:' . br() . form_dropdown('status', array('Prospects' => 'Prospects', 'Convertidos para Clientes' => 'Convertidos para Clientes'), $this->input->get('status')) . '</p>';
		
		$conteudo .= '<p style="float:left; margin-right: 20px">Pessoa Física/Jurídica: ' . br() . form_dropdown('tipo_pessoa', array('todos' => 'Todos', 'pessoa_fisica' => 'Pessoa Física', 'pessoa_juridica' => 'Pessoa Jurídica'), $this->input->get('tipo_pessoa')) . '</p>';
		
		$conteudo .= '<p style="float:left; margin-right: 20px">Cidade: ' . br() . form_dropdown('cidade', $this->_obter_cidades(), $this->input->get('cidade')) . '</p>';
		
		$conteudo .= '<p>Estado' . br() . form_dropdown('estado', $this->_obter_estados_prospects(), $this->input->get('estado')) . '</p>';
		
		$conteudo .= '<p style="margin-right: 10px;">' . form_label('Data: ' . br() . form_input('data_inicial', $this->input->get('data_inicial'), 'class="datepicker data_inicial" size="6"')) . ' à ' . form_input('data_final', $this->input->get('data_final'), 'class="datepicker" size="6"') . '</p>';

		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= '<p>' . form_submit('gerar_relatorio', 'Gerar Relatório') . '</p>';
		$conteudo .= form_close();
		
		
		$conteudo .= '
		<script type="text/javascript">
			$(".multi_select").multiselect({
				checkAllText: "Marcar Todos",
				uncheckAllText: "Desmarcar Todos",
				noneSelectedText: "Selecione as opções",
				selectedText: "# Selecionado(s)"
			}).multiselectfilter({
				width: 130,
				label: "Filtrar",
				placeholder: "Digite palavras-chave"
			});
		</script>
		';

		
		
		$conteudo .= '<br /><br />';
		$conteudo .= $relatorio;
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	
	
	//------------------------------
	//------------------------------
	
	function _obter_status()
	{
		return array('ativo' => 'Ativo', 'inativo' => 'Inativo');
	}
	
	function _obter_tipos_pessoa()
	{
		return array('pessoa_fisica' => 'Pessoa Física', 'pessoa_juridica' => 'Pessoa Jurídica');
	}
	
	function _obter_representantes($todos = TRUE)
	{
		$representantes = $this->db->from('usuarios')->where('status', 'ativo')->where('grupo', 'representantes')->order_by('nome', 'asc')->get()->result();
		
		if($todos == TRUE)
		{
			$_representantes = array(0 => 'Todos');
		}
		
		foreach ($representantes as $representante)
		{
			$_representantes[$representante->codigo] = $representante->codigo . ' - ' . $representante->nome;
		}
		
		return $_representantes;
	}
	
	/*
	function _obter_usuarios()
	{
		$usuarios = $this->db->from('usuarios')->where('status', 'ativo')->order_by('grupo', 'desc')->order_by('nome', 'asc')->get()->result();
		$_usuarios = array();
		
		foreach ($usuarios as $usuario)
		{
			$_usuarios[$usuario->id] = $usuario->nome . ' (grupo ' . element($usuario->grupo, $this->_obter_grupos()) . ')';
		}
		
		return $_usuarios;
	}
	*/
	
	function _obter_usuarios($id_usuario = NULL)
	{
		$usuarios = $this->db->from('usuarios')->where('status', 'ativo')->order_by('grupo', 'desc')->order_by('nome', 'asc')->get()->result();
		$_usuarios = array();
		
		foreach ($usuarios as $usuario)
		{
			if($id_usuario)
			{
		
				if($id_usuario == $usuario->id)
				{
					$_usuarios[$usuario->id] = $usuario->nome . ' (Para Mim)';
				}
				else
				{
					$_usuarios[$usuario->id] = $usuario->nome . ' (grupo ' . element($usuario->grupo, $this->_obter_grupos()) . ')';
				}
			
			}
			else
			{
				$_usuarios[$usuario->id] = $usuario->nome . ' (grupo ' . element($usuario->grupo, $this->_obter_grupos()) . ')';
			}
		}
		
		return $_usuarios;
	}
	
	function _obter_cidades()
	{
		$this->db->group_by("cidade"); 
	
		$cidades = $this->db->from('prospects')->where('cidade != "" AND codigo_usuario != "representantes"')->get()->result();
		$_cidades = array();
		
		$_cidades['todos'] = 'Todos';
		
		foreach($cidades as $cidade)
		{
			$_cidades[$cidade->cidade] = $cidade->cidade;
		}
		
		return $_cidades;
		
	}
	
	function _obter_estados_prospects()
	{
		$this->db->group_by('estado');
		
		$estados = $this->db->from('prospects')->where('estado != "" AND codigo_usuario != "representantes"')->get()->result();
		
		$_estados = array();
		
		$_estados['todos'] = 'Todos';
		
		foreach($estados as $estado)
		{
			$_estados[$estado->estado] = $estado->estado;
		}
		
		return $_estados; 
	
	}
	
}