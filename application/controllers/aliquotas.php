<?php

class Aliquotas extends MY_Controller {
	
	public function index()
	{
		$filtros = array(
			array(
				'nome' => 'origem',
				'descricao' => 'Origem',
				'tipo' => 'opcoes',
				'opcoes' => $this->_obter_estados(TRUE),
				'campo_mysql' => 'origem',
				'ordenar' => 0
			),
			array(
				'nome' => 'destino',
				'descricao' => 'Destino',
				'tipo' => 'opcoes',
				'opcoes' => $this->_obter_estados(TRUE),
				'campo_mysql' => 'destino',
				'ordenar' => 1
			),
			array(
				'nome' => 'procedencia',
				'descricao' => 'Procedência',
				'tipo' => 'opcoes',
				'opcoes' => $this->lista_procedencias(TRUE),
				'campo_mysql' => 'procedencia',
			),
		);
		
		if($_GET['my_submit']){
			$this->filtragem_mysql($filtros);
			
			$aliquotas = $this->db->from('alicota')->order_by('origem, destino')->limit(20, $this->input->get('per_page'))->get()->result();
			
			$this->filtragem_mysql($filtros);
			
			$total = $this->db->from('alicota')->get()->num_rows();
			
			$paginacao = $this->paginacao($total);
		}
		
		$this->load->view('layout', array('conteudo' => $this->load->view('aliquotas/index', array(
			'aliquotas' => $aliquotas,
			'estados' => $this->_obter_estados(),
			'procedencias' => $this->lista_procedencias(),
			'total' => $total,
			'filtragem' => $this->filtragem($filtros),
			'paginacao' => $this->paginacao($total)
		), TRUE)));
	}
	
	/**
	* Função: criar()
	* Objetivo: Cria ou editar uma alíquota ja existente
	* Param:
	* 	$edicao = Código da alíquota a ser editada
	*/
	public function criar($edicao = FALSE){
		
		if($this->input->post()){
			$dados = $this->input->post();
			
			$dados['alicota'] = floatval(str_replace(',', '.', $dados['alicota']));
			
			if(!$dados['origem']){
				$erro = 'Favor informar o estado de origem.';
			}else if(!$dados['destino']){
				$erro = 'Favor informar o estado de destino.';
			}else if(!$dados['alicota']){
				$erro = 'Favor informar o valor da alíquota.';
			}else if(!$dados['procedencia']){
				$erro = 'Favor informar a procedência.';
			}else{
				//Se não for uma edição verificar se a aliquota ja esta cadastrada
				if(!$edicao){
					$valida = $this->valida_aliquota(array('origem' => $dados['origem'], 'destino' => $dados['destino'], 'procedencia' => $dados['procedencia']));
					
					if($valida){
						$erro = 'Alíquota já cadastrada para este estado.';
					}
				//Se for uma edição atualiza os valores
				}else{
					$this->gravar($edicao, $dados);
				}
				
				//Se não for uma edição e não possuir erros grava a aliquota
				if(!$erro){
					$this->gravar(FALSE, $dados);
				}
			}
		}
		
		if($edicao){
			$aliquota = $this->db->from('alicota')->where('id', $edicao)->get()->row();
		}
		
		$this->load->view('layout', array('conteudo' => $this->load->view('aliquotas/criar', array(
			'erro' => $erro,
			'edicao' => $edicao,
			'aliquota' => $aliquota,
			'estados' => $this->_obter_estados(),
			'procedencias' => $this->lista_procedencias(),
		), TRUE)));
	}
	
	/**
	* Função: valida_aliquota()
	* Objetivo: Verifica se a aliquota ja existe no banco
	* Param:
	* 	$filtros = Array dos dados a serem filtrados array('campo_mysql' => 'valor')
	*/
	private function valida_aliquota($filtros){
		foreach($filtros as $campo => $valor){
			$this->db->where($campo, $valor);
		}
		
		$valida = $this->db->from('alicota')->get()->result();
		
		return $valida;
	}
	
	/**
	* Função: lista_procedencias()
	* Objetivo: Lista a opção de procedência (Nacional/Importado)
	* Param:
	* 	$todos = Retorna a opção todos para exibir na filtragem
	*/
	private function lista_procedencias($todos = FALSE){
		if($todos){
			return array('todos' => 'Todos', 'nacional' => 'Nacional', 'importado' => 'Importado');
		}else{
			return array('nacional' => 'Nacional', 'importado' => 'Importado');
		}
	}
	
	/**
	* Função: gravar()
	* Objetivo: Grava os dados de uma alíquota nova ou realiza a edição de uma existente
	* Param:
	* 	$id_aliquota = Código da alíquota caso for uma edição
	* 	$dados = Dados a serem inseridos/atualizados no banco
	*/
	private function gravar($id_aliquota, $dados){
		if(!$dados){
			redirect();
		}
		
		if($id_aliquota){ //Edição
			$this->db->where('id', $id_aliquota)->update('alicota', $dados);
		}else{
			$this->db->insert('alicota', $dados);
		}
		
		redirect('aliquotas');
	}
}