<?php

class Usuarios extends MY_Controller {
	
	function index()
	{
		$filtros = array(
			array(
				'nome' => 'codigo',
				'descricao' => 'Código',
				'tipo' => 'texto',
				'campo_mysql' => 'codigo',
				'ordenar' => 0
			),
			array(
				'nome' => 'grupo',
				'descricao' => 'Grupo',
				'tipo' => 'opcoes',
				'opcoes' => array_merge(array('todos' => 'Todos'), $this->_obter_grupos_usuarios()),
				'campo_mysql' => 'grupo',
				'ordenar' => 1
			),
			array(
				'nome' => 'nome',
				'descricao' => 'Nome',
				'tipo' => 'texto',
				'campo_mysql' => 'nome',
				'ordenar' => 2
			),
			array(
				'nome' => 'criacao',
				'descricao' => 'Criação',
				'tipo' => 'data',
				'campo_mysql' => 'timestamp',
				'ordenar' => 3
			),
			array(
				'nome' => 'status',
				'descricao' => 'Status',
				'tipo' => 'opcoes',
				'opcoes' => array_merge(array('todos' => 'Todos'), $this->_obter_status()),
				'campo_mysql' => 'status',
				'ordenar' => 1
			)
		);
		if($_GET['my_submit']){
			$this->filtragem_mysql($filtros);
			
			$usuarios = $this->db->from('usuarios')->get()->result();
			
			$this->filtragem_mysql($filtros);
			
			$total = $this->db->from('usuarios')->get()->num_rows();
			
			$paginacao = $this->paginacao($total);
			
			// por algum motivo o campo codigo de gestores é preenchido, vamos fazer uma correção
			foreach ($usuarios as $usuario)
			{
				if (!in_array($usuario->grupo, array('representantes', 'prepostos')))
				{
					unset($usuario->codigo);
				}
			}
		}	
		$this->load->view('layout', array('conteudo' => $this->load->view('usuarios/index', array('usuarios' => $usuarios, 'grupos' => $this->_obter_grupos_usuarios(), 'statuses' => $this->_obter_status(), 'filtragem' => $this->filtragem($filtros), 'paginacao' => $paginacao, 'total' => $total), TRUE)));
	}
	
	function liberarAcesso($id = NULL)
	{
		$this->db->update('usuarios', array(
			'id_sessao' => '',
			'logado' => ''
		), array('id' => $id));
		
		redirect('/usuarios');
	}
	
	function criar() {
		if ($_POST && !$_POST['somente_postar'])
		{
	
			if (in_array($this->input->post('grupo'), array('representantes', 'prepostos')) && !$this->input->post('codigo'))
			{
				$erro = 'Selecione um representante.';
			}
			else if (in_array($this->input->post('grupo'), array('revendas')) && !$this->input->post('codigo'))
			{
				$erro = 'Selecione uma revenda.';
			}
			else if (in_array($this->input->post('grupo'), array('vendedores')) && !$this->input->post('id_filial'))
			{
				$erro = 'Selecione uma filial.';
			}
			else if (!$this->input->post('nome'))
			{
				$erro = 'Digite um nome.';
			}
			else if ($this->db->from('usuarios')->where('nome', $this->input->post('nome'))->get()->row())
			{
				$erro = 'O nome digitado já existe.';
			}
			else if (strlen($this->input->post('senha')) < 4)
			{
				$erro = 'A senha deve conter no mínimo 4 caracteres.';
			}
			
			// ** informações cadastrais
			
			else if (!$this->input->post('nome_real'))
			{
				$erro = 'Digite o nome do usuário (nome completo).';
			}
			else if ($this->input->post('cpf') && !$this->_validar_cpf($this->input->post('cpf')))
			{
				$erro = 'Digite um CPF válido.';
			}
			else if ($this->input->post('cep') && !$this->_validar_cep($this->input->post('cep')))
			{
				$erro = 'Digite um CEP válido.';
			}
			else if ($this->input->post('telefone_1') && !$this->_validar_telefone($this->input->post('telefone_1')))
			{
				$erro = 'Digite um telefone válido para o campo Telefone 1.';
			}
			else if ($this->input->post('telefone_2') && !$this->_validar_telefone($this->input->post('telefone_2')))
			{
				$erro = 'Digite um telefone válido para o campo Telefone 2.';
			}
			else if ($this->input->post('email') && !valid_email($this->input->post('email')))
			{
				$erro = 'Digite um e-mail válido.';
			}
			else if ($this->input->post('hora_entrada') == '')
			{
				$erro = 'Digite um horário de entrada válido.';
			}
			else if ($this->input->post('hora_saida') == '')
			{
				$erro = 'Digite um horário de saída válido.';
			}
			
			// informações cadastrais **
			
			else
			{
			
				if (!$this->input->post('grupo') == 'representante')
				{
					$_POST['codigo'] = '';
				}
			
				$this->db->insert('usuarios', array(
					'timestamp' => time(),
					'id_filial' => ($this->input->post('grupo') == 'vendedores') ? $this->input->post('id_filial') : NULL,
					'status' => 'ativo',
					'grupo' => $this->input->post('grupo'),
					'codigo' => $this->input->post('codigo'),
					'nome' => $this->input->post('nome'),
					'senha' => $this->input->post('senha'),
					
					// ** informações cadastrais
					
					'nome_real' =>  $this->input->post('nome_real'),
					'cpf' =>  $this->input->post('cpf'),
					'rg' =>  $this->input->post('rg'),
					'cep' =>  $this->input->post('cep'),
					'endereco' =>  $this->input->post('endereco'),
					'numero' =>  $this->input->post('numero'),
					'bairro' =>  $this->input->post('bairro'),
					'cidade' =>  $this->input->post('cidade'),
					'estado' =>  $this->input->post('estado'),
					'telefone_1' =>  $this->input->post('telefone_1'),
					'telefone_2' =>  $this->input->post('telefone_2'),
					'email' =>  $this->input->post('email'),
					//custom
					'hora_entrada' =>  $this->input->post('hora_entrada'),
					'hora_saida' =>  $this->input->post('hora_saida'),
					//fim custom
					'tabela_precos' => serialize($this->input->post('tabela_precos'))
					
					// informações cadastrais **
				));
				
				$id_usuario = $this->db->insert_id();
				
				$this->salvar_macs($id_usuario);				
				
				if ($this->input->post('grupo') == 'supervisores')
				{
					if ($this->input->post('codigos_representantes'))
					{
						foreach ($this->input->post('codigos_representantes') as $codigo_representante)
						{
							$this->db->insert('representantes_supervisores', array(
								'id_supervisor' => $id_usuario,
								'codigo_representante' => $codigo_representante
							));
						}
					}
				}
				else if ($this->input->post('grupo') == 'revendas')
				{
					if (!$this->db->from('filiais')->where(array('codigo_revenda' => $this->input->post('codigo'), 'nome' => 'Matriz'))->get()->row()) {
						$this->db->insert('filiais', array(
							'timestamp' => time(),
							'status' => 'ativa',
							'codigo_revenda' => $this->input->post('codigo'),
							'nome' => 'Matriz'
						));
					}
				}
				
				redirect('usuarios');
			}
		}
		
		$representantes = $this->_obter_representantes($this->input->post('grupo') == 'vendedores' ? TRUE : FALSE);
		$_representantes = array_keys($representantes);
		
		$conteudo .= '
			<div class="caixa">
				<ul>
					<li>' . anchor('usuarios', 'Voltar', 'onclick="history.go(-1); return false;"') . '</li>
				</ul>
			</div>
		';
		
		$conteudo .= heading('Cadastrar Usuário', 2);
		
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		
		$conteudo .= form_open(current_url());
		
		$conteudo .= '<input type="hidden" name="somente_postar" value="" />';
		
		$conteudo .= '<p>' . form_label('Grupo:' . br() . form_dropdown('grupo', $this->_obter_grupos_usuarios(), $this->input->post('grupo'))) . '</p>';
		
		$conteudo .= '<p id="codigo_representante" style="display: none; float: left; margin-right: 10px;">' . form_label('<span id="descricao_codigo_representante"></span>:' . br() . form_dropdown('codigo', $representantes, $this->input->post('codigo'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false" onChange="representante_alterado"')) . '</p>';
		
		$conteudo .= '<p id="id_filial" style="display: none; float: left; margin-right: 10px;">' . form_label('Filial:' . br() . form_dropdown('id_filial', $this->_obter_filiais($this->input->post('codigo') ? $this->input->post('codigo') : $_representantes[0]), $this->input->post('id_filial'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false"')) . '</p>';
		
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Nome de usuário:' . br() . form_input('nome', $this->input->post('nome'), 'size="40"')) . '</p>';
		
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Senha:' . br() . form_input('senha', $this->input->post('senha'))) . '</p>';
		
		//custom
		
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Hora Entrada:' . br() . form_input('hora_entrada', $this->input->post('hora_entrada'), 'size="40" id="hora_entrada" alt="horas"')) . '</p>';
		
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Hora Saída:' . br() . form_input('hora_saida', $this->input->post('hora_saida'), 'size="40" id="hora_saida" alt="horas"')) . '</p>';
		
		$conteudo .= '<script type="text/javascript"> $.mask.masks.horas = {mask: "99:99:99"}; </script>';

		//fim custom
		
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= '<p style="vertical-align:middle; float:left"; margin-right:10px;" '
						.form_label('Mac Address: <br/>'. form_input('mac','','id="txt_mac"')).' <br /> '.form_label('Serial HD: <br/>'. form_input('serial_hd','','id="txt_serial"')).'
						<br /><input style="height:22px" type="submit" id="adicionar_mac" name="Adicionar" value="Adicionar" /></p>';
		
		$conteudo .= '<div style="clear: both;"></div> 	<br/>';
		$conteudo .= '<div id="div_mac">';
		$conteudo .= $this->montar_tabela_mac();
		$conteudo .= '</div>';
		
		
		$conteudo .= '<p id="codigos_representantes">' . form_label('Representantes:' . br() . form_dropdown('codigos_representantes[]', $this->_obter_representantes(NULL, TRUE), $this->input->post('codigos_representantes'), 'class="multiselect" multiple="multiple" style="height: 240px; width: 800px;"')) . '</p>';
		
		$conteudo .= '
			<script type="text/javascript">
				$(".multiselect").multiselect({
					checkAllText: "Marcar Todos",
					uncheckAllText: "Desmarcar Todos",
					noneSelectedText: "Selecione as opções",
					selectedText: "# Selecionado(s)"
				}).multiselectfilter({
					width: 130,
					label: "Filtrar",
					placeholder: "Digite palavras-chave"
				});
			</script>
		';
		
		// ** informações cadastrais
		
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '<h3>Informações Cadastrais</h3>';
		
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Nome completo:' . br() . form_input('nome_real', $this->input->post('nome_real'), 'size="40"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('CPF:' . br() . form_input('cpf', $this->input->post('cpf'), 'alt="cpf" size="10"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('RG:' . br() . form_input('rg', $this->input->post('rg'), 'size="10"')) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= heading('Endereço', 3);
		$conteudo .= '<div dojoType="dijit.Tooltip" connectId="cep" position="below">Ao digitar o CEP os campos Endereço, Bairro, Cidade e Estado serão preenchidos automaticamente.</div>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('CEP:' . br() . form_input('cep', $this->input->post('cep'), 'id="cep" alt="cep" size="5"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Endereço:' . br() . form_input('endereco', $this->input->post('endereco'), 'size="53"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Nº:' . br() . form_input('numero', $this->input->post('numero'), 'size="1"')) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Bairro:' . br() . form_input('bairro', $this->input->post('bairro'), 'size="20"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Cidade:' . br() . form_input('cidade', $this->input->post('cidade'), 'size="20"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Estado:' . br() . form_dropdown('estado', $this->_obter_estados(), $this->input->post('estado'))) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= heading('Contato', 3);
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Telefone 1:' . br() . form_input('telefone_1', $this->input->post('telefone_1'), 'alt="phone" size="10"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Telefone 2:' . br() . form_input('telefone_2', $this->input->post('telefone_2'), 'alt="phone" size="10"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('E-mail:' . br() . form_input('email', $this->input->post('email'), 'size="40"')) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';
		
		
		
		// Tabela de Preços
		if($this->input->post("grupo") == "representantes")
		{
			
			$conteudo .= heading('Tabela de Preços', 3);
			$tabelas_precos = $this->db_cliente->obter_tabelas_precos();
			
			foreach($tabelas_precos as $tabela_precos)
			{
				
				$conteudo .= '<p>' . form_checkbox('tabela_precos[' . $tabela_precos["codigo"] . ']', $tabela_precos["codigo"]) . ' Cod. ' . $tabela_precos["codigo"] . ' - ' . $tabela_precos["descricao"] . '</p>';
				
			}
			
		}
		
		
		// informações cadastrais **
		
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '<div style="border-top: 1px solid #DDD; margin-top: 25px;"></div>';
		
		$conteudo .= '<p>' . form_submit('concluir', 'Concluir') . ' ou ' . anchor('usuarios', 'Cancelar') . '</p>';
		
		$conteudo .= form_close();
		
		$conteudo .= '
			<script type="text/javascript">
			
				
			
				function is_numero(code) {
					var retorno = false;
					if ((code >=48 && code <=57) || (code>= 96 && code <=105)) {
						return true;
					}
					return false;
				}
				$(document).ready(function() {
					verificar_grupo();
					
					$("select[name=grupo]").change(function() { verificar_grupo(true); });
				});
				$("#txt_mac").live("keydown",function(event){
					var code = event.keyCode;
					
					
					if (event.ctrlKey) {
						return true;
					} 
					if (code == 9) { // tab
						return true;
					}
					if (code == 8 || code == 46) { // se for uma ação de apagar
						return true;
					}
					if (code >= 37 && code <= 40) { // se for uma seta
						return true;
					}
					var tamanho = $(this).val() != null ? $(this).val().length : 0;
					if (tamanho == 12) {
						return false;
					}
					/*
					* O mac é constituido por um valor hexadecimal, ou seja
					* ele contempla números e letras de "A" até "F", então por isso esta validação
					*
					*/
					if (is_numero(code) || (code >= 65 && code <=70)) {
						// code 65 = "A" e code 70 = "F"
						return true;							
					}
					
					
					
					return false;
				});
				$("#adicionar_mac").click(function(){
					var mac_usuario = $("#txt_mac").val();
					var serial_usuario = $("#txt_serial").val();
					if (mac_usuario == null || (mac_usuario.length > 0  && mac_usuario.length < 12)) {
						alert("O MAC ADDRESS deve conter 12 caracteres, com números e letras de A até F");
						return false;
					}
					if(mac_usuario.length == 0 && serial_usuario.length == 0){
						alert("Você deve preencher o Mac Address ou o Serial HD.");
						return false;
					}					
					$.ajax({
						url: "'.site_url('usuarios/adicionar_mac').'",
						method : \'GET\',						
						dataType : "json",
						data: {mac : mac_usuario, hd : serial_usuario},
						success: function (data) {							
							if (data.retorno == "ok") {
								location.reload(true);
								
								//$("#div_mac").html(data.html);
							} else {											
								alert(data.erro);
							}
						}
						
					});					
					return false;
				}); 
				$(".excluir_mac").click(function(){
					var key = $(this).attr("href");
					
					$.ajax({
						url: "'.site_url('usuarios/excluir_mac').'",
						method : \'GET\',						
						dataType : "json",
						data: {key : key},
						success: function (data) {
							if (data.retorno == "ok") {
								location.reload(true);
							} else {
								alert(data.erro);
							}
							
						}
					});
					return false;
				});
				function verificar_grupo(submit) {
					submit = submit ? submit : false;
					
					if ($("select[name=grupo]").val() == "supervisores")
					{
						$("#codigo_representante").show();
						$("#codigos_representantes").show();
						$("#descricao_codigo_representante").text("Representante");
						$("#id_filial").hide();
					}
					else if ($("select[name=grupo]").val() == "representantes" || $("select[name=grupo]").val() == "prepostos")
					{
						$("#codigo_representante").show();
						$("#codigos_representantes").hide();
						$("#descricao_codigo_representante").text("Representante");
						$("#id_filial").hide();
					}
					else if ($("select[name=grupo]").val() == "revendas")
					{
						$("#codigo_representante").show();
						$("#codigos_representantes").hide();
						$("#descricao_codigo_representante").text("Revenda");
						$("#id_filial").hide();
					}
					else if ($("select[name=grupo]").val() == "revendas" || $("select[name=grupo]").val() == "vendedores")
					{
						$("#codigo_representante").show();
						$("#codigos_representantes").hide();
						$("#descricao_codigo_representante").text("Revenda");
						$("#id_filial").show();
					}
					else
					{
						$("#codigo_representante").hide();
						$("#codigos_representantes").hide();
						$("#id_filial").hide();
					}
					
					/*if (submit) {
						$("input[name=somente_postar]").val("1");
						$("form").submit();
					}*/
				}
				
				function representante_alterado()
				{
					// atualizar filiais
					if ($("select[name=grupo]").val() == "vendedores")
					{
						$("input[name=somente_postar]").val("1");
						$("form").submit();
					}
				}
			</script>
		';
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function editar($id = NULL)
	{
		if (!$id)
		{
			redirect();	
		}
		
		$this->popular_mac_sessao($id);
		$usuario = $this->db->from('usuarios')->where('id', $id)->get()->row();
		
		$nome_representante = $this->db_cliente->obter_nome_representante($usuario->codigo);
		
		if ($_POST)
		{
			if (!$_POST['somente_postar']) {
				if (in_array($this->input->post('grupo'), array('representantes', 'prepostos')) && !$this->input->post('codigo'))
				{
					$erro = 'Selecione um representante.';
				}
				else if (in_array($this->input->post('grupo'), array('revendas')) && !$this->input->post('codigo'))
				{
					$erro = 'Selecione uma revenda.';
				}
				else if (in_array($this->input->post('grupo'), array('vendedores')) && !$this->input->post('id_filial'))
				{
					$erro = 'Selecione uma filial.';
				}
				else if (!$this->input->post('nome'))
				{
					$erro = 'Digite um nome.';
				}
				else if ($this->db->from('usuarios')->where(array('id !=' => $id, 'nome' => $this->input->post('nome')))->get()->row())
				{
					$erro = 'O nome digitado já existe.';
				}
				else if (strlen($this->input->post('senha')) < 4)
				{
					$erro = 'A senha deve conter no mínimo 4 caracteres.';
				}
				
				// ** informações cadastrais
				
				else if (!$this->input->post('nome_real'))
				{
					$erro = 'Digite o nome do usuário (nome completo).';
				}
				else if ($this->input->post('cpf') && !$this->_validar_cpf($this->input->post('cpf')))
				{
					$erro = 'Digite um CPF válido.';
				}
				else if ($this->input->post('cep') && !$this->_validar_cep($this->input->post('cep')))
				{
					$erro = 'Digite um CEP válido.';
				}
				else if ($this->input->post('telefone_1') && !$this->_validar_telefone($this->input->post('telefone_1')))
				{
					$erro = 'Digite um telefone válido para o campo Telefone 1.';
				}
				else if ($this->input->post('telefone_2') && !$this->_validar_telefone($this->input->post('telefone_2')))
				{
					$erro = 'Digite um telefone válido para o campo Telefone 2.';
				}
				else if ($this->input->post('email') && !valid_email($this->input->post('email')))
				{
					$erro = 'Digite um e-mail válido.';
				}
				else if ($this->input->post('hora_entrada') == '')
				{
					$erro = 'Digite um horário de entrada válido.';
				}
				else if ($this->input->post('hora_saida') == '')
				{
					$erro = 'Digite um horário de saída válido.';
				}
				
				// informações cadastrais **
				
				else
				{
					/* Removido para permitir que o supervisor possa atribuir um código de representante
					if ($this->input->post('grupo') == 'supervisores')
					{
						$_POST['codigo'] = '';
					}*/
					
					$this->db->update('usuarios', array(
						'id_filial' => ($this->input->post('grupo') == 'vendedores') ? $this->input->post('id_filial') : NULL,
						'status' => $this->input->post('status'),
						'grupo' => $this->input->post('grupo'),
						'codigo' => $this->input->post('codigo'),
						'nome' => $this->input->post('nome'),
						'senha' => $this->input->post('senha'),
						
						// ** informações cadastrais
						
						'nome_real' =>  $this->input->post('nome_real'),
						'cpf' =>  $this->input->post('cpf'),
						'rg' =>  $this->input->post('rg'),
						'cep' =>  $this->input->post('cep'),
						'endereco' =>  $this->input->post('endereco'),
						'numero' =>  $this->input->post('numero'),
						'bairro' =>  $this->input->post('bairro'),
						'cidade' =>  $this->input->post('cidade'),
						'estado' =>  $this->input->post('estado'),
						'telefone_1' =>  $this->input->post('telefone_1'),
						'telefone_2' =>  $this->input->post('telefone_2'),
						'email' =>  $this->input->post('email'),
						//custom
						'hora_entrada' =>  $this->input->post('hora_entrada'),
						'hora_saida' =>  $this->input->post('hora_saida'),
						//fim custom
						'tabela_precos' => serialize($this->input->post('tabela_precos'))
						
						// informações cadastrais **
						
					), array('id' => $id));
					$this->salvar_macs($id, true);
					if ($this->input->post('grupo') == 'supervisores')
					{
						$this->db->delete('representantes_supervisores', array('id_supervisor' => $id));
						
						if ($this->input->post('codigos_representantes'))
						{
							foreach ($this->input->post('codigos_representantes') as $codigo_representante)
							{
								$this->db->insert('representantes_supervisores', array(
									'id_supervisor' => $id,
									'codigo_representante' => $codigo_representante
								));
							}
						}
					}
					
					redirect('usuarios');
				}
			}
		}
		else
		{
			foreach ($usuario as $indice => $valor)
			{
				$_POST[$indice] = $valor;
			}
			
			if ($this->input->post('grupo') == 'supervisores')
			{
				$representantes_supervisores = $this->db->from('representantes_supervisores')->where('id_supervisor', $id)->get()->result();
				
				if ($representantes_supervisores)
				{
					foreach ($representantes_supervisores as $representante_supervisor)
					{
						$_POST['codigos_representantes'][] = $representante_supervisor->codigo_representante;
					}
				}
			}
		}
		// VALIDAR FORM **
		
		$representantes = $this->_obter_representantes($this->input->post('grupo') == 'vendedores' ? TRUE : FALSE);
		$_representantes = array_keys($representantes);
		
		$conteudo .= '
			<div class="caixa">
				<ul>
					<li>' . anchor('usuarios', 'Voltar', 'onclick="history.go(-1); return false;"') . '</li>
				</ul>
			</div>
		';
		
		// ** EXIBIR FORM
		$conteudo .= heading('Editar Usuário', 2);
		
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		
		$conteudo .= form_open(current_url());
		
		$conteudo .= '<input type="hidden" name="somente_postar" value="" />';
		
		$conteudo .= '<p>' . form_label('Status:' . br() . form_dropdown('status', $this->_obter_status(), $this->input->post('status'))) . '</p>';
		
		$conteudo .= '<p>' . form_label('Grupo:' . br() . form_dropdown('grupo', $this->_obter_grupos_usuarios(), $this->input->post('grupo'))) . '</p>';
		
		$conteudo .= '<p id="codigo_representante" style="display: none; float: left; margin-right: 10px;">' . form_label('<span id="descricao_codigo_representante"></span>:' . br() . form_dropdown('codigo', $representantes, $this->input->post('codigo'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false"')) . '</p>';
		
		$conteudo .= '<p id="id_filial" style="display: none; float: left; margin-right: 10px;">' . form_label('Filial:' . br() . form_dropdown('id_filial', $this->_obter_filiais($this->input->post('codigo') ? $this->input->post('codigo') : $_representantes[0]), $this->input->post('id_filial'), 'dojoType="dijit.form.FilteringSelect" queryExpr="*${0}*" autoComplete="false"')) . '</p>';
		
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Nome de usuário:' . br() . form_input('nome', $this->input->post('nome'), 'size="40"')) . '</p>';
		
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Senha:' . br() . form_input('senha', $this->input->post('senha'))) . '</p>';
		
		//custom
		
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Hora Entrada:' . br() . form_input('hora_entrada', $this->input->post('hora_entrada'), 'size="40" id="hora_entrada" alt="horas"')) . '</p>';
		
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Hora Saída:' . br() . form_input('hora_saida', $this->input->post('hora_saida'), 'size="40" id="hora_saida" alt="horas"')) . '</p>';
		
		$conteudo .= '<script type="text/javascript"> $.mask.masks.horas = {mask: "99:99:99"}; </script>';

		//fim custom
		
		//TODO AQUI
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= '<p style="vertical-align:middle; float:left"; margin-right:10px;" '
						.form_label('Mac Address: <br/>'. form_input('mac','','id="txt_mac"')).' <br /> '.form_label('Serial HD: <br/>'. form_input('serial_hd','','id="txt_serial"')).'
						<br /><input style="height:22px" type="submit" id="adicionar_mac" name="Adicionar" value="Adicionar" /></p>';
		
		$conteudo .= '<div style="clear: both;"></div> 	<br/>';
		$conteudo .= '<div id="div_mac">';		
		$conteudo .= $this->montar_tabela_mac($id);
		$conteudo .= '</div>';
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= '<p id="codigos_representantes">' . form_label('Representantes:' . br() . form_dropdown('codigos_representantes[]', $this->_obter_representantes(NULL, TRUE), $this->input->post('codigos_representantes'), 'class="multiselect" multiple="multiple" style="height: 240px; width: 800px;"')) . '</p>';
		
		$conteudo .= '
			<script type="text/javascript">
				$(".multiselect").multiselect({
					checkAllText: "Marcar Todos",
					uncheckAllText: "Desmarcar Todos",
					noneSelectedText: "Selecione as opções",
					selectedText: "# Selecionado(s)"
				}).multiselectfilter({
					width: 130,
					label: "Filtrar",
					placeholder: "Digite palavras-chave"
				});
			</script>
		';
		
		// ** informações cadastrais
		
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '<h3>Informações Cadastrais</h3>';
		
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Nome completo:' . br() . form_input('nome_real', $this->input->post('nome_real'), 'size="40"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('CPF:' . br() . form_input('cpf', $this->input->post('cpf'), 'alt="cpf" size="10"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('RG:' . br() . form_input('rg', $this->input->post('rg'), 'size="10"')) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= heading('Endereço', 3);
		$conteudo .= '<div dojoType="dijit.Tooltip" connectId="cep" position="below">Ao digitar o CEP os campos Endereço, Bairro, Cidade e Estado serão preenchidos automaticamente.</div>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('CEP:' . br() . form_input('cep', $this->input->post('cep'), 'id="cep" alt="cep" size="5"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Endereço:' . br() . form_input('endereco', $this->input->post('endereco'), 'size="53"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Nº:' . br() . form_input('numero', $this->input->post('numero'), 'size="1"')) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Bairro:' . br() . form_input('bairro', $this->input->post('bairro'), 'size="20"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Cidade:' . br() . form_input('cidade', $this->input->post('cidade'), 'size="20"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Estado:' . br() . form_dropdown('estado', $this->_obter_estados(), $this->input->post('estado'))) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';
		
		$conteudo .= heading('Contato', 3);
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Telefone 1:' . br() . form_input('telefone_1', $this->input->post('telefone_1'), 'alt="phone" size="10"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Telefone 2:' . br() . form_input('telefone_2', $this->input->post('telefone_2'), 'alt="phone" size="10"')) . '</p>';
		$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('E-mail:' . br() . form_input('email', $this->input->post('email'), 'size="40"')) . '</p>';
		$conteudo .= '<div style="clear: both;"></div>';
		
		
		
		// Tabela de Preços
		if($this->input->post("grupo") == "representantes")
		{
			
			$conteudo .= heading('Tabela de Preços', 3);
			$tabelas_precos = $this->db_cliente->obter_tabelas_precos();
			
			foreach($tabelas_precos as $tabela_precos)
			{
				
				$_tabelas_precos = unserialize($this->input->post('tabela_precos'));
				if($_tabelas_precos[$tabela_precos["codigo"]] == $tabela_precos["codigo"])
				{
					$ativacao = TRUE;
				}
				else
				{
					$ativacao = FAlSE;
				}
				
				$conteudo .= '<p>' . form_checkbox('tabela_precos[' . $tabela_precos["codigo"] . ']', $tabela_precos["codigo"], $ativacao) . ' Cod. ' . $tabela_precos["codigo"] . ' - ' . $tabela_precos["descricao"] . '</p>';
				
				$ativacao = FAlSE;
			}
			
		}
		
		
		
		// informações cadastrais **
		
		$conteudo .= '<div style="clear: both;"></div>';
		$conteudo .= '<div style="border-top: 1px solid #DDD; margin-top: 25px;"></div>';
		
		$conteudo .= '<p>' . form_submit('concluir', 'Concluir') . ' ou ' . anchor('usuarios', 'Cancelar') . '</p>';
		
		$conteudo .= form_close();
		
		$conteudo .= '
			<script type="text/javascript">
				function is_numero(code) {
					var retorno = false;					
					if ((code >=48 && code <=57) || (code>= 96 && code <=105)) {
						return true;
					}
					return false;
				}
				$(document).ready(function() {
					verificar_grupo();
					
					$("select[name=grupo]").change(function() { verificar_grupo(true); });
				});
				
				$("#txt_mac").live("keydown",function(event){
					var code = event.keyCode;
					
					if (event.ctrlKey) {
						return true;
					} 
					if (code == 9) { // tab
						return true;
					}
					if (code == 8 || code == 46) { // se for uma ação de apagar
						return true;
					}
					if (code >= 37 && code <= 40) { // se for uma seta
						return true;
					}
					
					var tamanho = $(this).val() != null ? $(this).val().length : 0;
					if (tamanho == 12) {
						return false;
					}
					/*
					* O mac é constituido por um valor hexadecimal, ou seja
					* ele contempla números e letras de "A" até "F", então por isso esta validação
					*
					*/
					if (is_numero(code) || (code >= 65 && code <=70)) {
						// code 65 = "A" e code 70 = "F"
						return true;							
					}
					
					return false;
				});
				$("#adicionar_mac").click(function(){
					var mac_usuario = $("#txt_mac").val();
					var serial_usuario = $("#txt_serial").val();
					var codigo_usuario = "'.$id.'";
					if (mac_usuario == null || (mac_usuario.length > 0  && mac_usuario.length < 12)) {
						alert("O MAC ADDRESS deve conter 12 caracteres, com números e letras de A até F");
						return false;
					}
					if(mac_usuario.length == 0 && serial_usuario.length == 0){
						alert("Você deve preencher o Mac Address ou o Serial HD.");
						return false;
					}
					$.ajax({
						url: "'.site_url('usuarios/adicionar_mac').'",
						method : \'GET\',						
						dataType : "json",
						data: {mac : mac_usuario, hd: serial_usuario, codigo_usuario : codigo_usuario},
						success: function (data) {							
							if (data.retorno == "ok") {
								location.reload(true);
								
								//$("#div_mac").html(data.html);
							} else {											
								alert(data.erro);
							}
						}
						
					});					
					return false;
				}); 
				$(".excluir_mac").click(function(){
					var key = $(this).attr("href");
					var codigo_usuario = "'.$id.'";
					$.ajax({
						url: "'.site_url('usuarios/excluir_mac').'",
						method : \'GET\',						
						dataType : "json",
						data: {key : key, codigo_usuario : codigo_usuario},
						success: function (data) {
							if (data.retorno == "ok") {
								location.reload(true);
							} else {
								alert(data.erro);
							}
							
						}
					});
					return false;
				});
				function verificar_grupo(submit) {
					submit = submit ? submit : false;
					
					if ($("select[name=grupo]").val() == "supervisores")
					{
						$("#codigo_representante").show();
						$("#codigos_representantes").show();
						$("#descricao_codigo_representante").text("Representante");
						$("#id_filial").hide();
					}
					else if ($("select[name=grupo]").val() == "representantes" || $("select[name=grupo]").val() == "prepostos")
					{
						$("#codigo_representante").show();
						$("#codigos_representantes").hide();
						$("#descricao_codigo_representante").text("Representante");
						$("#id_filial").hide();
					}
					else if ($("select[name=grupo]").val() == "revendas")
					{
						$("#codigo_representante").show();
						$("#codigos_representantes").hide();
						$("#descricao_codigo_representante").text("Revenda");
						$("#id_filial").hide();
					}
					else if ($("select[name=grupo]").val() == "revendas" || $("select[name=grupo]").val() == "vendedores")
					{
						$("#codigo_representante").show();
						$("#codigos_representantes").hide();
						$("#descricao_codigo_representante").text("Revenda");
						$("#id_filial").show();
					}
					else
					{
						$("#codigo_representante").hide();
						$("#codigos_representantes").hide();
						$("#id_filial").hide();
					}
					
					if (submit) {
						$("input[name=somente_postar]").val("1");
						$("form").submit();
					}
				}
				
				function representante_alterado()
				{
					// atualizar filiais
					if ($("select[name=grupo]").val() == "vendedores")
					{
						$("input[name=somente_postar]").val("1");
						$("form").submit();
					}
				}
			</script>
		';
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function gravar_geolocation($id_acesso, $lat, $lon)
	{
		$this->db->update('acessos_usuarios', array(
			'lat' => $lat,
			'lon' => $lon
		), array('id' => $id_acesso));
	}
	
	public function acessar() 
	{	
		$codigo_acesso = $this->input->get('ca');
		if (!$codigo_acesso) {
			redirect('index.html');
		}
		
		$usuario = $this->db->from('usuarios')->where('codigo_acesso', $codigo_acesso)->get()->row_array();
		if ($usuario != null) {
			$this->db->update('usuarios', array('codigo_acesso' => 'null'), array('id' =>$usuario['id']));
			foreach ($usuario as $indice => $valor) {
				$this->session->set_userdata($indice . '_usuario', $valor);
			}
			$this->session->set_userdata('logado','sim');
			
			if(($usuario['logado'] != '' || $this->session->userdata('logado') != 'sim'))
			{
				$this->session->set_userdata('bloqueado','sim');
			}
			$horaAgora = date("H:i:s");
			if($horaAgora < $usuario['hora_entrada'] && $horaAgora > $usuario['hora_saida'])
			{
				$this->session->set_userdata('bloqueado','sim');
			}
			
			redirect('visao_geral');
		} else {
			redirect('/index.html');
		}
	}
	
	public function autenticar() {
		
		$user	 = $this->input->post('usuario');
		$pass	 = $this->input->post('senha');
		$mac	 = $this->input->post('mac');
		$hd		 = $this->input->post('hd');
		
		$usuario = $this->db->from('usuarios')
						->where('nome', $user)
						->where('senha', $pass)
						->get()->row_array();
						
		if ($usuario != null) 
		{			
			$this->session->set_userdata('logado','sim');
			
			if(($usuario['logado'] != '' || $this->session->userdata('logado') != 'sim'))
			{
				$this->session->set_userdata('bloqueado','sim');
			}
			$horaAgora = date("H:i:s");
			if($horaAgora < $usuario['hora_entrada'] && $horaAgora > $usuario['hora_saida'])
			{
				$this->session->set_userdata('bloqueado','sim');
			}
			
			if ($this->mac_acesso($mac, $hd, $usuario['id'])) {
				$codigo_acesso = $this->gerar_codigo_acesso($usuario['id']);
				
				$this->db->where('id', $usuario['id']);
				$this->db->update('usuarios', array('codigo_acesso' => $codigo_acesso)); 
				echo $this->gera_xml_java($usuario, $codigo_acesso);
			} else {
				echo $this->gera_xml_erro("Acesso não autorizado, contacte o setor de TI de sua empresa.");
			}
			
		} else {
			echo $this->gera_xml_erro("Usuário ou senha inválidos.");
			//var_dump(debug_backtrace());
		}
	}
	
	
	private function mac_acesso($mac, $hd, $codigo_usuario) {
		$this->db->where('id_usuario', $codigo_usuario);
		$this->db->where("upper(mac) = upper('".$mac."')");
		$this->db->or_where("upper(hd) = upper('".$hd."')");
		$retorno = $this->db->from('usuarios_macs')->get()->row_array();
		
	//	var_dump($this->db->last_query());
		if ($retorno == null && count($retorno) == 0) {
			return false;
		}
		return true;
		
	}
	
	
	
	/**
	*/
	private function gera_xml_java($usuario, $codigo_acesso) {
		$dom = new DOMDocument("1.0", "UTF-8");
		$dom->preserveWhiteSpace = false;
		$dom->formatOutput = true;
		$root = $dom->createElement("usuario");
		$root->appendChild($dom->createElement("retorno", "OK"));
		
		$root->appendChild($dom->createElement("id", $usuario['id']));	
		$root->appendChild($dom->createElement("nome",$usuario['nome_real']));	
		$root->appendChild($dom->createElement("status",$usuario['status']));
		$root->appendChild($dom->createElement("grupo",$usuario['grupo']));
		$root->appendChild($dom->createElement("acesso",  $codigo_acesso));
		$dom->appendChild($root);
		return $dom->saveXML();
	}
	
	private function gera_xml_erro($erro) {
		$dom = new DomDocument("1.0", "UTF-8");
		$dom->preserveWhiteSpace = false;
		$dom->formatOutput = true;
		$root = $dom->createElement("usuario");
		$root->appendChild($dom->createElement("retorno",$erro));
		$dom->appendChild($root);
		return $dom->saveXML();
	}
	
	
	private function gerar_codigo_acesso($codigo_usuario){
		$time = time();
		$time .= $codigo_usuario;
		return $time;
	}
	
	function entrar()
	{
		// ** VALIDAR FORM
		if ($_POST)
		{
			$usuario = $this->db->from('usuarios')->where(array('nome' => $this->input->post('nome'), 'senha' => $this->input->post('senha')))->get()->row();
			
			if (!$usuario)
			{
				$erro = 'Dados inválidos';
			}
			else if ($usuario->status == 'inativo')
			{
				$erro = 'Você não possui permissão para entrar no sitema.';
			}
			else
			{
				$this->session->set_userdata('logado','sim');
				
				if(($usuario['logado'] != '' || $this->session->userdata('logado') != 'sim'))
				{
					$this->session->set_userdata('bloqueado','sim');
				}
				$horaAgora = date("H:i:s");
				if($horaAgora < $usuario['hora_entrada'] && $horaAgora > $usuario['hora_saida'])
				{
					$this->session->set_userdata('bloqueado','sim');
				}
				
				
				foreach ($usuario as $indice => $valor)
				{
					$this->session->set_userdata($indice . '_usuario', $valor);
				}
								
				$IpUsuario = ipUsuario();
				$this->db->insert('acessos_usuarios', array(
					'numero' => $this->db->from('acessos_usuarios')->where(array('id_usuario' => $usuario->id))->get()->num_rows() + 1,
					'timestamp' => time(),
					'id_usuario' => $usuario->id,
					'lat' => '-',
					'lon' => '-',
					'data_hora_login' => date("Y-m-d H:i:s"),
					'ip_externo' => $IpUsuario
				));
				
				if($this->session->userdata('grupo_usuario') == 'engenharia')
				{
					redirect('noticias');
				}else
				{
					$this->session->set_flashdata('obter_geolocation_para_o_acesso', $this->db->insert_id());
					$this->session->set_flashdata('exibir_alertas_lightbox', TRUE);
					redirect();
				}	
			}
		}
		// VALIDAR FORM **
		
		// ** EXIBIR FORM
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		$conteudo .= form_open(current_url());
		$conteudo .= '<p>' . form_label('Seu nome:' . br() . form_input('nome')) . '</p>';
		$conteudo .= '<p>' . form_label('Sua senha:' . br() . form_password('senha')) . '</p>';
		$conteudo .= '<p>' . form_submit('entrar', 'Entrar') . '</p>';
		$conteudo .= form_close();
		// EXIBIR FORM **
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function sairForcado()
	{
		$this->session->set_userdata('logado','');
		$this->session->sess_destroy();	
		redirect();
	}
	
	function sair()
	{
		$usuario = $this->db->from('usuarios')->where('id', $this->session->userdata('id_usuario'))->get()->row_array();
		$this->session->set_userdata('logado','');
		$this->db->update('usuarios', array(
			'id_sessao' => '',
			'logado' => ''
		), array('id' => $usuario['id']));
		
		//buscar último registro de login
		$acesso = $this->db->from('acessos_usuarios')->where('id_usuario', $this->session->userdata('id_usuario'))->order_by('data_hora_login','DESC')->limit(1)->get()->row_array();

		//echo "<pre>";
		//echo var_dump($acesso['id']);
		//echo "</pre>";
		//die();
		
		if($acesso != null)
		{
			$this->db->update('acessos_usuarios', array(
				'data_hora_logout' => date("Y-m-d H:i:s")
			), array('id' => $acesso['id']));
		}
		
		$this->session->sess_destroy();	
		redirect();
	}
	
	//
	
	function _obter_status()
	{
		return array('ativo' => 'Ativo', 'inativo' => 'Inativo');
	}
	
	function _obter_filiais($codigo_revenda = NULL)
	{
		if ($codigo_revenda)
		{
			$this->db->where('codigo_revenda', $codigo_revenda);
		}
		
		$filiais = $this->db->from('filiais')->get()->result();
		$_filiais = array();
		
		foreach ($filiais as $filial)
		{
			$_filiais[$filial->id] = $filial->nome;
		}
		
		return $_filiais;
	}
	
	function _obter_representantes($grupo_vendedor = FALSE, $obter_representantes_para_supervisores = FALSE)
	{
		$representantes = $this->db_cliente->obter_representantes();
		$_representantes = array();
		
		foreach ($representantes as $representante)
		{
			if ($grupo_vendedor)
			{
				if ($this->db->from('usuarios')->where(array('grupo' => 'revendas', 'codigo' => $representante['codigo']))->get()->row())
				{
					echo $this->db_cliente->last_query(); 
					$_representantes[$representante['codigo']] = $representante['codigo'] . ' - ' . utf8_encode($representante['nome']);
				}
			}
			else
			{
				if ($obter_representantes_para_supervisores)
				{
					$usuario = $this->db->from('usuarios')->where(array('codigo' => $representante['codigo'], 'grupo' => 'representantes'))->get()->row();
					
					if ($usuario)
					{
						$_representantes[$representante['codigo']] = $representante['codigo'] . ' - ' . ($usuario->nome_real ? $usuario->nome_real : $usuario->nome);
					}
				}
				else
				{
					$_representantes[$representante['codigo']] = $representante['codigo'] . ' - ' . utf8_encode($representante['nome']);
				}
			}
		}
		
		return $_representantes;
	}
	
	function obter_usuarios(){
		
		$sql = "SELECT * FROM (`usuarios`) WHERE ";
	
		if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')))
		{
			$sql .= "(`grupo` = 'gestores_comerciais' OR `id` = '".$this->session->userdata('id_usuario')."') AND ";
		}
		$sql .= "`status` = 'ativo' AND (`codigo` LIKE '%".$this->input->get('term')."%' or `nome` LIKE '%".$this->input->get('term')."%' or `nome_real` LIKE '%".$this->input->get('term')."%') ORDER BY `grupo`, `nome_real`";
		
		$query = $this->db->query($sql);
		
		$usuarios = $query->result();
		//print_r($usuarios);
		foreach ($usuarios as $usuario)
		{
			$dados[] = array('label' => $usuario->codigo . ' - ' . $usuario->nome_real , 'value' => $usuario->id);
		}
		echo json_encode($dados);
	}
	
	public function adicionar_mac() {
		$mac = $_GET['mac'];
		$hd = $_GET['hd'];
		$codigo_usuario = $_GET['codigo_usuario'];
		
		$macs = $this->obter_macs_sessao($codigo_usuario);
		
		if (is_array($macs) && in_array($mac, $macs)) {
			$retorno['retorno'] = "erro";
			$retorno['erro'] = "Mac já adicionado, por favor, verifique";
			echo json_encode($retorno);
			return;
		}
		
		$macs[] = array('mac' => $mac, 'hd' => $hd);		
		
		$this->atualizar_macs_sessao($macs, $codigo_usuario);
		$retorno = array();
		$retorno['retorno'] = "ok";
		//$retorno['html'] = $this->montar_tabela_mac($codigo_usuario);
		echo json_encode($retorno);
		
	}
	
	public function excluir_mac() {
		$posicao = $_GET['key'];
		$codigo_usuario = $_GET['codigo_usuario'];

		$macs = $this->obter_macs_sessao($codigo_usuario);		
		unset($macs[$posicao]);
		
		$this->atualizar_macs_sessao($macs, $codigo_usuario);
		$retorno['retorno'] = "ok";
		//$retorno['html'] = $this->montar_tabela_mac($codigo_usuario);
		echo json_encode($retorno);
	}
	
	/**
	*/
	private function montar_tabela_mac($codigo_usuario = null) {
		
		$macs = $this->obter_macs_sessao($codigo_usuario);
		$conteudo = "";

		if (is_array($macs) && count($macs) > 0) {
			$conteudo .= '<table class="novo_grid" style="width:50%">';
			$conteudo .= '<thead>';
			$conteudo .= '<tr>';
			$conteudo .= '<th style="text-align:center"> Mac Address</th>';
			$conteudo .= '<th style="text-align:center"> Serial HD</th>';
			$conteudo .= '<th style="text-align:center"> Opções </th>';
			$conteudo .= '</thead>';
			$conteudo .= '</tr>';
			foreach ($macs as $key => $mac) {
				$conteudo .= '<tr>';
				$conteudo .= '<th style="text-align:center">'.$mac['mac'].'</th>';
				$conteudo .= '<th style="text-align:center">'.$mac['hd'].'</th>';
				$conteudo .= '<th style="text-align:center"> <a href="'.$key.'" class="excluir_mac">Excluir</a> </th>';
				$conteudo .= '</tr>';
			}
			$conteudo .= '</table>';
		}		
		return $conteudo;
	}
	
	
	
	/**
	* Método que salva os macs da sessão no banco dados e associa 
	* esses macs ao usuário salvo no banco de dados, 
	* @param $codigo_usuario, id do usuário no banco de dados para poder associar os macs ao
	* 	usuário cadastrado/editado
	* @param $editando informa se a ação é editando, pois se estiver editando tem que buscar os 
	* 	macs na sessão daquele usuário específico.	
	*/
	private function salvar_macs($codigo_usuario, $editando = false) {		
		if ($editando) {
			$macs = $this->obter_macs_sessao($codigo_usuario);
		} else {
			$macs = $this->obter_macs_sessao();
		}
		
		$dados = array();		
		$this->db->where('id_usuario',$codigo_usuario);
		$this->db->delete('usuarios_macs');
		
		foreach ($macs as $mac) {
			$dado = array();
			$dado['mac'] = $mac['mac'];
			$dado['hd'] = $mac['hd'];
			$dado['id_usuario'] = $codigo_usuario;
			$dados[] = $dado;
		}
		if (is_array($dados) && count($dados) > 0) {
			$this->db->insert_batch('usuarios_macs',$dados);
		}
		
		if ($editando) {
			$this->limpar_macs_sessao($codigo_usuario);
		} else {
			$this->limpar_macs_sessao();
		}
	}
	
	/**
	* Verifica se já existe macs associados para aquele usuário na sessão, se não houver
	* busca no banco de dados e atualiza a sessao para aquele usuário
	* @param $codigo_usuario id do usuário no banco de dados para buscar os macs	
	*/
	private function popular_mac_sessao($codigo_usuario) {
		$sessao = $this->obter_macs_sessao($codigo_usuario);
		if (!is_array($sessao)) {
			$macs = $this->obter_macs($codigo_usuario);			
			$this->atualizar_macs_sessao($macs, $codigo_usuario);			
		}
	}
	
	/**
	* Busca os macs, de um determinado usuário, no banco de dados
	* @param $codigo_usuario, id do usuário no banco de dados
	* @return array mesmo se não houver nenhum mac no banco, retorna um array vazio	
	*/
	private function obter_macs($codigo_usuario) {
		$this->db->where('id_usuario', $codigo_usuario);		
		$macs = $this->db->from('usuarios_macs')->get()->result_array();
		$retorno = array();
		
		foreach ($macs as $mac) {
			$retorno[] = array('mac' => $mac['mac'], 'hd' => $mac['hd']);			
		}
		return $retorno;
	}
	
	/**
	*  Busca os macs de um usuário na sessao, é feita essa separação
	* para que não mantenha os macs quando alterar o usuário que está sendo editado 
	* @param $codigo_usuario se for null é porque é um usuário que está sendo criado no momento
	*    ou o código do usuario que está sendo editado
	* @return mixed conteudo da sessão
	*/
	private function obter_macs_sessao($codigo_usuario = null) {
		$variavel_sessao = $codigo_usuario ? $codigo_usuario.'_macs' : 'macs';	
		$retorno = $this->session->userdata($variavel_sessao);
		return $retorno;
	}
	
	/**
	* Atualiza os macs de um usuário na sessão, é feita essa separação para que não 
	* atrapalhe o usuário que está editando o usuário
	* @param $codigo_usuario se for null é porque é um usuário que está sendo criado no momento
		ou o id do usuário que está sendo edtiado no momento.
	*/
	private function atualizar_macs_sessao($macs, $codigo_usuario = null) {
		$variavel_sessao = $codigo_usuario ? $codigo_usuario.'_macs' : 'macs';				
		$this->session->set_userdata($variavel_sessao,$macs);
	}
	
	private function limpar_macs_sessao($codigo_usuario = null) {
		$variavel_sessao = $codigo_usuario ? $codigo_usuario.'_macs' : 'macs';		
		$this->session->unset_userdata($variavel_sessao);
	}
	
}