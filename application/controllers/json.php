<?php

class Json extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	
	function clientes()
	{
		$clientes = $this->db_cliente->obter_clientes();
		$_clientes = array();
		
		foreach ($clientes as $indice => $cliente)
		{
			$_clientes[$indice]['_indice'] = $cliente['codigo'] . '|' . $cliente['loja'];
			$_clientes[$indice]['_valor'] = $cliente['nome'] . ' - ' . $cliente['cpf'];
		}
		
		exit($this->_dojo_query_read_store($_clientes));
	}
	
	function produtos($codigo_grupo_produtos = NULL)
	{
		$produtos = $this->db_cliente->obter_produtos($codigo_grupo_produtos);
		$_produtos = array();
		
		foreach ($produtos as $indice => $produto)
		{
			$_produtos[$indice]['_indice'] = $produto['codigo'];
			$_produtos[$indice]['_valor'] = $produto['codigo'] . ' - ' . $produto['descricao'];
		}
		
		exit($this->_dojo_query_read_store($_produtos));
	}
	
	//
	
	function _dojo_query_read_store($itens = array()) {
		$query = $_REQUEST['_valor'];
		
		// remover "*"
		if (strlen($query) && $query[strlen($query) - 1] == '*') $query = substr($query, 0, strlen($query) - 1);
		
		$_itens = array();
		if ($query) {
			foreach ($itens as $item) {
				if (strpos(strtolower($item['_valor']), strtolower($query)) !== FALSE) $_itens[] = $item;
			}
		}
		
		$quantidade_itens = count($_itens);
		
		if (array_key_exists("start", $_REQUEST)) $_itens = array_slice($_itens, $_REQUEST['start']);
		if (array_key_exists("count", $_REQUEST)) $_itens = array_slice($_itens, 0, $_REQUEST['count']);
		
		return json_encode(array('identifier' => '_indice', 'items' => $_itens, 'numRows' => $quantidade_itens));
	}
	
}