<?php

class Instalar extends MY_Controller {
	
	function Instalar()
	{
		parent::MY_Controller();
		
		$this->load->dbforge();
	}
	
	function index()
	{
		// ** REPRESENTANTES DOS SUPERVISORES
		$this->dbforge->add_field(array(
			'id_supervisor' => array('type' => 'VARCHAR', 'constraint' => 255),
			'codigo_representante' => array('type' => 'VARCHAR', 'constraint' => 255)
		));
		$this->dbforge->create_table('representantes_supervisores', TRUE);
		// REPRESENTANTES DOS SUPERVISORES **
		
		// ** REGIÕES
		$this->dbforge->add_field(array(
			'id' => array('type' => 'INT', 'constraint' => 25, 'auto_increment' => TRUE),
			'timestamp' => array('type' => 'INT', 'constraint' => 25),
			'tipo' => array('type' => 'VARCHAR', 'constraint' => 255),
			'nome' => array('type' => 'VARCHAR', 'constraint' => 255)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('regioes', TRUE);
		// REGIÕES **
		
		// ** CIDADES DAS REGIÕES
		$this->dbforge->add_field(array(
			'id_regiao' => array('type' => 'INT', 'constraint' => 25),
			'cidade' => array('type' => 'VARCHAR', 'constraint' => 255)
		));
		$this->dbforge->create_table('cidades_regioes', TRUE);
		// CIDADES DAS REGIÕES **
		
		// ** ESTADOS DAS REGIÕES
		$this->dbforge->add_field(array(
			'id_regiao' => array('type' => 'INT', 'constraint' => 25),
			'estado' => array('type' => 'VARCHAR', 'constraint' => 255)
		));
		$this->dbforge->create_table('estados_regioes', TRUE);
		// ESTADOS DAS REGIÕES **
		
		// ** USUÁRIOS
		$this->dbforge->add_field(array(
			'id' => array('type' => 'INT', 'constraint' => 25, 'auto_increment' => TRUE),
			'timestamp' => array('type' => 'INT', 'constraint' => 25),
			'id_filial' => array('type' => 'INT', 'constraint' => 25),
			'status' => array('type' => 'VARCHAR', 'constraint' => 255),
			'grupo' => array('type' => 'VARCHAR', 'constraint' => 255),
			'codigo' => array('type' => 'VARCHAR', 'constraint' => 255),
			'nome' => array('type' => 'VARCHAR', 'constraint' => 255),
			'senha' => array('type' => 'VARCHAR', 'constraint' => 255),
			
			'nome_real' => array('type' => 'VARCHAR', 'constraint' => 255),
			'cpf' => array('type' => 'VARCHAR', 'constraint' => 255),
			'rg' => array('type' => 'VARCHAR', 'constraint' => 255),
			'cep' => array('type' => 'VARCHAR', 'constraint' => 255),
			'endereco' => array('type' => 'VARCHAR', 'constraint' => 255),
			'numero' => array('type' => 'VARCHAR', 'constraint' => 255),
			'bairro' => array('type' => 'VARCHAR', 'constraint' => 255),
			'cidade' => array('type' => 'VARCHAR', 'constraint' => 255),
			'estado' => array('type' => 'VARCHAR', 'constraint' => 255),
			'telefone_1' => array('type' => 'VARCHAR', 'constraint' => 255),
			'telefone_2' => array('type' => 'VARCHAR', 'constraint' => 255),
			'email' => array('type' => 'VARCHAR', 'constraint' => 255)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('usuarios', TRUE);
		// USUÁRIOS **
		
		// ** FILIAIS
		$this->dbforge->add_field(array(
			'id' => array('type' => 'INT', 'constraint' => 25, 'auto_increment' => TRUE),
			'timestamp' => array('type' => 'INT', 'constraint' => 25),
			'status' => array('type' => 'VARCHAR', 'constraint' => 255),
			'codigo_revenda' => array('type' => 'VARCHAR', 'constraint' => 255),
			'nome' => array('type' => 'VARCHAR', 'constraint' => 255)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('filiais', TRUE);
		// FILIAIS **
		
		// ** ACESSOS DOS USUÁRIOS
		$this->dbforge->add_field(array(
			'id' => array('type' => 'INT', 'constraint' => 25, 'auto_increment' => TRUE),
			'timestamp' => array('type' => 'INT', 'constraint' => 25),
			'id_usuario' => array('type' => 'INT', 'constraint' => 25),
			'lat' => array('type' => 'VARCHAR', 'constraint' => 255),
			'lon' => array('type' => 'VARCHAR', 'constraint' => 255),
			'ip_externo' => array('type' => 'VARCHAR', 'constraint' => 45),
			'data_hora_login' => array('type' => 'DATETIME'),
			'data_hora_logout' => array('type' => 'DATETIME')
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('acessos_usuarios', TRUE);
		// ACESSOS DOS USUÁRIOS **
		
		// ** PENDÊNCIAS
		$this->dbforge->add_field(array(
			'id' => array('type' => 'INT', 'constraint' => 25, 'auto_increment' => TRUE),
			'timestamp' => array('type' => 'INT', 'constraint' => 25),
			'id_usuario' => array('type' => 'INT', 'constraint' => 25),
			'status' => array('type' => 'VARCHAR', 'constraint' => 255),
			'prioridade' => array('type' => 'VARCHAR', 'constraint' => 255),
			'titulo' => array('type' => 'VARCHAR', 'constraint' => 255),
			'descricao' => array('type' => 'VARCHAR', 'constraint' => 255),
			'prazo_solucao' => array('type' => 'VARCHAR', 'constraint' => 255),
			'prazo_solucao_timestamp' => array('type' => 'INT', 'constraint' => 25),
			'ultima_mensagem_timestamp' => array('type' => 'INT', 'constraint' => 25),
			'id_usuario_ultima_mensagem' => array('type' => 'INT', 'constraint' => 25),
			'nome_usuario_ultima_mensagem' => array('type' => 'VARCHAR', 'constraint' => 255),
			'encerramento_timestamp' => array('type' => 'INT', 'constraint' => 25),
			'uploads' => array('type' => 'TEXT')
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('pendencias', TRUE);
		// PENDÊNCIAS **
		
		// ** USUÁRIOS DAS PENDÊNCIAS
		$this->dbforge->add_field(array(
			'id_pendencia' => array('type' => 'INT', 'constraint' => 25),
			'id_usuario' => array('type' => 'INT', 'constraint' => 25)
		));
		$this->dbforge->create_table('usuarios_pendencias', TRUE);
		// USUÁRIOS DAS PENDÊNCIAS **
		
		// ** MENSAGENS DAS PENDÊNCIAS
		$this->dbforge->add_field(array(
			'id' => array('type' => 'INT', 'constraint' => 25, 'auto_increment' => TRUE),
			'timestamp' => array('type' => 'INT', 'constraint' => 25),
			'id_pendencia' => array('type' => 'INT', 'constraint' => 25),
			'id_usuario' => array('type' => 'INT', 'constraint' => 25),
			'conteudo' => array('type' => 'TEXT')
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('mensagens_pendencias', TRUE);
		// MENSAGENS DAS PENDÊNCIAS **
		
		// ** NOTÍCIAS
		$this->dbforge->add_field(array(
			'id' => array('type' => 'INT', 'constraint' => 25, 'auto_increment' => TRUE),
			'timestamp' => array('type' => 'INT', 'constraint' => 25),
			'status' => array('type' => 'VARCHAR', 'constraint' => 255),
			'link_imagem' => array('type' => 'VARCHAR', 'constraint' => 255),
			'titulo' => array('type' => 'VARCHAR', 'constraint' => 255),
			'conteudo' => array('type' => 'TEXT')
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('noticias', TRUE);
		// NOTÍCIAS **
		
		// ** FEIRAS
		$this->dbforge->add_field(array(
			'id' => array('type' => 'INT', 'constraint' => 25, 'auto_increment' => TRUE),
			'timestamp' => array('type' => 'INT', 'constraint' => 25),
			'status' => array('type' => 'VARCHAR', 'constraint' => 255),
			'link_imagem' => array('type' => 'VARCHAR', 'constraint' => 255),
			'nome' => array('type' => 'VARCHAR', 'constraint' => 255),
			'data_inicial' => array('type' => 'VARCHAR', 'constraint' => 255),
			'data_inicial_timestamp' => array('type' => 'INT', 'constraint' => 25),
			'data_final' => array('type' => 'VARCHAR', 'constraint' => 255),
			'data_final_timestamp' => array('type' => 'INT', 'constraint' => 25)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('feiras', TRUE);
		// FEIRAS **
		
		// ** GRUPOS DE PRODUTOS ATIVOS
		$this->dbforge->add_field(array(
			'id' => array('type' => 'INT', 'constraint' => 25, 'auto_increment' => TRUE),
			'timestamp' => array('type' => 'INT', 'constraint' => 25),
			'codigo' => array('type' => 'VARCHAR', 'constraint' => 255)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('grupos_produtos_ativos', TRUE);
		// GRUPOS DE PRODUTOS ATIVOS **
		
		// ** HISTÓRICOS DOS CLIENTES
		$this->dbforge->add_field(array(
			'id' => array('type' => 'INT', 'constraint' => 25, 'auto_increment' => TRUE),
			'timestamp' => array('type' => 'INT', 'constraint' => 25),
			'codigo_cliente' => array('type' => 'VARCHAR', 'constraint' => 255),
			'loja_cliente' => array('type' => 'VARCHAR', 'constraint' => 255),
			'pessoa_contato' => array('type' => 'VARCHAR', 'constraint' => 255),
			'cargo' => array('type' => 'VARCHAR', 'constraint' => 255),
			'email' => array('type' => 'VARCHAR', 'constraint' => 255),
			'descricao' => array('type' => 'TEXT'),
			'protocolo' => array('type' => 'VARCHAR', 'constraint' => 255)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('historicos_clientes', TRUE);
		// HISTÓRICOS DOS CLIENTES **
		
		// ** COMPROMISSOS
		$this->dbforge->add_field(array(
			'id' => array('type' => 'INT', 'constraint' => 25, 'auto_increment' => TRUE),
			'timestamp' => array('type' => 'INT', 'constraint' => 25),
			'id_usuario' => array('type' => 'INT', 'constraint' => 25),
			'status' => array('type' => 'VARCHAR', 'constraint' => 255),
			'nome' => array('type' => 'VARCHAR', 'constraint' => 255),
			'descricao' => array('type' => 'TEXT'),
			'inicio_timestamp' => array('type' => 'INT', 'constraint' => 25)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('compromissos', TRUE);
		// COMPROMISSOS **
		
		// ** REAGENDAMENTOS DOS COMPROMISSOS
		$this->dbforge->add_field(array(
			'id' => array('type' => 'INT', 'constraint' => 25, 'auto_increment' => TRUE),
			'timestamp' => array('type' => 'INT', 'constraint' => 25),
			'id_compromisso' => array('type' => 'INT', 'constraint' => 25),
			'id_usuario' => array('type' => 'INT', 'constraint' => 25),
			'inicio_timestamp' => array('type' => 'INT', 'constraint' => 25)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('reagendamentos_compromissos', TRUE);
		// REAGENDAMENTOS DOS COMPROMISSOS **
		
		// ** PROSPECTS
		$this->dbforge->add_field(array(
			'id' => array('type' => 'INT', 'constraint' => 25, 'auto_increment' => TRUE),
			'timestamp' => array('type' => 'INT', 'constraint' => 25),
			'codigo_usuario' => array('type' => 'VARCHAR', 'constraint' => 255),
			'status' => array('type' => 'VARCHAR', 'constraint' => 255),
			'tipo_pessoa' => array('type' => 'VARCHAR', 'constraint' => 255),
			'nome' => array('type' => 'VARCHAR', 'constraint' => 255),
			'nome_fantasia' => array('type' => 'VARCHAR', 'constraint' => 255),
			'cpf' => array('type' => 'VARCHAR', 'constraint' => 255),
			'rg' => array('type' => 'VARCHAR', 'constraint' => 255),
			'cep' => array('type' => 'VARCHAR', 'constraint' => 255),
			'endereco' => array('type' => 'VARCHAR', 'constraint' => 255),
			'numero' => array('type' => 'VARCHAR', 'constraint' => 255),
			'bairro' => array('type' => 'VARCHAR', 'constraint' => 255),
			'cidade' => array('type' => 'VARCHAR', 'constraint' => 255),
			'estado' => array('type' => 'VARCHAR', 'constraint' => 255),
			'site' => array('type' => 'VARCHAR', 'constraint' => 255),
			'nome_contato_1' => array('type' => 'VARCHAR', 'constraint' => 255),
			'cargo_contato_1' => array('type' => 'VARCHAR', 'constraint' => 255),
			'telefone_contato_1' => array('type' => 'VARCHAR', 'constraint' => 255),
			'email_contato_1' => array('type' => 'VARCHAR', 'constraint' => 255),
			'nome_contato_2' => array('type' => 'VARCHAR', 'constraint' => 255),
			'cargo_contato_2' => array('type' => 'VARCHAR', 'constraint' => 255),
			'telefone_contato_2' => array('type' => 'VARCHAR', 'constraint' => 255),
			'email_contato_2' => array('type' => 'VARCHAR', 'constraint' => 255),
			'nome_contato_3' => array('type' => 'VARCHAR', 'constraint' => 255),
			'cargo_contato_3' => array('type' => 'VARCHAR', 'constraint' => 255),
			'telefone_contato_3' => array('type' => 'VARCHAR', 'constraint' => 255),
			'email_contato_3' => array('type' => 'VARCHAR', 'constraint' => 255),
			'banco_1' => array('type' => 'VARCHAR', 'constraint' => 255),
			'agencia_1' => array('type' => 'VARCHAR', 'constraint' => 255),
			'conta_1' => array('type' => 'VARCHAR', 'constraint' => 255),
			'banco_2' => array('type' => 'VARCHAR', 'constraint' => 255),
			'agencia_2' => array('type' => 'VARCHAR', 'constraint' => 255),
			'conta_2' => array('type' => 'VARCHAR', 'constraint' => 255),
			'observacao' => array('type' => 'TEXT'),
			'conversao_cliente_timestamp' => array('type' => 'INT', 'constraint' => 25),
			'codigo_cliente' => array('type' => 'VARCHAR', 'constraint' => 255),
			'loja_cliente' => array('type' => 'VARCHAR', 'constraint' => 255),
			'id_feira' => array('type' => 'INT', 'constraint' => 25)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('prospects', TRUE);
		// PROSPECTS **
		
		// ** EMPRESAS
		$this->dbforge->add_field(array(
			'id' => array('type' => 'INT', 'constraint' => 25, 'auto_increment' => TRUE),
			'timestamp' => array('type' => 'INT', 'constraint' => 25),
			'codigo_cliente' => array('type' => 'VARCHAR', 'constraint' => 255),
			'razao_social' => array('type' => 'VARCHAR', 'constraint' => 255),
			'nome_fantasia' => array('type' => 'VARCHAR', 'constraint' => 255),
			'cnpj' => array('type' => 'VARCHAR', 'constraint' => 255),
			'inscricao_estadual' => array('type' => 'VARCHAR', 'constraint' => 255),
			'cep' => array('type' => 'VARCHAR', 'constraint' => 255),
			'endereco' => array('type' => 'VARCHAR', 'constraint' => 255),
			'numero' => array('type' => 'VARCHAR', 'constraint' => 255),
			'bairro' => array('type' => 'VARCHAR', 'constraint' => 255),
			'cidade' => array('type' => 'VARCHAR', 'constraint' => 255),
			'estado' => array('type' => 'VARCHAR', 'constraint' => 255),
			'telefone_1' => array('type' => 'VARCHAR', 'constraint' => 255),
			'telefone_2' => array('type' => 'VARCHAR', 'constraint' => 255),
			'email' => array('type' => 'VARCHAR', 'constraint' => 255),
			'observacao' => array('type' => 'TEXT')
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('empresas', TRUE);
		// EMPRESAS **
		
		// ** HISTÓRICOS DOS PROSPECTS
		$this->dbforge->add_field(array(
			'id' => array('type' => 'INT', 'constraint' => 25, 'auto_increment' => TRUE),
			'timestamp' => array('type' => 'INT', 'constraint' => 25),
			'id_prospect' => array('type' => 'INT', 'constraint' => 25),
			'pessoa_contato' => array('type' => 'VARCHAR', 'constraint' => 255),
			'cargo' => array('type' => 'VARCHAR', 'constraint' => 255),
			'email' => array('type' => 'VARCHAR', 'constraint' => 255),
			'descricao' => array('type' => 'TEXT'),
			'protocolo' => array('type' => 'VARCHAR', 'constraint' => 255)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('historicos_prospects', TRUE);
		// HISTÓRICOS DOS PROSPECTS **
		
		// ** ORÇAMENTOS
		$this->dbforge->add_field(array(
			'id' => array('type' => 'INT', 'constraint' => 25, 'auto_increment' => TRUE),
			'timestamp' => array('type' => 'INT', 'constraint' => 25),
			'id_usuario' => array('type' => 'INT', 'constraint' => 25),
			'status' => array('type' => 'VARCHAR', 'constraint' => 255),
			'codigo_cliente' => array('type' => 'VARCHAR', 'constraint' => 255),
			'loja_cliente' => array('type' => 'VARCHAR', 'constraint' => 255),
			'id_prospect' => array('type' => 'INT', 'constraint' => 25),
			'codigo_forma_pagamento' => array('type' => 'VARCHAR', 'constraint' => 255),
			'codigo_transportadora' => array('type' => 'VARCHAR', 'constraint' => 255),
			'codigo_ordem_compra' => array('type' => 'VARCHAR', 'constraint' => 255),
			'data_entrega_timestamp' => array('type' => 'INT', 'constraint' => 25),
			'observacao' => array('type' => 'TEXT'),
			'motivo_reprovacao' => array('type' => 'TEXT')
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('orcamentos', TRUE);
		// ORÇAMENTOS **
		
		// ** PRODUTOS DOS ORÇAMENTOS
		$this->dbforge->add_field(array(
			'id' => array('type' => 'INT', 'constraint' => 25, 'auto_increment' => TRUE),
			'timestamp' => array('type' => 'INT', 'constraint' => 25),
			'id_orcamento' => array('type' => 'INT', 'constraint' => 25),
			'codigo' => array('type' => 'VARCHAR', 'constraint' => 255),
			'preco' => array('type' => 'DECIMAL', 'constraint' => '25,2'),
			'codigo_tabela_precos' => array('type' => 'VARCHAR', 'constraint' => 255),
			'quantidade' => array('type' => 'INT', 'constraint' => 25)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('produtos_orcamentos', TRUE);
		// PRODUTOS DOS ORÇAMENTOS **
		
		// ** PEDIDOS
		$this->dbforge->add_field(array(
			'id' => array('type' => 'INT', 'constraint' => 25, 'auto_increment' => TRUE),
			'timestamp' => array('type' => 'INT', 'constraint' => 25),
			'id_usuario' => array('type' => 'INT', 'constraint' => 25),
			'codigo_usuario' => array('type' => 'VARCHAR', 'constraint' => 255),
			'tipo' => array('type' => 'VARCHAR', 'constraint' => 255),
			'status' => array('type' => 'VARCHAR', 'constraint' => 255),
			'codigo_cliente' => array('type' => 'VARCHAR', 'constraint' => 255),
			'loja_cliente' => array('type' => 'VARCHAR', 'constraint' => 255),
			'id_prospect' => array('type' => 'INT', 'constraint' => 25),
			'codigo_forma_pagamento' => array('type' => 'VARCHAR', 'constraint' => 255),
			'tipo_frete' => array('type' => 'VARCHAR', 'constraint' => 255),
			'codigo_transportadora' => array('type' => 'VARCHAR', 'constraint' => 255),
			'codigo_ordem_compra' => array('type' => 'VARCHAR', 'constraint' => 255),
			'data_entrega_timestamp' => array('type' => 'INT', 'constraint' => 25),
			'observacao_pedido_imediato' => array('type' => 'TEXT'),
			'observacao_pedido_programado' => array('type' => 'TEXT'),
			'motivo_reprovacao' => array('type' => 'TEXT'),
			'conversao_pedido_timestamp' => array('type' => 'INT', 'constraint' => 25),
			'id_feira' => array('type' => 'INT', 'constraint' => 25)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('pedidos', TRUE);
		// PEDIDOS **
		
		// ** ITENS DOS PEDIDOS
		$this->dbforge->add_field(array(
			'id' => array('type' => 'INT', 'constraint' => 25, 'auto_increment' => TRUE),
			'timestamp' => array('type' => 'INT', 'constraint' => 25),
			'id_pedido' => array('type' => 'INT', 'constraint' => 25),
			'tipo' => array('type' => 'VARCHAR', 'constraint' => 255),
			'codigo' => array('type' => 'VARCHAR', 'constraint' => 255),
			'codigo_real' => array('type' => 'VARCHAR', 'constraint' => 255),
			'descricao' => array('type' => 'VARCHAR', 'constraint' => 255),
			'unidade_medida' => array('type' => 'VARCHAR', 'constraint' => 255),
			'preco' => array('type' => 'DECIMAL', 'constraint' => '25,2'),
			'preco_com_ipi' => array('type' => 'DECIMAL', 'constraint' => '25,2'),
			'preco_com_ipi_e_desconto' => array('type' => 'DECIMAL', 'constraint' => '25,2'),
			'ipi' => array('type' => 'DECIMAL', 'constraint' => '25,2'),
			'desconto' => array('type' => 'DECIMAL', 'constraint' => '25,2'),
			'valor_ipi' => array('type' => 'DECIMAL', 'constraint' => '25,2'),
			'valor_desconto' => array('type' => 'DECIMAL', 'constraint' => '25,2'),
			'valor_total' => array('type' => 'DECIMAL', 'constraint' => '25,2'),
			'valor_total_ipi' => array('type' => 'DECIMAL', 'constraint' => '25,2'),
			'valor_total_desconto' => array('type' => 'DECIMAL', 'constraint' => '25,2'),
			'valor_total_com_ipi' => array('type' => 'DECIMAL', 'constraint' => '25,2'),
			'valor_total_com_ipi_e_desconto' => array('type' => 'DECIMAL', 'constraint' => '25,2'),
			'codigo_tabela_precos' => array('type' => 'VARCHAR', 'constraint' => 255),
			'quantidade' => array('type' => 'INT', 'constraint' => 25)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('itens_pedidos', TRUE);
		// ITENS DOS PEDIDOS **
		
		// ** METAS
		$this->dbforge->add_field(array(
			'id' => array('type' => 'INT', 'constraint' => 25, 'auto_increment' => TRUE),
			'tipo_meta' => array('type' => 'VARCHAR', 'constraint' => 255),
			'meta_valor' => array('type' => 'DECIMAL', 'constraint' => '25,2'),
			'meta_quantidade' => array('type' => 'INT', 'constraint' => 25),
			'data_inicial' => array('type' => 'INT', 'constraint' => 25),
			'data_final' => array('type' => 'INT', 'constraint' => 25),
			'para' => array('type' => 'VARCHAR', 'constraint' => 255),
			'para_array' => array('type' => 'VARCHAR', 'constraint' => 255),
			'timestamp' => array('type' => 'INT', 'constraint' => 25)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('metas', TRUE);
		// METAS**
		
		//
		
		$this->_inserir_dados();
		
		redirect();
	}
	
	function _inserir_dados()
	{
		if (!$this->db->from('usuarios')->get()->row())
		{
			$this->db->insert('usuarios', array(
				'timestamp' => time(),
				'status' => 'ativo',
				'grupo' => 'gestores_comerciais',
				'nome' => 'Gestor Comercial',
				'senha' => '1234'
			));
			
			$this->db->insert('usuarios', array(
				'timestamp' => time(),
				'status' => 'ativo',
				'grupo' => 'gestores_financeiros',
				'nome' => 'Gestor Financeiro',
				'senha' => '1234'
			));
			
			$this->db->insert('usuarios', array(
				'timestamp' => time(),
				'status' => 'ativo',
				'grupo' => 'representantes',
				'nome' => 'Representante',
				'senha' => '1234'
			));
			
			$this->db->insert('usuarios', array(
				'timestamp' => time(),
				'status' => 'ativo',
				'grupo' => 'prepostos',
				'nome' => 'Preprosto',
				'senha' => '1234'
			));
		}
	}
	
}