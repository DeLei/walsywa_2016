<?php

class Visao_geral extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	
	function index()
	{
		if ($this->session->userdata('grupo_usuario') == 'administradores')
		{
			redirect('usuarios');
		}
		else if (in_array($this->session->userdata('grupo_usuario'), array('representantes', 'prepostos')))
		{
			$this->session->set_userdata('id_usuario_visao_geral', $this->session->userdata('id_usuario'));
		}
		else
		{
			$dropdown_de_representantes = $this->_obter_dropdown_representantes('id', array('name' => 'id_usuario_visao_geral', 'value' => $this->session->userdata('id_usuario_visao_geral'), 'id' => 'usuarios'), TRUE);
		}
		
		$this->load->view('layout', array('conteudo' => $this->load->view('visao_geral/index', array('dropdown_de_representantes' => $dropdown_de_representantes, 'ultima_noticia' => $this->db->from('noticias')->where(array('status' => 'ativa'))->order_by('id', 'desc')->limit(1)->get()->row()), TRUE)));
	}
	
	function obter_valor_vendido_no_periodo_teste()//($timestamp_inicial, $timestamp_final, $codigo_do_representante = NULL, $tipo_data = 'mes')
	{
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->config->load('db_cliente_' . $this->db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');
		
		if ($codigo_do_representante)
		{
			$this->db_cliente->where('F2_VEND1', $codigo_do_representante);
		}
		else
		{
			// filtrar somente os representantes ativos
			$this->db_cliente->where_in('F2_VEND1', $this->_obter_codigos_dos_representantes());
		}
		/*$r = $this->db_cliente
			->select('F2_EMISSAO')
			->select_sum('F2_VALFAT')
			->from($this->_db_cliente['tabelas']['notas_fiscais'])
			->where(array(
				//'F2_FILIAL' => 10,
				'F2_EMISSAO >=' => date('Ymd', $timestamp_inicial),
				'F2_EMISSAO <=' => date('Ymd', $timestamp_final),
				'F2_FILIAL =' => '03',
				'F2_FILIAL =' => '04',
				'F2_FILIAL =' => '05',
				//(F2_FILIAL = '03'	OR F2_FILIAL = '04' OR F2_FILIAL = '05')
				'D_E_L_E_T_ !=' => '*'
			))
			->group_by('F2_EMISSAO')
			->order_by('F2_EMISSAO', 'asc')
			->get()->result();
			*/
		/* ESSE
		$sql = "SELECT F2_EMISSAO, SUM(F2_VALFAT) AS F2_VALFAT 
			FROM SF2010 
			WHERE 
				F2_VEND1 = '71' 
				AND F2_EMISSAO >= '20110821' 
				AND F2_EMISSAO <= '20110920' 
				AND F2_FILIAL IN (3,4,5)
				AND D_E_L_E_T_ != '*' 
			GROUP BY F2_EMISSAO 
			ORDER BY F2_EMISSAO asc";
		*/
		
		$sql = "select D2_EMISSAO, SUM(D2_TOTAL) as D2_TOTAL
				from SD2010, SF4010, SF2010
			   where SD2010.D_E_L_E_T_ = ''
				 and SF4010.D_E_L_E_T_ = ''
				 and SF2010.D_E_L_E_T_ = ''
				 and F2_DOC = D2_DOC
				 and F2_VEND1 in ('71')
				 and D2_TES = F4_CODIGO
				 and F4_DUPLIC = 'S'
				 and D2_FILIAL in ('05')
				 and D2_EMISSAO between '20110101'
									and '20111231'
			group by D2_EMISSAO
			order by D2_EMISSAO asc";
		
		$query = $this->db_cliente->query($sql);
        
		//$r->result_array();
		
		$soma = 0;
		$_r = array();
		foreach ($query->result_array() as $row)
		{
			$soma += $row['F2_VALFAT'];
			//echo $row['F2_VALFAT'].' | ';
			print_r($row);echo '<br>';
		}
		
		echo '<br>SOMA:'.$soma.'<br>';
		
		//echo 'SQL::'.$this->db_cliente->last_query();
		//echo '<pre>';print_r($r);echo '</pre>';
		
		/*
		$_r = array();
		foreach ($r as $v)
		{
			$_r[date(($tipo_data == 'mes' ? 'Ym' : 'Ymd'), strtotime($v->F2_EMISSAO))] += floatval($v->F2_VALFAT);
		}*/
		//print_r($_r);
		//return $_r;
	}
	
	function obter_graficos()
	{
		$usuario = $this->db->from('usuarios')->where('id', $this->session->userdata('id_usuario_visao_geral'))->get()->row();
		
		$graficos = array();
		
		// ** GRÁFICO 1 E GRÁFICO 2: META VS REALIZADO E PREMIAÇÕES
		
		// calcular quantidade de meses que vamos pegar neste ano
		$qtd_meses_neste_ano = date('n');
		
		// calcular quantidade de meses que vamos pegar ano passado
		$qtd_meses_ano_passado = 12 - $qtd_meses_neste_ano;
		
		// se nao subtrairmos 1 da quantidade de meses a pegar no ano passado, o sistema vai começar a contar no mes errado
		$qtd_meses_ano_passado -= 1;
		
		// ** OBTER INFO DB CLIENTE
		
		$timestamp_inicial = mktime(0, 0, 0, 12 - $qtd_meses_ano_passado, 1, date('Y') - 1);
		$timestamp_final = mktime(0, 0, 0, 12, 31, date('Y'));
		
		$met = $this->obter_metas_no_periodo($timestamp_inicial, $timestamp_final, $usuario->codigo);
		
		$vend = $this->obter_valor_vendido_no_periodo($timestamp_inicial, $timestamp_final, $usuario->codigo);
		
		$prem = $this->obter_premiacoes_no_periodo($timestamp_inicial, $timestamp_final, $usuario->codigo);
		
		// OBTER INFO DB CLIENTE **
		
		// vamos calcular os valores do ano anterior
		// começamos no mes inicial (dezembro - quantidade de meses a pegar no ano passado) e vamos até o final (dezembro)
		for ($i = 12 - $qtd_meses_ano_passado; $i <= 12; $i++)
		{
			$timestamp = mktime(0, 0, 0, $i, 1, date('Y') - 1);
			$mes = date('Ym', $timestamp);
			
			if (!$graficos[0]['subtitulo'])
			{
				// caso ucwords não for usada, ao invés de Jan obteremos jan
				$graficos[0]['subtitulo'] = ucwords(strftime('%b', $timestamp)) . ' de ' . (date('Y') - 1) . ' a ';
				$graficos[2]['subtitulo'] = ucwords(strftime('%b', $timestamp)) . ' de ' . (date('Y') - 1) . ' a ';
			}
			
			$graficos[0]['categorias'][] = ucwords(strftime('%b', $timestamp));
			$graficos[2]['categorias'][] = ucwords(strftime('%b', $timestamp));
			
			$graficos[0]['metas'][] = array('y' => $met[$mes], 'ano' => date('Y') - 1);
			
			$graficos[0]['realizado'][] = array('y' => $vend[$mes], 'ano' => date('Y') - 1);
			
			$graficos[2]['premiacoes'][] = array('y' => $prem[$mes], 'ano' => date('Y') - 1);
			
		}
		
		// vamos calcular os valores do ano atual
		for ($i = 1; $i <= $qtd_meses_neste_ano; $i++)
		{
			$timestamp = mktime(0, 0, 0, $i, 1, date('Y'));
			$mes = date('Ym', $timestamp);
			
			$graficos[0]['categorias'][] = ucwords(strftime('%b', $timestamp));
			$graficos[2]['categorias'][] = ucwords(strftime('%b', $timestamp));
			
			$graficos[0]['metas'][] = array('y' => $met[$mes], 'ano' => date('Y'));
			
			$graficos[0]['realizado'][] = array('y' => $vend[$mes], 'ano' => date('Y'));
			
			$graficos[2]['premiacoes'][] = array('y' => $prem[$mes], 'ano' => date('Y'));
		}
		
		$graficos[0]['subtitulo'] .= ucwords(strftime('%b', $timestamp)) . ' de ' . date('Y');
		$graficos[2]['subtitulo'] .= ucwords(strftime('%b', $timestamp)) . ' de ' . date('Y');
		
		// GRÁFICO 1 E GRÁFICO 2: META VS REALIZADO E PREMIAÇÕES **
		
		// ** GRÁFICO 2: ACOMPANHAMENTO DE VENDAS
		
		$timestamp_inicial = mktime(0, 0, 0, date('m'), 1, date('Y'));
		$timestamp_final = mktime(0, 0, 0, date('m'), date('t', $timestamp_inicial), date('Y'));
		
		$vend = $this->obter_valor_vendido_no_periodo($timestamp_inicial, $timestamp_final, $usuario->codigo, 'dia');
		
		$met = $this->obter_metas_no_periodo($timestamp_inicial, $timestamp_final, $usuario->codigo, 'dia');
		
		$graficos[1]['subtitulo'] = ucwords(strftime('%b')) . ' de ' . date('Y');
		
		for ($i = 1; $i <= date('t'); $i++)
		{
			$timestamp = mktime(0, 0, 0, date('m'), $i, date('Y'));
			$dia = date('Ymd', $timestamp);
			
			$graficos[1]['categorias'][] = date('d', $timestamp);
			$graficos[1]['metas'][] = $met[$dia];
			
			$valor_vendido += $vend[$dia];
			
			$graficos[1]['realizado'][] = $valor_vendido;
		}
		
		// GRÁFICO 2: ACOMPANHAMENTO DE VENDAS **
		
		echo json_encode($graficos);
	}
	
	function obter_metas_no_periodo($timestamp_inicial, $timestamp_final, $codigo_do_representante = NULL, $tipo_data = 'mes')
	{
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->config->load('db_cliente_' . $this->db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');
		
		if ($codigo_do_representante)
		{
			$this->db_cliente->where('CT_VEND', $codigo_do_representante);
		}
		else
		{
			// filtrar somente os representantes ativos
			$this->db_cliente->where_in('CT_VEND', $this->_obter_codigos_dos_representantes());
		}
		
		$r = $this->db_cliente
			->select('CT_DATA')
			->select_sum('CT_VALOR')
			
			->from($this->_db_cliente['tabelas']['metas'])
			
			->where(array(
				//'CT_FILIAL' => 10,
				'CT_DATA >=' => date('Ymd', $timestamp_inicial),
				'CT_DATA <=' => date('Ymd', $timestamp_final),
				'D_E_L_E_T_ !=' => '*'
			))
			
			->group_by('CT_DATA')
			->order_by('CT_DATA', 'asc')
			->get()->result();
		
		$_r = array();
		
		foreach ($r as $v)
		{
			$_r[date(($tipo_data == 'mes' ? 'Ym' : 'Ymd'), strtotime($v->CT_DATA))] += floatval($v->CT_VALOR);
		}
		
		return $_r;
	}
	
	function obter_premiacoes_no_periodo($timestamp_inicial, $timestamp_final, $codigo_do_representante = NULL)
	{
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->config->load('db_cliente_' . $this->db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');
		
		if ($codigo_do_representante)
		{
			$this->db_cliente->where('E3_VEND', $codigo_do_representante);
		}
		else
		{
			// filtrar somente os representantes ativos
			$this->db_cliente->where_in('E3_VEND', $this->_obter_codigos_dos_representantes());
		}
		
		$r = $this->db_cliente
			->select('E3_EMISSAO')
			->select_sum('E3_COMIS')
			
			->from($this->_db_cliente['tabelas']['premiacoes'])
			
			->where(array(
				//'E3_FILIAL' => 10,
				'E3_EMISSAO >=' => date('Ymd', $timestamp_inicial),
				'E3_EMISSAO <=' => date('Ymd', $timestamp_final),
				'D_E_L_E_T_ !=' => '*'
			))
			
			->group_by('E3_EMISSAO')
			->order_by('E3_EMISSAO', 'asc')
			->get()->result();
		
		$_r = array();
		
		//foreach ($r as $v)
		foreach ($query->result_array() as $v)
		{
			//$_r[date(($tipo_data == 'mes' ? 'Ym' : 'Ymd'), strtotime($v->F2_EMISSAO))] += floatval($v->F2_VALFAT);
			//$_r[date(($tipo_data == 'mes' ? 'Ym' : 'Ymd'), strtotime($v->D2_EMISSAO))] += floatval($v->D2_TOTAL);
			echo '<pre>';print_r($v);echo '</pre>';
		}
		
		return $_r;
	}
	
	function obter_valor_vendido_no_periodo($timestamp_inicial, $timestamp_final, $codigo_do_representante = NULL, $tipo_data = 'mes')
	{
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->config->load('db_cliente_' . $this->db_cliente->erp);
		$this->_db_cliente = $this->config->item('db_cliente');
		
		if ($codigo_do_representante)
		{
			$this->db_cliente->where('F2_VEND1', $codigo_do_representante);
		}
		else
		{
			// filtrar somente os representantes ativos
			$this->db_cliente->where_in('F2_VEND1', $this->_obter_codigos_dos_representantes());
		}
		/*
		$r = $this->db_cliente
			->select('F2_EMISSAO')
			->select_sum('F2_VALFAT')
			
			->from($this->_db_cliente['tabelas']['notas_fiscais'])
			
			->where(array(
				//'F2_FILIAL' => 3,
				'F2_FILIAL IN (5)' => NULL,
				'F2_EMISSAO >=' => date('Ymd', $timestamp_inicial),
				'F2_EMISSAO <=' => date('Ymd', $timestamp_final),
				'D_E_L_E_T_ !=' => '*'
			))
			
			->group_by('F2_EMISSAO')
			->order_by('F2_EMISSAO', 'asc')
			->get()->result();
		//echo 'SQL::'.$this->db_cliente->last_query();
		*/
		$sql = "select D2_EMISSAO, SUM(D2_TOTAL) as D2_TOTAL
					from SD2010, SF4010, SF2010
				   where SD2010.D_E_L_E_T_ = ''
					 and SF4010.D_E_L_E_T_ = ''
					 and SF2010.D_E_L_E_T_ = ''
					 and F2_DOC = D2_DOC
					 and F2_VEND1 in ('71')
					 and D2_TES = F4_CODIGO
					 and F4_DUPLIC = 'S'
					 and D2_FILIAL in ('05')
					 and D2_EMISSAO between '".$timestamp_inicial."'
										and '".$timestamp_final."'
				group by D2_EMISSAO, D2_TOTAL
				order by D2_EMISSAO asc";
		
		$query = $this->db_cliente->query($sql);
		 
		$_r = array();
		
		//foreach ($r as $v)
		foreach ($query->result_array() as $v)
		{
			//$_r[date(($tipo_data == 'mes' ? 'Ym' : 'Ymd'), strtotime($v->F2_EMISSAO))] += floatval($v->F2_VALFAT);
			//$_r[date(($tipo_data == 'mes' ? 'Ym' : 'Ymd'), strtotime($v->D2_EMISSAO))] += floatval($v->D2_TOTAL);
			echo '<pre>';print_r($v);echo '</pre>';
		}
		
		return $_r;
	}
	
	function _obter_codigos_dos_representantes()
	{
		$representantes = $this->db->select('codigo')->from('usuarios')->where('grupo', 'representantes')->get()->result();
		$_representantes = array();
		
		foreach ($representantes as $representante)
		{
			$_representantes[] = $representante->codigo;
		}
		
		return $_representantes;
	}
	
	function editar_id_usuario($novo_id = NULL)
	{
		$this->session->set_userdata('id_usuario_visao_geral', $novo_id);
		
		redirect('visao_geral');
	}
	
}