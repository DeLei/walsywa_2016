<?php

class Descontos_classe extends MY_Controller {
	
	function __construct(){
		parent::__construct();
	}
	
	function index(){
		$classes = $this->db->from('descontos_classe')->order_by('id', 'asc')->get()->result();
		$total = $this->db->from('descontos_classe')->get()->num_rows();
		
		$this->load->view('layout', array('conteudo' => $this->load->view('descontos_classe/index', array('classes' => $classes, 'total' => $total), TRUE)));
	}
	
	function editar($id = NULL){
		if (!$id){
			redirect();	
		}
		
		$classe = $this->db->from('descontos_classe')->where('id', $id)->get()->row();
		
		// ** VALIDAR FORM
		if ($_POST){
			if (!$this->input->post('nome_classe')){
				$erro = 'Digite uma descrição.';
			}else if (!$this->input->post('desconto')){
				$erro = 'Digite um desconto.';
			}else if($this->input->post('desconto') > 100){
				$erro = 'Valor de desconto inválido.';
			}else{
				$desconto = str_replace(',', '.', $this->input->post('desconto'));
				$dados = array('nome_classe' => $this->input->post('nome_classe'),
							   'desconto' => $desconto);
							   
				$this->db->where('id', $id)->update('descontos_classe', $dados);
				
				redirect('descontos_classe');
			}
		}
		// VALIDAR FORM **
		$conteudo .= '
			<div class="caixa">
				<ul>
					<li>' . anchor('descontos_classe', 'Voltar', 'onclick="history.go(-1); return false;"') . '</li>
				</ul>
			</div>';
		// ** EXIBIR FORM
		$conteudo .= heading('Editar Desconto de Classe', 2);
		if ($erro){
			$conteudo .= '<p class="erro">' . $erro . '</p>';
		}
		
		$conteudo .= form_open(current_url());
		$conteudo .= '<br><p><strong>';
		$conteudo .= heading($classe->codigo_classe . ' - ' . $classe->nome_classe, 3);
		$conteudo .= '</strong></p>';
		$conteudo .= '<p>' . form_label('Descrição:' . br() . form_input('nome_classe', $classe->nome_classe)) . '</p>';
		$data = array('name'        => 'desconto',
					  'value'       => ($classe->desconto * 100),
					  'style'       => 'width:60px;',
					  'alt'       => 'porcento');
		$conteudo .= '<p>' . form_label('Desconto (%):' . br() . form_input($data)) . '</p>';
		$conteudo .= '<p>' . form_submit('concluir', 'Concluir') . ' ou ' . anchor('descontos_classe', 'Cancelar') . '</p>';
		$conteudo .= form_close();
		// EXIBIR FORM **
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
}