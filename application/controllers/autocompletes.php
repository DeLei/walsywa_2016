<?php

class Autocompletes extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('autocomplete');
	}
	
	function exemplo()
	{
	
		$conteudo = '
			<h2>Exemplo de Autocomplete</h2>
			
			<form action="#" method="post">
				<p>Cliente:<br><input type="text" name="codigo_e_loja_do_cliente" value="' . $_POST['_codigo_e_loja_do_cliente'] . '" alt="' . $_POST['codigo_e_loja_do_cliente'] . '" class="autocomplete" data-url="' . site_url('autocompletes/obter_clientes' . ($this->session->userdata('codigo_usuario') ? ('/' . $this->session->userdata('codigo_usuario')) : NULL)) . '"></p>
				
				<p><input type="submit" value="Enviar"></p>
			</form>
		';
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function obter_clientes($codigo_do_representante = NULL)
	{
		$palavras_chave = addslashes($_GET['term']);
		
		$dados = array();
		
		$clientes = $this->autocomplete->obter_clientes($palavras_chave, $codigo_do_representante);
		
		foreach ($clientes as $cliente)
		{
			$dados[] = array('label' => $cliente['nome'] . ' - ' . $cliente['cpf'], 'value' => $cliente['codigo'] . '|' . $cliente['loja']);
		}
		
		
		echo json_encode($dados);
	}
	
}

