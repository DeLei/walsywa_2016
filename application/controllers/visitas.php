<?php

class Visitas extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('db_formulario_visita');
	}
	
	function index() {
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->_db_cliente = $this->config->item('db_cliente');
		
		$ufs = array(
					 'todos' => 'TODOS',
					 'AC'=>'ACRE',
					 'AL'=>'ALAGOAS',
					 'AP'=>'AMAPÁ',
					 'AM'=>'AMAZONAS',
					 'BA'=>'BAHIA',
					 'CE'=>'CEARÁ',
					 'DF'=>'DISTRITO FEDERAL',
					 'ES'=>'ESPÍRITO SANTO',
					 'GO'=>'GOIÁS',
					 'MA'=>'MARANHÃO', 
					 'MT'=>'MATO GROSSO',
					 'MS'=>'MATO GROSSO DO SUL',
					 'MG'=>'MINAS GERAIS',
					 'PA'=>'PARÁ',
					 'PB'=>'PARAÍBA',
					 'PR'=>'PARANÁ',
					 'PE'=>'PERNAMBUCO',
					 'PI'=>'PIAUÍ',
					 'RJ'=>'RIO DE JANEIRO',
					 'RN'=>'RIO GRANDE DO NORTE',
					 'RS'=>'RIO GRANDE DO SUL' ,
					 'RO'=>'RONDÔNIA',
					 'RR'=>'RORAIMA',
					 'SC'=>'SANTA CATARINA',
					 'SP'=>'SÃO PAULO',
					 'SE'=>'SERGIPE',
					 'TO'=>'TOCANTINS' 
				 );
		
		$filtros = array(
			array(
				'nome' => 'codigo',
				'descricao' => 'Código',
				'tipo' => 'texto',
				'campo_mssql' => 'ZZQ_ID',
				'ordenar_ms' => 0
			),
			in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) ?array(
				'nome' => 'codigo_representante',
				'descricao' => 'Representante',
				'tipo' => 'opcoes',
				'campo_mssql' => $this->_db_cliente['campos']['formulario_visitas']['codigo_representante'],
				'opcoes' => $this->db_formulario_visita->get_representantes()
			) : null,
			array(
				'nome' => 'nome_cliente',
				'descricao' => 'Cliente/Prospect',
				'tipo' => 'texto',
				'campo_mssql' => 'ZZQ_NOMECL',
				'ordernar_ms' => 2
			),
			array(
				'nome' => 'cidade_cliente',
				'descricao' => 'Cidade',
				'tipo' => 'texto',
				'campo_mssql' => 'ZZQ_CIDCLI'
			),
			array(
				'nome' => 'estado_cliente',
				'descricao' => 'Estado',
				'tipo' => 'opcoes',
				'campo_mssql' => 'ZZQ_EST',
				'opcoes' => $ufs,
				'ordernar_ms' => 3
			),
			array(
				'nome' => 'data_visitacao',
				'descricao' => 'Data Visita',
				'tipo' => 'data',
				'campo_mssql' => 'ZZQ_DTVISI',
				'ordernar_ms' => 4
			)
		);
	
		$codigo_usuario		=	$this->session->userdata('id_usuario');
		
		if($_POST || $this->input->get('my_submit')){
			// para que a paginação no mssql funcione, devemos ordenar de alguma forma, caso o usuário não escolher uma ordenação, vamos fazer manualmente
			if (!$this->input->get('ordenar_ms'))
			{
				// vamos setar o $_GET ao invés de usar o order_by()
				// usando o order_by a flexinha no layout indicando o que está sendo ordenado não apareceria para o usuário
				$_GET['ordenar_ms'] = 'ZZQ_ID';
				$_GET['ordenar_ms_tipo'] = 'desc';
			}
			
			// caso o usuário logado for um representante, vamos mostrar somente os pedidos que ele mesmo fez
			if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores')))
			{
				$this->db_cliente->where('ZZQ_VEND', $this->session->userdata('codigo_usuario'));
			}
			
			$this->filtragem_mssql($filtros);			
		
			$visitas = $this->db_formulario_visita->obter_formularios_visita($this->db_cliente, $this->input->get('per_page'));
			//debug_pre($this->db_cliente->last_query());
			
			// caso o usuário logado for um representante, vamos mostrar somente os pedidos que ele mesmo fez
			if (!in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores')))
			{
				$this->db_cliente->where('ZZQ_VEND', $this->session->userdata('codigo_usuario'));
			}
						
			// FALSE = não usar order_by
			$this->filtragem_mssql($filtros, FALSE);
			
			$total = $this->db_formulario_visita->obter_formularios_visita($this->db_cliente);
			
		}
	
		$this->load->view('layout', array('conteudo' => $this->load->view('formulario_visita/visitas', array(
			'codigo_usuario' 			=>		$codigo_usuario,
			'visitas'					=>		$visitas,
			'filtragem' 				=>		$this->filtragem($filtros),
			'paginacao' 				=>		$this->paginacao($total['quantidade']),
			'total'						=>		$total
		), TRUE)));
	}
}