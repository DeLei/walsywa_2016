<?php

class Metas extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	
	function index()
	{
		/*
		$conteudo .= heading('Metas', 2);
		
		$conteudo .= '<p>' . anchor('metas/meta_representante/', 'Cadastrar - Meta Representante') . '</p>';
		$conteudo .= '<p>' . anchor('metas/meta_cliente/', 'Cadastrar - Meta Cliente') . '</p>';
		$conteudo .= '<p>' . anchor('metas/meta_produto/', 'Cadastrar - Meta Produto') . '</p>';
		$conteudo .= '<p>' . anchor('metas/meta_estado/', 'Cadastrar - Meta Estados') . '</p>';
		*/
		$this->load->view('layout', array('conteudo' => $conteudo));
		
	}
		
	
	function meta_representante($id = NULL)
	{
		
		if(!$this->input->post('ano'))
		{
			$_POST['ano'] = date('Y');
		}
	
		$conteudo .= heading('Cadastrar Meta - Representantes', 2);
		//$conteudo .= '<p>' . anchor('metas', 'Voltar') . '</p>';
		$conteudo .= '<br /><br />';
		$conteudo .= form_open_multipart(current_url());
		
		$conteudo .= '<p>' . form_label('Escolha um ano para a Meta: ' . form_dropdown('ano', $this->_obter_ano(), $this->input->post('ano'), 'onchange="submit()"')) . '</p>';
		
		

		$nome_meses = $this->_obter_meses();
		
		$conteudo .= '<table cellspacing="0" class="novo_grid"><thead><tr>';
		$conteudo .= '<th>Representante</th>';
		for($mes = 1; $mes <= 12; $mes++)
		{
			$conteudo .= '<th>' . $nome_meses[str_pad($mes, 2, "0", STR_PAD_LEFT)] . '</th>';
		}
		$conteudo .= '</tr></thead><tbody>';
		
		$representantes = $this->_obter_representantes_();
		
		$conteudo .= '
		<script type="text/javascript">
			$(document).ready(function(){
				$(".inserir_meta").live("click", function(e){
					e.preventDefault();
					
					var meses = new Array(); 
					meses[1] = "Janeiro";
					meses[2] = "Fevereiro";
					meses[3] = "Março";
					meses[4] = "Abril";
					meses[5] = "Maio";
					meses[6] = "Junho";
					meses[7] = "Julho";
					meses[8] = "Agosto";
					meses[9] = "Setembro";
					meses[10] = "Outubro";
					meses[11] = "Novembro";
					meses[12] = "Dezembro";
					
					var valor = $(this).html();
					var mes = $(this).attr("alt");
					var id_meta = $(this).attr("id");
					
					this_mes = mes.split("|");
					
					codigo = this_mes[0];
					
					var valor_meta = prompt("Digite o valor da meta para o mês de " + meses[this_mes[1]] + " de ' . $this->input->post('ano') . ':", "");
					
					$link = $(this);
					
					if(valor_meta)
					{
						$.ajax({
							type: "POST",
							url: "' . base_url() . 'index.php/metas/inserir_meta_faturamento/representante",
							data: {codigo: codigo, id_meta: id_meta, valor : valor_meta, mes: this_mes[1], ano: "' . $this->input->post('ano') . '"},
							success: function(dados){
								$link.parent().html(dados);
							}
						});
					}
					
					
					
				});
			});
		</script>
		';
		
		$conteudo .= heading('Faturamento', 2);
		foreach($representantes as $representante)
		{
			$conteudo .= '<tr>';
			$conteudo .= '<td>' . $representante['nome'] . '</td>';
			
			for($mes = 1; $mes <= 12; $mes++)
			{
			
				$data_inicial = explode('/', '01/' . $mes . '/' . $this->input->post('ano'));
				$inicio_timestamp = mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
				
				$ultimo_dia = date('t', $inicio_timestamp);
				
				$data_final = explode('/', $ultimo_dia . '/' . $mes . '/' . $this->input->post('ano'));
				$fim_timestamp = mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]);
				
				$meta =  $this->db->from('metas')->where(array(
					'tipo_meta' => 'meta_faturamento',
					'para_array' => $representante['codigo'], 
					'data_inicial' => $inicio_timestamp,
					'data_final' => $fim_timestamp,
					'para' => 'representante'))->get()->row();
				
				
				$conteudo .= '<td class="right">R$ <a href="#" class="inserir_meta" id="' . $meta->id . '" alt="' . $representante['codigo'] . '|' . $mes . '|' . $this->input->post('ano') . '">' . number_format($meta->meta_valor, 2, ',', '.') . '</a></td>';
			}

			$conteudo .= '</tr>';
		}
		$conteudo .= '</tbody></table>';
		
		
		//------------------------------------------------------------------------------------------------------
		
		$conteudo .= '
		<script type="text/javascript">
			$(document).ready(function(){
				$(".inserir_meta_prospeccao").live("click", function(e){
					e.preventDefault();
					
					var meses = new Array(); 
					meses[1] = "Janeiro";
					meses[2] = "Fevereiro";
					meses[3] = "Março";
					meses[4] = "Abril";
					meses[5] = "Maio";
					meses[6] = "Junho";
					meses[7] = "Julho";
					meses[8] = "Agosto";
					meses[9] = "Setembro";
					meses[10] = "Outubro";
					meses[11] = "Novembro";
					meses[12] = "Dezembro";
					
					var valor = $(this).html();
					var mes = $(this).attr("alt");
					var id_meta = $(this).attr("id");
					
					this_mes = mes.split("|");
					
					codigo = this_mes[0];
					
					var valor_meta = prompt("Digite o valor da meta para o mês de " + meses[this_mes[1]] + " de ' . $this->input->post('ano') . ':", "");
					
					$link = $(this);
					
					if(valor_meta)
					{
						$.ajax({
							type: "POST",
							url: "' . base_url() . 'index.php/metas/inserir_meta_prospeccao/representante",
							data: {codigo: codigo, id_meta: id_meta, valor : valor_meta, mes: this_mes[1], ano: "' . $this->input->post('ano') . '"},
							success: function(dados){
								$link.parent().html(dados);
							}
						});
					}
					
					
					
				});
			});
		</script>
		';
		
		$conteudo .= '<br /><br />';
		$conteudo .= heading('Prospecção', 2);
		$conteudo .= '<table cellspacing="0" class="novo_grid"><thead><tr>';
		$conteudo .= '<th>Representante</th>';
		for($mes = 1; $mes <= 12; $mes++)
		{
			$conteudo .= '<th>' . $nome_meses[str_pad($mes, 2, "0", STR_PAD_LEFT)] . '</th>';
		}
		$conteudo .= '</tr></thead><tbody>';
		
		foreach($representantes as $representante)
		{
			$conteudo .= '<tr>';
			$conteudo .= '<td>' . $representante['nome'] . '</td>';
			
			for($mes = 1; $mes <= 12; $mes++)
			{
			
				$data_inicial = explode('/', '01/' . $mes . '/' . $this->input->post('ano'));
				$inicio_timestamp = mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
				
				$ultimo_dia = date('t', $inicio_timestamp);
				
				$data_final = explode('/', $ultimo_dia . '/' . $mes . '/' . $this->input->post('ano'));
				$fim_timestamp = mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]);
				
				$meta =  $this->db->from('metas')->where(array(
					'tipo_meta' => 'meta_prospeccao',
					'para_array' => $representante['codigo'], 
					'data_inicial' => $inicio_timestamp,
					'data_final' => $fim_timestamp,
					'para' => 'representante'))->get()->row();
				
				
				$conteudo .= '<td class="right"><a href="#" class="inserir_meta_prospeccao" id="' . $meta->id . '" alt="' . $representante['codigo'] . '|' . $mes . '|' . $this->input->post('ano') . '">' . ($meta->meta_quantidade ? $meta->meta_quantidade : '00') . '</a></td>';
			}

			$conteudo .= '</tr>';
		}
		$conteudo .= '</tbody></table>';
		
		
		//------------------------------------------------------------------------------------------------------
		
		$conteudo .= '
		<script type="text/javascript">
			$(document).ready(function(){
				$(".inserir_meta_novos_clientes").live("click", function(e){
					e.preventDefault();
					
					var meses = new Array(); 
					meses[1] = "Janeiro";
					meses[2] = "Fevereiro";
					meses[3] = "Março";
					meses[4] = "Abril";
					meses[5] = "Maio";
					meses[6] = "Junho";
					meses[7] = "Julho";
					meses[8] = "Agosto";
					meses[9] = "Setembro";
					meses[10] = "Outubro";
					meses[11] = "Novembro";
					meses[12] = "Dezembro";
					
					var valor = $(this).html();
					var mes = $(this).attr("alt");
					var id_meta = $(this).attr("id");
					
					this_mes = mes.split("|");
					
					codigo = this_mes[0];
					
					var valor_meta = prompt("Digite o valor da meta para o mês de " + meses[this_mes[1]] + " de ' . $this->input->post('ano') . ':", "");
					
					$link = $(this);
					
					if(valor_meta)
					{
						$.ajax({
							type: "POST",
							url: "' . base_url() . 'index.php/metas/inserir_meta_novos_clientes/representante",
							data: {codigo: codigo, id_meta: id_meta, valor : valor_meta, mes: this_mes[1], ano: "' . $this->input->post('ano') . '"},
							success: function(dados){
								$link.parent().html(dados);
							}
						});
					}
					
					
					
				});
			});
		</script>
		';
		
		$conteudo .= '<br /><br />';
		$conteudo .= heading('Novos Clientes', 2);
		$conteudo .= '<table cellspacing="0" class="novo_grid"><thead><tr>';
		$conteudo .= '<th>Representante</th>';
		for($mes = 1; $mes <= 12; $mes++)
		{
			$conteudo .= '<th>' . $nome_meses[str_pad($mes, 2, "0", STR_PAD_LEFT)] . '</th>';
		}
		$conteudo .= '</tr></thead><tbody>';
		
		foreach($representantes as $representante)
		{
			$conteudo .= '<tr>';
			$conteudo .= '<td>' . $representante['nome'] . '</td>';
			
			for($mes = 1; $mes <= 12; $mes++)
			{
			
				$data_inicial = explode('/', '01/' . $mes . '/' . $this->input->post('ano'));
				$inicio_timestamp = mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
				
				$ultimo_dia = date('t', $inicio_timestamp);
				
				$data_final = explode('/', $ultimo_dia . '/' . $mes . '/' . $this->input->post('ano'));
				$fim_timestamp = mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]);
				
				$meta =  $this->db->from('metas')->where(array(
					'tipo_meta' => 'meta_novos_clientes',
					'para_array' => $representante['codigo'], 
					'data_inicial' => $inicio_timestamp,
					'data_final' => $fim_timestamp,
					'para' => 'representante'))->get()->row();
				
				
				$conteudo .= '<td class="right"><a href="#" class="inserir_meta_novos_clientes" id="' . $meta->id . '" alt="' . $representante['codigo'] . '|' . $mes . '|' . $this->input->post('ano') . '">' . ($meta->meta_quantidade ? $meta->meta_quantidade : '00') . '</a></td>';
			}

			$conteudo .= '</tr>';
		}
		$conteudo .= '</tbody></table>';
		
		$conteudo .= form_close();
	
		$this->load->view('layout', array('conteudo' => $conteudo));
	
	}
	
	function inserir_meta_faturamento($para)
	{
		$codigo = $this->input->post('codigo');
		$valor = $this->input->post('valor');
		$mes = $this->input->post('mes');
		$ano = $this->input->post('ano');
		$id_meta = $this->input->post('id_meta');
		
		$data_inicial = explode('/', '01/' . $mes . '/' . $ano);
		$inicio_timestamp = mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
		
		$ultimo_dia = date('t', $inicio_timestamp);
		
		$data_final = explode('/', $ultimo_dia . '/' . $mes . '/' . $ano);
		$fim_timestamp = mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]);
		
		
		if($id_meta)
		{
		
			$this->db->where(array('id' => $id_meta));
			$this->db->update('metas', array(
								'meta_valor' => $valor
							));
							
			$resultado = $this->db->from('metas')->where('id', $id_meta)->get()->row();
						
		}
		else
		{
			$this->db->insert('metas', array(
								'tipo_meta' => 'meta_faturamento',
								'meta_valor' => $valor,
								'data_inicial' => $inicio_timestamp,
								'data_final' => $fim_timestamp,
								'para' => $para,
								'para_array' => $codigo,
								'timestamp' => time()
							));
							
			$resultado = $this->db->from('metas')->where('id', $this->db->insert_id())->get()->row();
		}
		
		
		if($para == 'cliente')
		{
			echo 'R$ <a href="#" class="inserir_meta" id="' . $resultado->id . '" alt="' . $resultado->para_array  . '-' . date('m', $resultado->data_inicial) . '-' . date('Y', $resultado->data_inicial) . '">' . number_format($resultado->meta_valor, 2, ',', '.') . '</a>';
		}
		else
		{
			echo 'R$ <a href="#" class="inserir_meta" id="' . $resultado->id . '" alt="' . $resultado->para_array . '|' . date('m', $resultado->data_inicial) . '|' . date('Y', $resultado->data_inicial) . '">' . number_format($resultado->meta_valor, 2, ',', '.') . '</a>';
		}
		
	}
	
	
	function inserir_meta_prospeccao($para)
	{
		$codigo = $this->input->post('codigo');
		$valor = $this->input->post('valor');
		$mes = $this->input->post('mes');
		$ano = $this->input->post('ano');
		$id_meta = $this->input->post('id_meta');
		
		$data_inicial = explode('/', '01/' . $mes . '/' . $ano);
		$inicio_timestamp = mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
		
		$ultimo_dia = date('t', $inicio_timestamp);
		
		$data_final = explode('/', $ultimo_dia . '/' . $mes . '/' . $ano);
		$fim_timestamp = mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]);
		
		
		if($id_meta)
		{
		
			$this->db->where(array('id' => $id_meta));
			$this->db->update('metas', array(
								'meta_quantidade' => $valor
							));
							
			$resultado = $this->db->from('metas')->where('id', $id_meta)->get()->row();
						
		}
		else
		{
			$this->db->insert('metas', array(
								'tipo_meta' => 'meta_prospeccao',
								'meta_quantidade' => $valor,
								'data_inicial' => $inicio_timestamp,
								'data_final' => $fim_timestamp,
								'para' => $para,
								'para_array' => $codigo,
								'timestamp' => time()
							));
							
			$resultado = $this->db->from('metas')->where('id', $this->db->insert_id())->get()->row();
		}
		echo '<a href="#" class="inserir_meta_prospeccao" id="' . $resultado->id . '" alt="' . $resultado->para_array . '|' . date('m', $resultado->data_inicial) . '|' . date('Y', $resultado->data_inicial) . '">' . $resultado->meta_quantidade . '</a>';
	}
	
	
	function inserir_meta_novos_clientes($para)
	{
		$codigo = $this->input->post('codigo');
		$valor = $this->input->post('valor');
		$mes = $this->input->post('mes');
		$ano = $this->input->post('ano');
		$id_meta = $this->input->post('id_meta');
		
		$data_inicial = explode('/', '01/' . $mes . '/' . $ano);
		$inicio_timestamp = mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
		
		$ultimo_dia = date('t', $inicio_timestamp);
		
		$data_final = explode('/', $ultimo_dia . '/' . $mes . '/' . $ano);
		$fim_timestamp = mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]);
		
		
		if($id_meta)
		{
		
			$this->db->where(array('id' => $id_meta));
			$this->db->update('metas', array(
								'meta_quantidade' => $valor
							));
							
			$resultado = $this->db->from('metas')->where('id', $id_meta)->get()->row();
						
		}
		else
		{
			$this->db->insert('metas', array(
								'tipo_meta' => 'meta_novos_clientes',
								'meta_quantidade' => $valor,
								'data_inicial' => $inicio_timestamp,
								'data_final' => $fim_timestamp,
								'para' => $para,
								'para_array' => $codigo,
								'timestamp' => time()
							));
							
			$resultado = $this->db->from('metas')->where('id', $this->db->insert_id())->get()->row();
		}
		echo '<a href="#" class="inserir_meta_novos_clientes" id="' . $resultado->id . '" alt="' . $resultado->para_array . '|' . date('m', $resultado->data_inicial) . '|' . date('Y', $resultado->data_inicial) . '">' . $resultado->meta_quantidade . '</a>';
	}
	
	/*
	function meta_representante_($id = NULL)
	{
		
		if ($_POST)
		{		

			if(is_array($this->input->post('para_array')))
			{
				$para_array = implode(',', $this->input->post('para_array'));
			}
		
			$this->db->where(array('id' => $id));
			$this->db->update('metas', array('para_array' => $para_array));

			redirect('metas');
			
		}
		
	
		$conteudo .= heading('Cadastrar Meta', 2);
		
		$conteudo .= '<br /><br />';
		
		$conteudo .= form_open_multipart(current_url());
		
		$representantes = $this->db->from('usuarios')->where('status', 'ativo')->where('grupo', 'representantes')->order_by('nome', 'asc')->get()->result();
		
		$conteudo .= 'Representantes:<br />';
		$conteudo .= '<select name="para_array[]" multiple="multiple" class="multi_select" style="width: 490px;">';
		
		foreach ($representantes as $representante)
		{
			$conteudo .= '<option value="'.$representante->id.'"> ' . $representante->codigo . ' - ' . $representante->nome . '</option>';
		}
		
		$conteudo .= '</select>';
		$conteudo .= '<br /><br />';
		$conteudo .= '<p>' . form_submit('concluir', 'Concluir') . ' ou ' . anchor('metas/criar/' . $id, 'Voltar') . '</p>';
		
		$conteudo .= '
		<script type="text/javascript">
			$(".multi_select").multiselect({
				checkAllText: "Marcar Todos",
				uncheckAllText: "Desmarcar Todos",
				noneSelectedText: "Selecione as opções",
				selectedText: "# Selecionado(s)"
			}).multiselectfilter({
				width: 130,
				label: "Filtrar",
				placeholder: "Digite palavras-chave"
			});
		</script>
		';
		
		$conteudo .= form_close();
		
		$this->load->view('layout', array('conteudo' => $conteudo));
		
	}
	*/
	
	
	
	function meta_cliente($id = NULL)
	{
	
		$conteudo .= heading('Cadastrar Meta - Clientes', 2);
	
		if($_POST)
		{
		
				//$conteudo .= '<p>' . anchor('metas', 'Voltar') . '</p>';
				
				if(!$this->input->post('ano'))
				{
					$_POST['ano'] = date('Y');
				}
				
				$conteudo .= '<br /><br />';
				$conteudo .= form_open_multipart(current_url());
				
				$conteudo .= '<p>' . form_label('Escolha um anoipara Meta: ' . form_dropdown('ano', $this->_obter_ano(), $this->input->post('ano'), 'onchange="submit()"')) . '</p>';
				$conteudo .= form_hidden('representante', $this->input->post('representante'));
				
				$nome_meses = $this->_obter_meses();
				
				$conteudo .= '<table cellspacing="0" class="novo_grid"><thead><tr>';
				$conteudo .= '<th>Representante</th>';
				for($mes = 1; $mes <= 12; $mes++)
				{
					$conteudo .= '<th>' . $nome_meses[str_pad($mes, 2, "0", STR_PAD_LEFT)] . '</th>';
				}
				$conteudo .= '</tr></thead><tbody>';
			
				$cod_representante .= $this->db->from('usuarios')->where('id' , $this->input->post('representante'))->get()->row()->codigo;
				
				$clientes = $this->_obter_clientes($cod_representante);
				
				$conteudo .= '
				<script type="text/javascript">
					$(document).ready(function(){
						$(".inserir_meta").live("click", function(e){
							e.preventDefault();
							
							var meses = new Array(); 
							meses[1] = "Janeiro";
							meses[2] = "Fevereiro";
							meses[3] = "Março";
							meses[4] = "Abril";
							meses[5] = "Maio";
							meses[6] = "Junho";
							meses[7] = "Julho";
							meses[8] = "Agosto";
							meses[9] = "Setembro";
							meses[10] = "Outubro";
							meses[11] = "Novembro";
							meses[12] = "Dezembro";
							
							var valor = $(this).html();
							var mes = $(this).attr("alt");
							var id_meta = $(this).attr("id");
							
							this_mes = mes.split("-");
							
							codigo = this_mes[0];
							
							var valor_meta = prompt("Digite o valor da meta para o mês de " + meses[this_mes[1]] + " de ' . $this->input->post('ano') . ':", "");
							
							$link = $(this);
							
							if(valor_meta)
							{
								$.ajax({
									type: "POST",
									url: "' . base_url() . 'index.php/metas/inserir_meta_faturamento/cliente",
									data: {codigo: codigo, id_meta: id_meta, valor : valor_meta, mes: this_mes[1], ano: "' . $this->input->post('ano') . '"},
									success: function(dados){
										$link.parent().html(dados);
									}
								});
							}
							
							
							
						});
					});
				</script>
				';
				
				
				$conteudo .= heading('Faturamento', 2);
				
				
				foreach($clientes as $valor => $nome)
				{
					$conteudo .= '<tr>';
					$conteudo .= '<td>' . $nome . '</td>';
					
					for($mes = 1; $mes <= 12; $mes++)
					{
					
						$data_inicial = explode('/', '01/' . $mes . '/' . $this->input->post('ano'));
						$inicio_timestamp = mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
						
						$ultimo_dia = date('t', $inicio_timestamp);
						
						$data_final = explode('/', $ultimo_dia . '/' . $mes . '/' . $this->input->post('ano'));
						$fim_timestamp = mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]);
						
						$meta =  $this->db->from('metas')->where(array(
							'tipo_meta' => 'meta_faturamento',
							'para_array' => $valor, 
							'data_inicial' => $inicio_timestamp,
							'data_final' => $fim_timestamp,
							'para' => 'cliente'))->get()->row();
						
						
						$conteudo .= '<td class="right">R$ <a href="#" class="inserir_meta" id="' . $meta->id . '" alt="' . $valor  . '-' . $mes . '-' . $this->input->post('ano') . '">' . number_format($meta->meta_valor, 2, ',', '.') . '</a></td>';
					}

					$conteudo .= '</tr>';
				}
				$conteudo .= '</tbody></table>';
		
		}
		else
		{
			$conteudo .= '<br />';
			$conteudo .= form_open(current_url());
			$conteudo .= '<p>' . form_label('Representantes:' . br() . form_dropdown('representante', $this->_obter_representantes(), $this->input->post('representante'))) . '</p>';
			$conteudo .= '<p>' . form_submit('continuar', 'Continuar');// . ' ou ' . anchor('metas' . $id, 'Voltar') . '</p>';
			$conteudo .= '<br />';
			$conteudo .= form_close();
		}
		
		$this->load->view('layout', array('conteudo' => $conteudo));
		
	}

	
	function meta_estado()
	{
		if(!$this->input->post('ano'))
		{
			$_POST['ano'] = date('Y');
		}
		
		$conteudo .= heading('Cadastrar Meta - Estados', 2);
		$conteudo .= '<br /><br />';
		$conteudo .= form_open_multipart(current_url());
		
		$conteudo .= '<p>' . form_label('Escolha um ano para Meta: ' . form_dropdown('ano', $this->_obter_ano(), $this->input->post('ano'), 'onchange="submit()"')) . '</p>';
	
		$nome_meses = $this->_obter_meses();
		
		$conteudo .= '<table cellspacing="0" class="novo_grid"><thead><tr>';
		$conteudo .= '<th>Estados</th>';
		for($mes = 1; $mes <= 12; $mes++)
		{
			$conteudo .= '<th>' . $nome_meses[str_pad($mes, 2, "0", STR_PAD_LEFT)] . '</th>';
		}
		$conteudo .= '</tr></thead><tbody>';
		
		$estados = $this->_obter_estados();
		
		$conteudo .= heading('Faturamento', 2);
		
		$conteudo .= '
		<script type="text/javascript">
			$(document).ready(function(){
				$(".inserir_meta").live("click", function(e){
					e.preventDefault();
					
					var meses = new Array(); 
					meses[1] = "Janeiro";
					meses[2] = "Fevereiro";
					meses[3] = "Março";
					meses[4] = "Abril";
					meses[5] = "Maio";
					meses[6] = "Junho";
					meses[7] = "Julho";
					meses[8] = "Agosto";
					meses[9] = "Setembro";
					meses[10] = "Outubro";
					meses[11] = "Novembro";
					meses[12] = "Dezembro";
					
					var valor = $(this).html();
					var mes = $(this).attr("alt");
					var id_meta = $(this).attr("id");
					
					this_mes = mes.split("|");
					
					codigo = this_mes[0];
					
					var valor_meta = prompt("Digite o valor da meta para o mês de " + meses[this_mes[1]] + " de ' . $this->input->post('ano') . ':", "");
					
					$link = $(this);
					
					if(valor_meta)
					{
						$.ajax({
							type: "POST",
							url: "' . base_url() . 'index.php/metas/inserir_meta_faturamento/estado",
							data: {codigo: codigo, id_meta: id_meta, valor : valor_meta, mes: this_mes[1], ano: "' . $this->input->post('ano') . '"},
							success: function(dados){
								$link.parent().html(dados);
							}
						});
					}
					
					
					
				});
			});
		</script>
		';
		
		foreach($estados as $estado)
		{
			$conteudo .= '<tr>';
			$conteudo .= '<td>' . $estado . '</td>';
			
			for($mes = 1; $mes <= 12; $mes++)
			{
			
				$data_inicial = explode('/', '01/' . $mes . '/' . $this->input->post('ano'));
				$inicio_timestamp = mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
				
				$ultimo_dia = date('t', $inicio_timestamp);
				
				$data_final = explode('/', $ultimo_dia . '/' . $mes . '/' . $this->input->post('ano'));
				$fim_timestamp = mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]);
				
				$meta =  $this->db->from('metas')->where(array(
					'tipo_meta' => 'meta_faturamento',
					'para_array' => $estado, 
					'data_inicial' => $inicio_timestamp,
					'data_final' => $fim_timestamp,
					'para' => 'estado'))->get()->row();
				
				
				$conteudo .= '<td class="right">R$ <a href="#" class="inserir_meta" id="' . $meta->id . '" alt="' . $estado  . '|' . $mes . '|' . $this->input->post('ano') . '">' . number_format($meta->meta_valor, 2, ',', '.') . '</a></td>';
			}

			$conteudo .= '</tr>';
		}
		$conteudo .= '</tbody></table>';
		
		//---------------------------------------------------------------------------------------------------
		
		$conteudo .= '
		<script type="text/javascript">
			$(document).ready(function(){
				$(".inserir_meta_prospeccao").live("click", function(e){
					e.preventDefault();
					
					var meses = new Array(); 
					meses[1] = "Janeiro";
					meses[2] = "Fevereiro";
					meses[3] = "Março";
					meses[4] = "Abril";
					meses[5] = "Maio";
					meses[6] = "Junho";
					meses[7] = "Julho";
					meses[8] = "Agosto";
					meses[9] = "Setembro";
					meses[10] = "Outubro";
					meses[11] = "Novembro";
					meses[12] = "Dezembro";
					
					var valor = $(this).html();
					var mes = $(this).attr("alt");
					var id_meta = $(this).attr("id");
					
					this_mes = mes.split("|");
					
					codigo = this_mes[0];
					
					var valor_meta = prompt("Digite o valor da meta para o mês de " + meses[this_mes[1]] + " de ' . $this->input->post('ano') . ':", "");
					
					$link = $(this);
					
					if(valor_meta)
					{
						$.ajax({
							type: "POST",
							url: "' . base_url() . 'index.php/metas/inserir_meta_prospeccao/estado",
							data: {codigo: codigo, id_meta: id_meta, valor : valor_meta, mes: this_mes[1], ano: "' . $this->input->post('ano') . '"},
							success: function(dados){
								$link.parent().html(dados);
							}
						});
					}
					
					
					
				});
			});
		</script>
		';
		
		$conteudo .= '<br /><br />';
		$conteudo .= heading('Prospecção', 2);
		$conteudo .= '<table cellspacing="0" class="novo_grid"><thead><tr>';
		$conteudo .= '<th>Estados</th>';
		for($mes = 1; $mes <= 12; $mes++)
		{
			$conteudo .= '<th>' . $nome_meses[str_pad($mes, 2, "0", STR_PAD_LEFT)] . '</th>';
		}
		$conteudo .= '</tr></thead><tbody>';
		
		foreach($estados as $estado)
		{
			$conteudo .= '<tr>';
			$conteudo .= '<td>' . $estado . '</td>';
			
			for($mes = 1; $mes <= 12; $mes++)
			{
			
				$data_inicial = explode('/', '01/' . $mes . '/' . $this->input->post('ano'));
				$inicio_timestamp = mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
				
				$ultimo_dia = date('t', $inicio_timestamp);
				
				$data_final = explode('/', $ultimo_dia . '/' . $mes . '/' . $this->input->post('ano'));
				$fim_timestamp = mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]);
				
				$meta =  $this->db->from('metas')->where(array(
					'tipo_meta' => 'meta_prospeccao',
					'para_array' => $estado, 
					'data_inicial' => $inicio_timestamp,
					'data_final' => $fim_timestamp,
					'para' => 'estado'))->get()->row();
				
				
				$conteudo .= '<td class="right"><a href="#" class="inserir_meta_prospeccao" id="' . $meta->id . '" alt="' . $estado . '|' . $mes . '|' . $this->input->post('ano') . '">' . ($meta->meta_quantidade ? $meta->meta_quantidade : '00') . '</a></td>';
			}

			$conteudo .= '</tr>';
		}
		$conteudo .= '</tbody></table>';
		
		
		//--------------------------------------------------------------------------------------------//
		
		$conteudo .= '
		<script type="text/javascript">
			$(document).ready(function(){
				$(".inserir_meta_novos_clientes").live("click", function(e){
					e.preventDefault();
					
					var meses = new Array(); 
					meses[1] = "Janeiro";
					meses[2] = "Fevereiro";
					meses[3] = "Março";
					meses[4] = "Abril";
					meses[5] = "Maio";
					meses[6] = "Junho";
					meses[7] = "Julho";
					meses[8] = "Agosto";
					meses[9] = "Setembro";
					meses[10] = "Outubro";
					meses[11] = "Novembro";
					meses[12] = "Dezembro";
					
					var valor = $(this).html();
					var mes = $(this).attr("alt");
					var id_meta = $(this).attr("id");
					
					this_mes = mes.split("|");
					
					codigo = this_mes[0];
					
					var valor_meta = prompt("Digite o valor da meta para o mês de " + meses[this_mes[1]] + " de ' . $this->input->post('ano') . ':", "");
					
					$link = $(this);
					
					if(valor_meta)
					{
						$.ajax({
							type: "POST",
							url: "' . base_url() . 'index.php/metas/inserir_meta_novos_clientes/estado",
							data: {codigo: codigo, id_meta: id_meta, valor : valor_meta, mes: this_mes[1], ano: "' . $this->input->post('ano') . '"},
							success: function(dados){
								$link.parent().html(dados);
							}
						});
					}
					
					
					
				});
			});
		</script>
		';
		
		$conteudo .= '<br /><br />';
		$conteudo .= heading('Novos Clientes', 2);
		$conteudo .= '<table cellspacing="0" class="novo_grid"><thead><tr>';
		$conteudo .= '<th>Estados</th>';
		for($mes = 1; $mes <= 12; $mes++)
		{
			$conteudo .= '<th>' . $nome_meses[str_pad($mes, 2, "0", STR_PAD_LEFT)] . '</th>';
		}
		$conteudo .= '</tr></thead><tbody>';
		
		
		foreach($estados as $estado)
		{
			$conteudo .= '<tr>';
			$conteudo .= '<td>' . $estado . '</td>';
			
			for($mes = 1; $mes <= 12; $mes++)
			{
			
				$data_inicial = explode('/', '01/' . $mes . '/' . $this->input->post('ano'));
				$inicio_timestamp = mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
				
				$ultimo_dia = date('t', $inicio_timestamp);
				
				$data_final = explode('/', $ultimo_dia . '/' . $mes . '/' . $this->input->post('ano'));
				$fim_timestamp = mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]);
				
				$meta =  $this->db->from('metas')->where(array(
					'tipo_meta' => 'meta_novos_clientes',
					'para_array' => $estado, 
					'data_inicial' => $inicio_timestamp,
					'data_final' => $fim_timestamp,
					'para' => 'estado'))->get()->row();
				
				
				$conteudo .= '<td class="right"><a href="#" class="inserir_meta_novos_clientes" id="' . $meta->id . '" alt="' . $estado . '|' . $mes . '|' . $this->input->post('ano') . '">' . ($meta->meta_quantidade ? $meta->meta_quantidade : '00') . '</a></td>';
			}

			$conteudo .= '</tr>';
		}
		$conteudo .= '</tbody></table>';
		
		$conteudo .= form_close();
		
		//-------------------------------------------------------------------------------------------------------------------
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	
	function meta_produto($grupo = null)
	{
	
		
		if($_POST)
		{
			if($this->input->post('grupo'))
			{
				redirect('metas/meta_produto/' . $this->input->post('grupo'));
			}
			
		}
		
	
	
	
		if(!$grupo)
		{
	
			$conteudo .= heading('Cadastrar Meta', 2);
			
			$conteudo .= '<br /><br />';
				
			$conteudo .= form_open(current_url());
			
			$conteudo .= '<p>' . form_label('Grupo de Produtos:' . br() . form_dropdown('grupo', $this->_obter_grupo_produtos(), $this->input->post('grupo'))) . '</p>';
			
			$conteudo .= '<p>' . form_submit('continuar', 'Continuar');// . ' ou ' . anchor('metas' . $id, 'Voltar') . '</p>';
			
			$conteudo .= form_close();
			
		}
		else
		{
			
			if(!$this->input->post('ano'))
			{
				$_POST['ano'] = date('Y');
			}
			
			$conteudo .= heading('Cadastrar Meta - Produtos', 2);
			//$conteudo .= '<p>' . anchor('metas', 'Voltar') . '</p>';
			$conteudo .= '<br /><br />';
			$conteudo .= form_open_multipart(current_url());
			
			$conteudo .= '<p>' . form_label('Escolha um ano para Meta: ' . form_dropdown('ano', $this->_obter_ano(), $this->input->post('ano'), 'onchange="submit()"')) . '</p>';
			

			$nome_meses = $this->_obter_meses();
			
			$conteudo .= '<table cellspacing="0" class="novo_grid"><thead><tr>';
			$conteudo .= '<th>Representante</th>';
			for($mes = 1; $mes <= 12; $mes++)
			{
				$conteudo .= '<th>' . $nome_meses[str_pad($mes, 2, "0", STR_PAD_LEFT)] . '</th>';
			}
			$conteudo .= '</tr></thead><tbody>';
			
			$produtos = $this->_obter_produtos($grupo);
			
			$conteudo .= '
			<script type="text/javascript">
				$(document).ready(function(){
					$(".inserir_meta").live("click", function(e){
						e.preventDefault();
						
						var meses = new Array(); 
						meses[1] = "Janeiro";
						meses[2] = "Fevereiro";
						meses[3] = "Março";
						meses[4] = "Abril";
						meses[5] = "Maio";
						meses[6] = "Junho";
						meses[7] = "Julho";
						meses[8] = "Agosto";
						meses[9] = "Setembro";
						meses[10] = "Outubro";
						meses[11] = "Novembro";
						meses[12] = "Dezembro";
						
						var valor = $(this).html();
						var mes = $(this).attr("alt");
						var id_meta = $(this).attr("id");
						
						this_mes = mes.split("|");
						
						codigo = this_mes[0];
						
						var valor_meta = prompt("Digite o valor da meta para o mês de " + meses[this_mes[1]] + " de ' . $this->input->post('ano') . ':", "");
						
						$link = $(this);
						
						if(valor_meta)
						{
							$.ajax({
								type: "POST",
								url: "' . base_url() . 'index.php/metas/inserir_meta_faturamento/produto",
								data: {codigo: codigo, id_meta: id_meta, valor : valor_meta, mes: this_mes[1], ano: "' . $this->input->post('ano') . '"},
								success: function(dados){
									$link.parent().html(dados);
								}
							});
						}
						
						
						
					});
				});
			</script>
			';
			
			$conteudo .= heading('Faturamento', 2);
			foreach($produtos as $valor => $nome)
			{
				$conteudo .= '<tr>';
				$conteudo .= '<td>' . $nome . '</td>';
				
				for($mes = 1; $mes <= 12; $mes++)
				{
				
					$data_inicial = explode('/', '01/' . $mes . '/' . $this->input->post('ano'));
					$inicio_timestamp = mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
					
					$ultimo_dia = date('t', $inicio_timestamp);
					
					$data_final = explode('/', $ultimo_dia . '/' . $mes . '/' . $this->input->post('ano'));
					$fim_timestamp = mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]);
					
					$meta =  $this->db->from('metas')->where(array(
						'tipo_meta' => 'meta_faturamento',
						'para_array' => $valor, 
						'data_inicial' => $inicio_timestamp,
						'data_final' => $fim_timestamp,
						'para' => 'produto'))->get()->row();
					
					
					$conteudo .= '<td class="right">R$ <a href="#" class="inserir_meta" id="' . $meta->id . '" alt="' . $valor . '|' . $mes . '|' . $this->input->post('ano') . '">' . number_format($meta->meta_valor, 2, ',', '.') . '</a></td>';
				}

				$conteudo .= '</tr>';
			}
			$conteudo .= '</tbody></table>';
			
		}
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	
	}
	
	
	function _obter_tipo_meta($retornar_nome = NULL, $tipo = NULL)
	{

		if($tipo){
			
			if($tipo == 1)
			{
				$tipos_metas[0] = "Selecione...";
				$tipos_metas['meta_faturamento'] = "Faturamento";
				$tipos_metas['meta_prospeccao'] = "Prospecção";
				$tipos_metas['meta_novos_clientes'] = "Novos Clientes";
			}
			else
			{
				$tipos_metas[0] = "Selecione...";
				$tipos_metas['meta_faturamento'] = "Faturamento";
			}
			
		}
		else
		{
			$tipos_metas[0] = "Selecione...";
			$tipos_metas['meta_faturamento'] = "Faturamento";
			$tipos_metas['meta_prospeccao'] = "Prospecção";
			$tipos_metas['meta_novos_clientes'] = "Novos Clientes";
		}

		
		if($retornar_nome)
		{
			return $tipos_metas[$retornar_nome];
		}
		else
		{
			return $tipos_metas;
		}
	}
	
	function obter_tipo_meta_ajax($tipo = NULL)
	{
	
		if($tipo){
			
			if($tipo == 1)
			{
				$tipos_metas[0] = "Selecione...";
				$tipos_metas['meta_faturamento'] = "Faturamento";
				$tipos_metas['meta_prospeccao'] = "Prospecção";
				$tipos_metas['meta_novos_clientes'] = "Novos Clientes";
			}
			else
			{
				$tipos_metas[0] = "Selecione...";
				$tipos_metas['meta_faturamento'] = "Faturamento";
			}
			
		}
		else
		{
			$tipos_metas[0] = "Selecione...";
			$tipos_metas['meta_faturamento'] = "Faturamento";
			$tipos_metas['meta_prospeccao'] = "Prospecção";
			$tipos_metas['meta_novos_clientes'] = "Novos Clientes";
		}
		
	
		echo form_label('Tipo de Meta:' . br() . form_dropdown('tipo_meta', $tipos_metas, $this->input->post('tipo_meta'), 'id="tipo_meta"'));
		
	}
	
	function _obter_ano(){
		$ano_inicio = 2008;
		$ano_fim = date('Y');
		
		for($i = $ano_fim; $i >= $ano_inicio; $i--)
		{
			$anos[$i] = $i; 
		}
		
		return $anos;
	}
	
	function _obter_nomes($retornar_nome = NULL)
	{
		$nomes[0] = "Selecione...";
		$nomes['representante'] = "Representante";
		$nomes['cliente'] = "Cliente";
		$nomes['estado'] = "Estados";
		$nomes['produto'] = "Produto";
		
		if($retornar_nome)
		{
			return $nomes[$retornar_nome];
		}
		else
		{
			return $nomes;
		}
	}
	
	
	function _obter_representante($id = NULL)
	{
		$this->db->where('id', $id);
	
		$representante = $this->db->from('usuarios')->where('status', 'ativo')->where('grupo', 'representantes')->order_by('nome', 'asc')->get()->row();
		
		return $representante->nome;
	}
	
	function _obter_representantes()
	{
		$representantes = $this->db->from('usuarios')->where('status', 'ativo')->where('grupo', 'representantes')->order_by('nome', 'asc')->get()->result();

		$_representantes = array();
		
		foreach ($representantes as $representante)
		{
			$_representantes[$representante->id] = $representante->codigo . ' - ' . $representante->nome;
		}
		
		return $_representantes;
	}
	
	function _obter_representantes_()
	{
		$representantes = $this->db->from('usuarios')->where('status', 'ativo')->where('grupo', 'representantes')->order_by('nome', 'asc')->get()->result();

		$_representantes = array();
		
		foreach ($representantes as $representante)
		{
			$_representantes[] = array('nome' => $representante->codigo . ' - ' . $representante->nome, 'codigo' => $representante->id);
		}
		
		return $_representantes;
	}
	
	function _obter_codigos_representantes()
	{
		$representantes = $this->db->from('usuarios')->where('status', 'ativo')->where('grupo', 'representantes')->order_by('nome', 'asc')->get()->result();
		$_representantes = array();
		
		foreach ($representantes as $representante)
		{
			$_representantes[$representante->codigo] = $representante->codigo . ' - ' . $representante->nome;
		}
		
		return $_representantes;
	}
	
	function _obter_clientes($cod_representante)
	{
		$clientes = $this->db_cliente->obter_clientes($cod_representante);
		
		$_clientes = array();
		
		foreach($clientes as $cliente)
		{
			$_clientes[$cliente['codigo'] . '|' . $cliente['loja']] =  $cliente['nome'] . ' - Loja (' . $cliente['loja'] . ')';
		}
		
		return $_clientes;
	}
	
	function _obter_nome_cliente($cod_cliente, $cod_loja)
	{
		$cliente = $this->db_cliente->obter_nome_cliente($cod_cliente, $cod_loja);
		
		return $cliente;
	}
	
	function _obter_nome_produto($cod_produto)
	{
		$cliente = $this->db_cliente->obter_nome_produto($cod_produto);
		
		return $cliente;
	}
	
	function _obter_estados()
	{
		$estados = $this->db_cliente->obter_estados();
		
		$_estados = array();
		
		foreach($estados as $estado)
		{
			$_estados[$estado['estado']] =  $estado['estado'];
		}
		
		sort($_estados);
		
		return $_estados;
		
	}
	
	function _obter_grupo_produtos()
	{
		$grupos = $this->db_cliente->obter_grupos_produtos(FALSE);
		
		$_grupos = array();
		
		foreach($grupos as $grupo)
		{
			$_grupos[$grupo['codigo']] =  $grupo['descricao'];
		}
		
		return $_grupos;
	}
	
	function _obter_produtos($grupo = NULL)
	{
		$produtos = $this->db_cliente->obter_produtos($grupo);
		
		$_produtos = array();
		
		foreach($produtos as $produto)
		{
			$_produtos[$produto['codigo']] = ' ' . $produto['descricao'];
		}
		
		return $_produtos;
		
	}
	
	function _obter_grid($id_grid = NULL, $nome_grid = NULL, $cabealhos = NULL, $dados = NULL, $largura_grid = NULL, $altura_grid = NULL)
	{
		
		$conteudo .= '<script type="text/javascript">';
		
		//Nome do Grid
		$conteudo .= '
		var id_grid = "' . $nome_grid . '";';
		
		
		//* Dados
		$conteudo .= '
		var dados = [
		';
		foreach($dados as $_dados)
		{
			$dados_array = array();
		
			foreach($_dados as $dado)
			{
				$dados_array[] = "'" . str_replace("'", "\'", $dado) . "'";
			}
			
			$_dados_array[] = '[' . implode(",", $dados_array) . ']';
		}
		
		$conteudo .= implode(",", $_dados_array) . '
		';
		
		$conteudo .= '];';
		// Dados */
		
		
		//* Opções de dados
		$conteudo .= '
		var dsOption= {
		fields :[
		';
		
		
		$_cabecalhos = array();
		foreach($cabealhos as $cabecalho)
		{
			$_cabecalhos[] = '{name : "' . $this->_remove_acentos($cabecalho['nome'], true) . '"}';
		}
		
		
		$conteudo .= implode(',', $_cabecalhos);

		$conteudo .= '
			],

			recordType : "array",
			data : dados
		}
		';
		// Opçoes de dados*/
		
		
		
		// Opções de coluna /*
		$conteudo .='
		var colsOption = [
		';
		
		$_dados_colunas = array();
		
		foreach($cabealhos as $cabecalho)
		{
			$tool_tipe = 'toolTip : true ,toolTipWidth : 150';
			$_dados_colunas[] = '{id : "' . $this->_remove_acentos($cabecalho['nome'], true) . '", header : "' . $cabecalho['nome'] . '" ' . ($cabecalho['largura'] ? ', width : ' . $cabecalho['largura'] : "") . ($cabecalho['texto_longo'] ? ', ' . $tool_tipe : '') . '}';
		}
		
		$conteudo .= implode(',', $_dados_colunas);

		$conteudo .= '];
		';
		//Opções de coluna */
		
		
		//Opções do Grid /*
		$conteudo .= '
		var gridOption={
				id : id_grid,
				width: "' . $largura_grid . '",  //"100%", // 700,
				height: "' . $altura_grid. '",  //"100%", // 330,
				container : "' . $id_grid . '", 
				replaceContainer : true, 
				showGridMenu : true,
				allowHide	: true ,
				allowGroup	: true ,
				dataset : dsOption ,
				columns : colsOption,
				pageSize : 10,
				toolbarContent : "nav | print | state",
				skin : "mac",
				onMouseOver : function(value,  record,  cell,  row,  colNo, rowNo,  columnObj,  grid){
					if (columnObj && columnObj.toolTip) {
						grid.showCellToolTip(cell,columnObj.toolTipWidth);
					}else{
						grid.hideCellToolTip();
					}
				},
				onMouseOut : function(value,  record,  cell,  row,  colNo, rowNo,  columnObj,  grid){
					grid.hideCellToolTip();
				}
			};
		'; 
		//Opções do Gris */
		
		$conteudo .= '
			var mygrid=new Sigma.Grid( gridOption );
			Sigma.Util.onLoad( Sigma.Grid.render(mygrid) );
		';
		
		//Opções de Filtro
		$conteudo .= '
			function doFilter() {
				var filterInfo=[
				{
					fieldName : Sigma.Util.getValue("grid_nome_coluna"),
					logic : Sigma.Util.getValue("grid_logica"),
					value : Sigma.Util.getValue("grid_valor_coluna")
				}
				]
				var grid=Sigma.$grid(id_grid);
				var rowNOs=grid.applyFilter(filterInfo); 
			}

			function doUnfilter(){
				var grid=Sigma.$grid(id_grid);
				var rowNOs=grid.applyFilter([]);

			}
		';
		
		
		$conteudo .= '</script>';
		
		//Select Campos
		$conteudo .= '<select id="grid_nome_coluna">';
				
		foreach($cabealhos as $cabecalho)
		{
			$conteudo .= '<option value="' . $this->_remove_acentos($cabecalho['nome'], true) . '" >' . $cabecalho['nome'] . '</option>';
		}
		$conteudo .= '</select>';
		
		
		//Select Lógica
		$conteudo .= '
			<select id="grid_logica">
				<option value="equal">igual</option>
				<option value="notEqual">diferente</option>
				<option value="less">menor que</option>
				<option value="great">maior que</option>
				<option value="lessEqual">menor ou igual</option>
				<option value="greatEqual">maior ou igual</option>
				<option value="like" selected>padrão</option>
				<option value="startWith">iniciando com</option>
				<option value="endWith">terminando com</option>
			</select>
		';
		
		//Campo Valor
		$conteudo .= '<input type="text" id="grid_valor_coluna" value=""><br>';
		
		$conteudo .= '
			<p>
				<input type="button" value="Filtrar" onclick="doFilter()" />
				<input type="button" value="Desfazer" onclick="doUnfilter()">
			</p>
		';
		
		return $conteudo;
		
	}
	
	function _remove_acentos($string, $slug = false)
	{
		$string = strtolower($string);

		// Código ASCII das vogais
		$ascii['a'] = range(224, 230);
		$ascii['e'] = range(232, 235);
		$ascii['i'] = range(236, 239);
		$ascii['o'] = array_merge(range(242, 246), array(240, 248));
		$ascii['u'] = range(249, 252);

		// Código ASCII dos outros caracteres
		$ascii['b'] = array(223);
		$ascii['c'] = array(231);
		$ascii['d'] = array(208);
		$ascii['n'] = array(241);
		$ascii['y'] = array(253, 255);

		foreach ($ascii as $key=>$item) {
			$acentos = '';
			foreach ($item AS $codigo) $acentos .= chr($codigo);
			$troca[$key] = '/['.$acentos.']/i';
		}

		$string = preg_replace(array_values($troca), array_keys($troca), $string);

		// Slug?
		if ($slug) {
			// Troca tudo que não for letra ou número por um caractere ($slug)
			$string = preg_replace('/[^a-z0-9]/i', $slug, $string);
			// Tira os caracteres ($slug) repetidos
			$string = preg_replace('/' . $slug . '{2,}/i', $slug, $string);
			$string = trim($string, $slug);
		}

		return $string;
	}
	
	function relatorios($para = NULL)
	{
		
		/*
		
		if($_POST)
		{
			
			if(!$this->input->post('para'))
			{
				$erro = "Selecione uma opção para meta.";
			}
			else if(!$this->input->post('tipo_meta'))
			{
				$erro = "Selecione uma tipo de meta.";
			}
			else
			{
				if($this->input->post('tipo_meta') == 'meta_faturamento')
				{
					redirect('metas/relatorio_faturamento/' . $this->input->post('para'));
				}
				else if($this->input->post('tipo_meta') == 'meta_prospeccao')
				{
					redirect('metas/relatorio_prospeccao/' . $this->input->post('para'));
				}
				else if($this->input->post('tipo_meta') == 'meta_novos_clientes')
				{
					redirect('metas/relatorio_novos_clientes/' . $this->input->post('para'));
				}
				
			}
			
		}
	
	
		$conteudo .= heading('Relatórios de Metas', 2);
		if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
		$conteudo .= '<br />';
		$conteudo .= form_open(current_url());
		
		$conteudo .= '<p>' . form_label('Para:' . br() . form_dropdown('para', $this->_obter_nomes(), $this->input->post('para'), 'id="para"')) . '</p>';
		
		$conteudo .= '
			<script type="text/javascript">
				$(document).ready(function(){
				
					$("#para").change(function(){					
						if($(this).val() == "representante" || $(this).val() == "estado")
						{
							$.ajax({
							  url: "' . base_url() . 'index.php/metas/obter_tipo_meta_ajax/1",
							  success: function(data) {
								$("#ajax_tipo_meta").html(data);
							  }
							});
						}
						else
						{
							$.ajax({
							  url: "' . base_url() . 'index.php/metas/obter_tipo_meta_ajax/2",
							  success: function(data) {
								$("#ajax_tipo_meta").html(data);
							  }
							});
						}
					});
					
				});
			</script>
		';
		
		if($this->input->post('para'))
		{
			if($this->input->post('para') == "representante" || $this->input->post('para') == "cliente")
			{
				$num_tipo = 1;
			}else{
				$num_tipo = 2;
			}
			$tipo_conteudo = form_label('Tipo de Meta:' . br() . form_dropdown('tipo_meta', $this->_obter_tipo_meta(NULL, $num_tipo), $this->input->post('tipo_meta'), 'id="tipo_meta"'));

		}
		
		$conteudo .= '<p id="ajax_tipo_meta">' . $tipo_conteudo . '</p>';
		
		$conteudo .= '<p>' . form_submit('consultar', 'Consultar') . '</p>';
		$conteudo .= form_close();
		$conteudo .= '<br />';
		*/
		/*
		if($para == 'representante')
		{
			$conteudo .= '<p>' . anchor('metas/relatorio_faturamento/representante', 'Faturamento - Representantes') . '</p>';
			$conteudo .= '<p>' . anchor('metas/relatorio_prospeccao/representante', 'Prospecção - Representantes') . '</p>';
			$conteudo .= '<p>' . anchor('metas/relatorio_novos_clientes/representante', 'Novos Clientes - Representantes') . '</p>';
		}
		else if($para == 'estado')
		{
			$conteudo .= '<p>' . anchor('metas/relatorio_faturamento/estado', 'Faturamento - Estados') . '</p>';
			$conteudo .= '<p>' . anchor('metas/relatorio_prospeccao/estado', 'Prospecção - Estados') . '</p>';
			$conteudo .= '<p>' . anchor('metas/relatorio_novos_clientes/estado', 'Novos Clientes - Estados') . '</p>';
		}
		*/
	
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	//-----=-=-----=-=-----=-=-----
	
	function relatorio_faturamento($para = NULL)
	{
		if($para == 'representante')
		{
			$titulo = false;
			
			if($_GET)
			{
			
				$mes_inicial = $this->input->get('mes_inicial') ? $this->input->get('mes_inicial') : '01';
				$mes_final = $this->input->get('mes_final') ? $this->input->get('mes_final') : 12;
				$ano = $this->input->get('ano') ? $this->input->get('ano') : date('Y');
				
				if ($mes_inicial > $mes_final)
				{
					$erro = 'O período selecionado é inválido.';
				}
				else if(!$this->input->get('para_array'))
				{
					$erro = "Selecione um representante.";
				}
				else
				{
					$conteudo .= '
						<div class="caixa">
							<ul>
								<li>' . anchor('#', 'Imprimir', 'onclick="print(); return false;"') . '</li>
							</ul>
						</div>
					';
					/*
					$conteudo .= '
						<div class="caixa" style="margin-right: 10px">
							<ul>
								<li>' . anchor('metas/visualizar_grafico', 'Visualizar Gráfico') . '</li>
							</ul>
						</div>
					';*/
				
					if(!$titulo){
						$titulo = true;
						$conteudo .= heading('Relatórios de Faturamento - Representantes', 2);
					}
				
					//Listagem
					$ids_representantes = $this->input->get('para_array');
					if($ids_representantes)
					{						

							$representantes = $ids_representantes;
	
							$relatorio  .= '<div style="clear:both"></div>';
							
							
							//----------------------------------
							// Algoritimo para a tabela de relatório
							
							$total_meses = $mes_final - ($mes_inicial - 1);
							$divisor = ceil($total_meses / 3);
							
							$mi = $mes_inicial;
							
							for($tab = 1; $tab <= $divisor; $tab++)
							{

								$_m = array();
								for($m = $mi; $m <= ($mi + 2); $m++)
								{
									if($m <= $mes_final)
									{
										$_m[] = $m;
									}
									
									$mf = $m;
								}
								
								$mi = $mf + 1;
	
								$_tabs[] = $_m;
							}
							
							
							//----------------------------------
							
							$relatorio .= '<table class="hnordt" style="border: none;">';
							foreach($_tabs as $_tab){
							
								
								
								$nome_meses = $this->_obter_meses();
								
								$relatorio .= '<thead style="border: 1px solid #cccccc;">';
								$relatorio .= '<tr>';
								$relatorio .= '<th rowspan="2">Representante</th>';
								foreach($_tab as $mes)
								{							
									$relatorio .= '<th colspan="3">';
									$relatorio .= $nome_meses[str_pad($mes, 2, "0", STR_PAD_LEFT)] . '/' . $ano;
									$relatorio .= '</th>';
								}
								$relatorio .= '</tr>';
								
								$relatorio .= '<tr>';
								foreach($_tab as $mes)
								{							
									$relatorio .= '<th>Meta (R$)</th>';
									$relatorio .= '<th colspan="2">Realizado (R$)</th>';
								}
								$relatorio .= '</tr>';
								
								$relatorio .= '</thead>';
								
								$relatorio .= '</tbody>';
								
								$total_relatorio = array();
								foreach($representantes as $representante)
								{
									
									$relatorio .= '<tr style="border: 1px solid #cccccc;"><td style="border: 1px solid #cccccc;">';
										$relatorio .= $this->db->from('usuarios')->where('id' , $representante)->get()->row()->codigo . ' - ' . $this->db->from('usuarios')->where('id' , $representante)->get()->row()->nome;
									$relatorio .= '</td>';
									
									
									foreach($_tab as $mes)
									{
										
										$grafico[$representante][$mes . '/' . $ano]['data'] = $mes . '/' . $ano;
										$grafico[$representante][$mes . '/' . $ano]['nome'] = $this->db->from('usuarios')->where('id' , $representante)->get()->row()->codigo . ' - ' . $this->db->from('usuarios')->where('id' , $representante)->get()->row()->nome;
										
										$data_inicial = explode('/', '01/' . str_pad($mes, 2, "0", STR_PAD_LEFT) . '/' . $ano);
										$inicio_timestamp = mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
										
										$ultimo_dia = date('t', mktime(23, 59, 59, str_pad($mes, 2, "0", STR_PAD_LEFT), '01', $ano));
										
										$data_final = explode('/', $ultimo_dia . '/' . str_pad($mes, 2, "0", STR_PAD_LEFT) . '/' . $ano);
										$fim_timestamp = mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]);
										
										$meta_representante = $this->db->from('metas')->where(array(
											'tipo_meta' => 'meta_faturamento',
											'para' => 'representante',
											'para_array' => $representante,
											'data_inicial >=' => $inicio_timestamp,
											'data_final <= ' => $fim_timestamp
										))->get()->row();
										
										//Meta
										$relatorio .= '<td   class="right" style="border: 1px solid #cccccc;">';
											$relatorio .= number_format($meta_representante->meta_valor, 2, ',', '.');
											$grafico[$representante][$mes . '/' . $ano]['meta'] = $meta_representante->meta_valor;
										$relatorio .= '</td>';
										
										$total_faturamento = $this->db_cliente->obter_faturamento($this->db->from('usuarios')->where('id' , $representante)->get()->row()->codigo, $inicio_timestamp, $fim_timestamp);
										
										//Realizado
										$relatorio .= '<td class="right" style="border: 1px solid #cccccc;">';
											$relatorio .= number_format($total_faturamento[0]['valor_total'], 2, ',', '.');
											$grafico[$representante][$mes . '/' . $ano]['realizado'] = $total_faturamento[0]['valor_total'];
										$relatorio .= '</td>';
										
										//Porcentagem
										$relatorio .= '<td class="right" style="border: 1px solid #cccccc;">';

											$valor_met = $total_faturamento[0]['valor_total'];
											$valor_fat = $meta_representante->meta_valor;
											
											if(!empty($valor_met) AND !empty($valor_fat))
											{
												$porcentagem = ($valor_met / $valor_fat) * 100;
												$relatorio .= number_format($porcentagem, 2, ',', '.') . '%';
												$grafico[$representante][$mes . '/' . $ano]['porcentagem'] = number_format($porcentagem, 2, ',', '.') . '%';
											}
											else
											{
												$relatorio .= number_format(0, 2, ',', '.') . '%';
												$grafico[$representante][$mes . '/' . $ano]['porcentagem'] = number_format(0, 2, ',', '.') . '%';
											}
											
										$relatorio .= '</td>';
										
										//Somando Total
										$total_relatorio[$mes] = $total_relatorio[$mes] + $total_faturamento[0]['valor_total'];
										
									}
									
									$relatorio .= '</tr>';
								}
								
								//Exibindo Total
								$relatorio .= '<thead>';
								$relatorio .= '<tr style="border: 1px solid #cccccc;">
								<th>Total Realizado (R$)</th>';
								foreach($total_relatorio as $total)
								{
									$relatorio .= '<th colspan="3">' . number_format($total, 2, ',', '.') . '</th>';
								}
								$relatorio .= '</tr>';
								$relatorio .= '</thead>';
								
								$relatorio .= '<tr>';
								$relatorio .= '<td colspan="10" style="height: 30px;"></td>';
								$relatorio .= '</tr>';
								
							}
							
							$relatorio .= '</table>';
							
					}
				
				}
			
			}
			
			if(!$titulo)
				$conteudo .= heading('Relatórios de Faturamento - Representantes', 2);
			
			if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
			$conteudo .= '<br />';

			
			//$conteudo .= form_open(current_url(), 'class="nao_exibir_impressao"');
			$conteudo .= form_open(current_url(), array('method' => 'get', 'class' => 'nao_exibir_impressao'));
			
			$conteudo .= '<p>Representantes:' . br() . form_dropdown('para_array[]', $this->_obter_representantes(), $this->input->get('para_array'), ' multiple="multiple" class="multi_select" style="width: 490px;" ') . '</p>';
			
			$conteudo .= '<p style="float: left; margin-right: 10px;">Período:' . br() . form_dropdown('mes_inicial', $this->_obter_meses(), $mes_inicial, 'dojoType="dojox.form.DropDownSelect"') . ' a ' . form_dropdown('mes_final', $this->_obter_meses(), $mes_final, 'dojoType="dojox.form.DropDownSelect"') . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Ano:' . br() . form_dropdown('ano', array_combine(array_values(range(date('Y'), 2009)), array_values(range(date('Y'), 2009))), $ano, 'dojoType="dojox.form.DropDownSelect"')) . '</p>';
			$conteudo .= '<div style="clear: both;"></div>';
			
			$conteudo .= '<p>' . form_submit('gerar_relatorio', 'Gerar Relatório');// . ' ou ' . anchor('metas/relatorios/representante' . $id, 'Voltar') . '</p>';
			$conteudo .= form_close();
			
			
			$conteudo .= '
			<script type="text/javascript">
				$(".multi_select").multiselect({
					checkAllText: "Marcar Todos",
					uncheckAllText: "Desmarcar Todos",
					noneSelectedText: "Selecione as opções",
					selectedText: "# Selecionado(s)"
				}).multiselectfilter({
					width: 130,
					label: "Filtrar",
					placeholder: "Digite palavras-chave"
				});
			</script>
			';

			$conteudo .= $relatorio;
			
			//$this->session->set_userdata($grafico);
			//$this->session->set_userdata('grafico', $grafico);
			
			
			//$conteudo .= $this->criar_grafico($grafico);
			
			
			
			//gráficos
			/*
			foreach($grafico as $gf)
			{
				foreach($gf as $_gf)
				{
					$conteudo .= '<p>Nome - ' . $_gf['nome'] . '</p>';
					$conteudo .= '<p>Meta - ' . $_gf['meta'] . '</p>';
					$conteudo .= '<p>Realizado - ' . $_gf['realizado'] . '</p>';
					$conteudo .= '<p>Porcentagem - ' . $_gf['porcentagem'] . '</p>';
					$conteudo .= '<br /><br />';
					
					
				}
				
				$xml = '
					<chart caption="Country Comparison" shownames="1" showvalues="0" decimals="0" numberPrefix="$">

						<categories>
							<category label="Austria"/>
							<category label="Brazil"/>
							<category label="France"/>
							<category label="Germany"/>
							<category label="USA"/>
						</categories>

						<dataset seriesName="1996" color="AFD8F8" showValues="0">
							<set value="25601.34"/>
							<set value="20148.82"/>
							<set value="17372.76"/>
							<set value="35407.15"/>
							<set value="38105.68"/>
						</dataset>

						<dataset seriesName="1997" color="F6BD0F" showValues="0">
							<set value="57401.85"/>
							<set value="41941.19"/>
							<set value="45263.37"/>
							<set value="117320.16"/>
							<set value="114845.27"/>
						</dataset>

						<dataset seriesName="1998" color="8BBA00" showValues="0">
							<set value="45000.65"/>
							<set value="44835.76"/>
							<set value="18722.18"/>
							<set value="77557.31"/>
							<set value="92633.68"/>
						</dataset>
					</chart>';
				
				$conteudo .= $this->_gerar_grafico(array('tipo' => 'MSColumn3D', 'url_dados' => site_url($xml)));
			}
			*/
			
		}
		else if($para == 'cliente')
		{
			$titulo = false;
			
			if($_GET)
			{
			
				if($this->input->get("gerar_relatorio"))
				{
					
					$mes_inicial = $this->input->get('mes_inicial') ? $this->input->get('mes_inicial') : '01';
					$mes_final = $this->input->get('mes_final') ? $this->input->get('mes_final') : 12;
					$ano = $this->input->get('ano') ? $this->input->get('ano') : date('Y');
					
					if ($mes_inicial > $mes_final)
					{
						$erro = 'O período selecionado é inválido.';
					}
					else if(!$this->input->get('para_array'))
					{
						$erro = "Selecione um representante.";
					}
					else
					{
					
						$conteudo .= '
							<div class="caixa">
								<ul>
									<li>' . anchor('#', 'Imprimir', 'onclick="print(); return false;"') . '</li>
								</ul>
							</div>
						';
						/*
						$conteudo .= '
							<div class="caixa" style="margin-right: 10px">
								<ul>
									<li>' . anchor('metas/visualizar_grafico', 'Visualizar Gráfico') . '</li>
								</ul>
							</div>
						';*/
					
						if(!$titulo){
							$titulo = true;
							$conteudo .= heading('Relatórios de Faturamento - Clientes', 2);
						}
					
						//Listagem
						$cds_clientes = $this->input->get('para_array');
						if($cds_clientes)
						{

								$clientes = $cds_clientes;
								
								
								
								$nome_representante .= $this->db->from('usuarios')->where('id' , $this->input->get('representante'))->get()->row()->nome;
				
								
								$relatorio .= heading('<br /><br />Clientes do Representante: ' . $nome_representante, 4);
								$relatorio .= '<div style="clear:both"></div>';
								
								//----------------------------------
								// Algoritimo para a tabela de relatório
								
								$total_meses = $mes_final - ($mes_inicial - 1);
								$divisor = ceil($total_meses / 3);
								
								$mi = $mes_inicial;
								
								for($tab = 1; $tab <= $divisor; $tab++)
								{

									$_m = array();
									for($m = $mi; $m <= ($mi + 2); $m++)
									{
										if($m <= $mes_final)
										{
											$_m[] = $m;
										}
										
										$mf = $m;
									}
									
									$mi = $mf + 1;

									$_tabs[] = $_m;
								}
								
								
								//----------------------------------
								
								$relatorio .= '<table class="hnordt" style="border: none;">';
								foreach($_tabs as $_tab){
								
								
										$nome_meses = $this->_obter_meses();
										
										$relatorio .= '<thead style="border: 1px solid #cccccc;">';
										$relatorio .= '<th rowspan="2">Clientes</th>';
										foreach($_tab as $mes)
										{							
											$relatorio .= '<th colspan="3">';
											$relatorio .= $nome_meses[str_pad($mes, 2, "0", STR_PAD_LEFT)] . '/' . $ano;
											$relatorio .= '</th>';
										}
										
										// Título - Meta x Realizado
										$relatorio .= '<tr>';
										foreach($_tab as $mes)
										{							
											$relatorio .= '<th>Meta (R$)</th>';
											$relatorio .= '<th colspan="2">Realizado (R$)</th>';
										}
										$relatorio .= '</tr>';
										
										$relatorio .= '</thead>';
										
										
										$relatorio .= '<tbody>';
										
										$total_relatorio = array();
										foreach($clientes as $cliente)
										{
											$relatorio .= '<tr style="border: 1px solid #cccccc;"><td style="border: 1px solid #cccccc;">';
												$cods_cliente = explode('|', $cliente);
												$relatorio .= $this->_obter_nome_cliente($cods_cliente[0], $cods_cliente[1]);
											$relatorio .= '</td>';
											
											foreach($_tab as $mes)
											{	
											
												$grafico[$representante][$mes . '/' . $ano]['data'] = $mes . '/' . $ano;
												
												$data_inicial = explode('/', '01/' . str_pad($mes, 2, "0", STR_PAD_LEFT) . '/' . $ano);
												$inicio_timestamp = mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
												
												$ultimo_dia = date('t', mktime(23, 59, 59, str_pad($mes, 2, "0", STR_PAD_LEFT), '01', $ano));
												
												$data_final = explode('/', $ultimo_dia . '/' . str_pad($mes, 2, "0", STR_PAD_LEFT) . '/' . $ano);
												$fim_timestamp = mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]);
												
												$meta_representante = $this->db->from('metas')->where(array(
													'tipo_meta' => 'meta_faturamento',
													'para' => 'cliente',
													'para_array' => $cliente,
													'data_inicial >=' => $inicio_timestamp,
													'data_final <= ' => $fim_timestamp
												))->get()->row();
												
												// Meta 
												$relatorio .= '<td  class="right" style="border: 1px solid #cccccc;">';
													$relatorio .= number_format($meta_representante->meta_valor, 2, ',', '.');
													$grafico[$representante][$mes . '/' . $ano]['meta'] = $meta_representante->meta_valor;
												$relatorio .= '</td>';
												
												$total_faturamento = $this->db_cliente->obter_faturamento(NULL, $inicio_timestamp, $fim_timestamp, NULL, $cliente);

												//Realizado
												$relatorio .= '<td  class="right" style="border: 1px solid #cccccc;">';
													$relatorio .= number_format($total_faturamento[0]['valor_total'], 2, ',', '.');
													$grafico[$representante][$mes . '/' . $ano]['realizado'] = $total_faturamento[0]['valor_total'];
												$relatorio .= '</td>';
												
												//Porcentagem
												$relatorio .= '<td  class="right" style="border: 1px solid #cccccc;">';

													$valor_met = $total_faturamento[0]['valor_total'];
													$valor_fat = $meta_representante->meta_valor;
													
													if(!empty($valor_met) AND !empty($valor_fat))
													{
														$porcentagem = ($valor_met / $valor_fat) * 100;
														$relatorio .= number_format($porcentagem, 2, ',', '.') . '%';
													}
													else
													{
														$relatorio .= number_format(0, 2, ',', '.') . '%';
													}
													
												$relatorio .= '</td>';
												
												
												//Somando Total
												$total_relatorio[$mes] = $total_relatorio[$mes] + $total_faturamento[0]['valor_total'];
												
											}
											
											$relatorio .= '</tr>';
										}
										
										
										//Exibindo Total
										$relatorio .= '<thead>';
										$relatorio .= '<tr>
										<th>Total Realizado</th style="border: 1px solid #cccccc;">';
										if($total_relatorio)
										{
											foreach($total_relatorio as $total)
											{
												$relatorio .= '<th colspan="3" style="border: 1px solid #cccccc;">' . number_format($total, 2, ',', '.') . '</th>';
											}
										}
										$relatorio .= '</tr>';
										$relatorio .= '</thead>';
										
										$relatorio .= '<tr>';
										$relatorio .= '<td colspan="10" style="height: 30px;"></td>';
										$relatorio .= '</tr>';
									
									
								}
								$relatorio .= '</table>';
								
								
						}
					
					}
					
				}
				
				$conteudo = '
					<div class="caixa">
						<ul>
							<li>' . anchor('metas/relatorio_faturamento/cliente', 'Voltar', 'onclick="history.go(-1); return false;"') . '</li>
						</ul>
					</div>
				';
				//if(!$titulo)
					$conteudo .= heading('Relatórios de Faturamento - Clientes', 2);
				
				if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
				$conteudo .= '<br />';
				
				//$conteudo .= form_open(current_url(), 'class="nao_exibir_impressao"');
				$conteudo .= form_open(current_url(), array('method' => 'get', 'class' => 'nao_exibir_impressao'));
				
				$cod_representante .= $this->db->from('usuarios')->where('id' , $this->input->get('representante'))->get()->row()->codigo;
				$conteudo .= '<p>Clientes:' . br() . form_dropdown('para_array[]', $this->_obter_clientes($cod_representante), $this->input->get('para_array'), ' multiple="multiple" class="multi_select" style="width: 490px;" ') . '</p>';
				
				$conteudo .= '<p style="float: left; margin-right: 10px;">Período:' . br() . form_dropdown('mes_inicial', $this->_obter_meses(), $mes_inicial, 'dojoType="dojox.form.DropDownSelect"') . ' a ' . form_dropdown('mes_final', $this->_obter_meses(), $mes_final, 'dojoType="dojox.form.DropDownSelect"') . '</p>';
				$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Ano:' . br() . form_dropdown('ano', array_combine(array_values(range(date('Y'), 2009)), array_values(range(date('Y'), 2009))), $ano, 'dojoType="dojox.form.DropDownSelect"')) . '</p>';
				$conteudo .= '<div style="clear: both;"></div>';
				
				$conteudo .= form_hidden('representante', $this->input->get('representante'));
				
				$conteudo .= '<p>' . form_submit('gerar_relatorio', 'Gerar Relatório');// . ' ou ' . anchor('metas/relatorio_faturamento/cliente' . $id, 'Voltar') . '</p>';
				$conteudo .= form_close();
				
				$conteudo .= '
				<script type="text/javascript">
					$(".multi_select").multiselect({
						checkAllText: "Marcar Todos",
						uncheckAllText: "Desmarcar Todos",
						noneSelectedText: "Selecione as opções",
						selectedText: "# Selecionado(s)"
					}).multiselectfilter({
						width: 130,
						label: "Filtrar",
						placeholder: "Digite palavras-chave"
					});
				</script>
				';
				
				
				$conteudo .= $relatorio;
				
				
				//$conteudo .= $this->criar_grafico($grafico);
				
			}
			else
			{
				$conteudo .= heading('Relatórios de Faturamento - Clientes', 2);
				$conteudo .= '<br />';
				//$conteudo .= form_open(current_url());
				$conteudo .= form_open(current_url(), array('method' => 'get'));
				$conteudo .= '<p>' . form_label('Representantes:' . br() . form_dropdown('representante', $this->_obter_representantes(), $this->input->get('representante'))) . '</p>';
				$conteudo .= '<p>' . form_submit('continuar', 'Continuar') . '</p>';
				$conteudo .= '<br />';
				$conteudo .= form_close();
			}
		}
		else if($para == 'produto')
		{
			$titulo = false;
			
			if($_GET)
			{
				
				if($this->input->get("gerar_relatorio"))
				{
					
					$mes_inicial = $this->input->get('mes_inicial') ? $this->input->get('mes_inicial') : '01';
					$mes_final = $this->input->get('mes_final') ? $this->input->get('mes_final') : 12;
					$ano = $this->input->get('ano') ? $this->input->get('ano') : date('Y');
					
					if ($mes_inicial > $mes_final)
					{
						$erro = 'O período selecionado é inválido.';
					}
					else if(!$this->input->get('para_array'))
					{
						$erro = "Selecione um produto.";
					}
					else
					{
					
						$conteudo .= '
							<div class="caixa">
								<ul>
									<li>' . anchor('#', 'Imprimir', 'onclick="print(); return false;"') . '</li>
								</ul>
							</div>
						';
						/*
						$conteudo .= '
							<div class="caixa" style="margin-right: 10px">
								<ul>
									<li>' . anchor('metas/visualizar_grafico', 'Visualizar Gráfico') . '</li>
								</ul>
							</div>
						';*/
						
						if(!$titulo){
							$titulo = true;
							$conteudo .= heading('Relatórios de Faturamento - Produtos', 2);
						}
					
						//Listagem
						$cds_produtos = $this->input->get('para_array');
						if($cds_produtos)
						{
						
							$produtos = $cds_produtos;
							
							
							//Imprimir
							$nome_grupo .= $this->db_cliente->obter_nome_grupo_produtos($this->input->get('grupo_produtos'));

							$relatorio .= heading('<br /><br />Grupo de Produtos: ' . $nome_grupo, 4);
							$relatorio .= '<div style="clear:both"></div>';
							
							//----------------------------------
							// Algoritimo para a tabela de relatório
							
							$total_meses = $mes_final - ($mes_inicial - 1);
							$divisor = ceil($total_meses / 3);
							
							$mi = $mes_inicial;
							
							for($tab = 1; $tab <= $divisor; $tab++)
							{

								$_m = array();
								for($m = $mi; $m <= ($mi + 2); $m++)
								{
									if($m <= $mes_final)
									{
										$_m[] = $m;
									}
									
									$mf = $m;
								}
								
								$mi = $mf + 1;

								$_tabs[] = $_m;
							}
							
							
							//----------------------------------
							
							

							$relatorio .= '<table class="hnordt" style="border: none;">';
							foreach($_tabs as $_tab){
								
								$nome_meses = $this->_obter_meses();
								
								$relatorio .= '<thead style="border: 1px solid #cccccc;">';
								$relatorio .= '<th rowspan="2">Produtos</th>';
								foreach($_tab as $mes)
								{								
									$relatorio .= '<th colspan="3">';
									$relatorio .= $nome_meses[str_pad($mes, 2, "0", STR_PAD_LEFT)] . '/' . $ano;
									$relatorio .= '</th>';
								}
								
								// Título - Meta x Realizado
								$relatorio .= '<tr>';
								foreach($_tab as $mes)
								{								
									$relatorio .= '<th>Meta (R$)</th>';
									$relatorio .= '<th colspan="2">Realizado (R$)</th>';
								}
								$relatorio .= '</tr>';
								
								$relatorio .= '</thead>';
								
								$relatorio .= '<tbody>';
								
								$total_relatorio = array();
								foreach($produtos as $produto)
								{
									$relatorio .= '<tr style="border: 1px solid #cccccc;"><td style="border: 1px solid #cccccc;">';
									
									$relatorio .= $this->_obter_nome_produto($produto);
									
									$relatorio .= '</td>';
									
									foreach($_tab as $mes)
									{	

										$grafico[$representante][$mes . '/' . $ano]['data'] = $mes . '/' . $ano;
										
										$data_inicial = explode('/', '01/' . str_pad($mes, 2, "0", STR_PAD_LEFT) . '/' . $ano);
										$inicio_timestamp = mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
										
										$ultimo_dia = date('t', mktime(23, 59, 59, str_pad($mes, 2, "0", STR_PAD_LEFT), '01', $ano));
										
										$data_final = explode('/', $ultimo_dia . '/' . str_pad($mes, 2, "0", STR_PAD_LEFT) . '/' . $ano);
										$fim_timestamp = mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]);
										
										$meta_representante = $this->db->from('metas')->where(array(
											'tipo_meta' => 'meta_faturamento',
											'para' => 'produto',
											'para_array' => $produto,
											'data_inicial >=' => $inicio_timestamp,
											'data_final <= ' => $fim_timestamp
										))->get()->row();
										
										// Meta 
										$relatorio .= '<td class="right" style="border: 1px solid #cccccc;">';
											$relatorio .= number_format($meta_representante->meta_valor, 2, ',', '.');
											$grafico[$representante][$mes . '/' . $ano]['meta'] = $meta_representante->meta_valor;
										$relatorio .= '</td>';
										
										$total_faturamento = $this->db_cliente->obter_faturamento(NULL, $inicio_timestamp, $fim_timestamp, NULL, NULL, $produto);
										
										//Realizado
										$relatorio .= '<td class="right" style="border: 1px solid #cccccc;">';
											$relatorio .= number_format($total_faturamento[0]['valor_total'], 2, ',', '.');
											$grafico[$representante][$mes . '/' . $ano]['realizado'] = $total_faturamento[0]['valor_total'];
										$relatorio .= '</td>';
										

										//Porcentagem
										$relatorio .= '<td class="right" style="border: 1px solid #cccccc;">';

											$valor_met = $total_faturamento[0]['valor_total'];
											$valor_fat = $meta_representante->meta_valor;
											
											if(!empty($valor_met) AND !empty($valor_fat))
											{
												$porcentagem = ($valor_met / $valor_fat) * 100;
												$relatorio .= number_format($porcentagem, 2, ',', '.') . '%';
											}
											else
											{
												$relatorio .= number_format(0, 2, ',', '.') . '%';
											}
											
										$relatorio .= '</td>';
										
										//Somando Total
										$total_relatorio[$mes] = $total_relatorio[$mes] + $total_faturamento[0]['valor_total'];
										
									}
									
									$relatorio .= '</tr>';
								}
								
								//Exibindo Total
								$relatorio .= '<thead>';
								$relatorio .= '<tr style="border: 1px solid #cccccc;">
								<th>Total Realizado (R$)</th>';
								foreach($total_relatorio as $total)
								{
									$relatorio .= '<th colspan="3">' . number_format($total, 2, ',', '.') . '</th>';
								}
								$relatorio .= '</tr>';
								$relatorio .= '</thead>';
								
								$relatorio .= '<tr>';
								$relatorio .= '<td colspan="10" style="height: 30px;"></td>';
								$relatorio .= '</tr>';
								
							}
								
							$relatorio .= '</table>';

						
						}
					
					}
					
				}
				
				$conteudo = '
							<div class="caixa">
								<ul>
									<li>' . anchor('metas/relatorio_faturamento/produto', 'Voltar', 'onclick="history.go(-1); return false;"') . '</li>
								</ul>
							</div>
						';
				
				//if(!$titulo)
					$conteudo .= heading('Relatórios de Faturamento - Produtos', 2);
				
				if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
				$conteudo .= '<br />';
				
				//$conteudo .= form_open(current_url(), 'class="nao_exibir_impressao"');
				$conteudo .= form_open(current_url(), array('method' => 'get', 'class' => 'nao_exibir_impressao'));
				
				$conteudo .= '<p>Produtos:' . br() . form_dropdown('para_array[]', $this->_obter_produtos($this->input->get('grupo_produtos')), $this->input->get('para_array'), ' multiple="multiple" class="multi_select" style="width: 490px;" ') . '</p>';
				
				$conteudo .= '<p style="float: left; margin-right: 10px;">Período:' . br() . form_dropdown('mes_inicial', $this->_obter_meses(), $mes_inicial, 'dojoType="dojox.form.DropDownSelect"') . ' a ' . form_dropdown('mes_final', $this->_obter_meses(), $mes_final, 'dojoType="dojox.form.DropDownSelect"') . '</p>';
				$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Ano:' . br() . form_dropdown('ano', array_combine(array_values(range(date('Y'), 2009)), array_values(range(date('Y'), 2009))), $ano, 'dojoType="dojox.form.DropDownSelect"')) . '</p>';
				$conteudo .= '<div style="clear: both;"></div>';
				
				$conteudo .= form_hidden('grupo_produtos', $this->input->get('grupo_produtos'));
				
				$conteudo .= '<p>' . form_submit('gerar_relatorio', 'Gerar Relatório');// . ' ou ' . anchor('metas/relatorio_faturamento/produto' . $id, 'Voltar') . '</p>';
				$conteudo .= form_close();
				
				$conteudo .= '
				<script type="text/javascript">
					$(".multi_select").multiselect({
						checkAllText: "Marcar Todos",
						uncheckAllText: "Desmarcar Todos",
						noneSelectedText: "Selecione as opções",
						selectedText: "# Selecionado(s)"
					}).multiselectfilter({
						width: 130,
						label: "Filtrar",
						placeholder: "Digite palavras-chave"
					});
				</script>
				';
		

				$conteudo .= $relatorio;
				
				//$conteudo .= $this->criar_grafico($grafico);
				
			}
			else
			{
				$conteudo .= '<br />';
				//$conteudo .= form_open(current_url());
				$conteudo .= form_open(current_url(), array('method' => 'get'));
				$conteudo .= '<p>' . form_label('Grupo de Produtos:' . br() . form_dropdown('grupo_produtos', $this->_obter_grupo_produtos(), $this->input->get('grupo_produtos'))) . '</p>';
				$conteudo .= '<p>' . form_submit('continuar', 'Continuar');// . ' ou ' . anchor('metas/relatorios/' . $id, 'Voltar') . '</p>';
				$conteudo .= '<br />';
				$conteudo .= form_close();
			}
			
		}
		else if($para == 'estado')
		{
			$titulo = false;
			
			if($_GET)
			{
			
				$mes_inicial = $this->input->get('mes_inicial') ? $this->input->get('mes_inicial') : '01';
				$mes_final = $this->input->get('mes_final') ? $this->input->get('mes_final') : 12;
				$ano = $this->input->get('ano') ? $this->input->get('ano') : date('Y');
				
				if ($mes_inicial > $mes_final)
				{
					$erro = 'O período selecionado é inválido.';
				}
				else if(!$this->input->get('para_array'))
				{
					$erro = "Selecione um estado.";
				}
				else
				{
					$conteudo .= '
						<div class="caixa">
							<ul>
								<li>' . anchor('#', 'Imprimir', 'onclick="print(); return false;"') . '</li>
							</ul>
						</div>
					';
					/*
					$conteudo .= '
						<div class="caixa" style="margin-right: 10px">
							<ul>
								<li>' . anchor('metas/visualizar_grafico', 'Visualizar Gráfico') . '</li>
							</ul>
						</div>
					';*/
					
					if(!$titulo){
						$titulo = true;
						$conteudo .= heading('Relatórios de Faturamento - Estados', 2);
					}
				
					//Listagem
					$estados = $this->input->get('para_array');
					if($estados)
					{
						
						
						
						$relatorio  .= '<div style="clear:both"></div>';
						
						//----------------------------------
						// Algoritimo para a tabela de relatório
						
						$total_meses = $mes_final - ($mes_inicial - 1);
						$divisor = ceil($total_meses / 3);
						
						$mi = $mes_inicial;
						
						for($tab = 1; $tab <= $divisor; $tab++)
						{

							$_m = array();
							for($m = $mi; $m <= ($mi + 2); $m++)
							{
								if($m <= $mes_final)
								{
									$_m[] = $m;
								}
								
								$mf = $m;
							}
							
							$mi = $mf + 1;

							$_tabs[] = $_m;
						}
						
						
						//----------------------------------
		
						$relatorio .= '<table class="hnordt" style="border: none;">';
						foreach($_tabs as $_tab){
							
							$nome_meses = $this->_obter_meses();
							
							$relatorio .= '<thead style="border: 1px solid #cccccc;">';
							$relatorio .= '<th rowspan="2">Estados</th>';
							foreach($_tab as $mes)
							{							
								$relatorio .= '<th colspan="3">';
								$relatorio .= $nome_meses[str_pad($mes, 2, "0", STR_PAD_LEFT)] . '/' . $ano;
								$relatorio .= '</th>';
							}
							
							$relatorio .= '<tr>';
							foreach($_tab as $mes)
							{						
								$relatorio .= '<th>Meta (R$)</th>';
								$relatorio .= '<th colspan="2">Realizado (R$)</th>';
							}
							$relatorio .= '</tr>';
							
							$relatorio .= '</thead>';
							
							$relatorio .= '<tbody>';
							
							$total_relatorio = array();
							foreach($estados as $estado)
							{
								$relatorio .= '<tr style="border: 1px solid #cccccc;"><td  class="center" style="border: 1px solid #cccccc;">';
									
								$relatorio .= $estado;
								
								$relatorio .= '</td>';
								
								foreach($_tab as $mes)
								{
									
									$grafico[$representante][$mes . '/' . $ano]['data'] = $mes . '/' . $ano;
									
									$data_inicial = explode('/', '01/' . str_pad($mes, 2, "0", STR_PAD_LEFT) . '/' . $ano);
									$inicio_timestamp = mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
									
									$ultimo_dia = date('t', mktime(23, 59, 59, str_pad($mes, 2, "0", STR_PAD_LEFT), '01', $ano));
									
									$data_final = explode('/', $ultimo_dia . '/' . str_pad($mes, 2, "0", STR_PAD_LEFT) . '/' . $ano);
									$fim_timestamp = mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]);
									
									$meta_representante = $this->db->from('metas')->where(array(
										'tipo_meta' => 'meta_faturamento',
										'para' => 'estado',
										'para_array' => $estado,
										'data_inicial >=' => $inicio_timestamp,
										'data_final <= ' => $fim_timestamp
									))->get()->row();
									
									//Meta
									$relatorio .= '<td class="right" style="border: 1px solid #cccccc;">';
										$relatorio .= number_format($meta_representante->meta_valor, 2, ',', '.');
										$grafico[$representante][$mes . '/' . $ano]['meta'] = $meta_representante->meta_valor;
									$relatorio .= '</td>';
									
									$total_faturamento = $this->db_cliente->obter_faturamento(NULL, $inicio_timestamp, $fim_timestamp, $estado);
									
									//Realizado
									$valor_total = $total_faturamento[0];
									
									
									$relatorio .= '<td class="right" style="border: 1px solid #cccccc;">';		
										$relatorio .= number_format($teste, 2, ',', '.');
										$grafico[$representante][$mes . '/' . $ano]['realizado'] = $valor_total['valor_total'];
									$relatorio .= '</td>';
									
									//Porcentagem
									$relatorio .= '<td class="right" style="border: 1px solid #cccccc;">';

										$valor_met = $valor_total['valor_total'];
										$valor_fat = $meta_representante->meta_valor;
										
										if(!empty($valor_met) AND !empty($valor_fat))
										{
											$porcentagem = ($valor_met / $valor_fat) * 100;
											$relatorio .= number_format($porcentagem, 2, ',', '.') . '%';
										}
										else
										{
											$relatorio .= number_format(0, 2, ',', '.') . '%';
										}
										
									$relatorio .= '</td>';
									
									//Somando Total
									$total_relatorio[$mes] = $total_relatorio[$mes] + $valor_total['valor_total'];
									
								}
								
								$relatorio .= '</tr>';
							}
							
							//Exibindo Total
							$relatorio .= '<thead>';
							$relatorio .= '<tr style="border: 1px solid #cccccc;">
							<th>Total Realizado (R$)</th>';
							foreach($total_relatorio as $total)
							{
								$relatorio .= '<th colspan="3">' . number_format($total, 2, ',', '.') . '</th>';
							}
							$relatorio .= '</tr>';
							$relatorio .= '</thead>';
							
							$relatorio .= '<tr>';
							$relatorio .= '<td colspan="10" style="height: 30px;"></td>';
							$relatorio .= '</tr>';
							
							
						}
							
						$relatorio .= '</table>';
						
					}
				
				}
			
			}

			if(!$titulo){
				$titulo = true;
				$conteudo .= heading('Relatórios de Faturamento - Estados', 2);
			}
			
			if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
			$conteudo .= '<br />';
			
			//$conteudo .= form_open(current_url(), 'class="nao_exibir_impressao"');
			$conteudo .= form_open(current_url(), array('method' => 'get', 'class' => 'nao_exibir_impressao'));
			
			$conteudo .= '<p>Estados:' . br() . form_dropdown('para_array[]', $this->db_cliente->obter_grupo_estados(), $this->input->get('para_array'), ' multiple="multiple" class="multi_select" style="width: 490px;" ') . '</p>';
			
			$conteudo .= '<p style="float: left; margin-right: 10px;">Período:' . br() . form_dropdown('mes_inicial', $this->_obter_meses(), $mes_inicial, 'dojoType="dojox.form.DropDownSelect"') . ' a ' . form_dropdown('mes_final', $this->_obter_meses(), $mes_final, 'dojoType="dojox.form.DropDownSelect"') . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Ano:' . br() . form_dropdown('ano', array_combine(array_values(range(date('Y'), 2009)), array_values(range(date('Y'), 2009))), $ano, 'dojoType="dojox.form.DropDownSelect"')) . '</p>';
			$conteudo .= '<div style="clear: both;"></div>';
			
			$conteudo .= '<p>' . form_submit('gerar_relatorio', 'Gerar Relatório');// . ' ou ' . anchor('metas/relatorios/estado' . $id, 'Voltar') . '</p>';
			$conteudo .= form_close();
			
			$conteudo .= '
			<script type="text/javascript">
				$(".multi_select").multiselect({
					checkAllText: "Marcar Todos",
					uncheckAllText: "Desmarcar Todos",
					noneSelectedText: "Selecione as opções",
					selectedText: "# Selecionado(s)"
				}).multiselectfilter({
					width: 130,
					label: "Filtrar",
					placeholder: "Digite palavras-chave"
				});
			</script>
			';
	

			$conteudo .= $relatorio;
			
			//$conteudo .= $this->criar_grafico($grafico);
			
		}
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	
	function visualizar_grafico($tipo = NULL)
	{
		$conteudo .= heading('Gráfico', 2);
		
		$conteudo .= '
			<div class="caixa">
				<ul>
					<li>' . anchor('#', 'Imprimir', 'onclick="print(); return false;"') . '</li>
				</ul>
			</div>
		';
		
		$conteudo .= anchor('#', 'Voltar', 'onclick="history.go(-1); return false;"');
		
		if($tipo == 'quantidade')
		{
	
			$conteudo .= '<p style="height: 400px">';
			$conteudo .= $this->_gerar_grafico(array('tipo' => 'MSColumn3D', 'url_dados' => site_url('metas/grafico_quantidade_xml')));
			$conteudo .= '<p>';
		
		}
		else
		{
		
			$conteudo .= '<p style="height: 400px">';
			$conteudo .= $this->_gerar_grafico(array('tipo' => 'MSColumn3D', 'url_dados' => site_url('metas/grafico_faturamento_xml')));
			$conteudo .= '<p>';
			
		}
	
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	function criar_grafico($grafico)
	{
		
		if($grafico)
		{
		
			foreach($grafico as $gf)
			{
				
				foreach($gf as $_gf)
				{
					
					$_datas[$_gf['data']] = $_gf['data'];
					$_realizados[$_gf['data']] += $_gf['realizado'];
					$_metas[$_gf['data']] += $_gf['meta'];
					
				}

			}
			
			foreach($_datas as $_data)
			{
				$_grafico[$_data]['data'] = $_data;
				$_grafico[$_data]['realizado'] = $_realizados[$_data];
				$_grafico[$_data]['meta'] = $_metas[$_data];
			}
			
			$this->db->where('id', '1');
			$this->db->update('graficos', array(
				'dados' => serialize($_grafico)
			));
			
			
			
			return $conteudo;
		}
		
	}
	
	function grafico_faturamento_xml()
	{
		
		$grafico_xml = unserialize($this->db->from('graficos')->where('id', '1')->get()->row()->dados);
		
		
		if($grafico_xml)
		{

		$xml .= '<chart caption="" showValues="0" formatNumberScale="0" numberPrefix="R$ " decimalSeparator="," thousandSeparator=".">';

			$xml .= '<categories>';
			foreach($grafico_xml as $_gf)
			{
				$_data = explode('/', $_gf['data']);
				$meses = $this->_obter_meses();
			
				$xml .= '<category label="' .  $meses[str_pad($_data[0], 2, "0", STR_PAD_LEFT)] . '/' . $_data[1] . '" />';
			}
			$xml .= '</categories>';

			$xml .= '<dataset seriesName="Meta" color="AFD8F8" showValues="0">';
			foreach($grafico_xml as $_gf)
			{
				$xml .= '<set value="' . $_gf['meta'] . '" />';
			}
			$xml .= '</dataset>';

			$xml .= '<dataset seriesName="Realizado" color="F6BD0F" showValues="0">';
			foreach($grafico_xml as $_gf)
			{
				$xml .= '<set value="' . $_gf['realizado'] . '" />';
			}
			$xml .= '</dataset>';
			
		$xml .= '</chart>';
		}

		echo utf8_decode($xml);
		
	}
	
	function grafico_quantidade_xml()
	{
		
		$grafico_xml = unserialize($this->db->from('graficos')->where('id', '1')->get()->row()->dados);
		
		
		if($grafico_xml)
		{

		$xml .= '<chart caption="" showValues="0" formatNumberScale="0">';

			$xml .= '<categories>';
			foreach($grafico_xml as $_gf)
			{
				$_data = explode('/', $_gf['data']);
				$meses = $this->_obter_meses();
			
				$xml .= '<category label="' .  $meses[str_pad($_data[0], 2, "0", STR_PAD_LEFT)] . '/' . $_data[1] . '" />';
			}
			$xml .= '</categories>';

			$xml .= '<dataset seriesName="Meta" color="AFD8F8" showValues="0">';
			foreach($grafico_xml as $_gf)
			{
				$xml .= '<set value="' . $_gf['meta'] . '" />';
			}
			$xml .= '</dataset>';

			$xml .= '<dataset seriesName="Realizado" color="F6BD0F" showValues="0">';
			foreach($grafico_xml as $_gf)
			{
				$xml .= '<set value="' . $_gf['realizado'] . '" />';
			}
			$xml .= '</dataset>';
			
		$xml .= '</chart>';
		}

		echo utf8_decode($xml);
		
	}
	
	//-----=-=-----=-=-----=-=-----

	
	function relatorio_prospeccao($para = NULL)
	{
		if($para == 'representante')
		{
			$titulo = false;
		
			if($_GET)
			{
				$mes_inicial = $this->input->get('mes_inicial') ? $this->input->get('mes_inicial') : '01';
				$mes_final = $this->input->get('mes_final') ? $this->input->get('mes_final') : 12;
				$ano = $this->input->get('ano') ? $this->input->get('ano') : date('Y');
				
				if ($mes_inicial > $mes_final)
				{
					$erro = 'O período selecionado é inválido.';
				}
				else if(!$this->input->get('para_array'))
				{
					$erro = "Selecione um representante.";
				}
				else
				{
					/*
					$conteudo .= '
						<div class="caixa" style="margin-right: 10px">
							<ul>
								<li>' . anchor('metas/visualizar_grafico/quantidade', 'Visualizar Gráfico') . '</li>
							</ul>
						</div>
					';*/
					
					$conteudo .= '
						<div class="caixa">
							<ul>
								<li>' . anchor('#', 'Imprimir', 'onclick="print(); return false;"') . '</li>
							</ul>
						</div>
					';
					
					if(!$titulo){
							$titulo = true;
							$conteudo .= heading('Relatórios de Prospecção - Representantes', 2);
						}
					
					//Listagem
					$ids_representantes = $this->input->get('para_array');
					if($ids_representantes)
					{						

							$representantes = $ids_representantes;
							
							//Imprimir
							$relatorio  .= '<div style="clear:both"></div>';
							
							
							//----------------------------------
							// Algoritimo para a tabela de relatório
							
							$total_meses = $mes_final - ($mes_inicial - 1);
							$divisor = ceil($total_meses / 3);
							
							$mi = $mes_inicial;
							
							for($tab = 1; $tab <= $divisor; $tab++)
							{

								$_m = array();
								for($m = $mi; $m <= ($mi + 2); $m++)
								{
									if($m <= $mes_final)
									{
										$_m[] = $m;
									}
									
									$mf = $m;
								}
								
								$mi = $mf + 1;

								$_tabs[] = $_m;
							}
							
							
							//----------------------------------
							
							
							$relatorio .= '<table class="hnordt" style="border: none;">';
							foreach($_tabs as $_tab){
								
								$nome_meses = $this->_obter_meses();
								
								$relatorio .= '<thead style="border: 1px solid #cccccc;">';
								$relatorio .= '<th rowspan="2">Representante</th>';
								foreach($_tab as $mes)
								{							
									$relatorio .= '<th colspan="3">';
									$relatorio .= $nome_meses[str_pad($mes, 2, "0", STR_PAD_LEFT)] . '/' . $ano;
									$relatorio .= '</th>';
								}
								
								// Título - Meta x Realizado
								$relatorio .= '<tr>';
								foreach($_tab as $mes)
								{						
									$relatorio .= '<th>Meta</th>';
									$relatorio .= '<th colspan="2">Realizado</th>';
								}
								$relatorio .= '</tr>';
								
								$relatorio .= '</thead>';
								
								$relatorio .= '<tbody>';
								
								$total_relatorio = array();
								foreach($representantes as $representante)
								{
									$relatorio .= '<tr style="border: 1px solid #cccccc;"><td style="border: 1px solid #cccccc;">';
									
									$relatorio .= $this->db->from('usuarios')->where('id' , $representante)->get()->row()->codigo . ' - ' . $this->db->from('usuarios')->where('id' , $representante)->get()->row()->nome;
									$relatorio .= '</td>';
									
									foreach($_tab as $mes)
									{
										
										$grafico[$representante][$mes . '/' . $ano]['data'] = $mes . '/' . $ano;
										
										$data_inicial = explode('/', '01/' . str_pad($mes, 2, "0", STR_PAD_LEFT) . '/' . $ano);
										$inicio_timestamp = mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
										
										$ultimo_dia = date('t', mktime(23, 59, 59, str_pad($mes, 2, "0", STR_PAD_LEFT), '01', $ano));
										
										$data_final = explode('/', $ultimo_dia . '/' . str_pad($mes, 2, "0", STR_PAD_LEFT) . '/' . $ano);
										$fim_timestamp = mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]);
										
										$meta_representante = $this->db->from('metas')->where(array(
											'tipo_meta' => 'meta_prospeccao',
											'para' => 'representante',
											'para_array' => $representante,
											'data_inicial >=' => $inicio_timestamp,
											'data_final <= ' => $fim_timestamp
										))->get()->row();
										
										//Meta
										$relatorio .= '<td style="border: 1px solid #cccccc;">';
											$relatorio .= ($meta_representante->meta_quantidade ? $meta_representante->meta_quantidade : '0');
											$grafico[$representante][$mes . '/' . $ano]['meta'] = ($meta_representante->meta_quantidade ? $meta_representante->meta_quantidade : '0');
										$relatorio .= '</td>';
										
										$total = $this->_obter_prospeccao($this->db->from('usuarios')->where('id' , $representante)->get()->row()->codigo, $inicio_timestamp, $fim_timestamp);
		
										//print_r($total);
			
										// Realizado
										$relatorio .= '<td style="border: 1px solid #cccccc;">';
										$relatorio .= $total->total;
										$grafico[$representante][$mes . '/' . $ano]['realizado'] = $total->total;
										$relatorio .= '</td>';
										
										//Porcentagem
										$relatorio .= '<td style="border: 1px solid #cccccc;">';

											$valor_met = $meta_representante->meta_quantidade;
											$valor_fat = $total->total;
											
											if(!empty($valor_met) AND !empty($valor_fat))
											{
												$porcentagem = ($valor_fat / $valor_met) * 100;
												$relatorio .= $porcentagem . '%';
											}
											else
											{
												$relatorio .= '0%';
											}
											
										$relatorio .= '</td>';
										
										$total_relatorio[$mes] = $total_relatorio[$mes] + $total->total;
									}
									
									$relatorio .= '</tr>';
								}
								
								//Exibindo Total
								$relatorio .= '<thead>';
								$relatorio .= '<tr style="border: 1px solid #cccccc;">
								<th>Total Realizado (R$)</th>';
								foreach($total_relatorio as $total)
								{
									$relatorio .= '<th colspan="3">' . $total . '</th>';
								}
								$relatorio .= '</tr>';
								$relatorio .= '</thead>';
								
								$relatorio .= '<tr>';
								$relatorio .= '<td colspan="10" style="height: 30px;"></td>';
								$relatorio .= '</tr>';
								
							}
							
							$relatorio .= '</table>';
							
						
					}
				
				}
				
			
			}

			if(!$titulo)
				$conteudo .= heading('Relatórios de Novos Clientes - Estados', 2);

			if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
			$conteudo .= '<br />';
			
			//$conteudo .= form_open(current_url(), 'class="nao_exibir_impressao"');
			$conteudo .= form_open(current_url(), array('method' => 'get', 'class' => 'nao_exibir_impressao'));
			
			$conteudo .= '<p>Representantes:' . br() . form_dropdown('para_array[]', $this->_obter_representantes(), $this->input->get('para_array'), ' multiple="multiple" class="multi_select" style="width: 490px;" ') . '</p>';
			
			$conteudo .= '<p style="float: left; margin-right: 10px;">Período:' . br() . form_dropdown('mes_inicial', $this->_obter_meses(), $mes_inicial, 'dojoType="dojox.form.DropDownSelect"') . ' a ' . form_dropdown('mes_final', $this->_obter_meses(), $mes_final, 'dojoType="dojox.form.DropDownSelect"') . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Ano:' . br() . form_dropdown('ano', array_combine(array_values(range(date('Y'), 2009)), array_values(range(date('Y'), 2009))), $ano, 'dojoType="dojox.form.DropDownSelect"')) . '</p>';
			$conteudo .= '<div style="clear: both;"></div>';
			
			$conteudo .= '<p>' . form_submit('gerar_relatorio', 'Gerar Relatório');// . ' ou ' . anchor('metas/relatorios/representante' . $id, 'Voltar') . '</p>';
			$conteudo .= form_close();
			
			$conteudo .= '
			<script type="text/javascript">
				$(".multi_select").multiselect({
					checkAllText: "Marcar Todos",
					uncheckAllText: "Desmarcar Todos",
					noneSelectedText: "Selecione as opções",
					selectedText: "# Selecionado(s)"
				}).multiselectfilter({
					width: 130,
					label: "Filtrar",
					placeholder: "Digite palavras-chave"
				});
			</script>
			';
	

			$conteudo .= $relatorio;
			
			//$conteudo .= $this->criar_grafico($grafico);
			
		}
		else if($para == 'estado')
		{
			/*
			$conteudo .= '
				<div class="caixa">
					<ul>
						<li>' . anchor('metas', 'Voltar', 'onclick="history.go(-1); return false;"') . '</li>
					</ul>
				</div>
			';*/
			if($_GET)
				$conteudo .= '
						<div class="caixa">
							<ul>
								<li>' . anchor('#', 'Imprimir', 'onclick="print(); return false;"') . '</li>
							</ul>
						</div>
					';
			
			$conteudo .= heading('Relatórios de Prospecção - Estados', 2);
		
			if($_GET)
			{
				
				$mes_inicial = $this->input->get('mes_inicial') ? $this->input->get('mes_inicial') : '01';
				$mes_final = $this->input->get('mes_final') ? $this->input->get('mes_final') : 12;
				$ano = $this->input->get('ano') ? $this->input->get('ano') : date('Y');
				
				if ($mes_inicial > $mes_final)
				{
					$erro = 'O período selecionado é inválido.';
				}
				else if(!$this->input->get('para_array'))
				{
					$erro = "Selecione um representante.";
				}
				else
				{
				
					/*
					$conteudo .= '
						<div class="caixa" style="margin-right: 10px">
							<ul>
								<li>' . anchor('metas/visualizar_grafico/quantidade', 'Visualizar Gráfico') . '</li>
							</ul>
						</div>
					';*/
					
				
					//Listagem
					$estados = $this->input->get('para_array');
					if($estados)
					{						
							
							
							
							$relatorio  .= '<div style="clear:both"></div>';
							
							//----------------------------------
							// Algoritimo para a tabela de relatório
							
							$total_meses = $mes_final - ($mes_inicial - 1);
							$divisor = ceil($total_meses / 3);
							
							$mi = $mes_inicial;
							
							for($tab = 1; $tab <= $divisor; $tab++)
							{

								$_m = array();
								for($m = $mi; $m <= ($mi + 2); $m++)
								{
									if($m <= $mes_final)
									{
										$_m[] = $m;
									}
									
									$mf = $m;
								}
								
								$mi = $mf + 1;

								$_tabs[] = $_m;
							}
							
							
							//----------------------------------
							
							
							$relatorio .= '<table class="hnordt" style="border: none;">';
							foreach($_tabs as $_tab){
							
							
								$nome_meses = $this->_obter_meses();
								
								$relatorio .= '<thead style="border: 1px solid #cccccc;">';
								$relatorio .= '<th rowspan="2">Estados</th>';
								foreach($_tab as $mes)
								{							
									$relatorio .= '<th colspan="3">';
									$relatorio .= $nome_meses[str_pad($mes, 2, "0", STR_PAD_LEFT)] . '/' . $ano;
									$relatorio .= '</th>';
								}
								
								// Título - Meta x Realizado
								$relatorio .= '<tr>';
								foreach($_tab as $mes)
								{							
									$relatorio .= '<th>Meta</th>';
									$relatorio .= '<th colspan="2">Realizado</th>';
								}
								$relatorio .= '</tr>';
								
								$relatorio .= '</thead>';
								
								$relatorio .= '<tbody>';
								
								$total_relatorio = array();
								foreach($estados as $estado)
								{
									$relatorio .= '<tr style="border: 1px solid #cccccc;"><td style="border: 1px solid #cccccc;">';
									
									$relatorio .= $estado;
									$relatorio .= '</td>';
									
									foreach($_tab as $mes)
									{
									
										$grafico[$representante][$mes . '/' . $ano]['data'] = $mes . '/' . $ano;
										
										$data_inicial = explode('/', '01/' . str_pad($mes, 2, "0", STR_PAD_LEFT) . '/' . $ano);
										$inicio_timestamp = mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
										
										$ultimo_dia = date('t', mktime(23, 59, 59, str_pad($mes, 2, "0", STR_PAD_LEFT), '01', $ano));
										
										$data_final = explode('/', $ultimo_dia . '/' . str_pad($mes, 2, "0", STR_PAD_LEFT) . '/' . $ano);
										$fim_timestamp = mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]);
										
										$meta_representante = $this->db->from('metas')->where(array(
											'tipo_meta' => 'meta_prospeccao',
											'para' => 'estado',
											'para_array' => $estado,
											'data_inicial >=' => $inicio_timestamp,
											'data_final <= ' => $fim_timestamp
										))->get()->row();
										
										//Meta
										$relatorio .= '<td style="border: 1px solid #cccccc;">';
										$relatorio .= ($meta_representante->meta_quantidade ? $meta_representante->meta_quantidade : '0');
										$grafico[$representante][$mes . '/' . $ano]['meta'] = ($meta_representante->meta_quantidade ? $meta_representante->meta_quantidade : '0');
										$relatorio .= '</td>';
										
										$total = $this->_obter_prospeccao(NULL, $inicio_timestamp, $fim_timestamp, $estado);

										//print_r($total);

										// Realizado
										$relatorio .= '<td style="border: 1px solid #cccccc;">';
										$relatorio .= $total->total;
										$grafico[$representante][$mes . '/' . $ano]['realizado'] = $total->total;
										$relatorio .= '</td>';
										
										//Porcentagem
										$relatorio .= '<td style="border: 1px solid #cccccc;">';

											$valor_met = $meta_representante->meta_quantidade;
											$valor_fat = $total->total;
											
											if(!empty($valor_met) AND !empty($valor_fat))
											{
												$porcentagem = ($valor_fat / $valor_met) * 100;
												$relatorio .= $porcentagem . '%';
											}
											else
											{
												$relatorio .= '0%';
											}
											
										$relatorio .= '</td>';
										
										$total_relatorio[$mes] = $total_relatorio[$mes] + $total->total;
									}
									
									$relatorio .= '</tr>';
								}
								
								//Exibindo Total
								$relatorio .= '<thead>';
								$relatorio .= '<tr style="border: 1px solid #cccccc;">
								<th>Total Realizado (R$)</th>';
								foreach($total_relatorio as $total)
								{
									$relatorio .= '<th colspan="3">' . $total . '</th>';
								}
								$relatorio .= '</tr>';
								$relatorio .= '</thead>';
								
								$relatorio .= '<tr>';
								$relatorio .= '<td colspan="10" style="height: 30px;"></td>';
								$relatorio .= '</tr>';
								
							}
							
							$relatorio .= '</table>';
							
						
					}
				
				}
				
			}

			
			if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
			$conteudo .= '<br />';
			
			//$conteudo .= form_open(current_url(), 'class="nao_exibir_impressao"');
			$conteudo .= form_open(current_url(), array('method' => 'get', 'class' => 'nao_exibir_impressao'));
			
			$conteudo .= '<p>Estados:' . br() . form_dropdown('para_array[]', $this->_obter_estados(), $this->input->get('para_array'), ' multiple="multiple" class="multi_select" style="width: 490px;" ') . '</p>';
			
			$conteudo .= '<p style="float: left; margin-right: 10px;">Período:' . br() . form_dropdown('mes_inicial', $this->_obter_meses(), $mes_inicial, 'dojoType="dojox.form.DropDownSelect"') . ' a ' . form_dropdown('mes_final', $this->_obter_meses(), $mes_final, 'dojoType="dojox.form.DropDownSelect"') . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Ano:' . br() . form_dropdown('ano', array_combine(array_values(range(date('Y'), 2009)), array_values(range(date('Y'), 2009))), $ano, 'dojoType="dojox.form.DropDownSelect"')) . '</p>';
			$conteudo .= '<div style="clear: both;"></div>';
			
			$conteudo .= '<p>' . form_submit('gerar_relatorio', 'Gerar Relatório');// . ' ou ' . anchor('metas/relatorios/estado' . $id, 'Voltar') . '</p>';
			$conteudo .= form_close();
			
			$conteudo .= '
			<script type="text/javascript">
				$(".multi_select").multiselect({
					checkAllText: "Marcar Todos",
					uncheckAllText: "Desmarcar Todos",
					noneSelectedText: "Selecione as opções",
					selectedText: "# Selecionado(s)"
				}).multiselectfilter({
					width: 130,
					label: "Filtrar",
					placeholder: "Digite palavras-chave"
				});
			</script>
			';


			$conteudo .= $relatorio;
			
			//$conteudo .= $this->criar_grafico($grafico);
			
		}
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}
	
	
	
	function _obter_prospeccao($codigo_representante = NULL, $data_inicial_timestamp = NULL, $data_final_timestamp = NULL, $estado = NULL)
	{
		if($codigo_representante)
		{
			if ($data_inicial_timestamp)
			{
				$this->db->where('timestamp >=', $data_inicial_timestamp);
			}
			
			if ($data_final_timestamp)
			{
				$this->db->where('timestamp <=', $data_final_timestamp);
			}
			
			
			return $this->db->select('COUNT(*) AS total')->from('prospects')->where(array(
				'codigo_usuario' => $codigo_representante,
				'status' => 'convertido_cliente'
			))->get()->row();
			
			
			//return $this->db->last_query();
			
		}
		else if($estado)
		{
			
			if ($data_inicial_timestamp)
			{
				$this->db->where('timestamp >=', $data_inicial_timestamp);
			}
			
			if ($data_final_timestamp)
			{
				$this->db->where('timestamp <=', $data_final_timestamp);
			}
			
			return $this->db->select('COUNT(*) AS total')->from('prospects')->where(array(
				'estado' => $estado,
				'status' => 'convertido_cliente'
			))->get()->row();
			
		}
	}

	
	function relatorio_novos_clientes($para = NULL)
	{
		if($para == 'representante')
		{
			$titulo = false;
			
			if($_GET)
			{
				$mes_inicial = $this->input->get('mes_inicial') ? $this->input->get('mes_inicial') : '01';
				$mes_final = $this->input->get('mes_final') ? $this->input->get('mes_final') : 12;
				$ano = $this->input->get('ano') ? $this->input->get('ano') : date('Y');
				
				if ($mes_inicial > $mes_final)
				{
					$erro = 'O período selecionado é inválido.';
				}
				else if(!$this->input->get('para_array'))
				{
					$erro = "Selecione um representante.";
				}
				else
				{
				
					$conteudo .= '
						<div class="caixa">
							<ul>
								<li>' . anchor('#', 'Imprimir', 'onclick="print(); return false;"') . '</li>
							</ul>
						</div>
					';
					/*
					$conteudo .= '
						<div class="caixa" style="margin-right: 10px">
							<ul>
								<li>' . anchor('metas/visualizar_grafico/quantidade', 'Visualizar Gráfico') . '</li>
							</ul>
						</div>
					';
					*/
					if(!$titulo){
						$titulo = true;
						$conteudo .= heading('Relatórios de Novos Clientes - Representantes', 2);
					}
					
					//Listagem
					$ids_representantes = $this->input->get('para_array');
					if($ids_representantes)
					{						

							$representantes = $ids_representantes;
							
							$relatorio  .= '<div style="clear:both"></div>';
							
							//----------------------------------
							// Algoritimo para a tabela de relatório
							
							$total_meses = $mes_final - ($mes_inicial - 1);
							$divisor = ceil($total_meses / 3);
							
							$mi = $mes_inicial;
							
							for($tab = 1; $tab <= $divisor; $tab++)
							{

								$_m = array();
								for($m = $mi; $m <= ($mi + 2); $m++)
								{
									if($m <= $mes_final)
									{
										$_m[] = $m;
									}
									
									$mf = $m;
								}
								
								$mi = $mf + 1;

								$_tabs[] = $_m;
							}
							
							
							//----------------------------------

							$relatorio .= '<table class="hnordt" style="border: none;">';
							foreach($_tabs as $_tab){
							
								$nome_meses = $this->_obter_meses();
								
								
								$relatorio .= '<thead style="border: 1px solid #cccccc;">';
								$relatorio .= '<th rowspan="2">Representante</th>';
								foreach($_tab as $mes)
								{						
									$relatorio .= '<th colspan="3">';
									$relatorio .= $nome_meses[str_pad($mes, 2, "0", STR_PAD_LEFT)] . '/' . $ano;
									$relatorio .= '</th>';
								}
								
								// Título - Meta x Realizado
								$relatorio .= '<tr>';
								foreach($_tab as $mes)
								{							
									$relatorio .= '<th>Meta</th>';
									$relatorio .= '<th colspan="2">Realizado</th>';
								}
								$relatorio .= '</tr>';
								
								$relatorio .= '</thead>';
								
								$relatorio .= '<tbody>';
								
								$total_relatorio = array();
								foreach($representantes as $representante)
								{
									$relatorio .= '<tr style="border: 1px solid #cccccc;"><td style="border: 1px solid #cccccc;">';
									
									$relatorio .= $this->db->from('usuarios')->where('id' , $representante)->get()->row()->codigo . ' - ' . $this->db->from('usuarios')->where('id' , $representante)->get()->row()->nome;
									$relatorio .= '</td>';
									
									foreach($_tab as $mes)
									{

										$grafico[$representante][$mes . '/' . $ano]['data'] = $mes . '/' . $ano;
										
										$data_inicial = explode('/', '01/' . str_pad($mes, 2, "0", STR_PAD_LEFT) . '/' . $ano);
										$inicio_timestamp = mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
										
										$ultimo_dia = date('t', mktime(23, 59, 59, str_pad($mes, 2, "0", STR_PAD_LEFT), '01', $ano));
										
										$data_final = explode('/', $ultimo_dia . '/' . str_pad($mes, 2, "0", STR_PAD_LEFT) . '/' . $ano);
										$fim_timestamp = mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]);
										
										$meta_representante = $this->db->from('metas')->where(array(
											'tipo_meta' => 'meta_novos_clientes',
											'para' => 'representante',
											'para_array' => $representante,
											'data_inicial >=' => $inicio_timestamp,
											'data_final <= ' => $fim_timestamp
										))->get()->row();
										
										//Meta
										$relatorio .= '<td style="border: 1px solid #cccccc;">';
											$relatorio .= ($meta_representante->meta_quantidade ? $meta_representante->meta_quantidade : '0');
											$grafico[$representante][$mes . '/' . $ano]['meta'] = $meta_representante->meta_valor;
										$relatorio .= '</td>';
										
										$total = $this->db_cliente->obter_novos_clientes($this->db->from('usuarios')->where('id' , $representante)->get()->row()->codigo, $inicio_timestamp, $fim_timestamp);
		
										//print_r($total);
			
										$relatorio .= '<td style="border: 1px solid #cccccc;">';
										$relatorio .= $total[0]['total'];
										$grafico[$representante][$mes . '/' . $ano]['realizado'] = $total[0]['total'];
										$relatorio .= '</td>';
										
										
										//Porcentagem
										$relatorio .= '<td style="border: 1px solid #cccccc;">';

											$valor_met = $meta_representante->meta_quantidade;
											$valor_fat = $total[0]['total'];
											
											if(!empty($valor_met) AND !empty($valor_fat))
											{
												$porcentagem = ($valor_fat / $valor_met) * 100;
												$relatorio .= number_format($porcentagem, 2, ',', '.') . '%';
											}
											else
											{
												$relatorio .= '0%';
											}
											
										$relatorio .= '</td>';

										
										$total_relatorio[$mes] = $total_relatorio[$mes] + $total[0]['total'];
									}
									
									$relatorio .= '</tr>';
								}
								
								//Exibindo Total
								$relatorio .= '<thead>';
								$relatorio .= '<tr style="border: 1px solid #cccccc;">
								<th>Total Realizado (R$)</th>';
								foreach($total_relatorio as $total)
								{
									$relatorio .= '<th colspan="3">' . $total . '</th>';
								}
								$relatorio .= '</tr>';
								$relatorio .= '</thead>';
								
								$relatorio .= '<tr>';
								$relatorio .= '<td colspan="10" style="height: 30px;"></td>';
								$relatorio .= '</tr>';
							
							}
							$relatorio .= '</table>';

						
					}
				
				}
				
			
			}
			
			if(!$titulo)
				$conteudo .= heading('Relatórios de Novos Clientes - Representantes', 2);
			
			if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
			$conteudo .= '<br />';
			
			//$conteudo .= form_open(current_url(), 'class="nao_exibir_impressao"');
			$conteudo .= form_open(current_url(), array('method' => 'get', 'class' => 'nao_exibir_impressao'));
			
			$conteudo .= '<p style="float: left; margin-right: 10px;">Período:' . br() . form_dropdown('mes_inicial', $this->_obter_meses(), $mes_inicial, 'dojoType="dojox.form.DropDownSelect"') . ' a ' . form_dropdown('mes_final', $this->_obter_meses(), $mes_final, 'dojoType="dojox.form.DropDownSelect"') . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Ano:' . br() . form_dropdown('ano', array_combine(array_values(range(date('Y'), 2009)), array_values(range(date('Y'), 2009))), $ano, 'dojoType="dojox.form.DropDownSelect"')) . '</p>';
			$conteudo .= '<div style="clear: both;"></div>';
			
			$conteudo .= '<p>Representantes:' . br() . form_dropdown('para_array[]', $this->_obter_representantes(), $this->input->post('para_array'), ' multiple="multiple" class="multi_select" style="width: 490px;" ') . '</p>';
			
			$conteudo .= '<p>' . form_submit('gerar_relatorio', 'Gerar Relatório');// . ' ou ' . anchor('metas/relatorios/representante' . $id, 'Voltar') . '</p>';
			$conteudo .= form_close();
			
			$conteudo .= '
			<script type="text/javascript">
				$(".multi_select").multiselect({
					checkAllText: "Marcar Todos",
					uncheckAllText: "Desmarcar Todos",
					noneSelectedText: "Selecione as opções",
					selectedText: "# Selecionado(s)"
				}).multiselectfilter({
					width: 130,
					label: "Filtrar",
					placeholder: "Digite palavras-chave"
				});
			</script>
			';
	

			$conteudo .= $relatorio;
			
			$conteudo .= $this->criar_grafico($grafico);
			
		}
		else if($para == 'estado')
		{
		
			$titulo = false;
			
			if($_GET)
			{
				$mes_inicial = $this->input->get('mes_inicial') ? $this->input->get('mes_inicial') : '01';
				$mes_final = $this->input->get('mes_final') ? $this->input->get('mes_final') : 12;
				$ano = $this->input->get('ano') ? $this->input->get('ano') : date('Y');
				
				if ($mes_inicial > $mes_final)
				{
					$erro = 'O período selecionado é inválido.';
				}
				else if(!$this->input->get('para_array'))
				{
					$erro = "Selecione um estado.";
				}
				else
				{
					
					$conteudo .= '
						<div class="caixa">
							<ul>
								<li>' . anchor('#', 'Imprimir', 'onclick="print(); return false;"') . '</li>
							</ul>
						</div>
					';
					/*
					$conteudo .= '
						<div class="caixa" style="margin-right: 10px">
							<ul>
								<li>' . anchor('metas/visualizar_grafico/quantidade', 'Visualizar Gráfico') . '</li>
							</ul>
						</div>
					';
					*/
					
					if(!$titulo){
						$titulo = true;
						$conteudo .= heading('Relatórios de Novos Clientes - Estados', 2);
					}

					//Listagem
					$estados = $this->input->get('para_array');
					if($estados)
					{						

							$relatorio  .= '<div style="clear:both"></div>';
							
							//----------------------------------
							// Algoritimo para a tabela de relatório
							
							$total_meses = $mes_final - ($mes_inicial - 1);
							$divisor = ceil($total_meses / 3);
							
							$mi = $mes_inicial;
							
							for($tab = 1; $tab <= $divisor; $tab++)
							{

								$_m = array();
								for($m = $mi; $m <= ($mi + 2); $m++)
								{
									if($m <= $mes_final)
									{
										$_m[] = $m;
									}
									
									$mf = $m;
								}
								
								$mi = $mf + 1;

								$_tabs[] = $_m;
							}
							
							
							//----------------------------------
							
							$relatorio .= '<table class="hnordt" style="border: none;">';
							foreach($_tabs as $_tab){
							
								$nome_meses = $this->_obter_meses();
								
								$relatorio .= '<thead style="border: 1px solid #cccccc;">';
								$relatorio .= '<th rowspan="2">Estados</th>';
								foreach($_tab as $mes)
								{								
									$relatorio .= '<th colspan="3">';
									$relatorio .= $nome_meses[str_pad($mes, 2, "0", STR_PAD_LEFT)] . '/' . $ano;
									$relatorio .= '</th>';
								}
								
								// Título - Meta x Realizado
								$relatorio .= '<tr>';
								foreach($_tab as $mes)
								{							
									$relatorio .= '<th>Meta</th>';
									$relatorio .= '<th colspan="2">Realizado</th>';
								}
								$relatorio .= '</tr>';
								
								$relatorio .= '</thead>';
								
								$relatorio .= '<tbody>';
								
								$total_relatorio = array();
								foreach($estados as $estado)
								{
									$relatorio .= '<tr style="border: 1px solid #cccccc;"><td style="border: 1px solid #cccccc;">';
									
									$relatorio .= $estado;
									$relatorio .= '</td>';
									
									foreach($_tab as $mes)
									{	
										
										$grafico[$representante][$mes . '/' . $ano]['data'] = $mes . '/' . $ano;
										
										$data_inicial = explode('/', '01/' . str_pad($mes, 2, "0", STR_PAD_LEFT) . '/' . $ano);
										$inicio_timestamp = mktime(0, 0, 0, $data_inicial[1], $data_inicial[0], $data_inicial[2]);
										
										$ultimo_dia = date('t', mktime(23, 59, 59, str_pad($mes, 2, "0", STR_PAD_LEFT), '01', $ano));
										
										$data_final = explode('/', $ultimo_dia . '/' . str_pad($mes, 2, "0", STR_PAD_LEFT) . '/' . $ano);
										$fim_timestamp = mktime(23, 59, 59, $data_final[1], $data_final[0], $data_final[2]);
										
										$meta_representante = $this->db->from('metas')->where(array(
											'tipo_meta' => 'meta_novos_clientes',
											'para' => 'estado',
											'para_array' => $estado,
											'data_inicial >=' => $inicio_timestamp,
											'data_final <= ' => $fim_timestamp
										))->get()->row();
										
										
										//Meta
										$relatorio .= '<td style="border: 1px solid #cccccc;">'; 
											$relatorio .= ($meta_representante->meta_quantidade ? $meta_representante->meta_quantidade : '0');
											$grafico[$representante][$mes . '/' . $ano]['meta'] = $meta_representante->meta_valor;
										$relatorio .= '</td>';
										
										$total = $this->db_cliente->obter_novos_clientes(NULL, $inicio_timestamp, $fim_timestamp, $estado);
		
										//print_r($total);
			
										//Realizados
										$relatorio .= '<td style="border: 1px solid #cccccc;">';
										$relatorio .= $total[0]['total'];
										$grafico[$representante][$mes . '/' . $ano]['realizado'] = $total[0]['total'];
										$relatorio .= '</td>';
										
										//Porcentagem
										$relatorio .= '<td style="border: 1px solid #cccccc;">';

											$valor_met = $meta_representante->meta_quantidade;
											$valor_fat = $total[0]['total'];
											
											if(!empty($valor_met) AND !empty($valor_fat))
											{
												$porcentagem = ($valor_fat / $valor_met) * 100;
												$relatorio .= number_format($porcentagem, 2, ',', '.') . '%';
											}
											else
											{
												$relatorio .= '0%';
											}
											
										$relatorio .= '</td>';
										
										$total_relatorio[$mes] = $total_relatorio[$mes] + $total[0]['total'];
									}
									
									$relatorio .= '</tr>';
								}
								
								//Exibindo Total
								$relatorio .= '<thead>';
								$relatorio .= '<tr style="border: 1px solid #cccccc;">
								<th>Total Realizado (R$)</th>';
								foreach($total_relatorio as $total)
								{
									$relatorio .= '<th colspan="3">' . $total . '</th>';
								}
								$relatorio .= '</tr>';
								$relatorio .= '</thead>';
								
								$relatorio .= '<tr>';
								$relatorio .= '<td colspan="10" style="height: 30px;"></td>';
								$relatorio .= '</tr>';
								
							}
								
							$relatorio .= '</table>';
							
						
					}
					
					
				}
			}
			
			if(!$titulo)
				$conteudo .= heading('Relatórios de Novos Clientes - Estados', 2);
			
			if ($erro) $conteudo .= '<p class="erro">' . $erro . '</p>';
			$conteudo .= '<br />';
			
			//$conteudo .= form_open(current_url(), 'class="nao_exibir_impressao"');
			$conteudo .= form_open(current_url(), array('method' => 'get', 'class' => 'nao_exibir_impressao'));
			
			$conteudo .= '<p>Estados:' . br() . form_dropdown('para_array[]', $this->_obter_estados(), $this->input->post('para_array'), ' multiple="multiple" class="multi_select" style="width: 490px;" ') . '</p>';
			
			$conteudo .= '<p style="float: left; margin-right: 10px;">Período:' . br() . form_dropdown('mes_inicial', $this->_obter_meses(), $mes_inicial, 'dojoType="dojox.form.DropDownSelect"') . ' a ' . form_dropdown('mes_final', $this->_obter_meses(), $mes_final, 'dojoType="dojox.form.DropDownSelect"') . '</p>';
			$conteudo .= '<p style="float: left; margin-right: 10px;">' . form_label('Ano:' . br() . form_dropdown('ano', array_combine(array_values(range(date('Y'), 2009)), array_values(range(date('Y'), 2009))), $ano, 'dojoType="dojox.form.DropDownSelect"')) . '</p>';
			$conteudo .= '<div style="clear: both;"></div>';
			
			$conteudo .= '<p>' . form_submit('gerar_relatorio', 'Gerar Relatório');// . ' ou ' . anchor('metas/relatorios/estado' . $id, 'Voltar') . '</p>';
			$conteudo .= form_close();
			
			$conteudo .= '
			<script type="text/javascript">
				$(".multi_select").multiselect({
					checkAllText: "Marcar Todos",
					uncheckAllText: "Desmarcar Todos",
					noneSelectedText: "Selecione as opções",
					selectedText: "# Selecionado(s)"
				}).multiselectfilter({
					width: 130,
					label: "Filtrar",
					placeholder: "Digite palavras-chave"
				});
			</script>
			';
	

			$conteudo .= $relatorio;
			
			$conteudo .= $this->criar_grafico($grafico);
			
		}
		
		$this->load->view('layout', array('conteudo' => $conteudo));
	}

	
}