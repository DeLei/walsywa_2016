<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Codeigniter: mpdf libraries
 * Tuesday, July, 19 2011
 * 
 * @author bang.webdeveloper@gmail.com
 */
require_once dirname(__FILE__) . '/mpdf/mpdf.php';

class Pdf extends mPDF
{
    function __construct()
    {
        parent::__construct();
		
		$this->allow_charset_conversion=true;
		// permite a conversao (opcional)
		$this->charset_in='UTF-8';
		// converte todo o PDF para utf-8

		//$this->mirrorMargins = 1;	// Use different Odd/Even headers and footers and mirror margins
		//$mpdf->defaultheaderline = 0; 
		//Algumas configura��es do PDF
		$this->SetDisplayMode('fullpage');
		// modo de visualiza��o
		
		$stylesheet = file_get_contents(base_url() . 'third_party/css/screen2.css');
		// incorpora a folha de estilo ao PDF
		// O par�metro 1 diz que este � um css/style
		$this->WriteHTML($stylesheet,1);
		
		$stylesheet2 = file_get_contents(base_url() . 'third_party/css/print2.css');
		// incorpora a folha de estilo ao PDF
		// O par�metro 1 diz que este � um css/style
		$this->WriteHTML($stylesheet2,1);
		
		//seta o rodap�
		$this->SetFooter('{DATE j/m/Y - H:i}|{PAGENO}/{nb}|DevelopWeb / DW Portal do Representante');

    }
}