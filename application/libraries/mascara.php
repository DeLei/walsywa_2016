<?php

class Mascara {

    function Mascara() {

    }

    /**
     * Insere uma máscara na variável e retorna o a variável com a máscara
     * @param $valor: string
     * @param $mascara: string  - Tipo da máscara
     */
    function formatar_mascara($valor, $mascara) {

        $arrCampo = str_split($valor);
        $arr = str_split($mascara);
        $cont = 0;

        for ($i = 0; $i < sizeof($arr); $i++) {
            if ($arr[$i] == '#') {
                $arr[$i] = $arrCampo[$cont];
                $cont++;
            }
        }

        $valor = "";
        for ($i = 0; $i < sizeof($arr); $i++) {
            $valor .= $arr[$i];
        }
        return $valor;
    }

    /**
     * Coloca máscara no CNPJ e CPF
     * @param <string> $valor
     * @return <string> $valor  - Retorna os números com a máscara
     */
    function formatar_cnpj_cpf($valor) {
        $replace = str_split("/-.");

        for ($i = 0; $i < sizeof($replace); $i++) {
            $valor = str_replace($replace[$i], "", $valor);
        }
        if (strlen($valor) == 14) {
            $arrCampo = str_split($valor);
            $arr = str_split("##.###.###/####-##");
            $cont = 0;

            for ($i = 0; $i < sizeof($arr); $i++) {
                if ($arr[$i] == '#') {
                    $arr[$i] = $arrCampo[$cont];
                    $cont++;
                }
            }

            $valor = "";
            for ($i = 0; $i < sizeof($arr); $i++) {
                $valor .= $arr[$i];
            }
            return $valor;
        } else {
            if (strlen($valor) == 11) {
                $arrCampo = str_split($valor);
                $arr = str_split("###.###.###-##");
                $cont = 0;

                for ($i = 0; $i < sizeof($arr); $i++) {
                    if ($arr[$i] == '#') {
                        $arr[$i] = $arrCampo[$cont];
                        $cont++;
                    }
                }

                $valor = "";
                for ($i = 0; $i < sizeof($arr); $i++) {
                    $valor .= $arr[$i];
                }
                return $valor;
            } else {
                return $valor;
            }
        }
    }

}