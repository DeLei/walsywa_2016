<?php

//definimos os segundos que o usuário deverá ficar logado
define('TEMPO_LOGADO',1200);

class MY_Controller extends CI_Controller {
	
	private $total_registro_paginacao;
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('shazam_model');
		
		date_default_timezone_set('America/Sao_Paulo');
		
		//CUSTOM: MPD-220_Walsywa_01_05082013
		/*---------------------------------------------------*/
		/* Verifica se a conexão não é AJAX se não for então
		/* salva a URL para redirecionar o usuário para a mesma
		/*---------------------------------------------------*/
		if(!eAjax()) // Função esta no HELPER hnordt
		{
			// subtraimos o tempo em que o usuário entrou, do tempo atual "a diferença é em segundos"
		   if($this->session->userdata('tempo_permitido'))
		   {
			   // pegamos o tempo atual em que estamos:
			   $agora= mktime(date('H:i:s'));
			   $segundos=(is_numeric($this->session->userdata('tempo_permitido')) and is_numeric($agora)) ? ($agora-$this->session->userdata('tempo_permitido')):false;
		   }
		   else
		   {
			   $segundos = 0;
		   }

		   if($segundos > TEMPO_LOGADO) 
		   {
				$this->session->sess_destroy();
		   } 
		   else 
		   {
				//Se as sessions ainda existirem, exibimos o nome.
				//Gravamos o tempo atual em uma sessão, para compararmos depois.
				$this->session->set_userdata('tempo_permitido',  mktime(date('H:i:s')));
		   }
		}
		//FIM CUSTOM: MPD-220_Walsywa_01_05082013

		if (!$this->session->userdata('id_usuario') && !in_array(current_url(), array(site_url('usuarios/autenticar'), site_url('instalar'), site_url('usuarios/acessar'))))
		{									
			//echo '<center><h1 style="color:red;">Acesso não autorizado. É necessário se autenticar com o aplicativo.</h1><br />';
			//echo '<a href="'.base_url().'portalrepresentante.jar"> Clique aqui para baixar </a></center> ';
			//die();
			 include('seguranca/index.php');
			 die();
		} 
	}
	
	protected function verifica_seguranca_acesso($mac){
		return true;
	}
	
	function _obter_filiais()
	{
		$this->load->model('db_cliente', 'db_cliente_model');
		
		return $this->db_cliente_model->obter_filiais();
	
		/*
		return array(
			'01' => '01 - Jundiaí',
			'02' => '02 - 3 Corações',
			'03' => '03 - Camanducaia',
			'04' => '04 - Piçarras',
			'05' => '05 - Louveira'
		);
		*/
	}
	
	/*
		Exemplo de uso:
		$this->filtragem(array(
			'nome' => 'nome_fantasia',
			'descricao' => 'Nome fantasia',
			'tipo' => 'texto',
			'atributos' => 'size="40"',
			'opcoes' => array('valor_1' => 'Descrição 1', 'valor_2' => 'Descrição 2'), // usada somente para o tipo "opcoes"
			'url' => 'clientes/obter_clientes' // usada somente o tipo "autocomplete"
		));
	*/
	function filtragem($filtros)
	{
		$html = '<form action="#filtros" method="get" id="filtros">';
		
		foreach ($filtros as $filtro)
		{
			if (!$filtro)
			{
				continue;
			}
			
			if($filtro['tipo'] == 'cep'){
				$html .= '<p style="float: left; margin-right: 10px;">' . $filtro['descricao'] . ':';
				$value = ($_GET['cep_aproximado'] ? 'checked="true"' : '');
				$html .= '<input type="checkbox" name="cep_aproximado" '. $value .' style="size:1px;width: 10px;height: 10px;margin-bottom: 0px; margin-left:5px;"> Aproximado<br>';
			}else{
				$html .= '<p style="float: left; margin-right: 10px; width:230px;">' . $filtro['descricao'] . ':<br>';
			}
			
			switch ($filtro['tipo'])
			{
				case 'texto':
					$html .= '<input type="text" name="' . $filtro['nome'] . '" value="' . $_GET[$filtro['nome']] . '" ' . ($filtro['atributos'] ? $filtro['atributos'] : 'size="20"') . '>';
				break;
				
				case 'hora':
					$html .= '<input type="text" name="' . $filtro['nome'] . '" value="' . $_GET[$filtro['nome']] . '" ' . ($filtro['atributos'] ? $filtro['atributos'] : 'size="5" alt="time"') . '>';
				break;
				
				case 'opcoes':
					$html .= '<select name="' . $filtro['nome'] . '" ' . $filtro['atributos'] . '>';
					
					foreach ($filtro['opcoes'] as $valor => $descricao)
					{
						$html .= '<option value="' . $valor . '" ' . ($_GET[$filtro['nome']] == $valor ? 'selected="selected"' : NULL) . '>' . $descricao. '</option>';
					}
					
					$html .= '</select>';
				break;
				
				 case ($filtro['tipo'] == 'opcoes' || $filtro['tipo'] == 'opcoes_representantes_todos'):
                    $html .= '<select name="' . $filtro['nome'] . '" ' . $filtro['atributos'] . '>';

                    foreach ($filtro['opcoes'] as $valor => $descricao) {
                        $html .= '<option value="' . $valor . '" ' . ($_GET[$filtro['nome']] == $valor ? 'selected="selected"' : NULL) . '>' . strtoupper($descricao) . '</option>';
                    }

                    $html .= '</select>';
                    break;
				
				case 'opcoes_orcamento':
					$html .= '<select name="' . $filtro['nome'] . '" ' . $filtro['atributos'] . '>';
					
					foreach ($filtro['opcoes'] as $valor => $descricao)
					{
						$html .= '<option value="' . $valor . '" ' . ($_GET[$filtro['nome']] == $valor ? 'selected="selected"' : NULL) . '>' . $descricao. '</option>';
					}
					
					$html .= '</select>';
				break;
				
				
				case 'data':
					$html .= '<input type="text" name="' . $filtro['nome'] . '_inicial" value="' . $_GET[$filtro['nome'] . '_inicial'] . '" class="datepicker data_inicial" ' . ($atributos ? $atributos : 'size="7"') . ' style="float: left;  width: 100px;">  <input type="text" name="' . $filtro['nome'] . '_final" value="' . $_GET[$filtro['nome'] . '_final'] . '" class="datepicker" ' . ($filtro['atributos'] ? $filtro['atributos'] : 'size="7"') . ' style="float: left; width: 100px;">';
				break;
				
				case 'data_minima_inicio':
					$html .= '<input type="text" name="' . $filtro['nome'] . '_inicial" value="' . $_GET[$filtro['nome'] . '_inicial'] . '" class="datepicker_data_minima_inicio" ' . ($atributos ? $atributos : 'size="7"') . ' READONLY > a <input type="text" name="' . $filtro['nome'] . '_final" value="' . $_GET[$filtro['nome'] . '_final'] . '" class="datepicker_data_minima_final" ' . ($filtro['atributos'] ? $filtro['atributos'] : 'size="7"') . ' READONLY >';
				break;
				
				case 'autocomplete':
					$html .= '<input type="text" name="' . $filtro['nome'] . '" value="' . $_GET['_' . $filtro['nome']] . '" class="autocomplete" data-url="' . site_url($filtro['url']) . '" data-valor="' . $_POST[$filtro['nome']] . '" ' . ($filtro['atributos'] ? $filtro['atributos'] : NULL) . '>';
				break;
				
				case 'cep':
					$html .= '<input type="text" name="' . $filtro['nome'] . '" value="'. $_GET[$filtro['nome']] . '" alt="cep">';
				break;
                                       
                //CUSTOM: MPD-220 - 07/08/2013
				case 'data_fixa':
					$html .= '<input type="text" class="datepicker" MAXLENGTH="10" name="' . $filtro['nome'] . '" value="'. $_GET[$filtro['nome']] . '">';
				break;
                //FIM CUSTOM: MPD-220 - 07/08/2013
				
				case 'faixa_codigo':
                    $html .= '<input type="text" name="' . $filtro['nome'] . '_inicial" value="' . $_GET[$filtro['nome'] . '_inicial'] . '" ' . ($filtro['atributos'] ? $filtro['atributos'] : 'size="20"') . '>';
                    $html .= ' a ';
                    $html .= '<input type="text" name="' . $filtro['nome'] . '_final" value="' . $_GET[$filtro['nome'] . '_final'] . '" ' . ($filtro['atributos'] ? $filtro['atributos'] : 'size="20"') . '>';
                break;
					
                            
			}
			
			$html .= '</p>';
		}
		
		$html .= '<div style="clear: both; height: 1px;"></div>';
		
		$html .= '<p><input name="my_submit" type="submit" value="Filtrar"> <input type="submit" value="Limpar Filtros" onclick="resetar_form($(this).parents(\'form\'), true); return false;"></p>';
		
		$html .= '</form>';
		
		// ** ordenação
		
		$html .= '<script type="text/javascript">$(document).ready(function() {';
		
		foreach ($filtros as $filtro)
		{
			if (isset($filtro['ordenar']))
			{
				$html .= '$("table[class=novo_grid] thead tr th:eq(' . $filtro['ordenar'] . ')").wrapInner("<a href=\"' . $this->urlget(array('ordenar_my', 'ordenar_my_tipo')) . 'ordenar_my=' . $filtro['campo_mysql'] . '&ordenar_my_tipo=' . ($this->input->get('ordenar_my_tipo') == 'asc' ? 'desc' : 'asc') . '\"></a>");';
				
				if ($this->input->get('ordenar_my') == $filtro['campo_mysql'])
				{
					$html .= '$("table[class=novo_grid] thead tr th:eq(' . $filtro['ordenar'] . ') a").append(" ' . ($this->input->get('ordenar_my_tipo') == 'asc' ? '▼' : '▲') . '");';
				}
			}
			
			if (isset($filtro['ordenar_ms']))
			{
				$html .= '$("table[class=novo_grid] thead tr th:eq(' . $filtro['ordenar_ms'] . ')").wrapInner("<a href=\"' . $this->urlget(array('ordenar_ms', 'ordenar_ms_tipo')) . 'ordenar_ms=' . $filtro['campo_mssql'] . '&ordenar_ms_tipo=' . ($this->input->get('ordenar_ms_tipo') == 'asc' ? 'desc' : 'asc') . '\"></a>");';
				
				if ($this->input->get('ordenar_ms') == $filtro['campo_mssql'])
				{
					$html .= '$("table[class=novo_grid] thead tr th:eq(' . $filtro['ordenar_ms'] . ') a").append(" ' . ($this->input->get('ordenar_ms_tipo') == 'asc' ? '▼' : '▲') . '");';
				}
			}
		}
		
		$html .= '});</script>';
		
		// orndeção **
		
		return $html;
	}
	
	function filtragem_mysql($filtros)
	{
		foreach ($filtros as $filtro)
		{
			if (!$filtro['campo_mysql'])
			{
				continue;
			}
			
			switch ($filtro['tipo'])
			{
				case 'texto':
					if (!$this->input->get($filtro['nome']))
					{
						continue;
					}
					
					$this->db->like($filtro['campo_mysql'], $this->input->get($filtro['nome']), 'both');
				break;
				
				case 'opcoes':
					if (!$this->input->get($filtro['nome']) || $this->input->get($filtro['nome']) == 'todos')
					{
						continue;
					}
					
					$this->db->where($filtro['campo_mysql'], $this->input->get($filtro['nome']));
				break;
				
				case 'data':
					if ($this->input->get($filtro['nome'] . '_inicial'))
					{
						$this->db->where($filtro['campo_mysql'] . ' >=', $this->data2timestamp($this->input->get($filtro['nome'] . '_inicial')));
					}
					
					if ($this->input->get($filtro['nome'] . '_final'))
					{
						$this->db->where($filtro['campo_mysql'] . ' <=', $this->data2timestamp($this->input->get($filtro['nome'] . '_final')));
					}
				break;
				
				case 'autocomplete':
					if (!$this->input->get($filtro['nome']))
					{
						continue;
					}
					
					$this->db->where($filtro['campo_mysql'], $this->input->get($filtro['nome']));
				break;
			}
		}
		
		if ($this->input->get('ordenar_my'))
		{
			$this->db->order_by($this->input->get('ordenar_my'), $this->input->get('ordenar_my_tipo'));
		}
	}
	
	function filtragem_mssql($filtros, $gerar_order_by = TRUE)
	{
		foreach ($filtros as $filtro)
		{
			if (!$filtro['campo_mssql'])
			{
				continue;
			}
			
			switch ($filtro['tipo'])
			{
				case 'texto':
					if (!$this->input->get($filtro['nome']))
					{
						continue;
					}
					
					$filtro_nome = strtoupper($this->caracterEsp($this->input->get($filtro['nome'])));
					$this->db_cliente->like('UPPER(' . $filtro['campo_mssql'] . ')', $filtro_nome, 'both');
				break;
				
				case 'hora':
					if (!$this->input->get($filtro['nome']))
					{
						continue;
					}
					
					$this->db_cliente->where($filtro['campo_mssql'], $this->input->get($filtro['nome']));
				break;
				
				case 'opcoes':
					if (!$this->input->get($filtro['nome']) || $this->input->get($filtro['nome']) == 'todos')
					{
						continue;
					}
					
					$this->db_cliente->where($filtro['campo_mssql'], $this->input->get($filtro['nome']));
				break;
				
				case 'opcoes_representantes_todos':

                    if ($this->input->get($filtro['nome']) == 'todos' || !$this->input->get($filtro['nome'])) {
                        foreach ($filtro['opcoes'] as $indice => $valor) {
                            if ($indice != 'todos') {
                                $valores[] = (String) $indice;
                            }
                        }
                        $this->db_cliente->where_in($filtro['campo_mssql'], $valores);
                        continue;
                    }

                    $this->db_cliente->where($filtro['campo_mssql'], $this->input->get($filtro['nome']));
                    break;
				
				case 'opcoes_orcamento':
					if (!$this->input->get($filtro['nome']) || $this->input->get($filtro['nome']) == 'todos')
					{
						continue;
					}
					if($this->input->get($filtro['nome']) == 'A' ){
						
						$this->db_cliente->where_in($filtro['campo_mssql'],array(
								$this->input->get($filtro['nome']),
								 'L')
								);
					}else{
						$this->db_cliente->where($filtro['campo_mssql'], $this->input->get($filtro['nome']));
					}
					
				break;
				
				
				case 'data':
					if ($this->input->get($filtro['nome'] . '_inicial'))
					{
						$this->db_cliente->where($filtro['campo_mssql'] . ' >=', date('Ymd', $this->data2timestamp($this->input->get($filtro['nome'] . '_inicial'))));
					}
					
					if ($this->input->get($filtro['nome'] . '_final'))
					{
						$this->db_cliente->where($filtro['campo_mssql'] . ' <=', date('Ymd', $this->data2timestamp($this->input->get($filtro['nome'] . '_final'))));
					}

				break;
				
				case 'autocomplete':
					if (!$this->input->get($filtro['nome']))
					{
						continue;
					}
					
					$this->db_cliente->where($filtro['campo_mssql'], $this->input->get($filtro['nome']));
				break;
				
				case 'cep':
					if($this->input->get('cep_aproximado')){
						if($this->input->get($filtro['nome'])){
							$cep = str_replace('-', '', $this->input->get($filtro['nome']));
							//Chamado 1283 / 1397
							$cep_aproximado = substr($cep, 0, 5);
							
							//Chamado 1246 
							$this->db_cliente->where($filtro['campo_mssql'].' LIKE '."'".$cep_aproximado.'%'."'");
							
							//Como estava antes:
							//$this->db_cliente->like($filtro['campo_mssql'], $cep_aproximado);
						}
					}else{
						if($this->input->get($filtro['nome'])){
							$cep = str_replace('-', '', $this->input->get($filtro['nome']));
							$this->db_cliente->where($filtro['campo_mssql'], $cep);
						}
					}
				break;
                                                              
                //CUSTOM: MPD-220 - 07/08/2013
				case 'data_fixa':
                                    
					if($this->input->get($filtro['nome'])){
                                            //$this->db_cliente->where($filtro['campo_mssql'], $this->input->get($filtro['nome']));
                                            $this->db_cliente->where($filtro['campo_mssql'], date('Ymd', $this->data2timestamp($this->input->get($filtro['nome']))));
                                        }
                                        
				break;
                //FIM CUSTOM: MPD-220 - 07/08/2013
                                
                case 'faixa_codigo':
                    
                    if ($this->input->get($filtro['nome'] . '_inicial'))
                    {
                        $this->db_cliente->where($filtro['campo_mssql'] . ' >=', $this->input->get($filtro['nome'] . '_inicial'));
                    }
                    
                    if ($this->input->get($filtro['nome'] . '_final'))
                    {
                        $this->db_cliente->where($filtro['campo_mssql'] . ' <=', $this->input->get($filtro['nome'] . '_final'));
                    }
                    
                break;
               
			}
                        
		}
		
		if ($this->input->get('ordenar_ms') && $gerar_order_by)
		{
			$this->db_cliente->order_by($this->input->get('ordenar_ms'), $this->input->get('ordenar_ms_tipo'));
		}
	}
	
	
	/* CONJUNTO DE FUNCOES PARA PAGINACAO****/
	
	// RETORNA CALCULO DAS PAGINA ENVIADA PELA URL
	function paginacao_contador_asc()
	{
		$contador = 1;
		if($this->input->get('per_page') != 0 && is_numeric($this->input->get('per_page')))
			$contador = $this->input->get('per_page')+1;
		return($contador);
	}
	
	function paginacao_contador_desc($total_registro, $limit_consulta)
	{
		//$total_registro += 1;
		$contador = $total_registro;
		if($this->input->get('per_page') != 0 && is_numeric($this->input->get('per_page')))
			$contador = $total_registro -$this->input->get('per_page');
		return($contador);
	}
	
	// SETA O TOTAL DE REGISTRO GERADO PELA ULTIMA PAGINACAO
	function set_total_registro_paginacao($total)
	{
		$this->total_registro_paginacao = $total;
	}
	
	// RETORNA O TOTAL DE REGISTRO GERADO PELA ULTIMA PAGINACAO
	function get_total_registro_paginacao()
	{
		return($this->total_registro_paginacao);
	}
	
	function paginacao_total($total, $resultados_por_pagina = 20)
	{
		// PREPARA HTML PARA RETORNAR PAGINACAO PRONTA
		$paginacao = $this->paginacao($total, $resultados_por_pagina);
		
		$html = '<div class="rodape_grid">';
		if($total <= 0)
			$html .= '	<p> Nenhum registro encontrado.</p>';
		else 
			$html .= '<p>&nbsp;</p><p>'.number_format(($total), 0, ',', '.').' registros encontrados.</p>' . $paginacao;
		$html .= '</div>';
		return($html);
	}
	
	// RETORNA PAGINACAO GERADA PELA ULTIMA SQL
	function paginacao_rapida()
	{
		// CAPTURA A ULTIMA SQL EXECUTADA E REMOVE O COMANDO 'LIMIT'
		$sql = $this->db->last_query();
		$sql2 = $this->sql_remove_limite($sql);
		$total = $this->db->query($sql2[0])->num_rows();
		$paginacao = $this->paginacao($total, $sql2[1]);
		
		$this->total_registro_paginacao = $total;
		
		// PREPARA HTML PARA RETORNAR PAGINACAO PRONTA
		$html = '<div class="rodape_grid">';
		if($total <= 0)
			$html .= '	<p> Nenhum registro encontrado.</p>';
		else 
			$html .= '<p>&nbsp;</p><p>'.number_format(($this->total_registro_paginacao), 0, ',', '.').' registros encontrados.</p>' . $paginacao;
		$html .= '</div>';
		return($html);

		return($paginacao);
	}
	
	function sql_remove_limite($sql)
	{
		$sql_explode = explode('LIMIT', $sql);
		return($sql_explode);
	}
	
	
	function paginacao($total_de_resultados, $resultados_por_pagina = 20)
	{
		$this->load->library('pagination');
		
		$config['base_url'] = $this->urlget(array('per_page'));
		$config['total_rows'] = $total_de_resultados;
		$config['per_page'] = $resultados_por_pagina; 
		$config['num_links'] = 5;
		$config['page_query_string'] = TRUE;
		
		$config['full_tag_open'] = '<p class="paginacao">';
		$config['full_tag_close'] = '</p>';
		
		$config['first_link'] = 'Primeira Página';
		$config['last_link'] = 'Última Página';
		
		$this->pagination->initialize($config); 
		
		return $this->pagination->create_links();
	}
	// ****CONJUNTO DE FUNCOES PARA PAGINACAO
	
	
	function urlget($ignorar = array())
	{
		return current_url() . '/?' . $this->retget($ignorar);
	}
	
	function retget($ignorar = array())
	{
		$get = NULL;
		
		foreach ($_GET as $indice => $valor)
		{
			if (in_array($indice, $ignorar))
			{
				continue;
			}
			
			$get .= $indice . '=' . $valor . '&';
		}
		
		return $get;
	}
	
	function data2timestamp($data)
	{
		$data = explode('/', $data);
		
		return mktime(0, 0, 0, floatval($data[1]), floatval($data[0]), floatval($data[2]));
	}
	
	function log($log)
	{
		$arquivo = fopen('./log.txt', 'a+');
		fwrite($arquivo, $log . "\r\n");
		fclose($arquivo);
	}
	
	// essa função foi criada depois de alguns controllers já usarem _obter_usuarios()
	// um _ foi adicionado no final para previnir conflitos
	// essa função será usada para obter todos os usuários do sistema, exibindo o grupo, código, filial, etc
	function _obter_usuarios_($indice = 'id')
	{
		$_usuarios = array();
		
		// se o usuário for supervisor, devemos filtrar somente os usuário que ele pode visualizar
		if ($this->session->userdata('grupo_usuario') == 'supervisores')
		{
			$filtrar_codigos_usuarios = array();
			
			$representantes = $this->db
										->select('id_supervisor, codigo_representante')
										->from('representantes_supervisores')
										->join('usuarios', 'id_supervisor = id', 'INNER')
										->where('id_supervisor', $this->session->userdata('id_usuario'))
										->where('status', 'ativo')
										->get()->result();
										
			//echo $this->db->last_query();
			//die();
			foreach ($representantes as $representante)
			{
				$filtrar_codigos_usuarios[] = $representante->codigo_representante;
			}
		}
		
		// representantes
		
		if ($filtrar_codigos_usuarios) $this->db->where_in('codigo', $filtrar_codigos_usuarios);
		
		$representantes = $this->db->from('usuarios')->where('grupo', 'representantes')->order_by('nome', 'ASC')->get()->result();
		
		foreach ($representantes as $representante)
		{
			$_usuarios['representantes'][$representante->$indice] = ($representante->nome_real ? $representante->nome_real : $representante->nome) . ' - ' . $representante->codigo;
		}
		
		// prepostos
		
		if ($filtrar_codigos_usuarios) $this->db->where_in('codigo', $filtrar_codigos_usuarios);
		
		$prepostos = $this->db->from('usuarios')->where('grupo', 'prepostos')->get()->result();
		
		foreach ($prepostos as $preposto)
		{
			$_usuarios['prepostos'][$preposto->$indice] = $preposto->codigo . ' - ' . ($preposto->nome_real ? $preposto->nome_real : $preposto->nome);
		}
		
		// revendas
		
		if ($filtrar_codigos_usuarios) $this->db->where_in('codigo', $filtrar_codigos_usuarios);
		
		$revendas = $this->db->from('usuarios')->where('grupo', 'revendas')->get()->result();
		
		foreach ($revendas as $revenda)
		{
			$_usuarios['revendas'][$revenda->$indice]['revenda'] = $revenda->codigo . ' - ' . ($revenda->nome_real ? $revenda->nome_real : $revenda->nome);
			
			// filiais
			
			$filiais = $this->db->from('filiais')->where('codigo_revenda', $revenda->codigo)->get()->result();
			
			foreach ($filiais as $filial)
			{
				$_usuarios['revendas'][$revenda->$indice]['filiais'][$filial->id]['filial'] = ($filial->nome_real ? $filial->nome_real : $filial->nome);
			}
			
			// vendedores
			
			foreach ($filiais as $filial)
			{
				$vendedores = $this->db->from('usuarios')->where(array('grupo' => 'vendedores', 'id_filial' => $filial->id))->get()->result();
				
				foreach ($vendedores as $vendedor)
				{
					$_usuarios['revendas'][$revenda->$indice]['filiais'][$filial->id]['vendedores'][$vendedor->$indice] = $vendedor->codigo . ' - ' . ($vendedor->nome_real ? $vendedor->nome_real : $vendedor->nome);
				}
			}
		}
		
		// vendedores
		
		/*$vendedores = $this->db->from('usuarios')->where('grupo', 'vendedores')->get()->result();
		
		foreach ($vendedores as $vendedor)
		{
			$_usuarios['vendedores'][$vendedor->$indice] = $vendedor->nome . ' - Código ' . $vendedor->codigo;
		}*/
		
		return $_usuarios;
	}
	
	function _obter_indice_primeiro_usuario_usuarios($indice = 'id')
	{
		$usuarios = $this->_obter_usuarios_($indice);
		
		foreach ($usuarios as $grupo)
		{
			$usuarios = array_keys($grupo);
			$usuario = $usuarios[0];
			break;
		}
		
		return $usuario;
	}
	
	function obter_representantes($indice = 'id', $opcao_todos = FALSE, $opcao_nome = TRUE) {
       
  

    $usuarios = $this->_obter_usuarios_($indice);
  
        if ($opcao_todos) {
            $_usuarios['todos'] = 'Todos';
        }

        foreach ($usuarios as $grupo => $usuarios) {
            foreach ($usuarios as $valor => $usuario) {
				if($opcao_nome)
				{
				 $_usuarios[$valor] = $usuario;
				}
				else
				{
				 $_usuarios[] = (String) $valor;
				}
            }
        }

        return $_usuarios;
    }
	
	
	function _obter_dropdown_representantes($indice = 'id', $atributos = array(), $opcao_todos = FALSE) 
	{ 

		$usuarios = $this->_obter_usuarios_($indice); 
		
		if($opcao_todos)
		{
			$_usuarios[ucfirst('todos')][0] = 'Todos';
		}

		foreach($usuarios as $grupo => $usuarios) 
		{ 
			foreach($usuarios as $valor => $usuario) 
			{
				$_usuarios[ucfirst($grupo)][$valor] = $usuario; 
			} 
		}

		$html .= form_dropdown($atributos['name'], $_usuarios, $atributos['value'], ' onChange="' . $atributos['onChange'] . '" id="' . $atributos['id'] . '"'); 

		return $html; 

	} 	
 
	
	
	function _gerar_dropdown_usuarios($indice = 'id', $atributos = array(), $opcao_todos = FALSE)
	{
		$usuarios = $this->_obter_usuarios_($indice);
		
		$html = '<div value="' . $atributos['value'] . '" name="' . $atributos['name'] . '" dojoType="dojox.form.DropDownSelect" onChange="' . $atributos['onChange'] . '">';
		
		$tab = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		
		if ($opcao_todos)
		{
			$html .= '<span value="todos" disabled="disabled"><strong>Todos</strong></span>';
			$html .= '<span value="0">' . $tab . 'Todos</span>';
		}
		
		foreach ($usuarios as $grupo => $usuarios_grupo)
		{
			$html .= '<span value="' . $grupo . '" disabled="disabled"><strong>' . element($grupo, $this->_obter_grupos_usuarios()) . '</strong></span>';
			
			foreach ($usuarios_grupo as $indice => $valor)
			{
				if (!is_array($valor))
				{
					$html .= '<span value="' . $indice . '">' . $tab . $valor . '</span>';
				}
				else
				{
					$html .= '<span value="' . $indice . '">' . $tab . $valor['revenda'] . '</span>';
					
					if ($valor['filiais'])
					{
						foreach ($valor['filiais'] as $id_filial => $filial)
						{
							$html .= '<span value="' . $id_filial . '" disabled="disabled">' . $tab . $tab . $filial['filial'] . '</span>';
							
							if ($filial['vendedores'])
							{
								foreach ($filial['vendedores'] as $indice => $valor)
								{
									$html .= '<span value="' . $indice . '">' . $tab . $tab . $tab . $valor . '</span>';
								}
							}
						}
					}
				}
			}
		}
		
		$html .= '</div>';
		
		return $html;
	}
	
	function _obter_grupos_usuarios()
	{
		return array('gestores_comerciais' => 'Gestores Comerciais', 'supervisores' => 'Supervisores', 'representantes' => 'Representantes', 'engenharia' => 'Engenharia');//, 'revendas' => 'Revendas', 'vendedores' => 'Vendedores');
		//'gestores_financeiros' => 'Gestores Financeiros', 
	}
	
	function _gerar_json_dojo_query_read_store($itens = array()) {
		$query = $_REQUEST['_valor'];
		
		// remover "*"
		if (strlen($query) && $query[strlen($query) - 1] == '*') $query = substr($query, 0, strlen($query) - 1);
		
		$_itens = array();
		if ($query) {
			foreach ($itens as $item) {
				if (strpos(strtolower($item['_valor']), strtolower($query)) !== FALSE) $_itens[] = $item;
			}
		}
		
		/*if (array_key_exists("sort", $_REQUEST)) {
			$sort = $_REQUEST['sort'];
			
			$desc = strpos($sort, '-') === 0 ? TRUE : FALSE;
			$sort = strpos($sort, '-') === 0 ? substr($sort, 1) : $sort;
			
			if (in_array($sort, array_keys($_itens[0]))) {
				$to_sort = array();
				foreach ($_itens as $i) $to_sort[$i[$sort]] = $i;
				if ($desc) krsort($to_sort); else ksort($to_sort);
				$__itens = array();
				foreach ($to_sort as $i) $__itens[] = $i;
				$_itens = $__itens;
			}
		}*/
		
		$quantidade_itens = count($_itens);
		
		if (array_key_exists("start", $_REQUEST)) $_itens = array_slice($_itens, $_REQUEST['start']);
		if (array_key_exists("count", $_REQUEST)) $_itens = array_slice($_itens, 0, $_REQUEST['count']);
		
		return json_encode(array('identifier' => '_indice', 'items' => $_itens, 'numRows' => $quantidade_itens));
	}
	
	function _gerar_grafico($parametros)
	{
		return '<embed type="application/x-shockwave-flash" src="' . base_url() . 'misc/FusionCharts/' . $parametros['tipo'] . '.swf" height="' . ($parametros['altura'] ? $parametros['altura'] : '100%') . '" width="' . ($parametros['largura'] ? $parametros['largura'] : '100%') . '" flashvars="&dataURL=' . $parametros['url_dados'] . '"  />';
	}
	
	function _obter_descricao_tempo_segundos($segundos)
	{
		$minutos = $segundos / 60;
		$horas = $minutos / 60;
		$dias = $horas / 24;
		
		if ($segundos < 60)
		{
			return round($segundos) . ' segundo(s)';
		}
		else if ($minutos < 60)
		{
			return round($minutos) . ' minuto(s)';
		}
		else if ($horas < 24)
		{
			return round($horas) . ' hora(s)';
		}
		else
		{
			return round($dias) . ' dia(s)';
		}
	}
	
	function _obter_meses()
	{
		return array('01' => 'Janeiro', '02' => 'Fevereiro', '03' => 'Março', '04' => 'Abril', '05' => 'Maio', '06' => 'Junho', '07' => 'Julho', '08' => 'Agosto', '09' => 'Setembro', '10' => 'Outubro', '11' => 'Novembro', '12' => 'Dezembro');
	}
	
	function _obter_estados($todos = FALSE)
	{
		if($todos){
			return array('todos' => 'Todos', 'AC' => 'Acre', 'AL' => 'Alagoas', 'AP' => 'Amapá', 'AM' => 'Amazonas', 'BA' => 'Bahia', 'CE' => 'Ceará', 'DF' => 'Distrito Federal', 'ES' => 'Espírito Santo', 'GO' => 'Goiás', 'MA' => 'Maranhão', 'MT' => 'Mato Grosso', 'MS' => 'Mato Grosso do Sul', 'MG' => 'Minas Gerais', 'PA' => 'Pará', 'PB'=> 'Paraíba', 'PR' => 'Paraná', 'PE' => 'Pernambuco', 'PI' => 'Piauí', 'RJ' => 'Rio de Janeiro', 'RN' => 'Rio Grande do Norte', 'RS' => 'Rio Grande do Sul', 'RO' => 'Rondônia', 'RR' => 'Roraima', 'SC' => 'Santa Catarina', 'SP' => 'São Paulo', 'SE' => 'Sergipe', 'TO' => 'Tocantins');
		}else{
			return array('AC' => 'Acre', 'AL' => 'Alagoas', 'AP' => 'Amapá', 'AM' => 'Amazonas', 'BA' => 'Bahia', 'CE' => 'Ceará', 'DF' => 'Distrito Federal', 'ES' => 'Espírito Santo', 'GO' => 'Goiás', 'MA' => 'Maranhão', 'MT' => 'Mato Grosso', 'MS' => 'Mato Grosso do Sul', 'MG' => 'Minas Gerais', 'PA' => 'Pará', 'PB'=> 'Paraíba', 'PR' => 'Paraná', 'PE' => 'Pernambuco', 'PI' => 'Piauí', 'RJ' => 'Rio de Janeiro', 'RN' => 'Rio Grande do Norte', 'RS' => 'Rio Grande do Sul', 'RO' => 'Rondônia', 'RR' => 'Roraima', 'SC' => 'Santa Catarina', 'SP' => 'São Paulo', 'SE' => 'Sergipe', 'TO' => 'Tocantins');
		}
	}
	
	function _validar_cpf($cpf)
	{
		$cpf = str_replace(array('.', '-'), array('', ''), $cpf);
		
		if((!is_numeric($cpf)) or (strlen($cpf) <> 11))
		{
			return FALSE;
		}
		else
		{
			if ( ($cpf == '11111111111') || ($cpf == '22222222222') ||
			($cpf == '33333333333') || ($cpf == '44444444444') ||
			($cpf == '55555555555') || ($cpf == '66666666666') ||
			($cpf == '77777777777') || ($cpf == '88888888888') ||
			($cpf == '99999999999') || ($cpf == '00000000000') )
			{
				return FALSE;
			}
			else
			{
				$cpf_dv = substr($cpf, 9,2);
			}
		}
		for($i=0; $i<=8; $i++)
		{
			$digito[$i] = substr($cpf, $i,1);
		}
		$posicao = 10;
		$soma = 0;
		for($i=0; $i<=8; $i++)
		{
			$soma = $soma + $digito[$i] * $posicao;
			$posicao = $posicao - 1;
		}
		$digito[9] = $soma % 11;
		if($digito[9] < 2)
		{
			$digito[9] = 0;
		}
		else
		{
			$digito[9] = 11 - $digito[9];
		}
		$posicao = 11;
		$soma = 0;
		for ($i=0; $i<=9; $i++)
		{
			$soma = $soma + $digito[$i] * $posicao;
			$posicao = $posicao - 1;
		}
		$digito[10] = $soma % 11;
		if ($digito[10] < 2)
		{
			$digito[10] = 0;
		}
		else
		{
			$digito[10] = 11 - $digito[10];
		}
		$dv = $digito[9] * 10 + $digito[10];
		if ($dv != $cpf_dv)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	function _validar_cnpj($cnpj)
	{
		$cnpj = str_replace(array('.', '/', '-'), array('', '', ''), $cnpj);
		
		if ((!is_numeric($cnpj)) or (strlen($cnpj) <> 14))
		{
			return FALSE;
		}
		else
		{
			$i = 0;
			while ($i < 14)
			{
			$cnpj_d[$i] = substr($cnpj,$i,1);
			$i++;
			}
			$dv_ori = $cnpj[12] . $cnpj[13];
			$soma1 = 0;
			$soma1 = $soma1 + ($cnpj[0] * 5);
			$soma1 = $soma1 + ($cnpj[1] * 4);
			$soma1 = $soma1 + ($cnpj[2] * 3);
			$soma1 = $soma1 + ($cnpj[3] * 2);
			$soma1 = $soma1 + ($cnpj[4] * 9);
			$soma1 = $soma1 + ($cnpj[5] * 8);
			$soma1 = $soma1 + ($cnpj[6] * 7);
			$soma1 = $soma1 + ($cnpj[7] * 6);
			$soma1 = $soma1 + ($cnpj[8] * 5);
			$soma1 = $soma1 + ($cnpj[9] * 4);
			$soma1 = $soma1 + ($cnpj[10] * 3);
			$soma1 = $soma1 + ($cnpj[11] * 2);
			$rest1 = $soma1 % 11;
			if ($rest1 < 2)
			{
				$dv1 = 0;
			}
			else
			{
				$dv1 = 11 - $rest1;
			}
			$soma2 = $soma2 + ($cnpj[0] * 6);
			$soma2 = $soma2 + ($cnpj[1] * 5);
			$soma2 = $soma2 + ($cnpj[2] * 4);
			$soma2 = $soma2 + ($cnpj[3] * 3);
			$soma2 = $soma2 + ($cnpj[4] * 2);
			$soma2 = $soma2 + ($cnpj[5] * 9);
			$soma2 = $soma2 + ($cnpj[6] * 8);
			$soma2 = $soma2 + ($cnpj[7] * 7);
			$soma2 = $soma2 + ($cnpj[8] * 6);
			$soma2 = $soma2 + ($cnpj[9] * 5);
			$soma2 = $soma2 + ($cnpj[10] * 4);
			$soma2 = $soma2 + ($cnpj[11] * 3);
			$soma2 = $soma2 + ($dv1 * 2);
			$rest2 = $soma2 % 11;
			if ($rest2 < 2)
			{
				$dv2 = 0;
			}
			else
			{
				$dv2 = 11 - $rest2;
			}
			$dv_calc = $dv1 . $dv2;
			if ($dv_ori == $dv_calc)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
	}
	
	function _validar_cep($cep)
	{
		return strlen($cep) == 9;
	}
	
	function _validar_telefone($telefone)
	{
		return strlen($telefone) == 14;
	}
	
	function _validar_data($data)
	{
		if (!preg_match('/^\d{2}\/\d{2}\/\d{4}$/', $data) && !preg_match('/^\d{4}\-\d{2}\-\d{2}$/', $data)) return FALSE;
		
		return TRUE;
	}
	
	function _validar_horario($horario)
	{
		return preg_match('/^\d{2}:\d{2}$/', $horario);
	}
	
	function _validar_intervalo_horarios($horario_1, $horario_2)
	{
		$horario_1 = explode(':', $horario_1);
		$horario_2 = explode(':', $horario_2);
		
		return mktime($horario_1[0], $horario_1[1]) <= mktime($horario_2[0], $horario_2[1]);
	}
	
	function _converter_numero_inteiro($numero_inteiro)
	{
		return str_replace('.', '', $numero_inteiro);
	}
	
	function _converter_numero_decimal($numero_decimal)
	{
		return str_replace(array('.', ','), array('', '.'), $numero_decimal);
	}
	
	function _obter_grupos()
	{
		return array('administradores' => 'Administradores', 'gestores_comerciais' => 'Gestores Comerciais', 'gestores_financeiros' => 'Gestores Financeiros', 'representantes' => 'Representantes', 'prepostos' => 'Prepostos');
	}
	
	// dropdowns
	
	function _gerar_dropdown_clientes()
	{
		$conteudo = '<div dojoType="dojox.data.QueryReadStore" jsId="clientes_json" url="' . site_url('json/clientes') . '"></div><p style="float: left; margin-right: 10px;">' . form_label('Cliente:' . br() . form_dropdown('codigo_loja_cliente', array(), NULL, 'dojoType="dijit.form.FilteringSelect" autoComplete="false" store="clientes_json" searchAttr="_valor" pageSize="20" hasDownArrow="false" promptMessage="Digite o nome ou CPF/CNPJ do cliente procurar." id="codigo_loja_cliente" onChange="codigo_loja_cliente_modificado" displayedValue="' . $_POST['label_codigo_loja_cliente'] . '" style="width: 350px;"')) . '<input type="hidden" name="label_codigo_loja_cliente" /></p>';
		$conteudo .= '
			<script type="text/javascript">
				function codigo_loja_cliente_modificado(valor)
				{
					$("input[name=label_codigo_loja_cliente]").val(dijit.byId("codigo_loja_cliente").attr("displayedValue"));
				}
			</script>
		';
		
		return $conteudo;
	}
	
	function _gerar_dropdown_produtos()
	{
		$conteudo = '<div dojoType="dojox.data.QueryReadStore" jsId="produtos_json" url="' . site_url('json/produtos') . '"></div><p style="float: left; margin-right: 10px;">' . form_label('Produto:' . br() . form_dropdown('codigo_produto', array(), NULL, 'dojoType="dijit.form.FilteringSelect" autoComplete="false" store="produtos_json" searchAttr="_valor" pageSize="20" hasDownArrow="false" promptMessage="Digite o código ou descrição do produto para procurar." id="codigo_produto" onChange="codigo_produto_modificado" displayedValue="' . $_POST['label_codigo_produto'] . '" style="width: 350px;"')) . '<input type="hidden" name="label_codigo_produto" /></p>';
		$conteudo .= '
			<script type="text/javascript">
				function codigo_produto_modificado(valor)
				{
					$("input[name=label_codigo_produto]").val(dijit.byId("codigo_produto").attr("displayedValue"));
				}
			</script>
		';
		
		return $conteudo;
	}
	
	function debug($variavel, $var_dump = FALSE)
	{
		echo '<pre>';
		
		if ($var_dump)
		{
			var_dump($variavel);
		}
		else
		{
			print_r($variavel);
		}
		
		echo '</pre>';
	}
	
	function caracterEsp($string)
	{ 
	 	$charEsp = array('Á','À','Â','Ã','É','È','Ê','Í','Î','Ó','Ò','Ô','Õ','Ú','Ù','Û','Ç');  
		
		foreach($charEsp as $valorChar)
		{
			$string = str_replace($valorChar,'%',$string);
		} 
		return $string; 
	}
	
	function specialChars($string) {
		
		$palavra = str_replace( array('Š','Œ','Ž','š','œ','ž','Ÿ','¥','µ','À','Á','Â','Ã','Ä','Å','Æ','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ð','Ñ','Ò','Ó','Ô','Õ','Ö','Ø','Ù','Ú','Û','Ü','Ý','ß','à','á','â','ã','ä','å','æ','ç','è','é','ê','ë','ì','í','î','ï','ð','ñ','ò','ó','ô','õ','ö','ø','ù','ú','û','ü','ý','ÿ'), 
		array('S','O','Z','s','o','z','Y','Y','u','A','A','A','A','A','A','A','C','E','E','E','E','I','I','I','I','D','N','O','O','O','O','O','O','U','U','U','U','Y','s','a','a','a','a','a','a','a','c','e','e','e','e','i','i','i','i','o','n','o','o','o','o','o','o','u','u','u','u','y','y'),$string);
		
		$palavranova = strtoupper($palavra);
		
		return $palavranova; 
	}
}