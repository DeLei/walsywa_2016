<div class="caixa">
	<ul>
		<li><?= anchor('feiras', 'Voltar', 'onclick="history.go(-1); return false;"')?></li>
	</ul>
</div>
<?php 
	if($atendimento)
	{
		$ArrayOper = array(1 => 'FATURAMENTO', 2 => 'ORÇAMENTO', 3 => 'ATENDIMENTO');
		$ArrayFrete = array('C' => 'CIF', 'F' => 'FOB');
		$ArrayContat = array(1 => 'RECEPTIVO', 2 => 'ATIVO', 3 => 'FAX', 4 => 'REPRESENTANTE');
		$ArrayFilial = array('01' => '01-JUNDIAÍ','02' => '02-TRÊS CORAÇÕES','03' => '03-CAMANDUCAIA','04' => '04-PIÇARRAS','05' => '05-LOUVEIRA');
		/*
		echo '<pre>';
		print_r($atendimento);
		echo '</pre>';
		*/
?>
		<h2>Atendimentos</h2>
		<h3>Cliente: <?= ($atendimento?$atendimento['UA_CLIENTE'].' - '.utf8_encode($atendimento['UA_ZZNOMCL']):''); ?></h3>

		<table class="info" style="width:100%;">
	
			<tr>
				<th>Número:</th>
				<td><?=$atendimento[$this->_db_cliente['campos']['atendimentos']['numero']]?></td>
				
				<th>Data do atendimento:</th>
				<td><?=($atendimento[$this->_db_cliente['campos']['atendimentos']['data_atendimento']]?date('d/m/Y',strtotime($atendimento[$this->_db_cliente['campos']['atendimentos']['data_atendimento']])):'');?></td>
				
				<th>Data de retorno:</th>
				<td><?=($atendimento[$this->_db_cliente['campos']['atendimentos']['data_retorno']]?date('d/m/Y',strtotime($atendimento[$this->_db_cliente['campos']['atendimentos']['data_retorno']])):'');?></td>
				
				<th>Hora de retorno:</th>
				<td><?=$atendimento[$this->_db_cliente['campos']['atendimentos']['hora_retorno']]?></td>
			</tr>
			
			<tr>
				<th>Código do representante:</th>
				<td><?=$atendimento[$this->_db_cliente['campos']['representantes']['codigo']]?></td>
				
				<th>Representante:</th>
				<td colspan="5"><?=utf8_encode($atendimento[$this->_db_cliente['campos']['representantes']['nome']])?></td>
			</tr>
			
			<tr>
				<th>Filial:</th>
				<td><?=$ArrayFilial[$atendimento[$this->_db_cliente['campos']['atendimentos']['filial']]]?></td>
				
				<th>Empresa:</th>
				<td><?=$atendimento[$this->_db_cliente['campos']['atendimentos']['empresa']]?></td>
				
				<th>Tipo contato:</th>
				<td><?=($atendimento[$this->_db_cliente['campos']['atendimentos']['tipo_contato']]?$ArrayContat[$atendimento[$this->_db_cliente['campos']['atendimentos']['tipo_contato']]]:'')?></td>
				
				<th>Operação:</th>
				<td><?=$ArrayOper[$atendimento[$this->_db_cliente['campos']['atendimentos']['operacao']]]?></td>
			</tr>
			
			<tr>
				<th>Código do contato:</th>
				<td><?=$atendimento[$this->_db_cliente['campos']['atendimentos']['cod_contato']]?></td>
				
				<th>Nome do contato:</th>
				<td><?=utf8_encode($atendimento[$this->_db_cliente['campos']['atendimentos']['nome_contato']])?></td>
				
				<th>Código do operador:</th>
				<td><?=$atendimento[$this->_db_cliente['campos']['atendimentos']['cod_operador']]?></td>
				
				<th>Nome do operador:</th>
				<td><?=utf8_encode($atendimento['U7_NREDUZ'])?></td>
				
			</tr>
			<tr>
				
				<th>Tabela de preço:</th>
				<td><?=$atendimento[$this->_db_cliente['campos']['atendimentos']['tabela_preco']].'-'.utf8_encode($atendimento[$this->_db_cliente['campos']['tabelas_precos']['descricao']])?></td>
				
				<th>Código da mídia:</th>
				<td><?=$atendimento[$this->_db_cliente['campos']['atendimentos']['midia']]?></td>
				
				<th>Descrição da mídia:</th>
				<td colspan="3"><?=utf8_encode($atendimento['UH_DESC'])?></td>
			</tr>

			
			<tr>	
				<th>Condição de pagamento:</th>
				<td><?=$atendimento[$this->_db_cliente['campos']['atendimentos']['cond_pagamento']].'-'.utf8_encode($atendimento[$this->_db_cliente['campos']['formas_pagamento']['descricao']])?></td>
				
				<th>Fatura mínima:</th>
				<td><?=$atendimento[$this->_db_cliente['campos']['atendimentos']['fatura_min']]?></td>
				
				<th>Desconto:</th>
				<td><?=$atendimento[$this->_db_cliente['campos']['atendimentos']['desconto']]?></td>
				
				<th>Tipo de frete:</th>
				<td><?=($atendimento[$this->_db_cliente['campos']['atendimentos']['frete']]?$ArrayFrete[$atendimento[$this->_db_cliente['campos']['atendimentos']['frete']]]:'')?></td>
				
			</tr>
			
			<tr>	
				<th>Ocorrência:</th>
				<td colspan="7"><?=utf8_encode($atendimento[$this->_db_cliente['campos']['atendimentos']['ocorrencia']])?></td>
			</tr>
			<tr>	
				<th>Mensagem do pedido:</th>
				<td colspan="7"><?=utf8_encode($atendimento[$this->_db_cliente['campos']['atendimentos']['mensagem']])?></td>
			</tr>
			<tr>	
				<th>Mensagem da nota:</th>
				<td colspan="7"><?=utf8_encode($atendimento[$this->_db_cliente['campos']['atendimentos']['mennota']])?></td>
			</tr>
			<tr>	
				<th>Observação:</th>
				<td colspan="7"><?=utf8_encode($atendimento[$this->_db_cliente['campos']['atendimentos']['obs']])?></td>
			</tr>
		</table>
		

<?php
	} 
?>
	