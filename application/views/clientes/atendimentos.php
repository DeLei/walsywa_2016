<h2>Atendimentos </h2>
<h3>Cliente: <?= ($atendimentos?$atendimentos[0]['UA_CLIENTE'].' - '.utf8_encode($atendimentos[0]['UA_ZZNOMCL']):''); ?></h3>

<div class="topo_grid">
	<p><strong>Filtros</strong></p>
	
	<?php echo $filtragem; ?>
</div>

<table class="novo_grid">
	<thead>
		<tr>
			<th>Status</th>
			<th>Número</th>
			<th>Emissão</th>
			<th>Operação</th>
			<th>Representante</th>
			<th>Opções</th>
		</tr>
	</thead>
	
	<tbody>
	
		<?php if (!$atendimentos) : ?>
			<tr><td colspan="6" style="color: #900; padding: 10px;"><strong>Nenhum atendimento encontrado.</strong></td></tr>
		<?php 	else : 
				$ArrayOper = array(1 => 'FATURAMENTO', 2 => 'ORÇAMENTO', 3 => 'ATENDIMENTO');
		?>
			
			<?php foreach ($atendimentos as $atendimento) : ?>
				
				<tr>
					<td><?= $atendimento['CASE_STATUS']; ?></td>
					<td class="right"><?= $atendimento['UA_NUM']; ?></td>
					<td class="right"><?= date('d/m/Y',strtotime($atendimento['UA_EMISSAO'])); ?></td>
					<td><?= $ArrayOper[$atendimento['UA_OPER']]; ?></td>
					<td><?= utf8_encode($atendimento['A3_NOME']); ?></td>
					<td><?= anchor('clientes/detalhes_atendimento/'.$atendimento[$this->_db_cliente['campos']['atendimentos']['cliente']].'/'.$atendimento[$this->_db_cliente['campos']['atendimentos']['numero']], 'Detalhes') ?></td>
				</tr>
				
			<?php endforeach; ?>
		
		<?php endif; ?>	
		
	</tbody>
</table>
<div class="rodape_grid">
	<p><?= number_format($total, 0, ',', '.'); ?> registros</p>
	<?php echo $paginacao; ?>
</div>

