<?php 
	
$conteudo = '
			<div class="caixa">
				<ul>
					<li>' . anchor('#', 'Voltar', 'onclick="history.go(-1); return false;"') . '</li>';
		if(!$this->session->userdata('grupo_usuario')=='engenharia')
		{		
			$conteudo .= '
					<li>' . anchor('criar_pedidos/informacoes_cliente/pedido/' . $cliente['codigo_representante'] . '/0/0/0/' . $cliente['codigo'] . '-' . $cliente['loja'], 'Cadastrar Pedido') . '</li>
					<li>' . anchor('criar_pedidos/informacoes_cliente/orcamento/' . $cliente['codigo_representante'] . '/0/0/0/' . $cliente['codigo'] . '-' . $cliente['loja'], 'Cadastrar Orçamento') . '</li>
					<li>' . anchor('pedidos/?cliente=' . utf8_encode($cliente['nome']).'&my_submit=Filtrar#filtros', 'Consultar Pedidos') . '</li>
					<li>' . anchor('consultas/notas_fiscais/?cliente=' . utf8_encode($cliente['nome']).'&my_submit=Filtrar#filtros', 'Consultar Notas Fiscais') . '</li>
					<li>' . anchor('consultas/titulos_vencidos/?cliente=' . utf8_encode($cliente['nome']).'&my_submit=Filtrar#filtros', 'Consultar Títulos Vencidos').'</li>
					' . ($cliente['ultimo_pedido']['codigo'] ? '<li>' . anchor('pedidos/ver_detalhes/' . $cliente['ultimo_pedido']['codigo'].'&my_submit=Filtrar#filtros', 'Detalhes do Último Pedido') . '</li>' : NULL);
		}
		// chamado 74
			if($cliente['status_formulario'] == 6 )
			{
				if($cliente['permite_inclusao_formulario'] == 'S'){
					$conteudo .= '<li>' . anchor('formulario_visita/index/' . $cliente['codigo'] . '/' . $cliente['loja'], 'Formulário de Visita') . '</li>	';
				}
			}
			else if($cliente['status_formulario'] != 6)
			{
				$conteudo .= '<li>' . anchor('compromissos/ver_agendamentos/cliente/' . $codigo . '/' . $loja, 'Ver Todos os Agendamentos') . '</li>
							  <li>' . anchor('clientes/atendimentos/' . $cliente['codigo'], 'Consultar Atendimentos') . '</li>	
							  <li>' . anchor('formulario_visita/index/' . $cliente['codigo'] . '/' . $cliente['loja'], 'Formulário de Visita') . '</li>	';
			}
		// fim chamado 74
		$conteudo .='</ul>
			</div>';
		
		
		
		//Obter Classes
		$classes = $this->db->select()->from("descontos_classe")->get()->result();
		foreach($classes as $classe){
			$listagem[$classe->codigo_classe] = $classe->nome_classe;
		}
		//------
		
		$conteudo .= heading('Detalhes do Cliente', 2);
		
		//Cor do Status
		if($cliente['status'] == 'ativo')
		{
			$cor_status = 'green';
		}
		else
		{
			$cor_status = 'red';
		}
		
		$conteudo .= '<div style="margin-top:15px"><strong style="font-size:15px">' . $cliente['codigo'] . ' - ' . utf8_encode($cliente['nome']) . '</strong> (' . $this->mascara->formatar_cnpj_cpf($cliente['cpf']) . ') <span style="color:' . $cor_status . '">(' . strtoupper($cliente['status']) . ')</span></div>';
		$conteudo .= '<hr />';
		
		//$conteudo .= '<table><tr><td valign="top">';
		
		$conteudo .= '
			<table cellspacing="0" class="info" style="font-size: 11px">
				<tr>
					<th>CPF/CNPJ:</th>
					<td>' . $this->mascara->formatar_cnpj_cpf($cliente['cpf']) . '</td>
					
					<th>Desde:</th>
					<td>' . ($cliente['data_cadastro_timestamp'] ? date('d/m/Y', $cliente['data_cadastro_timestamp']) : 'não há') . '</td>
					
					<th>Loja:</th>
					<td>' . $cliente['loja'] . '</td>
				</tr>
				
				<tr>
					<th>Endereço:</th>
					<td>'; 
		$conteudo .= utf8_encode($cliente['endereco']);
		
		if($cliente['numero'])
		{
			$conteudo .= ', ' . $cliente['numero'];
		}
		
		$conteudo .= '</td>
					
					<th>Bairro:</th>
					<td colspan="3">' . utf8_encode($cliente['bairro']) . '</td>

				</tr>
				
				<tr>
					<th>Cidade:</th>
					<td>' . utf8_encode($cliente['cidade']) . '</td>
					
					<th>Estado:</th>
					<td colspan="3">' . $cliente['estado'] . '</td>
				</tr>';
				
	
				if(!($cliente['status_formulario'] == 6 )){		
				$conteudo .= 			
				'<tr>
					<th>Telefone:</th>
					<td>' . $cliente['telefone'] . '</td>
					
					<th>CEP:</th>
					<td colspan="3">' . $cliente['cep'] . '</td>
				</tr>
				
				
				<tr>
					<th>Classe:</th>
					<td>' .	($cliente['categoria'] ? element($cliente['categoria'], $listagem) : 'Sem Classe') . '</td>
					
					<th>E-mail:</th>
					<td colspan="3">' . mailto($cliente['email']) . '</td>
				</tr>';
				}else{
					$conteudo .= 			
					'<tr>
						<th>Classe:</th>
						<td>' .	($cliente['categoria'] ? element($cliente['categoria'], $listagem) : 'Sem Classe') . '</td>
						
						<th>CEP:</th>
						<td colspan="3">' . $cliente['cep'] . '</td>
					</tr>'
					
					;
				
				}
		/*
		echo '<pre>';
		print_r($cliente);
		echo '</pre>';
		*/
		$conteudo .= '<tr><td colspan="6">
		<script type="text/javascript">
			$(document).ready(function(){
				$("#mapa").goMap({
					address: "' . utf8_encode($cliente['endereco']) . ', ' . utf8_encode($cliente['cidade']) . ' - ' . $cliente['estado'] . '",
					zoom: 15, 
					maptype: "ROADMAP"
				});
				
				$.goMap.createMarker({  
						address: "' . utf8_encode($cliente['endereco']) . ', ' . utf8_encode($cliente['cidade']) . ' - ' . $cliente['estado'] . '"
				}); 
				
			});
		</script>
		<div id="mapa" style="height:150px; width:100%; float:left; border:1px solid #CCCCCC; margin-top: 10px"></div>';
		
		$conteudo .= '</td></tr></table>';
		
		$conteudo .= '<br /><div style="clear:left"></div>';
	if(!($cliente['status_formulario'] == 6 )){

	
$conteudo .= '<table>
		<tr><th style="padding-top: 0px;">';
		
			$conteudo .= '<h3 style="padding-left:1px;float:left">Informações Financeiras<h3>';//heading('Informações Financeiras', 3);
			if ($cliente['limite_credito'] > 0)
			{
				$limite_credito = '<strong style="color: #060;">R$ ' . number_format($cliente['limite_credito'], 2, ',', '.') . '</strong>';
			}
			else
			{
				$limite_credito = '<strong style="color: #900;">R$ ' . number_format($cliente['limite_credito'], 2, ',', '.') . '</strong>';
			}
			
			if ($cliente['total_titulos_vencidos'] <= 0)
			{
				$titulos_vencidos = '<strong style="color: #060;">R$ ' . number_format($cliente['total_titulos_vencidos'], 2, ',', '.') . '</strong>';
			}
			else
			{
				$titulos_vencidos = '<strong style="color: #900;">R$ ' . number_format($cliente['total_titulos_vencidos'], 2, ',', '.') . '</strong>';
			}
			
			$limite_disponivel = $cliente['total_titulos_em_aberto'] - $cliente['limite_credito'];
			
			$conteudo .= '
				<table cellspacing="0" class="info" style="font-size: 11px">
					<tr>
						<th>Limite de crédito:</th>
						<td>R$ ' . $limite_credito . '</td>
					</tr>
					<tr>
						<th>Títulos em aberto:</th>
						<td>R$ ' . number_format($cliente['total_titulos_em_aberto'], 2, ',', '.') . '</td>
					</tr>
					<tr>
						<th>Títulos vencidos:</th>
						<td>' . $titulos_vencidos . '</td>
					</tr>
					
					<tr>
						<th>Limite de crédito disponível:</th>
						<td>R$ ' . number_format($limite_disponivel , 2, ',', '.') . '</td>
					</tr>
				</table>';
			
			if ($cliente['ultimo_pedido']['emissao_timestamp'])
			{
				$data_ultimo_pedido = date('d/m/Y', $cliente['ultimo_pedido']['emissao_timestamp']);
			}
			else
			{
				$data_ultimo_pedido = " -- ";
			}
			
			$conteudo .= '<br /><br /><div style="clear:both"></div>';
$conteudo .= '</th>';
$conteudo .= '<td style="">';
		//---------------
		
		
			$conteudo .= heading('Informações do Último Pedido', 3);
			
			$conteudo .= '
			<table cellspacing="0" class="info" style="font-size: 11px">
				<tr>
					<th>Data do último pedido:</th>
					<td>' . $data_ultimo_pedido . '</td>
				</tr>
				<tr>
					<th>Valor do último pedido:</th>
					<td>' .  ($cliente['ultimo_pedido']['codigo_pedido'] ? anchor('pedidos/ver_detalhes/0/' . $cliente['ultimo_pedido']['codigo_pedido'] . '/0/' . $cliente['ultimo_pedido']['filial'], 'R$ ' . number_format(($cliente['ultimo_pedido']['valor']?$cliente['ultimo_pedido']['valor']:0), 2, ',', '.')) : number_format(($cliente['ultimo_pedido']['valor']?$cliente['ultimo_pedido']['valor']:0), 2, ',', '.')) . '</td>
				</tr>
				<tr>
					<th>Forma de Pagamento:</th>
					<td>' . ($cliente['ultimo_pedido']['descricao_forma_pagamento'] ? utf8_encode($cliente['ultimo_pedido']['descricao_forma_pagamento']) : " -- ") . '</td>
				</tr>
			';
			
			//Média de Faturamento - ultimos 12 meses
			
			if($cliente['ultimo_pedido']['emissao_timestamp'])
			{
				$mes_inicial = mktime(0, 0, 0, date('m')-12);
				$mes_final = time();
				
				$codigo_cliente = $cliente['codigo'].'|'.$cliente['loja'];
				
				$total_faturamento = $this->db_cliente->obter_faturamento(NULL, $mes_inicial, $mes_final, NULL, $codigo_cliente);

				$media_faturamento = $total_faturamento[0]['valor_total'] / 12;
			}
			
			
			
			$conteudo .= '
			<tr>
				<th>Média de Faturamento (ùltimos 12 meses):</th>
				<td>R$ ' . number_format($media_faturamento, 2, ',', '.') . '</td>
			</tr></table>';
$conteudo .= '</td>';
$conteudo .= '</tr>
			</table>';
		
		//---------------
		
		
		
		
		?>
		<br /><br /><div style="clear:both"></div>
		<?php 
		$conteudo .= heading('Históricos', 3);
		$conteudo .= '<p>' . anchor('#', 'Cadastrar Novo Histórico', 'onclick="$(this).parents(\'p\').hide().next().show().prev().prev().hide(); return false;"') . '</p>';
		$conteudo .= form_open(site_url('clientes/criar_historico_ajax/' . $cliente['codigo'] . '/' . $cliente['loja']), 'style="display: none;"');
		$conteudo .= '
			<div style="float: left;">
				<h3>Cadastrar Novo Histórico</h3>
				<p style="float: left; margin-right: 10px;">' . form_label('Pessoa de contato:' . br() . form_input('pessoa_contato', utf8_encode($cliente['nome']))) . '</p>
				<p style="float: left; margin-right: 10px;">' . form_label('Cargo:' . br() . form_input('cargo', NULL, 'size="10"')) . '</p>
				<p style="float: left; margin-right: 10px;">' . form_label('E-mail:' . br() . form_input('email', $cliente['email'])) . '</p>
				<div style="clear: both;"></div>
				<p>' . form_label('Descrição do histórico:' . br() . form_textarea(array('cols' => 51, 'rows' => 8, 'name' => 'descricao'))) . '</p>
				<p>' . form_label(form_checkbox('criar_compromisso', 'sim') . ' Agendar compromisso') . '</p>
				<p>' . form_submit('concluir', 'Concluir') . ' ou ' . anchor('#', 'Cancelar', 'onclick="$(this).parents(\'form\').hide().prev().show().prev().show(); return false;"') . '</p>
			</div>
			
			<div id="agendar_compromisso" style="border-left: 1px solid #DDD; float: left;  margin-left: 20px; padding-left: 20px; display: none;">
				<h3>Agendar Compromisso</h3>
				<p style="float: left; margin-right: 10px;">' . form_label('Data:' . br() . form_input('data_inicial_compromisso', NULL, 'class="datepicker" size="6"')) . '</p>
				<p style="float: left; margin-right: 10px;">' . form_label('Hora:' . br() . form_input('horario_inicial_compromisso', NULL, 'alt="time" size="1"')) . '</p>
				<div style="clear: both;"></div>
				<p>' . form_label('Descrição do compromisso:' . br() . form_textarea(array('cols' => 40, 'rows' => in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) ? 5 : 8, 'name' => 'descricao_compromisso'))) . '</p>
			</div>
			
			<div style="clear: both;"></div>
		';
		$conteudo .= form_close();
		
		////////////////////////////////////////////
		$filtros = array(
			array(
				'nome' => 'contato',
				'descricao' => 'Contato',
				'tipo' => 'texto',
				'campo_mysql' => 'pessoa_contato',
				'ordenar' => 1
			),
			array(
				'nome' => 'protocolo',
				'descricao' => 'Protocolo',
				'tipo' => 'texto',
				'campo_mysql' => 'protocolo',
				'ordenar' => 2
			),
			array(
				'nome' => 'criacao',
				'descricao' => 'Data',
				'tipo' => 'data',
				'campo_mysql' => 'timestamp',
				'ordenar' => 0
			)
		);
		$conteudo .= '<h3>Históricos</h3>';
		$conteudo .= '<div class="topo_grid">';
		$conteudo .= '	<p><strong>Filtros</strong></p>';
		
		$conteudo .= form_open(current_url(), array('method' => 'get'));
		
		$conteudo .= $filtragem;
		
		$conteudo .= '<div style="clear: both;"></div>';
	
		$conteudo .= form_close();
		
		$conteudo .= '</div>';
		
		////////////////////////////////////////////
		
		
		//$conteudo .= $this->db->last_query();
		
		// RETORNA PAGINACAO PRONTA
		
		
		
		
		
		////////////////////////////////////////////
		
		$conteudo .= '
			<script type="text/javascript">
				$(document).ready(function() {
					$("input[name=criar_compromisso]").click(function() {
						$("#agendar_compromisso").toggle();
					});
					
					$("input[name=concluir]").click(function() {
						var _this = this;
						
						$(this).attr("disabled", "disabled").val("Carregando...");
						
						$.post($(this).parents("form").attr("action"), $(this).parents("form").serialize(), function(dados) {
							if (dados.erro)
							{
								alert(dados.erro);
							}
							else
							{
								window.location = window.location;
							}
							
							$(_this).removeAttr("disabled").val("Concluir");
						}, "json");
					});
				});
			</script>
		';
		
		$conteudo .= $view_historicos;
		$conteudo .= $paginacao;
		}
		
	echo $conteudo;	