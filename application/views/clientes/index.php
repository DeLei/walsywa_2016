﻿<h2>Clientes</h2>

<div class="ajuda"><p>Clicando em "+ Opções" você pode <strong>cadastrar pedidos, consultar pedidos, notas fiscais e títulos vencidos</strong>.</p></div>

<?php
	if ($_GET['erro']){
		echo '<p class="erro"> Limite de consultas diárias esgotadas. Você só poderá efetuar novas consultas a partir de amanhã. </p>';
	}else if ($erro){
		echo '<p class="erro">' . $erro . '</p>';
	}
?>

<div class="topo_grid">
	<p><strong>Filtros</strong></p>
	
	<?php echo $filtragem; ?>
        
</div>

<table class="novo_grid">
	<thead>
		<tr>
			<th>Status</th>
			<th style="<?= !in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros','engenharia')) ? 'display: none;' : NULL; ?>">Representante</th>
			<th>Código</th>
			<th>Loja</th>
			<th>Nome/Razão Social</th>
			<th>Nome Fantasia</th>
			<th>Última Compra</th>
			<th>Opções</th>
		</tr>
	</thead>
	
	<tbody>
	<?php if (!$this->input->get('my_submit')) : ?>
			<tr><td colspan="10" style="color: #900; padding: 10px;"><strong>Realize um filtro.</strong></td></tr>
	<?php else : ?>
		<?php if (!$clientes) : ?>
			<tr><td colspan="10" style="color: #900; padding: 10px;"><strong>Nenhum cliente encontrado.</strong></td></tr>
		<?php else : ?>
			
			<?php foreach ($clientes as $cliente) : ?>
				<tr>
					<td class="center"><?=img(base_url() . 'misc/imagens/clientes/' . $cliente['STATUS'] . '.png') ?></td>
					<td style="<?= !in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros','engenharia')) ? 'display: none;' : NULL; ?>"><?= utf8_encode($cliente['A3_NOME']); ?></td>
					<td class="right"><?= $cliente['A1_COD']; ?></td>
					<td class="right"><?= $cliente['A1_LOJA']; ?></td>
					<td><?= utf8_encode($cliente['A1_NOME']); ?></td>
                                        <td><?=  utf8_encode($cliente['A1_NREDUZ']); ?></td>
                                    <!--CUSTOM: MPD-220 - 07/08/2013-->
					<td><?= ($cliente['A1_ULTCOM'] ? substr($cliente['A1_ULTCOM'], 6, 2)."/".substr($cliente['A1_ULTCOM'], 4, 2)."/".substr($cliente['A1_ULTCOM'], 0, 4) : "Não há"); ?></td>
                                    <!--FIM CUSTOM: MPD-220 - 07/08/2013-->
					<td class="center" style="font-size:25px;">
					<?php if(!($cliente['STATUS'] == 5 && $cliente['OKFORM'] == 'N')):?>
					<?php echo anchor('clientes/ver_detalhes/' . $cliente['A1_COD'] . '/' . $cliente['A1_LOJA'], '+ Opções'); ?>
					<?php endif;?>
					</td>
				</tr>
				
			<?php endforeach; ?>
		<?php endif; ?>
	<?php endif; ?>	
		
	</tbody>
</table>

<div class="rodape_grid">

</div>

<table cellpadding="10" cellspacing="10" style="">
	<tr>
		<td colspan="2"><b>Legenda</b></td>
	</tr>
	
	<tr>
		<td style="padding: 5px"><?=img(base_url() . 'misc/imagens/clientes/1.png') ?> </td>
		<td style="padding: 5px"> OK</td>
		
		<td style="padding: 5px"><?=img(base_url() . 'misc/imagens/clientes/2.png') ?> </td>
		<td style="padding: 5px"> Visitas > 30 Dias</td>
	
		<td style="padding: 5px"><?=img(base_url() . 'misc/imagens/clientes/3.png') ?> </td>
		<td style="padding: 5px"> Visitas > 60 Dias</td>
				
		<td style="padding: 5px"><?=img(base_url() . 'misc/imagens/clientes/4.png') ?> </td>
		<td style="padding: 5px"> Visitas > 90 Dias</td>
		
		<td style="padding: 5px"><?=img(base_url() . 'misc/imagens/clientes/8.png') ?> </td>
		<td style="padding: 5px"> Visitas > 90 Dias, lista não visitados</td>
		
		
		<td style="padding: 5px"><?=img(base_url() . 'misc/imagens/clientes/9.png') ?> </td>
		<td style="padding: 5px"> Visitas > 120</td>
	
		<td style="padding: 5px"><?=img(base_url() . 'misc/imagens/clientes/7.png') ?> </td>
		<td style="padding: 5px"> Não Processado</td>	
	
		
		<td style="padding: 5px"><?=img(base_url() . 'misc/imagens/clientes/5.png') ?> </td>
		<td style="padding: 5px"> Clientes outro vendedor</td>
		
		<td style="padding: 5px"><?=img(base_url() . 'misc/imagens/clientes/6.png') ?> </td>
		<td style="padding: 5px"> Disponível</td>
		
		
		
		
	</tr>
	
</table>