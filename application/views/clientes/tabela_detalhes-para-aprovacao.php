<table class="novo_grid">

	<thead>
		<tr>
			<th>Data</th>
			<th>Contato</th>
			<th>Cargo</th>
			<th>E-mail</th>
			<th>Protocolo</th>
			<th>Op��es</th>
		</tr>
	</thead>
	
	<tbody>
	<?php if (!$this->input->get('my_submit')) : ?>
			<tr><td colspan="10" style="color: #900; padding: 10px;"><strong>Realize um filtro.</strong></td></tr>
	<?php else : ?>
		<?php if (!$clientes) : ?>
			<tr><td colspan="10" style="color: #900; padding: 10px;"><strong>Nenhum cliente encontrado.</strong></td></tr>
		<?php else : ?>
			
			<?php foreach ($clientes as $cliente) : ?>
				
				<tr>
					<td style="<?= !in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) ? 'display: none;' : NULL; ?>"><?= $cliente['A3_NOME']; ?></td>
					<td class="right"><?= $cliente['A1_COD']; ?></td>
					<td class="right"><?= $cliente['A1_LOJA']; ?></td>
					<td><?= $cliente['A1_NOME']; ?></td>
					<td><?= $cliente['A1_NREDUZ']; ?></td>
					<td class="right"><?= $this->mascara->formatar_cnpj_cpf($cliente['A1_CGC']); ?></td>
					<td class="center"><?= $cliente['A1_TEL']; ?></td>
					<td class="center"><?php echo anchor('clientes/ver_detalhes/' . $cliente['A1_COD'] . '/' . $cliente['A1_LOJA'], '+ Op��es'); ?></td>
				</tr>
				
			<?php endforeach; ?>
		<?php endif; ?>
	<?php endif; ?>	
		
	</tbody>
</table>

<div class="rodape_grid">
	<p><?= number_format($total, 0, ',', '.'); ?> registros encontrados.</p>
	
	<?php echo $paginacao; ?>
</div>

=========================

		<table cellspacing="0" class="novo_grid" style="margin-top: 10px;">
			<thead>
				<tr>
					<th>Data</th>
					<th>Contato</th>
					<th>Cargo</th>
					<th>E-mail</th>
					<th>Protocolo</th>
					<th>Op��es</th>
					</tr>
				</thead>
			<tbody>
	<?php
		$entrou = false;
		$tabela_corpo = '';
		$conteudo = '';
		foreach ($historicos as $historico)
		{
			$entrou = true;
			$tabela_corpo .=
				'<tr>
					<td>' . date('d/m/Y H:i', $historico->timestamp) . '</td>
					<td>' . $historico->pessoa_contato . '</td>
					<td>' . $historico->cargo . '</td>
					<td>' . mailto($historico->email) . '</td>
					<td>' . $historico->protocolo . '</td>
					<td>
						<a class="colorbox_inline" href="#historico_' . $historico->id . '">Ver Detalhes</a><div style="display: none;">
						<div id="historico_' . $historico->id . '" style="max-width: 740px;">
							<table cellspacing="5">
								<tr>
									<td><strong>Cliente:</strong></td>
									<td>' . $cliente['nome'] . '</td></tr><tr><td><strong>Contato:</strong></td>
									<td>' . $historico->pessoa_contato . '</td></tr><tr><td><strong>Cargo:</strong></td>
									<td>' . $historico->cargo . '</td></tr><tr><td><strong>E-mail:</strong></td>
									<td>' . mailto($historico->email) . '</td>
								</tr>
							</table>
							<p>' . nl2br($historico->descricao) . '</p>
						</div>
					</div>
					</td>
				</tr>';
		}
		
		// HIST�RICOS **
		if($entrou){
			$tabela_corpo.= '</tbody></table>';
			$conteudo .= $tabela_cabecalho . $tabela_corpo . '<center><br>'.$total_registro.' registros encontrados.<br> </center>';
		}
		else $conteudo .= '<center><br>Nenhum registro encontrado.</center>';
		
		echo $conteudo;