<h2>Desconto Máximo por Classe</h2><br>

<table cellspacing="0" width="50%" class="novo_grid">
	<thead>
		<tr>
			<th width='15%'>Código</th>
			<th width='45%'>Descrição</th>
			<th width='20%'>Desconto Máximo</th>
			<th width='20%'>Opções</th>
		</tr>
	</thead>
	<tbody>
		<?php if ($classes) : ?>		
			<?php foreach ($classes as $classe): ?>
				<tr>
					<td class='center'><?= $classe->codigo_classe?></td>
					<td><?= $classe->nome_classe ?></td>
					<td class='center'><?= str_replace('.', ',', $classe->desconto) . '%' ?></td>
					<td class='center'><?= anchor('descontos_classe/editar/' . $classe->id, 'Editar') ?></td>
				</tr>
			<?php endforeach; ?>
		<?php else:?>
			<tr>
				<td colspan="4">Nenhuma classe encontrada.</td>
			</tr>
		<?php endif; ?>
	</tbody>
</table>

<div class="rodape_grid">
	<p><?= number_format($total, 0, ',', '.'); ?> registros encontrados.</p>
</div>