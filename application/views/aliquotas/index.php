<h2>Alíquotas</h2>

<p><?= anchor('aliquotas/criar', 'Nova Alíquota'); ?></p>

<div class="topo_grid">
	<p><strong>Filtros</strong></p>
	
	<?php echo $filtragem; ?>
</div>

<table class="novo_grid">
	<thead>
		<tr>
			<th>Origem</th>
			<th>Destino</th>
			<th>Alíquota</th>
			<th>Procedência</th>
			<th>Opções</th>
		</tr>
	</thead>
	
	<tbody>
	<?php if (!$this->input->get('my_submit')) : ?>
			<tr><td colspan="5" style="color: #900; padding: 10px;"><strong>Realize um filtro.</strong></td></tr>
	<?php else : ?>
		<?php if (!$aliquotas) : ?>
			<tr><td colspan="5" style="color: #900; padding: 10px;"><strong>Nenhuma alíquota encontrado.</strong></td></tr>
		<?php else : ?>
			<?php foreach ($aliquotas as $aliquota) : ?>
				<tr>
					<td><?= element($aliquota->origem, $estados); ?></td>
					<td><?= element($aliquota->destino, $estados); ?></td>
					<td class="center"><?= number_format($aliquota->alicota, 2, ',', '.') . '%'; ?></td>
					<td><?= element($aliquota->procedencia, $procedencias); ?></td>
					<td class="center"><?= anchor('aliquotas/criar/' . $aliquota->id, 'Editar'); ?></td>
				</tr>
			<?php endforeach; ?>
		<?php endif; ?>
	<?php endif; ?>	
	</tbody>
</table>

<div class="rodape_grid">
	<p><?= number_format($total, 0, ',', '.'); ?> registros encontrados.</p>
	
	<?php echo $paginacao; ?>
</div>