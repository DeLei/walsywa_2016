<?php if($edicao): ?>
	<h2> Editar Alíquota </h2>
<?php else: ?>
	<h2> Criar Alíquota </h2>
<?php endif; ?>


<?php if($erro): ?>
	<p class="erro"> <?= $erro; ?> </p>
<?php endif; ?>


<?= form_open(current_url()); ?>

<p> <?= form_label('Origem:' . br() . form_dropdown('origem', array(0 => 'Selecione...') + $estados, ($this->input->post('origem') ? $this->input->post('origem') : $aliquota->origem))); ?> </p>
<p> <?= form_label('Destino:' . br() . form_dropdown('destino', array(0 => 'Selecione...') + $estados, ($this->input->post('destino') ? $this->input->post('destino') : $aliquota->destino))); ?> </p>
<p> <?= form_label('Alíquota (%):' . br() . form_input('alicota', ($this->input->post('alicota') ? $this->input->post('alicota') : number_format($aliquota->alicota, 2, ',', '.')), 'size="10" alt="porcento"')); ?> </p>
<p> <?= form_label('Procedência:' . br() . form_dropdown('procedencia', array(0 => 'Selecione...') + $procedencias, ($this->input->post('procedencia') ? $this->input->post('procedencia') : $aliquota->procedencia))); ?> </p>

<div style="clear: both;"></div><br />

<p> <?= form_submit('', 'Concluir') . ' ou ' . anchor('aliquotas', 'Cancelar') ?> </p>

<?= form_close(); ?>