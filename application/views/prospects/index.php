<h2>Prospects</h2>

<?php
	if(in_array($this->session->userdata('grupo_usuario'), array('representantes', 'gestores_comerciais', 'supervisores')))
	{
?>
		<h3>Cadastrar Novo Prospect</h3>

		<ul>
			<li><?php echo anchor('prospects/criar/pessoa_fisica', 'Pessoa Física'); ?></li>
			<li><?php echo anchor('prospects/criar/pessoa_juridica', 'Pessoa Jurídica'); ?></li>
		</ul>
<?php
	}
?>	
<h3>Prospects Cadastrados</h3>

<div class="ajuda"><p>Clicando em "+ Opções" você pode <strong>cadastrar orçamentos, editar e converter prospects em clientes</strong>.</p></div>

<div class="topo_grid">
	<p><strong>Filtros</strong></p>
	
	<?php echo $filtragem; ?>
</div>

<table class="novo_grid">
	<thead>
		<tr>
			<th>Status</th>
			<th>Criação</th>
			<th style="<?= !in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais','engenharia', 'gestores_financeiros')) ? 'display: none;' : NULL; ?>">Representante</th>
			<th>Tipo de Pessoa</th>
			<th>Nome</th>
			<th>Fantasia</th>
			<th>Cidade</th>
			<th>Tel. 1</th>
			<th>Tel. 2</th>
			<th>Opções</th>
		</tr>
	</thead>
	
	<tbody>
	<?php if (!$this->input->get('my_submit')) : ?>
				<tr><td colspan="10" style="color: #900; padding: 10px;"><strong>Realize um filtro.</strong></td></tr>
	<?php else : ?>
		<?php if (!$prospects) : ?>
			
			<tr><td colspan="10" style="color: #900; padding: 10px;"><strong>Nenhum prospect encontrado.</strong></td></tr>
			
		<?php else : ?>
			
			<?php foreach ($prospects as $prospect) : ?>
				
				<tr>
					<td class='center'><?php echo img(base_url() . 'misc/imagens/prospects/' . $prospect->status. '.png') ?></td>
				
					<td class='center'><?php echo date('d/m/Y H:i:s', $prospect->timestamp); ?></td>
					
					<td style="<?= !in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros','engenharia')) ? 'display: none;' : NULL; ?>"><?=utf8_encode($prospect->nome_do_representante)?></td>
					<td><?php echo element($prospect->tipo_pessoa, $tipos_de_pessoa); ?></td>
					<td><?php echo ($prospect->nome ? $prospect->nome : '-'); ?></td>
					<td><?php echo ($prospect->nome_fantasia ? $prospect->nome_fantasia : '-'); ?></td>
					<td><?php echo $prospect->cidade; ?></td>
					<td class='right'><?php echo $prospect->telefone_contato_1; ?></td>
					<td class='right'><?php echo $prospect->telefone_contato_2; ?></td>
					<td class='center'><?php echo anchor('prospects/ver_detalhes/' . $prospect->id, '+ Opções'); ?></td>
				</tr>
				
			<?php endforeach; ?>
			
		<?php endif; ?>
	<?php endif; ?>	
	</tbody>
</table>

<div class="rodape_grid">
	<p><?= number_format($total, 0, ',', '.'); ?> registros encontrados.</p>
	
	<?php echo $paginacao; ?>
</div>
<table cellpadding="10" cellspacing="10" style="border: 1px solid #e5ce32; background-color: #fff">
					
					<tr>
						<td colspan="2"><b>Legenda</b></td>
					</tr>
					
					<tr>
						<td><?= img(base_url() . 'misc/imagens/prospects/ativo.png') ?> </td>
						<td>Ativo</td>
						
						<td><?= img(base_url() . 'misc/imagens/prospects/inativo.png') ?> </td>
						<td>Inativo</td>
					
						
					</tr>
					
</table>