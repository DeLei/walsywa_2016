<?php 
	$exibir_copiar = $this->config->item('exibir_copiar');
	$exibir_editar = $this->config->item('exibir_editar');
?>

<?php 
		if($pedido['tipo'] != 'pedido')
		{
			echo '
			<div class="caixa">
				<ul>
					<li>' . anchor('feiras', 'Voltar', 'onclick="history.go(-1); return false;"') . '</li>
				</ul>
			</div>
		';
		}
?>

<div id="espelho_pedido">

<?php

//Pedido
if($pedido['tipo'] == 'P' OR $pedido['tipo'] == 'pedido')
{
	//Botoes de Copiar e editar
	echo '<p style="margin-bottom: 10px;" class="nao_exibir_impressao">';

	if ($local_pedido == 'sc5_6') {
		if($exibir_copiar){
			echo anchor('criar_pedidos/copiar_pedido_sc/' . $pedido['codigo_representante'] . '/' .$pedido['codigo']. '/' .$pedido['filial'], 'Copiar Pedido', 'class="botao_a"');
		}
	}
	
	if ($this->session->userdata('grupo_usuario') == 'gestores_comerciais' || $this->session->userdata('grupo_usuario') == 'supervisores')
	{
		if($pedido['tipo'] != 'pedido')
		{

			
			// copiar, reprovar ou aprovar pedido apenas se o mesmo não foi importado.
			
				if($pedido['status'] != 'L') {
					if($pedido['status'] != 'R') {
						if ($pedido['status'] != 'I') {
							if ($local_pedido != 'sc5_6') {
								
									echo anchor('criar_pedidos/editar/pedido/' . $pedido['codigo_representante']. '/' . $pedido['id_pedido'] . '/0' . '/'.$modent , 'Editar Pedido', 'class="botao_b"');
								
							}
							if (($pedido['vencimento_shazam'] == '') or ($pedido['vencimento_shazam'] >= date('Ymd'))) {
								echo anchor('pedidos/aprovar_pedido/' . $pedido['id_pedido'], 'Aprovar Pedido', 'class="botao_d" onclick="return confirm(\'Tem certeza que deseja aprovar esse pedido?\')" ');
							} else {
								echo anchor('#', 'Aprovar Pedido', 'class="botao_d" onclick="alert(\'Este Pedido expirou em ' . protheus2data($pedido['vencimento_shazam']) . ' e não pode ser aprovado.\'); return false;" ');
							}
							echo anchor('pedidos/reprovar_pedido/' . $pedido['id_pedido'], 'Reprovar Pedido', 'class="botao_c"');	
						}
					}
				}
		}
		else
		{
			echo '
				<div class="caixa">
					<ul>
						<li>' . anchor('feiras', 'Voltar', 'onclick="history.go(-1); return false;"') . '</li>
					</ul>
				</div>
			';
			
			//if($exibir_copiar){
				//Chamado 001892 - Dois botões de copiar
				//echo anchor('criar_pedidos/informacoes_cliente/pedido/' . $pedido['codigo_representante'] . '/' .$pedido['codigo'] . '/' . TRUE . '/A', 'Copiar Pedido', 'class="botao_a"');
			//}
		}
		
		if($tipo_pedido_2)
		{
			if($tipo_pedido_2 == '1')
			{
				$tipo_texto = 'Imediato';
				$tipo_pedido_2 = '2';
			}
			else 
				if($tipo_pedido_2 == '2')
				{
					$tipo_texto = 'Programado';
					$tipo_pedido_2 = '1';
				}
				
			echo anchor('pedidos/ver_detalhes/pedido/' . $id_pedido_2 . '/0/0/' . $pedido['id_pedido'] . '/' . $tipo_pedido_2 , 'Visualizar Espelho do pedido ' . $tipo_texto, 'class="botao_a"');
		}
		
		
	}
	
	echo '</p>';
	//echo '<div style="clear:both"></div>';
	//-----------
}
else if($pedido['tipo'] == 'O')
{
	// Orçmento

	//Botoes de Copiar e editar
	echo '<p style="margin-bottom: 10px;" class="nao_exibir_impressao">';
	
	if($pedido['status'] == 'A' ||$pedido['status'] == 'L' ){
		if($exibir_editar){
			echo anchor('criar_pedidos/editar/orcamento/' . $pedido['codigo_representante']. '/' . $pedido['id_pedido'], 'Editar Orçamento', 'class="botao_b"');
		}	
	}
	if($exibir_copiar){
		echo anchor('criar_pedidos/informacoes_cliente/orcamento/' . $pedido['codigo_representante']. '/' . $pedido['id_pedido'].'/'.'1', 'Copiar Orçamento', 'class="botao_d"');
	}
	if($pedido['status'] == 'A'  || $pedido['status'] == 'L' ){
	
		if (($pedido['vencimento_shazam'] == '') or ($pedido['vencimento_shazam'] >= date('Ymd'))) {
			echo anchor('pedidos/transformar_pedido/' . $pedido['id_pedido'], 'Transformar em Pedido', 'class="botao_a" onclick="return confirm(\'Tem certeza que deseja transformar esse orçamento em pedido?\')"');
		}
	}
	echo '</p>';
	//echo '<div style="clear:both"></div>';
	//----------- 
}

?>

<div class="caixa_icones nao_exibir_impressao">
	<ul>
		<li style="background: url(<?=base_url() . 'third_party/img/printer1.png'?>) no-repeat;"><a href="<?=site_url() . '/pedidos/ver_pdf/'.(($pedido['tipo'] == 'O')?'orcamento':'pedido').'/' . ($pedido['codigo']?$pedido['codigo'].'/'.$pedido['filial']:$pedido['id_pedido'])?>" target="_blank" >Versão de Impressão</a></li>
		<li style="background: url(<?=base_url() . 'misc/icones/mail_replay.png'?>) no-repeat;"><a href="#" class="enviar_por_email">Enviar Por E-mail</a></li>
	</ul>
</div>

<?php //echo '<pre>';print_r($pedido);echo '</pre>'; ?>


<script type="text/javascript">
	$(document).ready(function() {
		$(".copiar_pedido").click(function() {
			$.get($(this).attr("href"), function(dados) {
				if (dados) {
					$.fn.colorbox({ html: dados });
				} else {
					window.location = "<?=site_url('pedidos/redirecionar_pedido_nao_concluido')?>";
				}
			});
			
			return false;
		});
		
		$(".enviar_por_email").click(function() {
		
			$.fn.colorbox({ html: "<h3 style=\"margin: 0;\">Enviar <?=(($pedido['tipo'] == 'orcamento'  || $pedido['tipo'] == 'O' ) ? 'Orçamento' : 'Pedido')?> por E-mail</h3><p>Digite o e-mail:<br /><input name=\"email\" type=\"text\" size=\"40\" /></p><p>Cópia 1:<br /><input name=\"email_2\" type=\"text\" size=\"40\" /></p><p>Cópia 2:<br /><input name=\"email_3\" type=\"text\" size=\"40\" /></p><p><input type=\"submit\" value=\"Enviar\" class=\"enviar_email\" /> <input type=\"submit\" value=\"Cancelar\" onclick=\"$.fn.colorbox.close();\" /></p>" });
			
			
			return false;
		});
		
		$(".enviar_email").live("click", function() {
			var _this = this;
			var email = $("input[name=email]").val();
			var email_2 = $("input[name=email_2]").val();
			var email_3 = $("input[name=email_3]").val();
			
			$(this).attr("disabled", "disabled").val("Carregando...");
			
			var emails = email;
			
			if (email_2)
			{
				emails += ", " + email_2;
			}
			
			if (email_3)
			{
				emails += ", " + email_3;
			}
			
			$.post("<?=site_url('pedidos/enviar_email/' .(($pedido['tipo'] == 'orcamento' || $pedido['tipo'] == 'O'  )? 'orcamento' : 'pedido').'/'. $id_pedido . '/' . $pedido['filial'])?>", { emails: emails }, function(dados) {
				if (dados.erro)
				{
					alert(dados.erro);
				}
				else
				{
					alert("Sucesso!");
					
					$.fn.colorbox.close();
				}
				
				$(_this).removeAttr("disabled").val("Enviar");
			}, "json");
		});
	});
</script>

<!--
<div style="clear: left;"></div>


<div id="empresa" class="exibir_somente_impressao">
	<img  id="logo" src="<?=base_url() . 'third_party/img/logo.jpg'?>" width="100" height="36" style="margin-right: 10px; float:left;">
	
	<?=$empresa->razao_social?> CNPJ: <?=$empresa->cnpj?><br />
	<?=$empresa->endereco?> - <?=$empresa->bairro?> - <?=$empresa->cep?> - <?=$empresa->cidade?> - <?=$empresa->estado?><br />
	Telefone: <?=$empresa->telefone_1?> - E-mail: <?=$empresa->email?>
</div>
-->
<div style="clear: left;"></div>
<h2>Espelho do <?=($pedido['tipo'] == 'P' || $pedido['tipo'] == 'pedido' ? 'Pedido' : 'Orçamento')?></h2>


<h4 style="margin-top: 15px;">Informações Gerais</h4>
<table>
	<tr>
		<td valign="top">
			<table class="info">
				<?php 
				$representante = $this->db_cliente->obter_representante($pedido['codigo_representante']);
				if($representante)
				{	?>
					<tr>
						<th>Representante:</th>
						<td><?=$representante['codigo'].' - ' . utf8_encode($representante['nome'])?></td>
					</tr>
				<?php
				} ?>
				
				<tr>
					<th>O.C.:</th>
					<td><?=$pedido['ordem_compra'] ? $pedido['ordem_compra'] : $pedido['codigo_ordem_compra'] //=utf8_decode($pedido['ordem_compra'] ? $pedido['ordem_compra'] : $pedido['codigo_ordem_compra'])?></td>
				</tr>
				
				<tr>
					<th>Status:</th>
					<td><?=  obter_status($pedido['status'],$pedido['tipo']);  //=utf8_decode($pedido['ordem_compra'] ? $pedido['ordem_compra'] : $pedido['codigo_ordem_compra'])?></td>
				</tr>
				
				
			</table>
		</td>
		
		<td valign="top">
			<table class="info">
				<tr>
					<th>Número:</th>
					<td><?=($pedido['id_pedido'] ? $pedido['id_pedido'] : $pedido['codigo'])?></td>
				</tr>
				<tr>
					<th>Tipo de frete:</th>
					<td>
						<?
							if($pedido['tipo_frete'] == 'F')
							{
								echo 'FOB';
							}
							else if($pedido['tipo_frete'] == 'C')
							{
								echo 'CIF';
							}
							else if($pedido['tipo_frete'] == 'R')
							{
								echo 'Redespacho';
							}
							else
							{
								echo $pedido['tipo_frete'];
							}
						?>
					</td>
				</tr>
				
			</table>
		</td>


<?php
	if($pedido['codigo_forma_pagamento'])
	{
		$forma_pagamento = $this->db_cliente->obter_forma_pagamento($pedido ? $pedido['codigo_forma_pagamento'] : $pedido['codigo_forma_pagamento']);
	}
	else
	{
		$forma_pagamento = $this->db_cliente->obter_forma_pagamento($pedido ? $pedido['condicao_pagamento'] : $pedido['condicao_pagamento']);
	}
?>
		<td valign="top">
			<table class="info">
				<tr>
					<th>Emissão:</th>
					<td>
						<?php
							if($pedido['emissao_timestamp'])
							{
								echo date('d/m/Y', $pedido['emissao_timestamp']);
							}
							else
							{
								if($pedido['data_emissao'])
								{
									echo date('d/m/Y', strtotime($pedido['data_emissao']));
								}
								else
								{
									echo 'não há';
								}
							}
						?>
					</td>
				</tr>
				<tr>
					<th>Edição:</th>
					<td>
						<?php
							if($pedido['data_edicao'])
							{
								echo date('d/m/Y', strtotime($pedido['data_edicao']));
							}
							else
							{
								echo 'não há';
							}
						?>
					</td>
				</tr>
				<?php if($pedido['data_conversao']): ?>
					<tr>
						<th>Data Conversão de Orçamento:</th>
						<td>
							<?php
								if($pedido['data_conversao'])
								{
									echo date('d/m/Y', strtotime($pedido['data_conversao']));
								}
								else
								{
									echo 'não há';
								}
							?>
						</td>
					</tr>
				<?php endif; ?>
				<?php if (($pedido['tipo'] == 'P') or ($pedido['tipo'] == 'pedido')): ?>
				<tr>
					<th>Aprovação:</th>
					<td>
						<?php
							if($pedido['data_aprovacao'])
							{
								echo date('d/m/Y', strtotime($pedido['data_aprovacao']));
							}
							else
							{
								echo 'não há';
							}
						?>
					</td>
				</tr>
				<?php endif; ?>
				<tr>
					<th>Forma pgto.:</th>
					<td><?=utf8_encode($forma_pagamento['descricao'])?></td>
				</tr>
				
				<!--<tr>
					<th>Data ent. prevista:</th>
					<td><?=($pedido['data_entrega'] != 0 ? date('d/m/Y', strtotime($pedido['data_entrega'])) : 'N/D')?></td>
				</tr>-->
			</table>
		</td>
	</tr>
</table>
<div style="clear: left;"></div>

<?php

if(($pedido['codigo_cliente'] && $pedido['loja_cliente']) || ($pedido['cliente']['codigo'] && $pedido['cliente']['loja']))
{
	
	
	if($pedido['codigo_cliente'] && $pedido['loja_cliente'])
	{
		$cliente = $this->db_cliente->obter_cliente($pedido['codigo_cliente'], $pedido['loja_cliente']);
	}
	else
	{
		$cliente = $this->db_cliente->obter_cliente($pedido['cliente']['codigo'], $pedido['cliente']['loja']);
	}
	
	if($cliente)
	{	
	?>


	<h4 style="margin-top: 15px;">Informações do Cliente</h4>
<table>
	<tr>
		<td valign="top">
			<table class="info">
				<tr>
					<th>Nome:</th>
					<td><?=utf8_encode($cliente['nome'])?></td>
				</tr>
				<tr>
					<th>Endereço:</th>
					<td><?=utf8_encode($cliente['endereco'])?></td>
				</tr>
				<tr>
					<th>E-mail:</th>
					<td><?=utf8_encode($cliente['email'])?></td>
				</tr>
				<tr>
					<th>Classe:</th>
					<td>
						<?php
							$classes = $this->db->select()->from('descontos_classe')->get()->result();
							foreach($classes as $classe){
								$listagem[$classe->codigo_classe] = $classe->nome_classe;
							}
							echo ($cliente['categoria'] ? element($cliente['categoria'], $listagem) : 'Sem Classe');
						?>
					</td>
				</tr>
			</table>
		</td>

		<td valign="top">
			<table class="info">
				<tr>
					<th>CPF/CNPJ:</th>
					<td><?=$cliente['cpf']?></td>
				</tr>
				<tr>
					<th>Tel.:</th>
					<td><?=$cliente['telefone']?></td>
				</tr>
				<tr>
					<th>Contato:</th>
					<td><?=utf8_encode($cliente['pessoa_contato'])?></td>
				</tr>
			</table>
		</td>

		<td valign="top">
			<table class="info">
				<tr>
					<th>Cód./Loja:</th>
					<td><?=$cliente['codigo']?> / <?=$cliente['loja']?></td>
				</tr>
				<tr>
					<th>Bairro/CEP:</th>
					<td><?=utf8_encode($cliente['bairro'])?> / <?=$cliente['cep']?></td>
				</tr>
				<tr>
					<th>Cidade/Est.:</th>
					<td><?=utf8_encode($cliente['cidade'])?> / <?=$cliente['estado']?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
	<?php
	}
}
else
{

	$prospect = $this->db->from('prospects')->where('id', $pedido['id_prospects'])->get()->row();

	//Informações do Prospect
?>
<h4 style="margin-top: 15px;">Informações do Prospect</h4>
			
			
		<table>
			<tr>
				<td valign="top">
					<table class="info">
						<tr>
							<th>Nome:</th>
							<td><?=utf8_encode($prospect->nome)?></td>
						</tr>
						<tr>
							<th>CPF/CNPJ:</th>
							<td><?=$prospect->cpf?></td>
						</tr>
						<tr>
							<th>Telefone:</th>
							<td><?=$prospect->telefone_contato_1?></td>
						</tr>
					</table>
				</td>
				<td valign="top">				
					<table class="info">
						<tr>
							<th>Código:</th>
							<td><?=$prospect->id?></td>
						</tr>
						
						<tr>
							<th>Cidade/Estado:</th>
							<td><?=utf8_encode($prospect->cidade) . '/' . $prospect->estado?></td>
						</tr>
	
						<tr>
							<th>E-mail:</th>
							<td><?=utf8_encode($prospect->email_contato_1)?></td>
						</tr>
					</table>
				</td>
				<td valign="top">				
					<table class="info">
						<tr>
							<th>Endereço:</th>
							<td><?=utf8_encode($prospect->endereco)?></td>
						</tr>
						<tr>
							<th>Bairro:</th>
							<td><?=utf8_encode($prospect->bairro)?></td>
						</tr>
						<tr>
							<th>CEP:</th>
							<td><?=$prospect->cep?></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
<?php

}

?>

<div style="clear: both;"></div>


<?php

	if($produtos){
	//print_r($produtos);
?>

<h4 style="margin-top: 15px;">Produtos</h4>

<table class="novo_grid info largura_tabela  tabela_produtos" cellspacing="0px">
	<thead>
		<tr>
		<th>Item</th>
		<th>Código</th>
		<th>Descrição</th>
		<th>U.M.</th>
		<th>Tabela</th>
		<th>Preço Unit. (R$)</th>
		<th>Desconto Esp. (%)</th>
		<th>Preço Venda (R$)</th>
		<th>Filial</th>
		<th>Quant.</th>
		<th>Total (R$)</th>
		<th>IPI (%)</th>
		<th>ST (R$)</th>
		<th>Total c/ IPI, ST(R$) e Desc(%)</th>
		<th>Peso Total (KG)</th>
		</tr>
	</thead>
	
	<tbody>
	
	<?php
		
	
		$item = count($produtos);
		$total_produtos_venda = 0;
		foreach($produtos as $produto)
		{
		if($item%2==0){
			$aux = true;
		}else{
			$aux = false;
		}
	?>
			
			<?php
				$valor_unitario_produto_desconto = aplicar_desconto($produto['preco_unitario'],$produto['desconto']);
				// ** calcular st				
				$total_dos_produtos_sem_ipi = $valor_unitario_produto_desconto * $produto['quantidade'];
				$total_dos_produtos_com_ipi = $total_dos_produtos_sem_ipi + ($total_dos_produtos_sem_ipi * ($produto['ipi'] / 100));
			?>
			
			<?php
			$total_valor_produto = $valor_unitario_produto_desconto * $produto['quantidade']; 
			$total_final_produto = ((($produto['ipi']?$produto['ipi']:0) * $total_valor_produto) / 100) + $total_valor_produto + ($produto['substituicao_tributaria']?$produto['substituicao_tributaria']:0);
			?>
			<tr <?=($aux?' class="linha_par"':null)?>>
				<td class="center"><?=str_pad($item--, 2, 0, STR_PAD_LEFT)?></td>
				<td class="center"><?=$produto['codigo_produto']?></td>
				<td><?=utf8_encode($produto['descricao_produto'])?></td>
				<td class="right"><?=$produto['unidade_medida']?></td>
				<td>
					<?php						
						$tabela = $this->db_cliente->obter_tabela_precos($produto['tabela_precos']);						
						//$tabela = $this->db_cliente->verificar_tabela_preco($cliente, $produto);					
						echo utf8_encode($tabela['descricao']);		 				
					?>
				</td>
				<td class="right"><?=number_format($produto['preco_unitario'], 5, ',', '.')?></td>
				<td class="right"><?=number_format($produto['desconto'], 5, ',', '.')?></td>
				<td class="right"><?=number_format($valor_unitario_produto_desconto, 5, ',', '.')?></td>
				<td class="right"><?=$produto['filial']?></td>
				<td class="right"><?=$produto['quantidade']?></td>
				
				<td class="right"><?=number_format($total_valor_produto, 5, ',', '.')?></td>
				<td class="right"><?=($produto['ipi']?$produto['ipi']:0)?></td>
				<td class="right"><?=number_format(($produto['substituicao_tributaria']?$produto['substituicao_tributaria']:0), 5, ',', '.')?></td>
				<td class="right"><?=number_format($total_final_produto, 5, ',', '.')?></td>
				<td class="right"><?=number_format($produto['peso_unitario'] * $produto['quantidade'], 5, ',', '.')?></td>
			</tr>
			<?php
				// Totais
				$total_quantidade += $produto['quantidade'];
				$total_peso += $produto['peso_unitario'] * $produto['quantidade'];
				$total_produtos_venda += $total_valor_produto;
				$total_valor += $total_final_produto;
				$frete += $produto['valor_frete'];
			?>
			
	<?php
		}
	?>
	
	</tbody>
</table>

<div style="clear:both"></div>

<h4 style="margin-top: 15px;">Totais</h4>

<table class="novo_grid info largura_tabela">
	<thead>
		<tr>
		<th>Quant. Vend.</th>
		<th>Peso Total (KG)</th>
		<!--<th>Frete (R$)</th>
		<th>Frete por KG (R$):</th>-->
		<th>Produtos (R$)</th>
		<th>Total do Pedido (R$)</th>
		</tr>
	</thead>
	
	<tbody>
		<tr>
			<td style="text-align: right;">
				<?=number_format($total_quantidade, 0, NULL, '.')?>
			</td>
			<td style="text-align: right;">
				<?=number_format($total_peso, 5, ',', '.')?>
			</td>
			<!--<td style="text-align: right;">
				<?=number_format($frete, 5, ',', '.')?>
			</td>
			<td style="text-align: right;">
				<?=number_format($frete / $total_peso, 5, ',', '.')?>
			</td>-->
			<td style="text-align: right;">
				<?=number_format($total_produtos_venda , 5, ',', '.')?>
			</td>
			<td style="font-weight: bold; text-align: right;">
				<?=number_format($frete + $total_valor, 5, ',', '.')?>
			</td>
		</tr>
	</tbody>
</table>



<?php
	}else if($itens){
?>
		<h4 style="margin-top: 15px;">Produtos</h4>

		<table class="novo_grid info largura_tabela  tabela_produtos" cellspacing="0px" >
			<thead>
				<tr>
				<th>Item</th>
				<th>Código</th>
				<th>Descrição</th>
				<th>U.M.</th>
				<th>Preço Unit. (R$)</th>
				<th>Desconto Esp. (%)</th>
				<th>Preço Venda (R$)</th>
				<th>Filial</th>
				<th>Quant. Vend.</th>
				<th>Total (R$)</th>
				<th>Quant. Fat.</th>
				<th>Faturado (R$)</th>
				</tr>
			</thead>
			
			<tbody>
			
			<?php
				$it = 1;
				
				foreach($itens as $item)
				{
					if($it%2==0){
						$aux = true;
					}else{
						$aux = false;
					}
			?>

				<?php
					if ($item['desconto'] > 0) {
						$valor_unitario_produto_desconto = ($item['preco_produto'] - (($item['preco_produto'] * $item['desconto']) / 100));
					}else{
						$valor_unitario_produto_desconto = $item['preco_produto'];
					}
				?>
				
				<tr <?=($aux?' class="linha_par"':null)?>>
						<td class="right"><?=$item['codigo_item']?></td>
						<td class="right"><?=$item['codigo_produto']?></td>
						<td><?=utf8_encode($item['descricao_produto'])?></td>
						<td class="center"><?=$item['unidade_medida_produto']?></td>
						<td class="right"><?=number_format(str_replace(",", ".", $item['preco_produto']), 5, ',', '.')?></td>
						<td class="right"><?=number_format($item['desconto'], 5, ',', '.')?></td>
						<td class="right"><?=number_format($valor_unitario_produto_desconto, 5, ',', '.')?></td>
						<td class="right"><?=$item['filial']?></td>
						<td class="right"><?=$item['quantidade_vendida_produto']?></td>
						<td class="right"><?=number_format(str_replace(",", ".", $valor_unitario_produto_desconto) * $item['quantidade_vendida_produto'], 5, ',', '.')?></td>
						<td class="right"><?=$item['quantidade_faturada_produto']?></td>
						<td class="right"><?=number_format(str_replace(",", ".", $valor_unitario_produto_desconto) * $item['quantidade_faturada_produto'], 5, ',', '.')?></td>
					</tr>
			<?php
					$quant_vend += str_replace(",", ".", $item['quantidade_vendida_produto']);
					$quant_fat += str_replace(",", ".", $item['quantidade_faturada_produto']);
					$valor_parcial += str_replace(",", ".", $valor_unitario_produto_desconto) * $item['quantidade_faturada_produto'];
					$valor_total += str_replace(",", ".", $valor_unitario_produto_desconto) * $item['quantidade_vendida_produto'];
					
					$it++;
				}
			?>
			
			</tbody>
		</table>
		
		<div style="clear:both"></div>

		<h4 style="margin-top: 15px;">Totais</h4>

		<table class="novo_grid info largura_tabela">
			<thead>
				<tr>
				<th>Quant. Vend.</th>
				<th>Quant. Fat.</th>
				<th>Valor Parcial</th>
				<th>Valor Total</th>
				</tr>
			</thead>
			
			<tbody>
				<tr>
					<td style="text-align: right;">
						<?=$quant_vend?>
					</td>
					<td style="text-align: right;">
						<?=$quant_fat?>
					</td>
					<td style="text-align: right;">
						<?=number_format($valor_parcial, 5, ',', '.')?>
					</td>
					<td style="text-align: right;">
						<?=number_format($valor_total, 5, ',', '.')?>
					</td>
		
				</tr>
			</tbody>
		</table>
	
<?php
		if ($pedido['notas_fiscais']){
?>
			<div style="clear:both"></div>
			<h4 style="margin-top: 15px;">Notas Fiscais</h4>

			<table class="novo_grid info largura_tabela">
				<thead>
					<tr>
					<th>Código N.F.</th>
					<th>Data Emissão</th>
					<th>Total (R$)</th>
					<th>ICMS (R$)</th>
					<th>IPI (R$)</th>
					<th>Frete</th>
					<th>Transp.</th>
					<th>Tel. Transp.</th>
					<th class="nao_exibir_impressao">Opções</th>
					</tr>
				</thead>
				
				<tbody>
				
					<?php
						foreach ($pedido['notas_fiscais'] as $nota_fiscal){
					?>
							<tr>
								
								<td class="center"><?=$nota_fiscal['codigo']?></td>
								<td class="center"><?=date('d/m/Y', $nota_fiscal['emissao_timestamp'])?></td>
								<td class="right"><?=number_format(str_replace(",", ".", $nota_fiscal['valor_total']), 5, ',', '.')?></td>
								<td class="right"><?=number_format(str_replace(",", ".", $nota_fiscal['valor_icms']), 5, ',', '.')?></td>
								<td class="right"><?=number_format(str_replace(",", ".", ($nota_fiscal['valor_ipi']?$nota_fiscal['valor_ipi']:0)), 5, ',', '.')?></td>
								<td class="center"><?=$nota_fiscal['tipo_frete']?></td>
								<td><?=utf8_encode($nota_fiscal['transportadora']['nome'])?></td>
								<td class="right"><?=$nota_fiscal['transportadora']['telefone']?></td>
								<td class="nao_exibir_impressao center">
									<a href="#nota_fiscal_<?=$nota_fiscal['codigo']?>" class="colorbox_inline">Ver Títulos</a>
									
									<?php
										echo '<div style="display: none;"><div id="nota_fiscal_' . $nota_fiscal['codigo'] . '" style="overflow: hidden;"><table cellspacing="0" style="width: 800px;"><thead><tr><th>Código N.F.</th><th>Parcela</th><th>Status</th><th>Valor (R$)</th><th>Vencimento</th><th>Pagamento</th></tr></thead><tbody>';
										foreach ($nota_fiscal['titulos'] as $titulo)
										{
											echo '<tr><td class="center">' . $nota_fiscal['codigo'] . '</td>
											<td class="center">' . $titulo['parcela'] . '</td><td>' . utf8_encode($titulo['status']) . '</td>
											<td class="center">' . number_format($titulo['valor'], 5, ',', '.') . '</td>
											<td class="center">' . date('d/m/Y', $titulo['vencimento_timestamp']) . '</td>
											<td class="center">' . ($titulo['pagamento_timestamp'] ? date('d/m/Y', $titulo['pagamento_timestamp']) : 'não há') . '</td>
											</tr>';
										}
										echo '</tbody></table></div></div>';
									?>
								</td>
					
							</tr>
					<?php
						}
					?>
				</tbody>
			</table>
<?php
		}
	}
	
if($pedido['obs_pedido'])
echo '<div style="clear:both"></div>
<h4 style="margin-top: 15px;">Observação:</h4>
	<table class="novo_grid info largura_tabela">
		
		<tbody>
			<tr>
				<td class="left" style="border-top:solid 1px #999;">'.utf8_encode($pedido['obs_pedido']).'</td>
			</tr>
		</tbody>
	</table>
	';
?>

<?php if ($pedido['vencimento_shazam']) { ?>
	<div style="margin-top: 12px; display: inline-table">
		<h4 style="margin-top: 15px;">Observação de Desconto:</h4> 
		<table class="novo_grid info largura_tabela">
		<tbody>
			<tr>
				<td class="left" style="border-top:solid 1px #999;">Foram utilizados descontos especiais e irão expirar em <strong> <?php echo protheus2data($pedido['vencimento_shazam']) ?> </strong></td>
			</tr>
		</tbody>
	</table>
	</div>
<?php }	?>

<?php if ($produto['status'] == 'R'){
		echo '<div style="clear:both"></div><br>
		<h4 style="margin-top: 15px;">Motivo da Reprovação:</h4>
			<table class="novo_grid info largura_tabela">
				<tbody>
					<tr>
						<td class="left" style="border-top:solid 1px #999;">'.utf8_encode($produto['motivo_reprovacao']).'</td>
					</tr>
				</tbody>
			</table>';
	}
?>

	<!-- Se for um pedido processado (SC5) exibi as informações de posicionamento -->
	<?php if($local_pedido == 'sc5_6'): ?>

		<div style="clear:both"></div>
		<h4 style="margin-top: 15px;">Posicionamento</h4>
		<table>
			<tr>
				<td valign="top">
					<i>Pedido</i>
					<table class="info" width="80%" style="margin-top:2px;">
						<tr>
							<th>Data:</th>
							<td class="right"><?= ($posicionamento['data_pedido'] ? protheus2data($posicionamento['data_pedido']) : 'Não Há'); ?></td>
						</tr>
						<tr>
							<th>Hora:</th>
							<td class="right"><?= ($posicionamento['hora_pedido'] ? $posicionamento['hora_pedido'] : 'Não Há'); ?></td>
						</tr>
					</table>
				</td>
				
				<td valign="top">
					<i>Liberação(Picking)</i>
					<table class="info" width="80%" style="margin-top:2px;">
						<tr>
							<th>Data:</th>
							<td class="right"><?= ($posicionamento['data_picking'] ? protheus2data($posicionamento['data_picking']) : 'Não Há'); ?></td>
						</tr>
						<tr>
							<th>Hora:</th>
							<td class="right"><?= ($posicionamento['hora_picking'] ? $posicionamento['hora_picking'] : 'Não Há'); ?></td>
						</tr>
					</table>
				</td>
				
				<td valign="top">
					<i>Separação</i>
					<table class="info" width="80%" style="margin-top:2px;">
						<tr>
							<th>Data:</th>
							<td class="right"><?= ($posicionamento['data_separacao'] ? protheus2data($posicionamento['data_separacao']) : 'Não Há'); ?></td>
						</tr>
						<tr>
							<th>Hora:</th>
							<td class="right"><?= ($posicionamento['hora_separacao'] ? $posicionamento['hora_separacao'] : 'Não Há'); ?></td>
						</tr>
					</table>
				</td>
				
				<td valign="top">
					<i>Conferência</i>
					<table class="info" width="80%" style="margin-top:2px;">
						<tr>
							<th>Data:</th>
							<td class="right"><?= ($posicionamento['data_conferencia'] ? protheus2data($posicionamento['data_conferencia']) : 'Não Há'); ?></td>
						</tr>
						<tr>
							<th>Hora:</th>
							<td class="right"><?= ($posicionamento['hora_conferencia'] ? $posicionamento['hora_conferencia'] : 'Não Há'); ?></td>
						</tr>
					</table>
				</td>
				
				<td valign="top">
					<i>Faturamento</i>
					<table class="info" width="80%" style="margin-top:2px;">
						<tr>
							<th>Data:</th>
							<td class="right"><?= ($posicionamento['data_nf'] ? protheus2data($posicionamento['data_nf']) : 'Não Há'); ?></td>
						</tr>
						<tr>
							<th>Hora:</th>
							<td class="right"><?= ($posicionamento['hora_nf'] ? $posicionamento['hora_nf'] : 'Não Há'); ?></td>
						</tr>
					</table>
				</td>
				
				<td valign="top">
					<i>Coleta</i>
					<table class="info" width="80%" style="margin-top:2px;">
						<tr>
							<th>Data:</th>
							<td class="right"><?= ($posicionamento['data_coleta'] ? protheus2data($posicionamento['data_coleta']) : 'Não Há'); ?></td>
						</tr>
						<tr>
							<th>Hora:</th>
							<td class="right"><?= ($posicionamento['hora_coleta'] ? $posicionamento['hora_coleta'] : 'Não Há'); ?></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	<?php endif; ?>
</div>