<?php 
	$exibir_copiar = $this->config->item('exibir_copiar');
	$exibir_editar = $this->config->item('exibir_editar');
?>

<h2><?= $orcamento ? 'Orçamentos' : 'Pedidos Aguardando Análise'; ?></h2>

<div class="ajuda"><p>Clicando em "+ Opções" você pode <strong>imprimir, gerar PDF, enviar por e-mail ou copiar o <?= $orcamento ? 'orçamento' : 'pedido'; ?></strong>.</p></div>

<div class="topo_grid">
	<p><strong>Filtros</strong></p>
	
	<?php echo $filtragem; ?>
</div>

<table class="novo_grid">
	<thead>
		<tr>
			<th>Status</th>
			<th style="<?= !in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) ? 'display: none;' : NULL; ?>">Representante</th>
			<th>Cliente</th>
			<?php
				if($orcamento) 
				{
					echo '<th>Prospect</th>';
				}
			?>
			<th>O. Compra</th>
			<th>Emissão</th>
			<th>Qtd. Vend.</th>
			<th>Total (R$)</th>
			<th style="width: 160px;">Opções</th>
		</tr>
	</thead>
	
	<tbody>
	<?php if (!$this->input->get('my_submit')) : ?>
			<tr><td colspan="10" style="color: #900; padding: 10px;"><strong>Realize um filtro.</strong></td></tr>
	<?php else : ?>
		<?php if (!$pedidos) : ?>
			<tr><td colspan="10" style="color: #900; padding: 10px;"><strong>Nenhum <?= $orcamento ? 'orçamento' : 'pedido'; ?> encontrado.</strong></td></tr>
		<?php else : ?>
			
			<?php foreach ($pedidos as $pedido) : ?>
				
				<tr>
					<td class="center" > <?= obter_status($pedido[$this->_db_cliente['campos']['pedidos_dw']['status']], (($orcamento)?'O':'P'), true) ?></td>
					<td style="<?= !in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) ? 'display: none;' : NULL; ?>"><?=utf8_encode($pedido[$this->_db_cliente['campos']['representantes']['nome']])?></td>
					<td><?= $pedido[$this->_db_cliente['campos']['pedidos_dw']['nome_cliente']]; ?></td>
					<?php
						if($orcamento) 
						{
							echo '<td>' . $pedido[$this->_db_cliente['campos']['pedidos_dw']['nome_prospects']] . '</td>';
						}
					?>
					<td><?= $pedido[$this->_db_cliente['campos']['pedidos_dw']['ordem_compra']]; ?></td>
					<td style="text-align: center;"><?= (trim($pedido[$this->_db_cliente['campos']['pedidos_dw']['data_emissao']]) ? date('d/m/Y', strtotime($pedido[$this->_db_cliente['campos']['pedidos_dw']['data_emissao']])) : 'N/D'); ?></td>
					<td style="text-align: right;"><?= number_format($pedido['quantidade_vendida'], 0, ',', '.'); ?></td>
					<td style="text-align: right;"><?= number_format($pedido['total'], 5, ',', '.'); ?></td>
					<td>	
					
					
						<?php	if($exibir_copiar):?>
							<?= anchor('criar_pedidos/informacoes_cliente/'.(($orcamento)?$orcamento.'/':'pedido/') . $pedido[$this->_db_cliente['campos']['representantes']['codigo']]. '/' . $pedido[$this->_db_cliente['campos']['pedidos_dw']['id_pedido']].'/'.'1', 'Copiar' )?>
						<?php endif;?>
						<?php	if($exibir_editar):?>
							<?php if( ($pedido[$this->_db_cliente['campos']['pedidos_dw']['status']] == 'A'   ) ||  ( $pedido[$this->_db_cliente['campos']['pedidos_dw']['status']] == 'L'   &&  orcamento)  ||$pedido[$this->_db_cliente['campos']['pedidos_dw']['status']] == 'R' ): ?>
							 | <?= anchor('criar_pedidos/editar/'.(($orcamento)?$orcamento.'/':'pedido/') . $pedido[$this->_db_cliente['campos']['representantes']['codigo']]. '/' . $pedido[$this->_db_cliente['campos']['pedidos_dw']['id_pedido']], 'Editar' )?>	
							
							<?php endif;?>
						<?php endif;?>
					 | <?= anchor('pedidos/ver_detalhes/' .(($orcamento)?$orcamento.'/':'pedido/'). $pedido[$this->_db_cliente['campos']['pedidos_dw']['id_pedido']], '+ Opções', ' class="mais_informacoes block_buttom" '); ?>
				</tr>
				
			<?php endforeach; ?>
			
		<?php endif; ?>
	<?php endif; ?>	
	</tbody>
</table>

<div class="rodape_grid">
	<p><?= number_format($total['quantidade'], 0, ',', '.'); ?> registros encontrados com o valor total de R$ <?= number_format($total['valor'], 5, ',', '.'); ?>.</p>
	
	<?php echo $paginacao; ?>
</div>

<?php
	if(!$orcamento)
	{
?>
<table cellpadding="10" cellspacing="10" style="border: 1px solid #e5ce32; background-color: #fff">
					
					<tr>
						<td colspan="2"><b>Legenda</b></td>
					</tr>
					
					<tr>
						<td><?= img(base_url() . 'misc/imagens/pedidos_aguardando/aguardando_analise.png'); ?> </td>
						<td>Aguardando Análise</td>
						
						<td><?= img(base_url() . 'misc/imagens/pedidos_aguardando/reprovado.png'); ?> </td>
						<td>Reprovado</td>
					
						<td><?= img(base_url() . 'misc/imagens/pedidos_aguardando/aguardando_importacao.png'); ?> </td>
						<td>Aguardando Importação</td>
						
						<td><?= img(base_url() . 'misc/imagens/pedidos_aguardando/importado.png'); ?> </td>
						<td>Importado</td>
					</tr>
					
</table>
<?php
	}else{
?>
	<table cellpadding="10" cellspacing="10" style="border: 1px solid #e5ce32; background-color: #fff">
					
					<tr>
						<td colspan="2"><b>Legenda</b></td>
					</tr>
					
					<tr>
						<td><?= img(base_url() . 'misc/imagens/pedidos_aguardando/aguardando_analise.png'); ?> </td>
						<td>Aguardando</td>
						
						<td><?= img(base_url() . 'misc/imagens/pedidos_aguardando/transformado_pedido.png'); ?> </td>
						<td>Transformado em Pedido</td>
					
						
					</tr>
					
</table>


<?php
	}
?>