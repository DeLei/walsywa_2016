<h2>Pedidos Analisados</h2>

<div class="ajuda"><p>Clicando em "+ Opções" você pode <strong>imprimir, gerar PDF, enviar por e-mail ou copiar o pedido</strong>.</p></div>
<?php
	if ($_GET['erro']){
		echo '<p class="erro"> Limite de consultas diárias esgotadas. Você só poderá efetuar novas consultas a partir de amanhã. </p>';
	}else if ($erro){
		echo '<p class="erro">' . $erro . '</p>';
	}
?>

<div class="topo_grid">
	<p><strong>Filtros</strong></p>
	
	<?php echo $filtragem; ?>
</div>

<table class="novo_grid">
	<thead>
		<tr>
			<th>Status</th>
			<th>Filial</th>
			<th style="<?= !in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) ? 'display: none;' : NULL; ?>">Representante</th>
			<th>Cliente</th>
			<th>Cód. Ped.</th>
			<th>Emissão</th>
			<th>Qtd. Vend.</th>
			<th>Qtd. Fat.</th>
			<th>Parcial (R$)</th>
			<th>Total (R$)</th>
			<th>Opções</th>
		</tr>
	</thead>
	
	<tbody>
	<?php if (!$this->input->get('my_submit')) : ?>
			<tr><td colspan="11" style="color: #900; padding: 10px;"><strong>Realize um filtro.</strong></td></tr>
	<?php else : ?>	
		<?php if (!$pedidos) : ?>
			
			<tr><td colspan="11" style="color: #900; padding: 10px;"><strong>Nenhum pedido encontrado.</strong></td></tr>
			
		<?php else : ?>
			
			<?php foreach ($pedidos as $pedido) : ?>
				
				<tr>
					<td class='center'><?= img(base_url() . 'misc/imagens/pedidos_analisados/'.str_replace(' ','_',strtolower($pedido['status'])).'.png'); ?></td>
					<td style="text-align: center;"><?=$pedido['C5_FILIAL']?></td>
					<td style="<?= !in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) ? 'display: none;' : NULL; ?>"><?= utf8_encode($pedido['A3_NOME']); ?></td>
					<td><?= utf8_encode($pedido['A1_NOME']); ?></td>
					<td><?= $pedido['C6_NUM']; ?></td>
					<td style="text-align: center;"><?= (trim($pedido['C5_EMISSAO']) ? date('d/m/Y', strtotime($pedido['C5_EMISSAO'])) : 'N/D'); ?></td>
					<td style="text-align: right;"><?= number_format($pedido['C6_QTDVEN'], 0, ',', '.'); ?></td>
					<td style="text-align: right;"><?= number_format($pedido['C6_QTDENT'], 0, ',', '.'); ?></td>
					<td style="text-align: right;"><?= number_format($pedido['total_parcial'], 5, ',', '.'); ?></td>
					<td style="text-align: right;"><?= number_format($pedido['total'], 5, ',', '.'); ?></td>
					<td><?= anchor('pedidos/ver_detalhes/pedido/' . $pedido['C6_NUM'] . '/0' .'/'. $pedido['C5_FILIAL'] , '+ Opções'); ?></td>
				</tr>
				
			<?php endforeach; ?>
			
		<?php endif; ?>
	<?php endif; ?>	
	</tbody>
</table>

<div class="rodape_grid">
	<p><?= number_format($total, 0, ',', '.'); ?> registros encontrados com o valor total de R$ <?= number_format($total_2['valor'], 5, ',', '.'); ?>.</p>
	
	<?php echo $paginacao; ?>
</div>
<table cellpadding="10" cellspacing="10" style="border: 1px solid #e5ce32; background-color: #fff">
	<tr>
		<td colspan="2"><b>Legenda</b></td>
	</tr>
	
	<tr>
		<td><?= img(base_url() . 'misc/imagens/pedidos_analisados/faturado.png') ?> </td>
		<td> Faturado</td>
		
		<td><?=img(base_url() . 'misc/imagens/pedidos_analisados/parcialmente_faturado.png') ?> </td>
		<td> Parcialmente Faturado</td>
	
		<td><?=img(base_url() . 'misc/imagens/pedidos_analisados/aguardando_faturamento.png') ?> </td>
		<td> Aguardando Faturamento</td>
	
		
	</tr>
	
</table>