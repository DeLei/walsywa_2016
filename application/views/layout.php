<!DOCTYPE html>

<html lang="pt-br">
	<head>
		<meta name="viewport" content="">
		<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE10">
		<meta charset="utf-8">
		<!-- Não armazenar cache -->
		<meta http-equiv="Cache-Control" content="No-Cache">
		<meta http-equiv="Pragma" content="No-Cache">
		<meta http-equiv="Expires" content="0">
		<title>DW Portal do Representante</title>
		<!-- BootStrap -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'third_party/bootstrap-3.3.4-dist/css/bootstrap.css'; ?>" >
		<script type="text/javascript" src="<?php echo base_url() . 'third_party/bootstrap-3.3.4-dist/js/bootstrap.min.js'; ?>"></script>
		
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'third_party/css/screen.css'; ?>" media="screen">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'third_party/css/print.css'; ?>" media="print">
		<?php if (!$this->session->userdata('id_usuario')) : ?> <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'third_party/css/login.css'; ?>" media="screen"> <?php endif; ?>
		
		<!--<link rel="stylesheet" type="text/css" href="<?php // echo base_url() . 'third_party/css/jquery_ui/jquery-ui-1.8.11.custom.css'; ?>" media="screen">-->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'third_party/css/smoothness/jquery-ui-1.8.17.custom.css'; ?>" media="screen">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'third_party/css/multiselect/jquery.multiselect.css'; ?>" media="screen">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'third_party/css/multiselect/jquery.multiselect.filter.css'; ?>" media="screen">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'third_party/css/colorbox/colorbox.css'; ?>" media="screen">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'third_party/css/etapas_pedidos.css'; ?>" media="screen">
		<link rel="shortcut icon" href="<?php echo base_url() . 'third_party/img/logo.png';?>" />
		<script type="text/javascript" src="<?php echo base_url() . 'third_party/js/jquery-1.7.1.min.js'; ?>"></script>
		<script type="text/javascript" src="<?php echo base_url() . 'third_party/js/jquery-ui-1.8.17.custom.min.js'; ?>"></script>
		<script type="text/javascript" src="<?php echo base_url() . 'third_party/js/ui_datepicker_pt-BR.js'; ?>"></script>
		<script type="text/javascript" src="<?php echo base_url() . 'third_party/js/jquery.multiselect.min.js'; ?>"></script>
		<script type="text/javascript" src="<?php echo base_url() . 'third_party/js/jquery.multiselect.filter.min.js'; ?>"></script>
		<script type="text/javascript" src="<?php echo base_url() . 'third_party/js/jquery.colorbox-min.js'; ?>"></script>
		<script type="text/javascript" src="<?php echo base_url() . 'third_party/js/jquery.meio.mask.min.js'; ?>"></script>
		<script type="text/javascript" src="<?php echo base_url() . 'third_party/js/highcharts.js'; ?>"></script>
		<script type="text/javascript" src="<?php echo base_url() . 'third_party/js/php.js'; ?>"></script>
		
		<script type="text/javascript" src="<?php echo base_url() . 'third_party/js/ckeditor/ckeditor.js'; ?>"></script>
		<script type="text/javascript" src="<?php echo base_url() . 'third_party/js/ckeditor/adapters/jquery.js'; ?>"></script>
		
		<!-- CUSTOM: MPD-220_Walsywa_01_05082013 - 09/08/2013 -->
		<script type="text/javascript" src="<?php echo base_url() . 'third_party/js/jquery.formatCurrency-1.4.0.js'; ?>"></script>
		<!-- FIM CUSTOM: MPD-220_Walsywa_01_05082013 - 09/08/2013 -->
                
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> 
		<script type="text/javascript" src="<?php echo  base_url(); ?>third_party/js/jquery.gomap-1.3.0.min.js"></script>
		
		<script type="text/javascript" src="<?php echo  base_url(); ?>third_party/js/developweb.js"></script>
		<script type="text/javascript" src="<?php echo  base_url(); ?>third_party/js/autocomplete.js"></script>
		
		<!-- 
		Inicio do refatoramento do código.		
		-->
		<script type="text/javascript" src="<?php echo  base_url(); ?>third_party/js/criar_pedidos/adicionar_produtos.js?<?php echo time(); ?>"></script>
		
		
		
		<script type="text/javascript">
			$(document).ready(function() {
				<?php if ($this->session->userdata('id_usuario')) : ?>
					
					obter_alertas(true);
					
					<?php if ($this->session->flashdata('exibir_alertas_lightbox')) : ?>
						
						$.fn.colorbox({ inline: true, href: '#alertas_lightbox', width: '95%', scrolling: false });
						
					<?php endif; ?>
					
				<?php endif; ?>
				
				<?php if ($this->session->flashdata('obter_geolocation_para_o_acesso')) : ?>
					if (navigator.geolocation)
					{
						navigator.geolocation.getCurrentPosition(function(position) {
							$.get("<?php echo site_url('usuarios/gravar_geolocation/' . $this->session->flashdata('obter_geolocation_para_o_acesso')); ?>/" + position.coords.latitude + "/" + position.coords.longitude);
						});
					}
				<?php endif; ?>
			});

			function obter_alertas(exibir_carregando)
			{
				if (exibir_carregando) $('.alertas .pedidos .conteudo, .alertas .pendencias .conteudo, .alertas .compromissos .conteudo').html('<p style="margin-bottom: 41px;">Carregando...</p>');
				
				$.get('<?php echo site_url('pedidos/obter_alertas'); ?>', function(dados) {
					$('.alertas .pedidos .conteudo').html(dados.pedidos);
					if(dados.num_pedidos){
						if(dados.num_pedidos > 99){
							dados.num_pedidos = '+';
						}
						$('#span_pedido').addClass("bolinha_alerta");
						$('#span_pedido').html(dados.num_pedidos);
					}	
				},'json');
				
				$.get('<?php echo site_url('pendencias/obter_alertas'); ?>', function(dados) {
					$('.alertas .pendencias .conteudo').html(dados.pendencias);
					if(dados.num_pendencias){
						if(dados.num_pendencias > 99){
							dados.num_pendencias = '+';
						}
						$('#span_pendencia').addClass("bolinha_alerta");
						$('#span_pendencia').html(dados.num_pendencias);
					}
				},'json');
				
				$.get('<?php echo site_url('compromissos/obter_alertas'); ?>', function(dados) {
					$('.alertas .compromissos .conteudo').html(dados.compromissos);
					if(dados.num_compromissos){
						if(dados.num_compromissos > 99){
							dados.num_compromissos = '+';
						}
						$('#span_compromisso').addClass("bolinha_alerta");
						$('#span_compromisso').html(dados.num_compromissos);
					}
				},'json');
				
				setTimeout('obter_alertas(false)', 30000);
			}


			
			
		</script>
		
		<script type="text/javascript"> 
		$( document ).ready(function(){
		
			//$('input[type="submit"]').addClass("btn btn-primary btn-lg");
				$('input[type="text"]').addClass("form-control");
				$('select').addClass("form-control");
				
				$('input[type="submit"]').addClass("btn btn-primary btn-lg");
				$('input[type="button"]').addClass("btn btn-primary btn-xs");
				
				
				$('.ajuda').remove();
				$('#cliente').focus();
				$('#produto').focus();
				
				$('#quantidade').attr("type", "number");
					
				
				/**
				$('#produto').change(function(e) {
					//if(e.which == 13) {
						$("#quantidade").focus();
						alert('You pressed enter!');
						return false;
					//}
				});
				**/
			});
		</script>
		
	</head>
	
	<body class="tundra">
		
	<div id="alerta" title="Alerta"></div>
	<div id="confirmacao" title="Confirmação"></div>
		<div id="geral">
			<?php 
				if ($this->session->userdata('id_usuario'))
				{
					$ultimo_acesso = $this->db->from('acessos_usuarios')->where('id_usuario', $this->session->userdata('id_usuario'))->order_by('id', 'desc')->limit(1)->get()->row();
					
					echo '<p id="informacoes_gerais">'. anchor('usuarios/sair','<span>Sair</span> '. img(base_url() . 'misc/icones/sair.png')).'<br />Bem-vindo(a) <strong>' . $this->session->userdata('nome_usuario') . '</strong>' . ((in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros'))) ? '' : ' ('.$this->shazam_model->obter_shazam_vendedor($this->session->userdata('codigo_usuario'), 'N')->QTDE.' Carta na manga)') . ' ('.$this->shazam_model->obter_shazam_vendedor($this->session->userdata('codigo_usuario'), 'S')->QTDE.' Carta na manga Especial)' . '<br /> <!-- Hoje é ' . date('d/m/Y') . '. -->' . ($ultimo_acesso ? 'Seu último acesso foi ' . date('d/m/Y H:i', $ultimo_acesso->timestamp) : 'Este é seu primeiro acesso') . '.</p>';//.<br />Hoje é ' . date('d/m/Y') . '. ' . ($ultimo_acesso ? 'Seu último acesso foi ' . date('d/m/Y H:i', $ultimo_acesso->timestamp) : 'Este é seu primeiro acesso') . '.</p>';
					
				}
			?>
			
			<!-- <h1>DW Portal do Representante</h1> -->
			<!-- border: 1px solid RED; -->
			
			<?php if ($this->session->userdata('id_usuario')) : ?>
			
			<img src="<?=base_url()?>third_party/img/logo_walsywa.png" style="float: left; height: 60px; ">
			
			<!-- <img src="<?=base_url()?>third_party/img/assinatura_develop.png" style="float: left; margin-top: 30px; margin-left: 20px; width: 350px;"> -->
			
			<?php else : ?>
			
			<!--<img src="<?=base_url()?>third_party/img/logo_walsywa.png" style="float: left; height: 50px; ">-->
			
			<!-- <img src="<?=base_url()?>third_party/img/assinatura_develop.png" style="margin-top: 20px; margin-left: 20px; width: 350px;"> -->
			
			<?php endif; ?>
			
			<div style="clear: both;"></div>
			
			<!--<h4 style="color: #fff; text-align: center; margin: 5px;">Esta é uma versão de demonstração, algumas opções estão desabilitadas.</h4>-->
			
			<?php if ($this->session->userdata('id_usuario')) : ?>
				
				<div id="navegacao">
					<ul>
						<?php 
							if($this->session->userdata('grupo_usuario') == 'engenharia')
							{
						?>
								<li <?php echo ($this->uri->segment(1) == 'prospects') ? 'class="ativo"' : NULL; ?>><?php echo anchor('prospects', img(base_url() . 'misc/icones/agt_family-off.png') . br() . 'Prospects'); ?></li>
								<li <?php echo ($this->uri->segment(1) == 'clientes') ? 'class="ativo"' : NULL; ?>><?php echo anchor('clientes', img(base_url() . 'misc/icones/agt_family.png') . br() . 'Clientes'); ?></li>
								<li <?php echo ($this->uri->segment(1) == 'compromissos') ? 'class="ativo"' : NULL; ?>><span id="span_compromisso" title="Compromissos para hoje ou em aberto"></span><?php echo anchor('compromissos', img(base_url() . 'misc/icones/month.png') . br() . 'Agenda'); ?></li>
								<li <?php echo ($this->uri->segment(1) == 'pendencias') ? 'class="ativo"' : NULL; ?>><span id="span_pendencia" title="Pendências em aberto ou aguardando"></span><?php echo anchor('pendencias', img(base_url() . 'misc/icones/kalarm.png') . br() . 'Pendências'); ?></li>
								<li <?php echo ($this->uri->segment(1) == 'noticias') ? 'class="ativo"' : NULL; ?>><?php echo anchor('noticias', img(base_url() . 'misc/icones/knewsticker.png') . br() . 'Notícias'); ?></li>
						<?php
							}
							else
							{
						?>
								<li <?php echo (!$this->uri->segment(1) || $this->uri->segment(1) == 'visao_geral') ? 'class="ativo"' : NULL; ?>><?php echo anchor('visao_geral', img(base_url() . 'misc/icones/folder_home.png') . br() . 'Visão Geral'); ?></li>
								<li <?php echo ($this->uri->segment(1) == 'prospects') ? 'class="ativo"' : NULL; ?>><?php echo anchor('prospects', img(base_url() . 'misc/icones/agt_family-off.png') . br() . 'Prospects'); ?></li>
								<li>
									<?php echo anchor('#', img(base_url() . 'misc/icones/kspread.png') . br() . 'Orçamentos', 'onclick="return false;"'); ?>
									<ul>
										<li > <?php echo anchor('pedidos/aguardando_analise/orcamento', 'Consultar Orçamentos'); ?></li>
										<li><?php echo anchor('criar_pedidos/informacoes_cliente/orcamento', 'Novo Orçamento'); ?></li>
									</ul>
								</li>
								<li><span id="span_pedido" title="Pedidos aguardando análise"></span>
									<?php echo anchor('#', img(base_url() . 'misc/icones/money.png') . br() . 'Pedidos', 'onclick="return false;"'); ?>
									<ul>
										<li><?php echo anchor('pedidos', 'Pedidos Analisados'); ?></li>
										<li><?php echo anchor('pedidos/aguardando_analise', 'Pedidos Aguardando Análise'); ?></li>
										<li><?php echo anchor('criar_pedidos', 'Novo Pedido'); ?></li>
									</ul>
								</li>
								<li <?php echo ($this->uri->segment(1) == 'clientes') ? 'class="ativo"' : NULL; ?>><?php echo anchor('clientes', img(base_url() . 'misc/icones/agt_family.png') . br() . 'Clientes'); ?></li>
								<li <?php echo ($this->uri->segment(1) == 'consultas') ? 'class="ativo"' : NULL; ?>>
									<?php echo anchor('#', img(base_url() . 'misc/icones/mail_find.png') . br() . 'Consultas', 'onclick="return false;"'); ?>
									<ul>
										<li><?php echo anchor('consultas/notas_fiscais', 'Consultar Notas Fiscais'); ?></li>
										<li><?php echo anchor('consultas/titulos_vencidos', 'Consultar Títulos Vencidos'); ?></li>
									</ul>
								</li>
								<?php if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros'))) { ?>
								
								<li>
									<?php echo anchor('#', img(base_url() . 'misc/icones/kexi.png') . br() . 'Relatórios', 'onclick="return false;"'); ?>
									<ul>

										<li><?php echo anchor('relatorios/relatorio_acessos', 'Acessos'); ?></li>
										<!-- <li><?php //echo anchor('relatorios/resultados_representantes', 'Resultados dos Representantes'); ?></li> -->
										<li><?php echo anchor('prospects/relatorio', 'Relatório de Prospects'); ?></li>
										
										<!--<li>
										<span class="ceta_menu"><img src="<?php echo base_url() . 'third_party/img/ceta_menu.png'; ?>"></span>
										<?php echo anchor('#', 'Meta x Realizado - Faturamento'); ?>
											<ul class="submenu">
												<li><?php echo anchor('metas/relatorio_faturamento/representante', 'Representantes'); ?></li>
												<li><?php echo anchor('metas/relatorio_faturamento/estado', 'Estados'); ?></li>
												<li><?php echo anchor('metas/relatorio_faturamento/cliente', 'Clientes'); ?></li>
												<li><?php echo anchor('metas/relatorio_faturamento/produto', 'Produtos'); ?></li>
											</ul>
										</li>
										<li>
											<span class="ceta_menu"><img src="<?php echo base_url() . 'third_party/img/ceta_menu.png'; ?>"></span>
											<?php echo anchor('#', 'Meta x Realizado - Novos Clientes'); ?>
											<ul class="submenu">
												<li><?php echo anchor('metas/relatorio_novos_clientes/representante', 'Representantes'); ?></li>
												<li><?php echo anchor('metas/relatorio_novos_clientes/estado', 'Estados'); ?></li>
											</ul>
										</li>
										<li>
											<span class="ceta_menu"><img src="<?php echo base_url() . 'third_party/img/ceta_menu.png'; ?>"></span>
											<?php echo anchor('#', 'Meta x Realizado - Prospecção'); ?>
											<ul class="submenu">
												<li><?php echo anchor('metas/relatorio_prospeccao/representante', 'Representantes'); ?></li>
												<li><?php echo anchor('metas/relatorio_prospeccao/estado', 'Estados'); ?></li>
											</ul>
										</li>-->
										
									</ul>
								</li>
								<?php } ?>
								<li <?php echo ($this->uri->segment(1) == 'compromissos') ? 'class="ativo"' : NULL; ?>><span id="span_compromisso" title="Compromissos para hoje ou em aberto"></span><?php echo anchor('compromissos', img(base_url() . 'misc/icones/month.png') . br() . 'Agenda'); ?></li>
								<li <?php echo ($this->uri->segment(1) == 'pendencias') ? 'class="ativo"' : NULL; ?>><span id="span_pendencia" title="Pendências em aberto ou aguardando"></span><?php echo anchor('pendencias', img(base_url() . 'misc/icones/kalarm.png') . br() . 'Pendências'); ?></li>
								<li <?php echo ($this->uri->segment(1) == 'noticias') ? 'class="ativo"' : NULL; ?>><?php echo anchor('noticias', img(base_url() . 'misc/icones/knewsticker.png') . br() . 'Notícias'); ?></li>
								
								<li <?php echo ($this->uri->segment(1) == 'formulario_visita') ? 'class="ativo"' : NULL; ?>>
										<?php echo anchor('#', img(base_url() . 'misc/icones/visita.png') . br() . 'Visitas', 'onclick="return false;"'); ?>
										<ul>
											<li><?php echo anchor('visitas', 'Visitas Realizadas'); ?></li>
											<li><?php echo anchor('formulario_visita', 'Formulário de Visita'); ?></li>
										</ul>
								</li>
								
								<li <?php echo ($this->uri->segment(1) == 'formulario_visita') ? 'class="ativo"' : NULL; ?>>
										<?php echo anchor('#', img(base_url() . 'misc/icones/rnc.png') . br() . 'RNC', 'onclick="return false;"'); ?>
										<ul>
											<li><?php echo anchor('rnc/listagem', 'Relatórios'); ?></li>
											<li><?php echo anchor('rnc', 'Novo Relatório'); ?></li>
										</ul>
								</li>
								
								<?php if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros'))) { ?>
								<li <?php echo ($this->uri->segment(1) == 'configuracoes' || $this->uri->segment(1) == 'aliquotas' || $this->uri->segment(1) == 'usuarios') ? 'class="ativo"' : NULL; ?>>
									<?php echo anchor('#', img(base_url() . 'misc/icones/gear.png') . br() . 'Configurações', 'onclick="return false;"'); ?>
									<ul>
										<li><?php echo anchor('configuracoes/dados_empresa', 'Dados da Empresa'); ?></li>
										<li><?php echo anchor('feiras', 'Eventos'); ?></li>
										<li><?php echo anchor('usuarios', 'Usuários'); ?></li>
										<li><?php echo anchor('descontos_classe', 'Descontos por Classe'); ?></li>
										<li><?php echo anchor('aliquotas', 'Alíquotas'); ?></li>
										<!-- <li><?php //echo anchor('filiais', 'Filiais'); ?></li> -->
										<!--<li><?php echo anchor('tabela_frete', 'Tabela de Frete'); ?></li>-->
										<!--<li>
											<span class="ceta_menu"><img src="<?php echo base_url() . 'third_party/img/ceta_menu.png'; ?>"></span>
											<?php echo anchor('#', 'Cadastrar Metas'); ?> 
											<ul class="submenu">
												<li><?php echo anchor('metas/meta_representante', 'Representante'); ?></li>
												<li><?php echo anchor('metas/meta_cliente', 'Cliente'); ?></li>
												<li><?php echo anchor('metas/meta_produto', 'Produto'); ?></li>
												<li><?php echo anchor('metas/meta_estado', 'Estados'); ?></li>
											</ul>
										</li>-->
										<!-- <li><?php //echo anchor('configuracoes/grupos_produtos_ativos', 'Grupos de Produtos Ativos'); ?></li> -->
									</ul>
								</li>
								<?php } ?>
								<li><?php //echo anchor(base_url() . 'offline_agrosandri.zip', img(base_url() . 'misc/icones/offline.png') . br() . 'Offline', array('target'=>'_blank')); ?></li>
								<li><?php // echo anchor('usuarios/sair', img(base_url() . 'misc/icones/gpgsm.png') . br() . 'Sair do Sistema'); ?></li>
							<?php
							}
							?>
					</ul>
				</div>


			<div style="clear: both;"></div>
			<div style="display:none;">
				<div id="alertas_lightbox">
					<div class="alertas">
						<div class="pedidos">
						<h3 style="color: #000; margin: 0; margin-bottom: 10px;">Pedidos</h3>
							<div class="conteudo"></div>
						</div>
						<div class="pendencias">
							<h3 style="color: #000; margin: 0; margin-bottom: 10px;">Pendências</h3>
							
							<div class="conteudo"></div>
						</div>
						
						<div class="compromissos">
							<h3 style="color: #000; margin: 0; margin-bottom: 10px;">Agenda</h3>
							<div class="conteudo"></div>
						</div>
					</div>
				</div>	
			</div>
			<?php endif; ?>
			
			<div id="conteudo" style="<?php echo $padding_conteudo ? 'padding: ' . $padding_conteudo . ';' : 'padding: 0 20px 20px 20px;'; ?>">
				<?php echo $conteudo; ?>
			</div>
			
			<p id="rodape"><?php echo img(base_url() . 'misc/logo_developweb.png'); ?> © 2011 Developweb. Todos os direitos reservados.<br /><br />Sistema licenciado para Developweb.</p>
		</div>
	</body>
</html>
