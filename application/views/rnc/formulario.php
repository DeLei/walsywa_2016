﻿<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'third_party/css/rnc_formulario.css'; ?>" media="screen">
<h2>Relatório de Não Conformidade</h2>

<?php
	if($dados_formulario && (count($dados_formulario['erros'] > 0))) {			
		foreach($dados_formulario['erros'] as $erro) {
			echo "<p class='erro'> {$erro} </p>";
		}
	}
?>

<?php echo form_open(current_url()); ?>
	
	
	<div class="row">
		<div class="col-sm-6">
			<label for="cliente">Cliente</label>
			<input type="text" id="cliente" name="codigo_loja_cliente" class="autocomplete pirulicoptero" value="<?=$dados_formulario["_codigo_loja_cliente"]; ?>" data-valor="<?=$dados_formulario["codigo_loja_cliente"]; ?>" data-url="<?=site_url('rnc/obter_clientes/' . ($codigo_usuario ? $codigo_usuario : 0)); ?>"  placeholder="Selecione o Cliente" required <?php echo ($dados_formulario['codigo'] ? 'disabled' : ''); ?>>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-3">
			<label for="filial">Filial</label>
			<select id="filial" name="filial">
				<option>Selecione...</option>
				<?php foreach($filialList as $codigo=> $filial): ?>
					<option value="<?=$codigo?>"  <?php echo  ($dados_formulario['filial'] == $codigo )?'selected':''?> ><?=$filial?></option>
				<?php endforeach;?>
			</select>			
		</div>
		
		<div class="col-sm-3">
			<label for="data_incidente">Data do Incidente</label>
			<input type="text" id="data_incidente" name="data_incidente" class="datepicker" value="<?php echo $dados_formulario['data_incidente'] ? protheus2data($dados_formulario['data_incidente']) : null; ?>" placeholder="Insira a data do incidente." readonly required <?php echo ($dados_formulario['codigo'] ? 'disabled' : ''); ?> />
		</div>
	</div>
	<br>
	
	<div class="row">
		<div class="col-sm-3">
			<label for="cliente">Pedido</label>
			<input type="text" id="pedido" name="pedido" class="autocomplete_pendente_cliente pirulicoptero pedido_nota" value="<?=$dados_formulario["_pedido"]; ?>" data-valor="<?=$dados_formulario["pedido"]; ?>" data-url="<?=site_url('rnc/obter_pedidos_cliente/'); ?>"  placeholder="Selecione o Pedido" required >
		</div>
		<div class="col-sm-3">
			<label for="cliente">Nota Fiscal</label>
			<input type="text" id="nota_fiscal" name="codigo_nota_fiscal" class="autocomplete_pendente_cliente pirulicoptero pedido_nota" value="<?= ($dados_formulario["codigo_nota_fiscal"])?$dados_formulario["codigo_nota_fiscal"].'/':''?><?=$dados_formulario["serie_nota_fiscal"]; ?>" data-valor="<?=$dados_formulario["codigo_nota_fiscal"].'|'.$dados_formulario["serie_nota_fiscal"]; ?>" data-url="<?=site_url('rnc/obter_notas_fiscais_clientes/' ); ?>"  placeholder="Selecione a Nota Fiscal" required >
		</div>
	</div>
	<br>
	
	<div class="row">
		<div class="col-sm-6">
			<label for="descricao">Descrição</label>
			<br>
			<textarea type="tex" id="descricao" name="descricao" rows="15" cols="85" placeholder="Digite uma Descrição para o incidente." required ><?php 
				echo $dados_formulario['descricao'] ? $dados_formulario['descricao'] : null; 
			?></textarea>
		</div>
	</div>
	
	<br>
	
	<?php echo form_submit('salvar_formulario', 'Salvar'); ?>
	<?php echo '<a href="'.site_url('rnc/listagem').'" onclick="return confirm(\'Deseja realmente Cancelar?\')" style="padding-left:20px;">Cancelar</a>'; ?>
		
<?php form_close(); ?>