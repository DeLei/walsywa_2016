﻿

<div id="espelho_rnc">
	<p style="margin-bottom: 10px;" class="nao_exibir_impressao">
			<?php
					
			echo anchor('rnc/index/' . $rnc->codigo, 'Editar ', 'class="botao_b"');
			
			?>	
	</p>
</div>


<div style="clear: left;"></div>
<h2>Dados Gerais</h2>


<h4 style="margin-top: 15px;">Informações Gerais</h4>
<table class="info2">
	<tr>
		<th>Status</th>
		<th>Filial</th>
		<th>Código</th>
		<th>Data do Incidente</th>	
		
	</tr>	
	
	<tr>
		<td><?= $statusList[(($rnc->status)?$rnc->status:'A')]?></td>		
		<td><?=$filiaisList[$rnc->filial] ?></td>
		<td><?=$rnc->codigo ?></td>
		<td><?=($rnc->data_incidente)?protheus2data($rnc->data_incidente): 'N/A'?></td>	
	</tr>

	
	
	<tr>
		<th>Cód / Cliente</th>
		<th>Pedido</th>
		<th>Nota Fiscal/ Série</th>
		<th>Emissão</th>
	</tr>
	<tr>
		<td><?=$rnc->codigo_cliente?>/<?=$rnc->nome_cliente ?> </td>
		<td><?=$rnc->pedido ?></td>
		<td><?=$rnc->codigo_nota_fiscal?>/<?=$rnc->serie_nota_fiscal ?> </td>
		<td><?=($rnc->emissao)?protheus2data($rnc->emissao):'N/A' ?></td>
	</tr>


	
</table>
<div style="clear: left;"></div>


<h4 style="margin-top: 15px;">Responsável</h4>
<table class="info2">
	
		
			
	<tr>
		<th>Responsável:</th>
		<th>Tipo:</th>
		<th>Tipo Responsável:</th>
	</tr>
	<tr>
		
		<td><?=($rnc->responsavel)?$rnc->responsavel:'N/A'?></td>
		<td><?=$tipoList[$rnc->tipo]?$tipoList[$rnc->tipo]:'N/A'?></td>
		<td><?= $responsavelTipoList[$rnc->tipo_responsavel]?$responsavelTipoList[$rnc->tipo_responsavel]:'N/A'?></td>
	</tr>		
</table>
<div style="clear: both;"></div>

<h4 style="margin-top: 15px;">Solução</h4>
<table class="info2 descricao"  >
	
		
			
	<tr>		
		<th>Descrição:</th>		
	</tr>
	<tr>		
		<td><?=$rnc->descricao?htmlentities($rnc->descricao):'N/A'?></td>
		
	</tr>

	<tr>
		<th>Simulação:</th>		
	</tr>
	<tr>
		<td><?=  $rnc->simulacao?htmlentities($rnc->simulacao): 'N/A'?></td>
	</tr>
	
	<tr>		
		<th>Ação Corretiva:</th>		
	</tr>
	<tr>		
		<td><?=$rnc->acao_corretiva?htmlentities($rnc->acao_corretiva):'N/A'?></td>
		
	</tr>
	<tr>		
		<th>Causa RNC:</th>		
	</tr>
	<tr>		
		<td><?=$rnc->causa_rnc?htmlentities($rnc->causa_rnc):'N/A'?></td>		
	</tr>
	<tr>		
		<th>Parecer Engenharia:</th>		
	</tr>
	<tr>		
		<td><?=$rnc->parecer_engenharia?htmlentities($rnc->parecer_engenharia):'N/A'?></td>
		
	</tr>
	
	
</table>
<div style="clear: both;"></div>


