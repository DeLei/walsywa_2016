﻿<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'third_party/css/rnc_formulario.css'; ?>" media="screen">


<h2>Relatório de Não Conformidade</h2>

<div class="topo_grid">
	<p><strong>Filtros</strong></p>
	<?php echo $filtragem;?>
</div>

<table class="novo_grid">

	<thead>
		<tr>
			<th>Status</th>
			<th>Código</th>
			<th>Pedido</th>
			<th>Nota Fiscal</th>
			<th style="<?= !in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) ? 'display: none;' : NULL; ?>">Representante</th>
			<th>Cliente</th>		
			<th>Ações</th>
		</tr>
	</thead>
	
	<tbody>
	
		<?php if (!$this->input->get('my_submit')) : ?>
			<tr><td colspan="11" style="color: #900; padding: 10px;"><strong>Realize um filtro.</strong></td></tr>
		<?php else : ?>
		
		<?php if (!$relatorios) : ?>	
			<tr><td colspan="11" style="color: #900; padding: 10px;"><strong>Nenhum Relatório Encontrado.</strong></td></tr>
		<?php else : ?>
		
			<?php foreach ($relatorios as $relatorio) : ?>
			
				<tr>
					<td style="text-align: center" ><?=img(base_url() . 'misc/imagens/rnc/'.(($relatorio['status'])?$relatorio['status']:'A').'.png') ?></td>
					<td style="text-align: center" ><?= $relatorio['codigo'];?></td>
					<td style="text-align: center" ><?= $relatorio['pedido'];?></td>
					<td style="text-align: center" ><?= $relatorio['codigo_nota_fiscal'];?></td>
					<td style="<?= !in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) ? 'display: none;' : NULL; ?>">
						<?= htmlentities($relatorio['nome_representante']);?>
						<?= htmlentities($relatorio['nome_representante']);?>
					</td>
					<td><?= htmlentities($relatorio['nome_cliente']);?></td>					
					<td style="font-size: 25px;">
						<a href="<?= site_url('rnc/ver_detalhes/' . $relatorio['codigo']); ?>"> Ver Detalhes</a>
						
					</td>
				</tr>
			<?php endforeach;?>
		<?php endif; ?>
	<?php endif; ?>	
	</tbody>
</table>

<div class="rodape_grid">
	<p><?= number_format($total['quantidade'], 0, ',', '.'); ?> registros encontrados.</p>
	
	<?php echo $paginacao; ?>
</div>


<table cellpadding="10" cellspacing="10" style="border: 1px solid #e5ce32; background-color: #fff">
	<tr>
		<td colspan="2"><b>Legenda</b></td>
	</tr>
	
	<tr>
		<td><?=img(base_url() . 'misc/imagens/rnc/A.png') ?> </td>
		<td> Aberto</td>
		<td>&nbsp;&nbsp;</td>	
		<td><?=img(base_url() . 'misc/imagens/rnc/T.png') ?> </td>
		<td> Atrasado</td>
		<td>&nbsp;&nbsp;</td>	
		<td><?= img(base_url() . 'misc/imagens/rnc/F.png') ?> </td>
		<td> Finalizado</td>		
	</tr>
	
</table>
