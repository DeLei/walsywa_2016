
<div class="caixa">
	<ul>
		<li><?=anchor('#', 'Voltar', 'onclick="history.go(-1); return false;"')?></li>
	</ul>
</div>


<?=heading('Agendamentos - ' . $nome, 2)?>

<br /><br />

<table cellpadding="10" cellspacing="10" style="border: 1px solid #e5ce32; background-color: #fbfca5">
	
	<tr>
		<td colspan="2"><b>Legenda</b></td>
	</tr>
	
	<tr>
		<td><?=img(base_url() . 'misc/imagens/compromissos/status_agendado.png')?></td>
		<td> Agendado</td>
		
		<td><?=img(base_url() . 'misc/imagens/compromissos/status_pendente.png')?></td>
		<td> Pendente</td>
	</tr>
	
	<tr>
		<td><?=img(base_url() . 'misc/imagens/compromissos/status_reagendado.png')?></td>
		<td> Reagendado</td>
	
		<td><?=img(base_url() . 'misc/imagens/compromissos/status_realizado.png')?></td>
		<td> Realizado</td>
	</tr>
	
</table>

<br /><br />


<table class="novo_grid">
	<thead>
		<tr>
			<th></th>
			<th>Início</th>
			<th>Tipo</th>
			<th>Destinatário</th>
			<th>Descrição</th>
			<th>Opções</th>
		</tr>
	</thead>
	
	<tbody>
	
	<?php
		foreach ($compromissos as $compromisso)
		{
	?>
			<tr>
				<td style="width: 1%;" ><?=img(base_url() . 'misc/imagens/compromissos/status_' . $compromisso->status . '.png')?></td>

				<td class="center"><?=date('d/m/Y H:i', $compromisso->inicio_timestamp)?></td>
				<td><?=$compromisso->tipo?></td>
				<td><?=($compromisso->detalhes ? $this->db->from('usuarios')->where('id', $compromisso->id_usuario_cad)->get()->row()->nome . ' - (<b>' . $compromisso->nome . '</b>)' : $compromisso->nome)?></td>
				<td><?=$compromisso->descricao . '</td><td>' . anchor('compromissos/ver_detalhes/' . $compromisso->id, '+ Opções')?></td>
			</tr>
	<?php
		}
	?>
	</tbody>
</table>

<div class="rodape_grid">
	<p><?= number_format($total, 0, ',', '.'); ?> registros encontrados.</p>
	
	<?php echo $paginacao; ?>
</div>