<h2>Agenda</h2>

<p><?=anchor('compromissos/criar', 'Cadastrar Compromisso')?></p>

<div class="topo_grid">
		<p><strong>Filtros</strong></p>
		<?php echo $filtragem; ?>
</div>
<table cellspacing="0" class="novo_grid">
	<thead>
		<tr>
			<th>Status</th>
			<th>Início</th>
			<th>Tipo</th>
			<th>Para</th>
			<th>Descrição</th>
			<th>Opções</th>
		</tr>
	</thead>
	<tbody>
		<?php if (!$this->input->get('my_submit')) : ?>
				<tr><td colspan="10" style="color: #900; padding: 10px;"><strong>Realize um filtro.</strong></td></tr>
		<?php else : ?>
			<?php if ($compromissos) : ?>		
				<?php foreach ($compromissos as $compromisso): ?>
								
				<tr>
					<td class='center'><?= img(base_url() . 'misc/imagens/compromissos/status_'.$compromisso->status.'.png') ?></td>
					<td class='center'><?= date('d/m/Y', $compromisso->inicio_timestamp)?></td>
					<td><?= $compromisso->tipo ?></td>
					<td><?= $compromisso->usuario_nome ?></td>
					<td><?= $compromisso->descricao ?></td>
					<td class='center'><?= anchor('compromissos/ver_detalhes/' . $compromisso->id, '+ Opções')?></td>
				</tr>
				<?php endforeach; ?>
			<?php else:?>
			<tr>
				<td colspan="6">Não foram listadas compromissos.</td>
			</tr>
			<?php endif; ?>
		<?php endif; ?>
	</tbody>
</table>

<div class="rodape_grid">
	<p><?= number_format($total, 0, ',', '.'); ?> registros encontrados.</p>
	<?= $paginacao; ?>
</div>
<table cellpadding="10" cellspacing="10" style="border: 1px solid #e5ce32; background-color: #fff">
					
					<tr>
						<td colspan="2"><b>Legenda</b></td>
					</tr>
					
					<tr>
						<td><?= img(base_url() . 'misc/imagens/compromissos/status_agendado.png') ?> </td>
						<td> Agendado</td>
						
						<td><?=img(base_url() . 'misc/imagens/compromissos/status_pendente.png') ?> </td>
						<td> Pendente</td>
					
						<td><?=img(base_url() . 'misc/imagens/compromissos/status_reagendado.png') ?> </td>
						<td> Reagendado</td>
					
						<td><?= img(base_url() . 'misc/imagens/compromissos/status_realizado.png')?> </td>
						<td> Realizado</td>
					</tr>
					
				</table>
