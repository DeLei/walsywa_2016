<div class="caixa">
	<ul>
		<li><?= anchor('prospects', 'Voltar', 'onclick="history.go(-1); return false;"'); ?></li>
	</ul>
</div>

<h2>Acessos do Usuário <?= $usuario->nome_real ? $usuario->nome_real : $usuario->nome; ?></h2>

<div class="topo_grid">
	<p><strong>Filtros</strong></p>
	
	<?php echo $filtragem; ?>
</div>

<table class="novo_grid">
	<thead>
		<tr>
			<th>N°</th>
			<th>Data</th>
			<th>IP Acesso</th>
			<th>Último LogIn</th>
			<th>Último LogOut</th>
			<th>Opções</th>
		</tr>
	</thead>
	
	<tbody>
		<?php if (!$acessos) : ?>
			
			<tr><td colspan="3" style="color: #900; padding: 10px;"><strong>Nenhum acesso registrado.</strong></td></tr>
			
		<?php else : ?>
			
			<?php foreach ($acessos as $acesso) : ?>
				
				<tr>
					<td><?= $acesso->numero; ?>°</td>
					<td style="text-align: center;"><?= date('d/m/Y H:i:s', $acesso->timestamp); ?></td>
					<td><?=$acesso->ip_externo?></td>
					<td><?=$acesso->data_hora_login?></td>
					<td><?=$acesso->data_hora_logout?></td>
					<td style="text-align: center;"><a class="ver_local" data-usuario="<?= $usuario->nome_real ? $usuario->nome_real : $usuario->nome; ?>" data-numero="<?= $acesso->numero; ?>" data-lat="<?= $acesso->lat; ?>" data-lon="<?= $acesso->lon; ?>" href="#">Ver Local</a></td>
				</tr>
				
			<?php endforeach; ?>
			
		<?php endif; ?>
	</tbody>
</table>

<div class="rodape_grid">
	<p><?= number_format($total, 0, ',', '.'); ?> registros encontrados.</p>
	
	<?php echo $paginacao; ?>
</div>

<script>
	$(document).ready(function() {
		$(".ver_local").click(function(evento) {
			evento.preventDefault();
			
			var _this = this;
			
			if (!$(this).data('lat') || !$(this).data('lon'))
			{
				alert("Não foi possível rastrear o local do " + $(this).data('numero') + "° acesso.");
			}
			else
			{
				new google.maps.Geocoder().geocode({ "latLng": new google.maps.LatLng($(this).data('lat'), $(this).data('lon')) }, function(dados) {
					var html = '<div id="mapa"><h3 style="margin: 0;"></h3><div style="border: 1px solid #ddd; width: 700px; height: 400px; margin-top: 10px;"></div></div>';
					
					$.fn.colorbox({ html: html, onComplete: function() {
						$("#mapa").find("h3").text($(_this).data('numero') + '° Acesso do Usuário ' + $(_this).data('usuario')).end().find("div").goMap({ markers: [{ latitude: $(_this).data('lat'), longitude: $(_this).data('lon'), html: { content: dados[0].formatted_address, popup: true } }], zoom: 15, maptype: "ROADMAP" });
					}, scrolling: false });
				});
			}
			
			return false;
		});
	});
</script>