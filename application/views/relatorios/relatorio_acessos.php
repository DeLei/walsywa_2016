


<h2>Acessos</h2>

<div class="topo_grid">
	<p><strong>Filtros</strong></p>
	
	<?php echo $filtragem; ?>
</div>

<table class="novo_grid">
	<thead>
		<tr>
			<th>Acessos Totais</th>
			<th>Usuário</th>
			<th>IP Acesso</th>
			<th>Primeiro Acesso</th>
			<th>Último Acesso</th>
			<th>Último LogIn</th>
			<th>Último LogOut</th>
			<th class="nao_exibir_impressao">Opções</th>
		</tr>
	</thead>
	<tbody>
	<?php
		
		if($acessos)
		{
		
		foreach($acessos as $acesso)
		{
	?>
	
		<tr>
			<td class="center"><?=$this->db->select('COUNT(id_usuario) as total')->from('acessos_usuarios')->where(array('id_usuario' => $acesso->id_usuario))->get()->row()->total?></td>
			<td><?=$acesso->nome?></td>
			<td><?=$acesso->ip_externo?></td>
			<td class="center"><?=date('d/m/y H:i:s', $this->db->from('acessos_usuarios')->where(array('id_usuario' => $acesso->id_usuario))->order_by("timestamp", "asc")->limit(1)->get()->row()->timestamp)?></td>
			<td class="center"><?=date('d/m/y H:i:s', $this->db->from('acessos_usuarios')->where(array('id_usuario' => $acesso->id_usuario))->order_by("timestamp", "desc")->limit(1)->get()->row()->timestamp)?></td>
			<td><?=$acesso->data_hora_login?></td>
			<td><?=$acesso->data_hora_logout?></td>
			<td class="nao_exibir_impressao center"><?=anchor('relatorios/ver_acessos/' . $acesso->id_usuario, 'Ver Detalhes')?></td>
		</tr>
	<?php
		}
	}
	?>
	</tbody>
</table>

<div class="rodape_grid">
	<p><?= number_format($total, 0, ',', '.'); ?> registros encontrados.</p>
	
	<?php echo $paginacao; ?>
</div>
