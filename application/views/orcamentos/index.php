<script type="text/javascript">
				$(document).ready(function(){
				verificar_tipo_consulta();
					
					$("input[name=tipo_consulta]").click(function() {
						verificar_tipo_consulta();
					});
				
				
				function verificar_tipo_consulta()
				{
					if ($("input[name=tipo_consulta]:checked").val() == "todos")
					{
						$("#cliente").hide();
						$("#prospect").hide();
					}
					else if ($("input[name=tipo_consulta]:checked").val() == "cliente")
					{
						$("#cliente").show();
						$("#prospect").hide();
					}
					else
					{
						$("#cliente").hide();
						$("#prospect").show();
					}
				}});
</script>


<?if($usuario['grupo']):?>

				<div class="caixa_b">
	
					<p>Usuário:</p>
					<?=$usuario['representante'];?>
			    <?php
				echo '	<script type="text/javascript">

							$(document).ready(function(){
									
									$("#usuarios").change(function(){
											var valor = $(this).val();
											window.location = "' . site_url('orcamentos/editar_codigo_usuario_consultar_orcamentos') . '/" + valor;
									});
									
							});

						</script>';
				?>
				</div>
			
<? endif;?>		
		


<h2>Orçamentos</h2>

<h3>Cadastrar Novo Orçamento</h3>

<p style="margin-bottom: 10px;"><button type="button"><?=anchor('criar_pedidos/informacoes_cliente/orcamento', 'Cadastrar Novo Orçamento')?></button></p>

    <?php if ($erro):?>
<p class="erro"><?=$erro?></p>;
    <?php endif; ?> 
     
		<?= form_open('orcamentos/index');?>
<p>Tipo de consulta:<br /> 
    <?=form_label(form_radio('tipo_consulta', 'todos', !$_POST['tipo_consulta'] || $_POST['tipo_consulta'] == 'todos' ? TRUE : FALSE) . ' Todos').'&nbsp;&nbsp;&nbsp;'.
    form_label(form_radio('tipo_consulta', 'cliente', $_POST['tipo_consulta'] == 'cliente' ? TRUE : FALSE) . ' Cliente').'&nbsp;&nbsp;&nbsp;'.
    form_label(form_radio('tipo_consulta', 'prospect', $_POST['tipo_consulta'] == 'prospect' ? TRUE : FALSE) . ' Prospect');?>
</p>

<p id="cliente">Cliente:<br><input type="text" style="width: 250px" name="codigo_loja_cliente" class="autocomplete" value="<?= $this->input->post("_codigo_loja_cliente") ?>" alt="<?=$this->input->post("codigo_loja_cliente")?>" data-url="<?= site_url('orcamentos/obter_clientes' . ($this->session->userdata('codigo_usuario_consultar_orcamentos') ? ('/' . $this->session->userdata('codigo_usuario_consultar_orcamentos')) : NULL))?>"></p>

<p id="prospect">Prospect:<br><input type="text" style="width: 250px" name="id_prospect" class="autocomplete" value="<?=$this->input->post("_id_prospect") . '" alt="' . $this->input->post("id_prospect") . '" data-url="' . site_url('orcamentos/obter_prospects' . ($this->session->userdata('codigo_usuario_consultar_orcamentos') ? ('/' . $this->session->userdata('codigo_usuario_consultar_orcamentos')) : NULL)) ?>">
</p>
		
<p><?=form_label('Data:' . br() . form_input('data_inicial', $this->input->post('data_inicial'), 'class="datepicker data_inicial" size="6"')) . ' à ' . form_input('data_final', $this->input->post('data_final'), 'class="datepicker" size="6"'); ?>
</p>

<p><?= form_submit('consultar', 'Consultar'); ?></p> <br />

<?= form_close();?>

<!--Resposta da consulta-->
<?php if (!$orcamentos) : ?>
	
	<div class="ajuda"><p>Não existem Orçamentos cadastrados.</p></div>
	 
<?php else : ?>
	 
	<div class="ajuda"><p>Clicando em "+ Opções" você pode <strong>imprimir, gerar PDF ou enviar por e-mail o orçamento</strong>.</p></div>
	<br />
	<p><h3>Resultado da Consulta</h3></p>
  
  <p><strong>Valor total:</strong> R$ <?=number_format($usuario['total'], 5, ',', '.');?> <strong>Quantidade:</strong> <?=number_format(count($orcamentos), 0, '', '.')?></p>
	<div class="topo_grid">
		<p><strong>Filtros</strong></p>
		
		<?php echo $filtragem; ?>
	</div>
	
	<table class="novo_grid">
		<thead>
			<tr>
				<th>Cliente/Prospect</th>
				<th>Emissão</th>
				<th>Quant. Vend.</th>
				<th>Total (R$)</th>
				<th>Opções</th>
			</tr>
		</thead>
		
		<tbody>
			<?php foreach ($orcamentos as $orcamento) : ?>
				
				<tr>
					<td><?php echo $orcamento['A1_NOME']; ?></td>
					<td class='center'><?php echo date('d/m/Y', $orcamento['C5_EMISSAO']); ?></td>
					<td class='right'><?php echo number_format($orcamento['C6_QTDVEN'], 0, NULL, '.'); ?></td>
					<td class='right'><?php echo number_format($orcamento['total'], 5, ',', '.'); ?></td>
					<td class='center'><?php echo anchor('pedidos/ver_detalhes/' . ($orcamento['id'] ? '0/' . $orcamento['id'] : $orcamento['codigo']), '+ Opções'); ?></td>
				</tr>
				
			<?php endforeach; ?>
		</tbody>
	</table>
	 
	<div class="rodape_grid">
		<?php echo $paginacao; ?>
	</div>
	
<?php endif; ?>