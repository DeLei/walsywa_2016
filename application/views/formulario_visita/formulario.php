﻿<style>
	label {
		width: 100%;
		font-weight: bold;
	}
	
	input[type=text], input[type=email]{
		width: 98%;
	}
	
	select{
		width: 100%;
		padding: 2px;
		height: 24px;
	}
	
	.column_5 {
		float: left;
		width: 4% !important;
		margin-right: 3px;
	}
	
	.column_10 {
		float: left;
		width: 9% !important;
		margin-right: 3px;
	}
	
	.column_25 {
		float: left;
		width: 23% !important;
		margin-right: 0px;
	}
	
	.column_33 {
		float: left;
		width: 32% !important;
		margin-right: 17px;
	}
	
	.column_40 {
		float: left;
		width: 39% !important;
		margin-right: 3px;
	}
	
	.column_45 {
		float: left;
		width: 44% !important;
		margin-right: 3px;
	}
	
	.column_50 {
		float: left;
		width: 49% !important;
		margin-right: 3px;
	}
	
	.column_75 {
		float: left;
		width: 72% !important;
		margin-right: 3px;
	}
	
	.column_100 {
		float: left;
		width: 100% !important;
	}
	
	.marginTop_10{
		margin-top: 10px;
	}
	
	.column_100 h3{
		margin-top: 0px;
		width: 100%;
		border-bottom: 1px solid #666;
	}
	
	.btn_full{
		margin-left: 3px;
		width: 100%;
		height: 100%;
		margin-top: 15px;
		padding: 3px;
	}
	
	.error{
		border: 1px solid red !important;
	}
	.success{
		border: 1px solid green !important;
	}
	
	.email_contato, nome_contato{
		-ms-word-break: break-all;

		 /* Be VERY careful with this, breaks normal words wh_erever */
		 word-break: break-all;

		 /* Non standard for webkit */
		 word-break: break-word;

		-webkit-hyphens: auto;
		-moz-hyphens: auto;
		hyphens: auto;
	}
	
	.novo_grid tbody tr:nth-child(odd){
		background: rgb(226, 228, 255);
	}
	
	input[type=email], select, textarea {
		margin-top: 2px;
		border: solid 1px #666;
		padding: 3px;
	}
</style>

<h2>Formulário de Visita</h2>

<?php

	echo form_open('formulario_visita/salvar_formulario', array('name' => 'form_visita'));     
	
	if($formulario_visita && (count($formulario_visita['erros'] > 0))) {
				
		foreach($formulario_visita['erros'] as $erro) {
			echo "<p class='erro'> {$erro} </p>";
		}
		
		echo form_textarea(array('name' => 'tbl_concorrentes', 'style' => 'display:none;'), $this->input->post('tbl_concorrentes')),
			 form_textarea(array('name' => 'tbl_contatos', 'style' => 'display:none;'), $this->input->post('tbl_contatos')),
			 form_textarea(array('name' => 'tbl_interesse', 'style' => 'display:none;'), $this->input->post('tbl_interesse')),
			 form_textarea(array('name' => 'tbl_produtos', 'style' => 'display:none;'), $this->input->post('tbl_produtos'));
		
	} else if($salvo === 's'){
		echo '<p class="sucesso"> Formulário de Visita foi salvo com sucesso!</p>';
	}
	
	$disabled = $codigo_formulario ? true : false;
		 
	echo $disabled ? form_hidden('codigo_formulario', $codigo_formulario) : '';
?>
	<p class="erro" id="rsvErrors" style="display: none"></p>
	<div class="column_33 marginTop_10">
		<div class="column_50">
			<?php echo form_label('Data de Visitação', 'data_visitacao');?>
			<?php echo form_input(array( 
						'name'        => 'data_visitacao',
						'id'          => 'data_visitacao',
						//'value'       => Date('d/m/Y'),
						'class'		  => 'datepicker_tres_dias',
						'style'       => 'width: 96% !important;',
						'disabled'	  => $disabled
				 ), ($this->input->post('data_visitacao')) ? $this->input->post('data_visitacao') : Date('d/m/Y') );
			?>
		</div>
   
        <input type="hidden" name="loja_cliente" value="">
       
		<div class="column_50">
			<?php echo form_label('Data de Preenchimento', 'data_preenchimento');?>
			<?php echo form_input(array( 
						'name'        => 'data_preenchimento',
						'id'          => 'data_preenchimento',
						//'value'       => Date('d/m/Y'),
						'disabled'    => true
				 ), ($this->input->post('data_preenchimento')) ? $this->input->post('data_preenchimento') : Date('d/m/Y') );
			?>
		</div>
		<div class="column_100 marginTop_10">
			<?php echo form_label('Setor', 'setor', array(
				'class' => 'column_100', 
				'style' => 'margin-bottom: 5px;'
			));?>
			<?php echo form_radio(array( 'name' => 'setor', 'value' => 'E', 'disabled' => $disabled, 'checked' => $this->input->post('setor') === 'E' ? true : false, 'class' => 'column_5') ), '<span class="column_45">Engenharia</span>',
					   form_radio(array( 'name' => 'setor', 'value' => 'R', 'disabled' => $disabled, 'checked' => $this->input->post('setor') === 'R' ? true : false, 'class' => 'column_5')), '<span class="column_45">Revenda</span>';?>
		</div>
		<div class="column_100 marginTop_10">
			<?php echo form_label('Cliente Novo?', 'cliente_novo', array('class' => 'column_100', 'style' => 'margin-bottom: 5px;')) ;?>
			<?php echo form_radio(array( 'name' => 'cliente_novo', 'value' => 'S', 'disabled' => $disabled, 'checked' => $this->input->post('cliente_novo') === 'S' ? true : false, 'class' => 'column_5')), '<span class="column_45">Sim</span>',
					   form_radio(array( 'name' => 'cliente_novo', 'value' => 'N', 'disabled' => $disabled, 'checked' => $this->input->post('cliente_novo') === 'N' ? true : false, 'class' => 'column_5')), '<span class="column_45">Não</span>';?>
		</div>
		
		<?php 
		
		//echo "<pre>";
		//echo var_dump($representante);
		//echo "</pre>";
		
		//echo $representante['categoria'];
		//echo ' - tipo';
			if(in_array($representante['categoria'], array(4, 5, 6))): ?>
			<div class="column_100 marginTop_10">
				<?php echo form_label('Visita de Coordenação?', 'visita_coordenacao', array(
					'class' => 'column_100', 
					'style' => 'margin-bottom: 5px;'
				));?>
				<?php echo form_radio(array( 'name' => 'visita_coordenacao', 'value' => 'S', 'disabled' => $disabled, 'checked' => $this->input->post('visita_coordenacao') === 'S' ? true : false, 'class' => 'column_5') ), '<span class="column_45">Sim</span>',
						   form_radio(array( 'name' => 'visita_coordenacao', 'value' => 'N', 'disabled' => $disabled, 'checked' => $this->input->post('visita_coordenacao') === 'N' || !$this->input->post('visita_coordenacao') ? true : false, 'class' => 'column_5')), '<span class="column_45">Não</span>';?>
			</div>
		<?php endif; ?>
		
		<div class="column_100 marginTop_10">
		
			<div class="column_100">
				<?php echo form_label('Cliente', 'cliente', array('class' => 'column_75')) ;?>
				<?php echo form_label('Código', 'codigo_cliente', array('class' => 'column_25', 'style' => 'margin-left: 7px;')) ;?>
			</div>
			
			<div class="column_100">
			
				<input type="text" id="input_cliente" name="input_cliente" class="autocomplete  column_75"<?php echo $codigo_formulario ? 'disabled="disabled"' : ''; ?>
					   value="<?=$this->input->post("_input_cliente"); ?>" alt="<?=$this->input->post("_input_cliente"); ?>" style="display: <?=$this->input->post('cliente_novo') === 'N' ? 'block' : 'none'?>;"
					   data-url="<?=site_url("formulario_visita/obter_clientes_formulario_visita"); ?>">
				<?php echo 
					 form_input(array(
							'name'        => 'input_cliente_novo',		 
							'id'          => 'input_cliente_novo',
							'class'       => 'column_75',
							'disabled'	  => $disabled,
							'style'		  => $this->input->post('cliente_novo') === 'S' ? 'display: block;' : 'display: none;'
					 ), $this->input->post('input_cliente_novo'));
				?>
				
				<?php echo form_input(array(
						'name'		=>	'codigo_cliente',
						'disabled'  => true,
						'class'     =>  'column_25'
					 ), $this->input->post('codigo_cliente')) ;
				?>
				<span id="input_cliente_notificacao"></span>
				
			</div>
		</div>
		
		<div class="column_100 marginTop_10">
			<div class="column_100">
				<?php echo form_label('Classificação', 'classificacao_cliente', array('class' => 'column_75')) ;?>
				<?php echo form_label('Telefone', 'telefone_cliente', array('class' => 'column_25', 'style' => 'margin-left: 7px;')) ;?>
			</div>
			<div class="column_100">
				<?php echo form_input(array(
						'name'			=>	'classificacao_cliente',
						'disabled'		=>	true,
						'class' 		=>  'column_75'
					 ), $this->input->post('classificacao_cliente') === '' ? 'Prospect' : $this->input->post('classificacao_cliente')) ;
				?>
				<?php echo form_input(array(
						'name'		  => 'telefone_cliente',
						'alt'         => 'phone_9digitos',
						'class' 	  => 'column_25',
						'disabled'    => $disabled
					 ), $this->input->post('telefone_cliente')) ;
				?>
			</div>
		</div>
		
		<div class="column_100 marginTop_10">
			<?php echo form_label('Tipo de Cliente', 'tipo_cliente');?>        		
			<?php echo '<select name="tipo_cliente"'; echo $codigo_formulario ? 'disabled="disabled"' : ''; echo '>';
					
					echo '<option value="">Selecione...</option>';
						
					foreach($tipos_cliente as $tipo) {
						echo '<option value="' . $tipo['tipo'] .'" ';  if($this->input->post('tipo_cliente') === $tipo['tipo']) { echo 'selected'; }else{ echo ''; } echo '>' . $tipo['tipo'] . ' - ' . htmlentities($tipo['descricao']) . '</option>';
					}
					
					echo '</select>' ;
			?>
		</div>
		
		<div class="column_100 marginTop_10">
			<?php echo form_label('Cidade/Região', 'cidade_regiao_cliente') ;?>
			<?php echo form_input(array('name' => 'cidade_regiao_cliente', 'disabled' => $disabled), $this->input->post('cidade_regiao_cliente')) ;?>
		</div>
		<div class="column_50 marginTop_10">
			<?php echo form_label('Estado', 'estado_cliente') ;?>
			<?php echo form_dropdown('estado_cliente', array(
					 ''	 =>'Selecione...',
					 'AC'=>'Acre',
					 'AL'=>'Alagoas',
					 'AP'=>'Amapá',
					 'AM'=>'Amazonas',
					 'BA'=>'Bahia',
					 'CE'=>'Ceará',
					 'DF'=>'Distrito Federal',
					 'ES'=>'Espírito Santo',
					 'GO'=>'Goiás',
					 'MA'=>'Maranhão', 
					 'MT'=>'Mato Grosso',
					 'MS'=>'Mato Grosso do Sul',
					 'MG'=>'Minas Gerais',
					 'PA'=>'Pará',
					 'PB'=>'Paraíba',
					 'PR'=>'Paraná',
					 'PE'=>'Pernambuco',
					 'PI'=>'Piauí',
					 'RJ'=>'Rio de Janeiro',
					 'RN'=>'Rio Grande do Norte',
					 'RS'=>'Rio Grande do Sul' ,
					 'RO'=>'Rondônia',
					 'RR'=>'Roraima',
					 'SC'=>'Santa Catarina',
					 'SP'=>'São Paulo',
					 'SE'=>'Sergipe',
					 'TO'=>'Tocantins' 
				 ), $this->input->post('estado_cliente'), $codigo_formulario ? 'disabled="disabled"' : '') ;
			?>
		</div>
		<div class="column_50 marginTop_10">
			<?php echo form_label('CEP', 'cep_cliente') ;?>
			<?php echo form_input(array(
					'name'		=>	'cep_cliente',
					'id'		=>	'cep_cliente',
					'disabled'  => $disabled,
					'alt'		=>	'cep'
				 ), $this->input->post('cep_cliente')) ;
			?>
		</div>
		<div id="frm_contatos" class="column_100 marginTop_10">
		
			<h3>Contatos</h3>
			
			<input type="hidden" name="contador_contatos"/>
			
			<div class="column_100 marginTop_10">
				<?php echo form_label('Nome', 'nome_contato') ;?>
				<?php echo form_input(array('name' => 'nome_contato', 'maxlength' => '40', 'disabled' => $disabled)) ;?>
			</div>
			
			<div class="column_50 marginTop_10">
				<?php echo form_label('Tipo de Contato', 'tipo_contato') ;?>
				<?php echo '<select name="tipo_contato"'; echo $codigo_formulario ? 'disabled="disabled"' : ''; echo '>';
					
					echo '<option value="">Selecione...</option>';
						
					foreach($tipos_contato as $tipo) {
						echo '<option value="' . $tipo['tipo'] .'" ';  if($this->input->post('tipo_contato') === $tipo['tipo']) { echo 'selected'; }else{ echo ''; } echo '>' . $tipo['tipo'] . ' - ' . htmlentities($tipo['descricao']) . '</option>';
					}
					
					echo '</select>' ;
				?>
			</div>
			
			<div class="column_50 marginTop_10">
				<?php echo form_label('Telefone', 'telefone_contato') ;?>
				<?php echo form_input(array(
						'name'	=> 'telefone_contato',
						'alt'   => 'phone_9digitos',
						'disabled' => $disabled
					 )) ;
				?>
			</div>
			
			<div class="column_100 marginTop_10">
			
				<div class="column_75">
					<?php echo form_label('Email', 'email_contato') ;?>
					<?php echo form_input(array('name' => 'email_contato', 'maxlength' => '60', 'disabled' => $disabled)) ;?>
				</div>
				
				<div class="column_25" style="width: 26%! important;">
					<?php echo  form_button(array(
							'name' => 'adicionar_contato',
							'value' => 'adicionar_contato',
							'content' => 'Adicionar', 
							'disabled' => $disabled,
							'class' => 'btn_full btn_add_contato'
						));
					?>
				</div>
				
			</div>
			
			<div class="column_100 marginTop_10">
				<table id="tbl_contatos" class="novo_grid">
					<thead>
						<tr>
							<th width="1%">#</th>
							<th width="auto%">Nome</th>
							<th width="6%">Tipo</th>
							<th width="auto">Telefone</th>
							<th width="auto">Email</th>
							<?php if (!$codigo_formulario){?><th width="10%">Ações</th><?php } ?>
						</tr>
					</thead>
					<tbody style="max-height: 400px; "></tbody>
				</table>
			</div>
		</div>
	</div>
	
	<div class="column_33 marginTop_10">			
		<div class="column_100 ">
			<?php echo form_label('Gerou Pedido/Orçamento', 'gerou_pedido_orcamento', array('class' => 'column_100', 'style' => 'margin-bottom: 5px;')) ;?>
			<?php echo form_radio(array( 'name' => 'gerou_pedido_orcamento', 'value' => 'S', 'disabled' => $disabled, 'checked' => $this->input->post('gerou_pedido_orcamento') === 'S' ? true : false, 'class' => 'column_5')), '<span class="column_45">Sim</span>',
					   form_radio(array( 'name' => 'gerou_pedido_orcamento', 'value' => 'N', 'disabled' => $disabled, 'checked' => $this->input->post('gerou_pedido_orcamento') === 'N' ? true : false, 'class' => 'column_5')), '<span class="column_45">Não</span>';?>
		</div>
		
		<div id="frm_concorrentes" class="column_100 marginTop_10">
		
			<h3>Concorrentes/Revenda</h3>
			
			<div class="column_100 marginTop_10">
				<?php echo form_label('Nome', 'nome_concorrente_revenda') ;?>
				<input type="text" id="nome_concorrente_revenda" name="nome_concorrente_revenda" class="autocomplete" <?php echo $codigo_formulario ? 'disabled="disabled"' : '';?>
					   value="<?=$this->input->post("_nome_concorrente_revenda"); ?>" alt="<?=$this->input->post("nome_concorrente_revenda"); ?>"
					   data-url="<?=site_url("formulario_visita/obter_concorrentes"); ?>">
			</div>
			
			<div class="column_100 marginTop_10">
				<?php echo form_label('Descrição', 'descricao_concorrente_revenda') ;?>
				<input type="text" id="descricao_concorrente_revenda" name="descricao_concorrente_revenda" class="autocomplete" <?php echo $codigo_formulario ? 'disabled="disabled"' : '';?>
					   value="<?=$this->input->post("_descricao_concorrente_revenda"); ?>" alt="<?=$this->input->post("descricao_concorrente_revenda"); ?>"
					   data-url="<?=site_url("formulario_visita/obter_produtos_formulario_visita"); ?>">
			</div>
			
			<div class="column_100 marginTop_10">
			
				<div class="column_75">
					<?php echo form_label('Preço', 'preco_concorrente_revenda') ;?>
					<?php echo form_input(array('name' => 'preco_concorrente_revenda', 'class' => 'currency', 'disabled' => $disabled)) ;?>
				</div>
				
				<div class="column_25" style="width: 26%! important;">
					<?php echo  form_button(array(
							'name' => 'adicionar_concorrente',
							'value' => 'adicionar_concorrente',
							'content' => 'Adicionar', 
							'disabled' => $disabled,
							'class' => 'btn_full btn_add_concorrente'
						));
					?>
				</div>
			</div>
			
			<input type="hidden" name="contador_concorrentes"/>
			<div class="column_100 marginTop_10">
				<table id="tbl_concorrentes" class="novo_grid">
					<thead>
						<tr>
							<th>#</th>
							<th>Nome</th>
							<th>Descrição</th>
							<th>Preço</th>
							<?php if (!$codigo_formulario){?><th>Ações</th><?php } ?>
						</tr>
					</thead>
					<tbody style="max-height: 400px; "></tbody>
				</table>
			</div>
		</div>
		
		<div id="frm_produtos" class="column_100 marginTop_10">
		
			<h3>Produtos Apresentados</h3>

			<div class="column_100 marginTop_10">
				<?php echo form_label('Descrição', 'descricao_produto_apresentado') ;?>	
				<input type="text" id="descricao_produto_apresentado" name="descricao_produto_apresentado" class="autocomplete" <?php echo $codigo_formulario ? 'disabled="disabled"' : '';?>
					   value="<?=$this->input->post("_descricao_produto_apresentado"); ?>" alt="<?=$this->input->post("descricao_produto_apresentado"); ?>"
					   data-url="<?=site_url("formulario_visita/obter_produtos_formulario_visita"); ?>">
			</div>
			
			<div class="column_100 marginTop_10">
			
				<div class="column_75">
					<?php echo form_label('Preço Objetivo', 'preco_objetivo_produto_apresentado') ;?>
						<?php echo form_input(array(
							'id'		=>	'preco_objetivo_produto_apresentado',
							'name'		=>	'preco_objetivo_produto_apresentado',
							'class'		=>	'currency',
							'disabled' => $disabled
						)) 
					;?>
				</div>
				
				<div class="column_25" style="width: 26%! important;">
					<?php echo  form_button(array(
							'name'		=> 'adicionar_produto_apresentado',
							'value' 	=> 'adicionar_produto_apresentado',
							'content' 	=> 'Adicionar', 
							'disabled' => $disabled,
							'class' 	=> 'btn_full btn_add_produto'
						));
					?>
				</div>
				
			</div>
			
			<div class="column_100 marginTop_10">
			
				<table id="tbl_produtos" class="novo_grid">
					<thead>
						<tr>
							<th>#</th>
							<th>Descrição</th>
							<th>Preço</th>
							<?php if (!$codigo_formulario){?><th>Ações</th><?php } ?>
						</tr>
					</thead>
					<tbody style="max-height: 400px; "></tbody>
				</table>
				
			</div>
		</div>
		
		<div id="frm_interesse" class="column_100 marginTop_10">
		
			<h3>Interesse em Treinamento</h3>
			
			<div class="column_100 marginTop_10">
			
				<div class="column_75">
					<?php echo form_label('Descrição', 'interesse_treinamento') ;?>
					<input type="text" id="interesse_treinamento" name="interesse_treinamento" class="autocomplete" <?php echo $codigo_formulario ? 'disabled="disabled"' : '';?>
					   value="<?=$this->input->post("_interesse_treinamento"); ?>" alt="<?=$this->input->post("interesse_treinamento"); ?>"
					   data-url="<?=site_url("formulario_visita/obter_produtos_formulario_visita"); ?>">
				</div>
				
				<div class="column_25" style="width: 26%! important;">
					<?php echo  form_button(array(
							'name' => 'adicionar_interesse_treinamento',
							'value' => 'adicionar_interesse_treinamento',
							'content' => 'Adicionar', 
							'disabled' => $disabled,
							'class' => 'btn_full btn_add_interesse'
						));
					?>
				</div>
				
			</div>
			
			<div class="column_100 marginTop_10">
				<table id="tbl_interesse" class="novo_grid">
					<thead>
						<tr>
							<th>#</th>
							<th>Descrição</th>
							<?php if (!$codigo_formulario){?><th>Ações</th><?php } ?>
						</tr>
					</thead>
					<tbody style="max-height: 400px; "></tbody>
				</table>
			</div>
		</div>
		
		<div id="div_revenda" >
			<div class="column_100 marginTop_10">
				<?php echo form_label('Material PDV', 'material_pdv') ;?>
				<?php echo form_dropdown('material_pdv', array(
						''			=>	'Selecione...',
						'A'	=>	'Adesivo', 
						'B'	=>	'Banner', 
						'D'	=>	'Display', 
						'F'	=>	'Folheto',
						'M'	=>	'Mostruário',
						'P'	=>	'Placa'
					 ), $this->input->post('material_pdv'), $codigo_formulario ? 'disabled="disabled"' : '') ;
				?>
			</div>
		</div>
		
	</div>
	
	<div id="div_engenharia">
		<div class="column_33 marginTop_10">
			<div id="frm_interesse" class="column_100">
				<h3>Engenharia</h3>	
				<div class="column_100 marginTop_10">
					<?php echo form_label('Obra', 'obra', array('class' => 'column_100', 'style' => 'margin-bottom: 5px;')) ;?>
					<?php echo form_radio(array( 'name' => 'obra', 'value' => 'S', 'disabled' => $disabled, 'checked' => $this->input->post('obra') === 'S' ? true : false, 'class' => 'column_5')), '<span class="column_45">Sim</span>',
							   form_radio(array( 'name' => 'obra', 'value' => 'N', 'disabled' => $disabled, 'checked' => $this->input->post('obra') === 'N' ? true : false, 'class' => 'column_5')), '<span class="column_45">Não</span>';?>
				</div>
				<div class="column_100 marginTop_10">
					<?php echo form_label('Nome da Obra', 'nome_obra') ;?>
					<?php echo form_input(array(
							'name'		=>	'nome_obra',
							'maxlength'	=>	'254',
							'disabled'  =>  'disabled'
						 ), $this->input->post('nome_obra')) ;
					?>
				</div>
				<div class="column_100 marginTop_10">
					<?php echo form_label('Escritório', 'escritorio', array('class' => 'column_100', 'style' => 'margin-bottom: 5px;')) ;?>
					<?php echo form_radio(array( 'name' => 'escritorio', 'value' => 'S', 'disabled' => $disabled, 'checked' => $this->input->post('escritorio') ? true : false, 'class' => 'column_5')), '<span class="column_45">Sim</span>',
							   form_radio(array( 'name' => 'escritorio', 'value' => 'N', 'disabled' => $disabled, 'checked' => $this->input->post('N') ? true : false, 'class' => 'column_5')), '<span class="column_45">Não</span>';?>
				</div>
				
				<div class="column_100 marginTop_10">
					<?php echo form_label('Segmento', 'segmento') ;?>
					
					<?php echo '<select name="segmento"'; echo $codigo_formulario ? 'disabled="disabled"' : ''; echo '>';
					
							echo '<option value="">Selecione...</option>';
							
							foreach($segmentos_obra as $segmento) {
								echo '<option value="' . $segmento['segmento'] .'" ';  if($this->input->post('segmento') === $segmento['segmento']) { echo 'selected'; }else{ echo ''; } echo '>' . $segmento['segmento'] . ' - ' . htmlentities($segmento['descricao']) . '</option>';
							}
							
						  echo '</select>' ;
					?>
				</div>
				
				<div class="column_100 marginTop_10">
					<?php echo form_label('Fase da Obra', 'fase_obra'); ?>
					<?php echo form_dropdown('fase_obra', array(
							''		=>		'Selecione...',
							'F'		=>		'Fundação',
							'E'		=>		'Estrutura',
							'L'		=>		'Alvenaria',
							'I'		=>		'Instalação Elétrica/Hidráulica',
							'A'		=>		'Acabamento'
						 ), $this->input->post('fase_obra'), $codigo_formulario ? 'disabled="disabled"' : '') ;
					?>
				</div>
				
				<div class="column_100 marginTop_10">
					<?php echo form_label('Aplicação', 'aplicacao', array('class' => 'column_100', 'style' => 'margin-bottom: 5px;')) ;?>
					<?php echo form_radio(array( 'name' => 'aplicacao', 'value' => 'S', 'disabled' => $disabled, 'checked' => $this->input->post('aplicacao') === 'S' ? true : false, 'class' => 'column_5')), '<span class="column_45">Sim</span>',
							   form_radio(array( 'name' => 'aplicacao', 'value' => 'N', 'disabled' => $disabled, 'checked' => $this->input->post('aplicacao') === 'N' ? true : false, 'class' => 'column_5')), '<span class="column_45">Não</span>';?>
				</div>
				<div class="column_100 marginTop_10">
					<?php echo form_label('Viável para Próximo Passo', 'viavel_proximo_passo') ;?>
					<?php echo form_dropdown('viavel_proximo_passo', array(
							''	 	=>  'Selecione...',
							'01'	=>	'01 - Sim',
							'02' 	=>	'02 - PA ligar cada 30 dias',
							'03'	=>	'03 - Obra Finalizada',
							'04'	=>	'04 - Empresa não compra nossos produtos'
						 ), $this->input->post('viavel_proximo_passo'), $codigo_formulario ? 'disabled="disabled"' : '') ;
					?>
				</div>
				<div class="column_100 marginTop_10">
					<?php echo form_label('Próximo Passo', 'proximo_passo', array('class' => 'column_100', 'style' => 'margin-bottom: 5px;')) ;?>
					<?php echo form_radio(array( 'name' => 'proximo_passo', 'value' => 'L', 'disabled' => $disabled, 'checked' => $this->input->post('proximo_passo') === 'L' ? true : false, 'class' => 'column_5')), '<span class="column_45">Ligação</span>',
							   form_radio(array( 'name' => 'proximo_passo', 'value' => 'V', 'disabled' => $disabled, 'checked' => $this->input->post('proximo_passo') === 'V' ? true : false, 'class' => 'column_5')), '<span class="column_45">Visita</span>';?>
				</div>
				
				<div class="column_50 marginTop_10">
					<?php echo form_label('Data', 'data_proximo_passo');?>
					<?php echo form_input(array(
								'name' => 'data_proximo_passo', 
								'class'	   => 'datepicker_formulario_visita',
								'disabled' => $disabled
								), 
								($this->input->post('data_proximo_passo')) ? $this->input->post('data_proximo_passo') : date('d/m/Y', strtotime("+60 days")),
								'class="datepicker_formulario_visita" style="width: 96% !important;"');
					?>
				</div>
				<div class="column_50 marginTop_10">
					<?php echo form_label('Hora', 'horario_proximo_passo');?>
					<?php echo form_input(array(
							 'name'=>'horario_proximo_passo',
							 'disabled' => $disabled,
							 'alt'=> $codigo_formulario ? 'horario' : 'horario'
						 ), $this->input->post('horario_proximo_passo'));
					?>
				</div>
				<div class="column_100 marginTop_10">
					<?php echo form_label('Resumo da Reunião', 'resumo_reuniao') ;?>
					<?php echo form_textarea(array(
							 'name'			=>	'resumo_reuniao',
							 'style'		=>	'resize:none',
							 'maxlength'	=>  '254',
							 'rows'			=> 	'10',
							 'cols'			=>  '48'
						 ), $this->input->post('resumo_reuniao')) ;
					?>
				</div>
			</div>
		</div>
	</div>
	<div class="column_100 marginTop_10">
<?php	 		
	 echo
	 form_submit('salvar_formulario', 'Salvar', array('id' => 'btn_salvar')),
	 '<a href="'.site_url('visitas').'" onclick="return confirm(\'Deseja realmente Cancelar?\')" style="padding-left:20px;">Cancelar</a>',
	 form_close();	
?>
	</div>


<script type="text/javascript">
	/*currency mask*/!function($){"use strict";$.browser||($.browser={},$.browser.mozilla=/mozilla/.test(navigator.userAgent.toLowerCase())&&!/webkit/.test(navigator.userAgent.toLowerCase()),$.browser.webkit=/webkit/.test(navigator.userAgent.toLowerCase()),$.browser.opera=/opera/.test(navigator.userAgent.toLowerCase()),$.browser.msie=/msie/.test(navigator.userAgent.toLowerCase()));var a={destroy:function(){return $(this).unbind(".maskMoney"),$.browser.msie&&(this.onpaste=null),this},mask:function(a){return this.each(function(){var b,c=$(this);return"number"==typeof a&&(c.trigger("mask"),b=$(c.val().split(/\D/)).last()[0].length,a=a.toFixed(b),c.val(a)),c.trigger("mask")})},unmasked:function(){return this.map(function(){var a,b=$(this).val()||"0",c=-1!==b.indexOf("-");return $(b.split(/\D/).reverse()).each(function(b,c){return c?(a=c,!1):void 0}),b=b.replace(/\D/g,""),b=b.replace(new RegExp(a+"$"),"."+a),c&&(b="-"+b),parseFloat(b)})},init:function(a){return a=$.extend({prefix:"",suffix:"",affixesStay:!0,thousands:",",decimal:".",precision:2,allowZero:!1,allowNegative:!1},a),this.each(function(){function b(){var a,b,c,d,e,f=s.get(0),g=0,h=0;return"number"==typeof f.selectionStart&&"number"==typeof f.selectionEnd?(g=f.selectionStart,h=f.selectionEnd):(b=document.selection.createRange(),b&&b.parentElement()===f&&(d=f.value.length,a=f.value.replace(/\r\n/g,"\n"),c=f.createTextRange(),c.moveToBookmark(b.getBookmark()),e=f.createTextRange(),e.collapse(!1),c.compareEndPoints("StartToEnd",e)>-1?g=h=d:(g=-c.moveStart("character",-d),g+=a.slice(0,g).split("\n").length-1,c.compareEndPoints("EndToEnd",e)>-1?h=d:(h=-c.moveEnd("character",-d),h+=a.slice(0,h).split("\n").length-1)))),{start:g,end:h}}function c(){var a=!(s.val().length>=s.attr("maxlength")&&s.attr("maxlength")>=0),c=b(),d=c.start,e=c.end,f=c.start!==c.end&&s.val().substring(d,e).match(/\d/)?!0:!1,g="0"===s.val().substring(0,1);return a||f||g}function d(a){s.each(function(b,c){if(c.setSelectionRange)c.focus(),c.setSelectionRange(a,a);else if(c.createTextRange){var d=c.createTextRange();d.collapse(!0),d.moveEnd("character",a),d.moveStart("character",a),d.select()}})}function e(b){var c="";return b.indexOf("-")>-1&&(b=b.replace("-",""),c="-"),c+a.prefix+b+a.suffix}function f(b){var c,d,f,g=b.indexOf("-")>-1&&a.allowNegative?"-":"",h=b.replace(/[^0-9]/g,""),i=h.slice(0,h.length-a.precision);return i=i.replace(/^0*/g,""),i=i.replace(/\B(?=(\d{3})+(?!\d))/g,a.thousands),""===i&&(i="0"),c=g+i,a.precision>0&&(d=h.slice(h.length-a.precision),f=new Array(a.precision+1-d.length).join(0),c+=a.decimal+f+d),e(c)}function g(a){var b,c=s.val().length;s.val(f(s.val())),b=s.val().length,a-=c-b,d(a)}function h(){var a=s.val();s.val(f(a))}function i(){var b=s.val();return a.allowNegative?""!==b&&"-"===b.charAt(0)?b.replace("-",""):"-"+b:b}function j(a){a.preventDefault?a.preventDefault():a.returnValue=!1}function k(a){a=a||window.event;var d,e,f,h,k,l=a.which||a.charCode||a.keyCode;return void 0===l?!1:48>l||l>57?45===l?(s.val(i()),!1):43===l?(s.val(s.val().replace("-","")),!1):13===l||9===l?!0:!$.browser.mozilla||37!==l&&39!==l||0!==a.charCode?(j(a),!0):!0:c()?(j(a),d=String.fromCharCode(l),e=b(),f=e.start,h=e.end,k=s.val(),s.val(k.substring(0,f)+d+k.substring(h,k.length)),g(f+1),!1):!1}function l(c){c=c||window.event;var d,e,f,h,i,k=c.which||c.charCode||c.keyCode;return void 0===k?!1:(d=b(),e=d.start,f=d.end,8===k||46===k||63272===k?(j(c),h=s.val(),e===f&&(8===k?""===a.suffix?e-=1:(i=h.split("").reverse().join("").search(/\d/),e=h.length-i-1,f=e+1):f+=1),s.val(h.substring(0,e)+h.substring(f,h.length)),g(e),!1):9===k?!0:!0)}function m(){r=s.val(),h();var a,b=s.get(0);b.createTextRange&&(a=b.createTextRange(),a.collapse(!1),a.select())}function n(){setTimeout(function(){h()},0)}function o(){var b=parseFloat("0")/Math.pow(10,a.precision);return b.toFixed(a.precision).replace(new RegExp("\\.","g"),a.decimal)}function p(b){if($.browser.msie&&k(b),""===s.val()||s.val()===e(o()))a.allowZero?a.affixesStay?s.val(e(o())):s.val(o()):s.val("");else if(!a.affixesStay){var c=s.val().replace(a.prefix,"").replace(a.suffix,"");s.val(c)}s.val()!==r&&s.change()}function q(){var a,b=s.get(0);b.setSelectionRange?(a=s.val().length,b.setSelectionRange(a,a)):s.val(s.val())}var r,s=$(this);a=$.extend(a,s.data()),s.unbind(".maskMoney"),s.bind("keypress.maskMoney",k),s.bind("keydown.maskMoney",l),s.bind("blur.maskMoney",p),s.bind("focus.maskMoney",m),s.bind("click.maskMoney",q),s.bind("cut.maskMoney",n),s.bind("paste.maskMoney",n),s.bind("mask.maskMoney",h)})}};$.fn.maskMoney=function(b){return a[b]?a[b].apply(this,Array.prototype.slice.call(arguments,1)):"object"!=typeof b&&b?($.error("Method "+b+" does not exist on jQuery.maskMoney"),void 0):a.init.apply(this,arguments)}}(window.jQuery||window.Zepto);
	/*form validator*/!function($){function processError(a,b){b=b.replace(/%%C%%/gi,",");var c=!0;switch(options.displayType){case"alert-one":alert(b),styleField(a,!0),c=!1;break;case"alert-all":case"display-html":returnHash.push([a,b])}return c}function displayHTMLErrors(a,b){for(var c=options.errorTextIntro+"<br /><br />",d=0;d<b.length;d++)c+=options.errorHTMLItemBullet+b[d][1]+"<br />",styleField(b[d][0],0==d);return b.length>0?($("#"+options.errorTargetElementId).css("display","block"),$("#"+options.errorTargetElementId).html(c),!1):!0}function styleField(a,b){if(void 0==a.type){b&&a[0].focus();for(var c=0;c<a.length;c++)$(a[c]).hasClass(options.errorFieldClass)||$(a[c]).addClass(options.errorFieldClass)}else options.errorFieldClass&&$(a).addClass(options.errorFieldClass),b&&a.focus()}function isValidEmail(a){var b=$.trim(a),c="@",d=".",e=b.indexOf(c),f=b.length;return-1==b.indexOf(c)||-1==b.indexOf(c)||0==b.indexOf(c)||b.indexOf(c)==f||-1==b.indexOf(d)||0==b.indexOf(d)||b.indexOf(d)==f||-1!=b.indexOf(c,e+1)||b.substring(e-1,e)==d||b.substring(e+1,e+2)==d||-1==b.indexOf(d,e+2)||-1!=b.indexOf(" ")?!1:!0}function isValidDate(a,b,c,d){var e;if(e=c%4!=0||c%100==0&&c%400!=0?[31,28,31,30,31,30,31,31,30,31,30,31]:[31,29,31,30,31,30,31,31,30,31,30,31],!a||!b||!c)return!1;if(1>a||a>12)return!1;if(0>c)return!1;if(1>b||b>e[a-1])return!1;if(d){var f=new Date,g=f.getMonth()+1,h=f.getDate(),i=f.getFullYear();1==String(g).length&&(g="0"+g),1==String(h).length&&(h="0"+h);var j=String(i)+String(g)+String(h);if(1==String(a).length&&(a="0"+a),1==String(b).length&&(b="0"+b),incomingDate=String(c)+String(a)+String(b),Number(j)>Number(incomingDate))return!1}return!0}var options={},returnHash=[];$.fn.RSV=function(a){return options=$.extend({},$.fn.RSV.defaults,a),this.each(function(){$(this).bind("submit",{currForm:this,options:options},$(this).RSV.validate)})},$.fn.RSV.defaults={rules:[],displayType:"alert-all",errorFieldClass:null,errorTextIntro:"Please fix the following error(s) and resubmit:",errorJSItemBullet:"* ",errorHTMLItemBullet:"&bull; ",errorTargetElementId:"rsvErrors",customErrorHandler:null,onCompleteHandler:null},$.fn.RSV.validate=function(event){options=event.data.options;var form=event.data.currForm,rules=options.rules;returnHash=[],"display-html"==options.displayType&&($("#"+options.errorTargetElementId).text("").hide(),null!=options.errorFieldClass&&$("."+options.errorFieldClass).removeClass(options.errorFieldClass));for(var i=0;i<rules.length;i++){var row=rules[i].replace(/\\,/gi,"%%C%%");row=row.split(",");for(var satisfiesIfConditions=!0;row[0].match("^if:");){var cond=row[0];cond=cond.replace("if:","");var comparison="equal",parts=[];-1!=cond.search("!=")?(parts=cond.split("!="),comparison="not_equal"):parts=cond.split("=");var fieldToCheck=parts[0],valueToCheck=parts[1],fieldnameValue="";if(void 0==form[fieldToCheck].type)for(var j=0;j<form[fieldToCheck].length;j++)form[fieldToCheck][j].checked&&(fieldnameValue=form[fieldToCheck][j].value);else"checkbox"==form[fieldToCheck].type?form[fieldToCheck].checked&&(fieldnameValue=form[parts[0]].value):fieldnameValue=form[parts[0]].value;if("equal"==comparison&&fieldnameValue!=valueToCheck){satisfiesIfConditions=!1;break}if("not_equal"==comparison&&fieldnameValue==valueToCheck){satisfiesIfConditions=!1;break}row.shift()}if(satisfiesIfConditions){var requirement=row[0],fieldName=$.trim(row[1]),fieldName2,fieldName3,errorMessage,lengthRequirements,date_flag;if("function"!=requirement&&void 0==form[fieldName])return alert('RSV Error: the field "'+fieldName+"\" doesn't exist! Please check your form and settings."),!1;if("function"!=requirement&&options.errorFieldClass)if(void 0==form[fieldName].type)for(var j=0;j<form[fieldName].length;j++)$(form[fieldName][j]).hasClass(options.errorFieldClass)&&$(form[fieldName][j]).removeClass(options.errorFieldClass);else $(form[fieldName]).hasClass(options.errorFieldClass)&&$(form[fieldName]).removeClass(options.errorFieldClass);switch(6==row.length?(fieldName2=row[2],fieldName3=row[3],date_flag=row[4],errorMessage=row[5]):5==row.length?(fieldName2=row[2],fieldName3=row[3],errorMessage=row[4]):4==row.length?(fieldName2=row[2],errorMessage=row[3]):errorMessage=row[2],requirement.match("^length")&&(lengthRequirements=requirement,requirement="length"),requirement.match("^range")&&(rangeRequirements=requirement,requirement="range"),requirement){case"required":if(void 0==form[fieldName].type){for(var oneIsChecked=!1,j=0;j<form[fieldName].length;j++)form[fieldName][j].checked&&(oneIsChecked=!0);if(!oneIsChecked&&!processError(form[fieldName],errorMessage))return!1}else if("select-multiple"==form[fieldName].type){for(var oneIsSelected=!1,k=0;k<form[fieldName].length;k++)form[fieldName][k].selected&&(oneIsSelected=!0);if(!(oneIsSelected&&0!=form[fieldName].length||processError(form[fieldName],errorMessage)))return!1}else if("checkbox"==form[fieldName].type){if(!form[fieldName].checked&&!processError(form[fieldName],errorMessage))return!1}else if(!form[fieldName].value&&!processError(form[fieldName],errorMessage))return!1;break;case"digits_only":if(form[fieldName].value&&form[fieldName].value.match(/\D/)&&!processError(form[fieldName],errorMessage))return!1;break;case"letters_only":if(form[fieldName].value&&form[fieldName].value.match(/[^a-zA-Z]/)&&!processError(form[fieldName],errorMessage))return!1;break;case"is_alpha":if(form[fieldName].value&&form[fieldName].value.match(/\W/)&&!processError(form[fieldName],errorMessage))return!1;break;case"custom_alpha":for(var conversion={L:"[A-Z]",V:"[AEIOU]",l:"[a-z]",v:"[aeiou]",D:"[a-zA-Z]",F:"[aeiouAEIOU]",C:"[BCDFGHJKLMNPQRSTVWXYZ]",x:"[0-9]",c:"[bcdfghjklmnpqrstvwxyz]",X:"[1-9]",E:"[bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ]"},reg_exp_str="",j=0;j<fieldName2.length;j++)reg_exp_str+=conversion[fieldName2.charAt(j)]?conversion[fieldName2.charAt(j)]:fieldName2.charAt(j);var reg_exp=new RegExp(reg_exp_str);if(form[fieldName].value&&null==reg_exp.exec(form[fieldName].value)&&!processError(form[fieldName],errorMessage))return!1;break;case"reg_exp":var reg_exp_str=fieldName2.replace(/%%C%%/gi,",");if(5==row.length)var reg_exp=new RegExp(reg_exp_str,fieldName3);else var reg_exp=new RegExp(reg_exp_str);if(form[fieldName].value&&null==reg_exp.exec(form[fieldName].value)&&!processError(form[fieldName],errorMessage))return!1;break;case"length":switch(comparison_rule="",rule_string="",lengthRequirements.match(/length=/)?(comparison_rule="equal",rule_string=lengthRequirements.replace("length=","")):lengthRequirements.match(/length>=/)?(comparison_rule="greater_than_or_equal",rule_string=lengthRequirements.replace("length>=","")):lengthRequirements.match(/length>/)?(comparison_rule="greater_than",rule_string=lengthRequirements.replace("length>","")):lengthRequirements.match(/length<=/)?(comparison_rule="less_than_or_equal",rule_string=lengthRequirements.replace("length<=","")):lengthRequirements.match(/length</)&&(comparison_rule="less_than",rule_string=lengthRequirements.replace("length<","")),comparison_rule){case"greater_than_or_equal":if(!(form[fieldName].value.length>=parseInt(rule_string)||processError(form[fieldName],errorMessage)))return!1;break;case"greater_than":if(!(form[fieldName].value.length>parseInt(rule_string)||processError(form[fieldName],errorMessage)))return!1;break;case"less_than_or_equal":if(!(form[fieldName].value.length<=parseInt(rule_string)||processError(form[fieldName],errorMessage)))return!1;break;case"less_than":if(!(form[fieldName].value.length<parseInt(rule_string)||processError(form[fieldName],errorMessage)))return!1;break;case"equal":var range_or_exact_number=rule_string.match(/[^_]+/),fieldCount=range_or_exact_number[0].split("-");if(2==fieldCount.length){if((form[fieldName].value.length<fieldCount[0]||form[fieldName].value.length>fieldCount[1])&&!processError(form[fieldName],errorMessage))return!1}else if(form[fieldName].value.length!=fieldCount[0]&&!processError(form[fieldName],errorMessage))return!1}break;case"valid_email":if(form[fieldName].value&&!isValidEmail(form[fieldName].value)&&!processError(form[fieldName],errorMessage))return!1;break;case"valid_date":var isLaterDate=!1;if("later_date"==date_flag?isLaterDate=!0:"any_date"==date_flag&&(isLaterDate=!1),!isValidDate(form[fieldName].value,form[fieldName2].value,form[fieldName3].value,isLaterDate)&&!processError(form[fieldName],errorMessage))return!1;break;case"same_as":if(form[fieldName].value!=form[fieldName2].value&&!processError(form[fieldName],errorMessage))return!1;break;case"range":switch(comparison_rule="",rule_string="",rangeRequirements.match(/range=/)?(comparison_rule="equal",rule_string=rangeRequirements.replace("range=","")):rangeRequirements.match(/range>=/)?(comparison_rule="greater_than_or_equal",rule_string=rangeRequirements.replace("range>=","")):rangeRequirements.match(/range>/)?(comparison_rule="greater_than",rule_string=rangeRequirements.replace("range>","")):rangeRequirements.match(/range<=/)?(comparison_rule="less_than_or_equal",rule_string=rangeRequirements.replace("range<=","")):rangeRequirements.match(/range</)&&(comparison_rule="less_than",rule_string=rangeRequirements.replace("range<","")),comparison_rule){case"greater_than_or_equal":if(!(form[fieldName].value>=Number(rule_string)||processError(form[fieldName],errorMessage)))return!1;break;case"greater_than":if(!(form[fieldName].value>Number(rule_string)||processError(form[fieldName],errorMessage)))return!1;break;case"less_than_or_equal":if(!(form[fieldName].value<=Number(rule_string)||processError(form[fieldName],errorMessage)))return!1;break;case"less_than":if(!(form[fieldName].value<Number(rule_string)||processError(form[fieldName],errorMessage)))return!1;break;case"equal":var rangeValues=rule_string.split("-");if((form[fieldName].value<Number(rangeValues[0])||form[fieldName].value>Number(rangeValues[1]))&&!processError(form[fieldName],errorMessage))return!1}break;case"function":if(custom_function=fieldName,eval("var result = "+custom_function+"()"),-1!=result.constructor.toString().indexOf("Array"))for(var j=0;j<result.length;j++)if(!processError(result[j][0],result[j][1]))return!1;break;default:return alert("Unknown requirement flag in validateFields(): "+requirement),!1}}}if("function"==typeof options.customErrorHandler){if(!options.customErrorHandler(form,returnHash))return!1}else if("alert-all"==options.displayType){for(var errorStr=options.errorTextIntro+"\n\n",i=0;i<returnHash.length;i++)errorStr+=options.errorJSItemBullet+returnHash[i][1]+"\n",styleField(returnHash[i][0],0==i);if(returnHash.length>0)return alert(errorStr),!1}else if("display-html"==options.displayType){var success=displayHTMLErrors(form,returnHash);if(!success)return!1}return"function"==typeof options.onCompleteHandler?options.onCompleteHandler():!0}}(jQuery);
	 
	 $('.currency').maskMoney({
		prefix: 'R$ ',
		thousands: '.',
		decimal: ',',
		precision: 6,
		allowZero: true
	 });
	/*									*
	 *	arrays tabelas	*
	 *									*/
	var tabelas = [];
	tabelas['contatos'] = [];
	tabelas['concorrentes'] = [];
	tabelas['produtos'] = [];
	tabelas['interesse'] = [];
	
	var regras = [];
	
	function validateEmail(email) { 
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}
	
	String.prototype.replaceArray = function(find, replace) {
	  var replaceString = this;
	  var regex; 
	  for (var i = 0; i < find.length; i++) {
		regex = new RegExp(find[i], 'g');
		replaceString = replaceString.replace(regex, replace[i]);
	  }
	  return replaceString;
	};
	
	Number.prototype.formatMoney = function(places, symbol, thousand, decimal) {
		places = !isNaN(places = Math.abs(places)) ? places : 2;
		symbol = symbol !== undefined ? symbol : "R$ ";
		thousand = thousand || ".";
		decimal = decimal || ",";
		var number = this, 
			negative = number < 0 ? "-" : "",
			i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
			j = (j = i.length) > 3 ? j % 3 : 0;
		return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
	};
	
	function currencyTofloat(valor){
		return parseFloat(valor.replaceArray(['[R$]', '[.]', '[,]'], ['', '', '.']));
	}
	
	function check_has_concorrentes() {
		if ($('#tbl_concorrentes tbody').html()) {
			$('input[name=contador_concorrentes').val('has');
		} else {
			$('input[name=contador_concorrentes').val('');
		}
	}
	
	function check_has_contatos() {
		if ($('#tbl_contatos tbody').html()) {
			$('input[name=contador_contatos').val('has');
		} else {
			$('input[name=contador_contatos').val('');
		}
	}
	
	/*									*
	 *	inicio funcoes tabela contatos	*
	 *									*/
	 $('.btn_add_contato').click(function(e){
		var validation = 1;
		var array = [];
		
		var nome = $(":input[name='nome_contato']");		
		if($.trim( nome.val() ) == ""){
			nome.addClass("error");
			validation = 0;
		}else{
			nome.addClass("success");
			array.push({'nome_contato': nome.val()});
		}
		
		var tipo = $("select[name='tipo_contato']");		
		if($.trim( tipo.val() ) == ""){
			tipo.addClass("error");
			validation = 0;
		}else{
			tipo.addClass("success");
			array.push({'tipo_contato': $("select[name='tipo_contato'] option:selected").text()});
		}
		
		if($.trim($(":input[name='email_contato']").val())){
			if(!validateEmail($(":input[name='email_contato']").val())){
				$(":input[name='email_contato']").addClass("error");
				alert('Email incorreto.')
				validation = 0;
			}
		}
		
		if(validation == 1){
			array.push({'telefone_contato': $(":input[name='telefone_contato']").val()});
			array.push({'email_contato': $(":input[name='email_contato']").val()});
			limparCamposGrid('#frm_contatos');
			
			var attr = $(this).attr('edicao');
			if (typeof attr !== typeof undefined && attr !== false) {
				arrayGrid(array,'add','contatos', true, true, attr);
				$(this).removeAttr('edicao');
			}else{
				arrayGrid(array,'add','contatos', true, true );
			}
			check_has_contatos();
		}
	 });
	/*									*
	 *  fim funcoes tabela contatos		*
	 *									*/
	 
	 /*									*
	 *	inicio funcoes tabela concorrentes	*
	 *									*/
	 $('.btn_add_concorrente').click(function(e){
		var validation = 1;
		var array = [];
		
		var nome  = $(":input[name='_nome_concorrente_revenda']");
		var desc  = $(":input[name='_descricao_concorrente_revenda']");
		var preco = $(":input[name='preco_concorrente_revenda']");
		
		if($.trim( $(":input[name='nome_concorrente_revenda']").val() ) == ""){
			nome.addClass("error");
			validation = 0;
		}else{
			nome.addClass("success");
			array.push({'nome_concorrente_revenda': nome.val()});
		}
		
		if($.trim( $(":input[name='_descricao_concorrente_revenda']").val() ) == ""){
			desc.addClass("error");
			validation = 0;
		}else{
			desc.addClass("success");
			array.push({'descricao_concorrente_revenda': desc.val()});
		}
		
		if($.trim( preco.val() ) == ""){
			preco.addClass("error");
			validation = 0;
		}else{
			val = currencyTofloat(preco.val());
			if(parseInt(val) > 9999999){
				preco.addClass("error");
				validation = 0;
				alert('O valor do preço é muito alto.');
			}else{
				preco.addClass("success");
				array.push({'preco_concorrente_revenda': preco.val()});
			}
		}
				
		if(validation == 1){			
			limparCamposGrid('#frm_concorrentes');
			
			var attr = $(this).attr('edicao');
			if (typeof attr !== typeof undefined && attr !== false) {
				arrayGrid(array,'add','concorrentes', true, true, attr);
				$(this).removeAttr('edicao');
			}else{
				arrayGrid(array,'add','concorrentes', true, true );
			}
			check_has_concorrentes();
		}
		
	 });
	/*									*
	 *  fim funcoes tabela concorrentes		*
	 *									*/
	 
	 /*									*
	 *	inicio funcoes tabela produtos	*
	 *									*/
	 $('.btn_add_produto').click(function(e){
		var validation = 1;
		var array = [];
		
		var input = $(":input[name='_descricao_produto_apresentado']");		
		if($.trim( $(":input[name='descricao_produto_apresentado']").val()) == ""){
			input.addClass("error");
			validation = 0;
		}else{
			input.addClass("success");
			array.push({'descricao_produto_apresentado': input.val()});
		}
		
		var preco = $(":input[name='preco_objetivo_produto_apresentado']");		
		if($.trim( preco.val() ) == ""){
			preco.addClass("error");
			validation = 0;
		}else{
			val = currencyTofloat(preco.val());
			if(parseInt(val) > 9999999){
				preco.addClass("error");
				validation = 0;
				alert('O valor do preço é muito alto.');
			}else{
				preco.addClass("success");
				array.push({'preco_objetivo_produto_apresentado': $(":input[name='preco_objetivo_produto_apresentado']").val()});
			}			
		}
		
		if(validation == 1){
			limparCamposGrid('#frm_produtos');
			
			var attr = $(this).attr('edicao');
			if (typeof attr !== typeof undefined && attr !== false) {
				arrayGrid(array,'add','produtos', true, true, attr);
				$(this).removeAttr('edicao');
			}else{
				arrayGrid(array,'add','produtos', true, true );
			}
		}		
		
		if($("#tbl_produtos tbody tr").length === 3){
			enableDisableCamposGrid('#frm_produtos');
		}
	 });
	/*									*
	 *  fim funcoes tabela produtos		*
	 *									*/
	 
	  /*									*
	 *	inicio funcoes interesse_treinamento	*
	 *									*/
	 $('.btn_add_interesse').click(function(e){
		var validation = 1;
		var array = [];
		
		var input = $(":input[name='_interesse_treinamento']");		
		if($.trim( $(":input[name='interesse_treinamento']").val() ) == ""){
			input.addClass("error");
			validation = 0;
		}else{
			input.addClass("success");
			array.push({'interesse_treinamento': input.val()});
		}
		
		if(validation == 1){
			limparCamposGrid('#frm_interesse');
			
			var attr = $(this).attr('edicao');
			if (typeof attr !== typeof undefined && attr !== false) {
				arrayGrid(array,'add','interesse', true, true, attr);
				$(this).removeAttr('edicao');
			}else{
				arrayGrid(array,'add','interesse', true, true );
			}
		}		
		
	 });
	/*									*
	 *  fim funcoes tabela interesse_treinamento		*
	 *									*/
	 
	 /*									*
	 *	inicio funcoes tabela 			*
	 *									*/
	 function reindex_array_keys(array, start){
		var temp = [];
		start = typeof start == 'undefined' ? 0 : start;
		start = typeof start != 'number' ? 0 : start;
		for(i in array){
			temp[start++] = array[i];
		}
		return temp;
	}
	
	var gridAtual = false;
	var htmlGrid = '';
	function arrayGrid(dados, acao, grid, editar, deletar, index){
		editar = editar === undefined ? false : editar;
		deletar = deletar === undefined ? false : deletar;
		
		if((!gridAtual) || (gridAtual != grid)) {
			gridAtual = grid;
			htmlGrid = '';
		}
		
		switch(acao){
			case 'bd':
			case "add":
				var seclastRow = index === undefined ? $("#tbl_"+grid+" tbody tr").length : parseInt(index);
				var html = "";
				var acoes = [];
				html += "<td>"+(seclastRow + 1)+"</td>";
				for(var i = 0; i < dados.length; i++){
					$.each(dados[i], function(key, value){
						value = !!!value ? '' : value;
						html +='<td class="'+key+'">'+value+'</td>';
					});
				}	
				
				if(acao == 'bd'){
					html = '<tr id="'+grid+'_'+seclastRow+'"> '+html+'</tr>';
					htmlGrid += html;
					$("#tbl_"+grid).append(htmlGrid);
				} 
				
				if(editar){
					acoes.push('<a href="#frm_'+grid+'" onclick="editarGrid('+(seclastRow)+',\''+grid+'\')">Editar</a>');
				}
				if(deletar){
					acoes.push('<a href="#tbl_'+grid+'" onclick="removerGrid('+(seclastRow)+',\''+grid+'\')">Remover</a>');
				}
				if(editar || deletar){
					html += '<td>' + acoes.join(' | ') + '</td>';
				}else{
					html += <?php if (!$codigo_formulario){?>'<td></td>'<?php }else{ ?> '' <?php }?>;
				}
				
				
				
				if(!!!index && acao != 'bd'){
					html = '<tr id="'+grid+'_'+seclastRow+'"> '+html+'</tr>';
					$("#tbl_"+grid+" tbody").append(html);
					tabelas[grid].push(dados);
				}else{
					tabelas[grid][index] = dados;
					$("#"+grid+'_'+index).html(html).show();
				}
				addGridHidden(grid);
			break;
		}
	 };
	 
	 function removerGrid(index, grid){
		$('#tbl_'+grid+' tbody tr:eq('+index+')').remove();
		delete tabelas[grid][index];
		tabelas[grid] = reindex_array_keys(tabelas[grid]);
		switch(grid) {
			case 'concorrentes':
				check_has_concorrentes();
				break;
			case 'contatos':
				check_has_contatos();
				break;
		}
	 }
	 
	 function editarGrid(index, grid){
		 tbl = '#tbl_'+grid+' tbody tr:eq('+index+') ';
		 
		 switch(grid)
		{
			case 'concorrentes':
				$(":input[name='_nome_concorrente_revenda']").val($(tbl).find('.nome_concorrente_revenda').text());
				$(":input[name='_descricao_concorrente_revenda']").val($(tbl).find('.descricao_concorrente_revenda').text());
				$(":input[name='preco_concorrente_revenda']").val($(tbl).find('.preco_concorrente_revenda').text());
				$(".btn_add_concorrente").attr('edicao', index);
				//enableDisableCamposGrid('#frm_'+grid);
				$(":input[name='preco_concorrente_revenda']").attr('disabled', false);
				$(".btn_add_concorrente").attr('disabled', false);
				break;
			case 'contatos':
				$(":input[name='nome_contato']").val($(tbl).find('.nome_contato').text());
				$("select[name='tipo_contato']").val($(tbl).find('.tipo_contato').text());
				$(":input[name='telefone_contato']").val($(tbl).find('.telefone_contato').text());
				$(":input[name='email_contato']").val($(tbl).find('.email_contato').text());
				$(".btn_add_contato").attr('edicao', index);
				break;
			case 'produtos':
				$(":input[name='_descricao_produto_apresentado']").val($(tbl).find('.descricao_produto_apresentado').text());
				$(":input[name='preco_objetivo_produto_apresentado']").val($(tbl).find('.preco_objetivo_produto_apresentado').text());
				$(".btn_add_produto").attr('edicao', index);
				break;
			case 'interesse':
				$(":input[name='interesse_treinamento']").val($(tbl).find('.interesse_treinamento').text());
				$(".btn_add_interesse").attr('edicao', index);
				break;
		}
		$(tbl).hide();
	 }
	 
	 function limparCamposGrid(div){
		$(div).find(':input').each(function() {
			var isDisabled = $(this).is(':disabled');
			if (isDisabled) {
				$(this).prop('disabled', false);
			}
			switch(this.type) {
				case 'password':
				case 'text':
				case 'textarea':
				case 'file':
				case 'select-one':
				case 'select-multiple':
					$(this).val('');
					break;
				case 'checkbox':
				case 'radio':
					this.checked = false;
			}
			$(this).removeClass('success');
			$(this).removeClass('error');
		});
	 }
	 
	 function addGridHidden(grid){	
		dados = JSON.stringify(tabelas[grid]);
		if($("textarea[name='tbl_"+grid+"']").length > 0){
			$("textarea[name='tbl_"+grid+"']").val(dados);
		}else{
			$('form').append('<textarea name="tbl_'+grid+'" style="display:none;">'+dados+'</textarea>');
		}
	 }
	 
	function json2array(json){
		var result = [];
		var keys = Object.keys(json);
		keys.forEach(function(key){
			result.push(json[key]);
		});
		return result;
	}

	 
	 function obj2array(json){
		var arr = [];		
		$.each(json, function(key, value){
			if(typeof json === 'object'){
				key = Object.keys(value);
				value = value[key];
				arr[key] = value;
			}
		});
		return arr;
	}
	 
	function carregaGrids(){
		if($("textarea[name='tbl_contatos']").val()){
			contatos = json2array(JSON.parse($("textarea[name='tbl_contatos']").val()));
			for(var i = 0; i < contatos.length; i++){
				arrayGrid(json2array(contatos[i]),'add','contatos');
			};			
		}
		if($("textarea[name='tbl_concorrentes']").val()){
			concorrente = json2array(JSON.parse($("textarea[name='tbl_concorrentes']").val()));
			$('input[name=contador_concorrentes').val(concorrente.length);
			for(var j = 0; j < concorrente.length; j++){
				arrayGrid(json2array(concorrente[j]),'add','concorrentes');
			};	
		}
		if($("textarea[name='tbl_interesse']").val()){
			interesse = json2array(JSON.parse($("textarea[name='tbl_interesse']").val()));
			for(var k = 0; k < interesse.length; k++){
				arrayGrid(json2array(interesse[k]),'add','interesse');
			};			
		}
		if($("textarea[name='tbl_produtos']").val()){
			produtos = json2array(JSON.parse($("textarea[name='tbl_produtos']").val()));
			for(var l = 0; l < produtos.length; l++){
				arrayGrid(json2array(produtos[l]),'add','produtos');
			};			
		}
	 }
	 
	
	 
	 function carregaGridsBD(){
		if($("[name='horario_proximo_passo']").val().length == 2){
			$("[name='horario_proximo_passo']").val( $("[name='horario_proximo_passo']").val() + ':00');
		}
		$('.novo_grid tbody').html('<tr><td colspan="6">Aguarde Carregando Informações</td></tr>');
		$.ajax({
		  dataType: "json",
		  url: '<?php echo site_url("formulario_visita/obter_grid/$codigo_formulario");?>',
		  success: function(ret){
			$.each(ret, function(grid, dados){
				if(dados.length == 0){
					$('#tbl_' + grid + " tbody").html('');
					$('#tbl_' + grid + " tbody").html('<tr><td colspan="6">Nenhum item a exibir</td></tr>');
				}
				for(var i = 0; i < dados.length; i++){
					var array = new Array();
					switch(grid){
						case 'contatos':
							array.push({'nome_contato': dados[i]['nome_contato']});
							array.push({'tipo_contato': dados[i]['tipo_contato'] + ' - ' + dados[i]['descricao']});
							array.push({'telefone_contato': dados[i]['telefone']});
							array.push({'email_contato': dados[i]['email']});
							$('#tbl_contatos tbody').html('');
							break;
						case 'concorrentes':
							codigo = (dados[i]['produto'] == '') ? '' : dados[i]['produto'] + ' - ';
							array.push({'nome_concorrente_revenda': dados[i]['concorrente']});
							array.push({'descricao_concorrente_revenda': codigo + dados[i]['nome_produto']});
							array.push({'preco_concorrente_revenda': parseFloat(dados[i]['preco']).formatMoney(6)});
							$('#tbl_concorrentes tbody').html('');
							check_has_concorrentes();
							break;
						case 'produtos':
							array.push({'descricao_produto_apresentado': dados[i]['produto_apresentado'] + ' - ' + dados[i]['descricao']});
							array.push({'preco_objetivo_produto_apresentado': parseFloat(dados[i]['preco']).formatMoney(6)});
							$('#tbl_produtos tbody').html('');
							break;
						case 'interesse':
							array.push({'interesse_treinamento': dados[i]['produto'] + ' - ' + dados[i]['descricao']});
							$('#tbl_interesse tbody').html('');
							break;
					
					}
					arrayGrid(array, 'bd', grid);
				}
			})
		  }
		});
	 }
	 
	 function enableDisableCamposGrid(div){
		$(div).find(':input').each(function() {
			var isDisabled = $(this).is(':disabled');

			if (isDisabled) {
				$(this).prop('disabled', false);
			} else {
				$(this).attr('disabled', 'disabled');
			}
		});
	 }
	 
	<?php 
		if($formulario_visita){
			echo 'carregaGrids();';
		}else if( $this->input->post('edicao')){
			echo "carregaGridsBD();";
		}
	?>
	
	/*									*
	 *	fim funcoes tabela 				*
	 *									*/
	
	var calcularDataProximoPasso = function (categoria_cliente) {
		
		var dataVisita = $('[name="data_visitacao"]').val();
		dataVisita = dataVisita.replace(/\//g, "-");
		dataVisita = dataVisita.substr(6, 9) + '-' + dataVisita.substr(3, 2) + '-' + dataVisita.substr(0, 2);
		dataVisita = new Date(dataVisita);
		dataVisita.setDate(dataVisita.getDate());
		
		switch(categoria_cliente) {
			case 'AA':
				dataVisita.setDate(dataVisita.getDate() + 30);
				break;
			case 'A':
				dataVisita.setDate(dataVisita.getDate() + 30);
				break;
			case 'B':
				dataVisita.setDate(dataVisita.getDate() + 60);
				break;
			case 'C':
				dataVisita.setDate(dataVisita.getDate() + 90);
				break;
			case 'Prospect':
				dataVisita.setDate(dataVisita.getDate() + 60);
				break;
			default:
				dataVisita.setDate(dataVisita.getDate() + 30);
		}
		
		
		$('[name="data_proximo_passo"]').val(date('d/m/Y',(dataVisita.getTime() / 1000) ));
	}
	
	function add_regra_validacao(regra, campo, msg){
		regras.push(regra + ',' + campo + ',' + msg);
	}
	
	function remove_regra_validacao(index){
		if(typeof index === 'array'){
			for(var i=0; index.length; i++){
				regras.splice(index[i], 0);
			}
		}else{
			regras.splice(index, 0);
		}
	}
	
	
	function regras_cliente(tipo){	
		<?php if(!$codigo_formulario):?>
		add_regra_validacao('if:cliente_novo=N,required', '_input_cliente', 'Informe o nome do Cliente.');
		add_regra_validacao('if:cliente_novo=S,required', 'input_cliente_novo', 'Informe o nome do Cliente.');
		add_regra_validacao('if:cliente_novo=S,length<=40', 'input_cliente_novo', 'Nome do cliente não pode conter mais de 40 caracteres.');
		add_regra_validacao('if:cliente_novo=S,required', 'classificacao_cliente', 'Informe a Classificação do Cliente.');
		add_regra_validacao('if:cliente_novo=S,required', 'telefone_cliente', 'Informe o Telefone do Cliente.');
		add_regra_validacao('if:cliente_novo=S,required', 'tipo_cliente','Informe o Tipo do Cliente.');
		add_regra_validacao('if:cliente_novo=S,required', 'cidade_regiao_cliente','Informe a Cidade/Região do Cliente.');
		add_regra_validacao('if:cliente_novo=S,length<=80', 'cidade_regiao_cliente', 'Cidade/Região não pode conter mais de 40 caracteres.');
		add_regra_validacao('if:cliente_novo=S,required', 'estado_cliente','Informe o Estado do Cliente.');
		add_regra_validacao('if:cliente_novo=S,required', 'cep_cliente','Informe o CEP do Cliente.');
		
		add_regra_validacao('required', 'obra', 'Informe à Obra.');
		add_regra_validacao('if:obra=S,required', 'nome_obra', 'Informe o nome da Obra.');
		add_regra_validacao('if:obra=S,length<=60', 'nome_obra', 'Nome da Obra não pode conter mais de 40 caracteres.');
		add_regra_validacao('required', 'escritorio', 'Informe o Escritório.');
		add_regra_validacao('required', 'proximo_passo','Informe o Próximo Passo.');
		add_regra_validacao('required', 'data_proximo_passo','Informe a Data do Próximo Passo.');
		add_regra_validacao('required', 'horario_proximo_passo','Informe a Hora do Próximo Passo.');
		add_regra_validacao('required', 'contador_contatos','Informe Pelo Menos um Contato.');
		add_regra_validacao('required', 'contador_concorrentes','Informe Pelo Menos um Concorrente.');
				
		<?php else:?>
		
		<?php endif;?>
		
	}
	
	function regras_engenharia(){
		<?php if(!$codigo_formulario):?>
		add_regra_validacao('if:setor=E,required', 'segmento','Selecione o Segmento.');
		add_regra_validacao('if:setor=E,required', 'fase_obra','Selecione a Fase da Obra.');
		add_regra_validacao('if:setor=E,required', 'aplicacao','Informe a Aplicação.');
		add_regra_validacao('if:setor=E,required', 'viavel_proximo_passo','Selecione uma opção para Viável para o Próximo Passo..');
		add_regra_validacao('if:setor=E,required', 'resumo_reuniao','Preencha o Resumo da Reunião.');
		add_regra_validacao('if:setor=E,length<254', 'resumo_reuniao', 'Resumo da Reunião não pode conter mais de 254 caracteres.');
		
		add_regra_validacao('if:setor=R,required', 'material_pdv', 'Selecione uma opção para o Material PDV.');
		<?php else:?>
		add_regra_validacao('if:setor=E,required', 'resumo_reuniao','Preencha o Resumo da Reunião.');
		add_regra_validacao('if:setor=E,length<254', 'resumo_reuniao', 'Resumo da Reunião não pode conter mais de 254 caracteres.');
		<?php endif;?>
		
	}
	
	$( document ).ready(function() {
		
		$('[name=visita_coordenacao]').change(function(){
			if($(this).val() == 'S'){
				$("#input_cliente").attr('data-url', '<?=site_url("formulario_visita/obter_clientes_formulario_visita/1"); ?>');
				resetarAutocomplete($("#input_cliente"));
			} else {
				$("#input_cliente").attr('data-url', '<?=site_url("formulario_visita/obter_clientes_formulario_visita"); ?>');
				resetarAutocomplete($("#input_cliente"));
			}
		});
	
	
		<?php if(!$codigo_formulario):?>
		add_regra_validacao('required', 'data_visitacao', 'Informe a Data de Visitação.'); 
		add_regra_validacao('required', 'data_preenchimento', 'Informe a Data de Preenchimento.');
		add_regra_validacao('required', 'cliente_novo', 'Informe se é um Cliente novo ou um Cliente existente.');
		<?php endif;?>
		
		add_regra_validacao('required', 'data_visitacao', 'Informe a Data de Visitação.'); 
		add_regra_validacao('required', 'data_preenchimento', 'Informe a Data de Preenchimento.');
		add_regra_validacao('required', 'setor', 'Selecione um Setor.');
		add_regra_validacao('required', 'cliente_novo', 'Informe se é um Cliente novo ou um Cliente existente.');
		
		regras_cliente();
		regras_engenharia();
		
		add_regra_validacao('required', 'gerou_pedido_orcamento', 'Informe se Gerou Pedido ou Orçamento.');

		$("form").RSV({
		  onCompleteHandler: function(){
			$('form').find('input, textarea, button, select, radio, checkbox').prop('disabled',false);
			return true;
		  },
		  errorFieldClass: "error",
		  errorTextIntro: 'Por favor verifique os erros abaixo:',
		  displayType: "display-html",
		  errorHTMLItemBullet: "&#8212; ",
			rules: regras
		});
		
		
		$("[name='horario_proximo_passo']").blur(function(e){
			if($(this).val().length == 2){
				$(this).val( $(this).val() + ':00');
			}
		});
		
		$('[name="salvar_formulario"]').on('click', function() {
			//$('input, select').attr('disabled', false);
		});
		
		$('[name="setor"]').on('click', function() {
			
			$('[name="material_pdv"]').val('Selecione...');
	
			$('[name="nome_obra"]').attr('disabled', true);
				
			$('[name="nome_obra"]').val('');
			$('[name="horario_proximo_passo"]').val('');
			$('[name="resumo_reuniao"]').val('');
				
			$('[name="obra"]:checked').removeAttr('checked');
			$('[name="escritorio"]:checked').removeAttr('checked');
			$('[name="aplicacao"]:checked').removeAttr('checked');
			$('[name="proximo_passo"]:checked').removeAttr('checked');
				
			$('[name="segmento"]').val('Selecione...');
			$('[name="fase_obra"]').val('Selecione...');
			$('[name="viavel_proximo_passo"]').val('Selecione...');
			
		});
		
		$('[name="cliente_novo"]').on('click change', function() {
			
			
			
			$('#input_cliente').attr('disabled', false);
			
			$('#input_cliente').val('');
			$('#input_cliente_novo').val('');
			$('[name="codigo_cliente"]').val('')
			$('[name="classificacao_cliente"]').val('');
			$('[name="telefone_cliente"]').val('');
			//$('[name="tipo_cliente"]').val('');
			$('[name="cidade_regiao_cliente"]').val('');
			$('[name="estado_cliente"]').val('');
			$('[name="cep_cliente"]').val('');
			$('[name="loja_cliente"]').val('');
		
			
			
			
			
			$('#tbl_contatos tbody').html('');
			tabelas['contatos'] = [];
			console.log($('[name="cliente_novo"]:checked').val());
			if($('[name="cliente_novo"]:checked').val() == 'S') {	
				$('[name="classificacao_cliente"]').val('teste');
				console.log('in');		
				$('#input_cliente').hide();	
				$('#input_cliente_novo').show();				
				
				console.log($('[name="classificacao_cliente"]').val());
				
				
				$('[name="classificacao_cliente"]').val('Prospect');
				$("input[name=input_cliente]").val('');

				$('[name="telefone_cliente"]').attr('disabled', false);
				//$('[name="tipo_cliente"]').attr('disabled', false);
                $('[name="tipo_cliente"]').val('');

				$('[name="cidade_regiao_cliente"]').attr('disabled', false);
				$('[name="estado_cliente"]').attr('disabled', false);
				$('[name="cep_cliente"]').attr('disabled', false);
				
				calcularDataProximoPasso('Prospect');
				
				//validacoes
					
			} else {

				enableDisableCamposGrid('frm_contatos');
				
				$('#input_cliente_novo').hide();
				$('#input_cliente').show();
				
				$('[name="telefone_cliente"]').attr('disabled', true);
				//$('[name="tipo_cliente"]').attr('disabled', true);
                $('[name="tipo_cliente"]').val('');

				$('[name="cidade_regiao_cliente"]').attr('disabled', true);
				$('[name="estado_cliente"]').attr('disabled', true);
				$('[name="cep_cliente"]').attr('disabled', true);		
			}
		
		});
		
		$('[name="data_visitacao"]').on('change', function() {
		
			if($('[name="cliente_novo"]:checked').val() == 'S') {	
				calcularDataProximoPasso('Prospect');
				
			} else {
				
				if($('[name="classificacao_cliente"]').val()) {	
					calcularDataProximoPasso($('[name="classificacao_cliente"]').val());
				}
			}
		});
		
		
		
		$("#input_cliente").focusout(function () {
			obter_informacoes_cliente($("input[name=input_cliente]").val());
		});
		
		$("#input_cliente").bind("keydown",function(){
			obter_informacoes_cliente($("input[name=input_cliente]").val());
		});	
		
		
		function obter_informacoes_cliente(codigo_loja_cliente)
		{
			
			$.get("<?=site_url('criar_pedidos/obter_informacoes_cliente_formulario_ajax')?>/" + codigo_loja_cliente.replace("|", "_"), function(dados) {
				
				if (dados.cliente_encontrado)
				{						

					if (dados.cliente.pode_visitar == '1') {
						$('#input_cliente').attr('disabled', true);
						
						$('[name="input_cliente"]').val(dados.cliente.nome);
						$('[name="loja_cliente"]').val(dados.cliente.loja);
						$('[name="input_cliente_hidden"]').val(dados.cliente.nome);
						$('[name="codigo_cliente"]').val(dados.cliente.codigo);
						$('[name="classificacao_cliente"]').val(dados.cliente.categoria);
						$('[name="telefone_cliente"]').val(dados.cliente.telefone);
						$('[name="cidade_regiao_cliente"]').val(dados.cliente.cidade);
						$('[name="estado_cliente"]').val(dados.cliente.estado);
						$('[name="cep_cliente"]').val(dados.cliente.cep);		
						//$('[name="tipo_cliente"]').val(dados.cliente.cliente_xtpcli);
						$('[name="tipo_cliente"]').val(dados.cliente.cliente_sativ1);
											
						calcularDataProximoPasso(dados.cliente.categoria);
						$('input[name=salvar_formulario]').attr('disabled', false);
						$('#input_cliente_notificacao').html('');
						
					} else {
						$('#input_cliente_notificacao').html('<span style="color: red; padding-top: 4px">Operação não permitida para este cliente</span>');
						$('input[name=salvar_formulario]').attr('disabled', true);
					}
					
					
				}
					
			}, "json");
			
			$.get("<?=site_url('formulario_visita/obter_contatos_cliente')?>/" + codigo_loja_cliente.replace("|", "_"), function(dados) {
				
				if (dados.cliente_encontrado)
				{	
					var contatos_cliente = [];
					
					for(index = 0; index < dados.contatos_cliente.length; ++index) {
						contatos_cliente.push({'nome_contato': dados.contatos_cliente[index].contato_nome});
						contatos_cliente.push({'tipo_contato': ''});
						contatos_cliente.push({'telefone_contato': dados.contatos_cliente[index].contato_fone});
						contatos_cliente.push({'email_contato': dados.contatos_cliente[index].contato_email});
						arrayGrid(contatos_cliente,'add','contatos');
						check_has_contatos();
						contatos_cliente = [];
					}
										
				}
				
			}, "json");
			
		}
		
		$('[name="obra"]').on('click', function() {
		
			if($('[name="obra"]:checked').val() == 'S') {
			
				$('[name="nome_obra"]').attr('disabled', false);
			
			} else {
			
				$('[name="nome_obra"]').val('');
				$('[name="nome_obra"]').attr('disabled', true);
				
			}
		
		});
		
		$('[name="_input_cliente"]').val($('[name="input_cliente"]').val());
		
		//Chamado 002663 - [CUSTOM] Melhoria busca cliente
		var codigo_cliente = "<?php echo $this->input->post('codigo_cliente'); ?>";
		var loja_cliente = "<?php echo $this->input->post('loja_cliente'); ?>";
		if(codigo_cliente && loja_cliente) {
			obter_informacoes_cliente(codigo_cliente + '|' + loja_cliente);
			
			$('[name="telefone_cliente"]').attr('disabled', true);
			$('[name="tipo_cliente"]').attr('disabled', true);
			$('[name="cidade_regiao_cliente"]').attr('disabled', true);
			$('[name="estado_cliente"]').attr('disabled', true);
			$('[name="cep_cliente"]').attr('disabled', true);	
		}
	});
	
</script>