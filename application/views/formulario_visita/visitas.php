﻿<h2>Visitas Realizadas</h2>

<div class="topo_grid">
	<p><strong>Filtros</strong></p>
	
	<?php echo $filtragem; ?>
</div>

<table class="novo_grid">
	<thead>
		<tr>
			<th>ID Formulário</th>
			<th style="<?= !in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) ? 'display: none;' : NULL; ?>">Representante</th>
			<th>Cliente</th>
			<th>Cidade/Estado</th>
			<th>Data Visita</th>
			<th>Ações</th>
		</tr>
	</thead>
	
	<tbody>
		<?php if (!$this->input->get('my_submit')) : ?>
			<tr><td colspan="11" style="color: #900; padding: 10px;"><strong>Realize um filtro.</strong></td></tr>
	<?php else : ?>
		<?php if (!$visitas) : ?>
			
			<tr><td colspan="11" style="color: #900; padding: 10px;"><strong>Nenhuma Visita Encontrada.</strong></td></tr>
			
		<?php else : ?>
			<?php foreach ($visitas as $visita) : ?>
				<tr>
					<td><?= $visita['codigo_formulario'];?></td>
					<td style="<?= !in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros')) ? 'display: none;' : NULL; ?>"><?= htmlentities($visita['nome_representante']);?></td>
					<td><?= $visita['nome_cliente'];?></td>
					<td><?= $visita['cidade_cliente'];?>/<?= $visita['estado_cliente'];?></td>
					<td><?= protheus2data($visita['data_visitacao']);?></td>
					<td><a href="<?= site_url('formulario_visita') ?>/?codigo=<?= $visita['codigo_formulario'] ?>"style="font-size: 25px;" >Editar</a></td>
				</tr>
			<?php endforeach;?>
		<?php endif; ?>
	<?php endif; ?>	
	</tbody>
</table>

<div class="rodape_grid">
	<p><?= number_format($total['quantidade'], 0, ',', '.'); ?> registros encontrados.</p>
	
	<?php echo $paginacao; ?>
</div>