﻿<?(!$pedido['id_pedido'] ? $acao_texto = 'Criar' : $acao_texto = 'Editar' )?>
<?=($pedido['tipo'] != 'orcamento' ? '<h2>' . $acao_texto . ' Pedido</h2>' : '<h2>' . $acao_texto . ' Orçamento</h2>')?>

<div id="etapas_pedidos">
	<?php
		if($pedido['tipo'] == 'orcamento')
		{
			$pagina = "orcamento";
		}
		else
		{
			$pagina = "pedido";
		}
	?>


	<a href="<?=site_url('criar_pedidos/informacoes_cliente/' . $pagina . '/' . $pedido['codigo_usuario'])?>">Infromações do Cliente ></a>
	<a href="<?=site_url('criar_pedidos/adicionar_produtos')?>">Adicionar Produtos ></a>
	<a href="<?=site_url('criar_pedidos/outras_informacoes')?>">Outras Informações ></a>
	<a href="<?=site_url('criar_pedidos/confirmacao')?>" class="selecionado">Finalizar</a>
	
	<div id="linha_posicao" style="width: 65%"></div>
</div>

<div style="clear:both"></div>

<div class="caixa">
	<ul>
		<li><?=anchor(site_url('criar_pedidos/outras_informacoes'), 'Voltar')?></li>
	</ul>
</div>

<?php 
	echo form_open(current_url()); 
?>

<div style="background-color: #e2e4ff; border: 1px solid #aaaaaa; padding: 0 10px 10px 10px; width: 600px; margin: 5px 5px 5px 0">
	<p>
		<span style="width:150px">Representante:</span> <b><?=$usuario->nome?></b>
	</p>
</div>

<div style="background-color: #e2e4ff; border: 1px solid #aaaaaa; padding: 0 10px 10px 10px; width: 600px; margin: 5px 5px 5px 0">
	<?php
		if($pedido['nome_prospect']) {
	?>
	<p>
		Prospect: <b><?=$pedido['nome_prospect']?></b>
	</p>
	<?php
		}
		else
		{
	?>
		<p>Cliente: <b><?=$pedido['_codigo_loja_cliente']?></b></p>
	<?php
		}
	?>
</div>
<?=br();?>
<div class="ajuda"><p>
Produtos com <b>Descontos</b> destacados em <b style="color:#f00">vermelho</b> estão sujeitos a aprovação financeira.
</p></div>

<?php
//Lista de Proutos
$lista_produtos .= '<table style="width: 100%;"><tr><td style="width: 50%" valign="top">';
$lista_produtos .= '<h3 style="color: #046380">Pedido Imediato</h3>';

//* Lista de Produtos
$lista_produtos .= '<div style="padding: 0 10px 0 0; width: 100%;"><table class="novo_grid" style="margin-top: 10px; width: 100%;" >
	<thead>
		<!-- <th>Item</th> -->
		<th>Código</th>
		<th>Descrição</th>
		<!-- <th>U.M.</th> -->
		<!-- <th>Tabela</th> -->
		<th>Preço Unit. (R$)</th>
		<th>Quant.</th>
		<th>Desc. (%)</th>
		<!--<th>Total (R$)</th>-->
		<th>IPI (%)</th>
		<th>Total c/ IPI (R$)</th>
		<!-- <th>ST (R$)</th> -->
		<!-- <th>Peso Total (KG)</th> -->
		
	</thead>
	
	<tbody>';
	
if($pedido['produtos'])
{

	$produtos = array_reverse($pedido['produtos']);

	
	$item = count($produtos);
	foreach($produtos as $indice => $produto)
	{
		$item--;

		$lista_produtos .= '<tr>
			<!-- <td class="center">' . str_pad($item, 2, 0, STR_PAD_LEFT) . '</td> -->
			<td class="center">' . $produto['codigo'] . '</td>
			<td>' . $produto['descricao'] . '</td>
			<!-- <td>' . $produto['unidade_medida'] . '</td> -->
			<!-- <td>' . $produto['tabela_precos']['descricao'] . '</td> -->
			<td class="right">' . number_format($produto['preco'], 2, ',', '.') . '</td>
			
			<td style="text-align:center">' . $produto['quantidade_pedido'] . '</td>
			<td class="right" '.($produto['alerta']=='S'?'style="color:#f00"':'style="color:green"').'>' .number_format(($produto['desconto']?$produto['desconto']:0), 2, ',', '.') . '</td>
			<!--<td class="right">' . number_format($produto['quantidade_pedido'] * ($produto['preco'] - (($produto['desconto']/100)*$produto['preco'])), 2, ',', '.')  . '</td>-->
			<td class="right">' . number_format($produto['ipi'],2,',','.') . '</td>
			<td class="right">' . number_format($produto['quantidade_pedido'] * (($produto['preco']+(($produto['ipi']/100)*$produto['preco'])) - (($produto['desconto']/100)*$produto['preco'])), 2, ',', '.'). '</td>
			<!-- <td class="right">' . number_format(0, 2, ',', '.') . '</td> -->
			<!-- <td class="right">' . number_format($produto['peso'] * $produto['quantidade_pedido'], 0, '', '.')  . '</td> -->
			
		</tr>';

		$valor_total_sd += $produto['quantidade_pedido'] * $produto['preco'];
		$ipi +=(($produto['ipi']/100)*$produto['preco'])*$produto['quantidade_pedido'];
		$desconto_total += (($produto['desconto']/100)*$produto['preco'])*$produto['quantidade_pedido'];
		$valor_total += $produto['quantidade_pedido'] * ($produto['preco'] - (($produto['desconto']/100)*$produto['preco']));
		$valor_total_ipi += $produto['quantidade_pedido'] * (($produto['preco']+(($produto['ipi']/100)*$produto['preco'])) - (($produto['desconto']/100)*$produto['preco']));

	}
	
}
else
{
	$lista_produtos .= '<tr><td colspan="8" align="center">Nenhum pedido imediato.</td></tr>';
}

$lista_produtos .= '</tbody></table></div>';

$lista_produtos .= '<p><strong>Total:</strong> R$ ' . number_format($valor_total, 2, ',', '.') . '</p>';

$lista_produtos .= '</td><td style="width: 50%" valign="top"><div style="padding: 0 0 0 10px; width: 100%;">';
$lista_produtos .= '<h3 style="color: #046380">Pedido Programado</h3>';
// Produtos Programado

	
//* Lista de Produtos

$lista_produtos .= '<table class="novo_grid_verde" style="margin-top: 10px; width: 100%;">
	<thead>
		<!-- <th>Item</th> -->
		<th>Código</th>
		<th>Descrição</th>
		<!-- <th>U.M.</th> -->
		<!-- <th>Tabela</th> -->
		<th>Preço Unit. (R$)</th>
		<th>Quant.</th>
		<th>Desc. (%)</th>
		<!--<th>Total (R$)</th>-->
		<th>IPI (%)</th>
		<th>Total c/ IPI (R$)</th>
		<!-- <th>ST (R$)</th> -->
		<!-- <th>Peso Total (KG)</th> -->
		
	</thead>
	
	<tbody>';

if($pedido['produtos_programados'])
{

	$produtos = array_reverse($pedido['produtos_programados']);
	
	$item = count($produtos);
	foreach($produtos as $indice => $produto)
	{

		$item--;
		
		$lista_produtos .= '<tr>
			<!-- <td class="center">' . str_pad($item, 2, 0, STR_PAD_LEFT) . '</td> -->
			<td class="center">' . $produto['codigo'] . '</td>
			<td>' . $produto['descricao'] . '</td>
			<!-- <td>' . $produto['unidade_medida'] . '</td> -->
			<!-- <td>' . $produto['tabela_precos']['descricao'] . '</td> -->
			<td class="right">' . number_format($produto['preco'], 2, ',', '.') . '</td>
			
			<td style="text-align:center">' . $produto['quantidade_pedido'] . '</td>
			<td class="right" '.($produto['alerta']=='S'?'style="color:#f00"':'style="color:green"').'>' .number_format(($produto['desconto']?$produto['desconto']:0), 2, ',', '.') . '</td>
			<!--<td class="right">' . number_format($produto['quantidade_pedido'] * ($produto['preco'] - (($produto['desconto']/100)*$produto['preco'])), 2, ',', '.')  . '</td>-->
			<td class="right">' . number_format($produto['ipi'],2,',','.') . '</td>
			<td class="right">' . number_format($produto['quantidade_pedido'] * (($produto['preco']+(($produto['ipi']/100)*$produto['preco'])) - (($produto['desconto']/100)*$produto['preco'])), 2, ',', '.'). '</td>
			<!-- <td class="right">' . number_format(0, 2, ',', '.') . '</td> -->
			<!-- <td class="right">' . number_format($produto['peso'] * $produto['quantidade_pedido'], 0, '', '.')  . '</td> -->
			
	
		</tr>';

		$valor_total_sd_programado += $produto['quantidade_pedido'] * $produto['preco'];
		$ipi_programado += (($produto['ipi']/100)*$produto['preco'])*$produto['quantidade_pedido'];
		$desconto_total_programado += (($produto['desconto']/100)*$produto['preco'])*$produto['quantidade_pedido'];
		$valor_total_programado += $produto['quantidade_pedido'] * ($produto['preco'] - (($produto['desconto']/100)*$produto['preco']));
		$valor_total_programado_ipi += $produto['quantidade_pedido'] * (($produto['preco']+(($produto['ipi']/100)*$produto['preco'])) - (($produto['desconto']/100)*$produto['preco']));
		

	}
	
}
else
{
	$lista_produtos .= '<tr><td colspan="8" align="center">Nenhum pedido programado.</td></tr>';
}

$lista_produtos .= '</tbody></table>';

$lista_produtos .= '<p><strong>Total:</strong> R$ ' . number_format($valor_total_programado, 2, ',', '.') . '</p>';

$lista_produtos .= '</div></td><tr></table>';
			
$lista_produtos .= '<p style="border-top: 1px solid #DDDDDD; padding: 10px 0 0 0"><strong>Total sem desconto:</strong> R$ ' . number_format($valor_total_sd + $valor_total_sd_programado, 2, ',', '.') . '</p>';
$lista_produtos .= '<p><strong>Desconto total:</strong> R$ ' . number_format($desconto_total + $desconto_total_programado, 2, ',', '.') . '</p>';
//$lista_produtos .= '<p><strong>Total do pedido:</strong> R$ ' . number_format($valor_total + $valor_total_programado, 2, ',', '.') . '</p>';
$lista_produtos .= '<p><strong>IPI total:</strong> R$ ' . number_format($ipi + $ipi_programado, 2, ',', '.') . '</p>';
$lista_produtos .= '<p style="border-bottom: 1px solid #DDDDDD; padding: 0 0 10px 0"><strong>Total com IPI e desconto:</strong> R$ ' . number_format($valor_total_ipi + $valor_total_programado_ipi, 2, ',', '.') . '</p>';
			
echo $lista_produtos;

?>

<?php 

//Totais

$peso_total_geral = $pedido['peso_total_geral'];
$valor_total= $pedido['valor_total'];
$valor_frete = $pedido['valor_frete'];
$valor_total_ipi = $pedido['valor_total_ipi'];
$valor_total_frete = $pedido['valor_total_frete'];

if($peso_total_geral): 

?>
<!--
<br />
<table class="novo_grid">
	<thead>
		<th>Peso Total (KG)</th>
		<th>Valor do Frete (R$)</th>
		<th>Valor do Frete por KG (R$):</th>
		<th>Valor dos Produtos (R$)</th>
		<th>Valor Total do Pedido (R$)</th>
	</thead>
	
	<tbody>
		<tr>
			<td class="right"><?=number_format($peso_total_geral, 0, '', '.')?></td>
			<td class="right"><?=number_format($valor_frete, 2, ',', '.')?></td>
			<td class="right"><?=number_format($valor_frete / $peso_total_geral, 2, ',', '.')?></td>
			<td class="right"><?=number_format($valor_total_ipi, 2, ',', '.')?></td>
			<td class="right"><strong><?=number_format($valor_total_frete, 2, ',', '.')?></strong></td>
		</tr>
	</tbody>
</table>
-->
<?php endif; ?>


<br />


<div style="background-color: #e2e4ff; border: 1px solid #aaaaaa; padding: 0 10px 10px 10px; width: 600px; margin: 5px 5px 5px 0">
	<p>
		Condição de Pagamento: <b><?=$formas_pagamento?> </b>
	</p>

	<p>
		Tipo de Frete:  <b><?=$pedido['tipo_frete']?> </b>
	</p>
	
	<p>
		Transportadora:  <b><?=$transportadora?> </b>
	</p>
	
	<p>
		Evento:  <b><?=($evento->nome ? $evento->nome : 'Nenhum')?> </b>
	</p>

	<p>
		Ordem de compra do cliente:  <b><?=$pedido['codigo_ordem_compra']?></b>
	</p>
	<?php
	if($pedido['data_entrega'])
	{?>
		<p>
			Data de entrega Pedido Programado:  <b><?=$pedido['data_entrega']?></b>
		</p>
	<?php
	} 
	if($pedido['observacao_pedido_imediato'])
	{
		echo "<p>
				Observação do Ped. Imediato: <br /> <b>".$pedido['observacao_pedido_imediato']."</b>
			</p>";
	}		
	
	if($pedido['observacao_pedido_programado'])
	{
		echo 	"<p>
					Observação do Ped. Programado: <br /> <b>".$pedido['observacao_pedido_programado']."</b>
				</p>";
	}
	?>
	
</div>

<br />

<?php

echo '<p>' . form_submit('confirmar', 'Finalizar', 'id="confirmar"');
echo ' ou <a id="cancelar_pedido" href="#">Cancelar</a></p>';
echo form_close();

?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#cancelar_pedido').click(function(e){
			e.preventDefault();
		
			if (confirm("Tem certeza que deseja cancelar o pedido?"))
			{  
				window.location = "<?=base_url() . 'index.php/criar_pedidos/cancelar_pedido'?>";
			}
		});
	});
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#confirmar').click(function(){
			
			var tipo = "<?=($pedido['tipo'] != 'orcamento' ? 'pedido' : 'orçamento')?>";
		
			if (confirm("Tem certeza que deseja finalizar o " + tipo + "?"))
			{  
				return true;
			}
			else
			{
				return false;
			}
		});
	});
</script>
