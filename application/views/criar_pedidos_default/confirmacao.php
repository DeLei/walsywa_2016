﻿<?(!$pedido['id_pedido'] ? $acao_texto = 'Cadastrar' : $acao_texto = 'Editar' )?>

<?php
	echo '
			<div class="caixa">
				<ul>
					<li>' . anchor('feiras', 'Voltar', 'onclick="history.go(-1); return false;"') . '</li>
				</ul>
			</div>
		';
?>

<?=($pedido['tipo'] != 'orcamento' ? '<h2>' . $acao_texto . ' Pedido</h2>' : '<h2>' . $acao_texto . ' Orçamento</h2>')?>

<div id="etapas_pedidos">
	<?php
		if($pedido['tipo'] == 'orcamento')
		{
			$pagina = "orcamento";
		}
		else
		{
			$pagina = "pedido";
		}
	?>


	<a href="<?=site_url('criar_pedidos/informacoes_cliente/' . $pagina . '/' . $pedido['codigo_usuario'])?>">Informações do Cliente ></a>
	<a href="<?=site_url('criar_pedidos/adicionar_produtos')?>">Adicionar Produtos ></a>
	<a href="<?=site_url('criar_pedidos/outras_informacoes')?>">Outras Informações ></a>
	<a href="<?=site_url('criar_pedidos/confirmacao')?>" class="selecionado">Finalizar</a>
	
	<div id="linha_posicao" style="width: 65%"></div>
</div>

<div style="clear:both"></div>
<!--
<div class="caixa">
	<ul>
		<li><?=anchor(site_url('criar_pedidos/outras_informacoes'), 'Voltar')?></li>
	</ul>
</div>
-->

<?php 
	echo form_open(current_url()); 
?>

<div style="background-color: #e2e4ff; border: 1px solid #aaaaaa; padding: 0 10px 10px 10px; width: 600px; margin: 5px 5px 5px 0">
	<p>
		<span style="width:150px">Representante:</span> <b><?=$usuario->nome?></b>
	</p>
</div>

<!--
<div style="background-color: #e2e4ff; border: 1px solid #aaaaaa; padding: 0 10px 10px 10px; width: 600px; margin: 5px 5px 5px 0">
	<p>
		<span style="width:150px">Unidade:</span>  <b><?=$pedido['unidade']?> - <?=$pedido['descricao_unidade']?></b>
	</p>
</div>
-->

<div style="background-color: #e2e4ff; border: 1px solid #aaaaaa; padding: 0 10px 10px 10px; width: 600px; margin: 5px 5px 5px 0">
	<p>
		<span style="width:150px">Tipo de Pedido:</span>  <b><?=$pedido['tipo_pedido']?> - <?=$pedido['nome_tipo_pedido']?></b>
	</p>
</div>

<div style="background-color: #e2e4ff; border: 1px solid #aaaaaa; padding: 0 10px 10px 10px; width: 600px; margin: 5px 5px 5px 0">
	<?php
		if($pedido['nome_prospect']) {
	?>
	<p>
		Prospect: <b><?=$pedido['nome_prospect']?></b>
	</p>
	<?php
		}
		else
		{
	?>
		<p>Cliente: <b><?=$pedido['_codigo_loja_cliente']?></b></p>
	<?php
		}
	?>
</div>

<!--
<div style="background-color: #e2e4ff; border: 1px solid #aaaaaa; padding: 0 10px 10px 10px; width: 600px; margin: 5px 5px 5px 0">
	<p>
		<span style="width:150px">Tabela de preços:</span>  <b><?=$tabela_precos['codigo']?> - <?=$tabela_precos['descricao']?></b>
	</p>
</div>
-->
<?php


//Lista de Proutos
$produtos = array_reverse($produtos);

if($produtos)
{
	
	$lista_produtos .= '<table class="novo_grid" style="margin-top: 20px">
					<thead>
							<th>Item</th>
							<th>Código</th>
							<th>Descrição</th>
							<th>U.M.</th>
							<th>Tabela</th>
							<th>Preço Unit. (R$)</th>
							<th>Quant.</th>
							<th>Total (R$)</th>
							<th>IPI (%)</th>
							<th>Total c/ IPI (R$)</th>
							<th>ST (R$)</th>
							<th>Desconto (R$)</th>
							<th>Total Geral (R$)</th>
					</thead>
					
					<tbody>';
	
	$item = count($produtos);
	foreach($produtos as $indice => $produto)
	{
		

		$lista_produtos .= '<tr>
			<td class="center">' . str_pad($item--, 2, 0, STR_PAD_LEFT) . '</td>
			<td class="center">' . $produto['codigo'] . '</td>
			<td>' . $produto['descricao'] . '</td>
			<td>' . $produto['unidade_medida'] . '</td>
			<td>' . $produto['tabela_precos']['descricao'] . '</td>
			<td class="right">' . number_format($produto['preco'], 2, ',', '.') . '</td>
			
			<td class="center">' . $produto['quantidade_pedido'] . '</td>
			
			<td class="right">' . number_format($produto['preco'] * $produto['quantidade_pedido'], 2, ',', '.')  . '</td>
			<td class="right">' . $produto['ipi'] . '</td>
			<td class="right">' . number_format($produto['total_ipi'], 2, ',', '.') . '</td>
			<td class="right">' . number_format($produto['valor_st'], 2, ',', '.') . '</td>
			<td class="right">' . number_format($produto['valor_desconto'], 2, ',', '.') . '</td>
			<td class="right">' . number_format(($produto['total_ipi'] + $produto['valor_st']) - $produto['valor_desconto'], 2, ',', '.') . '</td>
		</tr>';

	}
	
	
	$lista_produtos .= '</tbody>
	</table>';
	
	echo $lista_produtos;
}

?>

<?php 

//Totais
$peso_total_geral = $pedido['peso_total_geral'];
$valor_total= $pedido['valor_total'];
$valor_frete = $pedido['valor_frete'];
$valor_total_ipi = $pedido['valor_total_ipi'];
$valor_total_frete = $pedido['valor_total_frete'];

$total_st = $pedido['total_st'];
$total_desconto = $pedido['total_desconto'];

?>

<br />
<table class="novo_grid">
	<thead>
		<th>Valor total da ST (R$)</th>
		<th>Valor total do desconto (R$)</th>
		<th>Valor Total do Pedido (R$)</th>
	</thead>
	
	<tbody>
		<tr>
			<td class="right"><?=number_format($total_st, 2, ',', '.')?></td>
			<td class="right"><?=number_format($total_desconto, 2, ',', '.')?></td>
			<td class="right"><strong><?=number_format($valor_total, 2, ',', '.')?></strong></td>
		</tr>
	</tbody>
</table>


<br />


<div style="background-color: #e2e4ff; border: 1px solid #aaaaaa; padding: 0 10px 10px 10px; width: 600px; margin: 5px 5px 5px 0">
	
	<?php
		if($pedido['autorizado'])
		{
	?>
	<p>
		Pedido autorizado por: <b><?=$pedido['autorizado']?> </b>
	</p>
	<?php
		}
	?>
	
	<p>
		Condição de Pagamento: <b><?=$formas_pagamento?> </b>
	</p>

	<p>
		Tipo de Frete:  <b><?=$pedido['tipo_frete']?> </b>
	</p>
	
	<p>
		Transportadora:  <b><?=$transportadora?> </b>
	</p>
	
	<p>
		Evento:  <b><?=($evento->nome ? $evento->nome : 'Nenhum')?> </b>
	</p>
	<!--
	<p>
		<?php
		
			$cultura = array(
				'1' => 'Arroz',
				'2' => 'Banana',
				'3' => 'Hortifruti',
				'4' => 'Milho',
				'5' => 'Outros'
			);
		
		?>
	
		Cultura:  <b><?=$cultura[$pedido['cultura']]?> </b>
	</p>
-->
	<p>
		Ordem de compra do cliente:  <b><?=$pedido['codigo_ordem_compra']?></b>
	</p>
	
	<p>
		Data de entrega prevista:  <b><?=$pedido['data_entrega']?></b>
	</p>

	<p>
		Observação do Pedido: <br /> <b><?=$pedido['observacao_pedido_imediato']?></b>
	</p>
</div>

<br />

<?php

echo '<p>' . form_submit('confirmar', 'Finalizar', 'id="confirmar"');
echo ' ou <a id="cancelar_pedido" href="#">Cancelar</a></p>';
echo form_close();

?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#cancelar_pedido').click(function(e){
			e.preventDefault();
		
			if (confirm("Tem certeza que deseja cancelar o pedido?"))
			{  
				window.location = "<?=base_url() . 'index.php/criar_pedidos/cancelar_pedido'?>";
			}
		});
	});
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#confirmar').click(function(){
			
			var tipo = "<?=($pedido['tipo'] != 'orcamento' ? 'pedido' : 'orçamento')?>";
		
			if (confirm("Tem certeza que deseja finalizar o " + tipo + "?"))
			{  
				return true;
			}
			else
			{
				return false;
			}
		});
	});
</script>
