
<h2>Pendências</h2>

<p><?= anchor('pendencias/criar', 'Nova Pendência'); ?></p>

<div class="ajuda"><p>Clicando em "+ Opções" você pode <strong>visualizar e enviar mensagens, e solicitar o encerramento das pendências.</strong></p></div>

<div class="topo_grid">
    <p><strong>Filtros</strong></p>
    <?php echo $filtragem; ?>
</div>

<table class="novo_grid">
    <thead>
        <tr>
            <th>Status</th>
            <th>Anexos</th>
            <th>Criação</th>
            <th>Título</th>
            <th>Prazo Solução</th>
            <th>Prioridade</th>
            <th>De</th>
            <th>Para</th>
            <th>Última Mensagem</th>
            <th>Opções</th>
        </tr>
    </thead>

    <tbody>
	<?php if (!$this->input->get('my_submit')) : ?>
				<tr><td colspan="10" style="color: #900; padding: 10px;"><strong>Realize um filtro.</strong></td></tr>
	<?php else : ?>
		<?php if ($pendencias) : ?>
			<?php foreach ($pendencias as $pendencia) : ?>
				<tr>
					<td class='center'><?= img(base_url() . 'misc/imagens/pendencias/status_' . $pendencia->status . '.png') ?></td>
					<td class='center'><?= ($pendencia->uploads?$pendencia->uploads : 'não há') ?></td>
					<td class='center'><?= date('d/m/Y', $pendencia->timestamp) ?></td>
					<td><?= $pendencia->titulo ?></td>
					<td class='center' <?php 
					if($pendencia->prazo_solucao){
					/*$data = explode('/', $pendencia->prazo_solucao);
					$timestamp_da_data = mktime(0, 0, 0, $data[1], $data[0], $data[2]);*/
					if ($pendencia->prazo_solucao_timestamp < time()) {
					echo 'style="color: #F00;"';
				}} ?>><?= $pendencia->prazo_solucao; ?></td>
					<td><?= ucwords($pendencia->prioridade); ?></td>
					<td><?= $pendencia->id_usuario ?></td>
					<?php 
						$para = explode(',',$pendencia->para);
					?>
					<td><?= (count($para) == 1 ? $para[0] : 'Todos') ?></td>
					<td class='center'><?= ($pendencia->ultima_mensagem_timestamp ? date('d/m/Y H:i:s', $pendencia->ultima_mensagem_timestamp) : 'não há') ?></td>
					<td class='center'><?= anchor('pendencias/ver_detalhes/' . $pendencia->id, '+ Opções') ?></td>
				</tr>
			<?php endforeach; ?>
		<?php else:?>
		   <tr>
			   <td colspan="10"> Não foram listadas pendências.</td>
		   </tr>
		<?php endif; ?>
	<?php endif; ?>	
        </tbody>
    </table>

    <div class="rodape_grid">
	<p><?= number_format($total, 0, ',', '.'); ?> registros encontrados.</p>
	<?= $paginacao; ?>
        </div>
	<table cellpadding="10" cellspacing="10"
		style="border: 1px solid #e5ce32; background-color: #fff">
	
		<tr>
			<td colspan="2"><b>Legenda</b></td>
		</tr>
	
		<tr>
			<td><?= img(base_url() . 'misc/imagens/pendencias/status_em_aberto.png') ?>
			</td>
			<td>Em Aberto</td>
	
			<td><?= img(base_url() . 'misc/imagens/pendencias/status_aguardando_encerramento.png') ?>
			</td>
			<td>Aguardando Encerramento</td>
			
			<td><?= img(base_url() . 'misc/imagens/pendencias/status_encerrada.png') ?>
			</td>
			<td>Encerrada</td>
	
	
		</tr>
	
	</table>

