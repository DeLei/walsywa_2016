<table class="novo_grid">
	<thead>
		<tr>
			<th>Código N.F.</th>
			<th>Parcela</th>
			<th>Status</th>
			<th>Valor (R$)</th>
			<th>Vencimento</th>
			<th>Pagamento</th>
		</tr>
	</thead>
	
	<tbody>
		<?php
			if(!$titulos)
			{
		?>
				<tr>
				<td colspan="6" class='center' style="color: red">Não há</td>
				</tr>
		<?php
			}
			else
			{
		?>
	
		<?php foreach ($titulos as $titulo) : ?>
			
			<tr>
				<td class='right'><?= $titulo['E1_NUM']; ?></td>
				<td class='right'><?= $titulo['E1_PARCELA']; ?></td>
				<td class='center'><?= $titulo['status']; ?></td>
				<td class='right'><?= number_format($titulo['E1_VALOR'], 2, ',', '.'); ?></td>
				<td class='center'><?= ($titulo['E1_VENCTO'] ? date('d/m/Y', strtotime($titulo['E1_VENCTO'])) : '-'); ?></td>
				<td class='center'><?= ($titulo['E1_BAIXA'] ? date('d/m/Y', strtotime($titulo['E1_BAIXA'])) : '-'); ?></td>
			</tr>
			
		<?php endforeach; ?>
		
		<?php
			}
		?>
	</tbody>
</table>