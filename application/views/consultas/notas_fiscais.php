<h2>Consultar Notas Fiscais</h2>

<?php
	if ($_GET['erro']){
		echo '<p class="erro"> Limite de consultas diárias esgotadas. Você só poderá efetuar novas consultas a partir de amanhã. </p>';
	}else if ($erro){
		echo '<p class="erro">' . $erro . '</p>';
	}
?>

<div class="topo_grid">
	<p><strong>Filtros</strong></p>
	
	<?php echo $filtragem; ?>
</div>
<?

//echo $this->session->userdata('grupo_usuario');
//echo ' - grupo';

?>
<table class="novo_grid">
	<thead>
		<tr>
			<!-- Chamado 1374 - Incluído filtro e ordenação de cliente e representante para supervisores -->
			<th>Código N.F.</th>
			<th style="<?= !in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores')) ? 'display: none;' : NULL; ?>">Representante</th>
			<th>Cliente</th>
			<th>Emissão</th>
			<th>Total da Mercadoria(R$)</th>
			<th>Tipo NF</th>
			<th>Valor s/ Impostos</th>
			<th>Frete</th>
			<th>Transp.</th>
			<th>Tel. Transp.</th>
			<th colspan="2">Opções</th>
		</tr>
	</thead>
	
	<tbody>
	<?php if (!$this->input->get('my_submit')) : ?>
			<tr><td colspan="11" style="color: #900; padding: 10px;"><strong>Realize um filtro.</strong></td></tr>
	<?php else : ?>
		<?php if (!$nfs) : ?>
			
			<tr><td colspan="11" style="color: #900; padding: 10px;"><strong>Nenhuma nota fiscal encontrada.</strong></td></tr>
			
		<?php else : ?>
			
			<?php foreach ($nfs as $nf) : ?>
				
				<tr>
					<td  class='right'><?= $nf['F2_DOC']; ?></td>
					<td style="<?= !in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores')) ? 'display: none;' : NULL; ?>"><?= utf8_encode($nf['A3_NOME']); ?></td>
					<td><?= utf8_encode($nf['A1_NOME']); ?></td>
					<td  class='center'><?= date('d/m/Y', strtotime($nf['F2_EMISSAO'])); ?></td>
					<td class='right'><?= number_format($nf['F2_VALMERC'], 2, ',', '.'); ?></td>
					<td class='right'><?= number_format($nf['F2_VALICM'], 2, ',', '.'); ?></td>
					<td class='right'><?= number_format($nf['F2_VALIPI'], 2, ',', '.'); ?></td>
					<td class='center'><?= $nf['F2_TPFRETE'] ? 'CIF' : 'FOB'; ?></td>
					<td><?= utf8_encode($nf['A4_NOME']); ?></td>
					<td class='center'><?= $nf['A4_TEL']; ?></td>
					<td class='center' style="font-size:12px;"><a href="#" onclick="$(this).colorbox({ href: '<?= site_url('consultas/titulos') ?>/?codigo_nf=<?= $nf['F2_DOC'] ?>&cod_filial=<?= $nf['F2_FILIAL'] ?>&serie=<?= $nf['F2_SERIE'] ?>&cliente=<?= $nf['F2_CLIENTE'] ?>&loja=<?= $nf['F2_LOJA'] ?>' }); return false;">Ver Títulos</a></td>
					<td class='center' style="font-size:12px;"><a href="<?= site_url('consultas/obter_itens') ?>/<?= $nf['F2_DOC'] ?>/<?= $nf['F2_CLIENTE']?>/<?=$nf['F2_LOJA']?>">Ver Itens</a></td>
				</tr>
				
			<?php endforeach; ?>
			
		<?php endif; ?>
	<?php endif; ?>	
	</tbody>
</table>

<div class="rodape_grid">
	<p><?= number_format($total->QTD, 0, ',', '.'); ?> registros encontrados com o valor total de R$ <?= number_format($total->TOT_SIMP, 2, ',', '.'); ?>.</p>
	
	<?php echo $paginacao; ?>
</div>
