<?php
	echo '
		<div class="caixa">
			<ul>
				<li>' . anchor('clientes', 'Voltar', 'onclick="history.go(-1); return false;"') . '</li>
			</ul>
		</div>
	';
?>

<h2>Consultar Títulos Vencidos</h2>

<?php
	if ($erro){
		echo '<p class="erro">' . $erro . '</p>';
	}
?>

<div class="topo_grid">
	<p><strong>Filtros</strong></p>
	
	<?php echo $filtragem; ?>
</div>

<table class="novo_grid">
	<thead>
		<tr>
			<th>Cliente</th>
			<?php //Merge com conteúdo Piloto ?>
			<th style="<?= !in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores')) ? 'display: none;' : NULL; ?>">Representante</th>
			<th>Código N.F.</th>
			<th>Parcela</th>
			<th>Status</th>
			<th>Valor (R$)</th>
			<th>Vencimento</th>
		</tr>
	</thead>
	
	<tbody>
	<?php if (!$this->input->get('my_submit')) : ?>
			<tr><td colspan="10" style="color: #900; padding: 10px;"><strong>Realize um filtro.</strong></td></tr>
	<?php else : ?>
		<?php if (!$titulos) : ?>
			
			<tr><td colspan="10" style="color: #900; padding: 10px;"><strong>Nenhum título encontrado.</strong></td></tr>
			
		<?php else : ?>
			
			<?php foreach ($titulos as $titulo) : ?>
				
				<tr>
					<td><?= utf8_encode($titulo['A1_NOME']); ?></td>
					<td style="<?= !in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros', 'supervisores')) ? 'display: none;' : NULL; ?>"><?= utf8_encode($titulo['A3_NOME']); ?></td>
					<td class='right'><?= $titulo['E1_NUM']; ?></td>
					<td class='right'><?= $titulo['E1_PARCELA']; ?></td>
					<td class='center'><?= $titulo['status']; ?></td>
					<td  class='right'><?= number_format($titulo['E1_VALOR'], 2, ',', '.'); ?></td>
					<td class='center'><?= ($titulo['E1_VENCTO'] ? date('d/m/Y', strtotime($titulo['E1_VENCTO'])) : '-'); ?></td>
				</tr>
				
			<?php endforeach; ?>
			
		<?php endif; ?>
	<?php endif; ?>	
	</tbody>
</table>

<div class="rodape_grid">
	<p><?= number_format($total['quantidade'], 0, ',', '.'); ?> registros encontrados com o valor total de R$ <?= number_format($total['valor'], 2, ',', '.'); ?>.</p>
	
	<?php echo $paginacao; ?>
</div>
