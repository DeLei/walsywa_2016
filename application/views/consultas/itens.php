<div id="espelho_pedido">

<h2>Nota Fiscal <?php echo $nota_fiscal['codigo_nota_fiscal'] . '/' . $nota_fiscal['serie_nota_fiscal'] ?> </h2>

<div class="caixa nao_exibir_impressao">
	<ul>
		<li><?=anchor(site_url('consultas/notas_fiscais'), 'Voltar')?></li>
		<li style="background: url(<?=base_url() . 'third_party/img/printer1.png'?>) no-repeat;"><a href="#" onclick="print(); return false;">Imprimir</a></li>
	</ul>
</div>

<?php
	//echo '<pre>';
	//print_r($cliente);
	//echo '</pre>';
?>

<h4 style="margin-top: 15px;">Destinatário/Remetente</h4>
<table class="info">

	<tr>
		<th>Representante:</th>
		<td><?=utf8_encode($representante['nome'])?></td>
	</tr>
	
	<tr>
		<th>Razão Social/Nome:</th>
		<td><?=utf8_encode($cliente['nome'])?></td>
		
		<th>CNPJ/CPF:</th>
		<td><?=$cliente['cpf']?></td>
		
		<th>Inscrição estadual:</th>
		<td><?=$cliente['rg']?></td>
	</tr>

	<tr>
		<th>Município:</th>
		<td><?=utf8_encode($cliente['cidade'])?></td>
		
		<th>Bairro:</th>
		<td><?=utf8_encode($cliente['bairro'])?></td>
		
		<th>UF:</th>
		<td><?=$cliente['estado']?></td>
	</tr>
	<tr>
		<th>Emissão:</th>
		<td><?=date('d/m/Y', strtotime($nota_fiscal['data_emissao'])).' - '.$nota_fiscal['hora_emissao']?></td>
		
		<th>Transportadora:</th>
		<td><?=utf8_encode($transportadora['nome'])?></td>
		
		<th>Tel. Transp:</th>
		<td><?=$transportadora['telefone']?></td>
	</tr>
	<tr>
		<th>CEP:</th>
		<td colspan="5"><?=$cliente['cep']?>
		</td>
	</tr>
	
</table>

<div style="clear:both"></div>
<br />
<h4 style="margin-top: 15px;">Cálculo do Imposto</h4>
<table class="info">
	<tr>
		<th>Valor ICMS:</th>
		<td><?=number_format(doubleval($nota_fiscal['valor_icms']), 2, ',', '.')?></td>
		
		<th>Valor ICMS ST:</th>
		<td><?=number_format(doubleval($nota_fiscal['valor_icms_st']), 2, ',', '.')?></td>
		
		<th>Base de Cálculo de ICMS:</th>
		<td><?=number_format(doubleval($nota_fiscal['base_calculo_icms']), 2, ',', '.')?></td>
		
		<th>Base de Cal. ICMS ST:</th>
		<td><?=number_format(doubleval($nota_fiscal['base_calculo_icms_st']), 2, ',', '.')?></td>
	</tr>
	
	<tr>
		<th>Valor total do produto:</th>
		<td><?=number_format(doubleval($nota_fiscal['valor_total_produto']), 2, ',', '.')?></td>
		
		<th>Valor do frete:</th>
		<td><?=number_format(doubleval($nota_fiscal['valor_frete']), 2, ',', '.')?></td>
		
		<th>Valor do seguro:</th>
		<td><?=number_format(doubleval($nota_fiscal['valor_seguro']), 2, ',', '.')?></td>
		
		<th>Valor do desconto:</th>
		<td><?=number_format(doubleval($nota_fiscal['valor_desconto']), 2, ',', '.')?></td>
	</tr>
	
	<tr>
		<th>Valor do IPI:</th>
		<td><?=number_format(doubleval($nota_fiscal['valor_ipi']), 2, ',', '.')?></td>
		
		<th>Valor total da nota:</th>
		<td><?=number_format(doubleval($nota_fiscal['valor_total']), 2, ',', '.')?></td>
		
		<th>Despesas necessárias:</th>
		<td><?=number_format(doubleval($nota_fiscal['despesas_necessarias']), 2, ',', '.')?></td>
	</tr>
	
</table>


<div style="clear:both"></div>
<br />

<h4 style="margin-top: 15px;">Dados dos Produtos Transportados</h4>
<br />
<table class="novo_grid info largura_tabela">
	<thead>
		<tr>
			<th width="64">Código</th>
			<th width="168">Descrição</th>

			<th width="40">U.M.</th>
			<!--<th>Qtd. Entregue</th>-->
			<th width="104">Qtd. Vendida</th>
			<th width="123">Preço Unitário</th>
			<th width="38">IPI</th>
			<th width="38">ST</th>

			<!-- -------------------- -->
			<th width="135">Base de Cál. ICMS</th>
			<th width="94">Valor ICMS</th>
			<th width="76">Liq. ICMS</th>
			<!-- -------------------- -->

			<th width="103">Valor Total</th>
		</tr>
	</thead>
	
	<tbody>
		<?php if (!$itens) : ?>
			
			<tr><td colspan="10" style="color: #900; padding: 10px;"><strong>Nenhum título encontrado.</strong></td></tr>
			
		<?php else :
			$quant_entregue = 0;
			$quant_vendida  = 0;
			$total_faturado = 0;
		?>
			<?php
				foreach ($itens as $item) :
				$quant_entregue += $item['quantidade_entrega'];
				$quant_vendida  += $item['quantidade_vendida_produto'];
				$total_faturado += $item['total_faturado'];
			?>
				<tr>
					<td class='center'><?=$item['codigo_nota_fiscal']; ?></td>
					<td class='left'><?=utf8_encode($item['descricao']); ?></td>
					
					<td class='center'><?=$item['unidade_medida']; ?></td>
					<td class='right'><?=$item['quantidade_vendida_produto']; ?></td>
					<td class='right'><?=number_format($item['preco_unitario'], 2, ',', '.'); ?></td>
					<td class='right'><?=number_format($item['ipi'], 2, ',', '.'); ?></td>
					<td class='right'><?=number_format($item['st'], 2, ',', '.'); ?></td>
					<td class='right'><?=number_format($item['base_calc_icms'], 2, ',', '.'); ?></td>
					<td class='right'><?=number_format($item['valor_icms'], 2, ',', '.'); ?></td>
					<td class='right'><?=number_format($item['aliq_icms'], 2, ',', '.'); ?></td>
					<td class='right'><?= number_format($item['total_faturado'], 2, ',', '.'); ?></td>
				</tr>
				
			<?php endforeach; ?>
			
		<?php endif; ?>
		
	</tbody>
</table>

<!-- --------------------------------------------- -->

<div style="clear:both"></div>

	<h4 style="margin-top: 15px;">Totais</h4>

	<table class="novo_grid info largura_tabela">
		<thead>
			<tr>
			<th>CONDIÇÃO DE PAGAMENTO</th>
			<th>QTD. VENDIDA</th>
			<th>TOTAL FATURADO</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="text-align: right;">
					<?=number_format($nota_fiscal['condicao_pagamento'], 2, ',', '.')?>
				</td>
				<td style="text-align: right;">
					<?=number_format($quant_vendida, 2, ',', '.')?>
				</td>
				<td style="text-align: right;">
					<?=number_format($nota_fiscal['valor_total'], 2, ',', '.')?>
				</td>
			</tr>
		</tbody>
	</table>

<!-- --------------------------------------------- -->

</div>
