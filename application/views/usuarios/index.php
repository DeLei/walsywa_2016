<h2>Usuários</h2>

<p><?= anchor('usuarios/criar', 'Cadastrar Novo Usuário'); ?></p>

<div class="topo_grid">
	<p><strong>Filtros</strong></p>
	
	<?php echo $filtragem; ?>
</div>

<table class="novo_grid">
	<thead>
		<tr>
			<th>Código</th>
			<th>Grupo</th>
			<th>Nome</th>
			<th>Criação</th>
			<th>Status</th>
			<th>Opções</th>
		</tr>
	</thead>
	
	<tbody>
	<?php if (!$this->input->get('my_submit')) : ?>
				<tr><td colspan="10" style="color: #900; padding: 10px;"><strong>Realize um filtro.</strong></td></tr>
	<?php else : ?>
		<?php if (!$usuarios) : ?>
			
			<tr><td colspan="8" style="color: #900; padding: 10px;"><strong>Nenhum usuário encontrado.</strong></td></tr>
			
		<?php else : ?>
			
			<?php foreach ($usuarios as $usuario) : ?>
				
				<tr>
					<td class="right"><?= ($usuario->codigo ? $usuario->codigo : '-'); ?></td>
					<td><?= element($usuario->grupo, $grupos); ?></td>
					<td><?= $usuario->nome; ?></td>
					<td class="center"><?= date('d/m/Y', $usuario->timestamp); ?></td>
					<td class="center"><?= element($usuario->status, $statuses); ?></td>
					<td class="center"><?= anchor('usuarios/editar/' . $usuario->id, 'Editar') . ' | ' . anchor('relatorios/ver_acessos/' . $usuario->id, 'Ver Acessos') . ' | ' . anchor('usuarios/liberarAcesso/' . $usuario->id, 'Liberar Acesso') ; ?></td>
				</tr>
				
			<?php endforeach; ?>
			
		<?php endif; ?>
	<?php endif; ?>	
	</tbody>
</table>

<div class="rodape_grid">
	<p><?= number_format($total, 0, ',', '.'); ?> registros encontrados.</p>
	
	<?php echo $paginacao; ?>
</div>
