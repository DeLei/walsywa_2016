<h2>Notícias</h2>
<?if (in_array($this->session->userdata('grupo_usuario'), array('gestores_comerciais', 'gestores_financeiros'))): ?>
<p><?=anchor('noticias/criar', 'Cadastrar Nova Notícia')?></p>
<?endif;?>
<div class="topo_grid">
		<p><strong>Filtros</strong></p>
		<?php echo $filtragem; ?>
</div>
<table cellspacing="0" class="novo_grid">
	<thead>
		<tr>
			<th width='10%'>Status</th>
			<th width='20%'>Criação</th>
			
			<th width='50%'>Título</th>
			<th width='20%'>Opções</th>
		</tr>
	</thead>
	<tbody>
	<?php if (!$this->input->get('my_submit')) : ?>
		    <tr><td colspan="10" style="color: #900; padding: 10px;"><strong>Realize um filtro.</strong></td></tr>
	<?php else : ?>
		<?php if ($noticias) : ?>		
			<?php foreach ($noticias as $noticia): ?>
							
			<tr>
				<td class='center'><?= img(base_url() . 'misc/imagens/noticias/'.$noticia->status.'.png') ?></td>
				<td class='center'><?= date('d/m/Y H:i:s', $noticia->timestamp)?></td>
				
				<td><?= $noticia->titulo ?></td>
				<td class='center'><?= anchor('noticias/ver_detalhes/' . $noticia->id, 'Ver Detalhes', 'class="colorbox"'). $noticia->link_imagem; ?></td>
			</tr>
			<?php endforeach; ?>
		<?php else:?>
		<tr>
			<td colspan="4">Não foram listadas notícias.</td>
		</tr>
		<?php endif; ?>
	<?php endif; ?>
	</tbody>
</table>

<div class="rodape_grid">
	<p><?= number_format($total, 0, ',', '.'); ?> registros encontrados.</p>
	<?= $paginacao; ?>
</div>
<table cellpadding="10" cellspacing="10" style="border: 1px solid #e5ce32; background-color: #fff">
					
					<tr>
						<td colspan="2"><b>Legenda</b></td>
					</tr>
					
					<tr>
						<td><?= img(base_url() . 'misc/imagens/noticias/ativa.png') ?> </td>
						<td>Ativa</td>
						
						<td><?= img(base_url() . 'misc/imagens/noticias/inativa.png') ?> </td>
						<td>Inativa</td>
					
						
					</tr>
					
				</table>
