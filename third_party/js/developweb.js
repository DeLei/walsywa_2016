$(document).ready(function() {
	$('.novo_grid tbody tr:nth-child(even)').css('backgroundColor', '#fff');
	$('.novo_grid tbody tr:nth-child(odd)').css('backgroundColor', '#e2e4ff');
	$('table.hnordt tbody tr:nth-child(even)').css('backgroundColor', '#e7f1f6');
	
	$('input:text[class!=autocomplete]').setMask();
	
	$('#navegacao li a + ul').hide();
	$('#navegacao li').hover(function() { 
		$(this).children('ul').show(); 
		
		$('.submenu').css({
			'margin-top': '-25px',
			'left': (parseInt($(this).children('ul').css("width")) - 10) + 'px'
		});
	}, function() { 
		$(this).children('ul').hide(); 
	});
	
	$('input[alt=cep]').keyup(function() {
		if ($(this).val().length == 9)
		{
			if ($(this).attr("name") == "cep")
			{
				$.getScript('http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep=' + $(this).val(), function() {  
					if (resultadoCEP['resultado']) {
						$('input[name=endereco]').val(unescape(resultadoCEP['tipo_logradouro']) + ': ' + unescape(resultadoCEP['logradouro']));
						$('input[name=bairro]').val(unescape(resultadoCEP['bairro']));
						$('input[name=cidade]').val(unescape(resultadoCEP['cidade']));
						$('select[name=estado]').val(unescape(resultadoCEP['uf']));
						
						$('input[name=numero]').focus();
					}
				});
			}
			else if ($(this).attr("name") == "cep_cobranca")
			{
				$.getScript('http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep=' + $(this).val(), function() {  
					if (resultadoCEP['resultado']) {
						$('input[name=endereco_cobranca]').val(unescape(resultadoCEP['tipo_logradouro']) + ': ' + unescape(resultadoCEP['logradouro']));
						$('input[name=bairro_cobranca]').val(unescape(resultadoCEP['bairro']));
						$('input[name=cidade_cobranca]').val(unescape(resultadoCEP['cidade']));
						$('select[name=estado_cobranca]').val(unescape(resultadoCEP['uf']));
						
						$('input[name=numero_cobranca]').focus();
					}
				});
			}
		}
	});

//CUSTOM: MPD-220_Walsywa_01_05082013 - 09/08/2013
        $('.somente_numeros_e_virgula').blur(function()
	{
            $('.somente_numeros_e_virgula').formatCurrency({
			symbol: '',
			decimalSymbol: '.',
			digitGroupSymbol: '.',
			roundToDecimalPlace: 5,
			positiveFormat: '%s%n',
			negativeFormat: '-%s%n',
		});
	}).keyup(function (evento) {
            $(this).val($(this).val().replace(/[^0-9,.]/gi, ''));
	});
//FIM CUSTOM: MPD-220_Walsywa_01_05082013 - 09/08/2013
                    
	$('.colorbox').colorbox();
	$('.colorbox_inline').each(function() { $(this).colorbox({ inline: true, href: $(this).attr('href') }); });
	
	var datas = $('.datepicker').datepicker({
		dateFormat: 'dd/mm/yy',
		onSelect: function(data_selecionada) {
			datas.not(this).datepicker('option', $(this).hasClass('data_inicial') ? 'minDate' : 'maxDate', $.datepicker.parseDate($(this).data("datepicker").settings.dateFormat || $.datepicker._defaults.dateFormat, data_selecionada, $(this).data("datepicker").settings));
		}
	});
	
	$('.datepicker_data_minima_hoje').datepicker({
		dateFormat: 'dd/mm/yy',
		minDate: new Date()
	});
	
	$('.datepicker_data_minima_inicio').datepicker({
		dateFormat: 'dd/mm/yy',
		minDate: subtrairDias(new Date, 365),
		onSelect: function(data_selecionada) {
			datas.not(this).datepicker('option', $(this).hasClass('data_inicial') ? 'minDate' : 'maxDate', $.datepicker.parseDate($(this).data("datepicker").settings.dateFormat || $.datepicker._defaults.dateFormat, data_selecionada, $(this).data("datepicker").settings));
		}
	});
	
	$('.datepicker_data_minima_final').datepicker({
		dateFormat: 'dd/mm/yy',
		minDate: subtrairDias(new Date, 365),
		onSelect: function(data_selecionada) {
			datas.not(this).datepicker('option', $(this).hasClass('data_inicial') ? 'minDate' : 'maxDate', $.datepicker.parseDate($(this).data("datepicker").settings.dateFormat || $.datepicker._defaults.dateFormat, data_selecionada, $(this).data("datepicker").settings));
		}
	});
	
	$('.datepicker_tres_dias').datepicker({
		dateFormat: 'dd/mm/yy',
		minDate: subtrairDias(new Date, 3),
		maxDate: somarDias(new Date, 3)
	}).keypress(function(event) {
		event.preventDefault();
	});
	
	$('.datepicker_formulario_visita').datepicker({
		dateFormat: 'dd/mm/yy',
		minDate: new Date()
	}).keypress(function(event) {
		event.preventDefault();
	});
	
	Highcharts.setOptions({
		credits: {
			enabled: false
		},
		lang: {
			weekdays: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
			months: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']
		},
		title: {
			text: ' '
		},
		subtitle: {
			text: ' '
		},
		yAxis: {
			title: ' ',
			labels: {
				formatter: function() {
					if (!this.value)
					{
						return 0;
					}
					else if (this.value > 1000000)
					{
						return (this.value / 1000000) + ' milhões';
					}
					else if (this.value == 1000000)
					{
						return (this.value / 1000000) + ' milhão';
					}
					else
					{
						return (this.value / 1000) + ' mil';
					}
				}
			}
		}
	});
	
	$('textarea.editor').ckeditor(function() {}, {
		toolbar: [
			['Font', 'FontSize'],
			['Bold', 'Italic', 'Underline', 'Strike'],
			['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
			['NumberedList', 'BulletedList'],
			['Outdent', 'Indent'],
			['TextColor', 'BGColor'],
			['Link', 'Unlink']
		],
		format_tags: 'p;h2;h3;h4',
		font_defaultLabel: 'Verdana',
		fontSize_defaultLabel: 12,
		toolbarCanCollapse: false,
		removePlugins: 'elementspath',
		resize_enabled: false
	});
});

function resetar_form(form, reenviar)
{
	var reenviar = reenviar ? true : false;
	
	form.find('input[type=text]').val('');
	
	form.find('select').each(function() {
		$(this).val($(this).find('option:first').val());
	});
	
	if (reenviar)
	{
		form.submit();
	}
}

function subtrairDias(data, dias)
{
	return new Date(data.getTime() - (dias * 24 * 60 * 60 * 1000));
}

function somarDias(data, dias)
{
	return new Date(data.getTime() + (dias * 24 * 60 * 60 * 1000));
}

$.mask.masks = $.extend($.mask.masks,{
    phone_9digitos:{ mask: '(99) 99999-9999'},
	horario:{mask: '29:59'}
});
