$(document).ready(function() {
    /**
     @TODO: Refatorar
     Obter preco - descobrir motivo de utilizacao e adicionar comentario
     */
    function obter_preco() {
		$('[name = "adicionar_produto"]').attr('disabled', true);
        var codigo_produto = $('input[name="codigo_produto"]').val();        
        var codigo_tabela_precos = temp_tabela_preco;
		var filial = $('select[name=filial]').val();
		var tipo_sessao = temp_tipo.replace('/', '');
		
        $.ajax({
            type: "POST",
            url: temp_site_url + "criar_pedidos/obter_preco_produto",
            data: {codigo_produto: codigo_produto, codigo_tabela_precos: codigo_tabela_precos, filial: filial, tipo_sessao: tipo_sessao},
            success: function(preco) {

                $('#preco').val(preco);
                //CUSTOM: MPD-220_Walsywa_01_05082013 - 09/08/2013
                $('#hidden_preco').val(preco);
                //CUSTOM: MPD-220_Walsywa_01_05082013 - 09/08/2013
				// CHAMADO 001856
				if($('#preco').val() != 0) {
					$('[name = "adicionar_produto"]').attr('disabled', false);
				}
            }
        });

    }

    if ($('#produto').length > 0) {
        $('#produto').change(function() {
            obter_preco();
        });

        $('#produto').click(function() {
            obter_preco();
        });

        $('#produto').focus(function() {
            obter_preco();
        });

        $('#produto').keyup(function() {
            obter_preco();
        });
    }


    if ($('select[name=filial]').length > 0) {
        $('select[name=filial]').change(function() {
            obter_preco();
        });
    }


    /** Fim obtem preoco produto*/


    // Obter lista de entrada em estoque
    $(".consulta_entrada").live("click", function(e) {
        var array = explode("|", $(this).attr("alt"));
        var filial = array[0];
        var codigo = array[1];
        $.ajax({
            url: temp_site_url + 'criar_pedidos/listar_estoque',
            data: {filial: filial, codigo: codigo},
            method: "GET",
            dataType: "json",
            success: function(json) {
                $("#chegadas").html(json.tabela);
                $("#chegadas").attr("title", json.titulo);
                $("#chegadas").dialog({
                    modal: false,
                    width: 400,
                    buttons: {
                        Ok: function() {
                            $(this).dialog("close");
                        }
                    }
                });
            }
        });
    });

    // Excluir Produto
    $(".exlcuir_produto").live("click", function(e) {
        e.preventDefault();

        if (confirm("Tem certeza que deseja excluir o produto \"" + $(this).attr("title") + "\" ?")) {
            $.ajax({
                type: "POST",
                url: temp_site_url + 'criar_pedidos/excluir_produto' + temp_tipo,
                data: {indice: $(this).attr("href")},
                success: function(data) {
                    data = data.replace(" ", "");
                    if (data == "1")
                    {
                        location.href = temp_site_url + 'criar_pedidos/adicionar_produtos' + temp_tipo;
                    }
                    else
                    {
                        alert("Não foi possível excluir o produto.");
                    }

                }
            });
        }
    });

    $(".exlcuir_produto_programado").live("click", function(e) {
        e.preventDefault();

        if (confirm("Tem certeza que deseja excluir o produto \"" + $(this).attr("title") + "\" ?")) {
            $.ajax({
                type: "POST",
                url: temp_site_url + 'criar_pedidos/excluir_produto' + temp_tipo,
                data: {indice: $(this).attr("href"), programado: "TRUE"},
                success: function(data) {
                    data = data.replace(" ", "");
                    if (data == "1")
                    {
                        location.href = temp_site_url + 'criar_pedidos/adicionar_produtos' + temp_tipo;
                    }
                    else
                    {
                        alert("Não foi possível excluir o produto.");
                    }
                }
            });
        }
    });


    // Alterar Quantidade
    $(".alterar_quantidade").live("click", function(e) {
        e.preventDefault();
        var quantidade = prompt("Digite a nova quantidade:");

        if (quantidade)
        {
            quantidade = quantidade.replace(" ", "");
            var regex = /^[0-9]+$/;
            if (regex.test(quantidade))
            {
                $.ajax({
                    type: "POST",
                    url: temp_site_url + 'criar_pedidos/alterar_quantidade' + temp_tipo,
                    data: {indice: $(this).attr("href"), quantidade: quantidade},
                    success: function(data) {
                        data = data.replace(" ", "");
                        var dados = data.split("|");

                        if (dados[0] == "1")
                        {
                            location.href = temp_site_url + 'criar_pedidos/adicionar_produtos' + temp_tipo;
                        }
                        else if (dados[0] == "2")
                        {
                            alert("A quantidade disponível no estoque é " + dados[1] + ".");
                        }
                        else
                        {
                            alert("Não foi possível alterar a quantidade do produto.");
                        }

                    }
                });
            }
            else
            {
                alert("O valor digitado não é valido.");
            }
        }

    });

    // Alterar Quantidade Programado
    $(".alterar_quantidade_programado").live("click", function(e) {
        e.preventDefault();

        var quantidade = prompt("Digite a nova quantidade:");

        if (quantidade)
        {
            quantidade = quantidade.replace(" ", "");
            var regex = /^[0-9]+$/;
            if (regex.test(quantidade))
            {

                $.ajax({
                    type: "POST",
                    url: temp_site_url + 'criar_pedidos/alterar_quantidade' + temp_tipo,
                    data: {indice: $(this).attr("href"), quantidade: quantidade, programado: "TRUE"},
                    success: function(data) {
                        data = data.replace(" ", "");
                        var dados = data.split("|");

                        if (dados[0] == "1")
                        {
                            location.href = temp_site_url + 'criar_pedidos/adicionar_produtos' + temp_tipo;
                        }
                        else if (dados[0] == "2")
                        {
                            alert("A quantidade disponível no estoque é " + dados[1] + ".");
                        }
                        else
                        {
                            alert("Não foi possível alterar a quantidade do produto.");
                        }

                    }
                });
            }
            else
            {
                alert("O valor digitado não é valido.");
            }
        }

    });


    // Alterar Desconto
    $(".alterar_desconto").live("click", function(e)
    {
        e.preventDefault();

        var desconto = prompt("Digite o novo percentual de desconto:");


        if (desconto)
        {
            desconto = desconto.replace(" ", "");
            desconto = desconto.replace(",", ".");
            var regex = /^[0-9]+$/;
            if (regex.test(desconto))
            {

                $.ajax({
                    type: "POST",
                    url: temp_site_url + 'criar_pedidos/alterar_desconto' + temp_tipo,
                    data: {indice: $(this).attr("href"), desconto: desconto},
                    success: function(data) {
                        data = data.replace(" ", "");
                        var dados = data.split("|");

                        if (dados[0] == "1")
                        {
                            alert("Percentual de desconto inválido.");
                        } else if (dados[0] == "0") {
                            location.href = temp_site_url + 'criar_pedidos/adicionar_produtos' + temp_tipo;
                        } else if (dados[0] == "2") {
                            alert("Desconto inválido para esta classe de cliente.");
                        }

                    }
                });
            }
            else
            {
                alert("O valor digitado não é valido.");
            }
        }

    });


    // Alterar Desconto Programado
    $(".alterar_desconto_programado").live("click", function(e)
    {
        e.preventDefault();

        var desconto = prompt("Digite o novo percentual de desconto:");

        if (desconto)
        {
            desconto = desconto.replace(" ", "");
            var regex = /^[0-9]+$/;
            if (regex.test(desconto))
            {

                $.ajax({
                    type: "POST",
                    url: temp_site_url + 'criar_pedidos/alterar_desconto' + temp_tipo,
                    data: {indice: $(this).attr("href"), desconto: desconto, programado: "TRUE"},
                    success: function(data) {
                        data = data.replace(" ", "");
                        var dados = data.split("|");

                        if (dados[0] == "1")
                        {
                            alert("Percentual de desconto inválido.");
                        } else if (dados[0] == "0") {
                            location.href = temp_site_url + 'criar_pedidos/adicionar_produtos' + temp_tipo;
                        } else if (dados[0] == "2") {
                            alert("Desconto inválido para esta classe de cliente.");
                        }

                    }
                });
            }
            else
            {
                alert("O valor digitado não é valido.");
            }
        }

    });

    // Alterar Quantidade
    $(".transferir_quantidade").live("click", function(e)
    {
        e.preventDefault();

        var valor = prompt("Digite a quantidade a ser transferida:");


        if (valor)
        {
            valor = valor.replace(" ", "");
            var regex = /^[0-9]+$/;
            if (regex.test(valor))
            {

                $.ajax({
                    type: "POST",
                    url: temp_site_url + 'criar_pedidos/transferir_quantidade' + temp_tipo,
                    data: {indice: $(this).attr("href"), valor: valor},
                    success: function(data) {
                        data = data.replace(" ", "");
                        var dados = data.split("|");

                        if (dados[0] == "1")
                        {
                            location.href = temp_site_url + 'criar_pedidos/adicionar_produtos' + temp_tipo;
                        }
                        else if (dados[0] == "2")
                        {
                            alert("A quantidade disponível para transferência é " + dados[1] + ".");
                        }
                        else
                        {
                            alert("Não foi possível transferir a quantidade do produto.");
                        }

                    }
                });
            }
            else
            {
                alert("O valor digitado não é valido.");
            }
        }

    });

    $(".transferir_quantidade_programada").live("click", function(e)
    {
        e.preventDefault();

        var valor = prompt("Digite a quantidade a ser transferida:");


        if (valor)
        {
            valor = valor.replace(" ", "");
            var regex = /^[0-9]+$/;
            if (regex.test(valor))
            {

                $.ajax({
                    type: "POST",
                    url: temp_site_url + 'criar_pedidos/transferir_quantidade_programada' + temp_tipo,
                    data: {indice: $(this).attr("href"), valor: valor},
                    success: function(data) {
                        data = data.replace(" ", "");
                        var dados = data.split("|");

                        if (dados[0] == "1")
                        {
                            location.href = temp_site_url + 'criar_pedidos/adicionar_produtos' + temp_tipo;
                        }
                        else if (dados[0] == "2")
                        {
                            alert("A quantidade disponível para transferência é " + dados[1] + ".");
                        }
                        else
                        {
                            alert("Não foi possível transferir a quantidade do produto.");
                        }

                    }
                });
            }
            else
            {
                alert("O valor digitado não é valido.");
            }
        }
    });
    $(".transferir_tudo").live("click", function(e)
    {
        e.preventDefault();

        if (confirm("Tem certeza que deseja transferir todos os produtos\n do pedido imediato para o pedido programado?")) {

            $.ajax({
                type: "POST",
                url: temp_site_url + 'criar_pedidos/transferir_tudo' + temp_tipo,
                data: {valor: true},
                dataType: "json",
                success: function(data) {
                    if (data.status == 1)
                    {
                        alert(data.mensagem);
                        location.href = temp_site_url + 'criar_pedidos/adicionar_produtos' + temp_tipo;
                    }
                    else
                    {
                        alert(data.mensagem);
                    }

                }
            });
        }
    });



});


