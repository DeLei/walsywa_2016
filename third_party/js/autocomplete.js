/**
 * Fun��o:		setarCoresAutocomplete()
 * 
 * Descri��o:	Fun��o usada para colorir o autocomplete 
 * 				assim � pooss�vel diferenciar clientes Bloqueados de Ativos
 * 
 * @return string 
 */
function setarCoresAutocomplete() 
{
    var oldFn = $.ui.autocomplete.prototype._renderItem;

    $.ui.autocomplete.prototype._renderItem = function( ul, item) {
        var re = new RegExp("^" + this.term, "i") ;
        var classCSS = '000';
        var t = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<span style='color:#000;font-weight:bold;'>$1</span>");
       if(item.cor === 'V'){
		classCSS = 'red';
	   }else if(item.cor === 'A'){
		classCSS = 'blue';
	   }      
        
        //Caso for exibi��o de produtos adicionar span para exibi��o do icone 'P'
        
        return $( '<li></li>' )
        .data( "item.autocomplete", item )      
        .append( "<a>"+'<p class="descricao_produto" style="color: '+classCSS+';">'+t+'</p>'+ "</a>" )
        .appendTo( ul );
    };
}

function resetarAutocomplete(divnome) {
	var _this = divnome;
	
	$(divnome).autocomplete({
		source: $(divnome).attr('data-url'),
		minLength: 4,
		focus: function(evento, ui)
		{
			$(divnome).val(ui.item.label).next().val(ui.item.value);
			
			return false;
		},
		select: function(evento, ui)
		{
			$(divnome).val(ui.item.label).next().val(ui.item.value);
			
			return false;
		}
	});
}

$(document).ready(function() {
	setarCoresAutocomplete();
	$('.autocomplete').each(function() {
		var _this = this;
		
		// vamos alterar o nome do autocomplete, porque o valor que queremos � o autocomplete.value e n�o autocomplete.label
		
		
		$(this).after('<input type="hidden" name="' + $(this).attr('name') + '" value="' + ($(this).data('valor') ? $(this).attr('data-valor') : $(this).attr('alt')) + '">').attr('name', '_' + $(this).attr('name'));
		
		$(this).autocomplete({
			source: $(this).data('url'),
			minLength: 4,
			focus: function(evento, ui)
			{
				$(this).val(ui.item.label).next().val(ui.item.value);
				
				return false;
			},
			select: function(evento, ui)
			{
				$(this).val(ui.item.label).next().val(ui.item.value);
				
				return false;
			}
		});
	});
	
	
	$('.autocomplete_pendente_cliente').each(function() {
		var _this = this;
		
		// vamos alterar o nome do autocomplete, porque o valor que queremos � o autocomplete.value e n�o autocomplete.label
		$(this).after('<input type="hidden" name="' + $(this).attr('name') + '" value="' + ($(this).data('valor') ? $(this).data('valor') : $(this).attr('alt')) + '">').attr('name', '_' + $(this).attr('name'));
		
		$(this).autocomplete({
			source: function(request, response) {
					$.ajax({
						dataType: 'json',
						url: $(_this).data('url'),
						data: {
							codigo_loja_cliente: $('[name=codigo_loja_cliente').val(),	
							filial: 		$('[name=filial').val(),
							term:			request.term
							},
						success: function(dados){
							
							
							response($.map(dados, function(results) {
								return results;
									
							}));
						}
					
					});
					
				
			},
			
			
			minLength: 4,
			focus: function(evento, ui)
			{
				$(this).val(ui.item.label).next().val(ui.item.value);
				
				return false;
			},
			select: function(evento, ui)
			{
				$(this).val(ui.item.label).next().val(ui.item.value);
				
				
				$('[name=codigo_nota_fiscal]').val(ui.item.item.nota_fiscal+'|'+ui.item.item.serie);
				$('[name=_codigo_nota_fiscal]').val(ui.item.item.nota_fiscal+'/'+ui.item.item.serie);
				$('[name=pedido]').val(ui.item.item.codigo);
				$('[name=_pedido]').val(''+ ui.item.item.codigo);
				
				return false;
			}
		});
	});

	
	
	
	
	
	
});
